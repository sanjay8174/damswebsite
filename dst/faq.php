<html>
    <head> 
        <meta charset="utf-8">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>DAMS Scholarship</title>
        <link href="css/style.css?v=14" rel="stylesheet" type="text/css">
        <script src="js/script1.js"></script>   
         <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/registration.js"></script>
        <script>
//            function regform() {
//                $(".reg-form").css("display", "block");
//                $(".pop_reg_btn").css("display", "block");
//            }
            function regsw() {
                $('#firstpage').show();
                $('#secondpage').hide();
                $("#regdlg").css("display", "block");
                $("#dialog").css("display", "none");
                $(".res_main_pop").removeClass("pop_hide");
            }
            function regswhide() {
                $(".res_main_pop").addClass("pop_hide");
                setTimeout(function () {
                    $("#regdlg").css("display", "none");
                }, 300);
            }
        </script>
    </head>
    <body>
        
        <div class="outer-cover">
            <div class="wapper-main">
            <h1 class="page-heading">Help & Support</h1>
            <p class="page-sub">Let's take a step ahead and help you better.</p>
            <div class="form-div">
                <h2>FAQs</h2>
                <div class="question-div">
                    <span class="que-mark">Q1.</span>
                    <h3>How can I register for DAMS DST?</h3>
                    <p>By login to DAMS DST website click on <a href="javascrpit:void(0);" onclick="regsw()">register</a>.</p>
                </div>
                <div class="question-div">
                    <span class="que-mark">Q2.</span>
                    <h3>How can I login for DAMS DST?</h3>
                    <p>A link will be provided to you to login for exam.</p>
                </div>
                <div class="question-div">
                    <span class="que-mark">Q3.</span>
                    <h3>Will DAMS DST conduct in English or Hindi?</h3>
                    <p> In English only</p>
                </div>
                <div class="question-div">
                    <span class="que-mark">Q4.</span>
                    <h3>Can we give test on any device (PC, Laptop, Mobile, Tablet) or on particular device?</h3>
                    <p>Yes</p>
                </div>
                <div class="question-div">
                    <span class="que-mark">Q5.</span>
                    <h3>How will I get result of DAMS DST?</h3>
                    <p>Result will be announced on website</p>
                </div>
                <div class="question-div">
                    <span class="que-mark">Q6.</span>
                    <h3>Can I download my DAMS DST paper?</h3>
                    <p> No</p>
                </div>
                <div class="question-div">
                    <span class="que-mark">Q7.</span>
                    <h3>Can I appear my DAMS DST Exam without Internet connection?</h3>
                    <p> No</p>
                </div>
                <div class="question-div">
                    <span class="que-mark">Q8.</span>
                    <h3> Can I pause or suspend DAMS DST?</h3>
                    <p> No</p>
                </div>
                <div class="question-div">
                    <span class="que-mark">Q9.</span>
                    <h3> Can I switch my screen and go to another tab during online exam?</h3>
                    <p>No</p>
                </div>
                <div class="question-div">
                    <span class="que-mark">Q10.</span>
                    <h3>What will happen If some problem occur during DAMS DST?</h3>
                    <p>You can resume your test</p>
                </div>
            </div>
        </div>
        </div>
        <!--Registration popup start by MAYANK PATHAK-->
        <div id="regdlg" class="reg-form" style="display: none">
            <div class="res_pop_bg"></div>     
            <div class="res_main_pop registration reg_pop_wapper">
                <!--<b class="reg_back" onClick="regswhide()"></b>-->
                <span onclick="regswhide()" ></span>
                <div class="pop_title">Registration for Online Exam</div>
                <form name="studentRegistration" action="index.php?p=submit" enctype="multipart/form-data" onsubmit="return validateRegistration();" method="post">

                    <input type="hidden" name="sessionY" id="sessionY" value="1" />
                    <input type="hidden" name="mode" id="mode" value="register" />
                    <div id="firstpage">
                    <p>
                        <label>Name</label>
                        <input type="text" id="name"  name="Stuidentname" placeholder="Full Name"  >
                    </p>
                    <p>
                        <label>Email Address</label>
                        <input type="text" id="email" name="email" placeholder="Email Address">
                    </p>
                    <p>
                        <label>Phone / Mob.</label>
                        <input type="text" id="mobile" name="mobile" autocomplete="off" maxlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" placeholder="Mobile number">
                    </p>
                    <div class="reg_cls_strm">
                        <aside>
                            <label>Password</label>
                            <input type="password" id="password" name="password" placeholder="Enter Password">
                        </aside>
                        <aside style="float:right;">
                            <label>Confirm Password</label>
                            <input type="password" id="Cnfpassword" name="Cnpassword" placeholder="Confirm Password">
                        </aside>
                    </div>
                    <div class="reg_cls_strm">
<!--                        <aside style="width:100%;">
                            <label>Select Paper Category</label>
                            <select id="paperSelect" name="paperSelect" onchange="showCatDiv(this.value);">
                                <option value="0" selected="selected">Select Category</option>
                                <option value="1"> Paper A</option>
                                <option value="2"> Paper B</option>
                                <option value="3"> Paper C</option>
                                <?php
//                                AddressUtil::Itam('course');
//                                
                                ?>
                            </select>
                          
                        </aside>-->
                        <div class="ermsg" id="paperA">Anatomy, Biochemistry and Physiology (100 Q) Eligible only for prefoundation course
                                      <br>Eligibility: 2/3<sup>rd</sup>  students (1<sup>st</sup> & 2<sup>nd</sup> year)</div>
                        <div class="ermsg" id="paperB">ABP plus pathology, pharmacology, microbiology and forensic (100 ) Eligible only for foundation course<br>Eligibility : 5/6 students (3<sup>rd</sup> & final year)</div>
                        <div class="ermsg" id="paperC">All Subjects 100 Qs Eligible for Regular and Test and discussion courses Interns/Post intern students.</div>
                    </div>
                    
                    <div class="reg_cls_strm">
                        <aside style="width:100%;">
                            <label>Select Course</label>
                            <select id="courseSelect" name="course_Data" >
                                <!--<option value="0" selected="selected">Select Course</option>-->
                                <option value="" > Select Course</option>
                                <option value="1" > MD/MS Entrance</option>                                
<!--                                <option value="2"> MCI SCREENING</option>
                                <option value="3"> MDS QUEST</option>
                                <option value="4"> USMLE EDGE</option>
                                <option value="5"1.5> MRCP</option>
                                <option value="6"> MRCOG</option>
                                <option value="7"> PLAB</option>
                                <option value="8"> MRCGP</option>-->

                                <?php
//                                AddressUtil::Itam('course');
//                                
                                ?>
                            </select>
                            <!--<input type="text"  name="course_name" value="" id="course_name"/>-->
                        </aside>
                    </div>
                    <div class="reg_cls_strm">
                        <aside style="width:100%;">
                            <label>Select Year</label>
                            <select id="passingYear" name="passingYear" >
                                <!--<option value="0" selected="selected">Select Course</option>-->
                                <option value="" > Select Year</option>
                                <option value="1" > First Year</option>                                
                                <option value="2"> Second Year</option>
                                <option value="3"> Third Year</option>
                                <option value="4"> Final Year</option>
                                <option value="5">Interns </option>
                                <option value="6"> Post Interns</option>

                            </select>
                            <!--<input type="text"  name="course_name" value="" id="course_name"/>-->
                        </aside>
                    </div>
                    <div class="reg_cls_strm" >
                        <aside style="width:100%;">
                            <label>Country</label>
                            <select id="countrySelect" name="country" onchange="countryChange(this.value)">
                                <option value="0" selected="selected">Select Country</option>
                                <?php
                                AddressUtil::Itam('countrySelect', '1');
                                ?>
                            </select>
                            
                        </aside>
                        
                    </div>
                    <div class="reg_cls_strm" >
                        <aside>
                            <label>State</label>
                            <select id="stateSelect" name="state" onchange="stateChange(this.value)">
                                <option value="0" selected="selected">Select State</option>
                                <?php
                                AddressUtil::Itam('stateSelect', '1');
                                ?>
                            </select>
                            <input type='hidden' name="stateName" id="stateName" value="" >
                        </aside>
                        <aside style="float:right;" id="cityMapdiv">
                            <label>City</label>
                            <select id="citySelect" name="city">
                                <option value="0" selected="selected">Select City</option>
                                <?php
                                AddressUtil::Itam('citySelectValue');
                                ?>    
                            </select>
                            <input type='hidden' name="cityName" id="cityName" value="" >
                        </aside>
                    </div>
                    <p >
                        <label>Collage Name</label>
                        <input type="text" id="collage" name="collage" placeholder="College Name">
                    </p>
                    </div>
                    <div id="secondpage" class="second-data" style="display:none">
                        <p style="text-align:center;font-weight: bold">
                        Instructions:
                       </p>
                        <div class="aftr_inr_cntr featrs_wpr points" >
                        <ul>
                            <li>Exam will be conducted in the safety of your home with  secure Online Remote Proctored Examination Technology. </li>
                            <li>Exam will be conducted on PC/Laptop with 4GB RAM or Mobile. </li>
			    <li> Webcam/Front camera of mobile is must. </li>
			    <li>Adequate Internet speed for exam suggested of minimum 1 mbps. </li>
			    <li>Power backup is must. </li>
			    <li>Latest version of Google Chrome or Mozilla to be installed before starting of exam . </li>
			 
                        </ul>
                            <p class="pop_reg_btn new data-inst">
                            <a href="#"  class="acpt_trms"><input type="checkbox" id="chkbx" class="chkbx">I 've read the instructions carefully<br><small> I want to proceed for registration</small></a>
                     
                           </p>
                         </div>
                    </div>
                     <p class="pop_reg_btn new">
                                 <input type="submit" value="Register" id="formSubmit">
                    </p>
                </form>
            </div>
        </div>
        <!--Registration popup end-->
        
    </body>
      
</html>