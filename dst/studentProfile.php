<?php
if ($_SESSION[student_id] == '') {
    header('location:index.php');
} else {
    $data = $queryDao->studentData($_SESSION[student_id]);
}
?>

<html>
    <head> 
        <meta charset="utf-8">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>DAMS Scholarship</title>
        <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
        <link href="css/style.css?v=15" rel="stylesheet" type="text/css">
        <link href="css/responsive.css?v=8" rel="stylesheet" type="text/css">
        <link href="css/font/roboto/stylesheet.css" rel="stylesheet" type="text/css">
        <script src="js/script1.js"></script>         
        <script src="js/owl.carousel1.js"></script>
        <!--<script src="js/jquery-1.7.2.min.js"></script>-->
        <script src="js/responsiveTabs.js"></script>  
    </head>
    <body>
        <header id="nav-anchor" style="text-align: center;">
            <div class="nav_fixed">
                <div class="top_bar"></div>
                <div class="main_wrappr" style="display:inline-block;overflow:visible;" >
                    <a href="index.php" class="logo" style="text-align: left;">
                        <img  src="images/logo.png">
                    </a>
                    <nav class="nav_cntr">
                        <a class="toggleMenu" href="#"></a>
                        <ul class="nav">
                            <li class="test"><a href="javascript:void(0)" class="selected" id="li_a_overview">My Profile</a></li>
                            <li class="test"><a href="index.php?p=myOrder"  id="li_a_overview">My Order</a></li>
                            <li><a href="index.php?p=faq" target="_blank">FAQs</a></li>
                            <li class="reg_btn_nav" ><a  id="li_a_register" class="last" href="logout.php">Logout</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div class="custom-cartBanner-wrapper">
            <div class="custom-page-center">
                <h2>Student Profile</h2>
            </div>
        </div>
        <div class="cart-packagelist-wrapper">
            <div class="custom-page-center">
                <h3>Student Profile</h3>
                <div class="instruction-wrapper">
                    <form action="index.php?p=submit" method="post" id="editForm" name="editForm">
                    <div class="detail-list">
                        <h6>Personal Details</h6>
                        <div class="personal-detail-box">
                            <div class="personal-detail-list">
                                <label>Full Name</label>
                                <div class="edit-div">
                                    <input type="text" id="name" name="Stuidentname" placeholder="Full Name" disabled="" value="<?php echo $data->STUDENT_NAME; ?>">
                                </div>
                            </div>
                            <input type="hidden" name="mode" value="editstudent">
                            <div class="personal-detail-list">
                                <label>Email Address</label>
                                <div class="edit-div">
                                    <input type="text" id="email" name="email" placeholder="Email Address"  value="<?php echo $data->EMAIL; ?>">
                                </div>
                            </div>
                            <div class="cl"></div>
                        </div>
                        <div class="personal-detail-box">
                            <div class="personal-detail-list">
                                <label>Phone / Mob.</label>
                                <div class="edit-div">
                                    <input type="text" id="mobile" name="mobile" autocomplete="off" maxlength="10" placeholder="Mobile number" value="<?php echo $data->MOBILE; ?>">
                                </div>
                            </div>
                            <div class="personal-detail-list">
                                <label>Password</label>
                                <div class="edit-div">
                                    <input type="password" id="password" name="password" placeholder="Enter Password" value="<?php echo $data->PASSWORD; ?>">
                                </div>
                            </div>
                            <div class="cl"></div>
                        </div>
                        <div class="personal-detail-box">
                            <div class="personal-detail-list">
                                <label>Collage Name</label>
                                <div class="edit-div">
                                    <input type="text" disabled="" id="collage" name="collage" placeholder="College Name" value="<?php echo $data->COLLEGE_NAME; ?>" >
                                </div>
                            </div>
                            <!--							<div class="personal-detail-list">
                                                                                            <label>Confirm Password</label>
                                                                                            <div class="edit-div">
                                                                                            <input type="password" id="Cnfpassword" name="Cnpassword" placeholder="Confirm Password">
                                                                                                    </div>
                                                                                    </div>-->
                            <div class="personal-detail-list">
                                <label>Select Course</label>
                                <div class="edit-div">
                                    <select disabled="" id="courseSelect" name="course_Data">
                                        <!--<option value=""> Select Course</option>-->
                                        <option value="1"> MD/MS Entrance</option>    

                                    </select>
                                </div>
                            </div>
                            <div class="cl"></div>
                        </div>
                        <div class="personal-detail-box">
                            <div class="personal-detail-list">
                                <label>Select Year</label>
                                <div class="edit-div">
                                    <select id="passingYear" name="passingYear" >
                                        <option value=""> Select Year</option>
                                        <option value="1" <?php if ($data->COURSE_YEAR == 1) {
                                                                echo"selected";
                                                            }; ?> > First Year</option>                                
                                        <option value="2" <?php if ($data->COURSE_YEAR == 2) {
                                                                    echo"selected";
                                                                }; ?> > Second Year</option>
                                        <option value="3" <?php if ($data->COURSE_YEAR == 3) {
                                                        echo"selected";
                                                    }; ?> > Third Year</option>
                                        <option value="4" <?php if ($data->COURSE_YEAR == 4) {
                                                            echo"selected";
                                                        }; ?> > Final Year</option>
                                        <option value="5" <?php if ($data->COURSE_YEAR == 5) {
                                                                    echo"selected";
                                                                }; ?> >Interns </option>
                                        <option value="6" <?php if ($data->COURSE_YEAR == 6) {
                                                                    echo"selected";
                                                                }; ?> > Post Interns</option>

                                    </select>
                                </div>
                            </div>
                            <div class="personal-detail-list">
                                <label>Country</label>
                                <div class="edit-div">
                                    <select id="countrySelect" name="country" disabled="" onchange="countryChange(this.value)">
                                        <option <?php if ($data->COUNTRY_ID == 1) {
                                                                    echo"selected";
                                                                }; ?> >INDIA</option>
                                        <option <?php if ($data->COUNTRY_ID == 43) { echo"selected";
                                                                }; ?> >NEPAL</option>
                                    </select>
                                </div>
                            </div>
                            <div class="cl"></div>
                        </div>
                        <div class="personal-detail-box">
                            <div class="personal-detail-list">
                                <label>State</label>
                                <div class="edit-div">
                                    <select id="stateSelect" name="state" disabled="">
                                        <?php
                                        echo AddressUtil::Itam("stateSelect", "$data->COUNTRY_ID",$data->STATE_ID);
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="personal-detail-list">
                                <label>City</label>
                                <div class="edit-div">
                                    <select id="citySelect" name="city" disabled="">
                                        <?php
                                        echo AddressUtil::Itam("citySelect", "$data->STATE_ID",$data->CITY_ID);
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="cl"></div>
                        </div>
                        <div class="personal-detail-box">
                            <div class="personal-detail-list">
                                <a onclick=" editval();" id="formSubmit">Update</a>
                            </div>

                            <div class="cl"></div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <footer class="dst_footer">
            <div class="main_wrappr sec_wapper">
                <p class="foot_txt">© Delhi Academy of Medical Sciences Pvt. Ltd. All rights reserved.</p>
                <article class="followuson">
                    <a href="https://www.facebook.com/damsdelhiho" class="fb"></a>
                    <a href="https://twitter.com/damsdelhi" class="twt"></a>
                    <a href="https://plus.google.com/u/0/110912384815871611420/about" class="gpls"></a>
                    <a href="http://www.youtube.com/user/damsdelhi" class="yt"></a>

                </article>
            </div>
        </footer>
        <script type="text/javascript">

            $(document).ready(function () {
                RESPONSIVEUI.responsiveTabs();
                $(window).scroll(function () {
                    var window_top = $(window).scrollTop() + 0;
                    var div_top = $('#nav-anchor').offset().top;
                    if (window_top > div_top) {
                        $('.nav_fixed').addClass('stick');
                    } else {
                        $('.nav_fixed').removeClass('stick');
                    }
                });

            });

        </script> 
        <!--Nav fixed js end-->

        <!--Main navigation start-->
    <!--    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>-->
        <script type="text/javascript" src="js/script.js"></script>
        <script type="text/javascript" src="js/registration.js?v=1"></script>
        <script>
            $(function () {
                $('a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });
        </script>
        <!--Main navigation end-->
        <!--Tabs script start-->
        <script>
            $(document).ready(function () {
                $("#content .sw_hd").hide();
                $("#tabs li:first").attr("id", "current");
                $("#content .sw_hd:first").fadeIn();

                $('#tabs a').click(function (e) {
                    e.preventDefault();
                    $("#content .sw_hd").hide();
                    $("#tabs li").attr("id", "");
                    $(this).parent().attr("id", "current");
                    $('#' + $(this).attr('title')).fadeIn();
                });
            })
        </script>

        <!--Tabs video slider code start-->
        <script type="text/javascript">
            //1. set ul width 
            //2. image when click prev/next button
            var ul;
            var li_items;
            var imageNumber;
            var imageWidth;
            var prev, next;
            var currentPostion = 0;
            var currentImage = 0;

            
            function init() {
                ul = document.getElementById('image_slider');
                li_items = ul.children;
                imageNumber = li_items.length;
                imageWidth = li_items[0].children[0].clientWidth;
                ul.style.width = parseInt(imageWidth * imageNumber) + 'px';
                prev = document.getElementById("prev");
                next = document.getElementById("next");
                //.onclike = slide(-1) will be fired when onload;
                /*
                 prev.onclick = function(){slide(-1);};
                 next.onclick = function(){slide(1);};*/
                prev.onclick = function () {
                    onClickPrev();
                };
                next.onclick = function () {
                    onClickNext();
                };
            }

            function animate(opts) {
                var start = new Date;
                var id = setInterval(function () {
                    var timePassed = new Date - start;
                    var progress = timePassed / opts.duration;
                    if (progress > 1) {
                        progress = 1;
                    }
                    var delta = opts.delta(progress);
                    opts.step(delta);
                    if (progress == 1) {
                        clearInterval(id);
                        opts.callback();
                    }
                }, opts.delay || 17);
                //return id;
            }

            function slideTo(imageToGo) {
                var direction;
                var numOfImageToGo = Math.abs(imageToGo - currentImage);
                // slide toward left

                direction = currentImage > imageToGo ? 1 : -1;
                currentPostion = -1 * currentImage * imageWidth;
                var opts = {
                    duration: 500,
                    delta: function (p) {
                        return p;
                    },
                    step: function (delta) {
                        ul.style.left = parseInt(currentPostion + direction * delta * imageWidth * numOfImageToGo) + 'px';
                    },
                    callback: function () {
                        currentImage = imageToGo;
                    }
                };
                animate(opts);
            }

            function onClickPrev() {
                if (currentImage == 0) {
                    slideTo(imageNumber - 1);
                } else {
                    slideTo(currentImage - 1);
                }
            }

            function onClickNext() {
                if (currentImage == imageNumber - 1) {
                    slideTo(0);
                } else {
                    slideTo(currentImage + 1);
                }
            }

            window.onload = init;
        </script>


        <script>

            RESPONSIVEUI.responsiveTabs();

        </script>	
    </body>

</html>