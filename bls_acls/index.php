    <!DOCTYPE html>
<?php 
error_reporting(0);
$course_id = $_REQUEST['c'];

?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>BLS/ACLS Coaching Institute, BLS/ACLS</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="inner-bg no_bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include '../header.php';
$pathCMS = constant::$pathCMS;
?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
          <h2 >BLS/ACLS </h2>
          <h3><span>DAMS now offers American Heart Association certified BLS/ACLS </span></h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<!--<div style="overflow:hidden;">-->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>BLS/ACLS COURSES IN DELHI CENTER <span class="book-ur-seat-btn" style='display:none'><a title="Book Your Seat" href="
http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></span></h3>
        <p style="font-size:22px;padding:5px 0px;">Course Details Format</p><br>
        <p>DAMS now offers American Heart Association certified BLS/ACLS courses in Delhi center. This is part of DAMS vision to be a one stop center for all your needs as a medical student. </p>
        <p style="margin-top: 10px;">Basic life support (BLS) is a level of medical care which is used for victims of life-threatening illnesses or injuries until they can be given full medical care at a hospital. </p>
        <p style="margin-top: 10px;">Advanced cardiac life support or advanced cardiovascular life support (ACLS) refers to a set of clinical interventions for the urgent treatment of cardiac arrest, stroke and other life-threatening medical emergencies, as well as the knowledge and skills to deploy those interventions.</p>
        <p style="margin-top: 15px;"><strong>New batch is on  16th March 2020(BLS) & 17th & 18th March 2020(ACLS).</strong></p>
        <!--            <div class="course-detail-box">
       <div class="course-detail-box1">
                <p>Nephrology</p>
                <p>Immunology</p>
                <p>Statistics</p>
                <p>Neurology</p>
                <p>Cell biology</p>
            </div>
            <div class="course-detail-box2">
                <p>Gastroenterology</p>
                <p>Infectious Disease</p>
                <p>Haematology / Oncology</p>
                <p>Psychiatry</p>
                <p>Rheumatology</p>
            </div>
            <div class="course-detail-box2">
                <p>Respiratory</p>
                <p>Cardiology</p>
                <p>Endocrinology</p>
                <p>Pharmacology</p>
                <p>Dermatology</p>
            </div>
        </div></div>  -->
        
      
      <!--<div 	 style='width:23%;border-radius:3px;float:right;background-color:#00a651;padding:12px 0px;text-align:center;'><p style='font-size:46px;color:#ffffff'>13-15<sup>th</sup></p><p style='font-size:50px;color:#ffffff'>APRIL</p><p style='color:#ffffff;font-size:23px;'>Mon-Tue<span style='font-size:25px;color:#ffffff'>&nbsp;&nbsp;2015</span></p></div>-->
      <?php include 'usmle-middle-accordion.php'; ?>
    </aside>    
    <aside class="content-right">
<!--        <div class="content-royal-college res_css">	
            <img src="images/logo-royal-college.png" />
            <p>MEMBERSHIP OF THE ROYAL COLLEGES</p>
            <p>OF PHYSICIANS OF THE UNITED KINGDOM</p>
        </div>-->
        <div class="content-date-venue res_css" style="margin-top:0px;">
            <h1>DATES</h1>
            <p style="font-weight:bold;font-size:15px;">1. BLS- 16th March 2020.</p>
            <p style="font-weight:bold;font-size:15px;">2. ACLS- 17th & 18th March 2020.</p>
            <p style="border: 1px dotted #c4c4c4;margin:10px 0px;width:95%;"></p>
            <h1>PRICE</h1>
            <p style="font-weight:bold;font-size:15px;">1. BLS- Rs 4000 + GST = Rs 4,720/ -<br>
                2. ACLS - Rs 6000 + GST = Rs 7,080/- <br>
3. BLS+ ACLS - Rs 10000 + GST = Rs 11,800/- </p>
            <p style="border: 1px dotted #c4c4c4;margin:10px 0px;width:95%;"></p>
            <h1>VENUE</h1>
            <p>4 B, Grover Chamber,Pusa Road</p>
            <p>Near Karol Bagh Metro Station</p>
            <p>New Delhi, Delhi - 110005</p>
            <p>Ph. No. : 011-4009 4009</p>
            <p>(Enquiry Timing:- 10am To 8pm)</p>
        </div>
          <?php include 'enquiryform.php'; ?>
      <div class="content-date-venue res_css" style=" width: 75%; margin-top: 15px; display: inline-block; padding: 6px;line-height: 0px;">
        <img src="images/BLSACLS-LOGO.png" width="100%" style="margin:0px; border:0px solid #ccc;">
        </div>
      <div class="content-date-venue res_css" style=" width: 75%; margin-top: 15px; display: inline-block; padding: 6px;line-height: 0px;">
        <img src="images/BLS-1.jpg" width="100%" style="margin:0px; border:0px solid #ccc;">
        </div>
      <div class="content-date-venue res_css" style=" width: 75%; margin-top: 15px; display: inline-block; padding: 6px;line-height: 0px;">
        <img src="images/DSC_0758.png" width="100%" style="margin:0px; border:0px solid #ccc;">
        </div>
      <div class="content-date-venue res_css" style=" width: 75%; margin-top: 15px; display: inline-block; padding: 6px;line-height: 0px;">
        <img src="images/DSC_0751.jpg" width="100%" style="margin:0px; border:0px solid #ccc;">
        </div>
       <!--   <div class="content-date-venue res_css" style=" width: 75%; margin-top: 0px; display: inline-block; padding: 3px;">
        <a target="_blank" href="https://www.youtube.com/embed/f-vZok3Mazo"><img style="float:left;" width="100%" height="236" src="<?php echo $pathCMS.'/youTubeThumbs/f-vZok3Mazo.jpg' ?>"  alt="f-vZok3Mazo" frameborder="0"></a> 
        <iframe style="float:left;" width="100%" height="236" src="https://www.youtube.com/embed/f-vZok3Mazo" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="content-date-venue res_css" style=" width: 75%; margin-top: 0; display: inline-block; padding: 3px;">
        <a target="_blank" href="https://www.youtube.com/embed/3rwQhxc7xt0"><img style="float:left;" width="100%" height="236" src="<?php echo $pathCMS.'/youTubeThumbs/3rwQhxc7xt0.jpg' ?>"  alt="3rwQhxc7xt0" frameborder="0"></a>  
        <iframe style="float:left;" width="100%" height="236" src="https://www.youtube.com/embed/3rwQhxc7xt0" frameborder="0" allowfullscreen></iframe>
        </div>
        
        <div class="content-date-venue res_css" style=" padding: 0; width: 78%; border: 0; margin-top: 0; display: inline-block;">
         <img src="images/mrcp1.jpg" style="width:100%; margin-bottom:5px;" />
         <img src="images/mrcp2.jpg" style="width:100%;" />
        </div>-->
        
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php //echo "Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"; ?>
<?php include 'footernew.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<!--<script type="text/javascript">
 var m=0;
 var l=0;
 var n=0;
 var videointerval ='';
 var newsinterval = '';
 $(document).ready(function(){
   var count1=$("#recent li").length;
   $("#prev").click(function(){
	 if(m>0)
	 {
	   var j=m+3;
       $("#li"+j).hide('fast','linear');
	   m--;
	   $("#li"+m).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   $(".slide-arrow > a.left").css('background','url(images/right.png)');
	   if(m==0)
	   {
		 $(".slide-arrow > a.right").css('background','url(images/left.png)');   
	   }
	 }
   });
   $("#next").click(function(){	 
	 if(m<count1-4)
	 {
       $("#li"+m).hide('fast','linear');
	   var j=m+4;
	   $("#li"+j).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   m++;
	   if(m==(count1-4))
	   {
		 $(".slide-arrow > a.left").css('background','url(images/right-inactive.png)');
	   }
     }	 
   });

   window.onload = function()
   {	  
	    var count2=$(".videos-content-box > img").length;
	    var count3=$(".news-content-box > ul").length;
        function setvideo()
		{
		   l=l%count2;		
		   $("#vd"+l).fadeOut('slow','linear');
		   $("#v"+l).removeClass("current");
	       var j=(l+1)%count2;			
		   $("#vd"+j).fadeIn('slow','linear');
		   $("#v"+j).addClass("current");        	
	       l++;	
        }
     videointerval = setInterval(setvideo, 5000);
   }
 });
 function news(val)
 {
   if(val=='0')
   {	   
	  $("#ul1").hide();
	  $("#u1").removeClass("current"); 
	  $("#ul2").hide();
	  $("#u2").removeClass("current");
	  $("#ul0").slideDown(1000,'linear');
	  $("#u0").addClass("current");   
   }
   if(val=='1')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul2").hide();
	 $("#u2").removeClass("current");
	 $("#ul1").slideDown(1000,'linear');
	 $("#u1").addClass("current"); 
   }
   if(val=='2')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul1").hide();
	 $("#u1").removeClass("current");
	 $("#ul2").slideDown(1000,'linear');
	 $("#u2").addClass("current");
   } 
 }
 
 function video(val)
 {
   setTimeout('videointerval', 5000);
   if(val=='0')
   {	   
      $("#vd1").hide();
	  $("#vd1").removeClass("display_block");
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#vd2").removeClass("display_block");
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#vd3").removeClass("display_block");
	  $("#v3").removeClass("current");
	  $("#vd0").addClass("display_block");
	  $("#vd0").fadeIn('fast','linear');
	  $("#vd0").removeClass("display_none");
	  $("#v0").addClass("current");
   }
   if(val=='1')
   {	   
      $("#vd0").hide();
	  $("#vd0").removeClass("display_block");
	  $("#v0").removeClass("current");
	  $("#vd2").hide();
	  $("#vd2").removeClass("display_block");
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#vd3").removeClass("display_block");
	  $("#v3").removeClass("current");
	  $("#vd1").addClass("display_block");
	  $("#vd1").fadeIn('fast','linear');
	  $("#vd1").removeClass("display_none");
	  $("#v1").addClass("current");
   }
   if(val=='2')
   {	   
      $("#vd0").hide();
	  $("#vd0").removeClass("display_block");
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#vd1").removeClass("display_block");
	  $("#v1").removeClass("current");
	  $("#vd3").hide();
	  $("#vd3").removeClass("display_block");
	  $("#v3").removeClass("current");
	  $("#vd2").addClass("display_block");
	  $("#vd2").fadeIn('fast','linear');
	  $("#vd2").removeClass("display_none");
	  $("#v2").addClass("current");
   }
   if(val=='3')
   {	   
      $("#vd0").hide();
	  $("#vd0").removeClass("display_block");
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#vd1").removeClass("display_block");
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#vd2").removeClass("display_block");
	  $("#v2").removeClass("current");
	  $("#vd3").addClass("display_block");
	  $("#vd3").fadeIn('fast','linear');
	  $("#vd3").removeClass("display_none");
	  $("#v3").addClass("current");
   }
   l=val;
   videointerval = setInterval(setnews, 5000);  
 }
 
 function sliddes(val)
{
   var sp=$('.h2toggle > span').size();
   for(var d=1;d<=sp;d++)
   {   
       if(val!=d)
	   { 
    	 $('#s'+d).removeClass('minus-ico');
         $('#s'+d).addClass('plus-ico');
		 $('#di'+d).slideUp(400);
	   }      
   }
   
   $('#di'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#s'+val).removeClass('plus-ico');
            $('#s'+val).addClass('minus-ico');
       }
	   else
	   {
			$('#s'+val).removeClass('minus-ico');
            $('#s'+val).addClass('plus-ico');
       }
   });
}
</script>-->
</body>
</html>
