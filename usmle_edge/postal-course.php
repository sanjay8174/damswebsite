<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<!--<link href="css/font-face.css" rel="stylesheet" type="text/css" />-->
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->
<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left banner-left-postion">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <aside class="banner-right">
        <div class="banner-right-btns"> 
         <!--<a href="dams-publication.php?c=1" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a>-->
           <a href="http://damspublications.com/" target="_blank" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> 
         <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> 
         <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> 
        </div>
      </aside>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li style="background:none;"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Postal Course" class="active-link">Postal Course</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading">
            <h4>Postal Course
              <div class="book-ur-seat-btn book-hide"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
            </h4>
            <article class="showme-main">
              <aside class="privacy-content">
                <p>If you can not come to DAMS, our courses will come to you. Join our Postal Series Delhi Academy of Medical Sciences (DAMS) provides postal courses. The study material is developed by DAMS, which has been working since last one decade. The study material is compact &amp; effective which is neither bulky nor vague. Instead it is easy to understand and has unique presentation of all the subject as per requirements of examination. DAMS team has worked hard to provide error free text and smart &amp; shortcut methods to solve problems</p>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span> About the exam :-</div>
                <p>NIMHANS PG Medical Entrance Exam has a single paper. This paper encompasses MCQ's of the objective nature. Questions of the graduate level shall be posed in this paper. The time period of this examination is based on the many departments. Aspirants who have a clear knowledge about the pattern and syllabus of the MBBS course will be able to perform admirably in this exam.</p>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>Course Highlights :-</div>
                <ul class="benefits">
                  <li><span></span>The study material includes full coverage of syllabus of PG medical entrance examination including high yielding text as well as cutting edge question banks.</li>
                  <li><span></span>The study material includes subject wise sequentially presented theory sections and practice sets which include solved questions with answers and explanations.</li>
                  <li><span></span>The study material is proved to be highly useful for all the examinations including AIPG, AIIMS, PGI Chandigarh, State PG exams, UPSC, DNB and MCI screening examinations. The previous feedback of the students is highly appreciable who have succeded in various examinations.</li>
                  <li><span></span>Date of admission test: Last week of June.</li>
                  <li><span></span>Study material is time to time updated and latest updation and current developments are sent time to time . Study material includes previous solved questions of 6 to 10 years.</li>
                </ul>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>HOW TO APPLY ?</div>
                <p>Download Application form &amp; enclose Demand Draft (note that cheques are not accepted) of concerned fee as mentioned in fee structure (including postal charges).</p>
                <p>Duly filled Application form alongwith demand draft to be made in favour of "Delhi Academy of Medical Sciences Pvt. Ltd.", payable at New Delhi  should be sent by registered post / courier to :</p>
                <p>Delhi Academy of Medical Sciences
                  Grovers Chamber
                  4B, 3rd Floor, Pusa Road, 
                  New Delhi-110005</p>
                <p>Now courses are avaliable through online payment. For buying any course please go to <a href="https://damsdelhi.com/online-registration-new4.php.">https://damsdelhi.com/online-registration-new4.php.</a></p>
              </aside>
            </article>
            <div class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'md-ms-right-accordion.php'; ?>
          <div class="national-quiz-add"> <a href="national.php" title="National Quiz"><img src="images/national-quiz.jpg" alt="National Quiz" /></a> </div>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
</body>
</html>