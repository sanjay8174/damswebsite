<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG </title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php error_reporting(0); ?>
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="career-banner">
<aside class="banner-left">
<h2>If you have the passion to <br>teach &amp; excel, </h2>
<h3>Sky is limit to grow in DAMS</h3>
</aside>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<section class="event-container">
<aside class="center-left">
<div class="inner-center">
<h4>Career at DAMS</h4>
<article class="showme-main">
<div class="career-content">
<p>For over 15 years Delhi Academy of Medical Sceiences (DAMS) has nutured and led hundreds of students to PG Medical Entrance Success.  At DAMS, we are working toward a future where everyone's potential can be fulfilled and we're growing fast through our impact.</p>
<p>&nbsp;</p>
<p>We are committed to being a great place to work. We are committed to making a big difference in the world. If you want substantial difference through your work, AND want to have fun in a challenging yet supportive environment, take a look at the positions we have currently available:</p>
<div class="main-position-set">
<h5>Positions Available</h5>
<ul>
<li>
<aside class="position-set-left">
<div class="position-box">
<div class="position-inner-box">
<div class="position-content-box">
<div class="position-heading"> <span></span> Position Heading</div>
<div class="position-content">
<p>Preferably well educated female for working with our medical 
students and doctors. Explain our courses to them and 
motivate them. Salary is no bar for deserving candidates.</p>
<div class="apply-now"><a href="javascript:void(0);" id="student-career1" title="Apply Now">Apply&nbsp;Now</a></div>
</div>
</div>
</div>
</div>
</aside>
<aside class="position-set-right">
<div class="position-box">
<div class="position-inner-box">
<div class="position-content-box">
<div class="position-heading"> <span></span> Marketing Manager</div>
<div class="position-content">
<p>Preferably well  educated female for working with our medical 
students and doctors. Explain our courses to them and 
motivate them. Salary is no bar for deserving candidates.</p>
<div class="apply-now"><a href="javascript:void(0);" id="student-career2" title="Apply Now">Apply&nbsp;Now</a></div>
</div>
</div>
</div>
</div>
</aside>
</li>
<li>
<aside class="position-set-left">
<div class="position-box">
<div class="position-inner-box">
<div class="position-content-box">
<div class="position-heading"> <span></span> Faculty to train students for PG Medical Entrance</div>
<div class="position-content">
<p>Preferably well  educated female for working with our medical 
students and doctors. Explain our courses to them and 
motivate them. Salary is no bar for deserving candidates.</p>
<div class="apply-now"><a href="javascript:void(0);" id="student-career3" title="Apply Now">Apply&nbsp;Now</a></div>
</div>
</div>
</div>
</div>
</aside>
<aside class="position-set-right">
<div class="position-box">
<div class="position-inner-box">
<div class="position-content-box">
<div class="position-heading"> <span></span>  Admin Executives</div>
<div class="position-content">
<p>Preferably well educated female for working with our medical 
students and doctors. Explain our courses to them and 
motivate them. Salary is no bar for deserving candidates.</p>
<div class="apply-now"><a href="javascript:void(0);" id="student-career4" title="Apply Now">Apply&nbsp;Now</a></div>
</div>
</div>
</div>
</div>
</aside>
</li>
</ul>
</div>
</div>
</article>
</div> 
</aside> 
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
