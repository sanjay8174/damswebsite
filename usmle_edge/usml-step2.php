<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, USMLE EDGE</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="usmle-edge-banner">
<aside class="banner-left">
<h2>USMLE EDGE</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include 'usmle-banner-btn.php'; ?>
</article>
</div>
</section> 
<!-- Banner End Here -->

<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">

<div class="page-heading">
<span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
<ul>
<li class="bg_none"><a href="usml-intro.php" title="USMLE EDGE">USMLE EDGE</a></li>
<li><a title="USMLE Edge Step 2" class="active-link">USMLE Edge Step 2</a></li>
</ul>
</div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading paddin-zero">
<h4>USMLE EDGE Step-2 <span class="book-ur-seat-btn book-hide"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
<article class="showme-main">
<div class="idams-content">
<div class="franchisee-box">
<span>Overview :</span>
<p>Step 2 assesses whether you can apply medical knowledge, skills, and understanding of clinical science essential for the provision of patient care under supervision and includes emphasis on health promotion and disease prevention. Step 2 ensures that due attention is devoted to principles of clinical sciences and basic patient-centered skills that provide the foundation for the safe and competent practice of medicine.</p>
<p>Step 2 CK is constructed according to an integrated content outline that organizes clinical science material along two dimensions: physician task and disease category.</p>
<p>Step 2 CK is a <span style="color:#990000;font-size:14px;">one-day</span> examination. The test items are divided into blocks, and test item formats may vary within The number of items in a block will be displayed at the beginning of each block. This number will vary among blocks, but will not exceed 45 items. The total number of items on the overall examination form will not exceed 355 items. Regardless of the number of items, 60 minutes are allotted for the completion of each block.</p>
<p>On the test day, examinees have a minimum of 45 minutes of break time and a 15- minute optional tutorial. The amount of time available for breaks may be increased by finishing a block of test items or the optional tutorial before the allotted time expires.</p>
</div>
                    <div class="franchisee-box"> <span>Step 1 Test Specifications-</span>
                  <p>Table 1: USMLE Step 1 Test Specifications<sup style="color:red">*</sup></p>
                       <table>
                           <tr>
                               <th><p>System</p></th>
                               <th> <p>Range</p></th>		
                              
                           </tr>
                           <tr>
                          <td><p>Immune System</p>
                                 <p>Blood & Lymphoreticular Systems</p>
                                 <p>Behavioral Health</p>
                                  <p>Nervous System & Special Senses</p>
                                   <p>Skin & Subcutaneous Tissue</p>
                                   <p>Musculoskeletal System</p>
                                   <p>Cardiovascular System</p>
                                   
                                   
                                   <p>Respiratory System</p>
                                   <p>Gastrointestinal System</p>
                                   <p>Renal & Urinary Systems</p>
                                   <p>Pregnancy, Childbirth, & the Puerperium</p>
                                   <p>Female Reproductive System & Breast</p>
                                   <p>Male Reproductive System</p>
                                   <p> Endocrine System</p>
                                    <p>Multisystem Processes & Disorders  </p>                    
                                   <p>Biostatistics & Epidemiology/Population Health</p>
                                   
                          </td>
                          <td><p>85% – 95%</p></td>		
                          
                          </tr>
                          <tr>
                              <td><p>Interpretation of the Medical Literature</p>
                              </td>
                              <td>
                                  <p>1%-5%</p>
                              </td>
                          </tr>
                          <tr>
                              <td><p>General Principles of Foundational Science</p></td>
                              <td><p>1% – 3%</p></td>
                          </tr>
                      </table>
                </div>
                       <div class="franchisee-box">
                   <ul>
                          <li>Medical knowledge/scientific concepts: Applying foundational science concepts</li>
                          <li>Patient care: Diagnosis</li>
                           <li>Patient care: Management</li>
                           <li>Communication and interpersonal skills</li>
                           <li> Professionalism, including legal and ethical issues</li>
                           <li>Systems-based practice, including patient safety</li>
                           <li>Practice-based learning, including biostatistics and epidemiology</li>
                          
                   </ul>
                   </div>
                    <div class="franchisee-box"> <span>Table 2: Step 2 CK Physician Task/Competency Specifications</span>
                       <table>
                           <tr>
                               <th><p>Competency</p></th>
                               <th> <p>Range</p></th>		
                              
                           </tr>
                           <tr>
                           <td><p>Medical Knowledge/Scientific Concepts</p></td>
                           <td><p>10% – 15%</p></td>		
                           
                          </tr>
                           <tr>
                          <td><p>Patient Care: Diagnosis</p>
                              <ul>
                                   <li>History/Physical Examination</li>
                                   <li>Laboratory/Diagnostic Studies</li>
                                   <li>Diagnosis</li>
                                   <li>Prognosis/Outcome</li>
                              </ul>
                                   
                          </td>
                          <td><p>40% – 50%</p></td>		
                          
                          </tr>
                          <tr>
                              <td><p>Patient Care: Management Health Maintenance/Disease Prevention</p>
                                  <ul>
                                     <li> Pharmacotherapy </li>
                                      <li>Clinical Interventions</li>
                                       <li>Mixed Management</li>
                                       <li>Surveillance for Disease Recurrence</li>
                                  </ul>
                              </td>
                              <td>
                                  <p>30% – 35%</p>
                              </td>
                          </tr>
                          <tr>
                              <td><p>Communication</p> 
                                      <p>Professionalism</p> 
                                       <p>Systems-based Practice/Patient Safety</p> 
                                        <p>Practice-based Learning</p></td>
                              <td><p>3% – 7%</p></td>
                          </tr>
                      </table>
                </div>
</div>
</article>
<div class="book-ur-seat-btn margn-zero"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
</div>
    <div class="pg-medical-main tab-hide">
  <div class="pg-heading"><span></span>USMLE Step 1</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <h5>Highlight</h5>
      <div class="coures-list-box-content">
        <p>USMLE Step 1 Free Practice Test Sign up for our diagnostic exam and find out your target areas for USMLE Step 1.</p>
        
        <p>This is an hour long test covers 50 questions</p>
        
        <p>absolutely FREE.</p>
        <p><br><strong style="color: red;">Sign up Now</strong></p>
        <p>&nbsp;</p>
        <p><a href="https://onlinetest.damsdelhi.com/" target="_blank">Sign up click on : onlinetest.damsdelhi.com</a></p>
      </div>
    </div>
  </div>
</div>
</aside>
<aside class="gallery-right">
<?php include 'dams-usmle-edge.php'; ?>
<!--for Enquiry -->
<?php include 'enquiryform.php'; ?>
<!--for Enquiry -->
</aside>
</section>
</div>
</div>
    <div class="wrapper">
    <aside class="content-left" style="margin-top:10px;width: 100%;">
        <div class="course-box">
         <p><b>Disclaimer</b>
         <br>
         <p style="text-align: justify;font-size: 12px">USMLE® is a joint program of the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners (NBME). The ECFMG® is a registered trademark of the Educational Commission for Foreign Medical Graduates. The Match® is a registered service mark of the National Resident Matching Program® (NRMP®). The NRMP is not affiliated with DAMS. Electronic Residency Application Service (ERAS®) is a program of the association of American Medical Colleges and is not affiliated with DAMS. Test names and other trademarks are the property of the respective trademark holders. None of the trademark holders are affiliated with DAMS or this website.<br>
        </div>
      </aside>
        </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>