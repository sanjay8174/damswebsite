<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];

?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>USMLE EDGE STEP 2 CS</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg no_bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php';
$pathCMS = constant::$pathCMS;
?>
<!-- Banner Start Here -->
<section class="inner-banner usmlecs">
  <div class="wrapper">
    <article class="usmle-edge-banner step2csusmle">
      <aside class="banner-left banner_over">
          <h2 style="color: #3593c9;font-family: "pt_sansregular";font-size: 30px; ">USMLE STEP 2 CS COURSE</h2>
          <h5><span style="color: rgb(35, 166, 29);font-size: 22px;">3 Days Workshop </br> US Trained Physician American Standardized Patients </span></h5>         
      </aside>
      <?php // include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<!--<div style="overflow:hidden;">-->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h5>USMLE EDGE STEP 2 CS Course <span class="book-ur-seat-btn"><a title="Book Your Seat" href="http://registration.damsdelhi.com" target="_blank"> <span>&nbsp;</span> Book Your Seat</a></span></h5>
        <p style="font-size:22px;padding:5px 0px;">Course Details Format</p>   
        <p style="font-size:17px;padding:10px 0px 7px 0px;">Step 2 CS: Clinical Skills</p>   
        <p>STEP 2 Clinical Skills CS is an exam that uses standardized patients to test examinees on their ability to gather information from patients, perform physical examinations, and communicate their findings to patients and colleagues.</br></br>Administered at five regional test centers in the US Atlanta, Chicago, Houston, Los Angeles, and Philadelphia</p>
      </div>
   <div class="pg-medical-main tab-hide" style="display:block;">
  <div class="pg-heading"><span></span>Objective Structure</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
         <h5 class="course_heading h2toggle2" onclick="sliddes2('21');"><span class="plus-ico" id="s21"></span>Step 2 CS: The Exam</h5>
      <div class="coures-list-box-content" id="di21" style="display: none;">          
        <ul class="course-new-list">
             <li style="margin:0px;"> <h6>Your Step 2 CS administration will include:</h6></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">12 patient encounters </p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">The examination session lasts approximately 8 hours </p></li>        
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">You will have 50 minutes of break time: </p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">10-minute break after the 3rd patient encounter  </p></li>        
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">30-minute break (which includes a light lunch) after the 6th patient encounter </p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">10-minute break after the 9th patient encounter.</p></li>        
        </ul>    
      </div>
    </div>
    <div class="coures-list-box">
          <h5 class="course_heading h2toggle2 "  onclick="sliddes2('22');"><span class="plus-ico" id="s22"></span>Objective Structured Clinical Examination (OSCE) </h5>
      <div class="coures-list-box-content" id="di22" style="display: none;"> 
        

        <ul class="course-new-list">
              <li style="margin:0px;"> <h6>Step 2 Clinical Skills has to be taken in USA</h6></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">Cost - $1505  </p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">8 hour exam, with 3 breaks (10min +30 min + 10 min)</p></li>        
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">12 standardized patients (actors)</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">15 minutes for history + examination</p></li>        
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">10 minutes to write a patient note describing the findings, initial differential diagnosis list and a list of initial tests</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">No scores, just pass or fail</p></li>        
        </ul>    
      </div>
    </div>
  </div></div>  
       
         <!--h-->

        
         <!--end-->
        

<div class="pg-medical-main tab-hide" style="display:block;">
  <div class="pg-heading"><span></span>COMPONENTS OF STEP 2 CS EXAM</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
        <h5 class="course_heading h2toggle" onclick="sliddes('1');"><span class="plus-ico" id="s1"></span>Integrated Clinical Encounter (ICE)</h5>
      <div class="coures-list-box-content" id="di1" style="display: none;"> 
          
          <p class="content_heads">This component includes:</p>
          
        <ul class="course-new-list">
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">The ability of the examinees to collect pertinent clinical information from the SP</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">TYPE an appropriate patient note with differential after the clinical encounter. </p></li>
        
        </ul>
          <p style="font-size:14px;">The former part is graded by the simulated patient, and the latter by a practicing physician.</p>
      </div>
        </div>
       <div class="coures-list-box">
            <h5 class="course_heading h2toggle" onclick="sliddes('2');"><span class="plus-ico" id="s2"></span>Communication and Interpersonal Skills (CIS)</h5>
      <div class="coures-list-box-content " id="di2" style="display: none;"> 
         
          <p class="content_heads">This component includes evaluating the examinees :</p>
        <ul class="course-new-list">
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">Question asking skills,</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">Information sharing skills and </p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">Professionalism/rapport/empathy</p></li>
         
        </ul>
      </div>
            </div>
        <div class="coures-list-box">
       <h5 class="course_heading h2toggle" onclick="sliddes('3');"><span class="plus-ico" id="s3"></span>Spoken English Proficiency (SEP)</h5>
      <div class="coures-list-box-content " id="di3" style="display: none;">    
          <p class="content_heads">This component includes clarity of spoken English communication in the context of</p>
        <ul class="course-new-list">
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">pronunciation,</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">word choice, and </p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">minimizing the need to repeat questions or statements.</p></li>
          
        </ul>
      </div>
       </div>
        <div class="coures-list-box">
        <h5 class="course_heading h2toggle" onclick="sliddes('4');"><span class="plus-ico" id="s4"></span>SCORING</h5>
      <div class="coures-list-box-content " id="di4" style="display: none;">          
        <ul class="course-new-list">
          <li style="    padding: 10px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">The test is graded on a pass/fail basis, without any numerical score associated with it (as opposed to the other parts of the USMLE series).</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">In order to pass, one must achieve a grade of "pass" in each of the three sub-components of the exam.</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">A score report is provided in the event of failure.</p></li>
          
        </ul>
      </div>
        </div>
    </div>
  </div>
         
   <div class="pg-medical-main tab-hide" style="display:block;">
  <div class="pg-heading"><span></span>DAMS is offering a unique 3-day course</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">      
        <ul class="course-new-list">
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">Learn from experienced Attending physicians from Mayo Clinic (former IMGs)  and Faculty from Carnegie Mellon</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">Work with American Standardized Patients: to help you get used to the accent, behavior and mannerism of American patients</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">Assess your readiness with mock tests and exercises</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">Get personalized attention and 1:1 feedback on patient encounters and mock tests</p></li>
          <li style="    padding: 0px 0px 0px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;">Experience fully equipped encounter rooms just as you would in the real CS exam </p></li>
        </ul>
      </div>
        </div>
      <div class="coures-list-box">
        
          <h5 class="course_heading h2toggle1 abc"onclick="sliddes1('11');"><span class="plus-ico" id="s11"></span>Day 1: Step 2 CS Orientation</h5>
          <div class="coures-list-box-content " id="di11" style="display: none;"> 
          <p>&nbsp;</p>
        <ul class="course-new-list">
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Roadmap to US Residency</strong></p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Insights on navigating the process from Step 1 to Match day</strong></p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Step 2 CS in depth discussion</strong></p>
              <ul >
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Understanding the exam and its components</p></li>
              </ul>
          </li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Cultural competence</strong></p>
              <ul >
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Social interactions in the US and how they relate to CS </p></li>
              </ul>
          </li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Physician-Patient interaction in the US</strong></p>
              <ul >
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">History taking and physical examination template</p></li>
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Behavioral and sexual history taking</p></li>
              </ul>
          </li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Patient counseling</strong></p>
              <ul >
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Empathy and establishing trust</p></li>
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Patient autonomy and shared decision making</p></li>
              </ul>
          </li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>CS Patient note</strong> </p>
              <ul >
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Tips and tricks</p></li>
              </ul>
          </li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Mock Exercises</strong></p>
              <ul >
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Social interaction</p></li>
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">History taking, counseling</p></li>
              </ul>
          </li>
          
        </ul>
      </div>
     </div>
      <div class="coures-list-box">
        
          <h5 class="course_heading h2toggle1 abc" onclick="sliddes1('12');"><span class="plus-ico" id="s12"></span>Day 2: Instructor led case discussions  hands-on practice</h5>  
          <div class="coures-list-box-content " id="di12" style="display: none;"> 
          <p>&nbsp;</p>
        <ul class="course-new-list">
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Discussion of common cases</strong></p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Mock exercises with instructors</strong></p>
            <ul >
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Differential diagnoses and diagnostic evaluation</p></li>
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Appropriate use of medical devices in the exam room  </p></li>
                  <li style="    padding: 0px 0px 0px 0px"><p style="padding-left: 10px;font-size: 14px;">Telephone CS patient encounter</p></li>
              </ul>
          </li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Common IMG mistakes in CS and how to avoid them</strong></p> </li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Hands-on: Patient encounter with American standardized patient</strong></p></li>
     </ul>
      </div>
          </div>
      <div class="coures-list-box">
         
          <h5 class="course_heading h2toggle1 abc" onclick="sliddes1('13');"><span class="plus-ico" id="s13"></span>Day 3: Mock tests/hands-on practice and feedback</h5>    
           <div class="coures-list-box-content " id="di13" style="display: none;"> 
          <p>&nbsp;</p>
        <ul class="course-new-list">
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Recap of Day 2 </strong></p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Hands-on patient encounter</strong></p></li>          
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Personalized feedback on encounter and patient note</strong></p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Role play exercises on social interaction, counseling</strong></p></li>          
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Best practices and mistakes to avoid</strong></p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong>Course summary</strong></p></li>          
        </ul>
      </div>
          </div>
    </div>
  </div>
 <!-- <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Features</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">      
        <ul class="course-new-list">
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong style="margin-right: 20px;">Live, Expert Taught Sessions</strong>Live course in the INDIA at your Doorstep , spanning 3 days (2 days of lectures and practice, 1 day of practice exam).</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong style="margin-right: 20px;">Live Practice Exam</strong>At the end of the course you will experience a Mock 6 patient encounter exam, so can find out how you would perform on the real exam, providing you with feedback which you can build on, in time for the real Exam.</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong style="margin-right: 20px;">Physician-Patient Communication</strong>Receive 10 hours of small group coaching or One on one training to help you to learn appropriate and strong communication skills, a key area of assessment in the exam.</p></li>
          <li style="    padding: 0px 0px 10px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong style="margin-right: 20px;">10 hours of Live Online Lectures</strong>Access over 15 hours of interactive lectures, accessible from anywhere.</li>
          <li style="    padding: 0px 0px 0px 0px"><span class="sub-arrow"></span><p style="padding-left: 10px;font-size: 14px;"><strong style="margin-right: 20px;">Lecture Notes</strong>You will receive 30+ fully updated lecture notes, with the key information and tips to take away from the course, for review after the course has finished.</p></li>
        </ul>
      </div>
    </div>
  </div></div>-->

<!--<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Facilitator</div>
  <div class="course-new-section">
   <div class="coures-list-box">
      <div class="coures-list-box-content">
<div class="mrcp_boxess" style="margin-top:0;border-bottom: 0px dotted #ccc;">
        <div class="mrcp_dr_img"><img src='images/Image_3.png'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Nitin Kararia</h1>                      
            <p class="mrcp_dr_sbtl" style="font-size:14px;">DR NITIN KARARIA graduated from Government Dental College, Jaipur in 2005. He secured 3rd rank in Rajasthan state Pre PG entrance examination and completed post graduation in Conservative Dentistry and Endodontics from government dental college, Jaipur in 2009. He is presently working as Reader at Rajasthan Dental College and Hospital, Jaipur and keenly involved in academic and clinical activities. He has many scientific publications to his credit out of which three are in Pubmed indexed journals. He is also sharing his experience in coaching and training graduates for the post graduate entrance examinations.</p></br>
            <p class="mrcp_dr_sbtl" style="font-size:14px;">The aim of this course is to hone the clinical skills and knowledge of graduates in the field of Conservative Dentistry and Endodontics and to familiarize them with the latest techniques and armamentaria and help them emerge a better clinician.</p>
		   
        </div>
</div></div>
    </div>
    </div>
  </div>-->
<!--        <div class="pg-medical-main" style="display:block;margin:25px 0 0 0;border: 0px solid #F0AC49;line-height: 0px;">
<img src="images/course_1.jpg" style="margin:0px; border:0px solid #ccc;" width="100%">
</div>-->


    </aside>    
    <aside class="content-right">
<!--        <div class="content-date-venue res_css" style="margin-top:0px;">
            <h1>DATES</h1>
            <p style="font-weight:bold;font-size:18px;">Starting From 23-May-2016</p>
            <p style="border: 1px dotted #c4c4c4;margin:10px 0px;width:95%;"></p>
            <h1>VENUE</h1>
            <p>4 B, Grover Chamber,Pusa Road</p>
            <p>Near Karol Bagh Metro Station</p>
            <p>New Delhi, Delhi - 110005</p>
            <p>Ph. No. : 011 - 40094009</p>
            <p>(Enquiry Timing:- 10am To 8pm)</p>
        </div>-->
          <?php include 'enquiryform.php'; ?>
    </aside>
  </div>
    <script>
function sliddes2(val)
   {
   var sp = $('.h2toggle2 > span').size();
   for(var d=21;d<=sp+20;d++)
   {
       if(val!=d){
                        $('#s'+d).removeClass('minus-ico');
                        $('#s'+d).addClass('plus-ico');
                        $('#di'+d).slideUp(400);
	   }
   }

   $('#di'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
                            $('#s'+val).removeClass('plus-ico');
                            $('#s'+val).addClass('minus-ico');
                    }else
	   {
                            $('#s'+val).removeClass('minus-ico');
                           $('#s'+val).addClass('plus-ico');
                   }
    });
}
  function sliddes(val)
   {
   var sp = $('.h2toggle > span').size();
   for(var d=1;d<=sp;d++)
   {
       if(val!=d){
                        $('#s'+d).removeClass('minus-ico');
                        $('#s'+d).addClass('plus-ico');
                        $('#di'+d).slideUp(400);
	   }
   }

   $('#di'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
                            $('#s'+val).removeClass('plus-ico');
                            $('#s'+val).addClass('minus-ico');
                    }else
	   {
                            $('#s'+val).removeClass('minus-ico');
                           $('#s'+val).addClass('plus-ico');
                   }
    });
}

 function sliddes1(val)
   {
   var sp = $('.h2toggle1 > span').size();
   for(var d=11;d<=sp+10;d++)
   {
       if(val!=d){
                        $('#s'+d).removeClass('minus-ico');
                        $('#s'+d).addClass('plus-ico');
                        $('#di'+d).slideUp(400);
	   }
   }

   $('#di'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
                            $('#s'+val).removeClass('plus-ico');
                            $('#s'+val).addClass('minus-ico');
                    }else
	   {
                            $('#s'+val).removeClass('minus-ico');
                           $('#s'+val).addClass('plus-ico');
                   }
    });
}
 
</script>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
