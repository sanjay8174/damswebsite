<?php ob_start(); ?>
<!DOCTYPE html>
<?php

header("location:https://usmle.damsdelhi.com/index.php?c=4&n=");

exit();
$course_id = '4';
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>USMLE EDGE Coaching Institute, USMLE EDGE</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h2>USMLE EDGE</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
     <div  class="bannerlfthead"style=""> 
	   <p>Live Usmle courses first time in India, being launched on <span style="font-size:22px;  font-family: sans-serif;  font-weight: 600;">8<sup>th</sup> August.</span> </p>
	   <p> <span style="font-weight: 600;">It is an 16 weeks course only on weekends.</span></p> 
	   <p>Handouts and question bank will be provide along with complete counselling for Usmle .</p> 
       <p>Only counselling and Question bank packages also available . Please call for more details.</p>
	   <span class="book-ur-seat-btn1"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> Book Your Seat</a></span>
	</div>
      </aside>
      <?php include'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>USMLE Full Package<!--<span class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span>--></h3>
        
        <div class="franchisee-box"> 
          <p>Requirements to conquer the USMLE are hard work, courage and determination.<br>
          Our approach to exam preparation is dynamic, holistic and proven.</p>
          <p>We propose our own unique course which is integrated Foundation PG course with USMLE EDGE. How many of us
                sure in the prefinal-final year stage about USMLE? How many times have you had a need for a course which gives us an 
                edge to tackle both the exams? Yes DAMS now offers the only course which offers online simulated exams for both steps 
                and counseling sessions for USMLE integrated with its very popular PG foundation course.</p>         
          <p>Another course, we are now offering is the USMLE Simulated Test Series with counseling sessions for people who want to appear for only USMLE.</p>
          <p>By August 2015 we will be launching our own live lecture/class room program for the USMLE as well.</p>
          
           <p>&nbsp;</p>
           <div class="how-to-apply-heading"><span></span>DAMS Program for USMLE will include:-</div> 
           <ul class="usmledge">
            <li>Mentorship and Assistance for USMLE applications.</li>
            <li>Live lectures by experienced Faculty for USMLE Prep for Step 1 and 2 CK including extensive online Q bank.</li>
            <li>Help with Observership and Clerkship applications.</li>
            <li>Residency application help.</li>
            <li>Visa Counseling, Travel/Relocation Information.</li>
           </ul> 
          <p>&nbsp;</p>
          <div class="how-to-apply-heading"><span></span>Counselling:-</div> 
          <p><strong>The work with your expert counselor will typically include:</strong></p>
          <p>A carefully crafted daily USMLE study schedule tailored to your progress, strengths and weaknesses.<br>
         Guidance regarding which resources to use, comprehensive review of high-yield material.<br>
          Assessment of your practice exam performance like NBME and U world.<br>
         Gauging Preparedness quotient and giving expert advice regarding the ideal time to take your assessments and ultimately your USMLE.<br>
          Development and refinement of USMLE test-taking strategies to build your confidence and endurance.<br>
          Complete application work.<br>
          Eras application help.<br>
          Advice on way forward.<br>
         Accountability, guidance and moral support.</p>
                    
          <p>&nbsp;</p>
          <p><strong>We are soon going to launch our Live lecture program for USMLE Step 1 which will include :</strong></p>
          <p>16 weeks of prep with 3-4 classes a week .</p> 
          <p>Or</p>
           <p>Weekend prep for students and working professionals where we will have only Sunday classes.</p>
          
      </div>
      <?php include 'usmle-middle-accordion.php'; ?>
    </aside>
    <aside class="content-right">
      <?php include 'dams-usmle-edge.php'; ?>
      <?php
    include 'openconnection.php';
    $count = 0;
    $i = 0;
    $sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
    while ($row = mysql_fetch_array($sql)) {
        $newsDetail[$count] = urldecode($row['HEADING']);
        $count++;
    }
    ?>
      <div class="news-update-box">
        <div class="n-heading"><span></span> News &amp; Updates</div>
        <div class="news-content-box">
          <div style="width:100%; float:left; height:228px; overflow:hidden;">
            <?php
                        $j = 0;
                        $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) {
                        ?>
            <ul id="ul<?php echo $i; ?>" <?php if ($i == '0') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php if ($newsDetail[$j] != '') {
 ?>
            <li class="orange"> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            <?php if ($newsDetail[$j] != '') { ?>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            </ul>
            <?php
                        }
?>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="usmle-news.php?c='<?php echo $course_id ?>'" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
            <div class="slider-mini-dot">
              <ul>
                <?php $i = 0;
                                    for ($i = 0; $i < ceil($count / 3); $i++) { ?>
                <li id="u<?php echo $i; ?>" <?php if ($i == '0') {
 ?> class="current" <?php } ?> onClick="news('<?php echo $i; ?>',<?php echo ceil($count / 3); ?>);"></li>
                <?php } ?>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
      <div class="news-update-box">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block">
            <iframe width="100%" height="236" src="//www.youtube.com/embed/z_xgJNXaWuQ?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd1" class="display_none">
            <iframe width="100%" height="236" src="//www.youtube.com/embed/VRJ89h2DkS0?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd2" class="display_none">
            <iframe width="100%" height="236" src="//www.youtube.com/embed/VRJ89h2DkS0?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd3" class="display_none">
            <iframe width="100%" height="236" src="//www.youtube.com/embed/z_xgJNXaWuQ?wmode=transparent" class="border_none"></iframe>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="#" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
    </aside>
      <aside class="content-left" style="margin-top:10px;width: 100%">
        <div class="course-box">
         <p><b>Disclaimer</b>
         <br>
         <p style="text-align: justify;font-size: 12px">USMLE® is a joint program of the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners (NBME). The ECFMG® is a registered trademark of the Educational Commission for Foreign Medical Graduates. The Match® is a registered service mark of the National Resident Matching Program® (NRMP®). The NRMP is not affiliated with DAMS. Electronic Residency Application Service (ERAS®) is a program of the association of American Medical Colleges and is not affiliated with DAMS. Test names and other trademarks are the property of the respective trademark holders. None of the trademark holders are affiliated with DAMS or this website.<br>
        </div>
      </aside>
  </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<!--<script type="text/javascript">
 var m=0;
 var l=0;
 var n=0;
 var videointerval ='';
 var newsinterval = '';
 $(document).ready(function(){
   var count1=$("#recent li").length;
   $("#prev").click(function(){
	 if(m>0)
	 {
	   var j=m+3;
       $("#li"+j).hide('fast','linear');
	   m--;
	   $("#li"+m).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   $(".slide-arrow > a.left").css('background','url(images/right.png)');
	   if(m==0)
	   {
		 $(".slide-arrow > a.right").css('background','url(images/left.png)');
	   }
	 }
   });
   $("#next").click(function(){
	 if(m<count1-4)
	 {
       $("#li"+m).hide('fast','linear');
	   var j=m+4;
	   $("#li"+j).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   m++;
	   if(m==(count1-4))
	   {
		 $(".slide-arrow > a.left").css('background','url(images/right-inactive.png)');
	   }
     }
   });

   window.onload = function()
   {
	    var count2=$(".videos-content-box > img").length;
	    var count3=$(".news-content-box > ul").length;
        function setvideo()
		{
		   l=l%count2;
		   $("#vd"+l).fadeOut('slow','linear');
		   $("#v"+l).removeClass("current");
	       var j=(l+1)%count2;
		   $("#vd"+j).fadeIn('slow','linear');
		   $("#v"+j).addClass("current");
	       l++;
        }
     videointerval = setInterval(setvideo, 5000);
   }
 });
 function news(val)
 {
   if(val=='0')
   {
	  $("#ul1").hide();
	  $("#u1").removeClass("current");
	  $("#ul2").hide();
	  $("#u2").removeClass("current");
	  $("#ul0").slideDown(1000,'linear');
	  $("#u0").addClass("current");
   }
   if(val=='1')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current");
	 $("#ul2").hide();
	 $("#u2").removeClass("current");
	 $("#ul1").slideDown(1000,'linear');
	 $("#u1").addClass("current");
   }
   if(val=='2')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current");
	 $("#ul1").hide();
	 $("#u1").removeClass("current");
	 $("#ul2").slideDown(1000,'linear');
	 $("#u2").addClass("current");
   }
 }

 function video(val)
 {
   setTimeout('videointerval', 5000);
   if(val=='0')
   {
      $("#vd1").hide();
	  $("#vd1").removeClass("display_block");
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#vd2").removeClass("display_block");
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#vd3").removeClass("display_block");
	  $("#v3").removeClass("current");
	  $("#vd0").addClass("display_block");
	  $("#vd0").fadeIn('fast','linear');
	  $("#vd0").removeClass("display_none");
	  $("#v0").addClass("current");
   }
   if(val=='1')
   {
      $("#vd0").hide();
	  $("#vd0").removeClass("display_block");
	  $("#v0").removeClass("current");
	  $("#vd2").hide();
	  $("#vd2").removeClass("display_block");
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#vd3").removeClass("display_block");
	  $("#v3").removeClass("current");
	  $("#vd1").addClass("display_block");
	  $("#vd1").fadeIn('fast','linear');
	  $("#vd1").removeClass("display_none");
	  $("#v1").addClass("current");
   }
   if(val=='2')
   {
      $("#vd0").hide();
	  $("#vd0").removeClass("display_block");
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#vd1").removeClass("display_block");
	  $("#v1").removeClass("current");
	  $("#vd3").hide();
	  $("#vd3").removeClass("display_block");
	  $("#v3").removeClass("current");
	  $("#vd2").addClass("display_block");
	  $("#vd2").fadeIn('fast','linear');
	  $("#vd2").removeClass("display_none");
	  $("#v2").addClass("current");
   }
   if(val=='3')
   {
      $("#vd0").hide();
	  $("#vd0").removeClass("display_block");
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#vd1").removeClass("display_block");
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#vd2").removeClass("display_block");
	  $("#v2").removeClass("current");
	  $("#vd3").addClass("display_block");
	  $("#vd3").fadeIn('fast','linear');
	  $("#vd3").removeClass("display_none");
	  $("#v3").addClass("current");
   }
   l=val;
   videointerval = setInterval(setnews, 5000);
 }

 function sliddes(val)
{
   var sp=$('.h2toggle > span').size();
   for(var d=1;d<=sp;d++)
   {
       if(val!=d)
	   {
    	 $('#s'+d).removeClass('minus-ico');
         $('#s'+d).addClass('plus-ico');
		 $('#di'+d).slideUp(400);
	   }
   }

   $('#di'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#s'+val).removeClass('plus-ico');
            $('#s'+val).addClass('minus-ico');
       }
	   else
	   {
			$('#s'+val).removeClass('minus-ico');
            $('#s'+val).addClass('plus-ico');
       }
   });
}
</script>-->
</body>
</html>
