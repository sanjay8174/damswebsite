<?php  $pageName= substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); ?>

<div class="pg-medical-main tab-hide">
  <div class="pg-heading"><span></span>Usmle Edge Courses</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <h5> Usmle Edge Courses</h5>
      <div class="coures-list-box-content">
        <ul class="course-new-list">
          <li><a href="usml-step1.php" title="USMLE STEP-1 (TS)"><span class="sub-arrow"></span>USMLE Step 1</a></li>
          <li><a href="usml-step2.php" title="USMLE STEP-2 (TS)"><span class="sub-arrow"></span>USMLE Step2 CK</a></li>
          <!--<li><a href="usmle-egde-combo.php" title="USMLE Combo"><span class="sub-arrow"></span>USMLE Combo</a></li>-->
          <li><a href="usmle-edge-full-package.php" title="USMLE Full Package"><span class="sub-arrow"></span>USMLE Full Package</a></li>
        </ul>
        <p><br><strong>Each of the three Steps of the USMLE complements the others; No Step can stand alone in the assessment of readiness for medical licensure.</strong></p>
      </div>
    </div>
  </div>
</div>
<div class="pg-medical-main tab-hide">
  <div class="pg-heading"><span></span>USMLE Step 1</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <h5>Highlight</h5>
      <div class="coures-list-box-content">
        <p>USMLE Step 1 Free Practice Test Sign up for our diagnostic exam and find out your target areas for USMLE Step 1.</p>
        
        <p>This is an hour long test covers 50 questions</p>
        
        <p>absolutely FREE.</p>
        <p><br><strong style="color: red;">Sign up Now</strong></p>
        <p>&nbsp;</p>
        <p><a href="https://onlinetest.damsdelhi.com/" target="_blank">Sign up click on : onlinetest.damsdelhi.com</a></p>
      </div>
    </div>
  </div>
</div>
