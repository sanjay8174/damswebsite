<html>
    <head>
        <style>
            .setouter{border: 3px solid #2f3197; width:99%;display: inline-block;float:left;}
            .sethead{ border: 2px solid #de801f;background:#e7e8ea;margin:2%;width:96%;}
            .headingset{ color: #069f49; font-size: 33px;
    font-weight: bold;
    margin: 9px 0;
    text-align: center;}
            .setcent{margin: 20px 5px 0;font-size: 12px}
            .cenbor{ border: 2px solid #282828;
    border-radius: 2px;
    float: left;
    margin: 8px 27px;
    text-align: center;
    width: 16.7%;padding: 4px 0px}
            .centrename{color: #c9573c;font-weight: bold;font-size: 14px}
            .centrecall{color: #039851;font-weight: bold;font-size: 11px}
            .linearbor{text-align: center;display: inline-block;width: 100%}
            .linearborder{height: 5px;display: inline-block;position: relative}
            .linearborder:after{
                background:-moz-linear-gradient(left,#ee4d25 70%,#e7e8ea 100%);
                background:-webkit-gradient(left top,left bottom,color-stop(0,#ee4d25),color-stop(100%,#e7e8ea));
                background:-webkit-linear-gradient(top,#ee4d25 70%,#e7e8ea 100%);
                background:-o-linear-gradient(top,#ee4d25 70%,#e7e8ea 100%);
                content: "";
                float: left;
                position: absolute;
                width: 250px;height: 5px;
            }
            .linearborder:before{
                background:-moz-linear-gradient(right,#ee4d25 70%,#e7e8ea 100%);
                background:-webkit-gradient(right top,right bottom,color-stop(0,#ee4d25) 70%,color-stop(100%,#e7e8ea));
                background:-webkit-linear-gradient(right,#ee4d25 70%,#e7e8ea 100%);
                background:-o-linear-gradient(right,#ee4d25 70%,#e7e8ea 100%);
                content: "";
                position: absolute;float: right;
                width: 250px;height: 5px;right: 0px
            }
            .outfoote{width: 100%;display: inline-block;text-align: center}
            .bordwid{   border: 2px solid #2f3197;
    border-radius: 6px;
    float: left;
    padding: 2px 6px;
    width: auto;margin: 7px}
            .bordin{ color: #bc2c1f;font-weight: bold}
        </style>
    </head>
    <body style="font-family:sans-serif">
	
        <div class="setouter">
            <div class="sethead">
            <p class="headingset">USMLE SATELLITE CENTRE</p>
                <div class="linearbor"><span class="linearborder"></span></div>
            </div>
            <div class="setcent">
            <div class="centerset">
              <div class="cenbor"> <span class="centrename">Ahmedabad</span><br>
                <span class="centrecall">09825066428</span>
                </div>
              <div class="cenbor"> <span class="centrename">Cochin</span><br>
                <span class="centrecall">09496979000</span>
                </div>
              <div class="cenbor"> <span class="centrename">Jabalpur</span><br>
                <span class="centrecall">09425155125</span>
                </div>
              <div class="cenbor"> <span class="centrename">Lucknow</span><br>
                <span class="centrecall">09335741999</span>
                </div>
              <div class="cenbor"> <span class="centrename">Shimla</span><br>
                <span class="centrecall">08905751109</span>
                </div>
              <div class="cenbor"> <span class="centrename">Aligarh</span><br>
                <span class="centrecall">08173001383</span>
                </div>
              <div class="cenbor"> <span class="centrename">Calicut</span><br>
                <span class="centrecall">09446939494</span>
                </div>
              <div class="cenbor"> <span class="centrename">Hyderabad</span><br>
                <span class="centrecall">09493316965</span>
                </div>
              <div class="cenbor"> <span class="centrename">Ludhiana</span><br>
                <span class="centrecall">07837536346</span>
                </div>
              <div class="cenbor"> <span class="centrename">Varanasi</span><br>
                <span class="centrecall">08173001913</span>
                </div>
              <div class="cenbor"> <span class="centrename">Amritsar</span><br>
                <span class="centrecall">07837536346</span>
                </div>
              <div class="cenbor"> <span class="centrename">Chennai</span><br>
                <span class="centrecall">09500123864</span>
                </div>
              <div class="cenbor"> <span class="centrename">Imphal</span><br>
                <span class="centrecall">07308161777</span>
                </div>
              <div class="cenbor"> <span class="centrename">Mangalore</span><br>
                <span class="centrecall">09886067181</span>
                </div>
              <div class="cenbor"> <span class="centrename">Vijyawada</span><br>
                <span class="centrecall">09493316965</span>
                </div>
              <div class="cenbor"> <span class="centrename">Bengaluru</span><br>
                <span class="centrecall">08861251895</span>
                </div>
              <div class="cenbor"> <span class="centrename">Chandigarh</span><br>
                <span class="centrecall">09781687300</span>
                </div>
              <div class="cenbor"> <span class="centrename">Indore</span><br>
                <span class="centrecall">09229298699</span>
                </div>
              <div class="cenbor"> <span class="centrename">Manipal</span><br>
                <span class="centrecall">098886067181</span>
                </div>
              <div class="cenbor"> <span class="centrename">Trivamdrum</span><br>
                <span class="centrecall">09496579000</span>
                </div>
              <div class="cenbor"> <span class="centrename">Bilaspur</span><br>
                <span class="centrecall">09993669994</span>
                </div>
              <div class="cenbor"> <span class="centrename">Dehradun</span><br>
                <span class="centrecall">09536776766</span>
                </div>
              <div class="cenbor"> <span class="centrename">Jam Nagar</span><br>
                    <span class="centrecall">09825066428</span>
                </div>
              <div class="cenbor"> <span class="centrename">Mumbai</span><br>
                <span class="centrecall">09322207254</span>
                </div>
              <div class="cenbor"> <span class="centrename">Belgaum</span><br>
                <span class="centrecall">09875840741</span>
                </div>
              <div class="cenbor"> <span class="centrename">Guwahati</span><br>
                <span class="centrecall">08486003120</span>
                </div>
              <div class="cenbor"> <span class="centrename">Kolkata</span><br>
                <span class="centrecall">09051233890</span>
                </div>
              <div class="cenbor"> <span class="centrename">Pune</span><br>
                <span class="centrecall">09890447499</span>
                </div>
                <div class="cenbor"> <span class="centrename">Thrissur</span><br>
                <span class="centrecall">08594004666</span>
                </div>
                </div>
            
            
            
            </div>
            <div class="outfoote">
            <div style="width:auto;margin:0 auto; display:inline-block">
                <div class="bordwid">
                <span class="bordin"><span style="color:#3098bc">Website:</span> www.damsdelhi.com</span>
                </div>
                   <div class="bordwid">
                <span class="bordin"><span style="color:#3098bc">E-Mail:</span> usmle@damsdelhi.com</span>
                </div></div>
            </div>
        
        </div>
    
    </body>
</html>