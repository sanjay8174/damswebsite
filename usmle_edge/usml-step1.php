<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h2>USMLE EDGE</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="usml-intro.php" title="USMLE EDGE">USMLE EDGE</a></li>
          <li><a title="USMLE Edge Step 1" class="active-link">USMLE Edge Step 1 </a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>USMLE EDGE Step-1 Overview<span class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span> </h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="franchisee-box"> <span>Step-I :-</span>
                  <p>The USMLE Step 1 is the first of the 3 examinations that you must pass in order to become the licensed physician in the United States. It is a joint endeavor of FSMB and NBME. It is a common exam taken by the US medical students and International graduates. USMLE Step 1 assesses whether you understand and can apply important concepts of the sciences basic to the practice of medicine, with special emphasis on principles and mechanisms underlying health, disease, and modes of therapy. Step 1 is constructed according to an integrated content outline that organizes basic science material along two dimensions: 
                         <span style="color:#990000;font-size: 14px;">system and process</span>.
                         <span style="font-size: 15px;"> USMLE Step 1 can be taken at the end of second year of medical school</span>. </p>
                  <p>Step 1 is a <span style="color:#990000;font-size: 14px;">one-day </span>examination. It is a computer based test. The testing day includes 322 multiple-choice items divided into 7 blocks of 46 items; 60 minutes are allotted for completion of each block of test items. On the test day, examinees have a minimum of 45 minutes of break time and a 15- minute optional tutorial. The amount of time available for breaks may be increased by finishing a block of test items or the optional tutorial before the allotted time expires. 
                         <span style="font-size: 15px;">You will be able to skip back and forth among questions, but ONLY within a block of questions. Once the hour is over, you will be unable to return to those 50 items. The test items aren't grouped by clinical subject but are presented in a random, interdisciplinary sequence. All USMLE questions are in one best-answer format. There are no matching questions on the computerized USMLE Step 1.</span></p>
                </div>
                   <div class="franchisee-box"><p>Sections focusing on individual organ systems are subdivided according to normal and abnormal processes, including principles of therapy. Each Step 1 examination covers content related to the following traditionally defined disciplines:<p>
                   <ul>
                          <li>Anatomy</li>
                          <li>Behavioral sciences</li>
                           <li>Biochemistry</li>
                           <li>Biostatistics and epidemiology</li>
                           <li>Microbiology</li>
                           <li>Pathology</li>
                           <li>Pharmacology</li>
                           <li>Physiology</li>
                   </ul>
                       <p>The Step 1 examination also covers content related to the following interdisciplinary areas:</p>
                   <ul>
                          <li>Aging</li>
                          <li>Immunology</li>
                           <li>Nutrition</li>
                           <li>Molecular and cell biology</li>
                           <li>Genetics</li>
                   </ul>
                   </div>
                <div class="franchisee-box"> <span>Step 1 Test Specifications-</span>
                  <p>Table 1: USMLE Step 1 Test Specifications<sup style="color:red">*</sup></p>
                       <table>
                           <tr>
                               <th><p>System</p></th>
                               <th> <p>Range</p></th>		
                              
                           </tr>
                           <tr>
                           <td><p>General Principles of Foundational Science<sup>**</sup></p></td>
                           <td><p>15%-20%</p></td>		
                           
                          </tr>
                           <tr>
                          <td><p>Immune System, Blood & Lymphoreticular System</p>
                                 <p>Behavioral Health</p>
                                 <p>Nervous System & Special Senses</p>
                                  <p>Skin & Subcutaneous Tissue</p>
                                   <p>Musculoskeletal System</p>
                                   <p>Cardiovascular System</p>
                                   <p>Respiratory System</p>
                                   <p>Gastrointestinal System</p>
                                   <p>Renal & Urinary System</p>
                                   <p>Pregnancy, Childbirth, & the Puerperium</p>
                                   <p>Female /Male Reproductive & Breast</p>
                                   <p>Endocrine System</p>
                                   
                          </td>
                          <td><p>60%-70%</p></td>		
                          
                          </tr>
                          <tr>
                              <td><p>Multisystem Processes & Disorders</p>
                                      <p>Biostatistics & Epidemiology</p>
                                      <p>Population Health</p>
                                      <p>Social Sciences</p>
                              </td>
                              <td>
                                  <p>15%-20%</p>
                              </td>
                          </tr>
                          <tr>
                              <td><p>Process</p></td>
                              <td><p></p></td>
                          </tr>
                          <tr>
                              <td><p>Normal Processes</p></td>
                              <td><p>10%-15%</p></td>
                          </tr>
                          <tr>
                              <td><p>Abnormal Processes</p></td>
                              <td><p>55%-60%</p></td>
                          </tr>
                          <tr>
                              <td><p>Principles of Therapeutics</p></td>
                              <td><p>15%-20%</p></td>
                          </tr>
                          <tr>
                              <td><p>Other</p></td>
                              <td><p>10%-15%</p></td>
                          </tr>
                      </table>
                </div>
                <div class="franchisee-box">
                <ul class="dnb-list">
                  <h5>Strategies:-</h5>
                  <p>&nbsp;</p>
                  <li>Read each question carefully. It is important to understand what is being asked.</li>
                  <li>Try to generate an answer and then look for it in the option list.</li>
                  <li>Alternatively, read each option carefully, eliminating those that are clearly incorrect.</li>
                  <li>Of the remaining options, select the one that is most correct.</li>
                  <li>If unsure about an answer, it is better to guess since unanswered questions are automatically counted as wrong answers.</li>
                </ul></div>
                  <div class="franchisee-box">
                  <h5 style="font-size: 20px;">Single-Item Questions :-</h5>
                  <p>A single patient-centered vignette is associated with one question followed by four or more response options. The 
                        response options are lettered (ie, A, B, C, D, E). A portion of the questions involves interpretation of graphic or 
                        pictorial materials. You are required to select the best answer to the question. Other options may be partially correct, 
                        but there is only ONE BEST answer. This is the traditional, most frequently used multiple-choice question format on 
                        the examination.</p>
                  <p>The questions are prepared by examination committees composed of faculty members, teachers, investigators, and
                         clinicians with recognized prominence in their respective fields. Committee members are selected to provide broad 
                          representation from the academic, practice, and licensing communities across the United States and Canada.</p>
              <h4 style="color:#990000;font-size: 16px">Example Item:</h4>
              <p>A 32-year-old woman with type 1 diabetes mellitus has had progressive renal failure over the past 2 years. She has 
                    not yet started dialysis. Examination shows no abnormalities. Her hemoglobin concentration is 9 g/dL, hematocrit is 
                    28%, and mean corpuscular volume is 94 m3. A blood smear shows normochromic, normocytic cells. Which of the 
                    following is the most likely cause?</p>
              <p>A. Acute blood loss<br>
                     B. Chronic lymphocytic leukemia<br>
                     C. Erythrocyte enzyme deficiency<br>
                     D. Erythropoietin deficiency<br>
                     E. Immunohemolysis<br>
                     F. Microangiopathic hemolysis<br>
                     G. Polycythemia vera<br>
                     H. Sickle cell disease<br>
                     I. Sideroblastic anemia<br>
                    J. β-Thalassemia trait<br>
                    (Answer: D)</p>
              <h5 style="font-size: 20px;">Sequential Item Sets :-</h5>
              <p>A single patient-centered vignette may be associated with two or three consecutive questions about the information
                   presented. Each question is linked to the initial patient vignette but is testing a different point. Questions are designed 
                   to be answered in sequential order. You are required to select the ONE BEST answer to each question. Questions 
                   are designed to be answered in sequential order. You must click "Proceed to Next Item" to view the next item in the 
                   set; once you click on this button, you will not be able to add or change an answer to the displayed (previous) item.</p>
                  </div>
            </article>
            <div class="book-ur-seat-btn margn-zero"><a href="http://registration.damsdelhi.com" target="_blank" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div>
          </div>
            <div class="pg-medical-main tab-hide">
  <div class="pg-heading"><span></span>USMLE Step 1</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <h5>Highlight</h5>
      <div class="coures-list-box-content">
        <p>USMLE Step 1 Free Practice Test Sign up for our diagnostic exam and find out your target areas for USMLE Step 1.</p>
        
        <p>This is an hour long test covers 50 questions</p>
        
        <p>absolutely FREE.</p>
        <p><br><strong style="color: red;">Sign up Now</strong></p>
        <p>&nbsp;</p>
        <p><a href="https://onlinetest.damsdelhi.com/" target="_blank">Sign up click on : onlinetest.damsdelhi.com</a></p>
      </div>
    </div>
  </div>
</div>
        </aside>
        <aside class="gallery-right">
          <?php include 'dams-usmle-edge.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
          
      </section>
        
    </div>
  </div>
    <div class="wrapper">
    <aside class="content-left" style="margin-top:10px;width: 100%;">
        <div class="course-box">
         <p><b>Disclaimer</b>
         <br>
         <p style="text-align: justify;font-size: 12px">USMLE® is a joint program of the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners (NBME). The ECFMG® is a registered trademark of the Educational Commission for Foreign Medical Graduates. The Match® is a registered service mark of the National Resident Matching Program® (NRMP®). The NRMP is not affiliated with DAMS. Electronic Residency Application Service (ERAS®) is a program of the association of American Medical Colleges and is not affiliated with DAMS. Test names and other trademarks are the property of the respective trademark holders. None of the trademark holders are affiliated with DAMS or this website.<br>
        </div>
      </aside>
        </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>