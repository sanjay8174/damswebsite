<?php  $pageName= substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); ?>

<div class="pg-medical-main1">
  <div class="pg-heading"><!--<span></span>-->Three parts:</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
        <p class="para_heading">
        <ul class="course-new-list1">
             <li><span class="sub-arrow"></span>Step 1 (Preclinical MCQ) – 8 hours</li>
             <li><span class="sub-arrow"></span>Step 2
             <ul  class="course_detail">
                                  <li>Step 2 CK (Clinical MCQ) – 9 hours</li>
                                  <li>Step 2 CS (Clinical OSCE) – 8 hours (Only in the USA)</li>
             </ul>
             </li>
             <li><span class="sub-arrow"></span>ECFMG Certification<sup>*</sup></li>
             <li><span class="sub-arrow"></span>Step 3 – 16 hours (Only in the USA)
                               <ul>
                                   <li>To be taken after you start working, but can be taken after you are ECFMG certified</li>
                               </ul>
             </li>
            
        
        </ul>
           
      </div>
    </div>
  </div>
</div>
<div class="pg-medical-main1">
  <div class="pg-heading"><!--<span></span>-->First Time in India: Live USMLE Classes</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <h5>USMLE Step I Prep : </h5>
      <h4 class="head_para">Available as Live Face to Face Classes in New Delhi </h4>
      <div class="coures-list-box-content">
        <p class="para_heading">Live lectures. Options include:<br>
        <ul class="course-new-list1">
             <li><span class="sub-arrow"></span>20 weeks Live face to face classes with Expert Faculty Interaction </li>
             <li><span class="sub-arrow"></span>This is a Weekend only prep for students and working professionals only on Saturdays and Sundays</li>
             <li><span class="sub-arrow"></span>Course material,E books  and online q bank Provided</li>
             <li><span class="sub-arrow"></span>Complete Counselling for all paper work for exam and other applications.</li>
             <li><span class="sub-arrow"></span>Complete 1 stop Solution for all your USMLE needs</li>
             <li><span class="sub-arrow"></span>Also available VSAT classes for USMLE all over India .<br>Course duration 24 weeks, on 3 days of the week between 5 pm to 9 pm .</li>
        
        </ul>
           
      </div>
    </div>
  </div>
</div>
<div class="pg-medical-main1 ">
  <div class="pg-heading"><!--<span></span>-->Want to Ace USMLE?  Reach out to DAMS.  </div>
  <div class="course-new-section">
      <div class="coures-list-box">
            <h5>Live lectures Face to face : Learn from Expert Faculty</h5>
      <div class="coures-list-box-content">
        <p class="para_heading">Interact with our expert faculty that combine brilliance in their teaching skills with an intense focus on the USMLE exam. <br></p>

        <ul class="course-new-list1">
              <li><span class="sub-arrow"></span> <p>Extremely high yield topics</p></li>
             <li><span class="sub-arrow"></span>Clinical correlation</li>
              <li><span class="sub-arrow"></span>Subject grasp and retention</li>
              <li><span class="sub-arrow"></span>Focused and Comprehensive</li>
        </ul><br>
      </div>
      </div>
      <div class="coures-list-box">
            <h5>Expert Counseling</h5>
      <div class="coures-list-box-content">
      <ul class="course-new-list1">
              <li><span class="sub-arrow"></span>Daily USMLE custom tailored study schedule </li>
             <li><span class="sub-arrow"></span>Guidance on resources to use </li>
              <li><span class="sub-arrow"></span>Development and refinement of USMLE test-taking strategies </li>
              <li><span class="sub-arrow"></span>Complete application work </li>
              <li><span class="sub-arrow"></span>Complete application work </li>
              <li><span class="sub-arrow"></span>Step by step advice on way forward  </li>
              <li><span class="sub-arrow"></span>Gauging Preparedness quotient and giving Expert advice </li>
        </ul>
      </div>
      </div>
      <div class="coures-list-box">
            <h5>Value added Add On’s:</h5>
      <div class="coures-list-box-content">
      <ul class="course-new-list1">
              <li><span class="sub-arrow"></span>Electives and Observership application help  </li>
             <li><span class="sub-arrow"></span>Q bank for USMLE Step 1</li>
              <li><span class="sub-arrow"></span>Over 2300 practice questions </li>
              <li><span class="sub-arrow"></span>E books on all subjects and Topics covered in the Live lectures </li>
              <li><span class="sub-arrow"></span>Networking Opportunity</li>
              <li><span class="sub-arrow"></span><p class="li_para">Through the course you will meet many doctors and USMLE test takers. These great new contacts helps in group discussion and</p></li>
              <li><span class="sub-arrow"></span>Visa help, with  mock interview training </li>
        </ul>
      </div>
      </div>
  </div>
</div>
        <div class="course-box" style="width:100%;padding: 5px;box-sizing: border-box;border: 1px solid #F0AC49;margin: 25px 0px 0px;">
                  <img style="width:100%;" src="images/USMLE_banner.png">
        </div>
<div class="pg-medical-main1">
  <div class="pg-heading"><!--<span></span>-->Centers all over India for VSAT courses for USMLE step 1</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <h5 style="text-align: center;">Please contact your nearest center for more details on our USMLE program : </h5>
      <?php include 'setaliteCenters.php'; ?>
    </div>
  </div>
</div>
