<!DOCTYPE html>
<?php
$course_id = '4';
?>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>USMLE EDGE Coaching Institute, USMLE EDGE</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>USMLE EDGE</h3>
        <p>It is getting progressively difficult each year to get into the US medical system, yet we at DAMS believe that it is the lack of information that is the biggest drawback that an IMG experiences to get to medical licensure in the United States. We at DAMS now take up that responsibility to provide the right information and guide to do the right thing at the right time to achieve success in United States Medical Licensing Examination.<br>
          <br>
          We propose our own unique course which is integrated <strong>Foundation PG course with USMLE EDGE. How many of us are sure in the prefinal-final year stage about USMLE? How many times have you asked this question-we need a course which gives us both? Yes DAMS now offers the only course which offers online simulated exams for both steps and counselling sessions for USMLE integrated with its very popular PG foundation course.</strong><br>
          <br>
          <strong>Another course, we are now offering is the USMLE Simulated Test Series with counselling sessions for people who want to appear for only USMLE. Very soon we will be launching our own class room programme for the USMLE as well.</strong><br>
          <br>
          The USMLE assesses a physician's ability to apply knowledge, concepts, and principles, and to demonstrate fundamental patient-centered skills, that are important in health and disease and that constitute the basis of safe and effective patient care. Each of the three Steps of the USMLE complements the others; no Step can stand alone in the assessment of readiness for medical licensure. </p>
        <p>The United States Medical Licensing Examination is a three-step examination for medical licensure in the United States and is sponsored by the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners (NBME).</p>
      </div>
      <?php include 'usmle-middle-accordion.php'; ?>
    </aside>
    <aside class="content-right">
      <?php include 'dams-usmle-edge.php'; ?>
      <?php include 'newsRight.php'; ?>
      <?php include 'studentInterview.php'; ?>
    </aside>
  </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>