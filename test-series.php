<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, NEET PG</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 1;
$courseNav_id = 1;
?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="test-series">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Test Series" class="active-link">Test Series</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading full-space paddin-zero">
            <h4>DAMS Test Series</h4>
            <article class="showme-main">
              <div class="main-test">
                <ul>
                  <li class="first margn-zero"> <span>Grand&nbsp;Test</span>
                    <p>only institute <br />
                      predicting <br />
                      new questions</p>
                  </li>
                  <li class="secound"> <span style="padding:0px 2px;">AIPG(NBE/NEET)</span>
                    <p>making sure that <br />
                      youget the repeats <br />
                      correct</p>
                  </li>
                  <li class="third"> <span>SWT&nbsp;Series</span>
                    <p>Comprehensive <br />
                      coverage of topis <br />
                      with reffrences</p>
                  </li>
                  <li class="four"> <span>Special</span>
                    <p>special papers on neet<br />
                      pattern, pgi, state pg’s<br />
                      nimhans &amp; aipg</p>
                  </li>
                </ul>
              </div>
              <div class="test-series-content">
                <ul class="duration-content">
                  <li>
                    <label>Duration :</label>
                    <span>6 Months</span></li>
                  <li>
                    <label>Total no of tests :</label>
                    <span>20 Tests</span></li>
                  <li>
                    <label>Total no. of questions per Test :</label>
                    <span>200 (total questions 4000)</span></li>
                </ul>
                <ul class="some-points">
                  <li><span class="blue_arrow"></span> <span>Authentic explanations from our highly prestigious faculty with references.</span></li>
                  <li><span class="blue_arrow"></span> <span>QuesQuestions are based on AIPG(NBE/NEET) Pattern PG, AIIMS, DNB, UPSC exam pattern.</span></li>
                  <li><span class="blue_arrow"></span> <span>We have various Test Centres for more competitive environment &amp; comfortable approach.</span></li>
                  <li><span class="blue_arrow"></span> <span>Fully explained solved answers key of all 200 Questions will be give immediately after completion</span></li>
                  <li><span class="blue_arrow"></span> <span>All Students enrolled in between the session will get all previous Test papers.</span></li>
                  <li><span class="blue_arrow"></span> <span>All India Ranking with detailed marks will be provided at our website: <a href="https://www.damsdelhi.com" title="PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG">www.damsdelhi.com</a></span></li>
                </ul>
                <p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern.  With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner for AIPG(NBE/NEET) Pattern examinations. We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI,  UPSC, DNB &amp;  MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.</p>
              </div>
              <div class="download-pdf">
                <ul>
                  <li>
                    <div class="download">
                      <div class="orange-box"> I &amp; II - 2014
                        Test  Series 
                        Schedule </div>
                      <a href="downloadpdf/test%20series%20-I&amp;II.pdf" target="_blank"><img src="images/download-arrow.png" title="Download" alt="Download" /> <span>Download</span></a> </div>
                    <div class="download">
                      <div class="orange-box"> III &amp; IV - 2014
                        Test  Series 
                        Schedule </div>
                      <a href="downloadpdf/test%20series%20-III%20&amp;IV.pdf" target="_blank"><img src="images/download-arrow.png" title="Download" alt="Download" /> <span>Download</span></a> </div>
                    <div class="download">
                      <div class="orange-box"> SWT Topic 
                        Distribution </div>
                      <a href="downloadpdf/swt%20TOPICS%20%20DISTRIBUTION.pdf" target="_blank"><img src="images/download-arrow.png" title="Download" alt="Download" />&nbsp;<span>Download</span></a></div>
                  </li>
                </ul>
              </div>
              <div class="how-to-apply">
                <div class="how-to-apply-heading"><span></span> How to Apply</div>
                <div class="how-to-content">
                  <p>Fees to be paid by a Demand Draft/Pay-Order (in favour of <span>‘DELHI ACADEMY OF MEDICAL SCIENCES PVT .LTD.</span>) The DD Pay should be payable at Delhi. DD of total Fee Amount, along with dully filled form to be sent by 
                    registered post/Courier or Submitted personally at <span>Corp. Office, 4B . Pusa Road, Grover’s Building, Near Karol Bagh Metro Station, New Delhi – 110005</span>, DD with form can be given to centre coordinator also. Form can be obtained from respective centre or can be downloaded from our website: <a href="https://www.damsdelhi.com.">https://www.damsdelhi.com.</a> It has also been given with this Grand Test. For your other friends you can get it Xeroxed.</p>
                  <br />
                  <p>When we receive your DD/Pay order, we will acknowledge immediately by SMS to you or E-mail to you. We will send you receipt by post also. You will show this receipt to get laminated I/D Card.</p>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'md-ms-right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>