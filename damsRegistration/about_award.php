
<!DOCTYPE html>
<?php
error_reporting(0);
$aboutUs_Id = $_REQUEST['a'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<?php  $getAboutusContent = $Dao->getAboutusContent($aboutUs_Id);
$getAboutus = $Dao->getAboutus();
$path = constant::$path;
?>
<section class="inner-banner">
<div class="wrapper">
<article style="background:url('<?php echo $path."/images/background_images/".$getAboutusContent[0][0];  ?>') right top no-repeat;">
<aside class="banner-left">
<?php echo  $getAboutusContent[0][1]; ?>
</aside>
</article>
</div>
</section>
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
<ul>
<li class="bg_none"><a href="<?php echo $getAboutus[0][1]."?a=".$getAboutus[0][0] ?>" title="<?php echo $getAboutus[0][2]?>"><?php echo $getAboutus[0][2]?></a></li>
</ul>
</div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading">
<h4>Awards and Achievements</h4>
<p class="award_p"><strong>Our mission is to spread quality education has always been highlighted in the media and has been praised endlessly by awards.</strong></p>
<article class="showme-main">
<div class="about-content">
<div class="publication-box-new aw_cntr" style="width: 97%;" >
<img src='<?php echo $path."/images/news_award/Award.jpg" ?>' width="100%">
<h2>South Asia E-Health Summit Award 2014</h2>
<p>Delhi Academy of Medical Science was awarded the South Asia E-Health Summit Award 2014 for excellence in medical education. Dr Sumer Sethi, director Delhi Academy of Medical Science received the award form E-learning expert, professor Vaidyanathan Balasubramanyam of St John’s Medical College, Bangaluru.</p>
</div>
<div class="publication-box-new aw_cntr" >
<img src='<?php echo $path."/images/news_award/DSC01423.JPG"?>' width="100%">
<h2 style="border-bottom:none;">South Asia E-Health Summit Award 2014</h2>
</div>
<div class="publication-box-new aw_cntr" style="clear:left" >
<img src='<?php echo $path."/images/news_award/DSC01424.JPG" ?>' width="100%">
<h2 style="border-bottom:none;">Top 100 Franchise Opportunities for the year 2014</h2>
</div>
<div class="publication-box-new aw_cntr" style="float: left;
        margin-top: 30px !important;
width: 45.4% !important;" >
<img src='<?php echo $path. "/images/news_award/DSC01425.JPG" ?>' width="100%">
<h2 style="border-bottom:none;">Indian Dental Achievers Awards (IDAA) - 2014</h2>
</div>
<div class="publication-box-new aw_cntr right" style="    width: 40% !important;
    margin-top: 30px !important;
    float: right;
" >
    <img src='<?php echo  $path."/images/news_award/DSC01432.JPG" ?>' width="100%">
<h2 style="border-bottom:none;">Education Excellence Awards</h2>
</div></div></article></div></aside>
<aside class="gallery-right">
<?php include 'about-right-accordion.php'; ?>
<?php include 'enquiryform.php'; ?>
</aside></section></div></div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script> 
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="js/registration.js"></script> 
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>