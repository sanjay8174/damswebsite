<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<!-- [if gte IE8]>
<link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
function ontab(val) {
	var len = $("#accordion > li").size();
    for(i=1;i<=len;i++)
	{
		if(val==i)
		{
			if(i==1)
			{
			  $('#accorsp'+i).removeClass('bgspanblk');
		      $('#aol'+i).slideDown();
		      $('#accorsp'+i).addClass('bgspan');
		      $('#accor'+i).addClass('light-blue');
			}
			else
			{
			  $('#accorsp'+i).removeClass('bgspanblk');
		      $('#aol'+i).slideDown();
		      $('#accorsp'+i).addClass('bgspan');
		      $('#accor'+i).addClass('light-blue-inner');				
			}
		}
		else
		{ 
		    if(i==1)
		    {
			  $('#accorsp'+i).removeClass('bgspan');
		      $('#aol'+i).slideUp();
		      $('#accorsp'+i).addClass('bgspanblk');
		      $('#accor'+i).removeClass('light-blue');
		    }
		    else
		    {
			  $('#accorsp'+i).removeClass('bgspan');
		      $('#aol'+i).slideUp();
		      $('#accorsp'+i).addClass('bgspanblk');
		      $('#accor'+i).removeClass('light-blue-inner');
		    }
		}
	}
};
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">

<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
<div class="wrapper">
<article>

<div class="big-nav">
<ul>
<li class="face-face"><a href="course.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement active"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
</ul>
</div>

<aside class="banner-left">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>

<?php include'banner-button.php'; ?>
</article>
</div>
</section> 

<!-- Banner End Here -->

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span>
<ul>
<li style="background:none;"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Achievement" class="active-link">Achievement</a></li>
</ul>
</div>

<section class="video-container">

<div class="achievment-left">
<div class="achievment-left-links">
<ul id="accordion">
<li style="border:none;" id="accor1" onClick="ontab('1');" class="light-blue-inner"><span id="accorsp1" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
<ol class="achievment-inner" style="display:block;" id="aol1">
<li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
<!--
<li><span class="mini-arrow">&nbsp;</span><a href="mciscreening_sep_2013.php" title="MCI Screening September">MCI Screening September</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="mciscreening_mar_2013.php" title="MCI Screening March">MCI Screening March</a></li>
-->
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>


</ol>
</li>
<li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
<ol class="achievment-inner" style="display:none;" id="aol2">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
<!--
<li><span class="mini-arrow">&nbsp;</span><a href="mciscreening_sep_2012.php" title="MCI Screening September">MCI Screening September</a></li> 
-->
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>

</ol>
</li>
<li id="accor3" onClick="ontab('3');"><span id="accorsp3" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
<ol class="achievment-inner" style="display:none;" id="aol3"> 
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
</ol>
</li>

</ul>
</div>
</div>

<aside class="gallery-left" style="float:right; padding:0px;">
<div class="inner-left-heading">
<h4>AIIMS November 2013</h4>
<section class="showme-main">

<article class="achievment-photos-section">

<ul class="main-students-list">
<li class="tp-border">
<div class="students-box" style="border-left:none;">
<img src="images/students/1.jpg" alt="Student" title="Student" />
<p><span>Dr. ALPESH GOYAL</span> Rank: 20</p>
</div>

<div class="students-box">
<img src="images/students/2.jpg" alt="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" title="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" />
<p><span>Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM</span> Rank: 20</p>
</div>

<div class="students-box">
<img src="images/students/3.jpg" alt="Dr. SHEJOL SUMIT" title="Dr. SHEJOL SUMIT" />
<p><span>Dr. SHEJOL SUMIT</span> Rank: 20</p>
</div>

<div class="students-box">
<img src="images/students/4.jpg" alt="Dr. ATUL JAIN" title="Dr. ATUL JAIN" />
<p><span>Dr. ATUL JAIN</span> Rank: 20</p>
</div>

</li>
<li>
<div class="students-box" style="border-left:none;">
<img src="images/students/1.jpg" alt="Student" title="Student" />
<p><span>Dr. ALPESH GOYAL</span> Rank: 20</p>
</div>

<div class="students-box">
<img src="images/students/2.jpg" alt="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" title="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" />
<p><span>Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM</span> Rank: 20</p>
</div>

<div class="students-box">
<img src="images/students/3.jpg" alt="Dr. SHEJOL SUMIT" title="Dr. SHEJOL SUMIT" />
<p><span>Dr. SHEJOL SUMIT</span> Rank: 20</p>
</div>

<div class="students-box">
<img src="images/students/4.jpg" alt="Dr. ATUL JAIN" title="Dr. ATUL JAIN" />
<p><span>Dr. ATUL JAIN</span> Rank: 20</p>
</div>

</li>
<li>
<div class="students-box" style="border-left:none;">
<img src="images/students/1.jpg" alt="Student" title="Student" />
<p><span>Dr. ALPESH GOYAL</span> Rank: 20</p>
</div>

<div class="students-box">
<img src="images/students/2.jpg" alt="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" title="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" />
<p><span>Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM</span> Rank: 20</p>
</div>

<div class="students-box">
<img src="images/students/3.jpg" alt="Dr. SHEJOL SUMIT" title="Dr. SHEJOL SUMIT" />
<p><span>Dr. SHEJOL SUMIT</span> Rank: 20</p>
</div>

<div class="students-box">
<img src="images/students/4.jpg" alt="Dr. ATUL JAIN" title="Dr. ATUL JAIN" />
<p><span>Dr. ATUL JAIN</span> Rank: 20</p>
</div>
</li>
</ul>
</article>
</section>
</div>
</aside>
</section>
</div>
</div>
</section>

<!-- Midle Content End Here -->



<!-- Footer Css Start Here -->

<?php include 'footer.php'; ?>

<!-- Footer Css End Here -->
<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->
</body></html>