<script>
function sliddes2(val)
{
   var spp=$('div.accordionButton > span').size();
   for(var d=1;d<=spp;d++)
   {   
       if(val!=d)
	   { 
    	 $('#sq-mci'+d).removeClass('arrowup');
         $('#sq-mci'+d).addClass('arrowdwn');
		 $('#diMci'+d).slideUp(400);
	   }      
   }
   
   $('#diMci'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#sq-mci'+val).removeClass('arrowdwn');
            $('#sq-mci'+val).addClass('arrowup');
       }
	   else
	   {
			$('#sq-mci'+val).removeClass('arrowup');
            $('#sq-mci'+val).addClass('arrowdwn');
       }
   });
}
</script>

<div id="accor-wrapper">
  <div class="accordionButton" onclick="sliddes2('1')" style="margin-top:0px;"><span class="arrowdwn" id="sq-mci1"></span>Classroom Course</div>
  <div class="accordionContent" id="diMci1" style="display: none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="mci-reguler-course.php" title="Regular Course"><span class="sub-arrow"></span> Regular Course</a></li>
          <li><a href="mci-crash-course.php" title="Crash Course"><span class="sub-arrow"></span> Crash Course</a></li>
          <li><a href="mci-test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddes2('2')"><span class="arrowdwn" id="sq-mci2"></span>Distance Learning Course</div>
  <div class="accordionContent" id="diMci2" style="display: none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="mci-online-test-series.php" title="Online Test Series"><span class="sub-arrow"></span> Online Test Series</a></li>
          <li><a href="mci-postal-course.php" title="Postal Test Series"><span class="sub-arrow"></span> Postal Test Series</a></li>
          <!--<li><a href="#" title="Mock Test Series"><span class="sub-arrow"></span> Mock Test Series</a></li>-->
          <li><a href="dams-publication.php?c=2" title="iDAMS Tablet Based Course"><span class="sub-arrow"></span> iDAMS Tablet Based Course</a></li>
        </ol>
      </ul>
    </div>
  </div>
</div>
