<script>
function sliddesqz(val)
{
   var spq=$('div.accordionButton > span').size();
   for(var d=1;d<=spq;d++)
   {   
       if(val!=d)
	   { 
    	 $('#sqz'+d).removeClass('arrowup');
         $('#sqz'+d).addClass('arrowdwn');
		 $('#dipz'+d).slideUp(400);
	   }      
   }
   
   $('#dipz'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#sqz'+val).removeClass('arrowdwn');
            $('#sqz'+val).addClass('arrowup');
       }
	   else
	   {
			$('#sqz'+val).removeClass('arrowup');
            $('#sqz'+val).addClass('arrowdwn');
       }
   });
}
</script>

<div id="accor-wrapper">
  <div class="accordionButton" onclick="sliddesqz('1')"><span class="arrowdwn" id="sqz1"></span>National Quiz</div>
  <div class="accordionContent" id="dipz1" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="gray-matter.php" title="Concept of Grey Matter" ><span class="sub-arrow"></span>Concept of Grey Matter</a></li>
          <li><a href="gray-matter-testimonial.php" title="Testimonials"><span class="sub-arrow"></span>Testimonials</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddesqz('2')"><span class="arrowdwn" id="sqz2"></span>2012</div>
  <div class="accordionContent" id="dipz2" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="round1.php" title="1st Campus Round 2012"><span class="sub-arrow"></span>1st Campus Round 2012</a></li>
          <li><a href="round2.php" title="2nd Campus Round 2012"><span class="sub-arrow"></span>2nd Campus Round 2012</a></li>
          <li><a href="grand.php" title="Grand Finale 2012"><span class="sub-arrow"></span>Grand Finale 2012</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddesqz('3')"><span class="arrowdwn" id="sqz3"></span>2013</div>
  <div class="accordionContent" id="dipz3" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="season2round1.php" title="1st Campus Round 2013"><span class="sub-arrow"></span>1st Campus Round 2013</a></li>
          <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
          <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddesqz('4')"><span class="arrowdwn" id="sqz4"></span>2014</div>
  <div class="accordionContent" id="dipz4" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="aiims-delhi.php" title="1st Campus Round 2014"><span class="sub-arrow"></span>1st Campus Round 2014</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddesqz('5')"><span class="arrowdwn" id="sqz5"></span>2015</div>
  <div class="accordionContent" id="dipz5" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
           <li><a href="national_2015.php" title="1st Campus Round 2015"><span class="sub-arrow"></span>1st Campus Round 2015</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The Grand Finale of season 4 of Grey Matter DAMS National Quiz got over in PULSE 2015. </a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The Grand Finale was held in Conference Hall of JLN Auditorium, AIIMS campus, New Delhi on 17th September 2015. </a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>This year too the winning team was from AIIMS, New Delhi. </a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The first runners up trophy was lifted by team from SMS Medical College, Jaipur. </a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The second runners up were the team from Govt. Medical College, Imphal, Manipur. </a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>It was a nail-biting final, and all the teams had a great contest till the deciding moments. </a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>Congratulations!! To all winning teams teams. </a></li>
          
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddesqz('6')"><span class="arrowdwn" id="sqz6"></span>2016</div>
  <div class="accordionContent" id="dipz6" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The great Grand Finale of Grey Matter DAMS National Quiz, Season 5 took place on 13 Nov 2016.</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The four teams were from four zones of the country.</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The quiz was telecasted live on YouTube and Facebook. It was watched by more than 10000 doctors all over India. LATEST ADDITION TO THE CHAIN OF EVENTS  THAT DAMS HAS PIONEERED IN INDIA. Another plume in the hat.</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The quiz format was unique and was presented by 4 quiz masters namely Dr. Sumer, Dr. Deepti, Dr. Sachin Garg, Dr. Siddharth Mishra.</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The quiz lasted for 2 hrs and at the end of it teams were longing for more.</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The second runners up was the team from R G Kar Medical College, Kolkata. Team represented by Subodh Shankar Behera & Indrashish Chaudhury.</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>They won a cash prize of Rs.15000/- and a discount of 50% in course fees for any DAMS course, valid nationwide.</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The first runners up was the team from Thanjavur med college, team represented by M Sethuraman & Ragul Ajay BG. They won a cash prize of Rs.25000/- and 100% discount on course fees for any DAMS course, valid nationwide.</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>The WINNERS for the season 5 of this prestigious quiz were the students from AIIMS, New Delhi, represented by Umang Arora & Shubham Agarwal.</a></li>
          <li><a href="javascript:void(0)"><span class="sub-arrow"></span>They lifted the WINNERS trophy and won a cash prize of Rs.50000/-(Fifty Thousand) and 100% discount on course fees.</a></li>
        </ol>
      </ul>
    </div>
  </div>
</div>
