<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">
<div class="big-nav">
<ul>
<li class="face-face"><a href="index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement" class="a-achievement-active">Achievement</a></li>
</ul>
</div>
<aside class="banner-left banner-l-heading">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include 'md-ms-banner-btn.php'; ?>
</article>
</div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span>
<ul>
<li class="bg_none"><a href="index.php" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Achievement" class="active-link">Achievement</a></li>
</ul>
</div>

<section class="video-container">

<div class="achievment-left">
<div class="achievment-left-links">
<ul id="accordion">
<li id="accor5" onClick="ontab('5');" class="light-blue border_none"><span id="accorsp5" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2015">2015</a>
<ol class="achievment-inner display_block" id="aol5">
<li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="wb_2015.php" title="West Bengal">WB 2015</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipgme_2015.php" title="AIPGME">AIPGME</a></li>
</ol>
</li>

<li id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
<ol class="achievment-inner display_none" id="aol1">
<li><span class="mini-arrow">&nbsp;</span><a href="jipmer_nov_2014.php" title="JIPMER NOV">JIPMER NOV</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_nov_2014.php" title="PGI NOV">PGI NOV</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims-nov-2014.php" title="AIIMS NOV">AIIMS NOV</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_jun_2014.php" title="DNB June">DNB June</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2014.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2014.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2014.php" title="AIPGME">AIPGME</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="UPPG_2014.php" title="UPPG">UPPG</a></li>
</ol>
</li>

<li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
<ol class="achievment-inner display_none" id="aol2">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2013.php" title="AIPGME">AIPGME</a></li>
</ol>
</li>

<li id="accor3" onClick="ontab('3');"><span id="accorsp3" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
<ol class="achievment-inner display_none" id="aol3">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>
</ol>
</li>

<li id="accor4" onClick="ontab('4');"><span id="accorsp4" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
<ol class="achievment-inner display_none" id="aol4">
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
</ol>
</li>

</ul>
</div>
</div>

<aside class="gallery-left respo-achievement right_pad0">
<div class="inner-left-heading">
<h4>West Bengal 2015</h4>
<section class="showme-main">

<ul class="idTabs idTabs5">
<li><a href="#jquery" class="selected" title="West Bengal 2015">WB 2015 </a></li>
<li><a href="#official" title="Interview">Interview</a></li>
</ul>

<div id="jquery">
<article class="interview-photos-section">
<ul class="main-students-list">
<li class="tp-border">
<div class="students-box border_left_none">
<img src="images/topper/wb-2.jpg" alt="Dr. Debanu De" title="Dr. Debanu De" />
<p><span>Dr. Debanu De</span> WBPG - 1</p>
</div>

<div class="students-box">
<img src="images/topper/wb-1.jpg" alt="Dr. Raj Dutta" title="Dr. Raj Dutta" />
<p><span>Dr. Raj Dutta</span> WBPG - 2</p>
</div>

<div class="students-box">
<img src="images/topper/wb-3.jpg" alt="Dr. Pooja Saha" title="Dr. Pooja Saha" />
<p><span>Dr. Pooja Saha</span> WBPG - 3</p>
</div>

<div class="students-box">
<img src="images/topper/wb-4.jpg" alt="Dr. Debayan Dasgupta" title="Dr. Debayan Dasgupta" />
<p><span>Dr. Debayan Dasgupta</span> WBPG - 6</p>
</div>
</li>

<li>
<div class="students-box border_left_none">
<img src="images/topper/wb-5.jpg" alt="Dr. Pritam singha Roy" title="Dr. Pritam singha Roy" />
<p><span>Dr. Pritam singha Roy</span> WBPG - 7</p>
</div>

<div class="students-box">
<img src="images/topper/wb-6.jpg" alt="Dr. Sudeshna Malakar" title="Dr. Sudeshna Malakar" />
<p><span>Dr. Sudeshna Malakar</span> WBPG - 9</p>
</div>

<div class="students-box">
<img src="images/topper/wb-7.jpg" alt="Dr. Pijush Kanti Nandi" title="Dr. Pijush Kanti Nandi" />
<p><span>Dr. Pijush Kanti Nandi</span> WBPG - 11</p>
</div>



<div class="students-box">
<img src="images/topper/wb-8.jpg" alt="Dr. Keka Mandali" title="Dr. Keka Mandali" />
<p><span>Dr. Keka Mandali</span> WBPG - 14</p>
</div>

<div class="students-box border_left_none">
<img src="images/topper/wb-9.jpg" alt="Dr. Satyajit Mahto" title="Dr. Satyajit Mahto" />
<p><span>Dr. Satyajit Mahto</span> WBPG - 20</p>
</div>

<div class="students-box">
<img src="images/topper/wb-10.jpg" alt="Dr. Uttiya Saha" title="Dr. Uttiya Saha" />
<p><span>Dr. Uttiya Saha</span> WBPG - 29</p>
</div>

<div class="students-box">
<img src="images/topper/wb-11.jpg" alt="Dr. Souvik Guha" title="Dr. Souvik Guha" />
<p><span>Dr. Souvik Guha</span> WBPG - 30</p>
</div>
    
<div class="students-box">
<img src="images/topper/wb-12.jpg" alt="Dr. Debopom Goswami" title="Dr. Debopom Goswami" />
<p><span>Dr. Debopom Goswami</span> WBPG - 31</p>
</div>
    
<div class="students-box border_left_none">
<img src="images/topper/wb-13.jpg" alt="Dr. Sourav Sharma" title="Dr. Sourav Sharma" />
<p><span>Dr. Sourav Sharma</span> WBPG - 32</p>
</div>

<div class="students-box">
<img src="images/topper/wb-14.jpg" alt="Dr. Amlan Dutta" title="Dr. Amlan Dutta" />
<p><span>Dr. Amlan Dutta</span> WBPG - 50</p>
</div>

<div class="students-box">
<img src="images/topper/wb-15.jpg" alt="Dr. Sujoy Kabiraj" title="Dr. Sujoy Kabiraj" />
<p><span>Dr. Sujoy Kabiraj</span> WBPG - 65</p>
</div>

<div class="students-box">
<img src="images/topper/wb-16.jpg" alt="Dr. Aridam Santra" title="Dr. Aridam Santra" />
<p><span>Dr. Aridam Santra</span> WBPG - 82</p>
</div>

<div class="students-box border_left_none" style="border-right: 1px dashed #b7b7b7;">
<img src="images/topper/wb-17.jpg" alt="Dr. Souravmoy Talukdar" title="Dr. Souravmoy Talukdar" />
<p><span>Dr. Souravmoy Talukdar</span> WBPG - 97</p>
</div>

</li>

</ul>
<br style="clear:both;" />
<br style="clear:both;" />

</article></div>

<div id="official">
<article class="interview-photos-section">

<div class="achievment-videos-section">
<ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/Cn1YckjnbhY" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Shashank Raj</span></p>
                            <p>Rank: 4th PGI, Rank: 28th AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/OWv9mXT_fxI" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Abhenil Mittal</span></p>
                            <p>Rank: 6th AIIMS Nov 2014, Rank: 7th PGI</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/OFy6m-azegA" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Siddharth Jain</span></p>
                            <p>Rank: 1st AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/EbUbqGEiuVM?list=PLcou4I3N5obYaiRIyAx4TKlraHGS5VdHa" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Ravi Sharma</span></p>
                            <p>Rank: 2nd AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                       </li>
                       <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Tuhina Goel</span></p>
                            <p>Rank: 1st AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Nishant Kumar</span></p>
                            <p>Rank: 56th AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                       </li>
                       <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Geetika Srivastava</span></p>
                            <p>Rank: 6th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bhawna Attri</span></p>
                            <p>Rank: 24th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>

                    </ul>
</div>
</article>
</div>
</section>
</div>
</aside>
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
