<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="index.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Achievement" class="active-link">Achievement</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li id="accor5" onClick="ontab('5');"><span id="accorsp5" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2015">2015</a>
                <ol class="achievment-inner display_none" id="aol5">
                  <li><span class="mini-arrow">&nbsp;</span><a href="wb_2015.php" title="West Bengal">WB 2015</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipgme_2015.php" title="AIPGME">AIPGME</a></li>
                </ol>
              </li>
              <li id="accor1" onClick="ontab('1');" class="light-blue"><span id="accorsp1" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
                <ol class="achievment-inner display_block" id="aol1">
                  <li><span class="mini-arrow">&nbsp;</span><a href="jipmer_nov_2014.php" title="JIPMER NOV">JIPMER NOV</a></li>
                  <li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="pgi_nov_2014.php" title="PGI NOV">PGI NOV</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims-nov-2014.php" title="AIIMS NOV">AIIMS NOV</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_jun_2014.php" title="DNB June">DNB June</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2014.php" title="PGI May">PGI May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2014.php" title="AIIMS May">AIIMS May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipge_2014.php" title="AIPGME">AIPGME</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="UPPG_2014.php" title="UPPG">UPPG</a></li>
                </ol>
              </li>
              <li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
                <ol class="achievment-inner display_none" id="aol2">
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipge_2013.php" title="AIPGME">AIPGME</a></li>
                </ol>
              </li>
              <li id="accor3" onClick="ontab('3');"><span id="accorsp3" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
                <ol class="achievment-inner display_none" id="aol3">
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>
                </ol>
              </li>
              <li id="accor4" onClick="ontab('4');"><span id="accorsp4" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
                <ol class="achievment-inner display_none" id="aol4">
                  <li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading">
            <h4>PGI NOV 2014</h4>
            <section class="showme-main">
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="PGI Nov 2014">PGI Nov 2014</a></li>
                <li><a href="#official" title="Interview">Interview</a></li>
              </ul>
              <div id="jquery">
                <article class="interview-photos-section">
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box" style="border-left:none"> 
                        <img src="images/topper/VIVEK_LANKA.jpg" width="100px" height="109px" alt="Dr. VIVEK LANKA" title="Dr. VIVEK LANKA" />
                        <p><span>Dr. VIVEK LANKA<br><strong>Rank - 1</strong></span></p>
                      </div>
                      <div class="students-box"> 
                        <img src="images/topper/AMIT_GUPTA.jpg" width="100px" height="109px" alt="Dr. AMIT GUPTA" title="Dr. AMIT GUPTA" />
                        <p><span>Dr. AMIT GUPTA<br><strong>Rank - 2</strong></span></p> 
                      </div>
                      <div class="students-box"> 
                        <img src="images/topper/SHASHANK_RAJ.jpg" width="100px" height="109px" alt="Dr. SHASHANK RAJ" title="Dr. SHASHANK RAJ" />
                        <p><span>Dr. SHASHANK RAJ<br><strong>Rank - 4</strong></span></p> 
                      </div>
                      <div class="students-box"> 
                        <img src="images/topper/ABHENIL_MITTAL.jpg" width="100px" height="109px" alt="Dr. ABHENIL MITTAL" title="Dr. ABHENIL MITTAL" />
                        <p><span>Dr. ABHENIL MITTAL<br><strong>Rank - 7</strong></span></p>
                      </div>                      
                    </li>
                    <li style="margin-top: -5px;">
                      <div class="students-box" style="border-left:none"> 
                       <img src="images/topper/CHARANPREET_SINGH.jpg" width="100px" height="109px" alt="Dr. CHARANPREET SINGH" title="Dr. CHARANPREET SINGH" />
                       <p><span>Dr. CHARANPREET SINGH<br><strong>Rank - 9</strong></span></p> 
                      </div>
                      <div class="students-box" style="border-right: 1px dashed #b7b7b7;"> 
                        <img src="images/topper/ROHAN_KAMAT.jpg" width="100px" height="109px" alt="Dr. ROHAN KAMAT" title="Dr. ROHAN KAMAT" />
                        <p><span>Dr. ROHAN KAMAT<br><strong>Rank - 10</strong></span></p> 
                      </div>
                    </li>
                  </ul>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                  <div class="schedule-mini-series"> <span class="mini-heading">PGI Nov - Result 2014</span>
                    <div class="schedule-mini-top"> <span class="one-part">S. No.</span> <span class="two-part">Student Name</span> <span class="three-part">&nbsp;</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li style="border:none;"><span class="one-parts">1</span> <span class="two-parts schedule-left-line">Dr. SIDDHARTH JAIN</span> </li>
                        <li><span class="one-parts">2</span> <span class="two-parts schedule-left-line">DR. VIVEK LANKA</span> </li>
                        <li><span class="one-parts">3</span> <span class="two-parts schedule-left-line">Dr. AMIT GUPTA</span> </li>
                        <li><span class="one-parts">4</span> <span class="two-parts schedule-left-line">Dr. SHASHANK RAJ</span> </li>
                        <li><span class="one-parts">5</span> <span class="two-parts schedule-left-line">Dr. ABHENIL MITTAL</span> </li>
                        <li><span class="one-parts">6</span> <span class="two-parts schedule-left-line">Dr. VYBHAV VENKATESH</span> </li>
                        <li><span class="one-parts">7</span> <span class="two-parts schedule-left-line">Dr. CHARANPREET SINGH</span> </li>
                        <li><span class="one-parts">8</span> <span class="two-parts schedule-left-line">Dr. ROHAN KAMAT</span> </li>
                        <li><span class="one-parts">9</span> <span class="two-parts schedule-left-line">Dr. SONALI SURI</span> </li>
                        <li><span class="one-parts">10</span> <span class="two-parts schedule-left-line">Dr. KRITI KISHORE</span> </li>
                        <li><span class="one-parts">11</span> <span class="two-parts schedule-left-line">Dr. SHUBHRA MISHRA</span> </li>
                        <li><span class="one-parts">12</span> <span class="two-parts schedule-left-line">Dr. ROHIT KANSAL</span> </li>
                        <li><span class="one-parts">13</span> <span class="two-parts schedule-left-line">Dr. MANDULA PHANI PRIYA</span> </li>
                        <li><span class="one-parts">14</span> <span class="two-parts schedule-left-line">Dr. DIVYA JOSHI</span> </li>
                        <li><span class="one-parts">15</span> <span class="two-parts schedule-left-line">Dr. NEHA TANEJA</span> </li>
                        <li><span class="one-parts">16</span> <span class="two-parts schedule-left-line">Dr. SAGNIK SEN</span> </li>
                        <li><span class="one-parts">17</span> <span class="two-parts schedule-left-line">Dr. ANISH CHOUDHARY</span> </li>
                        <li><span class="one-parts">18</span> <span class="two-parts schedule-left-line">Dr. APURVA SOOD</span> </li>
                        <li><span class="one-parts">19</span> <span class="two-parts schedule-left-line">Dr. SNEHA SHARMA</span> </li>
                        <li><span class="one-parts">20</span> <span class="two-parts schedule-left-line">Dr. PANKAJ KAPOOR</span> </li>
                        <li><span class="one-parts">21</span> <span class="two-parts schedule-left-line">Dr. RAJAT CHOUDHARY</span> </li>
                        <li><span class="one-parts">22</span> <span class="two-parts schedule-left-line">Dr. PRAGYA GUPTA</span> </li>
                        <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line" style="color:#000;">And many more...</span> </li>
                      </ul>
                    </div>
                  </div>
                </article>
              </div>
              <div id="official">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/Cn1YckjnbhY" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Shashank Raj</span></p>
                            <p>Rank: 4th PGI, Rank: 28th AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/OWv9mXT_fxI" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Abhenil Mittal</span></p>
                            <p>Rank: 6th AIIMS Nov 2014, Rank: 7th PGI</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/OFy6m-azegA" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Siddharth Jain</span></p>
                            <p>Rank: 1st AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/EbUbqGEiuVM?list=PLcou4I3N5obYaiRIyAx4TKlraHGS5VdHa" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Ravi Sharma</span></p>
                            <p>Rank: 2nd AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                       </li>
                       <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Tuhina Goel</span></p>
                            <p>Rank: 1st AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Nishant Kumar</span></p>
                            <p>Rank: 56th AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                       </li>
                       <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Geetika Srivastava</span></p>
                            <p>Rank: 6th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bhawna Attri</span></p>
                            <p>Rank: 24th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      
                    </ul>
                  </div>
                </article>
              </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
