<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG </title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<style>
    .career-content p{
        font-size: 16px;
        line-height:28px;
    }
</style>
</head>
<body class="inner-bg">
<?php error_reporting(0); ?>
<?php include 'registration.php'; ?><?php include 'enquiry.php'; ?><?php include 'social-icon.php'; ?><?php include 'header.php'; ?><section class="inner-banner"><div class="wrapper"><article class="career-banner"><aside class="banner-left"><h2>National Exit Test</h2></aside></article></div></section> 
    <section class="inner-gallery-content">
        <div class="wrapper">
            <div class="photo-gallery-main">
                <section class="event-container">
                    <aside class="center-left">
                        <div class="inner-center">
                            <h4>NEXT (National Exit Test )</h4>
                            <article class="showme-main">
                                <div class="career-content">
                                    <p>
At this point we all know India is looking at a national level licensing exam which will also serve as a single window for PG entrance and Foreign medical graduates licensing exam under the NMC act ( National Medical commission ). Passing this exam is mandatory and even the MBBS curriculum now has a significant element of horizontal and vertical integration.

DAMS has been pioneer in the segment of objective based medical learning and we offer courses for both theory and practical aspects of NEXT exam, and offering the further advantage of being the pioneer in integrated medical education, a sample of which is available in our numerous free integrated learning videos called medicine unplugged on YouTube which is acclaimed worldwide. Under the able leadership of Dr Sumer Sethi, MD Radiology and Dr Deepti Sethi, MS Obs Gyne we represent an unique blend of versatile educators catering to needs of medical students for last two decades. We assure you of DAMS stamp of quality in education and integrated learning needed for this new challenge for medicos called as NEXT.
</p>
                                    </div></article></div> 
                    </aside> 
                </section>
            </div>
        </div>
    </section>
    <?php include 'footer.php'; ?>
    <script type="text/javascript" src="js/html5.js"></script>
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/registration.js"></script>
    <script type="text/javascript" src="js/add-cart.js"></script>
    </body>
</html>