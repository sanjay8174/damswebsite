/*
	Supersized - Fullscreen Slideshow jQuery Plugin
	Version 3.1.4
	www.buildinternet.com/project/supersized
	
	By Sam Dunn / One Mighty Roar (www.onemightyroar.com)
	Released under MIT License / GPL License
*/

(function($){

	//  Add in supersized elements
	$(document).ready(function() {
		$('body').prepend('').append('<div id="supersized"></div>');
	});
    
    $.supersized = function(options){
    	
    	window.supersized = {};
    	el = '#supersized';
        var base = this;
        //  Access to jQuery and DOM versions of element
        base.$el = $(el);
        base.el = el;
        
		// General slideshow variables
		var inAnimation = false;			// Prevents animations from stacking
		var isPaused = false;				// Tracks paused on/off
		var currentSlide = false;			// Current slide number
		var updateImages = false;			// Trigger to update images after slide jump
		var image_path = false;				// Image path for nav items
		var linkTarget = false;				// Whether links open in new window
		
        var thumbPage = false;				//Thumbnail page
        var thumbInterval = false;			//Thumbnail interval
        
        //  Add a reverse reference to the DOM object
        base.$el.data("supersized", base);
        
		base.init = function(){
        	//  Combine options
        	base.options = $.extend({},$.supersized.defaultOptions, options);
           
            supersized._build();
            supersized._init();
			$(window).load(function(){
				supersized.launch();
				supersized.theme();
			});
        };
        
        //  Methods
        supersized = {
        	
        	// Theme specific
        	theme : function(){
        	
        		if (base.options.slide_links) $('#slide-list').css('margin-left', -$('#slide-list').width()/2)
        		
        		// Thumb Intervals
        		thumbInterval = Math.floor($(base.options.thumb_tray).width() / $('> li', base.options.thumb_list).outerWidth(true)) * $('> li', base.options.thumb_list).outerWidth(true);
        		thumbPage = 0;
        		
        		$('#thumb-forward').click(function(){
        			if (thumbPage - thumbInterval <= -$(base.options.thumb_list).width()){
        				thumbPage = 0;
        				$(base.options.thumb_list).stop().animate({'left': thumbPage}, {duration:500, easing:'easeOutExpo'});
        			}else{
        				thumbPage = thumbPage - thumbInterval;
        				$(base.options.thumb_list).stop().animate({'left': thumbPage}, {duration:500, easing:'easeOutExpo'});
        			}
        		});
        		
        		$('#thumb-back').click(function(){
        			if (thumbPage + thumbInterval > 0){
        				thumbPage = Math.floor($(base.options.thumb_list).width() / thumbInterval) * -thumbInterval;
        				if ($(base.options.thumb_list).width() <= -thumbPage) thumbPage = thumbPage + thumbInterval;
        				$(base.options.thumb_list).stop().animate({'left': thumbPage}, {duration:500, easing:'easeOutExpo'});
					}else{
        				thumbPage = thumbPage + thumbInterval;
        				$(base.options.thumb_list).stop().animate({'left': thumbPage}, {duration:500, easing:'easeOutExpo'});
        			}
        		});
        		
        		        		
        		/*
        		// Thumb scrub
				$(base.options.thumb_tray).mousemove(function(e) {
					if ($(base.options.thumb_list).width() > $(window).width()){
						var $list = $(base.options.thumb_list); 
						var containerwidth = $(base.options.thumb_tray).width(); 
						var listwidth = $(base.options.thumb_list).width(); 
						var mousepos = 1;
					  	var diff = e.pageX - mousepos;
						if (diff > 10 || diff < -10) { 
						    mousepos = e.pageX; 
						    newX = (containerwidth - listwidth) * (e.pageX/containerwidth);
						    diff = parseInt(Math.abs(parseInt($(base.options.thumb_list).css('left'))-newX )).toFixed(0);
						    $(base.options.thumb_list).stop().animate({'left':newX}, {duration:diff*3, easing:'easeOutExpo'});
						}
					}
				});
				*/
				
        	},
        	
        	// Place require components
        	_build : function(){
        	
        		// Add in slide markers
	        	thisSlide = 0;
				var markers = '';
				var thumbMarkers = '';
				var thumbImage = '';
				while(thisSlide <= base.options.slides.length-1){
					if(thisSlide == 0){
						if (base.options.slide_links) markers = markers+'<li class="slide'+thisSlide+' current-slide"><a></a></li>';
						if (base.options.thumb_links){
							base.options.slides[thisSlide].thumb ? thumbImage = base.options.slides[thisSlide].thumb : thumbImage = base.options.slides[thisSlide].image;
							thumbMarkers = thumbMarkers+'<li class="thumb'+thisSlide+' current-thumb"><img src="'+thumbImage+'"/></li>';
						};
					}else{
						if (base.options.slide_links) markers = markers+'<li class="slide'+thisSlide+'"><a></a></li>';
						if (base.options.thumb_links){
							base.options.slides[thisSlide].thumb ? thumbImage = base.options.slides[thisSlide].thumb : thumbImage = base.options.slides[thisSlide].image;
							thumbMarkers = thumbMarkers+'<li class="thumb'+thisSlide+'"><img src="'+thumbImage+'"/></li>';
						};
					}
					thisSlide++;
				}
				if (base.options.slide_links) $('#slide-list').append(markers);
				if (base.options.thumb_links){
					$(base.options.thumb_tray).append('<ul id="thumb-list"></ul>');
						$(base.options.thumb_list).append(thumbMarkers);
				}
				
				// Add in thumbnails
				if (base.options.thumbnail_navigation){
					// Load previous thumbnail
					currentSlide - 1 < 0  ? prevThumb = base.options.slides.length - 1 : prevThumb = currentSlide - 1;
					$('#prevthumb').show().html($("<img/>").attr("src", base.options.slides[prevThumb].image));
					
					// Load next thumbnail
					currentSlide == base.options.slides.length - 1 ? nextThumb = 0 : nextThumb = currentSlide + 1;
					$('#nextthumb').show().html($("<img/>").attr("src", base.options.slides[nextThumb].image));
				}
				
        	},
        	
        	// Setup
    		_init : function(){
    		
    			image_path = base.options.image_path;	// Default image path for navigation control buttons
    			
    			// Determine if starting slide random
				if (base.options.start_slide){
					currentSlide = base.options.start_slide - 1;
				}else{
					currentSlide = Math.floor(Math.random()*base.options.slides.length);	// Generate random slide number
				}
				
				// If links should open in new window
				var linkTarget = base.options.new_window ? ' target="_blank"' : '';
				
				// Set slideshow quality (Supported only in FF and IE, no Webkit)
				if (base.options.performance == 3){
					base.$el.addClass('speed'); 		// Faster transitions
				} else if ((base.options.performance == 1) || (base.options.performance == 2)){
					base.$el.addClass('quality');	// Higher image quality
				}
							
				// Shuffle slide order if needed		
				if (base.options.random){
					arr = base.options.slides;
					for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);	// Fisher-Yates shuffle algorithm (jsfromhell.com/array/shuffle)
				    base.options.slides = arr;
				}
				
				/***Load initial set of images***/
		
				if (base.options.slides.length > 1){
					// Set previous image
					currentSlide - 1 < 0  ? loadPrev = base.options.slides.length - 1 : loadPrev = currentSlide - 1;	// If slide is 1, load last slide as previous
					var imageLink = (base.options.slides[loadPrev].url) ? "href='" + base.options.slides[loadPrev].url + "'" : "";
					
					var imgPrev = $('<img src="'+base.options.slides[loadPrev].image+'"/>');
					imgPrev.appendTo(base.el).wrap('<a class="image-loading" ' + imageLink + linkTarget + '></a>');
				
					imgPrev.load(function(){
						$('.image-loading').removeClass('image-loading');
						$(this).data('origWidth', $(this).width()).data('origHeight', $(this).height());
						supersized.resizeNow();	// Resize background image
					});	// End Load
				}
				
				// Set current image
				imageLink = (base.options.slides[currentSlide].url) ? "href='" + base.options.slides[currentSlide].url + "'" : "";
				var img = $('<img src="'+base.options.slides[currentSlide].image+'"/>');
				img.appendTo(base.el).wrap('<a class="image-loading activeslide" ' + imageLink + linkTarget + '></a>');
				
				img.load(function(){
					$('.image-loading').removeClass('image-loading');
					$(this).data('origWidth', $(this).width()).data('origHeight', $(this).height());
					supersized.resizeNow();	// Resize background image
				});
				
				
				if (base.options.slides.length > 1){
					// Set next image
					currentSlide == base.options.slides.length - 1 ? loadNext = 0 : loadNext = currentSlide + 1;	// If slide is last, load first slide as next
					imageLink = (base.options.slides[loadNext].url) ? "href='" + base.options.slides[loadNext].url + "'" : "";
					
					var imgNext = $('<img src="'+base.options.slides[loadNext].image+'"/>');
					imgNext.appendTo(base.el).wrap('<a class="image-loading" ' + imageLink + linkTarget + '></a>');
				
					imgNext.load(function(){
						$('.image-loading').removeClass('image-loading');
						$(this).data('origWidth', $(this).width()).data('origHeight', $(this).height());
						supersized.resizeNow();	// Resize background image
					});	// End Load
				}
				/***End load initial images***/
				
				//  Hide elements to be faded in
				base.$el.fadeTo(0,0);
				$('#controls-wrapper').hide();
				$(base.options.thumb_tray).animate({bottom : -$(base.options.thumb_tray).height()}, 0 );
				$('.nav-item').hide();
				if (base.options.progress_bar) $('#progress-bar').hide();
			
    		},
    		
    		launch : function(){
    		
    			$('#supersized-loader').hide();		// Hide loading animation
				base.$el.fadeTo('fast',1);				// Fade in background
				$('#controls-wrapper').show();		// Display controls
				$(base.options.thumb_list).width($('> li', base.options.thumb_list).length * $('> li', base.options.thumb_list).outerWidth(true));	//Adjust to true width of thumb markers
				if (base.options.progress_bar) $('#progress-bar').show();
				
				supersized.resizeNow();	// Resize background image
				
				if (base.options.slide_captions) $('#slidecaption').html(base.options.slides[currentSlide].title);		// Pull caption from array
				// Hide navigation if turned off
				if (!(base.options.navigation)){
					$('#navigation').hide();
					$('.nav-item').hide();
				}else{
					$('.nav-item').show();
				}
				
				
				// Start slideshow if enabled
				if (base.options.slideshow && base.options.slides.length > 1){
				
					if (base.options.slide_counter){	// Initiate slide counter if active
						$('#slidecounter .slidenumber').html(currentSlide + 1);			// Pull initial slide number from options		
		    			$('#slidecounter .totalslides').html(base.options.slides.length);	// Pull total from length of array
		    		}
		    		
		    		slideshow_interval = setInterval(supersized.nextSlide, base.options.slide_interval);	// Initiate slide interval
					
					// Prevent slideshow if autoplay disabled
		    		if (!(base.options.autoplay)){
						
						clearInterval(slideshow_interval);	// Stop slideshow
						isPaused = true;	// Mark as paused
						
						if (base.options.progress_bar) $('#progress-bar').stop().animate({left : -$(window).width()}, 0 );
						if ($(base.options.play_button).attr('src')) $(base.options.play_button).attr("src", image_path + "play.png");	// If pause play button is image, swap src
	    				
					}
					
					if (base.options.progress_bar && !isPaused) supersized.progressBar();
					
					// Thumbnail Navigation
					if (base.options.thumbnail_navigation){
						
						// Next thumbnail clicked
						$('#nextthumb').click(function() {
					    	supersized.nextSlide();
					    });
					    
					    // Previous thumbnail clicked
					    $('#prevthumb').click(function() {
					    	supersized.prevSlide();
					    });
					    
					}
					
					// Navigation controls
					if (base.options.navigation){

					
						$('#navigation a').click(function(){  
	   						$(this).blur();  
	   						return false;  
	   					});
						
						// Next button clicked
					    $('#nextslide').click(function() {
					    	supersized.nextSlide();
					    });
					    
					    // Previous button clicked
					    $('#prevslide').click(function() {
					    	supersized.prevSlide();
					    });
						
					    // Pause/play element clicked
					    $(base.options.play_button).click(function() {
							supersized.playToggle();						    
					    });
					    
					}	// End navigation controls
					
				}	// End slideshow options
    		
    			// Keyboard Navigation
				if (base.options.keyboard_nav){
					
					$(document.documentElement).keyup(function (event) {
					
						if(inAnimation) return false;		// Abort if currently animating
						
						clearInterval(slideshow_interval);	// Stop slideshow, prevent buildup
						
						// Left Arrow or Down Arrow
						if ((event.keyCode == 37) || (event.keyCode == 40)) {
							supersized.prevSlide();
						
						// Right Arrow or Up Arrow
						} else if ((event.keyCode == 39) || (event.keyCode == 38)) {
							supersized.nextSlide();
						
						// Spacebar	
						} else if (event.keyCode == 32) {
							supersized.playToggle();
						}
					
					});
				}
				
				
				// Pause when hover on image
				if (base.options.slideshow && base.options.pause_hover){
					$(base.el).hover(function() {
					
						if(inAnimation) return false;		// Abort if currently animating
				   			
				   			if(!(isPaused) && base.options.navigation){
				   				if ($(base.options.play_button).attr('src')) $(base.options.play_button).attr("src", image_path + "pause.png"); 	// If image, swap to pause
				   				clearInterval(slideshow_interval);
				   				if (base.options.progress_bar) $('#progress-bar').stop().animate({left : -$(window).width()}, 0 );
				   			}
				   		
				   	}, function() {
							
						if(!(isPaused) && base.options.navigation){
							if ($(base.options.play_button).attr('src')) $(base.options.play_button).attr("src", image_path + "pause.png");	// If image, swap to active
							slideshow_interval = setInterval(supersized.nextSlide, base.options.slide_interval);
						}
						
				   	});
				}
				
				if (base.options.slide_links){
					// Slide marker clicked
					$('#slide-list li').click(function(){
					
						index = $('#slide-list li').index(this);
						targetSlide = index + 1;
						
						supersized.goTo(targetSlide);
						return false;
						
					});
				}
								
				if (base.options.thumb_links){
					// Thumb marker clicked
					$('li', base.options.thumb_list).click(function(){
					
						index = $('li', base.options.thumb_list).index(this);
						targetSlide = index + 1;
						
						supersized.goTo(targetSlide);
						return false;
						
					});
				}
				
				$('#tray-button').toggle(function(){
					$(base.options.thumb_tray).stop().animate({bottom : 0}, 300 );
					if ($('#tray-arrow').attr('src')) $('#tray-arrow').attr("src", image_path + "button-tray-down.png");
					return false;
				}, function() {
					$(base.options.thumb_tray).stop().animate({bottom : -$(base.options.thumb_tray).height()}, 300 );
					if ($('#tray-arrow').attr('src')) $('#tray-arrow').attr("src", image_path + "button-tray-up.png");
					return false;
				});	
						
				// Adjust image when browser is resized
				$(window).resize(function(){
					// Pause progress bar so images can refocus
					if (base.options.progress_bar)$('#progress-bar').stop().animate({left : -$(window).width()}, 0 );
					clearInterval(slideshow_interval);
					if (!isPaused){
						slideshow_interval = setInterval(supersized.nextSlide, base.options.slide_interval);
					}
					if (base.options.thumb_links){
						// Adjust thumbnail markers
						if ($(base.options.thumb_list).width() > $(base.options.thumb_tray).width()){
							$('#thumb-back, #thumb-forward').fadeIn('fast');
							$(base.options.thumb_list).stop().animate({'left':0}, 200);
							thumbPage = 0;
						}else{
							$('#thumb-back, #thumb-forward').fadeOut('fast');
						}
						thumbInterval = Math.floor($(base.options.thumb_tray).width() / $('> li', base.options.thumb_list).outerWidth(true)) * $('> li', base.options.thumb_list).outerWidth(true);
					}
					
					
		    		supersized.resizeNow();
				});
				
				// Account for loading in IE
				$(document).ready(function() {
					supersized.resizeNow();
				});
    		
    		},
    		
    		progressBar : function(){
    			$('#progress-bar').stop().animate({left : -$(window).width()}, 0 ).animate({ left:0 }, base.options.slide_interval);
    		},
    		
    		// Adjust image size
    		resizeNow : function(){
    			
				return base.$el.each(function() {
				
			  		//  Resize each image seperately
			  		$('img', base.el).each(function(){
			  			
						thisSlide = $(this);
						var ratio = (thisSlide.data('origHeight')/thisSlide.data('origWidth')).toFixed(2);	// Define image ratio
						//alert(thisSlide.data('origHeight') + ' x ' + thisSlide.data('origWidth'));
						
						// Gather browser size
						var browserwidth = $(window).width();
						var browserheight = $(window).height();
						var offset;
						
						/**Resize image to proper ratio**/
						
						if ((browserheight <= base.options.min_height) && (browserwidth <= base.options.min_width)){	// If window smaller than minimum width and height
						
							if ((browserheight/browserwidth) > ratio){
								base.options.fit_landscape && ratio <= 1 ? resizeWidth(true) : resizeHeight(true);	// If landscapes are set to fit
							} else {
								base.options.fit_portrait && ratio > 1 ? resizeHeight(true) : resizeWidth(true);		// If portraits are set to fit
							}
						
						} else if (browserwidth <= base.options.min_width){		// If window only smaller than minimum width
						
							if ((browserheight/browserwidth) > ratio){
								base.options.fit_landscape && ratio <= 1 ? resizeWidth(true) : resizeHeight();	// If landscapes are set to fit
							} else {
								base.options.fit_portrait && ratio > 1 ? resizeHeight() : resizeWidth(true);		// If portraits are set to fit
							}
							
						} else if (browserheight <= base.options.min_height){	// If window only smaller than minimum height
						
							if ((browserheight/browserwidth) > ratio){
								base.options.fit_landscape && ratio <= 1 ? resizeWidth() : resizeHeight(true);	// If landscapes are set to fit
							} else {
								base.options.fit_portrait && ratio > 1 ? resizeHeight(true) : resizeWidth();		// If portraits are set to fit
							}
						
						} else {	// If larger than minimums
							
							if ((browserheight/browserwidth) > ratio){
								base.options.fit_landscape && ratio <= 1 ? resizeWidth() : resizeHeight();	// If landscapes are set to fit
							} else {
								base.options.fit_portrait && ratio > 1 ? resizeHeight() : resizeWidth();		// If portraits are set to fit
							}
							
						}
						
						/**End Image Resize**/
						
						
						/**Resize Functions**/
						
						function resizeWidth(minimum){
							if (minimum){	// If minimum height needs to be considered
								if(thisSlide.width() < browserwidth || thisSlide.width() < base.options.min_width ){
									if (thisSlide.width() * ratio >= base.options.min_height){
										thisSlide.width(base.options.min_width);
							    		thisSlide.height(thisSlide.width() * ratio);
							    	}else{
							    		resizeHeight();
							    	}
							    }
							}else{
								if (base.options.min_height >= browserheight && !base.options.fit_landscape){	// If minimum height needs to be considered
									if (browserwidth * ratio >= base.options.min_height || (browserwidth * ratio >= base.options.min_height && ratio <= 1)){	// If resizing would push below minimum height or image is a landscape
										thisSlide.width(browserwidth);
										thisSlide.height(browserwidth * ratio);

									} else if (ratio > 1){		// Else the image is portrait
										thisSlide.height(base.options.min_height);
										thisSlide.width(thisSlide.height() / ratio);
									} else if (thisSlide.width() < browserwidth) {
										thisSlide.width(browserwidth);
							    		thisSlide.height(thisSlide.width() * ratio);
									}
								}else{	// Otherwise, resize as normal
									thisSlide.width(browserwidth);
									thisSlide.height(browserwidth * ratio);
								}
							}
						};
						
						function resizeHeight(minimum){
							if (minimum){	// If minimum height needs to be considered
								if(thisSlide.height() < browserheight){
									if (thisSlide.height() / ratio >= base.options.min_width){
										thisSlide.height(base.options.min_height);
										thisSlide.width(thisSlide.height() / ratio);
									}else{
										resizeWidth(true);
									}
								}
							}else{	// Otherwise, resized as normal
								if (base.options.min_width >= browserwidth){	// If minimum width needs to be considered
									if (browserheight / ratio >= base.options.min_width || ratio > 1){	// If resizing would push below minimum width or image is a portrait
										thisSlide.height(browserheight);
										thisSlide.width(browserheight / ratio);
									} else if (ratio <= 1){		// Else the image is landscape
										thisSlide.width(base.options.min_width);
							    		thisSlide.height(thisSlide.width() * ratio);
									}
								}else{	// Otherwise, resize as normal
									thisSlide.height(browserheight);
									thisSlide.width(browserheight / ratio);
								}
							}
						};
						
						/**End Resize Functions**/
						
						
						// Horizontally Center
						if (base.options.horizontal_center){
							$(this).css('left', (browserwidth - $(this).width())/2);
						}
						
						// Vertically Center
						if (base.options.vertical_center){
							$(this).css('top', (browserheight - $(this).height())/2);
						}
						
					});
					
					// Basic image drag and right click protection
					if (base.options.image_protect){
						
						$('img', base.el).bind("contextmenu",function(){
							return false;
						});
						$('img', base.el).bind("mousedown",function(){
							return false;
						});
					
					}
					
					return false;
					
				});
				
    		},
    		
    		/*
    			Slide Controls
    		*/
    		
    		nextSlide : function(){
    			
    			if(inAnimation) return false;		// Abort if currently animating
					else inAnimation = true;		// Otherwise set animation marker
			   
			    clearInterval(slideshow_interval);	// Stop slideshow
			    if (base.options.progress_bar)$('#progress-bar').stop().animate({left : -$(window).width()}, 0 );
			    
			    var slides = base.options.slides;	// Pull in slides array
				
				var currentslide = base.$el.find('.activeslide');		// Find active slide
				currentslide.removeClass('activeslide');				// Remove active class
				
			    if ( currentslide.length == 0 ) currentslide = base.$el.find('a:last');	// If end of set, note this is last slide
			    var nextslide = currentslide.next().length ? currentslide.next() : base.$el.find('a:first');
				var prevslide = nextslide.prev().length ? nextslide.prev() : base.$el.find('a:last');
				
				// Update previous slide
				prevslide.addClass('prevslide');
				
				// Get the slide number of new slide
				currentSlide + 1 == base.options.slides.length ? currentSlide = 0 : currentSlide++;
				
				// If hybrid mode is on drop quality for transition
				if (base.options.performance == 1) base.$el.removeClass('quality').addClass('speed');	
				
				/**** Image Loading ****/
				
				// Load next image
				loadSlide = false;
				
				currentSlide == base.options.slides.length - 1 ? loadSlide = 0 : loadSlide = currentSlide + 1;	// Determine next slide
				imageLink = (base.options.slides[loadSlide].url) ? "href='" + base.options.slides[loadSlide].url + "'" : "";	// If link exists, build it
				
				var img = $('<img src="'+base.options.slides[loadSlide].image+'"/>');
				img.appendTo(base.el).wrap('<a class="image-loading" ' + imageLink + linkTarget + '></a>').hide();
				
				img.load(function(){
					$('.image-loading').removeClass('image-loading');
					supersized._origDim($(this));
				});	// End Load
				
				// Update thumbnails (if enabled)
				if (base.options.thumbnail_navigation == 1){
				
					// Load previous thumbnail
					currentSlide - 1 < 0  ? prevThumb = base.options.slides.length - 1 : prevThumb = currentSlide - 1;
					$('#prevthumb').html($("<img/>").attr("src", base.options.slides[prevThumb].image));
				
					// Load next thumbnail
					nextThumb = loadSlide;
					$('#nextthumb').html($("<img/>").attr("src", base.options.slides[nextThumb].image));
					
				}
				
				currentslide.prev().remove(); // Remove Old Image
				
				/**** End Image Loading ****/
				
				// Update slide number
				if (base.options.slide_counter){
				    $('#slidecounter .slidenumber').html(currentSlide + 1);
				}
				
				//Update slide markers
				if (base.options.slide_links){
					$('.current-slide').removeClass('current-slide');
					$('#slide-list li').eq(currentSlide).addClass('current-slide');
				}
				
				if (base.options.thumb_links){
					$('.current-thumb').removeClass('current-thumb');
					$('li', base.options.thumb_list).eq(currentSlide).addClass('current-thumb');
					// If thumb out of view
					if ($(base.options.thumb_list).width() > $(base.options.thumb_tray).width()){
						if (currentSlide == 0){
							thumbPage = 0;
							$(base.options.thumb_list).stop().animate({'left': thumbPage}, {duration:500, easing:'easeOutExpo'});
						} else if ($('.current-thumb').offset().left - $(base.options.thumb_tray).offset().left >= thumbInterval){
	        				thumbPage = thumbPage - thumbInterval;
	        				$(base.options.thumb_list).stop().animate({'left': thumbPage}, {duration:500, easing:'easeOutExpo'});
						}
					}
				}
				
				// Update captions
			    if (base.options.slide_captions){
			    	(base.options.slides[currentSlide].title) ? $('#slidecaption').html(base.options.slides[currentSlide].title) : $('#slidecaption').html('');
			    }
			    
			    nextslide.hide().addClass('activeslide');	// Update active slide
			    
		    	switch(base.options.transition){
		    		
		    		case 0: case 'none':	// No transition
		    		    nextslide.show(); inAnimation = false;
		    		    break;
		    		case 1: case 'fade':	// Fade
		    		    nextslide.fadeTo(base.options.transition_speed, 1, function(){ supersized.afterAnimation(); });
		    		    break;
		    		case 2: case 'slideTop':	// Slide Top
		    		    nextslide.animate({top : -$(window).height()}, 0 ).show().animate({ top:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
		    		    break;
		    		case 3: case 'slideRight':	// Slide Right
		    			nextslide.animate({left : $(window).width()}, 0 ).show().animate({ left:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
		    			break;
		    		case 4: case 'slideBottom': // Slide Bottom
		    			nextslide.animate({top : $(window).height()}, 0 ).show().animate({ top:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
		    			break;
		    		case 5: case 'slideLeft':  // Slide Left
		    			nextslide.animate({left : -$(window).width()}, 0 ).show().animate({ left:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
		    			break;
		    		case 6: case 'carouselRight':	// Carousel Right
		    			nextslide.animate({left : $(window).width()}, 0 ).show().animate({ left:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
						currentslide.animate({ left: -$(window).width() }, base.options.transition_speed );
		    			break;
		    		case 7: case 'carouselLeft':   // Carousel Left
		    			nextslide.animate({left : -$(window).width()}, 0 ).show().animate({ left:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
						currentslide.animate({ left: $(window).width() }, base.options.transition_speed );
		    			break;
		    	};
			    return false;	
    		},
    		
    		prevSlide : function(){
    		
    			if(inAnimation) return false;		// Abort if currently animating
					else inAnimation = true;		// Otherwise set animation marker
				
				clearInterval(slideshow_interval);	// Stop slideshow
			    if (base.options.progress_bar)$('#progress-bar').stop().animate({left : -$(window).width()}, 0 );
				
				var slides = base.options.slides;	// Pull in slides array
		
				var currentslide = base.$el.find('.activeslide');		// Find active slide
				currentslide.removeClass('activeslide');				// Remove active class
				
			    if ( currentslide.length == 0 ) currentslide = base.$el.find('a:first');	// If end of set, note this is first slide
			    var nextslide =  currentslide.prev().length ? currentslide.prev() : base.$el.find('a:last');
				var prevslide =  nextslide.next().length ? nextslide.next() : base.$el.find('a:first');
				
				// Update previous slide
				prevslide.addClass('prevslide');
						
				// Get current slide number
				currentSlide == 0 ?  currentSlide = base.options.slides.length - 1 : currentSlide-- ;
				
				// If hybrid mode is on drop quality for transition
				if (base.options.performance == 1) base.$el.removeClass('quality').addClass('speed');	
						
				/**** Image Loading ****/
				
				// Load next image
				loadSlide = false;
				currentSlide - 1 < 0  ? loadSlide = base.options.slides.length - 1 : loadSlide = currentSlide - 1;	// Determine next slide
				imageLink = (base.options.slides[loadSlide].url) ? "href='" + base.options.slides[loadSlide].url + "'" : "";	// If link exists, build it
				
				var img = $('<img src="'+base.options.slides[loadSlide].image+'"/>');
				img.prependTo(base.el).wrap('<a class="image-loading" ' + imageLink + linkTarget + '></a>').hide();
				
				img.load(function(){
					$('.image-loading').removeClass('image-loading');
					supersized._origDim($(this));
				});	// End Load
				
				// Update thumbnails (if enabled)
				if (base.options.thumbnail_navigation == 1){
				
					// Load previous thumbnail
					prevThumb = loadSlide;
					$('#prevthumb').html($("<img/>").attr("src", base.options.slides[prevThumb].image));
					
					// Load next thumbnail
					currentSlide == base.options.slides.length - 1 ? nextThumb = 0 : nextThumb = currentSlide + 1;
					$('#nextthumb').html($("<img/>").attr("src", base.options.slides[nextThumb].image));
				}
				
				currentslide.next().remove(); // Remove Old Image
				
				/**** End Image Loading ****/
				
				// Update slide counter
				if (base.options.slide_counter){
				    $('#slidecounter .slidenumber').html(currentSlide + 1);
				}
				
				//Update slide markers
				if (base.options.slide_links){
					$('.current-slide').removeClass('current-slide');
					$('#slide-list li').eq(currentSlide).addClass('current-slide');
				}
				
				if (base.options.thumb_links){
					$('.current-thumb').removeClass('current-thumb');
					$('li', base.options.thumb_list).eq(currentSlide).addClass('current-thumb');
					// If thumb out of view
					if ($(base.options.thumb_list).width() > $(base.options.thumb_tray).width()){
						if (currentSlide == base.options.slides.length - 1){
							thumbPage = Math.floor($(base.options.thumb_list).width() / thumbInterval) * -thumbInterval;
							if ($(base.options.thumb_list).width() <= -thumbPage) thumbPage = thumbPage + thumbInterval;
							$(base.options.thumb_list).stop().animate({'left': thumbPage}, {duration:500, easing:'easeOutExpo'});
						} else if ($('.current-thumb').offset().left - $(base.options.thumb_tray).offset().left < 0){
							if (thumbPage + thumbInterval > 0) return false;
	        				thumbPage = thumbPage + thumbInterval;
	        				$(base.options.thumb_list).stop().animate({'left': thumbPage}, {duration:500, easing:'easeOutExpo'});
						};
					}
				}
				
				// Update captions
			    if (base.options.slide_captions){
			    	(base.options.slides[currentSlide].title) ? $('#slidecaption').html(base.options.slides[currentSlide].title) : $('#slidecaption').html('');
			    }
				
			    nextslide.hide().addClass('activeslide');	// Update active slide
			    
			    switch(base.options.transition){
			    		
		    		case 0: case 'none':	// No transition
		    		    nextslide.show(); inAnimation = false; supersized.afterAnimation();
		    		    break;
		    		case 1: case 'fade':	// Fade
		    		    nextslide.fadeTo(base.options.transition_speed, 1, function(){ supersized.afterAnimation(); });
		    		    break;
		    		case 2: case 'slideTop':	// Slide Top (reverse)
		    		    nextslide.animate({top : $(window).height()}, 0 ).show().animate({ top:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
		    		    break;
		    		case 3: case 'slideRight':	// Slide Right (reverse)
		    			nextslide.animate({left : -$(window).width()}, 0 ).show().animate({ left:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
		    			break;
		    		case 4: case 'slideBottom': // Slide Bottom (reverse)
		    			nextslide.animate({top : -$(window).height()}, 0 ).show().animate({ top:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
		    			break;
		    		case 5: case 'slideLeft':  // Slide Left (reverse)
		    			nextslide.animate({left : $(window).width()}, 0 ).show().animate({ left:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
		    			break;
		    		case 6: case 'carouselRight':	// Carousel Right (reverse)
		    			nextslide.animate({left : -$(window).width()}, 0 ).show().animate({ left:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
						currentslide.animate({ left: $(window).width() }, base.options.transition_speed );
		    			break;
		    		case 7: case 'carouselLeft':   // Carousel Left (reverse)
		    			nextslide.animate({left : $(window).width()}, 0 ).show().animate({ left:0 }, base.options.transition_speed, function(){ supersized.afterAnimation(); });
						currentslide.animate({ left: -$(window).width() }, base.options.transition_speed );
		    			break;
		    	};
			    return false;	
    		},
    		
    		playToggle : function(){
    		
    			if(inAnimation) return false;		// Abort if currently animating
							
				if (isPaused){
					
					if ($(base.options.play_button).attr('src')) $(base.options.play_button).attr("src", image_path + "pause.png");	// If image, swap to pause
					
					// Resume slideshow
					isPaused = false;
					if (base.options.progress_bar && !isPaused) supersized.progressBar();
		        	slideshow_interval = setInterval(supersized.nextSlide, base.options.slide_interval);
		        	  
	        	}else{
	        		
	        		if ($(base.options.play_button).attr('src')) $(base.options.play_button).attr("src", image_path + "play.png");	// If image, swap to play
	        		
	        		// Stop slideshow
	        		clearInterval(slideshow_interval);	
	        		isPaused = true;
	        		if (base.options.progress_bar && isPaused)$('#progress-bar').stop().animate({left : -$(window).width()}, 0 );
	       		
	       		}
			    
			    return false;
    		
    		},
    		
    		goTo : function(targetSlide){
    			
    			if(inAnimation) return false;		// Abort if currently animating
    			
    			var totalSlides = base.options.slides.length;
    			updateImages = true;
    			
    			// If target outside range
    			if(targetSlide < 0){
    				targetSlide = totalSlides;
    			}else if(targetSlide > totalSlides){
    				targetSlide = 1;
    			}
				targetSlide = totalSlides - targetSlide + 1;
				
				clearInterval(slideshow_interval);	// Stop slideshow, prevent buildup
				if (base.options.progress_bar)$('#progress-bar').stop().animate({left : -$(window).width()}, 0 );
				
				if (currentSlide == totalSlides - targetSlide){
					if(!(isPaused)){
						slideshow_interval = setInterval(supersized.nextSlide, base.options.slide_interval);
						supersized.progressBar();
					} 
					return false;
				}
				
				// If ahead of current position
				if(totalSlides - targetSlide > currentSlide ){
					
					// Adjust for new next slide
					currentSlide = totalSlides-targetSlide-1;
					supersized._placeSlide('next');
					
				//Otherwise it's before current position
				}else if(totalSlides - targetSlide < currentSlide){
					
					// Adjust for new prev slide
					currentSlide = totalSlides-targetSlide+1;
				    supersized._placeSlide('prev');
				    
				}
				
				// set active markers
				if (base.options.slide_links){
					$('#slide-list .current-slide').removeClass('current-slide');
					$('#slide-list li').eq((totalSlides-targetSlide)).addClass('current-slide');
				}
				
				if (base.options.thumb_links){
					$('#thumb-list .current-thumb').removeClass('current-thumb');
					$('#thumb-list li').eq((totalSlides-targetSlide)).addClass('current-thumb');
				}
				
    		},
    		
    		_placeSlide : function(place){
    			
    			if (place == 'next'){
    			
    				// Remove slide to be replaced
					$('.activeslide').next().remove();
					
					loadSlide = false;
					currentSlide == base.options.slides.length - 1 ? loadSlide = 0 : loadSlide = currentSlide + 1;	// Determine next slide
					imageLink = (base.options.slides[loadSlide].url) ? "href='" + base.options.slides[loadSlide].url + "'" : "";	// If link exists, build it
					
					var img = $('<img src="'+base.options.slides[loadSlide].image+'"/>');
					img.appendTo(base.el).wrap('<a class="image-loading" ' + imageLink + linkTarget + '></a>').hide();
					
					img.load(function(){
						$('.image-loading').removeClass('image-loading');
						supersized._origDim($(this));
						supersized.resizeNow();
						if (updateImages) supersized.nextSlide();
					});	// End Load
					
    			}else if (place == 'prev'){
    			
    				// Remove slide to be replaced
					$('.activeslide').prev().remove();
					
					loadSlide = false;
					currentSlide - 1 < 0  ? loadSlide = base.options.slides.length - 1 : loadSlide = currentSlide - 1;	// Determine next slide
					imageLink = (base.options.slides[loadSlide].url) ? "href='" + base.options.slides[loadSlide].url + "'" : "";	// If link exists, build it
					
    				var img = $('<img src="'+base.options.slides[loadSlide].image+'"/>');
					img.prependTo(base.el).wrap('<a class="image-loading" ' + imageLink + linkTarget + '></a>').hide();
					
					img.load(function(){
						$('.image-loading').removeClass('image-loading');
						supersized._origDim($(this));
				    	supersized.resizeNow();
				    	if (updateImages) supersized.prevSlide();
					});	// End Load
    			}
    			
    		},
    		
    		_origDim : function(targetSlide){
				targetSlide.show().data('origWidth', targetSlide.width()).data('origHeight', targetSlide.height());
    		},
    		
    		afterAnimation : function(){ 
					
				// If hybrid mode is on swap back to higher image quality
				if (base.options.performance == 1){
			    	base.$el.removeClass('speed').addClass('quality');
				}
				
				if (updateImages){
					updateImages = false;
					supersized._placeSlide('next');
					supersized._placeSlide('prev');
				}
							
				supersized.resizeNow();
				// Resume slideshow
				if (!isPaused){
					slideshow_interval = setInterval(supersized.nextSlide, base.options.slide_interval);
					if (base.options.progress_bar && !isPaused) supersized.progressBar();	//  Start slide timer
				}
				inAnimation = false;
				return false;
    		
    		}
    		
    	}
		
        // Run initializer
        base.init();
	};
	
	$.supersized.defaultOptions = {
    
    	// Functionality
		slideshow               :   1,		// Slideshow on/off
		autoplay				:	1,		// Slideshow starts playing automatically
		start_slide             :   1,		// Start slide (0 is random)
		random					: 	0,		// Randomize slide order (Ignores start slide)
		slide_interval          :   5000,	// Length between transitions
		transition              :   1, 		// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
		transition_speed		:	750,	// Speed of transition
		new_window				:	1,		// Image links open in new window/tab
		pause_hover             :   0,		// Pause slideshow on hover
		keyboard_nav            :   1,		// Keyboard navigation on/off
		performance				:	1,		// 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed //  (Only works for Firefox/IE, not Webkit)
		image_protect			:	1,		// Disables image dragging and right click with Javascript
		image_path				:	'img/', // Default image path
											   
		// Size & Position					   
		min_width		        :   0,		// Min width allowed (in pixels)
		min_height		        :   0,		// Min height allowed (in pixels)
		vertical_center         :   1,		// Vertically center background
		horizontal_center       :   1,		// Horizontally center background
		fit_portrait         	:   0,		// Portrait images will not exceed browser height
		fit_landscape			:   0,		// Landscape images will not exceed browser width  
											   
		// Components
		slide_links				:	1,		// Individual links for each slide
		thumb_links				:	1,		// Individual thumb links for each slide					   
		progress_bar			:	1,		// Timer for each slide
		navigation              :   1,		// Slideshow controls on/off
		thumbnail_navigation    :   0,		// Thumbnail navigation
		slide_counter           :   1,		// Display slide numbers
		slide_captions          :   1,		// Slide caption (Pull from "title" in slides array)
		
		// General Elements
    	play_button				:	'#pauseplay',	//Play/Pause button
    	next_button				:	'#nextslide',	//Next button
    	prev_button				:	'#prevslide',	//Prev button
    	thumb_tray				:	'#thumb-tray',	//Thumbnail tray
    	thumb_list				:	'#thumb-list'	//Thumbnail list
    	
    };
    
    $.fn.supersized = function(options){
        return this.each(function(){
            (new $.supersized(options));
        });
    };
		
})(jQuery);
