
jQuery(function($){
$.supersized({

//Functionality
slideshow               :    1,        // Slideshow on/off
autoplay                :    1,        // Slideshow starts playing automatically
start_slide             :    1,        // Start slide (0 is random)
random                  :    0,        // Randomize slide order (Ignores start slide)
 
slide_interval          :    3000,     // Length between transitions
 
transition              :    1,        // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
 
transition_speed        :    1000,      // Speed of transition
 
performance             :    1,        // 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
image_path              :    'images/',   // Default image path
slides                  :    [         // Slideshow Images

{image : 'slider/images/home1.jpg', title : 'Slide', url : ''},  
            {image : 'slider/images/home2.jpg', title : 'Slide', url : ''}, 
            {image : 'slider/images/home3.jpg', title : 'Slide', url : ''},
            {image : 'slider/images/home4.jpg', title : 'Slide', url : ''},			
            {image : 'slider/images/home5.jpg', title : 'Slide', url : ''}

]

}); 

 
});