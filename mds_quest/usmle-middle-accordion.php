<?php  $pageName= substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); ?>

<div class="pg-medical-main tab-hide">
  <div class="pg-heading"><span></span>Usmle Edge Courses</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <h5> Usmle Edge Courses</h5>
      <div class="coures-list-box-content">
        <ul class="course-new-list">
          <li><a href="usml-step1.php" title="USMLE STEP-1 (TS)"><span class="sub-arrow"></span>USMLE STEP-1 (TS)</a></li>
          <li><a href="usml-step2.php" title="USMLE STEP-2 (TS)"><span class="sub-arrow"></span>USMLE STEP-2 (TS)</a></li>
          <li><a href="usmle-egde-combo.php" title="USMLE Combo"><span class="sub-arrow"></span>USMLE Combo</a></li>
          <li><a href="usmle-edge-full-package.php" title="USMLE Full Package"><span class="sub-arrow"></span>USMLE Full Package</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
