<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, DAMS MDS Quest</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 3;
$courseNav_id = 9;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="test-series">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="dams-mds-quest.php" title="MDS Quest">MDS Quest</a></li>
          <li><a title="Postal Test Series" class="active-link">Postal Test Series</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>MDS Postal Test Series - 2016 <span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main">
              <div class="test-series-content paddin-zero">
			  <h2>Additional benefits of being a Damsonian</h2>
			  <ul class="some-points">
			  <li><span class="blue_arrow postcourse"></span> <span><p>1.Exclusive DAMS "MDS QUEST-16" club on Facebook which includes all subject faculties for doubts clarification and regular subject updates</p></span></li>
			  <li><span class="blue_arrow postcourse"></span> <span><p>2.Exclusive discussions of AIIMS SrShip , AIPG, PGI, & AIIMS Medical & Dental Exam discussions limited to only DAMS MDS QUEST Students.</p></span></li>
			  <li><span class="blue_arrow postcourse"></span> <span><p>3."DAMS CLOUD" a unique feature and first of its kind in India where you can watch previous discussion videos along with more tests and updates</p></span></li>
			  <li><span class="blue_arrow postcourse"></span> <span><p>4.Pan India reach of over 100 plus centres to give you correct idea of your preparation. Soon we will be introducing more new things to provide the much needed competitive edge to our students</p></span></li>
			  </ul>
                <ul class="duration-content">
                  <li>
                    <label>Price :</label>
                    <span>8015/-(including s.tax)</span> </li>
               <!--   <li>
                    <label>Duration :</label>
                    <span>1 Year</span></li>
                  <li>
                    <label>Total no of tests :</label>
                    <span>Approximately 40 Tests</span></li>
                  <li>
                    <label>Total no. of questions per Test :</label>
                    <span class="in_grand_inline">300 in GRAND TESTS &amp; 200 IN SUBJECTWISE TESTS, WITH SPECIAL MOCK TEST FOR AIIMS, AIPG (NBE), PGI , NIMHANS, STATE PGs</span></li>-->
                </ul>
                <!--<ul class="some-points">
                  <li><span class="blue_arrow"></span> <span>Authentic explanations from our highly prestigious faculty with references.</span></li>
                  <li><span class="blue_arrow"></span> <span>Questions are based on AIPG (NBE PATTERN) AIPG(NBE/NEET) Pattern PG, AIIMS, PGI DNB, UPSC exam pattern.</span></li>
                  <li><span class="blue_arrow"></span> <span>We have various Test Centres for more competitive environment &amp; comfortable approach.</span></li>
                  <li><span class="blue_arrow"></span> <span>Fully explained solved answers key of all Questions will be give immediately after completion</span></li>
                  <li><span class="blue_arrow"></span> <span>All Students enrolled in between the session will get all previous Test papers.</span></li>
                  <li><span class="blue_arrow"></span> <span>All India Ranking with detailed marks will be provided at our website: <a href="https://www.damsdelhi.com" title="PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG">www.damsdelhi.com</a></span></li>
                </ul>-->
                <!--<p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern.  With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner for AIPG(NBE/NEET) Pattern examinations. We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI,  UPSC, DNB &amp;  MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.<br>
                  <br>
                </p>-->
                <div class="franchisee-box paddin-zero">
                  <p><span class="price_font">Dental Carrer Counselling for MDS :</span> 09999158131</p>
                </div>
              </div>
               <ul class="idTabs mini-tab">
                <li><a href="#test1" class="mds-tab">Test Series - I</a></li>
                <li><a href="#test2" class="mds-tab">Test Series - II</a></li>
                <li><a href="#test3" class="mds-tab">Test Series - III</a></li>
                <li><a href="#test4" class="mds-tab">Test Series - IV</a></li>
                <li><a href="#test5" class="mds-tab">Test Series - V</a></li>
                <li style="display:none;"><a href="#test6" class="mds-tab">Revision Test Series</a></li>
                <li><a href="#test6" class="mds-tab">Test Series - VI</a></li>
                <!--<li><a href="#test7" class="mds-tab">Test Series - VII</a></li>--->
              </ul>
              <div id="test1">
                <div class="test-tab-content">
                  <a href="https://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>22<sup>nd </sup>Nov, 2015</label>
                        <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                      <li>
                        <label>29<sup>th </sup>Nov, 2015</label>
                        <span>Human Anatomy</span></li>
                      <li>
                        <label>06<sup>th</sup>Dec, 2015</label>
                        <span>Dental Anatomy and Dental Histology</span></li>
                      <li>
                        <label>13<sup>th </sup>Dec, 2015</label>
                        <span>Physiology +Biochemistry</span></li>
                      <li class="yellow_bg">
                        <label style="font-weight:600;">20<sup>th </sup>Dec, 2015</label>
                        <span style="font-weight:600;">AIPG Mock</span></li>
                      <li>
                        <label>27<sup>th </sup>Dec, 2015</label>
                        <span>Oral pathology(OMDR)  1</span></li>
                      <li>
                        <label>03<sup>rd </sup>Jan, 2016</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                      <li class="green_bg">
                        <label style="font-weight:600;">10<sup>th </sup>Jan, 2016</label>
                        <span style="font-weight:600;">Tenative date for AIPG</span></li>
                      <li>
                        <label>17<sup>th </sup>Jan, 2016</label>
                        <span>General medicine+ General  Surgery</span></li>
                      <li>
                        <label>24<sup>th </sup>Jan, 2016</label>
                        <span>Pedodontics</span></li>
                      <li>
                        <label>31<sup>st </sup>Jan, 2016</label>
                        <span>General Pathology + Microbiology</span></li>
                      <li>
                        <label>07<sup>th </sup>Feb, 2016</label>
                        <span>Oral Surgery 1</span></li>
                      <li>
                        <label>14<sup>th </sup>Feb, 2016</label>
                        <span>Oral Surgery 2</span></li>
                      <li class="yellow_bg">
                        <label style="font-weight:600;">21<sup>st </sup>Feb, 2016</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      <li>
                        <label>28<sup>th </sup>Feb, 2016</label>
                        <span>Pharmacology</span></li>
                      <li>
                        <label>06<sup>th </sup>Mar, 2016</label>
                        <span>Prosthodontics  1 and related Dental Materials</span></li>
                      <li>
                        <label >13<sup>th </sup>Mar, 2016</label>
                        <span >Prosthodontics 2 and related  Dental Materials</span></li>
                      <li  class="yellow_bg">
                        <label style="font-weight:600;">20<sup>th </sup>Mar, 2016</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Mar, 2016</label>
                        <span>Community Dentistry</span></li>
                      <li>
                        <label>03<sup>rd </sup>Apr, 2016</label>
                        <span>Periodontology</span></li>
                      <li>
                        <label>10<sup>th </sup>Apr, 2016</label>
                        <span>Orthodontics and Related Dental Materials</span></li>
                      <li class="yellow_bg">
                        <label style="font-weight:600;">17<sup>th </sup>Apr, 2016</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      <li class="light_green_bg">
                        <label style="font-weight:600;">24<sup>th </sup>Apr, 2016</label>
                        <span style="font-weight:600;">AIIMS Mock</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test2">
                <div class="test-tab-content">
                  <a href="https://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                        <li><label>27<sup>th </sup>Dec, 2015</label> <span>Oral pathology(OMDR) 1</span></li>
                        <li><label>03<sup>rd </sup>Jan, 2016</label> <span>Oral Pathology (OMDR) 2</span></li>
                        <li class="green_bg" style="font-weight:600;"><label >10<sup>th </sup>Jan, 2016</label> <span >Tenative date for AIPG</span></li>
                        <li><label>17<sup>th </sup>Jan, 2016</label> <span>General medicine+ General  Surgery</span></li>
                        <li><label>24<sup>th </sup>Jan, 2016</label> <span>Pedodontics </span></li>
                        <li><label>31<sup>st </sup>Jan, 2016</label> <span>General Pathology + Microbiology</span></li>
                        <li><label>07<sup>th </sup>Feb, 2016</label> <span>Oral Surgery 1</span></li>
                        <li ><label>14<sup>th </sup>Feb, 2016</label><span>Oral Surgery 2</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>21<sup>st </sup>Feb, 2016</label> <span> Grand Test</span></li>
                        <li><label>28<sup>th </sup>Mar, 2016</label> <span>Pharmacology</span></li>
                        <li><label>06<sup>th </sup>Mar, 2016</label> <span>Prosthodontics  1 and related Dental Materials</span></li>
                        <li ><label>13<sup>th </sup>Mar, 2016</label> <span>Prosthodontics 2 and related  Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>21<sup>st </sup>Mar, 2016</label> <span>Grand Test</span></li>
                        <li><label>27<sup>th </sup>Mar, 2016</label> <span>Community Dentistry</span></li>
                         <li><label>03<sup>rd </sup>Apr, 2016</label> <span>Periodontology</span></li>
                        <li><label>10<sup>th </sup>Apr, 2016</label> <span>Orthodontics and Related Dental Materials</span></li>
                        <li class="yellow_bg"  style="font-weight:600;"><label>17<sup>th </sup>Apr, 2016</label> <span>Grand Test</span></li>
                        <li class="light_green_bg"  style="font-weight:600;"><label>24<sup>th </sup>Apr, 2016</label> <span>AIIMS Mock</span></li>
                        <li><label>01<sup>st </sup>May, 2016</label> <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                        <li class="green_bg"  style="font-weight:600;"><label>08<sup>th </sup>May, 2016</label> <span>Tentative Date for AIIMS</span></li>
                        <li><label>15<sup>th </sup>May, 2016</label> <span>Human Anatomy</span></li>
                        <li><label>22<sup>nd </sup>May, 2016</label> <span>Dental Anatomy and Dental Histology</span></li>
                        <li><label>29<sup>st </sup>May, 2016</label> <span>Physiology +Biochemistry</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test3">
                <div class="test-tab-content">
                  <a href="https://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                        <li><label>07<sup>th </sup>Feb, 2016</label> <span>Oral Surgery-1</span></li>
                        <li ><label>14<sup>th </sup>Feb, 2016</label> <span>Oral Surgery 2</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>21<sup>st </sup>Feb, 2016</label> <span>Grand Test</span></li>
                        <li ><label>28<sup>th </sup>Feb, 2016</label> <span>Pharmacology</span></li>
                        <li><label>06<sup>th </sup>Mar, 2016</label> <span>Prosthodontics  1 and related Dental Materials</span></li>
                        <li><label>13<sup>th </sup>Mar, 2016</label> <span>Prosthodontics  2 and related Dental Materials</span></li>
                        <li class="yellow_bg"  style="font-weight:600;"><label>20<sup>th </sup>Mar, 2016</label> <span>Grand Test</span></li>
                        <li><label>27<sup>th </sup>Mar, 2016</label> <span>Community Dentistry</span></li>
                       
                        <li><label>03<sup>rd </sup>Apr, 2016</label> <span>Periodontology</span></li>
                        <li><label>10<sup>th </sup>Apr, 2016</label> <span>Orthodontics and Related Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>17<sup>th </sup>Apr, 2016</label> <span>Grand Test</span></li>
                        <li class="light_green_bg" style="font-weight:600;"><label>24<sup>th </sup>Apr, 2016</label> <span>AIIMS Mock</span></li>
                        <li><label>01<sup>st </sup>May, 2016</label> <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                        <li class="green_bg"  style="font-weight:600;"><label>08<sup>th </sup>May, 2016</label> <span>Tentative Date for AIIMS</span></li>
                        <li><label>15<sup>th </sup>May, 2016</label> <span>Human Anatomy</span></li>
                        <li><label>22<sup>nd </sup>May, 2016</label> <span>Dental Anatomy and Dental Histology</span></li>
                        <li><label>29<sup>th </sup>May, 2016</label> <span>Physiology +Biochemistry</span></li>
                        <li><label>05<sup>th </sup>Jun, 2016</label> <span>Oral pathology(OMDR)  1</span></li>
                        <li><label>12<sup>th </sup>Jun, 2016</label> <span>Oral Pathology (OMDR) 2</span></li>
                        <li class="yellow_bg"  style="font-weight:600;"><label>19<sup>th </sup>Jun, 2016</label> <span>Grand Test</span></li>
                        <li><label>26<sup>th </sup>Jun, 2016</label> <span>General medicine+ General  Surgery</span></li>
                        <li><label>03<sup>rd </sup>Jul, 2016</label> <span>Physiology +Biochemistry</span></li>
                        <li><label>10<sup>th </sup>Jul, 2016</label> <span>General Pathology + Microbiology</span></li>
                        <li class="yellow_bg"  style="font-weight:600;"><label>17<sup>th </sup>Jul, 2016</label> <span>Grand Test</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test4">
                <div class="test-tab-content">
                  <a href="https://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                        <li><label>06<sup>th </sup>Mar, 2016</label> <span>Prosthodontics  1 and related Dental Materials</span></li>
                        <li ><label>13<sup>th </sup>Mar, 2016</label> <span>Prosthodontics 2 and related  Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>20<sup>th </sup>Mar, 2016</label> <span>Grand Test</span></li>
                        <li><label>27<sup>th </sup>Mar, 2016</label> <span>Community Dentistry</span></li>
                        <li><label>03<sup>rd </sup>Apr, 2016</label> <span>Periodontology</span></li>
                        <li><label>10<sup>th </sup>Apr, 2016</label> <span>Orthodontics and Related Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>17<sup>th </sup>Apr, 2016</label> <span>Grand Test</span></li>
                        <li class="light_green_bg" style="font-weight:600;"><label>24<sup>th </sup>Apr, 2016</label> <span>AIIMS Mock</span></li>
                        <li><label>01<sup>st </sup>May, 2016</label> <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                        <li class="green_bg"  style="font-weight:600;"><label>08<sup>th </sup>May, 2016</label> <span>Tentative Date for AIIMS</span></li>
                        <li><label>15<sup>th </sup>May, 2016</label> <span>Human Anatomy</span></li>
                        <li><label>22<sup>th </sup>May, 2016</label> <span>Dental Anatomy and Dental Histology</span></li>
                        <li><label>29<sup>th </sup>May, 2016</label> <span>Physiology +Biochemistry</span></li>
                        <li><label>05<sup>th </sup>Jun, 2016</label> <span>Oral pathology(OMDR)  1</span></li>
                        <li><label>12<sup>th </sup>Jun, 2016</label> <span>Oral Pathology (OMDR) 2</span></li>
                        <li class="yellow_bg"  style="font-weight:600;"><label>19<sup>th </sup>Jun, 2016</label> <span>Grand Test</span></li>
                        <li><label>26<sup>th </sup>Jun, 2016</label> <span>General medicine+ General  Surgery</span></li>
                        <li><label>03<sup>rd </sup>Jul, 2016</label> <span>Pedodontics</span></li>
                        <li><label>10<sup>th </sup>Jul, 2016</label> <span>General Pathology + Microbiology</span></li>
                        <li class="yellow_bg"  style="font-weight:600;"><label>17<sup>th </sup>Jul, 2016</label> <span>Grand Test</span></li>
                        <li><label>24<sup>th </sup>Jul, 2016</label> <span>Oral Surgery-1</span></li>
                        <li><label>31<sup>st </sup>Jul, 2016</label> <span>Oral Surgery 2</span></li>
                        <li><label>07<sup>th </sup>Aug, 2016</label> <span>Pharmacology</span></li>                      
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test5">
                <div class="test-tab-content">
                  <a href="https://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                        <li><label>03<sup>rd </sup>Apr, 2016</label> <span>Periodontology</span></li>
                        <li><label>10<sup>th </sup>Apr, 2016</label> <span>Orthodontics and Related Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>17<sup>th </sup>Apr, 2016</label> <span>Grand Test</span></li>
                        <li class="light_green_bg" style="font-weight:600;"><label>24<sup>th </sup>Apr, 2016</label> <span>AIIMS Mock</span></li>
                        <li><label>01<sup>st </sup>May, 2016</label> <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                        <li class="green_bg" style="font-weight:600;"><label>08<sup>th </sup>May, 2016</label> <span>Tentative Date for AIIMS</span></li>
                        <li><label>15<sup>th </sup>May, 2016</label> <span>Human Anatomy </span></li>
                        <li><label>22<sup>nd </sup>May, 2016</label> <span>Dental Anatomy and Dental Histology</span></li>
                        <li><label>29<sup>th </sup>May, 2016</label> <span>Physiology +Biochemistry</span></li>
                        <li><label>05<sup>th </sup>Jun, 2016</label> <span>Oral pathology(OMDR)  1</span></li>
                        <li><label>12<sup>th </sup>Jun, 2016</label> <span>Oral Pathology (OMDR) 2</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>19<sup>th </sup>Jun, 2016</label> <span>Grand Test</span></li>
                        <li><label>26<sup>th </sup>Jun, 2016</label> <span>General medicine+ General  Surgery</span></li>
                        <li><label>03<sup>rd </sup>Jul, 2016</label> <span>Pedodontics</span></li>
                        <li><label>10<sup>th </sup>Jul, 2016</label> <span>General Pathology + Microbiology</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>17<sup>th </sup>Jul, 2016</label> <span>Grand Test</span></li>
                        <li><label>24<sup>th </sup>Jul, 2016</label> <span>Oral Surgery-1</span></li>
                        <li><label>31<sup>st </sup>Jul, 2016</label> <span>Oral Surgery 2</span></li>
                        <li><label>07<sup>th </sup>Aug, 2016</label> <span>Pharmacology</span></li>
                        <li ><label>14<sup>th </sup>Aug, 2016</label> <span>Prosthodontics  1 and related Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>21<sup>st </sup>Aug, 2016</label> <span>Grand Test </span></li>
                        <li><label>28<sup>th </sup>Aug, 2016</label> <span>Prosthodontics 2 and related  Dental Materials</span></li>
                        <li><label>04<sup>th </sup>Sep, 2016</label> <span>Community Dentistry</span></li
                    ></ul>
                  </div>
                </div>
              </div>
              <div id="test6">
                <div class="test-tab-content">
                  <a href="https://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>15<sup>th </sup>May, 2016</label>
                        <span>Human Anatomy</span></li>
                      <li>
                        <label>22<sup>nd </sup>May, 2016</label>
                        <span>Dental Anatomy and Dental Histology</span></li>
                      <li>
                        <label>29<sup>th </sup>May, 2016</label>
                        <span>Physiology +Biochemistry</span></li>
                      <li>
                        <label>05<sup>th </sup>Jun, 2016</label>
                        <span>Oral pathology(OMDR)  1</span></li>
                      <li>
                        <label>12<sup>th </sup>Jun, 2016</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                      <li class="yellow_bg" style="font-weight:600;">
                        <label>19<sup>th </sup>Jun, 2016</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>26<sup>th </sup>Jun, 2016</label>
                        <span>General medicine+ General Surgery</span></li>
                      <li>
                        <label>03<sup>rd </sup>Jul, 2016</label>
                        <span>Pedodontics</span></li>
                      <li>
                        <label>10<sup>th </sup>Jul, 2016</label>
                        <span>General Pathology + Microbiology</span></li>
                      <li class="yellow_bg"style="font-weight:600;">
                        <label>17<sup>th </sup>Jul, 2016</label>
                        <span>Grand Test </span></li>
                      <li>
                        <label>24<sup>th </sup>Jul, 2016</label>
                        <span>Oral Surgery-1</span></li>
                      <li>
                        <label>31<sup>st </sup>Jul, 2016</label>
                        <span>Oral Surgery 2</span></li>
                     <li>
                        <label>07<sup>th </sup>Aug, 2016</label>
                        <span>Pharmacology</span></li>
                        
                    <li class="yellow_bg" style="font-weight:600;">
                    <label>14<sup>th </sup>Aug, 2016</label>
                    <span>Grand Test</span></li>
                    
                    <li>
                    <label>21<sup>st </sup>Aug, 2016</label>
                    <span>Prosthodontics  1 and related Dental Materials</span></li>
                    
                    <li>
                    <label>28<sup>th </sup>Aug, 2016</label>
                    <span>Prosthodontics 2 and related  Dental Materials</span></li>
                    <li>
                    <label>04<sup>th </sup>Sep, 2016</label>
                    <span>Community Dentistry</span></li>
                    
                    <li>
                    <label>11<sup>th </sup>Sep, 2016</label>
                    <span>Periodontology</span></li>
                    
                    <li class="yellow_bg" style="font-weight:600;">
                    <label>18<sup>th </sup>Sep, 2016</label>
                    <span>Grand Test</span></li>
                    
                    <li>
                    <label>25<sup>th </sup>Sep, 2016</label>
                    <span>Orthodontics and Related Dental Materials</span></li>
                    
                    <li>
                    <label>02<sup>nd </sup>Oct, 2016</label>
                    <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                    <li><label>09<sup>th </sup>Oct, 2016</label>
                    <span>Human Anatomy</span></li>
                    <li class="yellow_bg" style="font-weight:600;"><label>16<sup>th </sup>Oct, 2016</label>
                    <span>Grand Test</span></li>
                    <li><label>23<sup>rd </sup>Oct, 2016</label>
                    <span>Dental Anatomy and Dental Histology</span></li>
                    <li><label>30<sup>th </sup>Oct, 2016</label>
                    <span>Physiology +Biochemistry</span></li>
                    
                    <li><label>06<sup>th </sup>Nov, 2016</label>
                    <span>Oral pathology(OMDR)  1</span></li>
                     <li><label>13<sup>th </sup>Oct, 2016</label>
                    <span>Oral pathology(OMDR)  2</span></li>
                    <li class="yellow_bg" style="font-weight:600;"><label>20<sup>th </sup>Oct, 2016</label>
                    <span>Grand Test</span></li>
                        
                    </ul>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>