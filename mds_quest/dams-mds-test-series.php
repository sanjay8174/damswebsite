<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, DAMS MDS Quest</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php error_reporting(0);
include 'registration.php';
$course_id = $_REQUEST['c'];
$courseNav_id = 11;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
  <?php $getNavContent=$Dao->getCourseNavContent($courseNav_id); ?>
<!--    <article class="test-series">-->
        <article style="width:100%;float:left;height:318px;background: url('http://damsdelhi.com/images/background_images/<?php echo $getNavContent[0][2]; ?>') right top no-repeat;">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left banner-left-postion">
        <?php echo   $getNavContent[0][1]; ?>
          <!--<h2>Be smart &amp;<br> take your future in Your Hand </h2>
        <h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants,<br>
          who otherwise couldn't take the advantage of the DAMS teaching<br>
          because of various reasons like non-availability of DAMS centre<br>
          in the vicinity would be greatly benefited.</h3>-->
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="http://damsdelhi.com/index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="index.php?c=<?php echo $course_id ?>" title="MDS Quest">MDS Quest</a></li>
          <li><a title="MDS Test Series" class="active-link">MDS Test Series</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>MDS Test Series - 2020<span class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
	    <?php echo $getNavContent[0][0]; ?>
            <div class="test-series-content paddin-zero">
              <ul class="duration-content">
                <li>
                  <label>Price :</label>
                  <span class="price_font_block">8260/- 
                   <span class="including_tax_inline">(including s.tax)</span>
                  </span></li>
              <li>
                  <label>Dental Career Counselling for MDS :</label>
                  <span class="price_font">09999158131</span></li>
              </ul>
            </div>
            <div class="showme-main book-hide test_series_inline">
              <ul class="idTabs mini-tab">
                <li><a href="#test1" class="mds-tab">Test Series - I</a></li>
                <li><a href="#test2" class="mds-tab">Test Series - II</a></li>
                <li><a href="#test3" class="mds-tab">Test Series - III</a></li>
                <li><a href="#test4" class="mds-tab">Test Series - IV</a></li>
                <li><a href="#test5" class="mds-tab">Test Series - V</a></li>
                <li style="display:none;"><a href="#test6" class="mds-tab">Revision Test Series</a></li>
                <li><a href="#test6" class="mds-tab">Test Series - VI</a></li>
                <li><a href="#test7" class="mds-tab">Test Series - VII</a></li>
                <li><a href="#test8" class="mds-tab">Test Series - VIII</a></li>
              </ul>
              <div id="test1">
                <div class="test-tab-content">
                  <a href="http://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>03<sup>rd </sup>Nov,2019</label>
                        <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                      <li>
                        <label>10<sup>th </sup>Nov,2019</label>
                        <span>Human Anatomy</span></li>
                      <li class="yellow_bg">
                        <label>17<sup>th </sup>Dec, 2019</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>24<sup>th </sup>Dec, 2019</label>
                        <span>Dental Anatomy and Dental Histology</span></li>
                      <li>
                        <label>1<sup>st</sup>Dec, 2019</label>
                        <span>Physiology +Biochemistry</span></li>
                      <li>
                        <label>08<sup>th </sup>Dec, 2019</label>
                        <span>Oral pathology(OMDR) 1</span></li>
                      <li class="yellow_bg">
                       <label style="font-weight:600;">15<sup>th </sup>Dec, 2019</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      <li>
                        <label>22<sup>nd </sup>Dec, 2019</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                      <li>
                        <label>29<sup>th </sup>Dec, 2019</label>
                        <span>General medicine+ General Surgery</span></li>
                      <li>
                          <label >05<sup>th </sup>Jan, 2020</label>
                        <span>Pedodontics</span></li>
                      <li>
                       <label>12<sup>th </sup>Jan, 2020</label>
                        <span>General Pathology + Microbiology</span></li>
                      <li class="yellow_bg">
                        <label style="font-weight:600;">19<sup>th </sup>Jan, 2020</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      <li>
                        <label>26<sup>th </sup>Jan, 2020</label>
                        <span>Oral Surgery 1</span></li>
                      <li>
                        <label>02<sup>nd </sup>Feb, 2020</label>
                        <span>Oral Surgery 2</span></li>
                      <li>
                       <label>09<sup>th </sup>Feb, 2020</label>
                        <span>Pharmacology</span></li>
                      <li class="yellow_bg">
                        <label style="font-weight:600;">16<sup>th </sup>Feb, 2020</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      <li>
                       <label >23<sup>rd </sup>Feb, 2020</label>
                        <span >Prosthodontics 1 and related Dental Materials</span></li>
                      <li>
                       <label>01<sup>st </sup>Mar, 2020</label>
                        <span>Prosthodontics 2 and related Dental Materials</span></li>
                      <li>
                        <label>08<sup>th </sup>Mar, 2020</label>
                        <span>Community Dentistry</span></li>
                      <li class="yellow_bg">
                        <label style="font-weight:600;">15<sup>st </sup>Mar, 2020</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      <li>
                        <label>22<sup>nd </sup>Mar, 2020</label>
                        <span>Periodontology</span></li>
                      <li>
                        <label>29<sup>th </sup>Mar, 2020</label>
                        <span>Orthodontics and Related Dental Materials</span></li>
                      <li class="light_green_bg">

                        <label>12<sup>th </sup>Apr, 2020</label>
                        <span>AIIMS Mock</span></li>

                      <li class="yellow_bg">
                        <label style="font-weight:600;">19<sup>th </sup>Apr, 2020</label>

                        <span style="font-weight:600;">Grand Test</span></li>
                      
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test2">
                <div class="test-tab-content">
                  <a href="http://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                        
<li>
                       <label>22<sup>nd </sup>Dec, 2019</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                      <li>
                        <label>29<sup>th </sup>Dec, 2019</label>
                        <span>General medicine+ General Surgery</span></li>
                      <li>
                        <label>05<sup>th </sup>Jan, 2020</label>
                        <span>Pedodontics</span></li>
                      <li>
                        <label>12<sup>th </sup>Jan, 2020</label>
                        <span>General Pathology + Microbiology</span></li>
                      <li class="yellow_bg">
                          <label style="font-weight:600;">19<sup>th </sup>Jan, 2020</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      <li>
                        <label>26<sup>th </sup>Jan, 2020</label>
                        <span>Oral Surgery 1</span></li>
                      <li>
                        <label>02<sup>nd </sup>Feb, 2020</label>
                        <span>Oral Surgery 2</span></li>
                      <li>
                       <label>09<sup>th </sup>Feb, 2020</label>
                        <span>Pharmacology</span></li>
                      <li>
                        <li class="yellow_bg">
                        <label style="font-weight:600;">16<sup>th </sup>Feb, 2020</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      <li>  
                        <label>23<sup>rd </sup>Feb, 2020</label>
                        <span>Prosthodontics 1 and related Dental Materials</span></li>
                      <li>
                       <label >01<sup>st </sup>Mar, 2020</label>
                        <span >Prosthodontics 2 and related  Dental Materials</span></li>
                      <li>
                        <label>08<sup>th </sup>Mar, 2020</label>
                        <span>Community Dentistry</span></li>
                      <li  class="yellow_bg">
                       <label style="font-weight:600;">15<sup>th </sup>Mar, 2020</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                      
                      <li>
                        <label>22<sup>nd </sup>Mar, 2020</label>
                        <span>Periodontology</span></li>
                      <li>
                        <label>29<sup>th </sup>Mar, 2020</label>
                        <span>Orthodontics and Related Dental Materials</span></li>
                      
                      <li><label>05<sup>th </sup>Apr, 2020</label> <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                      <li class="yellow_bg">
                        <label style="font-weight:600;">12<sup>th </sup>Apr, 2020</label>
                        <span style="font-weight:600;">AIIMS Mock</span></li>
                      <li class="light_green_bg">
                       <label style="font-weight:600;">19<sup>th </sup>Apr, 2010</label>
                        <span style="font-weight:600;">Grand Test</span></li>
                                         
                        
                        <li><label>26<sup>th </sup>Apr, 2020</label> <span>Human Anatomy</span></li>
                        <li><label>03<sup>th</sup>May, 2020</label> <span>Dental Anatomy and Dental Histology</span></li>
                        <li class="green_bg"  style="font-weight:600;"><label>10<sup>th </sup>May, 2020</label> <span>Tentative Date for AIIMS</span></li>
                        
                        <li><label>17<sup>th </sup>May, 2020</label> <span>Physiology +Biochemistry</span></li>
                        <li><label>24<sup>th </sup>May, 2020</label> <span>Oral pathology(OMDR) 1</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test3">
                <div class="test-tab-content">
                  <a href="http://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                        <li><label>26<sup>th </sup>Jan, 2020</label> <span>Oral Surgery-1</span></li>
                        <li ><label>02<sup>nd </sup>Feb, 2020</label> <span>Oral Surgery 2</span></li>
                        <li ><label>09<sup>th </sup>Feb, 2020</label> <span>Pharmacology</span></li>
                        <li class="yellow_bg"  style="font-weight:600;"><label>16<sup>th </sup>feb, 2020  </label><span>Grand Test</span></li>
                        
                         <li><label>23<sup>rd </sup>Feb, 2020</label> <span>Prosthodontics  1 and related Dental Materials</span></li>
                        
                       
                        <li><label>01<sup>st</sup>Mar, 2020</label> <span>Prosthodontics  2 and related Dental Materials</span></li>
                        
                        <li><label>08<sup>th </sup>Mar, 2020</label> <span>Community Dentistry</span></li>
                       <li class="yellow_bg"  style="font-weight:600;"><label>15<sup>th </sup>Mar, 2020  </label><span>Grand Test</span></li>
                        <li><label>22<sup>nd </sup>Mar, 2020</label> <span>Periodontology</span></li>
                        <li><label>29<sup>th </sup>Mar, 2020</label> <span>Orthodontics and Related Dental Materials</span></li>
                        <li><label>05<sup>th </sup>Apr, 2020</label> <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>12<sup>th </sup>Apr, 2020</label> <span>AIIMS Mock</span></li>
                        <li class="light_green_bg" style="font-weight:600;"><label>19<sup>th </sup>Apr, 2020</label> <span>Grand Test</span></li>
                        
                         <li><label>26<sup>th </sup>Apr, 2020</label> <span>Human Anatomy</span></li>
                        
                       
                        <li><label>03<sup>th </sup>May, 2020</label> <span>Dental Anatomy and Dental Histology</span></li>
                       <li class="green_bg"  style="font-weight:600;"><label>10<sup>th </sup>May, 2020</label> <span>Tentative Date for AIIMS</span></li>
                        <li><label>17<sup>th </sup>May, 2020</label> <span>Physiology +Biochemistry</span></li>
                        <li><label>24<sup>th </sup>May, 2020</label> <span>Oral pathology (OMDR)  1</span></li>
                        <li><label>31<sup>st </sup>May, 2020</label> <span>Oral Pathology (OMDR) 2</span></li>
                        <li class="light_green_bg"  style="font-weight:600;"><label>16<sup>th </sup>Jun, 2020</label> <span>PGI EXAM</span></li>
                        <li><label>07<sup>th </sup>Jun, 2020</label> <span>General medicine+ General  Surgery</span></li>
                        <li><label>14<sup>th </sup>Jun, 2020</label> <span>Pedodontics</span></li>
                        
                        <li class="yellow_bg"  style="font-weight:600;"><label>21<sup>th </sup>Jun, 2020</label> <span>Grand Test</span></li>
                        
                        <li><label>28<sup>th </sup>Jun, 2020</label> <span>General Pathology + Microbiology</span></li>
                        <li class="yellow_bg"  style="font-weight:600;"><label>19<sup>th </sup>Jul, 2020</label> <span>Grand Test</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test4">
                <div class="test-tab-content">
                  <a href="http://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                        <li><label>23<sup>rd </sup>Feb, 2020</label> <span>Prosthodontics  1 and related Dental Materials</span></li>
                        <li><label>01<sup>st </sup>Mar, 2020</label> <span>Prosthodontics  2 and related Dental Materials</span></li>
                        
                        <li><label>08<sup>th </sup>Mar, 2020</label> <span>Community Dentistry</span></li>
                       <li class="yellow_bg"  style="font-weight:600;"><label>15<sup>th </sup>March, 2020 </label> <span>Grand Test</span></li>
                        <li><label>22<sup>nd </sup>Mar, 2020</label> <span>Periodontology</span></li>
                        <li><label>29<sup>th </sup>mar, 2020</label> <span>Orthodontics and Related Dental Materials</span></li>
                         <li><label>05<sup>th </sup>Apr, 2020</label> <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>12<sup>th </sup>Apr, 2020</label> <span>AIIMS Mock</span></li>
                        <li class="light_green_bg" style="font-weight:600;"><label>19<sup>th </sup>Apr, 2020</label> <span>Grand Test</span></li>
                       
                         <li><label>26<sup>th </sup>Apr, 2020</label> <span>Human Anatomy</span></li>
                        
                       
                        <li><label>03<sup>th </sup>May, 2020</label> <span>Dental Anatomy and Dental Histology</span></li>
                        <li class="green_bg"  style="font-weight:600;"><label>10<sup>th </sup>May, 2020</label> <span>Tentative Date for AIIMS</span></li>
                        <li><label>17<sup>th </sup>May, 2020</label> <span>Physiology +Biochemistry</span></li>
                        <li><label>24<sup>th </sup>May, 2020</label> <span>Oral pathology (OMDR)  1</span></li>
                        <li><label>31<sup>st </sup>May, 2020</label> <span>Oral Pathology (OMDR) 2</span></li>
                        <li><label>07<sup>rd </sup>Jun, 2020</label> <span>General medicine+ General  Surgery</span></li>
                        <li><label>14<sup>rd </sup>Jun, 2020</label> <span>Pedodontics</span></li>
                         <li class="yellow_bg"  style="font-weight:600;"><label>21<sup>st </sup>Jun, 2020</label> <span>Grand Test</span></li>
                        
                       
                        <li><label>28<sup>th </sup>Jun, 2020</label> <span>General Pathology + Microbiology</span></li>
                        <li><label>05<sup>th </sup>Jul, 2020</label> <span>Oral Surgery-1</span></li>
                        
                        <li><label>12<sup>th </sup>Jul, 2020</label> <span>Oral Surgery 2</span></li>
                        <li class="yellow_bg"  style="font-weight:600;"><label>19<sup>th </sup>Jul, 2020</label> <span>Grand Test</span></li>
                        <li><label>26<sup>th </sup>Jul, 2020</label> <span>Pharmacology</span></li>                      
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test5">
                <div class="test-tab-content">
                  <a href="http://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                        <li><label>22<sup>nd </sup>Mar, 2020</label> <span>Periodontology</span></li>
                        <li><label>29<sup>th </sup>Apr, 2020</label> <span>Orthodontics and Related Dental Materials</span></li>
                        <li><label>05<sup>th </sup>Apr, 2020</label> <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>12<sup>th </sup>Apr, 2020</label> <span>AIIMS Mock</span></li>
                        <li class="light_green_bg" style="font-weight:600;"><label>19<sup>th </sup>Apr, 2020</label> <span>Grand Test</span></li>
                        <li><label>26<sup>th </sup>Apr, 2020</label> <span>Human Anatomy </span></li>
                        <li><label>03<sup>th </sup>May, 2020</label> <span>Dental Anatomy and Dental Histology </span></li>
                        <li class="green_bg" style="font-weight:600;"><label>10<sup>th </sup>May, 2020</label> <span>Tentative Date for AIIMS</span></li>
                        <li><label>17<sup>th </sup>May, 2020</label> <span>Physiology +Biochemistry</span></li>
                        <li><label>24<sup>th </sup>May, 2020</label> <span>Oral pathology(OMDR)  1</span></li>
                        <li><label>31<sup>st </sup>May, 2020</label> <span>Oral Pathology (OMDR) 2</span></li>
                        <li><label>07<sup>th </sup>Jun, 2020</label> <span>General medicine+ General  Surgery</span></li>
                        <li><label>14<sup>th </sup>Jun, 2020</label> <span>Pedodontics</span></li>
                        <li class="light_green_bg" style="font-weight:600;"><label>21<sup>st </sup>Jun, 2020</label> <span>Grand Test</span></li>
                        <li><label>28<sup>th </sup>Jun, 2020</label> <span>General Pathology + Microbiology</span></li>
                        <li><label>05<sup>th </sup>Jul, 2020</label> <span>Oral Surgery-1</span></li>
                        <li><label>12<sup>th </sup>Jul, 2020</label> <span>Oral Surgery 2</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>19<sup>th </sup>Jul, 2020</label> <span>Grand Test</span></li>
                        <li><label>26<sup>th </sup>Jul, 2020</label> <span>Pharmacology</span></li>
                        <li><label>02<sup>nd </sup>Aug, 2020</label> <span>Prosthodontics  1 and related Dental Materials</span></li>
                        <li><label>09<sup>th </sup>Aug, 2020</label> <span>Prosthodontics 2 and related  Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>16<sup>th </sup>Aug, 2020</label> <span>Grand Test </span></li>
                        <li><label>23<sup>rd </sup>Aug, 2020</label> <span>Community Dentistry</span></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="test6">
                <div class="test-tab-content">
                  <a href="http://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                        <li><label>03<sup>rd </sup>May, 2020</label> <span>Dental Anatomy and Dental Histology</span></li>
                        <li class="green_bg" style="font-weight:600;"><label>10<sup>th </sup>May, 2020</label> <span>Tentative Date for AIIMS</span></li>
                        <li><label>17<sup>th </sup>May, 2020</label> <span>Physiology +Biochemistry</span></li>
                        <li><label>24<sup>th </sup>May, 2020</label> <span>Oral pathology(OMDR)  1</span></li>
                        <li><label>31<sup>st </sup>May, 2020</label> <span>Oral Pathology (OMDR) 2</span></li>
                        <li><label>07<sup>th </sup>Jun, 2020</label> <span>General medicine+ General  Surgery</span></li>
                        <li><label>14<sup>th </sup>Jun, 2020</label> <span>Pedodontics</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>21<sup>st </sup>Jun, 2020</label> <span>Grand Test</span></li>
                        <li><label>28<sup>th </sup>Jun, 2020</label> <span>General Pathology + Microbiology</span></li>
                        <li><label>05<sup>th </sup>Jul, 2020</label> <span>Oral Surgery-1</span></li>
                        <li><label>12<sup>th </sup>Jul, 2020</label> <span>Oral Surgery 2</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>19<sup>th </sup>Jul, 2020</label> <span>Grand Test</span></li>
                        <li><label>26<sup>th </sup>Jul, 2020</label> <span>Pharmacology</span></li>
                        <li><label>02<sup>nd </sup>Aug, 2020</label> <span>Prosthodontics  1 and related Dental Materials</span></li>
                        <li><label>09<sup>th </sup>Aug, 2020</label> <span>Prosthodontics 2 and related  Dental Materials</span></li>
                        <li class="yellow_bg" style="font-weight:600;"><label>16<sup>th </sup>Aug, 2020</label> <span>Grand Test </span></li>
                      <li>
                   <label>23<sup>rd </sup>Aug, 2020</label>
                    <span>Community Detistry</span></li>
                        <li>
                   <label>30<sup>th </sup>Aug, 2020</label>
                    <span>Periodontology</span></li>
                    
                    <li>
                   <label>06<sup>th </sup>Sep, 2020</label>
                    <span>Orthodontics and Related Dental Materials</span></li>
                    
                    <li>
                    <label>13<sup>th </sup>Sep, 2020</label>
                    <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                    
                    <li class="yellow_bg" style="font-weight:600;">
                   <label>20<sup>th </sup>Sep, 2020</label>
                    <span>Grand Test</span></li>
                    
                     <li>
                    <label>26<sup>th </sup>Apr, 2020</label>
                    <span>Human Anatomy</span></li>

                        
                    </ul>
                  </div>
                </div>
              </div>      
              
              <div id="test7">
                <div class="test-tab-content">
                  <a href="http://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>14<sup>th</sup>Jun, 2020</label>
                        <span>Pedodoncs</span></li>
                      <li  class="yellow_bg">
                        <label>21<sup>th </sup>Jun, 2020</label>
                        <span>Grand test</span></li>
                      <li>
                        <label>28<sup>th </sup>Jun, 2020</label>
                        <span>General Pathology + Microbiology</span></li>
                      <li>
                        <label>05<sup>th </sup>Jul, 2020</label>
                        <span>Oral Surgery-1</span></li>
                      <li>
                        <label>12<sup>th </sup>Jul, 2020</label>
                        <span>Oral Surgery-2</span></li>
                      <li class="yellow_bg">
                        <label>19<sup>th </sup>Jul, 2020</label>
                        <span>Grand Test</span></li>
                      
                      <li>
                        <label>26<sup>th </sup>Jul, 2020</label>
                        <span>Pharmacology</span></li>
                      <li>
                        <label>02<sup>nd </sup>Aug, 2020</label>
                        <span> Prosthodontics 1 and related Dental Materials</span></li>
                      
                
                      <li> <label>09<sup>th </sup>Aug, 2020</label>
                        <span>Prosthodontics 2 and related Dental Materials</span></li>
                      <li>
                          <li class="yellow_bg">
                        <label>16<sup>th </sup>Aug, 2020</label>
                        <span>Grand Test</span></li>
                        
<!--                          <label>01<sup>st </sup>Sep, 2020</label>
                        <span>Community Dentistry</span></li>
                      <li>
                        <label>08<sup>th</sup>Sep, 2020</label>
                        <span>Periodontology</span></li>
                     <li class="yellow_bg">
                        <label>15<sup>th </sup>Sep, 2020</label>
                        <span>Grand Test</span></li>
                    <li>
                    <label>22<sup>nd </sup>Sep, 2020</label>
                    <span>Orthodontics and Related Dental Materials</span></li>
                    
                    <li>
                    <label>29<sup>th </sup>Sep, 2020</label>
                    <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                    
                    <li>
                    <label>06<sup>th </sup>Oct, 2020</label>
                    <span>Human Anatomy</span></li>
                    
                    <li>
                    <label>13<sup>th </sup>Oct, 2020</label>
                    <span>Dental Anatomy and Dental Histology</span></li>
                    <li class="yellow_bg">
                    <label>20<sup>th </sup>Oct, 2020</label>
                    <span>Grand Test</span></li>
                    
                    
                    
                    <li>
                    <label>27<sup>th </sup>Nov, 2020</label>
                    <span>Physiology + Biochemistry</span></li>
                    
                    <li>
                    <label>03<sup>rd </sup>Nov, 2020</label>
                    <span>Oral pathology(OMDR) 1</span></li>
                       <li>
                    <label>10<sup>th </sup>Nov, 2020</label>
                    <span>Oral Pathology (OMDR) 2</span></li>
                    <li class="yellow_bg">
                    <label>17<sup>th </sup>Nov, 2020</label>
                    <span>Grand Test</span></li>-->
                    
                 
                        
                    </ul>
                  </div>
                </div>
              </div>
                
              <div id="test8">
                <div class="test-tab-content">
                  <a href="http://www.damsdelhi.com/downloadpdf/9.pdf" target="_blank" id="newtopic">Topic Distribution</a>
                  <div class="test-combo-content">
                    <ul>
                      <li>
                        <label>23<sup>rd</sup>Aug, 2020</label>
                        <span>Community Dentistry</span></li>
                      <li>
                        <label>30<sup>th</sup>Aug, 2020</label>
                        <span>Periodontology</span></li>
                      <li>
                        <label>06<sup>th </sup>Sep, 2020</label>
                        <span>Orthodontics and Related Dental Materials</span></li>
                      <li>
                        <label>13<sup>th </sup>Sep, 2020</label>
                        <span>Endodontics & Conservative Dentistry and related Dental Materials</span></li>
                      <li class="yellow_bg">
                        <label>20<sup>th </sup>Sep, 2020</label>
                        <span>Grand Test</span></li>
                      <li>
                        <label>27<sup>th </sup>Sep, 2020</label>
                        <span>Human Anatomy</span></li>
                      <li>
                        <label>04<sup>th </sup>Sep, 2020</label>
                        <span>Dental Anatomy and Dental Histology</span></li>
                      <li>
                        <label>11<sup>th </sup>Oct, 2020</label>
                        <span>Physiology + Biochemistry</span></li>
                      <li class="yellow_bg">
                        <label>18<sup>th </sup>Oct, 2020</label>
                        <span>Grand Test</span></li>
                      <li>
                      <li>
                        <label>25<sup>th </sup>Oct, 2020</label>
                        <span>Oral pathology(OMDR) 1</span></li>
                      <li>
                        <label>01<sup>st </sup>Nov, 2020</label>
                        <span>Oral Pathology (OMDR) 2</span></li>
                      </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style="margin-top:20px; display: inline-block; width: 99.3%;display:none;">
           <img src="../images/dams_super_series.jpg" width="100%;">
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'mds-right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>

<!-- Footer Css End Here -->

</body>
</html>
