
<!--<div class="row">                
<div class="col-lg-12">
    <h3><img src="images/samsung-lg.png"/><strong>&nbsp;&&nbsp;iDams Tab Offer Price </strong></h3>
                </div>


<div class="col-lg-12">
              <table  class="price_deail1" align="center" border="0" cellpadding="2" cellspacing="2" width="100%">
            <tbody>
            <tr style="color:#FFF; font-size:16px;">
              <td align="center" bgcolor="#00984B" >Price</td>
              <td align="center" bgcolor="#00994d" >iDams Samsung T116 </td>
              <td align="center" bgcolor="#00994d" >Samsung Tab E (T 561) </td>           
              <td align="center" bgcolor="#00994d" >iDams Tab </td>           
            </tr>
              
             <tr style="background-color: #d7efff;font-size:16px;">
              <td align="center">iDams MRP</td>
              <td align="center" width="257">Rs 11,400</td>
              <td align="center" width="269">Rs 19,900</td>
              <td align="center" width="269">Rs </td>
            </tr>

            <tr style="color:#FFF; font-weight:bold;font-size:16px;" bgcolor="#FE7A42 ;">
              <td align="center" bgcolor="#FE7A42 ">Price for CAT-o-Mobile students</td>
              <td align="center" bgcolor="#FE7A42 ">Rs 9,290</td>
              <td align="center" bgcolor="#FE7A42 " width="269">Rs 15,590</td>
            </tr>
          </tbody>
          </table>
    </div>
    </div>-->

<div class="row" >
          <div class="col-lg-12">
              <h3 >
                  <strong>Hardware Specifications</strong>
              </h3>
          </div>
          <div class="col-lg-12" >
                 <table class="tab_specification1" >
            <tbody>
            <tr class="firstchield">
              <th class="col_width_spec1">Sr. No.</th>
              <th  class="col_width_spec2" >Specification</th>
              <th  class="col_width_spec5" style="display: none;">Basic iDAMS Tab</th>
              <th class="col_width_spec3"  >idams Sam-1</th>
              <th class="col_width_spec4">idams Sam-2</th>
             
            </tr>
             <tr class="price_idams">
                 <td class="col_width_spec2" colspan="2"><strong>Price</strong></td>
               <!--<td class="col_width_specs5"><strong>Rs.  35500</strong></td>-->
              <td class="col_width_spec3"><strong>Rs.  40,500</strong></td>
              <td class="col_width_spec4"><strong>Rs.  45,500 </strong></td>
             
            </tr>
              
            <tr>
              <td class="col_width_spec1">1</td>
              <td class="col_width_spec2">Size</td>
               <!--<td class="col_width_specs5">7 Inches</td>-->
              <td class="col_width_spec3">7 Inches</td>
              <td class="col_width_spec4">8 Inches</td>
             
            </tr>
            <tr>
              <td class="col_width_spec1">2</td>
              <td class="col_width_spec2">Dimension</td>
              <!--<td class="col_width_specs5">108 x 188.4 x 9 mm</td>-->
              <td class="col_width_spec3">116.4 x 193.4 x 9.7 mm</td>
              <td class="col_width_spec4">212.1 x 124.1 x 8.9 mm</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">3</td>
              <td class="col_width_spec2">Touch Panel</td>
              <!--<td class="col_width_specs">TFT</td>-->
              <td  class="col_width_spec3">TFT</td>
              <td class="col_width_spec4">TFT</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">4</td>
              <td class="col_width_spec2">Platform</td>
              <!--<td class="col_width_specs">3G/2G, GPRS/EDGE support.</td>-->
              <td class="col_width_spec3">3G Band - B1 (IMT 2100MHz), B8 (GSM00MHz)2G Band B8 (GSM 850MHz), B11 (R-GSM 900MHz), B13 (DCS 1800MHz), B14 (PCS    1900MHz), Voice</td>
              <td class="col_width_spec4"> 2G GSM, 3G WCDMA, 4G LTE FDD, 4G LTE TDD ,LTE</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1" >5</td>
              <td class="col_width_spec2">Resolution</td>
              <!--<td class="col_width_specs">1024x600</td>-->
              <td class="col_width_spec3">1024x600 (WSVGA)</td>
              <td class="col_width_spec4">1280x800</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">6</td>
              <td class="col_width_spec2">Internal SD Card</td>
              <!--<td class="col_width_specs">4 GB Flash Memory & 32 GB Expandable through micro SD card</td>-->
              <td class="col_width_spec3" >8 GB (upto 32 GB extendable MicroSd</td>
              <td class="col_width_spec4" >16 GB (upto 256 GB extendable MicroSd</td>
              
            </tr>
            <tr bgcolor="#d7efff">
              <td class="col_width_spec1">7</td>
              <td class="col_width_spec2">Battery Life</td>
              <!--<td class="col_width_specs">3000 mAh</td>-->
              <td class="col_width_spec3">3600 mAH</td>
              <td class="col_width_spec4">5000 mAH</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">8</td>
              <td class="col_width_spec2">Camera (Front and Rear)</td>
              <!--<td class="col_width_specs">2 MP rear with Flash & 0.3 MP front Camera</td>-->
              <td class="col_width_spec3">CMOS, 5.0 MP (Auto Focus)</td>
              <td class="col_width_spec4">Rear 8.0 MP Front 5.0 MP</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">9</td>
              <td class="col_width_spec2">CPU / Chip</td>
              <!--<td class="col_width_specs">1 GHz Dual Core Processor</td>-->
              <td class="col_width_spec3">Quad Core, 1.2 Ghz</td>
              <td class="col_width_spec4"> 1.4GHz| Quad /Qualcomm | MSM8917	</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">10</td>
              <td class="col_width_spec21">Multimedia Options</td>
              <!--<td class="col_width_specs5">Video    playback formats - MP4, M4V, 3GP,   3G2, WMV, ASF, AVI, FLV, MKV, WEBM, TS Audio formats - MP3, M4A, 3GA,    </td>-->
              <td class="col_width_spec3">Video    playback formats - MP4, M4V, 3GP,   3G2, WMV, ASF, AVI, FLV, MKV, WEBM, TS Audio formats - MP3, M4A, 3GA,   AAC, OGG, OGA, WAV, WMA, AMR, AWB, FLAC, MID,    MIDI, XMF, MXMF, IMY,   RTTTL, RTX, OTA</td>
              <td class="col_width_spec4">Video playback    formats - MP4, M4V, 3GP, 3G2, WMV, ASF, AVI, FLV, MKV, WEBM;Audio formats - MP3, M4A, 3GA, AAC, OGG, OGA, WAV, WMA, AMR, AWB, FLAC, MID, MIDI, XMF, MXMF, IMY, RTTTL, RTX, OTA</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">11</td>
              <td class="col_width_spec2">RAM</td>
              <!--<td class="col_width_specs">1 GB DDRlll RAM</td>-->
              <td class="col_width_spec3">1    GB</td>
              <td class="col_width_spec4">2    GB</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">12</td>
              <td class="col_width_spec2">Software</td>
              <!--<td class="col_width_specs">Android JELLY BEAN 4.1.2</td>-->
              <td class="col_width_spec3">4.4    (Kitkat)</td>
              <td class="col_width_spec4">7.1     (Nougat)</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">13</td>
              <td class="col_width_spec2">Connectivity</td>
              <!--<td class="col_width_specs"> 4.0,USB,Built-in Wi-Fi Module 802.11 b/g/n, 2.4 GHzEar Jack, Dual SIM Support,</td>-->
              <td class="col_width_spec3">Bluetooth, Bluetooth Profiles, USB, Wi-Fi, Wi-fi Direct, AGPS, Ear Jack, Sim Support, PC Sync</td>
              <td class="col_width_spec4">Bluetooth, Bluetooth Profiles, USB, Wi-Fi, Wi-fi Direct, ANT+, Ear Jack, Sim Support(SS), PC Sync</td>
              
            </tr>
            <tr>
              <td class="col_width_spec1">14</td>
              <td class="col_width_spec2">USB</td>
               <!--<td class="col_width_specs">2.0</td>-->
              <td class="col_width_spec3">2.0, OTG</td>
              <td class="col_width_spec4">2.0</td>
             
            </tr>

          <tr class="buy_idams">
              <td class="col_width_spec1" colspan="2" class="buy_idams1"><strong>BUY</strong></td>
              <!--<td class="col_width_specs5"><a href="http://registration.damsdelhi.com" style="text-decoration: none"><span style="cursor: pointer;" class="buy_now">Buy Now</span></a></td>-->
              <td class="col_width_spec3"><a href="http://registration.damsdelhi.com" style="text-decoration: none"><span style="cursor: pointer;" class="buy_now">Buy Now</span></a></td>
              <td class="col_width_spec4"><a href="http://registration.damsdelhi.com" style="text-decoration: none"><span style="cursor: pointer;" class="buy_now">Buy Now</span></a></td>

             
            </tr>
          </tbody>
                 </table>
              </div>
    </div>
                <div class="row"> 
                  <div class="col-lg-12"> 
<!--                       <p style="text-align: left;font-size:14px;font-size: 15px;font-family: 'pt_sansregular';line-height: 25px;margin-block-end: 20px;font-weight: 600;"> Also includes Grand test &amp; Subject wise test series (online)+ Recent updates &amp; Last look revision booklets<br></p>-->
                  <table class="table-striped" border="0" cellpadding="2" cellspacing="2" width="100%">
        <tbody>
        <tr>
          <td  bgcolor="#f3f3f3" style="background-color: #d7efff; text-align:center;width:50%"><h2 style="text-align:center;">Note: Lectures will be available for 7 months</h2></td>

          <td style="background-color: #d7efff; text-align:center; text-align:center;width:50%" ><div class="book-ur-seat-btn"><a href="http://registration.damsdelhi.com" title="Book Your Seat"> <span>&nbsp;</span>Buy Now</a></div></td>

        </tr> 
        </tbody>
    </table>
      </div>
    <div class="col-lg-12" style="margin-top: 15px;font-size:14px;">              
                  <p style="text-align: left;font-size:14px;font-family: 'pt_sansregular';"><u><strong style="color:#000">NOTE</strong></u><br>You can send DD in favor of :<br>“Delhi Academy of Medical Sciences Pvt. Ltd” payable at “New Delhi”. </p>
                  <p style="text-align: left;font-size:14px;font-size: 14px;font-family: 'pt_sansregular';"><i><strong>For pre-booking contact 011-40094009 or mail </strong><a style="text-align: left;font-size:14px;font-size: 14px;font-family: 'pt_sansregular';color: #3593C9;"><u>idams@damsdelhi.com</u></a></i></p>
         </div>
</div>