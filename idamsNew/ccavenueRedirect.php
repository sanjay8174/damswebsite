<?php
function verifyChecksum($MerchantId , $OrderId, $Amount, $AuthDesc, $WorkingKey,  $CheckSum)
	{
		$str = "";
		$str = "$MerchantId|$OrderId|$Amount|$AuthDesc|$WorkingKey";
		$adler = 1;
		$adler = adler32($adler,$str);
		if($adler==$CheckSum) return true;
		else return false;
	}

	function getChecksum($MerchantId, $OrderId, $Amount, $redirectUrl, $WorkingKey)  {
		$str = "$MerchantId|$OrderId|$Amount|$redirectUrl|$WorkingKey";
		$adler = 1;
		$adler = adler32($adler,$str);
		return $adler;
	}

	function adler32($adler , $str)
	{
		$BASE =  65521 ;

		$s1 = $adler & 0xffff ;
		$s2 = ($adler >> 16) & 0xffff;
		for($i = 0 ; $i < strlen($str) ; $i++)
		{
			$s1 = ($s1 + Ord($str[$i])) % $BASE ;
			$s2 = ($s2 + $s1) % $BASE ;
		}
		return leftshift($s2 , 16) + $s1;
	}

	function leftshift($str , $num)
	{

		$str = DecBin($str);

		for( $i = 0 ; $i < (64 - strlen($str)) ; $i++)
			$str = "0".$str ;

		for($i = 0 ; $i < $num ; $i++)
		{
			$str = $str."0";
			$str = substr($str , 1 ) ;
			//echo "str : $str <BR>";
		}
		return cdec($str) ;
	}

	function cdec($num)
	{
		$dec=0;
		for ($n = 0 ; $n < strlen($num) ; $n++)
		{
		   $temp = $num[$n] ;
		   $dec =  $dec + $temp*pow(2 , strlen($num) - $n - 1);
		}

		return $dec;
	}

include('openconnection.php');

$timezone = "Asia/Calcutta";
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

function getCCavenueStatus($orderId){
     $str = file_get_contents("https://www.ccavenue.com/servlet/new_txn.OrderStatusTracker?Merchant_Id=M_acc27085_27085&Order_Id=".$orderId);
     if(strpos($str, "&AuthDesc=")!==false){
                $startPoint = strpos($str, "&AuthDesc=");
                $findValue = substr($str, $startPoint+10,1);
                return $findValue;
     } else {
                return "NotAvailable";
     }
}

function getCenterName($centerId){
    if($centerId == ""){
        return "NA";
    } else {
        if(strpos($centerId,"#")!==false){
            $centerDetail = explode("#",$centerId);

            if(!isset($centerDetail[0]) && !isset($centerDetail[1])){
                return "NA";
            }

            if($centerDetail[0]=="c"){
                $query = "select name from center where id='$centerDetail[1]' limit 1";
            } else if($centerDetail[0]=="tc"){
                $query = "select name from testcenter where id='$centerDetail[1]' limit 1";
            }

            $result = @mysql_query($query) or die("Could not get Center Name");
            if(mysql_num_rows($result)>0){
                $row = mysql_fetch_array($result);
                return $row["name"];
            } else {
                return "NA";
            }
        } else {
            return "NA";
        }

    }
}

function updateStatusForOrderID($orderID, $status){
    $dateTime = date("Y-m-d H:i:s");
    $query = "UPDATE STUDENT_PAYMENT SET STATUS='".$status."' WHERE ORDER_ID='".$orderID."'";
    $result = @mysql_query($query) or die("Could not Insert Data");
}

function getDataOfOrderId($orderID){
    $query = "SELECT * FROM STUDENT_PAYMENT AS SP where SP.ORDER_ID='".$orderID."'";
    $result = @mysql_query($query) or die("Could not get data");
    return $result;
}

function getCorseNameAmount($centerId){
    if($centerId == ""){
            $row["NAME"] = "NA";
            $row["AMOUNT"] = "NA";
            return $row;
    } else {
        $query = "select NAME,AMOUNT from COURSE where ID='$centerId' limit 1";
        $result = @mysql_query($query) or die("Could not get Center Name");
        if(mysql_num_rows($result)>0){
            $row = mysql_fetch_array($result);
            return $row;
        } else {
            $row["NAME"] = "NA";
            $row["AMOUNT"] = "NA";
            return $row;
        }
    }
}
?>
<?php
echo isset($_REQUEST["Merchant_Id"]) .'&&'. isset($_REQUEST["Order_Id"]) .'&&'. isset($_REQUEST["Amount"]) .'&&'. isset($_REQUEST["AuthDesc"]) .'&&'. isset($_REQUEST["Checksum"]);
if(isset($_REQUEST["Merchant_Id"]) && isset($_REQUEST["Order_Id"]) && isset($_REQUEST["Amount"]) && isset($_REQUEST["AuthDesc"]) && isset($_REQUEST["Checksum"])) {
         $WorkingKey = "y1049p2tk1swhboa7lj26ft6nbvwn3q6";

        $MerchantId = $_REQUEST["Merchant_Id"];
	$OrderId = $_REQUEST["Order_Id"];
	$Amount = $_REQUEST["Amount"];
	$AuthDesc = $_REQUEST["AuthDesc"];
        $avnChecksum = $_REQUEST["Checksum"];
        $error = false;
       $Checksum = verifyChecksum($MerchantId, $OrderId, $Amount, $AuthDesc, $WorkingKey, $avnChecksum);
        $AuthDescByCCavenue = getCCavenueStatus($OrderId);

        if($Checksum && $AuthDesc==="Y" && $AuthDescByCCavenue =="Y") {
		$message="Thank you for Buy Course. Your credit card has been charged and your transaction is successful.";
                $mailMessage = "Transaction successful";
                $status = "Y";
	//Here you need to put in the routines for a successful transaction such as sending an email to customer,setting database status, informing logistics etc etc. please ensure that you check that the amount and all parameters are correct and the same as posted to CCavenue.
	} else if($Checksum && $AuthDesc==="B") {
                if($AuthDescByCCavenue =="B") {
                    $message="Thank you for Buy Course.We will keep you posted regarding the status of your order through e-mail";
                    $mailMessage = "Pending(Under Batch Processing)";
                    $status = "B";
                } else if($AuthDescByCCavenue =="Y") {
                    $message="Thank you for Buy Course. Your credit card has been charged and your transaction is successful.";
                    $mailMessage = "Transaction successful";
                    $status = "Y";
                } else if($AuthDescByCCavenue =="N") {
                    $message="Sorry! Your Transaction is not successful.";
                    $mailMessage = "Transaction is not successful";
                    $status = "N";
                } else {
                    $message="Security Error! Illegal access detected.";
                    $error = true;
                }
		//Here you need to put in the routines/e-mail for a  "Batch Processing" order
		//This is only if payment for this transaction has been made by an American Express Card or by any netbank and status is not known is real time  the authorisation status will be  available only after 5-6 hours  at the "View Pending Orders" or you may do an order status query to fetch the status . Refer inetegrtaion document for order status tracker documentation"
	} else if($Checksum && $AuthDesc==="N" && $AuthDescByCCavenue =="N") {
		$message="Sorry! Your Transaction is not successful.";
                $mailMessage = "Transaction is not successful";
                $status = "N";
		//Here you need to put in the routines for a failed
		//transaction such as sending an email to customer
		//setting database status etc etc
	} else {
		$message="Security Error! Illegal access detected.";
                $error = true;

			//Here you need to check for the checksum, the checksum did not match hence the error.
	}
}  else {
		$message="Security Error! Illegal access detected";
                $error = true;
			//Here you need to check for the checksum, the checksum did not match hence the error.
}

if($error===false && isset($status)){
    updateStatusForOrderID($OrderId, $status);

    /*------------Send Message-----------------------*/

    $result = getDataOfOrderId($OrderId);
    if($row = mysql_fetch_array($result)){
        $ids=$row['PRODUCT_IDS'];
	$idarr=  explode(',', $ids);
	
	$bookName='';
	for($i=0;$i<count($idarr);$i++){
          if($idarr[$i]!='' && $idarr[$i]!='0'){ 
		
		$query=mysql_query("select * from PRODUCT WHERE PRODUCT_ID =$idarr[$i]");
            while($rows=mysql_fetch_object($query)) {                
                   if($bookName==''){
				$bookName=urldecode($rows->PRODUCT_NAME);}else{
				$bookName.=",".urldecode($rows->PRODUCT_NAME);}   

                }}}
	$name = $row["STUDENT_NAME"];
        $email = $row["EMAIL"];
        $mobile = $row["MOBILE"];
        $address = $row["ADDRESS"];
        $college = $row["COLLEGE"];
        $Amount = $row["AMOUNT"];
        $dateTimeMail = date("F j, Y g:i a");
    }


    if($status == "Y"){
        $message="Thank you for Buy Book:-<b>$CourseName</b>. Your credit card has been charged and <b>your transaction is successful</b>.";
    }
    
    $subject="Order Id: $OrderId,Online Transaction Status for Publication Books is submitted by ".$email;
    $message2="<html xmlns='https://www.w3.org/1999/xhtml'>
        <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <title>Buy Course</title>

        </head>

        <body>
                <table border=0 cellpadding=9 cellspacing=9 style=border-collapse: collapse bordercolor=#111111 width=100% id=AutoNumber1>

        <tr>
        <td><font size=2 face=verdana>
        Hello,<br><br>
        Mr./Ms. $name has completed transaction process for buy Course by CCavenue.<br><br>
        Information about the visitor and the status of transaction process:<br>
        Order Id: $OrderId <br>
        Name: $name<br>
        E-mail Id: $email<br>
        Mobile Number: $mobile<br>
        Address: $address<br>
        College/University: $college<br>
        Books: $bookName<br>
        Total Amount: $Amount INR<br>
        Time: $dateTimeMail <br>
        Status: <b>$mailMessage</b> <br><br><br>

        </font></td>
        </tr>
                                                </table>
                </body>
        </html>";




    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= "From:$email" . "\r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;

    $success2=mail('info@damsdelhi.com,chandan@damsdelhi.com,accounts@damsdelhi.com,kapil@damsdelhi.com,manish@damsdelhi.com,tejpal@damsdelhi.com',$subject,$message2,$headers);

    /*------------Send Message End-------------------*/

    /*------------Send Message to Student -----------*/
//    if($Course=="31" && $status=="Y"){
//        $subject2="Successfully Enrolled for Course: ".$CourseName;
//        $message3="<html xmlns='https://www.w3.org/1999/xhtml'>
//            <head>
//            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
//            <title>Buy Course</title>
//
//            </head>
//
//            <body>
//                    <table border=0 cellpadding=9 cellspacing=9 style=border-collapse: collapse bordercolor=#111111 width=100% id=AutoNumber1>
//
//            <tr>
//            <td><font size=2 face=verdana>
//            Hello Doctor,<br><br>
//            AIIMS Nov 2012 Paper Discussion on 30th April 2013 from 9am onwards. <br>
//            Address:- Dr. Sarvpalli Radhakrishan Auditorium, APS Colony, Delhi Cant. Near Metro Pillar No. 205.<br>
//            Contacts:- - 01140094009, 9953550295, 8447461113-114-115 <br>
//            Please carry your reciept, when you will come to this address.<br><br>
//
//            </font></td>
//            </tr>
//                                                    </table>
//                    </body>
//            </html>";
//
//
//
//
//        $headers  = 'MIME-Version: 1.0' . "\r\n";
//        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//        $headers .= "From: donotreply@damsdelhi.com" . "\r\n";
//        $headers .= "X-Priority: 3\r\n";
//        $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;
//
//        $success3=mail($email,$subject2,$message3,$headers);
//    }
    /*------------Send Message End-------------------*/
}
header("Location:thanks.php?a=$AuthDescByCCavenue");
?>
