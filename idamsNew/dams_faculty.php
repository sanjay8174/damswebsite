<!DOCTYPE html>
<?php
$aboutUs_Id = $_REQUEST['a'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Faculty, Delhi Academy of Medical Sciences, NEET PG</title>
<meta name="description" content="About the faculty at Delhi Academy of Medical Sciences, one of the best PG Medical Entrance Coaching" />
<meta name="keywords" content="DAMS Faculty, DAMS faculties, Faculties at DAMS, Faculty at DAMS, Faculty in DAMS, Faculty for PG Medical Entrance Exam, Faculty for MD/MS Entrance Examination" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<?php $getAboutusContent = $Dao->getAboutusContent($aboutUs_Id);
$getAboutus = $Dao->getAboutus();  ?>
<section class="inner-banner">
<div class="wrapper">
<article style="background:url('images/background_images/<?php echo $getAboutusContent[0][0];  ?>') right top no-repeat;width:100%;float:left;height:318px;">
<aside class="banner-left">
<?php  echo $getAboutusContent[0][1]; ?>
</aside></article></div></section>
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
<ul>
<li class="bg_none"><a href="<?php echo $getAboutus[0][1]."?a=".$getAboutus[0][0] ?>" title="<?php echo $getAboutus[0][2]?>"><?php echo $getAboutus[0][2]?></a></li>
<li><a title="Our Faculty" class="active-link">Our Faculty</a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<style>
#damsfacultycontent ul li {
list-style-type: disc;
margin-left: 30px;
padding: 4px;
color: #494949;
}
</style>
<div id="damsfacultycontent" class="inner-left-heading responc-left-heading">
<article class="showme-main">
<div class="about-content">
<?php echo $getAboutusContent[0][2]; ?>
</div></article></div></aside>
<aside class="gallery-right">
<?php include 'about-right-accordion.php'; ?>
<?php include 'enquiryform.php'; ?>
</aside></section></div></div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body></html>