<?php  $pageName= substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); ?>

<div id="store-wrapper">
  <div class="dams-store-link"><span></span>DAMS USMLE Edge</div>
  <div class="dams-store-content">
    <div class="inner-store">
      <ul>
        <li class="border_none"><a href="usml-step1.php" title="USMLE STEP-1 (TS)" <?php if($pageName=="usml-step1.php"){?>class="active-store"<?php }?>>USMLE STEP-1 (TS)</a></li>
        <li><a href="usml-step2.php" title="USMLE STEP-2 (TS)" <?php if($pageName=="usml-step2.php"){?>class="active-store"<?php }?>>USMLE STEP-2 (TS)</a></li>
        <li><a href="usmle-egde-combo.php" title="USMLE Combo" <?php if($pageName=="usmle-egde-combo.php"){?>class="active-store"<?php }?>>USMLE Combo</a></li>
        <li><a href="usmle-edge-full-package.php" title="USMLE Full Package" <?php if($pageName=="usmle-edge-full-package.php"){?>class="active-store"<?php }?>>USMLE Full Package</a></li>
      </ul>
    </div>
  </div>
</div>
