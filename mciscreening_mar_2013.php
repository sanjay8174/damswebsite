<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mci-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="mci_screening.php" title="MCI Screening">MCI Screening</a></li>
          <li><a title="Achievement" class="active-link">Achievement</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li id="accor1" onClick="ontab('1');" class="light-blue boder-none">
               <span id="accorsp1" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
                <ol class="achievment-inner display_block" id="aol1">
                  <li><span class="mini-arrow">&nbsp;</span>
                   <a href="mciscreening_sep_2013.php" title="MCI Screening September">MCI Screening September</a>
                  </li>
                  <li class="active-t"><span class="mini-arrow">&nbsp;</span>
                   <a href="mciscreening_mar_2013.php" title="MCI Screening March">MCI Screening March</a>
                  </li>
                </ol>
              </li>
              <li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
                <ol class="achievment-inner display_none" id="aol2">
                  <li><span class="mini-arrow">&nbsp;</span>
                   <a href="mciscreening_sep_2012.php" title="MCI Screening September">MCI Screening September</a>
                  </li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading">
            <h4>MCI Screening March 2013</h4>
            <section class="showme-main">
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="MCI Screening March 2013">MCI Screening March 2013</a></li>
                <li><a href="#official" title="Interview">Interview</a></li>
              </ul>
              <div id="jquery">
                <article class="interview-photos-section">
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> 
                       <img src="images/students/BINITA-CHANDAN.jpg" alt="Dr. BINITA CHANDAN" title="Dr. BINITA CHANDAN" />
                        <p><span>Dr. BINITA CHANDAN</span></p>
                      </div>
                      <div class="students-box"> 
                       <img src="images/students/SHAMIM-JAWITH-ABDUL-MAJEED.jpg" alt="Dr. SHAMIM JAWITH ABDUL MAJEED" title="Dr. SHAMIM JAWITH ABDUL MAJEED" />
                        <p><span>Dr. SHAMIM JAWITH ABDUL MAJEED</span></p>
                      </div>
                      <div class="students-box"> 
                       <img src="images/students/ANUBHA-V-SHARMA.jpg" alt="Dr. ANUBHA V SHARMA" title="Dr. ANUBHA V SHARMA" />
                        <p><span>Dr. ANUBHA V SHARMA</span></p>
                      </div>
                      <div class="students-box"> 
                       <img src="images/students/SAURABH-KAUSHAL.jpg" alt="Dr. SAURABH KAUSHAL" title="Dr. SAURABH KAUSHAL" />
                        <p><span>Dr. SAURABH KAUSHAL</span></p>
                      </div>
                    </li>
                    <li>
                      <div class="students-box border_left_none"> 
                       <img src="images/students/RAHUL-SAXENA.jpg" alt="Dr. RAHUL SAXENA" title="Dr. RAHUL SAXENA" />
                        <p><span>Dr. RAHUL SAXENA</span></p>
                      </div>
                      <div class="students-box"> 
                       <img src="images/students/VIJAY-PRASHANT-RAO.jpg" alt="Dr. VIJAY PRASHANT RAO" title="Dr. VIJAY PRASHANT RAO" />
                        <p><span>Dr. VIJAY PRASHANT RAO</span></p>
                      </div>
                      <div class="students-box"></div>
                      <div class="students-box"></div>
                    </li>
                  </ul>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                  <div class="schedule-mini-series"> 
                   <span class="mini-heading">MCI Screening March - Result 2013</span>
                  <div class="schedule-mini-top"> 
                   <span class="one-part">S. No.</span> <span class="two-part">Student Name</span> <span class="three-part">&nbsp;</span> 
                  </div>
                  <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"><span class="one-parts">1</span> <span class="two-parts schedule-left-line">Dr. BINITA CHANDAN</span> </li>
                        <li><span class="one-parts">2</span> <span class="two-parts schedule-left-line">Dr. SHAMIM JAWITH ABDUL MAJEED</span> </li>
                        <li><span class="one-parts">3</span> <span class="two-parts schedule-left-line">Dr. ANUBHA V SHARMA</span> </li>
                        <li><span class="one-parts">4</span> <span class="two-parts schedule-left-line">Dr. SAURABH KAUSHAL</span> </li>
                        <li><span class="one-parts">5</span> <span class="two-parts schedule-left-line">Dr. RAHUL SAXENA</span> </li>
                        <li><span class="one-parts">6</span> <span class="two-parts schedule-left-line">Dr. VIJAY PRASHANT RAO</span> </li>
                        <li><span class="one-parts">7</span> <span class="two-parts schedule-left-line">Dr. SHUBAIPG(NBE/NEET) Pattern KUAMR</span> </li>
                        <li><span class="one-parts">8</span> <span class="two-parts schedule-left-line">Dr. NAVEEN KUMAR KHANNA</span> </li>
                        <li><span class="one-parts">9</span> <span class="two-parts schedule-left-line">Dr. JENCY DAS</span> </li>
                        <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line" style="color:#000;">And many more...</span> </li>
                      </ul>
                    </div>
                  </div>
                </article>
              </div>
              <div id="official">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/cC2a-En6XK4?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Sonam Verma</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/VK0ol9h2h8c?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Rup Narayan Jha</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/YJaxKtFiAMA?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Rashad Rahib</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/zvRSvxwOfwc?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Rakesh Kela</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </article>
              </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>