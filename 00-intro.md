<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Introduction - Composer</title>
        <meta name="description" content="A Dependency Manager for PHP">
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <link rel="stylesheet" href="/css/style.css?v=10">
        <link rel="stylesheet" href="/css/libs/prism.css?v=6">

        <script src="/js/libs/modernizr-2.0.6.min.js"></script>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/docsearch.js/2/docsearch.min.css">
    </head>

    <body>
        <div id="container">
            <header>
                                    <a href="/"
                           title="Go back to the homepage"
                           aria-label="Go back to the homepage">🏠 Home</a><a class="active"
                           href="/doc/00-intro.md"
                           title="Getting started with Composer"
                           aria-label="Getting started with Composer">Getting Started</a><a class=""
                           href="/download/"
                           title="Go the the Download page to see how to download Composer"
                           aria-label="Go the the Download page to see how to download Composer">Download</a><a class=""
                           href="/doc/"
                           title="View the Composer documentation"
                           aria-label="View the Composer documentation">Documentation</a><a class="last"
                           href="https://packagist.org/"
                           title="Browse Composer packages on packagist.org (external link to Packagist.org)"
                           aria-label="Browse Composer packages on packagist.org (external link to Packagist.org)">Browse
                            Packages</a>                            </header>
            <main role="main">
                <div id="main">
                    
    <div id="searchbar" class="clearfix">
        <input id="docsearch" type="text" placeholder="Type here to search documentation..."
               title="Enter text here and submit to search the documentation"/>
    </div>

            <ul class="toc">
                
            <li>
            <a href="#dependency-management">Dependency management</a> 
                    </li>
            <li>
            <a href="#system-requirements">System Requirements</a> 
                    </li>
            <li>
            <a href="#installation-linux-unix-macos">Installation - Linux / Unix / macOS</a> 
                            <ul>
                        
            <li>
            <a href="#downloading-the-composer-executable">Downloading the Composer Executable</a> 
                            <ul>
                        
            <li>
            <a href="#locally">Locally</a> 
                    </li>
            <li>
            <a href="#globally">Globally</a> 
                    </li>
    
                </ul>
                    </li>
    
                </ul>
                    </li>
            <li>
            <a href="#installation-windows">Installation - Windows</a> 
                            <ul>
                        
            <li>
            <a href="#using-the-installer">Using the Installer</a> 
                    </li>
            <li>
            <a href="#manual-installation">Manual Installation</a> 
                    </li>
    
                </ul>
                    </li>
            <li>
            <a href="#using-composer">Using Composer</a> 
                    </li>
    
        </ul>
    
    <h1 id="introduction">Introduction<a href="#introduction" class="anchor">#</a></h1>
<p>Composer is a tool for dependency management in PHP. It allows you to declare
the libraries your project depends on and it will manage (install/update) them
for you.</p>
<h2 id="dependency-management">Dependency management<a href="#dependency-management" class="anchor">#</a></h2>
<p>Composer is <strong>not</strong> a package manager in the same sense as Yum or Apt are. Yes,
it deals with "packages" or libraries, but it manages them on a per-project
basis, installing them in a directory (e.g. <code>vendor</code>) inside your project. By
default it does not install anything globally. Thus, it is a dependency
manager. It does however support a "global" project for convenience via the
<a href="03-cli.md#global">global</a> command.</p>
<p>This idea is not new and Composer is strongly inspired by node's
<a href="https://www.npmjs.com/">npm</a> and ruby's <a href="https://bundler.io/">bundler</a>.</p>
<p>Suppose:</p>
<ol>
<li>You have a project that depends on a number of libraries.</li>
<li>Some of those libraries depend on other libraries.</li>
</ol>
<p>Composer:</p>
<ol>
<li>Enables you to declare the libraries you depend on.</li>
<li>Finds out which versions of which packages can and need to be installed, and
installs them (meaning it downloads them into your project).</li>
<li>You can update all your dependencies in one command.</li>
</ol>
<p>See the <a href="01-basic-usage.md">Basic usage</a> chapter for more details on declaring
dependencies.</p>
<h2 id="system-requirements">System Requirements<a href="#system-requirements" class="anchor">#</a></h2>
<p>Composer requires PHP 5.3.2+ to run. A few sensitive php settings and compile
flags are also required, but when using the installer you will be warned about
any incompatibilities.</p>
<p>To install packages from sources instead of simple zip archives, you will need
git, svn, fossil or hg depending on how the package is version-controlled.</p>
<p>Composer is multi-platform and we strive to make it run equally well on Windows,
Linux and macOS.</p>
<h2 id="installation-linux-unix-macos">Installation - Linux / Unix / macOS<a href="#installation-linux-unix-macos" class="anchor">#</a></h2>
<h3 id="downloading-the-composer-executable">Downloading the Composer Executable<a href="#downloading-the-composer-executable" class="anchor">#</a></h3>
<p>Composer offers a convenient installer that you can execute directly from the
command line. Feel free to <a href="https://getcomposer.org/installer">download this file</a>
or review it on <a href="https://github.com/composer/getcomposer.org/blob/master/web/installer">GitHub</a>
if you wish to know more about the inner workings of the installer. The source
is plain PHP.</p>
<p>There are in short, two ways to install Composer. Locally as part of your
project, or globally as a system wide executable.</p>
<h4 id="locally">Locally<a href="#locally" class="anchor">#</a></h4>
<p>To install Composer locally, run the installer in your project directory. See
<a href="https://getcomposer.org/download/">the Download page</a> for instructions.</p>
<p>The installer will check a few PHP settings and then download <code>composer.phar</code>
to your working directory. This file is the Composer binary. It is a PHAR
(PHP archive), which is an archive format for PHP which can be run on
the command line, amongst other things.</p>
<p>Now run <code>php composer.phar</code> in order to run Composer.</p>
<p>You can install Composer to a specific directory by using the <code>--install-dir</code>
option and additionally (re)name it as well using the <code>--filename</code> option. When
running the installer when following
<a href="https://getcomposer.org/download/">the Download page instructions</a> add the
following parameters:</p>
<pre><code class="language-bash">php composer-setup.php --install-dir=bin --filename=composer</code></pre>
<p>Now run <code>php bin/composer</code> in order to run Composer.</p>
<h4 id="globally">Globally<a href="#globally" class="anchor">#</a></h4>
<p>You can place the Composer PHAR anywhere you wish. If you put it in a directory
that is part of your <code>PATH</code>, you can access it globally. On Unix systems you
can even make it executable and invoke it without directly using the <code>php</code>
interpreter.</p>
<p>After running the installer following <a href="https://getcomposer.org/download/">the Download page instructions</a>
you can run this to move composer.phar to a directory that is in your path:</p>
<pre><code class="language-bash">mv composer.phar /usr/local/bin/composer</code></pre>
<p>If you like to install it only for your user and avoid requiring root permissions,
you can use <code>~/.local/bin</code> instead which is available by default on some
Linux distributions.</p>
<blockquote>
<p><strong>Note:</strong> If the above fails due to permissions, you may need to run it again
with sudo.</p>
<p><strong>Note:</strong> On some versions of macOS the <code>/usr</code> directory does not exist by
default. If you receive the error "/usr/local/bin/composer: No such file or
directory" then you must create the directory manually before proceeding:
<code>mkdir -p /usr/local/bin</code>.</p>
<p><strong>Note:</strong> For information on changing your PATH, please read the
<a href="https://en.wikipedia.org/wiki/PATH_(variable)">Wikipedia article</a> and/or use
your search engine of choice.</p>
</blockquote>
<p>Now run <code>composer</code> in order to run Composer instead of <code>php composer.phar</code>.</p>
<h2 id="installation-windows">Installation - Windows<a href="#installation-windows" class="anchor">#</a></h2>
<h3 id="using-the-installer">Using the Installer<a href="#using-the-installer" class="anchor">#</a></h3>
<p>This is the easiest way to get Composer set up on your machine.</p>
<p>Download and run
<a href="https://getcomposer.org/Composer-Setup.exe">Composer-Setup.exe</a>. It will
install the latest Composer version and set up your PATH so that you can
call <code>composer</code> from any directory in your command line.</p>
<blockquote>
<p><strong>Note:</strong> Close your current terminal. Test usage with a new terminal: This is
important since the PATH only gets loaded when the terminal starts.</p>
</blockquote>
<h3 id="manual-installation">Manual Installation<a href="#manual-installation" class="anchor">#</a></h3>
<p>Change to a directory on your <code>PATH</code> and run the installer following
<a href="https://getcomposer.org/download/">the Download page instructions</a>
to download <code>composer.phar</code>.</p>
<p>Create a new <code>composer.bat</code> file alongside <code>composer.phar</code>:</p>
<pre><code class="language-bash">C:\bin&gt;echo @php "%~dp0composer.phar" %*&gt;composer.bat</code></pre>
<p>Add the directory to your PATH environment variable if it isn't already.
For information on changing your PATH variable, please see
<a href="https://www.computerhope.com/issues/ch000549.htm">this article</a> and/or
use your search engine of choice.</p>
<p>Close your current terminal. Test usage with a new terminal:</p>
<pre><code class="language-bash">C:\Users\username&gt;composer -V
Composer version 1.0.0 2016-01-10 20:34:53</code></pre>
<h2 id="using-composer">Using Composer<a href="#using-composer" class="anchor">#</a></h2>
<p>Now that you've installed Composer, you are ready to use it! Head on over to the
next chapter for a short and simple demonstration.</p>
<p class="prev-next"><a href="01-basic-usage.md">Basic usage</a> &rarr;</p>

    <p class="fork-and-edit">
        Found a typo? Something is wrong in this documentation?
        <a href="https://github.com/composer/composer/edit/master/doc/00-intro.md"
           title="Go to the doc master to fork and propose updates (external link)"
           aria-label="Go to the doc master to fork and propose updates (external link)">Fork and edit</a> it!
    </p>
                </div>
            </main>
            <footer>
                                
                <p class="license">
                    Composer and all content on this site are released under the <a href="https://github.com/composer/composer/blob/master/LICENSE" title="View the MIT license (external link to GitHub.com)" aria-label="View the MIT license (external link to GitHub.com)">MIT license</a>.
                </p>
            </footer>
        </div>

        <script src="/js/libs/prism.js?v=6"></script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('set', 'anonymizeIp', true);
            ga('create', 'UA-26723099-2', 'auto');
            ga('send', 'pageview');
        </script>
        <script src="https://cdn.jsdelivr.net/docsearch.js/2/docsearch.min.js"></script>
        <script>
            if (document.getElementById('docsearch')) {
                docsearch({
                    apiKey: '8f77725b2f2db4166675acc6e8ea3526',
                    indexName: 'getcomposer',
                    inputSelector: '#docsearch',
                    queryHook: function (query) {
                        var parts = query.split(' ');

                        parts = parts.map(function (part, index) {
                            var word = part.trim();

                            // quote --foo args so they are not parsed as negations but rather return actual results
                            if (word.length >= 2 && word[0] === '-' && word[1] === '-') {
                                word = '"' + word + (index === parts.length - 1 ? '' : '"');
                            }
                            // quote -x args
                            if (word.length == 2 && word[0] === '-') {
                                word = '"' + word + (index === parts.length - 1 ? '' : '"');
                            }

                            return word;
                        });

                        return parts.join(' ');
                    },
                    debug: false
                });
            }
        </script>
    </body>
</html>
