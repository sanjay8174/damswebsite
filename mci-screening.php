<!DOCTYPE html>
<?php
$course_id = '2';
?>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCI Screening Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mci-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left banner-left-postion">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>MCI Screening</h3>
        <p><br>
          <strong> 88% PASS RATE IN MCI SCREENING MARCH 2013<br>
          79% PASS RATE IN MCI SCREENING SEPTEMBER 2012<br>
          </strong> </p>
        <br>
        <p>DAMS has arrived in style in this segment.</p>
        <p> MCI screening passing rates were generally believed to be low, before we started exploring this side. Our focused efforts have consistently given high percentage selection in MCI screening. Poor quality coaching academies functional in this segment are largely believed to be the reason for students not passing this exam. Also this year the MCI screening exam showed a variance in trend with more trickier MCQs and PGME like questions which small tuition house are generally not able to cater. DAMS being a consortium of more than 100 post graduate specialist teachers has the knack of predicting MCQS &amp; helping FMG’s achieve their dreams. Moreover, we have even helped many foreign graduates to become PG in various subjects in India, unlike others who are actually a dead end for foreign medical graduates.<br>
          <br>
          So, our request to FMGE ASPIRANT is choose the leaders in medical education, who are in pg medical exam segment for last 15 years and have presence across India &amp; we promise you will pass the exam in the FIRST ATTEMPT WITH US. Premier institute for PG medical entrance in India, rated by students as number one consortium of educationists with branches all over India since 2000, spearheaded by Sumer Sethi, MD Radiologist and topper in various PG medical entrances now offers unique courses for MCI screening, high yielding and affordable as well. Registered privated limited firm having served more than 50,000 Indian medical graduates now offers its unique courses for FMGs as well. Easly reachable and well located in Gautam Nagar.<br>
          <br>
          Why BEAT ABOUT THE BUSH when we know exactly what is important for you?<br>
          Why waste money when you can get a 5month course with the best in the business?<br>
          Have you tried all other so far and yet results have not been there?<br>
          Do you know most of these so called institutes are not even run by Doctors?<br>
          Clearing MCI is still a dead end if you don’t get through PG special combo packages for PG as well. For PG Medical entrance this year MCI has started new national level exam called as AIPG(NBE/NEET) Pattern which DAMS is the pioneer for providing insight into it. Secure your future with these experienced professionals.<br>
          <br>
          <!--<strong>The batch is starting by 25th April 2013 starting in Gautam Nagar Centre.</strong><br>--> 
          <strong>COURSE DETAILS</strong><br>
          <strong>MCI Regular Course</strong><br>
          <strong>4 Months Programme</strong><br>
          &raquo;   Study Material<br>
          &raquo;   Full day sessions<br>
          &raquo;   Class Tests<br>
          &raquo;   &ldquo; High yielding Golden points &rdquo; after each discussion<br>
          &raquo;   Revision Test Series<br>
        </p>
        <p>So come and join DAMS to fulfill your dreams by preparing for FMGs entrance exams.</p>
      </div>
      <?php include 'mci-middle-accordion.php'; ?>
      <div class="recent-pg-main">
        <div class="recent-pg-heading">
          <div class="slide-arrow" style="margin-right:20px;"> <a class="left" id="next">#</a> <a class="right" id="prev">#</a> </div>
          <a href="photo-gallery.php" class="photogal" style="margin-left:15px; float:left;"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a></div>
        <ul id="recent" style="height:166px; padding-top:0px;">
          <li id="li0"> <img src="images/mci-top/1.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Vaishnavi Dutta Mishra</span> 
            <!--<span class="_rank">Rank : <strong>1</strong></span> --> 
            <span class="_field">MCI Screening</span> </li>
          <li id="li1"> <img src="images/mci-top/2.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Sankii Agrawal</span> 
            <!--<span class="_rank">Rank : <strong>1</strong></span> --> 
            <span class="_field">MCI Screening</span> </li>
          <li id="li2"> <img src="images/mci-top/3.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Arya Trilok Sankhla</span> 
            <!--<span class="_rank">Rank : <strong>1</strong></span> --> 
            <span class="_field">MCI Screening</span> </li>
          <li id="li3"> <img src="images/mci-top/4.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Neeli Neelima Rani</span> 
            <!--<span class="_rank">Rank : <strong>1</strong></span> --> 
            <span class="_field">MCI Screening</span> </li>
          <li id="li4"> <img src="images/mci-top/5.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Vinod Naspuri</span> 
            <!--<span class="_rank">Rank : <strong>1</strong></span> --> 
            <span class="_field">MCI Screening</span> </li>
          <li id="li5"> <img src="images/mci-top/6.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. E.Lakshman Kumar</span> 
            <!--<span class="_rank">Rank : <strong>1</strong></span> --> 
            <span class="_field">MCI Screening</span> </li>
          <li id="li6"> <img src="images/mci-top/7.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Ayan Sarkar</span> 
            <!--<span class="_rank">Rank : <strong>2</strong></span> --> 
            <span class="_field">MCI Screening</span> </li>
          <li id="li7"> <img src="images/mci-top/3.jpg" title="Topper" alt="Topper"/> <span class="_name">Dr. Arya Trilok Sankhla</span> 
            <!--<span class="_rank"><strong>Rank 5</strong></span> --> 
            <span class="_field">MCI Screening</span> </li>
        </ul>
        <div class="view-all-photo"><a href="mciscreening_sep_2013.php" title="View All">View&nbsp;All</a></div>
      </div>
    </aside>
    <aside class="content-right">
      <?php include 'mci-right-accordion.php'; ?>
      <?php
include 'openconnection.php';
$count = 0;$i=0;$newsDetail=array();
$sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
while($row = mysql_fetch_array($sql)){
    $newsDetail[$count] = urldecode($row['HEADING']);
    $count++;
}
?>
      <div class="news-update-box">
        <div class="n-heading"><span></span> News &amp; Updates</div>
        <div class="news-content-box">
          <div style="width:100%; float:left; height:228px; overflow:hidden;">
            <?php
        $j=0;
        for($i=0;$i<ceil($count/3);$i++){  ?>
            <ul id="ul<?php echo $i; ?>" <?php if($i=='0'){ ?> style="display:block;" <?php }else{ ?> style="display:none;" <?php } ?>>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php if($newsDetail[$j]!=''){?>
            <li class="orange"> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            <?php if($newsDetail[$j]!=''){?>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            </ul>
            <?php 
        }
        ?>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="mci-news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
            <div class="slider-mini-dot">
              <ul>
                <?php $i=0; for($i=0;$i<ceil($count/3);$i++){  ?>
                <li id="u<?php echo $i; ?>" <?php if($i=='0'){ ?> class="current" <?php } ?> onClick="news('<?php echo $i; ?>',<?php echo ceil($count/3); ?>);"></li>
                <?php } ?>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
      <div class="news-update-box">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/cC2a-En6XK4" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd1" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/5jaHSl8EHPE" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd2" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/zvRSvxwOfwc" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd3" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/YJaxKtFiAMA" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="mciscreening_sep_2013.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="gallerySlider/owl.carousel.js"></script>
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
<?php mysql_close($myconn); ?>
</body>
</html>
