<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * 
 *      Author Of this Page     :   MAYANK PATHAK
 *      Page Created on `       :   16/Sept/2015    10:12:20
 * 
 */
?>



<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>DAMS Scholarship</title>
        <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="css/responsive.css" rel="stylesheet" type="text/css">
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/responsiveTabs.js"></script>
        <script type="text/javascript">
            function show() {
                $("#dialog").css("display", "block");
                $(".res_main_pop").removeClass("pop_hide");
            }
            function showhide() {
                $(".res_main_pop").addClass("pop_hide");
                setTimeout(function () {
                    $("#dialog").css("display", "none");
                }, 300);

            }

            function regsw() {
                $("#regdlg").css("display", "block");
                $("#dialog").css("display", "none");
                $(".res_main_pop").removeClass("pop_hide");
            }
            function regswhide() {
                $(".res_main_pop").addClass("pop_hide");
                setTimeout(function () {
                    $("#regdlg").css("display", "none");
                }, 300);
            }
            function back() {
                $("#regdlg").css("display", "none");
                $("#dialog").css("display", "block");
            }
        </script>
    </head>

    <body>

        <!--Login popup start on 17/9/2015 by MAYANK PATHAK --> 
        <div id="dialog" style="display: none;">
            <div class="res_pop_bg"></div>     
            <div class="res_main_pop" style="top:57%;">
                <span onclick="showhide()" ></span>
                <div class="pop_title">Login with your Account</div>
                <form action="http://onlinetest.damsdelhi.com/index.php?pageName=submit" method="POST"  name="loginForm" onsubmit="return validateLogin();">
                    <div class="reg_cls_strm">
                        <aside style="width:100%;">
                            <label>Select Year</label>
                            <select name="sessionY" id="sessionY">
                                <option value="0">2014</option>
                                <option value="1" selected="selected">2015</option>
                            </select>
                        </aside>
                    </div>
                    <p>
                        <label>Roll No. / Email</label>
                        <input type="text" id="loginEmail" placeholder="Please enter your Roll No./Email" name="email">
                    </p>
                    <p>
                        <label>Password</label>
                        <input type="password" id="loginPassword" placeholder="Please enter your Password" name="pass">
                    </p>
                    <p>
                        <a href="#" class="forgot_pwd">Forgot your Password ?</a>
                        <input type="submit" value="Login">
                    </p>
                </form>
                <p>
                    <a href="#" class="forgot_pwd" onclick="regsw()">Don't have an account ? <b>Click here for Registration</b></a>
                    <!--<input type="submit" id="btnModalPopup" onclick="regsw()" class="register_btn" value="Register">-->
                </p>
            </div>
        </div>
        <!--Login popup end-->

        <!--Registration popup start by MAYANK PATHAK-->
        <div id="regdlg" style="display: none">
            <div class="res_pop_bg"></div>     
            <div class="res_main_pop registration">
                <!--<b class="reg_back" onClick="regswhide()"></b>-->
                <span onclick="regswhide()" ></span>
                <div class="pop_title">Registration for Online Exam</div>
                <form name="studentRegistration" action="index.php?p=submit" enctype="multipart/form-data" onsubmit="return validateRegistration();" method="post">

                    <input type="hidden" name="sessionY" id="sessionY" value="1" />
                    <input type="hidden" name="mode" id="mode" value="register" />
                    <p>
                        <label>Name</label>
                        <input type="text" id="name"  name="Stuidentname" placeholder="Enter your Name"  >
                    </p>
                    <p>
                        <label>Email Address</label>
                        <input type="text" id="email" name="email" placeholder="Enter your Mail Id">
                    </p>
                    <p>
                        <label>Phone / Mob.</label>
                        <input type="text" id="mobile" name="mobile" autocomplete="off" maxlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" placeholder="Enter your 10 digit Phone/Mobile number">
                    </p>
                    <div class="reg_cls_strm">
                        <aside>
                            <label>Password</label>
                            <input type="password" id="password" name="password" placeholder="Enter Password">
                        </aside>
                        <aside style="float:right;">
                            <label>Confirm Password</label>
                            <input type="password" id="Cnfpassword" name="Cnpassword" placeholder="Confirm Password">
                        </aside>
                    </div>
                    <div class="reg_cls_strm">
                        <aside style="width:100%;">
                            <label>Select Paper Category</label>
                            <select id="paperSelect" name="paperSelect" onchange="showCatDiv(this.value);">
                                <option value="0" selected="selected">Select Category</option>
                                <!--<option value="1"> Paper A</option>-->
                                <option value="2"> Paper B</option>
                                <option value="3"> Paper C</option>
                                <?php
//                                AddressUtil::Itam('course');
//                                
                                ?>
                            </select>
                          
                        </aside>
                        <div class="ermsg" id="paperA">Anatomy, Biochemistry and Physiology (100 Q) Eligible only for prefoundation course
                                      <br>Eligibility: 2/3<sup>rd</sup>  students (1<sup>st</sup> & 2<sup>nd</sup> year)</div>
                        <div class="ermsg" id="paperB">ABP plus pathology, pharmacology, microbiology and forensic (100 ) Eligible only for foundation course<br>Eligibility : 5/6 students (3<sup>rd</sup> & final year)</div>
                        <div class="ermsg" id="paperC">All Subjects 100 Qs Eligible for Regular and Test and discussion courses Interns/Post intern students.</div>
                    </div>
                    
                    <div class="reg_cls_strm">
                        <aside style="width:100%;">
                            <label>Select Course</label>
                            <select id="courseSelect" name="course_Data" >
                                <!--<option value="0" selected="selected">Select Course</option>-->
                                <option value="1" selected="selected"> MD/MS ENTRANCE</option>
<!--                                <option value="2"> MCI SCREENING</option>
                                <option value="3"> MDS QUEST</option>
                                <option value="4"> USMLE EDGE</option>
                                <option value="5"1.5> MRCP</option>
                                <option value="6"> MRCOG</option>
                                <option value="7"> PLAB</option>
                                <option value="8"> MRCGP</option>-->

                                <?php
//                                AddressUtil::Itam('course');
//                                
                                ?>
                            </select>
                            <!--<input type="text"  name="course_name" value="" id="course_name"/>-->
                        </aside>
                    </div>
                    <div class="reg_cls_strm" >
                        <aside>
                            <label>State</label>
                            <select id="stateSelect" name="state" onchange="stateChange(this.value)">
                                <option value="0" selected="selected">Select State</option>
                                <?php
                                AddressUtil::Itam('stateSelect', '1');
                                ?>
                            </select>
                            <input type='hidden' name="stateName" id="stateName" value="" >
                        </aside>
                        <aside style="float:right;" id="cityMapdiv">
                            <label>City</label>
                            <select id="citySelect" name="city">
                                <option value="0" selected="selected">Select City</option>
                                <?php
                                //AddressUtil::Itam('citySelectValue');
                                ?>    
                            </select>
                            <input type='hidden' name="cityName" id="cityName" value="" >
                        </aside>
                    </div>
                    <p>
                     <!--<a href="#" class="acpt_trms"><input type="checkbox" class="chkbx">I accept the <br><small>terms and conditions</small></a>-->
                        <input type="submit" value="Register" id="formSubmit">
                    </p>
                </form>
            </div>
        </div>
        <!--Registration popup end-->
        <header id="nav-anchor" style="text-align: center;">
            <div class="nav_fixed">
                <div class="top_bar"></div>
                <div class="main_wrappr" style="display:inline-block;overflow:visible;" >
                    <a href="index.php" class="logo" style="text-align: left;">
                        <img  src="images/logo.png">
                    </a>
                    <nav class="nav_cntr">
                        <a class="toggleMenu" href="#"></a>
                        <ul class="nav">
                            <li class="test"><a href="#overview" class="selected" id="li_a_overview">Overview </a></li>
                            <!--      <li><a href="#package">Buy Package</a></li>-->
                            <li><a href="#features" id="li_a_features">Features</a></li>
							<!--<li><a href="#" id="li_a_Result">Result<span  class="resultblink"style="" >new</span></a>
							<ul class="restul">
							<li><a href="result/DST-CATEGORY-A.pdf" id="li_a_Result" target="_blank"class="resdropdown">DST-CATEGORY-A</a></li>
							<li><a href="result/DST-CATEGORY-B.pdf" id="li_a_Result" target="_blank" class="resdropdown">DST-CATEGORY-B</a></li>
							<li><a href="result/DST-CATEGORY-C.pdf" id="li_a_Result" target="_blank"  class="resdropdown">DST-CATEGORY-C</a></li>
							</ul>
							</li>-->
                            <li><a href="#photos" id="li_a_photos">Photos<span  class="resultblink"style="" >new</span></a></li>
                            <li><a href="#vedios" id="li_a_vedio">Videos<span  class="resultblink"style="" >new</span></a></li>
                            <li><a href="#contactUs" id="li_a_contactUs">Contact Us</a></li>
                     <!--      <li><a href="#" id="li_a_register" class="last" onclick="regsw()">Register</a></li>-->
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <section class="sld_section">
            <div><img width="100%" src="images/slider_bg.jpg"></div>  
        </section>

        <section>
            <div class="main_wrappr">
                <div id="overview" class="scrl_point"></div>
                <div class="inr_wpr first object">
                    <div class="aftr_inr_cntr">
                        <h3 class="bold_font">Objective :</h3>
                        <p > DAMS Scholarship test is an attempt by DAMS team to honor and nuture talent. We are offering a free courses and discounts all over India. Special scholarship is being offered to the economical challenged students.<sum style="color: red; display: inline; position: absolute; margin-left: -2px; font-size: 24px;">*</sum></p>
                    </div>
                </div>
                <div class="inr_wpr first">
                    


                    <div class="aftr_inr_cntr">

                        <h3 class="bold_font ">DST paper Categories :</h3>

                        <div class="cat">
                            <table class="ctgtbl" width="100%" border="1px #ccc"style="border-radius:10px; border:1px solid #ccc "  >
                                <tr><td colspan="2"> <h4 class="bold_font heading">Different category exams are being offered for medical students in different semesters, Please choose the papers you are eligible for</h4></td></tr>
                                <tr> <td>Category A</td>
                                    <td>
                                        <ul>
                                            <li><strong> Exam date :</strong> 20th August 2018</li>
                                            <li><strong> Late date to Register :</strong> 15th August 2018</li>
                                            <li><strong> Result declaration :</strong> 16th September 2018</li>
                                            <li><strong> Last date to claim the scholarship :</strong> 30th December 2018</li>
                                            <li><strong> Exam Timing :</strong> 4:30pm to 6:30pm</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr><td>Category B</td>
                                    <td>
                                        <ul>
                                            <li><strong> Exam date :</strong> 7th October 2018</li>
                                            <li><strong> Late date to Register :</strong> 30th September 2018</li>
                                            <li><strong> Result declaration :</strong> 20th November 2018</li>
                                            <li><strong> Last date to claim the scholarship :</strong> 30th December 2018</li>
                                            <li><strong> Exam Timing :</strong> 11am to 1pm</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr><td >Category C</td>
                                    <td>
                                        <ul>
                                            <li><strong> Exam date :</strong> 7th October 2018</li>
                                            <li><strong> Late date to Register :</strong> 30th September 2018</li>
                                            <li><strong> Result declaration :</strong> 20th November 2018</li>
                                            <li><strong> Last date to claim the scholarship :</strong> 30th December 2018</li>
                                            <li><strong> Exam Timing :</strong> 11am to 1pm</li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>

                        </div><br>
                        <h3 class="bold_font mark" style="margin-top:25px;">Mark your dates:</h3>
                        <p >Registration can be done online. Please reconfirm the exact timing with local centers.</p>
                        <br>
                        <table width="100%" class="ctgtbl" border="1px solid #ccc">
                            <tr><td class="date">Test Date: </td><td class="date">Timings:</td></tr>
                            <tr><td>20<sup>th</sup> August 2018 (Category A)</td><td>4:30pm to 6:30pm ( 2 Hours)</td></tr>
                            <tr><td>7<sup>th</sup> October 2018 (Category B)</td><td>11am to 1pm ( 2 Hours)</td></tr>
                            <tr><td>7<sup>th</sup> October 2018 (Category C)</td><td>11am to 1pm ( 2 Hours)</td></tr>
                        </table>
                        <br>   <br>
                        <h3 class="bold_font mark">On registration:</h3>
                        <p >Advantages for every Registrant.   </p>
                        <p>   Prizes at both national and state levels. </p><br>

                        <table width="100%"   border="1px solid #ccc" class="ctgtbl">
                            <tr>
                                <td class="date">Registration: </td>
                                <td class="date">Category </td>
                                <td class="date">Last date: </td><td class="date">Result declaration:</td></tr>
                            <tr><td rowspan="3">Online</td>
                                <td>Category A</td>
                                <td>15<sup>th</sup> August 2018</td>
                                <td>16<sup>th</sup> September 2018</td>
                            </tr>
                            <tr>
                                <td>Category B</td>
                                <td>30<sup>th</sup> September 2018</td><td>20<sup>th</sup> November 2018</td>
                            </tr>
                            <tr>
                                <td>Category C</td>
                                <td>30<sup>th</sup> September 2018</td><td>20<sup>th</sup> November 2018</td>
                            </tr>
                        </table>
                        <div class="award">
                            <div class="n-award"><span></span>National Award Ceremony – 10th December 2018</div><div class="l-date"><span></span>Last date for claiming scholarship - 30th December 2018</div>

                        </div>
                    </div>
                </div>



                <div class="inr_wpr">
                    <div class="ctnt_heading scrl_prnt">
                        <div id="features" class="scrl_point"></div>
                        <h1 class="extr_bold">Benefits to student</h1>
                    </div>
                    <h2>ALL INDIA RANKING FOR ALL CATEGORIES</h2>
                    <div class="aftr_inr_cntr featrs_wpr">
                        <ul>
                            <li><span></span><b style="width:auto;margin:0;">TOP 10 -</b> FREE FOR ALL COURSES INCLUDING CLASSROOM COURSES, ONLINE TEST SERIES &amp;POSTAL COURSES (ACROSS INDIA)</li>							
                            <li><span></span><b style="width:auto;margin:0;">11-20 –</b> 50% DISCOUNT ON ALL COURSES INCLUDING CLASSROOM COURSES, ONLINE TEST SERIES &amp; POSTAL COURSES (ACROSS INDIA)</li>
                           <li><span></span><b style="width:auto;margin:0;">21-50-</b>  25% DISCOUNT ON ALL COURSES INCLUDING CLASSROOM COURSES, ONLINE TEST SERIES &amp; POSTAL COURSES (ACROSS INDIA)</li>
                            <li><span></span><b style="width:auto;margin:0;">51-100- </b> 10% DISCOUNT ON ALL COURSES INCLUDING CLASSROOM COURSES, ONLINE TEST SERIES &amp; POSTAL COURSES (ACROSS INDIA)</li>
                            

                        </ul>
                        <h2 style="margin-left:0px;">NOTE: Only All India Ranking will be considered.</h2>
                    </div>
                    
                    <!--<div class="aftr_inr_cntr featrs_wpr">
                        <ul>
                            <li><span></span>FREE Coaching to Top 20 students anywhere in the country</li>
			    <li><span></span>Top 3 rankers at national level get I-DAMS (Category C)</li>							
                            <li><span></span>Discounts available for economically challenged students. (documentation required)<sum style="color: red; display: inline; position: absolute; margin-left: 0px; font-size: 24px;">*</sum></li>
                            <li><span></span>Lucrative scholarship based on state ranks (can claim only national level or state level)</li>

                        </ul>
                    </div>-->
                    <!--<h2>If they do not   Join a classroom course</h2>-->
                    <!--<div class="aftr_inr_cntr featrs_wpr">
                        <ul>
                            <li><span></span>Top 3 rankers at national level get I DAMS ( Category C)</li>
                            <li><span></span> Top 50 Online Test series free (All categories) </li>
                            <li><span></span>All students get 25 % concession on joining postal/online test-Series(Have to claim till 31st December)</li>
                            <li><span></span>DAMS Harrison 19th Notebook for Top 100. HANDBOOK ON Radiology by Dr Sumer Sethi to Top 200 students. </li>
                            <li><span></span>Suggestions for improving performance</li>
                            <li><span></span>Projected National Rank in AIPG/AIIMS</li>
                            <li><span></span>Detailed Analysis highlighting strong &amp; weak areas</li>
                            <li><span></span>Free invitation to motivational &amp; exam orientation seminars for 6months</li>

                        </ul>
                    </div>-->
                </div>
                <div class="inr_wpr first">
                    <div class="aftr_inr_cntr note ">
                        <p class="stu-detail" ><span></span>Students can claim only one advantage either classroom or non-classroom. No other concession can be clubbed with this offer. </p>
                    </div>
                </div>
                <div class="inr_wpr">
                    <div class="ctnt_heading scrl_prnt center">
                        <div id="features" class="scrl_point"></div>
                        <h1 class="extr_bold">Centres</h1>
                    </div>
                    <div class="cntr_detail">
                        <div class="cntr-list andhara">
                            <p>andra PRADESH</p>
                            <ul>
                                <li> GUNTUR</li>
                                <li>HYDERABAD(R.T.C. X ROAD)</li>
                                <li>HYDERABAD(JUBILEE HILLS)</li>
                                <li>KAKINADA</li>
                                <li>KARIM NAGAR</li>
                                <li>KHAMAM</li>
				<li>KURNOOL</li>
				<li>SRIKAKULAM</li>
                                <li>TIRUPATI</li>
                                <li>VIJYAWADA</li>
                                <li>VISHAKAPATNAM</li>
                                <li>WARANGAL</li>

                            </ul>
                            <p>ASSAM</p>
                            <ul>
                                <li>DIBRUGARH</li>
                                <li>GUWAHATI</li>
                                <li>JORHAT</li>
                                <li>SILCHAR</li>                                
                            </ul>
                              <p>BIHAR</p>
                            <ul >
				<li>BHAGALPUR</li>
                                <li>DARBHANGA</li>
                                <li>PATNA</li>
				<li>GAYA</li>
                            </ul>
                            <p>CHHATTISGARH</p>
                            <ul> <li> BILASPUR</li>
                                <li> DURG(BHILAI)</li>
                                <li>JAGDALPUR </li>
                                <li>RAIPUR</li>
                            </ul>
                            <p>DELHI</p>
                            <ul> 
                                <li> DILSHAD GARDEN</li>
                                <li>GUATAM NAGAR </li>
                            </ul>
                         <p class="blank">GOA</p>
                            <ul>
                            </ul> 
                        </div>
                        <div class="cntr-list bihar">
		
                             <p>GUJARAT</p>
                            <ul><li>AHMEDABAD </li>                                
				<li>JAMNAGAR</li>
				<li>RAJKOT</li>
                                <li>SURAT</li>
                                <li>VADODARA</li>
                            </ul>  


                          <p>HARIYANA</p>
                          
                            <ul><li>AGROHA</li>
				<li>GURGAON</li>
				<li>KHANPUR</li>
                                <li>MEWAT(NUH)</li>
                            <li>ROHTAK</li>
                                
                            </ul>
                           <p>HIMACHAL</p>
                            <ul><li>SHIMLA    </li>
                                <li>TANDA</li>
                            </ul>
                            <p>J&K</p>
                            <ul>  
                                <li>JAMMU    </li>
                                <li>SRINAGAR(UK)</li>
                                <li>SRINAGAR(J&K)</li>
                            </ul>
                            <p>JHARKHAND</p>
                            <ul>  
                                <li>JAMSHEDPUR   </li>
				<li>RANCHI    </li>
                            </ul>    

                              <p>UTTRAKHAND</p>
                            <ul>
                                <li>DEHRADUN (MALVIYA NAGAR)    </li>
                                <li>DEHRADUN (ADARSH NAGAR)    </li>
                                <li>HALDWANI</li>
                                <li>SRINAGAR</li>
                            </ul> 

                              <p>Manipur</p>
                            <ul>
                             <li>IMPHAL</li>
                            </ul>                         
                        </div>

                        <div class="cntr-list HARIYANA">



                             <p>KERALA</p>
                            <ul>
				<li>CALICUT   </li>
				<li>COCHIN    </li>
				<li>KOTTAYAM (GANDHI NAGAR)  </li>
                                <li>KOTTAYAM (AYRATTUNADA)  </li>
                                <li>KOLLAM</li>
                                <li>KANNUR</li>
                                <li>MALAPPURAM</li>
				<li>THRISSUR   </li>
                                <li>TRIVANDRUM-1</li>
                                <li>TRIVANDRUM-2</li>
                                <li>THIRUVALLA</li>
                            </ul>
                            <p>MADHYA PRADESH</p>
                            <ul>  <li>BHOPAL</li>
				<li>GWALIOR</li>
                                <li>INDORE</li>
                                <li>JABALPUR</li>
                                <li>REWA</li>
                                <li>SAGAR</li>
                            </ul>  

                          
                             <p>WEST BENGAL</p>
                            <ul class="maha"> 
                                <li>BANKURA  </li>
				<li>KOLKATA    </li>
                                <li>SILIGURI  </li>
                            </ul>                         
                          <p>TELANGANA</p>
                            <ul>  
                                <li>KARIMNAGAR</li>
                                <li>KHAMMAM</li>
                            </ul>                           
                            <p>ODISHA</p>
                            <ul> 
                                <li>BERHAMPUR    </li>
                                <li>BHUBANESHWAR  </li>
                                <li>BURLA  </li>
                                <li>CUTTAK</li>
                            </ul>
                        </div>
                        <div class="cntr-list KARNATAKA">
                            <p>RAJASTHAN</p>
                            <ul> 
                                <li>AJMER    </li>
                                <li>BIKANER  </li>
                                <li>JAIPUR     </li>
                                <li>JODHPUR  </li>
                                <li>JHALAWAR  </li>
                                <li>UDAIPUR</li>
                                <li>KOTA</li>
                            </ul> 
                            <p>UTTAR PRADESH</p>
                            <ul>  
                                <li>AGRA    </li>
                                <li>ALIGARH  </li>
                                <li>ALLAHABAD    </li>
                                <li>AMBEDKARNAGAR  </li>
                                <li>AZAMGARH</li>
                                <li>BAREILLY(BHOJIPURA)    </li>
                                <li>BAREILLY(RAMPUR)  </li>
                                <li>BAREILLY(ASHISH ROYAL PARK)  </li>
                                <li>ETAWAH</li>
				<li>GHAZIABAD</li>
                                <li>GORAKHPUR</li>
				<li>JHANSI</li>
                                <li>KANPUR</li>
                                <li>LUCKNOW</li>
                                <li>MEERUT</li>
				<li>ORAI</li>
				<li>SAFAI</li>
                                <li>VARANASI</li>
                            </ul>

                            <p>TRIPURA</p>
                            <ul>  <li>AGARTALA   </li>

                            </ul>

			   <p>TAMILNADU</p>
                            <ul>  
                                <li>CHENNAI    </li>
				<li>COIMBATORE    </li>
                                <li>DHARMAPURI  </li>
				<li>SALEM   </li>
                                <li>MADURAI   </li> 
								<li>Nagercoil</li>
								<li>Tirunelveli</li>
                            </ul> 
                           


                        </div>
                        <div class="cntr-list maha">
                             <p>MAHARASTRA</p>
                            <ul class="maha">  <li>AHMEDNAGAR    </li>
                                <li>AKOLA</li>
                                <li>AMBAJOGAI</li>
                                <li>AMRAVATI</li>
                                <li>AURANGABAD</li>
                                <li>AHMEDNAGAR(LONI)</li>
                                <li>DHULE</li>
                                <li>KOHLAPUR</li>
                                <li>MUMBAI (DADAR (EAST))</li>
                                <li>MUMBAI(NAVI MUMBAI)</li>
                                <li>LATUR</li>
                                <li>MIRAJ</li>
                                <li>NASHIK</li>
                                <li>NAGPUR</li>
                                <li>NANDED</li>
                                <li>PUNE</li>
				<li>RATNAGIRI</li>								
                                <li>SATARA</li>
                                <li>SOLAPUR</li>
                                <li>TALEGAON</li>  
                                <li>WARDHA</li>
                                <li>YAVATMAL</li>  								
                            </ul>                             

                        </div>
                        <div class="cntr-list ODISHA">


                            <p>PUNJAB</p>
                            <ul > 
                                <li>AMRITSAR    </li>
                                <li>BATHINDA  </li>
                                <li>CHANDIGARH    </li>
                                <li>FARIDKOT  </li>
                                <li>JALANDHAR    </li>
				<li>LUDHIANA   </li>
				<li>PATHANKOT</li>
                                <li>PATIALA  </li>
                            </ul> 
                          
                            <p class="blank">PONDICHERRY</p>
                            <ul>
                            </ul>

                        </div>


                    </div>

                </div>
                </div>
                <div class="main_wrappr main_wrappr_photo_sec">
                <div class="inr_wpr">
                    <div class="ctnt_heading scrl_prnt center">
                        <div id="photos" class="scrl_point"></div>
                        <h1 class="extr_bold">Photos</h1>
                    </div>
                <div class="cntr_detail cntr_detail_photo">    
                   <div class="responsive-tabs">
                       <h2>2018</h2>
                <div class="tabsshowhide">
                        
                    <div class="cntr_detail cntr_detail_photo">
                        <ul>
                            <li class="photo">
                                <img class="image_student" src="images/students/AISHWARYA_PANT.JPG"> 
                                <p style="padding-top:10px;text-align: center">Dr. AISHWARYA PANT</p>
                                <p style="padding-top:10px;text-align: center">(Category-A, Rank-7)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/BALAJI_DATTACHARYA_DUTTA.JPG">
                              <p style="padding-top:10px;text-align: center">Dr. BALAJI DATTACHARYA DUTTA </p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-1)</p>
                            </li> 
                            <li class="photo">
                              <img class="image_student" src="images/students/DIVYANSH_GOYAL.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr. DIVYANSH GOYAL </p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-2)</p>
                            </li> 

                            <li class="photo">
                                <img class="image_student" src="images/students/MUKESH_SIDANA.jpg" style="height: 214px">
                              <p style="padding-top:10px;text-align: center">Dr. MUKESH SIDANA</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-66)</p>
                            </li>                            
                            <li class="photo">
                              <img class="image_student" src="images/students/NAMAN_GUPTA.jpg"> 
                              <p style="padding-top:10px;text-align: center">Dr. NAMAN GUPTA </p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-47)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/RAHUL_AMITABH.JPG">
                              <p style="padding-top:10px;text-align: center">Dr.RAHUL AMITABH </p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-4)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/SIDDARTH_SARAF.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr. SIDDARTH SARAF </p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-10)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/VISHAKHA_NARSHNEY.jpg" style="height: 214px"> 
                              <p style="padding-top:10px;text-align: center">Dr. VISHAKHA NARSHNEY</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-31)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/DSC_0275.JPG"> 
                              <p style="padding-top:10px;text-align: center">Group Photo</p>
                              <p style="padding-top:10px;text-align: center"> &nbsp;</p>
                              
                            </li>
                            
                        </ul>   
                    </div>
                    </div>
                   <h2>2017</h2>
                        <div class="tabsshowhide">
                            <div class="cntr_detail cntr_detail_photo">
                                <ul>
                                    <li class="photo">
                                        <img class="image_student" src="images/students/CATEGORY_D_1.JPG"> 
                                        <p style="padding-top:10px;text-align: center">Dr.Ahmad Ozair</p>
                                        <p style="padding-top:10px;text-align: center">(Category-A, Rank-1)</p>
                                    </li>
                                    <li class="photo">
                                        <img class="image_student" src="images/students/CATEGORY_D_2.JPG"> 
                                        <p style="padding-top:10px;text-align: center">Dr.Kashish Malhotra</p>
                                        <p style="padding-top:10px;text-align: center">(Category-A, Rank-2)</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                <h2>2016</h2>
                <div class="tabsshowhide">
                        
                    <div class="cntr_detail cntr_detail_photo">
                        <ul>
                            <li class="photo">
                                <img class="image_student" src="images/students/CATEGORY_A_1.png"> 
                                <p style="padding-top:10px;text-align: center">Dr. Anubhav Agrawal</p>
                                <p style="padding-top:10px;text-align: center">(Category-A, Rank-1)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_A_2.png">
                              <p style="padding-top:10px;text-align: center">Dr. Lucky Singhal </p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-4)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_A_3.png">
                              <p style="padding-top:10px;text-align: center">Dr. Rohit Gupta</p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-1)</p>
                            </li>
                            <li class="photo">
                                <img class="image_student" src="images/students/CATEGORY_A_4.png"> 
                                <p style="padding-top:10px;text-align: center">Dr. Sabal Salija  </p>
                                <p style="padding-top:10px;text-align: center">(Category-A, Rank-4)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_A_5.png">
                              <p style="padding-top:10px;text-align: center">Dr. Shiva Kanaujiya </p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-6)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_A_6.png">
                              <p style="padding-top:10px;text-align: center">Dr. Shivang Singh </p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-6)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_A_7.png">
                              <p style="padding-top:10px;text-align: center">Dr. Vipul</p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-10)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_A_8.png">
                              <p style="padding-top:10px;text-align: center">Dr. Vishavjit Singh </p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-3)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_B_1.png">
                              <p style="padding-top:10px;text-align: center">Dr. Damini S. P.</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-1)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_B_2.png">
                              <p style="padding-top:10px;text-align: center">Dr. Nalini Prajapati</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-11)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_B_3.png">
                              <p style="padding-top:10px;text-align: center">Dr. Nidhi Yadav</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-6)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_B_4.png">
                              <p style="padding-top:10px;text-align: center">Dr. Sarthak Mehta </p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-21)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_B_5.png">
                              <p style="padding-top:10px;text-align: center">Dr. Shah Suchit</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-7)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_B_6.png">
                              <p style="padding-top:10px;text-align: center">Dr. Shung Ming Chiu</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-17)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_B_7.png">
                              <p style="padding-top:10px;text-align: center">Dr. Vishal Tyagi</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-38)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_B_8.png">
                              <p style="padding-top:10px;text-align: center">Dr. Yogini Nalwaya</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-11)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_C_1.png">
                              <p style="padding-top:10px;text-align: center">Dr. Anchal Sahni </p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-131)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_C_2.png">
                              <p style="padding-top:10px;text-align: center">Dr. Auish Jain</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-8)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_C_3.png">
                              <p style="padding-top:10px;text-align: center">Dr. Gawtham Reddy</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-20)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_C_4.png">
                              <p style="padding-top:10px;text-align: center">Dr. Kali Prasad Rath</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-4)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_C_5.png">
                              <p style="padding-top:10px;text-align: center">Dr. Kuldeep Sharma</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-67)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_C_6.png">
                              <p style="padding-top:10px;text-align: center">Dr. Mohit Mangla</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-24)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_C_7.png">
                              <p style="padding-top:10px;text-align: center">Dr. Nikhil Singhania</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-5)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_C_8.png">
                              <p style="padding-top:10px;text-align: center">Dr. Sunil Kumar Shukla</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-17)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/CATEGORY_C_9.png">
                              <p style="padding-top:10px;text-align: center">Dr. Vimal Kumar varma</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-14)</p>
                            </li>
    


                        </ul>   
                    </div>
                    </div>
                <h2>2015</h2>
                <div class="tabsshowhide">
                        
                    <div class="cntr_detail cntr_detail_photo">
                        <ul>
                            <li class="photo">
                                <img class="image_student" src="images/students/akshay_ganesh_kumar.JPG"> 
                                <p style="padding-top:10px;text-align: center">Dr.Akshay Ganesh Kumar</p>
                                <p style="padding-top:10px;text-align: center">(Category-A, Rank-13)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/harsh_sura.JPG">
                              <p style="padding-top:10px;text-align: center">Dr.Harsh Sura</p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-10)</p>
                            </li> 
                            <li class="photo">
                              <img class="image_student" src="images/students/ketan_bhatnagar.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Ketan Bhatnagar</p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-2)</p>
                            </li> 

                            <li class="photo">
                              <img class="image_student" src="images/students/komalgupta.JPG">
                              <p style="padding-top:10px;text-align: center">Dr.Komal Gupta</p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-18)</p>
                            </li>                            
                            <li class="photo">
                              <img class="image_student" src="images/students/umang_arora.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Umang Arora</p>
                              <p style="padding-top:10px;text-align: center">(Category-A, Rank-11)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/ayush_jain.JPG">
                              <p style="padding-top:10px;text-align: center">Dr.Ayush Jain</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-4)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/deekshagoyal.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Deeksha Goyal</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-19)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/rajiv_kumar.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Rajiv Kumar</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-16)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/reshamsingh.JPG">   
                              <p style="padding-top:10px;text-align: center">Dr.Resham Singh</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-1)</p>
                            </li>

                            <li class="photo">
                              <img class="image_student" src="images/students/sumegha_mittal.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Sumegha Mittal</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-13)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/vaibhav_trivedi.JPG">
                              <p style="padding-top:10px;text-align: center">Dr.Vaibhav Trivedi</p>
                              <p style="padding-top:10px;text-align: center">(Category-B, Rank-13)</p>
                            </li> 
                            <li class="photo">
                              <img class="image_student" src="images/students/anjali_vats.JPG">
                              <p style="padding-top:10px;text-align: center">Dr.Anjali Vats Kumar</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-32)</p>
                            </li>                            
                            <li class="photo">
                              <img class="image_student" src="images/students/kautilya_patel.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Kautilya Patel</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-15)</p>
                            </li>

                            <li class="photo">
                              <img class="image_student" src="images/students/kundan_gupta.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Kundan Gupta</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-2)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/rahul_roy.JPG">  
                              <p style="padding-top:10px;text-align: center">Dr.Rahul Roy.JPG</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-21)</p>
                            </li>                            
                            <li class="photo">
                              <img class="image_student" src="images/students/sanghamitra_mandal.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Sanghamitra Mandal</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-91)</p>
                            </li>
                            <li class="photo">
                              <img class="image_student" src="images/students/vinaygoel.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Vinay Goel</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-12)</p>
                            </li>                            
                            <li class="photo">
                              <img class="image_student" src="images/students/zaheen_khan.JPG"> 
                              <p style="padding-top:10px;text-align: center">Dr.Zaheen Khan</p>
                              <p style="padding-top:10px;text-align: center">(Category-C, Rank-16)</p>
                            </li>

                            <li class="photo">
                              <img class="image_student" src="images/students/group.JPG"> 
                              <p style="padding-top:10px;text-align: center">Group Photo</p>
                              <p style="padding-top:10px;text-align: center"> &nbsp;</p>
                              
                            </li>
                            
                        </ul>   
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    <div class="main_wrappr">
                <div class="inr_wpr">
                    <div class="ctnt_heading scrl_prnt center">
                        <div id="vedios" class="scrl_point"></div>
                        <h1 class="extr_bold">Videos</h1>
                    </div>
                    <div class="cntr_detail">
                        <ul>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/dfMBVdBntkQ" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/jOTpcT09APc" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/e1YIb_AH6vg" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/DGQ_cvnUEkQ" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/7KhK2x90jks" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/p43dA0mhXhs" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/yD8BY6mYmAE" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/SlLrg81ukdk" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/gk87z8xiRIo" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/-n8LFtdD7xE" frameborder="0" allowfullscreen></iframe>
                            </li>
                            <li class="vedio">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/HJ6f3jr0sWw" frameborder="0" allowfullscreen></iframe>
                            </li>
                            
                        </ul>   
                    </div>
                    </div>
                <div class="inr_wpr">
                    <div class="ctnt_heading scrl_prnt">
                        <div id="features" class="scrl_point"></div>
                        <h1 class="extr_bold">LOW INCOME CRITERIA </h1>
                    </div>
                    <h2 style="font-size: 16px;">(CONDITIONS FOR SCHOLARSHIP)</h2>
                    <div class="aftr_inr_cntr featrs_wpr">
                        <ul>
                            <li><span></span>Scholarship shall be awarded to the students who have secured not less than <b style="display: inline;width: auto;margin-left: 0px;"> 70% marks in the test conducted by DAMS for granting scholarship</b> and the annual income of whose parents/guardian from all sources does not exceed Rs. 300,000/ per annum. Students having the lowest family income shall be given preference in the ascending order.</li>
			    <li><span></span>The Scholarship shall not be given on the basis of Caste.</li>
			    <li><span></span>Scholarship shall not be awarded for more than the normal period of time taken to complete the Coaching course.</li>
			    <li><span></span>Scholarship shall not be given to more than one student in a family.</li>
			    <li><span></span> The students should be regular in attendance.</li>
			    <li><span></span>Income certificate should be on self-certification basis by way of an affidavit on non-judicial stamp paper for self-employed parents and from employer for employed parents and for farmers submit their agriculture land records.</li>
			    <li><span></span>If a student violates any other term and condition of the scholarship, the scholarship may be suspended or cancelled.</li>
			    <li><span></span> If a student is found to have obtained a scholarship by false statement/certificates, his/her scholarship shall be cancelled forthwith and the amount of the scholarship paid shall be recovered, at the discretion of the concerned Divisional Administration of DAMS.</li>
			    <li><span></span>DAMS will lay down the detailed procedure for processing and sanctioning of scholarships to eligible students.</li>
			    <li><span></span>The scheme shall be evaluated at regular intervals by the DAMS.</li>
			    <li><span></span>The regulations can be changed at any time at the discretion of the Board of DAMS.</li>							                           
                        </ul>
                    </div>                    
                </div>
                
                <div class="inr_wpr footer_links_wpr">
                    <div id="contactUs" class="" ></div>
                    <div class="ftr_inr_cntr">

                        <article class="info_prt important-dates">
                            <h3 class="ft_ttl bold_font">Imporatant Dates</h3>
                            <ul>

                                <li><a>Registration of LAST DATE : 15<sup>th</sup> August 2018 (CATEGORY A) and  30 <sup>th</sup> September 2018 (CATEGORY B & C)</a></li>
                                <li><a>DAMS SCHOLARSHIP “TEST” (DST):20<sup>th</sup> August 2018 (CATEGORY A) and  07 <sup>th</sup> October 2018 (CATEGORY B & C)</a></li>
                                <li><a >Result declaration:16<sup>th</sup> September 2018 (CATEGORY A) and 20 <sup>th</sup> November 2018 ( CATEGORY B & C)</a></li>
                                <li><a >National Award Ceremony:10<sup>st</sup> December 2018</a></li>
                                <!--<li><a >Last date for claiming scholarship-Nov 30, 2015 </a></li>-->
                            </ul>
                        </article>

                        <article class="info_prt info_prt2 contact-box">
                            <h3 class="ft_ttl bold_font">Contact Us</h3>   
                            <div class="cpyrt_dtls">
                                <!--Mobile : <strong>+91 9873314110, 9811862082</strong> <br>-->
                                Landline No : <strong>011 - 40094009</strong> <br> 
                                Email : <strong>info@damsdelhi.com</strong> <br> 
                                Website : <strong>www.damsdelhi.com</strong>
                            </div>
                        </article>

                        <article class="followuson">
                            <h3 class="ft_ttl bold_font">Follow us on</h3>
                            <a href="https://www.facebook.com/damsdelhiho" class="fb"></a>
                            <a href="https://twitter.com/damsdelhi" class="twt"></a>
                            <a href="https://plus.google.com/u/0/110912384815871611420/about" class="gpls"></a>
                            <a href="http://www.youtube.com/user/damsdelhi" class="yt"></a>

                        </article>
                    </div>
                </div>
                
            </div>
        
        
    </section>
    <footer>
        <div class="main_wrappr">
            <p class="foot_txt">© Delhi Academy of Medical Sciences Pvt. Ltd. All rights reserved.</p>
        </div>
    </footer>
    <!--Nav fixed js start-->
    <script src="js/jquery-1.9.1.min.js"></script> 
    <script type="text/javascript">

	$(document).ready(function () {
                   RESPONSIVEUI.responsiveTabs();
		$(window).scroll(function () {
			var window_top = $(window).scrollTop() + 0;
			var div_top = $('#nav-anchor').offset().top;
			if (window_top > div_top) {
				$('.nav_fixed').addClass('stick');
			} else {
				$('.nav_fixed').removeClass('stick');
			}
		});

	});

    </script> 
    <!--Nav fixed js end-->

    <!--Main navigation start-->
<!--    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>-->
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/registration.js"></script>
    <script>
	$(function () {
		$('a[href*=#]:not([href=#])').click(function () {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});
    </script>
    <!--Main navigation end-->
    <!--Tabs script start-->
    <script>
        $(document).ready(function () {
            $("#content .sw_hd").hide();
            $("#tabs li:first").attr("id", "current");
            $("#content .sw_hd:first").fadeIn();

            $('#tabs a').click(function (e) {
                e.preventDefault();
                $("#content .sw_hd").hide();
                $("#tabs li").attr("id", "");
                $(this).parent().attr("id", "current");
                $('#' + $(this).attr('title')).fadeIn();
            });
        })();
    </script>

    <!--Tabs video slider code start-->
    <script type="text/javascript">
        //1. set ul width 
        //2. image when click prev/next button
        var ul;
        var li_items;
        var imageNumber;
        var imageWidth;
        var prev, next;
        var currentPostion = 0;
        var currentImage = 0;


        function init() {
            ul = document.getElementById('image_slider');
            li_items = ul.children;
            imageNumber = li_items.length;
            imageWidth = li_items[0].children[0].clientWidth;
            ul.style.width = parseInt(imageWidth * imageNumber) + 'px';
            prev = document.getElementById("prev");
            next = document.getElementById("next");
            //.onclike = slide(-1) will be fired when onload;
            /*
             prev.onclick = function(){slide(-1);};
             next.onclick = function(){slide(1);};*/
            prev.onclick = function () {
                onClickPrev();
            };
            next.onclick = function () {
                onClickNext();
            };
        }

        function animate(opts) {
            var start = new Date;
            var id = setInterval(function () {
                var timePassed = new Date - start;
                var progress = timePassed / opts.duration;
                if (progress > 1) {
                    progress = 1;
                }
                var delta = opts.delta(progress);
                opts.step(delta);
                if (progress == 1) {
                    clearInterval(id);
                    opts.callback();
                }
            }, opts.delay || 17);
            //return id;
        }

        function slideTo(imageToGo) {
            var direction;
            var numOfImageToGo = Math.abs(imageToGo - currentImage);
            // slide toward left

            direction = currentImage > imageToGo ? 1 : -1;
            currentPostion = -1 * currentImage * imageWidth;
            var opts = {
                duration: 500,
                delta: function (p) {
                    return p;
                },
                step: function (delta) {
                    ul.style.left = parseInt(currentPostion + direction * delta * imageWidth * numOfImageToGo) + 'px';
                },
                callback: function () {
                    currentImage = imageToGo;
                }
            };
            animate(opts);
        }

        function onClickPrev() {
            if (currentImage == 0) {
                slideTo(imageNumber - 1);
            }
            else {
                slideTo(currentImage - 1);
            }
        }

        function onClickNext() {
            if (currentImage == imageNumber - 1) {
                slideTo(0);
            }
            else {
                slideTo(currentImage + 1);
            }
        }

        window.onload = init;
    </script>
    <!--Tabs video slider code start-->
    <!--Tabs script end-->

    <!--Testimonila script start-->
    <script type="text/javascript">
        $(document).ready(function (fadeLoop) {

            var fad = $('.fader');
            var counter = 0;
            var divs = $('.fader').hide();
            var dur = 500;

            fad.children().filter('.fader').each(function (fad) {

                function animator(currentItem) {

                    animator(fad.children(":first"));

                    fad.mouseenter(function () {
                        $(".fader").stop();
                    });
                    fad.mouseleave(function () {
                        animator(fad.children(":first"));
                    });
                }
                ;

            });

            function showDiv() {
                divs.fadeOut(dur)
                        .filter(function (index) {
                            return index == counter % divs.length;
                        })
                        .delay(dur)
                        .fadeIn(dur);
                counter++;
            }
            ;

            showDiv();

            return setInterval(function () {
                showDiv();
            }, 2 * 5000);
        });

    </script>
    <!--Testimonial script end-->

    <script type="text/javascript">
        //srcipt to show stream
        $(document).ready(function () {
<?php
if ($_REQUEST[msg] == '1') {
    $rlt = explode('?', $_SERVER[SERVER_NAME] . '' . $_SERVER[REQUEST_URI]);
    ?>
                alert('Email already exist');
                var xyz = '<?php echo $rlt[0]; ?>';
                $("#modeURL").val(xyz);
<?php } ?>


<?php if ($_REQUEST[m] == '1') {
    ?>
                var message = 'You are successfully registered to DAMS SCHOLARSHIP';
                alert(message);

<?php } ?>

<?php if ($_REQUEST[m] == '2') {
    ?>
                var message = 'This Email is already registered to DAMS SCHOLARSHIP';
                alert(message);

<?php } ?>
<?php if ($_REQUEST[m] == '3') {
    ?>
                var message = 'Registration Failed due to some technical errors please try after sometime';
                alert(message);

<?php } ?>

        });


        function showCatDiv(val){
            if(val==1){
                $("#paperA").show();$("#paperB").hide();$("#paperC").hide();
            }else if(val==2){
                $("#paperB").show();$("#paperA").hide();$("#paperC").hide();
            }else if(val==3){
                $("#paperC").show();$("#paperA").hide();$("#paperB").hide();
            }else{
                 $("#paperC").hide();$("#paperA").hide();$("#paperB").hide();
            }
        }

    </script>
        <script>

                 RESPONSIVEUI.responsiveTabs();
           
</script>
</body>
</html>





