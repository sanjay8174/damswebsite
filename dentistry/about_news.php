<!DOCTYPE html>
<?php
$aboutUs_Id = $_REQUEST['a'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="/images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<?php  $getAboutusContent = $Dao->getAboutusContent($aboutUs_Id);
$getAboutus = $Dao->getAboutus();
$path = constant::$path;
?>
<section class="inner-banner">
<div class="wrapper">
<article style="background:url('<?php echo $path. "/images/background_images/".$getAboutusContent[0][0];  ?>') right top no-repeat;">
<aside class="banner-left">
<?php echo  $getAboutusContent[0][1]; ?>
</aside></article></div></section>
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
<ul>
<li class="bg_none"><a href="<?php echo $getAboutus[0][1]."?a=".$getAboutus[0][0] ?>" title="<?php echo $getAboutus[0][2]?>"><?php echo $getAboutus[0][2]?></a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading">
<h4>News Media</h4>
<article class="showme-main">
<div class="about-content">
<div class="course-box nw_wpr" style="padding:0; box-shadow:none;">
<ul class="idTabs responce-show"> 
<li><a href="#print">Print Coverage</a></li>
<li><a href="#online">Media Coverage</a></li> 
</ul>
<div id="print">
<div class="satellite-content satellite-content1">
<div class="idams-box1"> 
<div class="image-row">
<div class="gallery-list1 display_block" id="gallery1">
<ul id="ul1" class="display_block">
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/Dabang_Dunia_bhopal.jpg" ?>'  data-lightbox="example-set" title="Daband Dunia,  28th Jan 2015, Bhopal">
<img class="example-image" src='<?php echo $path."/images/news_award/Dabang_Dunia_bhopal.jpg" ?>' >
<p><strong>Daband Dunia</strong><br>28th Jan 2015<br>Bhopal</p>
</a></li>
<li class="publication-box-new nw_cntr" >
<a class="example-image-link" href='<?php echo $path."/images/news_award/TheHitavadaBhopal28Jan2015.jpg" ?>' title="The Hitavada, 28th Jan 2015, Bhopal">
<img class="example-image" src='<?php echo $path."/images/news_award/TheHitavadaBhopal28Jan2015.jpg" ?>' >
<p><strong>The Hitavada</strong><br>28th Jan 2015<br>Bhopal</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/ThePioneerBhopal28Jan2015.jpg" ?>' title="The Pioneer, 28th Dec 2015, Bhopal">
<img class="example-image" src='<?php echo $path."/images/news_award/ThePioneerBhopal28Jan2015.jpg"?>' >
<p><strong>The Pioneer</strong><br>28th Dec 2015<br>Bhopal</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-3.jpg" ?>' data-lightbox="example-set" title="Rajasthan Patrika, 25th Dec 2014, Jaipur">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-3.jpg" ?>' >
<p><strong>Rajasthan Patrika</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-2.jpg" ?>' title="Dainik Bhor, 25th Dec 2014, Jaipur">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-2.jpg" ?>' >
<p><strong>Dainik Bhor</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-4.jpg" ?>' title="Aaspass, 25th Dec 2014, Jaipur">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-4.jpg" ?>' >
<p><strong>Aaspass</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-1.jpg" ?>' title="Hukumnama Samachar, 25th Dec 2014, Jaipur">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-1.jpg" ?>'>
<p><strong style="white-space:nowrap">Hukumnama Samachar</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-5.jpg" ?>' title="Dainik Navajyoti, 25th Dec 2014, Jaipur">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-5.jpg"?>' >
<p><strong>Dainik Navajyoti</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-6.jpg" ?>' title="Daily News, 26th Dec 2014, Jaipur">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-6.jpg" ?>' >
<p><strong>Daily News</strong><br>26th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-7.jpg" ?>' title="Hindustan Times, 18th Dec 2014, Delhi/NCR">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-7.jpg"?>' >
<p><strong>Hindustan Times</strong><br>18th Dec 2014<br>Delhi/NCR</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-8.jpg" ?>"' title="The Statesman, 12th Dec 2014, Kolkata">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-8.jpg"?>' >
<p><strong>The Statesman</strong><br>12th Dec 2014<br>Kolkata</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-9.jpg"?>' title="The Hindu, 15th Dec 2014, New Delhi">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-9.jpg"?>' >
<p><strong>The Hindu</strong><br>15th Dec 2014<br>New Delhi</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/chandi-1.jpeg" ?>' title=" AAJ SAMAJ, Chandigadh">
<img class="example-image" src='<?php echo $path."/images/news_award/chandi-1.jpeg" ?>' >
<p><strong>Aaj Samaj</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/chandi-2.jpeg"?>' title="Dainik Bhaskar, Chandigadh">
<img class="example-image" src='<?php echo $path."/images/news_award/chandi-2.jpeg"?>' >
<p><strong>Dainik Bhaskar</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/chandi-3.jpeg" ?>' title=" HIM PRABHA, Chandigadh">
<img class="example-image" src='<?php echo $path."/images/news_award/chandi-3.jpeg"?>' >
<p><strong>Him Prabha</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/chandi-4.jpeg" ?>' title="NAYA LOKYUG, New Delhi">
<img class="example-image" src='<?php echo $path."/images/news_award/chandi-4.jpeg"?>' >
<p><strong>Naya Lokyug</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/chandi-5.jpeg"?>' title="THE INDIAN EXPRESS, Chandigadh">
<img class="example-image" src='<?php echo $path."/images/news_award/chandi-5.jpeg"?>' >
<p><strong>The Indian Express</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/chandi-6.jpeg" ?>' title="PUNJAB KESARI, Delhi/NCR">
<img class="example-image" src='<?php echo $path."/images/news_award/chandi-6.jpeg" ?>' >
<p><strong>Punjab Keshari</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-11.jpg" ?>' title="Adhikar, 25th Dec 2014, Jaipur">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-11.jpg" ?>' >
<p><strong>Adhikar</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href='<?php echo $path."/images/news_award/nws-15.jpg" ?>' title="Metro Bytes, 26th Dec 2014, Jaipur">
<img class="example-image" src='<?php echo $path."/images/news_award/nws-15.jpg"?>' >
<p><strong>Metro Bytes</strong><br>26th Dec 2014<br>Jaipur</p>
</a></li></ul></div></div></div></div></div>
<div id="online">
<div class="satellite-content">
<div class="idams-box1">
<div class="image-row">
<div class="gallery-list1 display_block" id="gallery2">
</div></div></div></div></div></div></div></article></div></aside>
<aside class="gallery-right">
<?php include 'about-right-accordion.php'; ?>
<?php include 'enquiryform.php'; ?>
</aside></section></div></div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script> 
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="js/registration.js"></script> 
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.6.js"></script>
<script type="text/javascript">
$(function() {
$('#gallery1 a').lightBox();
});
$(function() {
$('#gallery2 a').lightBox();
});
$(function() {
$('#gallery3 a').lightBox();
});
$(function() {
$('#gallery4 a').lightBox();
});
function blockchange1(val, ids)
{
var str=String(ids);
var galarr = ["gallery1", "gallery2", "gallery3", "gallery4"];
var countul1 = $('#gallery1 > ul').size();
var countul2 = $('#gallery2 > ul').size();
var countul3 = $('#gallery3 > ul').size();
var countul4 = $('#gallery4 > ul').size();
if(ids=="gallery1")
{
for(var g=1;g<=countul1;g++)
{
if(val==g)
{
$("#ul"+g).fadeIn('slow','swing');
$("#ul"+g).removeClass('display_none');
$("#ul"+g).addClass('display_block');
$("#ula"+g).addClass("active-dot"); 
}
else
{
$("#ul"+g).fadeOut('fast','linear');
$("#ul"+g).removeClass('display_block');
$("#ul"+g).addClass('display_none');
$("#ula"+g).removeClass("active-dot"); 
}
}
}
else if(ids=="gallery2")
{
for(var g=4;g<=(countul2+3);g++)
{
if(val==g)
{
$("#ul"+g).fadeIn('slow','swing');
$("#ul"+g).removeClass('display_none');
$("#ul"+g).addClass('display_block');
$("#ula"+g).addClass("active-dot"); 
}
else
{
$("#ul"+g).fadeOut('fast','linear');
$("#ul"+g).removeClass('display_block');
$("#ul"+g).addClass('display_none');
$("#ula"+g).removeClass("active-dot"); 
}
}
}
else if(ids=="gallery3")
{
for(var g=7;g<=(countul3+6);g++)
{
if(val==g)
{
$("#ul"+g).fadeIn('slow','swing');
$("#ul"+g).removeClass('display_none');
$("#ul"+g).addClass('display_block');
$("#ula"+g).addClass("active-dot"); 
}
else
{
$("#ul"+g).fadeOut('fast','linear');
$("#ul"+g).removeClass('display_block');
$("#ul"+g).addClass('display_none');
$("#ula"+g).removeClass("active-dot"); 
}
}
}
else if(ids=="gallery4")
{
for(var g=10;g<=(countul4+9);g++)
{
if(val==g)
{
$("#ul"+g).fadeIn('slow','swing');
$("#ul"+g).removeClass('display_none');
$("#ul"+g).addClass('display_block');
$("#ula"+g).addClass("active-dot"); 
}
else
{
$("#ul"+g).fadeOut('fast','linear');
$("#ul"+g).removeClass('display_block');
$("#ul"+g).addClass('display_none');
$("#ula"+g).removeClass("active-dot"); 
}
}
}
};
function divchange(val)
{
var countdiv= $('#changerdiv > li').size();
for(var d=1;d<=countdiv;d++)
{
if(val==d)
{
$("#divgal"+d).addClass("present");
$("#gallery"+d).fadeIn('slow','swing');
$("#gallery"+d).removeClass('display_none');
$("#gallery"+d).addClass('display_block');
$("#galdots"+d).fadeIn('slow','swing');
$("#galdots"+d).removeClass('display_none');
$("#galdots"+d).addClass('display_block');
}
else
{
$("#divgal"+d).removeClass("present");
$("#gallery"+d).fadeOut('fast','linear');
$("#gallery"+d).removeClass('display_block');
$("#gallery"+d).addClass('display_none');
$("#galdots"+d).fadeOut('fast','linear');  
$("#galdots"+d).removeClass('display_block');
$("#galdots"+d).addClass('display_none');    
}
}
}
</script>
</body>
</html>