<?php  $pageName= substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); ?>

<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Features Cloud Campus </div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
        <ul class="course-new-list">
<!--            <p style="font-size:14px;color:#525252;text-align: justify;">This program is being delivered at your door step with no need for travel and loss of work. The program will be conducted by faculty members who understand the basic issues as a beginner and also understand the complexities of being an advanced level researcher. The participants will also get an opportunity to learn the analysis on their own data. 

              "The program even though being started for the first time will revolutionize PG training in India as it will serve as a bench mark of Tele-education in India"</p><br>  -->
            
          <li><span class="sub-arrow"></span>1000+ Video Lectures & Study Materials</li>
          <li><span class="sub-arrow"></span> Study anytime & anywhere</li>
          <li><span class="sub-arrow"></span>FLASH Cards based on Questions Asked in Previous Year Exams.</li>
          <li><span class="sub-arrow"></span>Previous year’s solved question papers with online test series.</li> 

        </ul>
      </div>
    </div>
  </div></div>
<!--  <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Highlight</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>According to New Format,Curriculum and Syllabus.</li>
          <li><span class="sub-arrow"></span>The Faculty is UK Based and has been Involved in Delivering this course both Nationally and Internationally.</li>  
        </ul>
      </div>
    </div>
  </div></div>-->

<!--<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Facilitator</div>
  <div class="course-new-section">
   <div class="coures-list-box">
      <div class="coures-list-box-content">
<div class="mrcp_boxess" style="margin-top:0;">
        <div class="mrcp_dr_img"><img src='images/Dr_nicolla.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Nicolla Connelly</h1>
            <p class="mrcp_dr_sbtl">MB CHb, MRCP</p>
            <p class="mrcp_dr_sbtl">Course Director&nbsp; - &nbsp;ABMA MRCP Part 1 Course </p>          
		   <p class="mrcp_dr_sbtl">Specialist Registrar&nbsp; - &nbsp;Internal Medicine,Mersey Deanery</p>
		   
        </div>
</div>

<div class="mrcp_boxess" style="border-bottom:none;">
        <div class="mrcp_dr_img"><img src='images/Dr_kristian.jpg'/></div>
        <div class="mrcp_inr_boxes">
           <h1 class="mrcp_dr_ttl">Dr Kristian Skinner</h1>
            <p class="mrcp_dr_sbtl">MB CHb, MRCP</p>
            <p class="mrcp_dr_sbtl">Course Co-Director&nbsp; - &nbsp;ABMA MRCP Part 1 Course</p>          
		   <p class="mrcp_dr_sbtl">Specialist Medical Registrar&nbsp; - &nbsp;Liverpool and Greater Manchester UK</p>  
        </div>

</div> </div>     


      <div class="coures-list-box-content">
<div class="mrcp_boxess" style="margin-top:0;">
        <div class="mrcp_dr_img"><img src='images/dr_james.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr James Galloway</h1>
          <p class="mrcp_dr_sbtl">MBCHB, CHM, MSC, MRCP(UK), PH</p>
            <p class="mrcp_dr_sbtl">Course Director</p>
            <p style="font-size:14px;">Dr Galloway is a lecturer in rheumatology at King’s College London, and is also an honorary consultant rheumatologist at King’s College Hospital.He is currently lecturer at King’s medical school in London as well as delivers lectures to a broad range of post-graduate courses. He established the independent Manchester MRCP part one course with Dr Jones in 2006. He is an educational consultant course director and programme lead for MRCP part 1 and PACES.</p>
        </div>
</div></div>

 <div class="mrcp_boxess">
        <div class="mrcp_dr_img"><img src='images/dr_mathew.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Matthew Jones</h1>
            <p class="mrcp_dr_sbtl">MBChB, MD, MRCP(UK), MRCP(Neurology)</p>
            <p class="mrcp_dr_sbtl">Course Co-Director</p>
            <p style="font-size:14px;">Dr Jones is a consultant neurologist and clinical teaching fellow at Greater Manchester Neurosciences Centre, Salford Royal Foundation Trust, UK. His teaching interests lie in both undergraduate and postgraduate education. He is a lecturer at Manchester Medical School. He has been co-directing the Manchester MRCP course since 2006. He is Director of MRCP PACES course in Manchester as well. Dr Jones will be delivering the neurology presentation online for this course.</p>
        </div>

</div>   <div class="mrcp_boxess">
        <div class="mrcp_dr_img"><img src='images/dr_ashok.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr. Ashok Kumar Kapoor</h1>
            <p class="mrcp_dr_sbtl">M.B.B.S. MD ( lnternal Medicine). DM ( Cardiology),FICA(USA),F.Card(Germany)</p>
            <p class="mrcp_dr_sbtl">Specialist Cardiologist</p>
            <p class="mrcp_dr_sbtl">Consultant Cardiologist. Mediclinic Group of Hospitals, Dubai</p>
            <p class="mrcp_dr_sbtl">Consultant Cardiologist. International modern Hospital, Dubai</p>
            <p class="mrcp_dr_sbtl">Chairman-Getwell Medical Center- Bur Dubai, Dubai UAE</p>
            <p style="font-size:14px;">Dr Kapoor is a well-established consultant cardiologist in the UAE for more than 20 years. He is the founder of Arlington British Medical Academy – UK.</p>
        </div>

</div>
<div class="mrcp_boxess">
        <div class="mrcp_dr_img"><img src='images/dr_sumer.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr. Sumer K. Sethi</h1>
            <p class="mrcp_dr_sbtl">MBBS, MD (Radiology)</p>
            <p class="mrcp_dr_sbtl">Course Facilitator</p>
            <p class="mrcp_dr_sbtl">Director – DAMS (India)</p>
            <p class="mrcp_dr_sbtl">CEO – TELERAD providers – India</p>
            <p style="font-size:14px;">Dr Sethi is a senior Radiologist based in New Delhi. He has been very active in PG Medical Education for the last 16 years.</p>
        </div>

</div>
        <div class="mrcp_boxess" style="border-bottom:none;">
            <div class="mrcp_dr_img"><img src='images/dr_zubir.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Zubair Ahmad</h1>
            <p class="mrcp_dr_sbtl">MRCGP, DRCOG, DFFP, FRCGP, PCME</p>
            <p class="mrcp_dr_sbtl">Course Facilitator</p>
            <p style="font-size:14px;">Dr Zubair Ahmad, a GP Trainer from North West Deanery of England and a Primary Care Consultant based in Manchester UK for the last 16 years.  He has been heavily involved in both post graduate and undergraduate medical teaching since 2007. He has been facilitating these courses for many years. </p>
        </div>

        </div>
    </div>
    </div>
  </div>-->
  <div class="pg-medical-main" style="display:block;border: 0px solid #F0AC49;">
	<img src="images/Dams_Cloud_Home_Page.jpg" style="margin-top:15px; border:1px solid #ccc;" width="100%">
    </div>
  </div>
</div>