
<?php
error_reporting(0);
session_start();
require("config/autoloader.php");
Logger::configure('config/log4php.xml');
require_once './source/util/mail/mailApiService.php';
require 'MainLayoutDao.php'; 

$mainDao = new MainLayoutDao();

$name        = $_POST["name"];
$address     = $_POST["address"];
$hospital    = $_POST["hospital"];
$city        = $_POST["city"];
$mobile      = $_POST["mobile"];
$email       = $_POST["email"];
$message     = $_POST["message"];

$insertData  = $mainDao->insertEnquiry($name,$address,$hospital,$city,$mobile,$email,$message);

$compName    = "DAMS";
$subject     = "Enquiry Form";
$imgSrc      = "https://test.gingertab.com/MSI/images/title.gif";
$message     = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns  = "https://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
        <title>' . $compName . '</title>
    </head>
    <body bgcolor="#eee2da" style="margin:0; padding:0; font-family:Arial, Helvetica, sans-serif;">
        <div style="width:700px; margin:0 auto; border:1px #CCC solid; padding:10px 20px 20px 20px;background-color: #F7EFEE;">
            <table style="width:700px" align="center" cellpadding="0px" cellspacing="0px">
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td style="float: left; width:330px;padding:35px 0 5px 0;font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#3a8ccf">ENQUIRY DETAIL</td>
                                    <td style="text-align:right;width:auto;padding:0px 0 5px 0"><a target="_blank"><img src="'.$imgSrc.'" width="220px"/></a></td>
                                </tr>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;color:#333333;line-height:18px">
                            <tr>
                                <td style="padding:5px 0px 5px 15px;background-color:#f5f5f5;border-bottom:1px solid #cccccc;width:150px;font-weight:bold">Full Name</td>
                                <td style="color:#333333;font-size:12px;border-bottom:1px solid #cccccc;font-family:Verdana,Arial,Helvetica,sans-serif;padding:5px 0px 5px 10px">' . $name . '</td>	</tr>
                            <tr>
                                <td style="padding:5px 0px 5px 15px;background-color:#f5f5f5;border-bottom:1px solid #cccccc;width:150px;font-weight:bold">Address</td>
                                <td style="color:#333333;font-size:12px;border-bottom:1px solid #cccccc;font-family:Verdana,Arial,Helvetica,sans-serif;padding:5px 0px 5px 10px">' . $address . '</td>	</tr>
                            <tr>
                                <td style="padding:5px 0px 5px 15px;background-color:#f5f5f5;border-bottom:1px solid #cccccc;width:150px;font-weight:bold">Hospital Name</td>
                                <td style="color:#333333;font-size:12px;border-bottom:1px solid #cccccc;font-family:Verdana,Arial,Helvetica,sans-serif;padding:5px 0px 5px 10px"> ' . $hospital . '</td>
                            </tr>
                            <tr>
                                <td style="padding:5px 0px 5px 15px;background-color:#f5f5f5;border-bottom:1px solid #cccccc;width:150px;font-weight:bold">City Name</td>
                                <td style="color:#333333;font-size:12px;border-bottom:1px solid #cccccc;font-family:Verdana,Arial,Helvetica,sans-serif;padding:5px 0px 5px 10px"> ' . $city . '</td>
                            </tr>
                            <tr>
                                <td style="padding:5px 0px 5px 15px;background-color:#f5f5f5;border-bottom:1px solid #cccccc;width:150px;font-weight:bold">Mobile</td>
                                <td style="color:#333333;font-size:12px;border-bottom:1px solid #cccccc;font-family:Verdana,Arial,Helvetica,sans-serif;padding:5px 0px 5px 10px"> ' . $mobile . '</td>
                            </tr>
                            <tr>
                                <td style="padding:5px 0px 5px 15px;background-color:#f5f5f5;border-bottom:1px solid #cccccc;width:150px;font-weight:bold">Email</td>
                                <td style="color:#333333;font-size:12px;border-bottom:1px solid #cccccc;font-family:Verdana,Arial,Helvetica,sans-serif;padding:5px 0px 5px 10px"> ' . $email . '</td>
                            </tr>
                            <tr>
                                <td style="padding:5px 0px 5px 15px;background-color:#f5f5f5;border-bottom:1px solid #cccccc;width:150px;font-weight:bold">Message</td>
                                <td style="color:#333333;font-size:12px;border-bottom:1px solid #cccccc;font-family:Verdana,Arial,Helvetica,sans-serif;padding:5px 0px 5px 10px"> ' . $message . '</td>
                            </tr>
                        </table>
                     </td>
                </tr>
            </table>
        </div>
    </body>
</html>';


        $mailTextData = str_replace("\n.", "\n..", $message);
        $mailTextData = wordwrap($mailTextData, 70, "\r\n");
        $mailTextData = trim(utf8_encode($mailTextData));
        $maildao = new MailAPIService();

        $emailArray = array("shobhit.shukla@gingerwebs.in");   
//        $emailArray = array("kapil@damsdelhi.com","info@damsdelhi.com","chandan@damsdelhi.com");   
        for($j=0; $j<count($emailArray); $j++){
        $msgId = $maildao->sendMail_AWS($emailArray[$j], $mailTextData,$subject);
         
     }
?>
<script>
        location.href = "index.php";
    </script>
