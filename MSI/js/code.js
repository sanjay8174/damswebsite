window.onload = init

function init()
{
	var mySlideShow = document.getElementById("banner");
	if ( mySlideShow != null )
	{ 
		if ( mySlideShow.title != null )
			slide_time = mySlideShow.title;
		startAnimation()
	}
	
  if (document.getElementById("language"))
  { createMask()
  }
  
  if (document.getElementById("nav_tabs"))
  { tabSet()
  }
  
  leftNavigation();  
}




/*

newPage(loc) forwards browser to "loc"

*/

/*
function newPage(loc)
{ window.location.href = loc
}
*/

function newPage(loc, new_window)
{
//	alert("newPage("+loc+", "+new_window+")");
	if ( new_window != null && new_window == "true")
	{
		window.open( loc );
		return false;
	}
	else
	{
		window.location.href = loc	
	}
}






/*

Intro Animation for Home Page

*/

var slideOpacity = new Array()
var currentSlide = 0
var homeRestart = false
var mySlideCount = 0;
var slide_time = 6000;

function startAnimation()
{
	if ( document.getElementById("slider_box") )
	{
		document.getElementById("slider_box").style.display = "block"
	}
	
	if ( mySlideCount <=0 )
	{
		mySlideCount = getElementCount( "banner", "div", "slide_image");
//		alert( "mySlideCount: "+mySlideCount );
		
		for (var  i=1; i < mySlideCount ; i++ )
		{
			slideOpacity[i] = 0;
		}
		slideOpacity[0] = 100;

	}
	
	setInterval("animate()",50)
	anim1 = setInterval("nextSlide()",slide_time)
}

function getElementCount(theParentElementId, theTagName, theElementId )
{
	var myCount = 0;
	var myParentElement = document.getElementById( theParentElementId );
	if ( myParentElement != null )
	{
		var myElementList = myParentElement.getElementsByTagName( theTagName );
		if (myElementList != null )
		{
			var myElement = null;
			for (var  i=0; i < myElementList.length; i++ )
			{
				myElement = myElementList[i];
				if ( myElement != null )
				{
					if (  myElement.id != null && myElement.id.indexOf( theElementId ) > -1  )
					{
						myCount++;
					}
				}
			}
		}
	}
	return myCount;
//	alert( "mySlideCount: "+mySlideCount );
}

function animate()
{
	for (i=0; i<mySlideCount; i++)
	{
		if (slideOpacity[i] != 0)
    	{
    		document.getElementById("slide_image" + i).style.display = "block"
    	}
    	else
    	{
    		document.getElementById("slide_image" + i).style.display = "none"
    	}
    
  
		if ((i == currentSlide) && (slideOpacity[i] != 100))
		{
			slideOpacity[i] = slideOpacity[i] + 10
		}
		else if ((i != currentSlide) && (slideOpacity[i] != 0))
		{
			slideOpacity[i] = slideOpacity[i] - 10
		}
    
		obj = document.getElementById("slide_image" + i)
		setOpacity(obj,slideOpacity[i])
	}
}

function nextSlide()
{
	currentSlide++
	if (currentSlide == mySlideCount)
	{
		currentSlide = 0
	}
	setSelectedSlide()
	setText()
}

function buttonClicked(which)
{
	currentSlide = which
	setSelectedSlide()
	setText()
	clearInterval(anim1)
	if (homeRestart)
	{
		clearTimeout(restart)
	}
	restart = setTimeout("restartAnimation()",slide_time/2)
	homeRestart = true
}

function restartAnimation()
{
	homeRestart = false
	anim1 = setInterval("nextSlide()",slide_time)
}

function setSelectedSlide()
{
	if ( document.getElementById("slider_box") )
	{
		document.getElementById("slider_box").className = "select" + currentSlide
	}	
}

function setText()
{
	for (i=0; i<mySlideCount; i++)
	{
		if ( document.getElementById("text" + i) )
		{
			if (i == currentSlide)
			{
				document.getElementById("text" + i).style.display = "block"
			}
			else
			{
				document.getElementById("text" + i).style.display = "none"
			}
		}
	}
}

function setOpacity(obj,num)
{
	obj.style.MozOpacity = (num / 100)
	obj.style.opacity = (num / 100)
	obj.style.filter = "alpha(opacity=" + num + ")"
}









/*

Language Pop-up Layer functions

*/

function createMask()
{ newElement = document.createElement("div")
  newElement.setAttribute("id","mask")
  obj = document.getElementById("language")
  document.getElementsByTagName("body")[0].insertBefore(newElement,obj)
  document.getElementById("mask").style.height = document.getElementsByTagName("body")[0].clientHeight + "px"
  document.getElementById("mask").onclick = hideLanguage
}

function showLanguage()
{ document.getElementById("mask").style.display = "block"
  document.getElementById("language").style.display = "block"

  var yPos = document.body.scrollTop
  
  if (yPos == 0)
  { if (window.pageYOffset)
    { yPos = window.pageYOffset;
    }
    else
    { yPos = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
    }
  }
  
  document.getElementById("language").style.marginTop = (yPos-100) + "px"
  
}

function hideLanguage()
{ document.getElementById("mask").style.display = "none"
  document.getElementById("language").style.display = "none"
}









/*

Tabbed Navigation functions

*/

function tabChange(which)
{ lis = document.getElementById("nav_tabs").getElementsByTagName("li")
  total = lis.length
  window.location.hash = "tab" + which
  
  for (i=0; i<total; i++)
  { if (i != which)
    { lis[i].className = ""
      document.getElementById("nav_content" + i).style.display = "none"
    }
    else
    { lis[i].className = "selected"
      document.getElementById("nav_content" + i).style.display = "block"
    }
  }
}

function tabSet()
{ hsh = window.location.hash
  if (hsh == "")
  { return
  }
  else
  { len = String(hsh).length;
    which = String(hsh).substring(len,len-1); 
    tabChange(which)
  }
}

function gNavToggle(obj)
{ ul = obj.parentNode.getElementsByTagName("ul")[0]
  status = ul.style.display
  
  if (status == "block")
  { ul.style.display = "none"
  }
  else
  { ul.style.display = "block"
  }
}








/*

Share Tool code

*/

var addthis_config =
{
   data_track_linkback: true
}

var addthis_share = 
{ 
	templates: {twitter: 'Recommend reading from imshealth.com: {{url}} @IMSHealth'}
}

var more_toggle_flag = 0;
function toggle_more(){
	if(more_toggle_flag == 0){
		document.getElementById("more-menu").style.display = "inline";
		document.getElementById("social-nav").style.height = "70px";
		document.getElementById("more-link").className = "less";
		more_toggle_flag = 1;
	}
	else{
		document.getElementById("more-menu").style.display = "none";
		document.getElementById("social-nav").style.height = "35px";
		document.getElementById("more-link").className = "more";
		more_toggle_flag = 0;
	}
}

/*

Publish functions

*/

function unpublishManagedObject(flag, cmId, siteVCMId, channelVCMId, message) {
	
	if (flag != 'true')
	{
		alert(message);
		return;
	}

	var windowTitle = 'configureTemplating' + cmId;
	var context = '/jobsp';
	var path = '/secure/newJob.do';
	path += '?spfSOs=' + cmId;
	path += '&vgnPubType=unpublish';
	path += '&vgnContentType=content';
	path += '&spfRP=/placeholder';
	path += '&spfLP=/' + siteVCMId + '/' + channelVCMId;
	SimplePopwin(context + path, windowTitle, '', '800', '600');
}

function publishChannel(flag, cmId, siteVCMId, channelVCMId, message) {
	
	if (flag != 'true')
	{
		alert(message);
		return;
	}

	var windowTitle = 'New Job ' + cmId;
	var context = '/jobsp';
	var path = '/secure/newJob.do';
	path += '?spfSOs=' + cmId;
	path += '&vgnPubType=publish';
	path += '&vgnContentType=channel';
	path += '&spfRP=/placeholder';
	path += '&spfLP=/' + siteVCMId + '/' + channelVCMId;
	SimplePopwin(context + path, windowTitle, '', '800', '600');
}

function unpublishChannel(flag, cmId, siteVCMId, channelVCMId, message) 
{
	if (flag != 'true')
	{
		alert(message);
		return;
	}

	var windowTitle = 'New Job ' + cmId;
	var context = '/jobsp';
	var path = '/secure/newJob.do';
	path += '?spfSOs=' + cmId;
	path += '&vgnPubType=unpublish';
	path += '&vgnContentType=channel';
	path += '&spfRP=/placeholder';
	path += '&spfLP=/' + siteVCMId + '/' + channelVCMId;
	SimplePopwin(context + path, windowTitle, '', '800', '600');
}

function leftNavigation() 
{
	var myLeftNavigation = document.getElementById("left_navigation");
	if ( myLeftNavigation != null )
	{
		var myLeftNavigationULs = myLeftNavigation.parentNode.getElementsByTagName("ul");
		if ( myLeftNavigationULs != null )
		{
			for (i=0; i<myLeftNavigationULs.length; i++)
			{
				var myLeftNavigationUL = myLeftNavigationULs[i];
				if ( myLeftNavigationUL != null )
				{
					var myLeftNavigationLIs = myLeftNavigationUL.getElementsByTagName("li");
					for (j=0; j<myLeftNavigationLIs.length; j++)
					{
						var myLeftNavigationLI = myLeftNavigationLIs[j];
						if ( myLeftNavigationLI != null && myLeftNavigationLI.className == "selected" )
						{
							var myBrowserVersion = navigator.appVersion;
							if ( myBrowserVersion != null && myBrowserVersion.indexOf( "MSIE 7.0" ) > -1  )
							{
								var myLeftNavigationAs = myLeftNavigationLI.getElementsByTagName("a");
								if ( myLeftNavigationAs != null && myLeftNavigationAs.length > 0)
								{
									myLeftNavigationA = myLeftNavigationAs[0];
									if	(	myLeftNavigationA != null &&
											myLeftNavigationA.getClientRects().length > 1
										)
									{
										myLeftNavigationA.style.backgroundImage="none";
										myLeftNavigationA.style.backgroundImage="url(bullet_dash_sel.gif)";
										myLeftNavigationA.style.backgroundRepeat="no-repeat";
										myLeftNavigationA.style.backgroundPosition="-9px 8px";
									}								
								}								
							}
						}
					}
				}
			}
		}
	}
}

