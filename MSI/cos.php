<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		



	<title>MammoScreen India</title>
<meta name="description" content="#">
<meta name="keywords" content="#">
<meta name="#" content="#">


		


	  
		<link rel="stylesheet" href="css/styles.css" media="all" type="text/css">
		

	
		<script language="JavaScript" src="js/code.js" type="text/javascript"></script>
		

		




	


		<script type="text/javascript">
		//<![CDATA[
		hs.registerOverlay({
			html: '<div class="closebutton" onclick="return hs.close(this)" title="Close"></div>',
			position: 'top right',
			fade: 2 // fading the semi-transparent overlay looks bad in IE
		});
		
		hs.graphicsDir = '';
		hs.wrapperClassName = 'borderless';
	    hs.showCredits = false;
		//]]>
		</script>
	
	<!-- Highslide - END -->
	

	







	</head>

	<body>



                                                   <!---main table start--->
                                                   


                                                     <!---page start--->

<div id="page">

                                                                                                                                                       <!---header start--->


<div id="header">

<div id="had_top">
<div id="menu">
<ul>
<li><a href="index.php">HOME</a></li>
<li><a href="contact_us.php">CONTACT US</a></li>
<li><a href="#">SITE MAP</a></li>
</ul>
</div><!-- id menu -->
</div><!-- id had_top -->

<div id="logo">
<div id="logo_title">
<a href="index.php"><img src="images/title.gif" class="india_text" /></a>
</div><!-- id logo_title -->
<a href="index.php"><img src="images/logo_mammo_screen.gif" alt="lets pink"></a>
</div><!-- id logo -->

<div class="search">

<input type="text" class="search_text" /></input>
<input src="images/search.gif" alt="Search" name="search_form" onclick="#" type="image" width="34" height="17"></input>

</div> <!-- id search -->                                                          

              
<div id="navi">
<ul>

<li>
<div class="navi_open"  >
<table>
<tbody><tr>
<td >
<a href="Vision_and_Mission.php">
<img src="images/awareness_small.gif" alt="" width="130" height="90">
<div class="navi_had">Vision and Mission</div><!-- class navi_had -->
<p>To provide excellent health care service by obtaining high quality mammograms with their accurate  reports and archiving the same for future comparison, </p>
</a>
</td>
<td >
<a href="our team.html">
<img src="images/our_team_small.gif" alt="" width="130" height="90">
<div class="navi_had">Our Team</div><!-- class navi_had -->
<p>A dedicated team of health professionals from different subspecialties, but with same goal…. </p>
</a>
</td>
</tr>
</tbody></table>
</div><!-- class navi_open -->
<a href="#">About Us</a>
</li>

<li>
<div class="navi_open">
<table>
<tbody><tr>
<td >
<a href="abt_mammo.php">
<img src="images/abt_mammo_small.jpg" alt="" width="130" height="90">
<div class="navi_had">Mammography</div><!-- class navi_had -->
<p>Mammography is the process of using low-energy-X-rays to examine the human breast</p>
</a>
</td>
<td >
<a href="cos.php">
<img src="images/screened_small.gif" alt="" width="130" height="90">
<div class="navi_had">Concept of Screening</div><!-- class navi_had -->
<p>Worldwide breast cancer remains the commonest cancer to affect women.  </p>
</a>
</td>
<td >
<a href="indications.php">
<img src="images/indi_small.jpg" alt="" width="135" height="90">
<div class="navi_had">Indications</div><!-- class navi_had -->
<p>Every asymptomatic female between age group of 50-69 should undergo annual screening mammogram.  </p>
</a>
</td>
<td >
<a href="limitation.php">
<img src="images/limit_small.jpg" alt="" width="135" height="90">
<div class="navi_had">Limitations</div><!-- class navi_had -->
<p>All diagnostic modalities have their limitations, so do mammograms! Few cancers in early stage may not be picked up on mammograms.  </p>
</a>
</td>
</tr>
</tbody></table>
</div><!-- class navi_open -->
<a href="#">About Mammography</a>
</li>

<li>
<div class="navi_open" >
<table>
<tbody><tr>
<td >
<a href="our quality.php">
<img src="images/qua_small.gif" alt="" width="130" height="90">
<div class="navi_had">Quality Control </div><!-- class navi_had -->
<p>All staff intensively trained on regular basis by qualified professionals. </p>
</a>
</td>
<td >
<a href="why_choose_mammoscreen.php">
<img src="images/provide_small.jpg" alt="" width="130" height="90">
<div class="navi_had">We Provide</div><!-- class navi_had -->
<p>Dedicated Breast care service-First of its kind in India.</p>
</a>
</td>
</tr>
</tbody></table>
</div><!-- class navi_open -->
<a href="why_choose_mammoscreen.php">Why Choose Us</a>
</li>

<li>
<a href="news.php">News</a>
</li>

<li>
<a href="https://mammoscreenindia.blogspot.in/">Blog</a>
</li>

<li>
<a href="career.php">Career</a>
</li>

<li>
<a href="partner_with_us.php">Partner with Us</a>
</li>

<li>
<a href="contact_us.php">Contact Us</a>
</li>

</ul>
</div><!-- id navi -->


</div><!-- id header -->
                              
	                                                <!--- header close --->


                                                    <!--- cos start --->
<div id="cos">



<div id="bannerrow">
<div id="banner">&nbsp;</div><!--banner clsoe -->
<div id="bannerabtmammo">
<div >
<h1>Concept of Screening</h1>
<ul>
<li>Why Breast cancer screening ?</li>
<li>Criteria for screening by WHO </li>
<li>Any Risk ?</li>
<li>Sensitivity</li>
<li>Specificity</li>

</ul>

</div>
</div><!--bannerabtmammo clsoe -->
</div><!--bannerrow clsoe -->




<div id="content">



<div id="content-left">


<div id="content-left-image">

<ul>
<li class="lady_img">&nbsp;</li>
<li><img src="images/contact.gif"  /></li>

</ul>

</div><!--content-left-image start -->


<?php include 'enquiry.php'; ?>

<!--<div class="query_box1" >
<h1>Enquiry Form</h1>

		
<ul>
<li><input name="" type="text" onfocus="javascript:if(this.value=='Your Full Name') this.value='';" value="Your Full Name" class="query_box-text" /></li>
<li><input name="" type="text" onfocus="javascript:if(this.value=='Full Address') this.value='';" value="Full Address" class="query_box-text1" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='Hospital Name') this.value='';" value="Hospital Name" class="query_box-text" /></li>
<li><input name="" type="text" onfocus="javascript:if(this.value=='Select City') this.value='';" value="Select City" class="query_box-text" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='Telephone No.') this.value='';" value="Telephone No." class="query_box-text" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='E-Mail Address') this.value='';" value="E-Mail Address" class="query_box-text" /></li>

<li><input name="" type="text" onfocus="javascript:if(this.value=='Type Your Message') this.value='';" value="Type Your Message" class="query_box-text2" /></li>

<li><input name="" value="Submit" type="button" class="query-button" /></li>
</ul>

</div> class query_box -->



</div><!--content-left close -->



<!--content-right start-->

<div id="content-right">




<!--mammodetail start -->


<div id="mammodetail">

<ul>
<h1>Why Breast cancer screening ?</h1>

<p>Worldwide breast cancer remains the <strong>commonest cancer to affect women.</strong> There are no known interventions that can prevent breast cancer. Early detection through breast screening is the best health intervention known to significantly reduced breast cancer mortality.<br />

Finding breast cancer, when it is small (less than 15 mm) and has not yet spread to axillary lymph nodes provides opportunity for more effective treatment. 

</p>
</ul>
<ul>
<h1>Criteria for screening by WHO </h1>



<li>The disease being screened is serious and prevalent, </li>
<li>The test is sensitive and specific, </li>
<li>The test is well tolerated, </li>
<li>The test is inexpensive, and </li>
<li>The test changes therapy or outcome. </li>

</ul>
<ul>
<h1>Any Risk?</h1>

<p>The radiation exposure associated with mammography is a potential risk of screening although low energy x-rays (usually around 30 kVp ) are used. 
</p>
</ul>
<ul>
<h1>Sensitivity</h1>

<p>Sensitivity refers to the proportion of true-positive results. Estimates of the sensitivity of screening mammography from different studies range from 83 to 95 percent.
</p>
</ul>
<ul>
<h1>Specificity</h1>

<p>Specificity refers to the proportion of true-negative results, or tests that correctly indicate that a woman does not have breast cancer among screened women without breast cancer. Specificity generally falls in the range of 90 to 98 percent. In other words, the risk of a false-positive mammogram is about 1 in 10. 
</p>

</ul>



</div><!--mammodetail close -->





</div><!--content-right close-->







</div><!--content close-->



</div><!-- id cos -->


                                                     <!--  enquiry_form_mammo close --->
                                                     <!--- abt_mammo close --->
                                                          <!--- footer start --->

<div id="footer_border"></div>
<div id="footer">
<table>
<tbody><tr>
<td>
<h6>About Us</h6>
<ul>
<li><a href="Vision_and_Mission.php">Vision and Mission</a></li>
<li><a href="our team.php">Our Team</a></li>
</ul>
</td>
<td>
<h6>About Mammgraphy</h6>
<ul>
<li><a href="abt_mammo.php">Mammgraphy</a></li>
<li><a href="cos.php">Concept of screening</a></li>
<li><a href="indications.php">Indications</a></li>
<li><a href="limitation.php">Limitations</a></li>
</ul>
</td>
<td>
<h6>Why Choose Us</h6>
<ul>
<li><a href="why_choose_mammoscreen.php">We Provide</a></li>
<li><a href="our quality.php">Quality Control </a></li>
</ul>
</td>
<td class="last">
<ul>
<li><a href="news.php">News</a></li>
<li><a href="https://mammoscreenindia.blogspot.in/">Blog</a></li>
<li><a href="career.php">Career</a></li>
<li><a href="partner_with_us.php">Partner with us</a></li>
<li><a href="contact_us.php">Contact us</a></li>
</ul>
</td>
</tr>
</tbody></table>
</div><!-- id footer -->

<div id="foot">
<div class="foot_text">
<ul>
<li><a href="contact_us.php">Contact Us</a></li>
<li><a href="#">Privacy</a></li>
<li><a href="#">Trademarks</a></li>
<li><a href="#">Site Map</a></li>
<li>Copyright © 2011 lets pink. All rights reserved.</li>
<li class="last"><a href="https://www.gingerwebs.com" target="_blank">Website Design by Ginger Webs</a></li>
</ul>
</div><!-- div class foot_text -->
<div class="social">
<ul >
<li ><a href="#"><img src="images/fb.gif" /></a></li>
<li ><a href="#"><img src="images/twitter.gif" /></a></li>
<li ><a href="#"><img src="images/link.gif" /></a></li>
</ul>
</div><!-- div id social -->
</div><!-- div id foot -->



                                                  <!--- footer close --->


</div><!-- id page -->
		
		

        
                                                 <!---page close--->
        
        
   


		
		                                         <!---main table close--->
		

</body>
