<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/styles.css" media="all" type="text/css"></head>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.6.4.js"></script>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/enquiry.js"></script>

<form id="formID" action="enquirySubmit.php" method="post"  onsubmit="return Submit_enquiry()" enctype="multipart/form-data">

<div class="query_box1" >
    
<h1>Enquiry Form</h1>
		
<ul>
    <li><input type="text" id="name" name="name" placeholder="Name" class="query_box-text" /></li>

    <li><input type="text" id="address" name="address" placeholder="Address" class="query_box-text1" /></li>

    <li><input type="text" id="hospital" name="hospital" placeholder="Hospital" class="query_box-text" /></li>

    <li><input type="text" id="city" name="city" placeholder="City" class="query_box-text" /></li>

    <li><input type="text" id="mobile"  name="mobile" placeholder="Mobile" maxlength="10" class="query_box-text" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');"/></li>

    <li><input type="text" id="email" name="email" placeholder="Email" class="query_box-text" /></li>

    <li><input  type="text" id="message" name="message" placeholder="Message" class="query_box-text2" /></li>

   <li><input  type="Submit" value="Submit"  class="query-button" /></li>
   
</ul>

</div>
    </form>
