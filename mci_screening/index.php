<!DOCTYPE html>
<?php error_reporting(0); ?>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>MCI Screening Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<meta name="description" content=" If you are a Foreign Medical Graduate join DAMS for MCI Screening Coaching in New Delhi, India." />
<meta name="keywords" content=" MCI Screening Coaching, MCI Screening Coaching Institute, MCI Screening Coaching Delhi, MCI Screening Coaching India, FMG Screening Coaching Institute" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<style>
    .downcrse {
    border-radius: 10px;
    background-color: #399DD7;
    color: white;
    float: right;
    border:none;
    padding:10px;
    margin-right: 15px;
    position:relative;
}
.gif-img{
    position: absolute;
    right: -13px;
    top: -7px;
    width:28px;
}
.gif-img img{
    width: 100%;
}
.course-box p{
    width: 100%;
display: inline-block;
margin-top:20px;
}
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
	height:500px;
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active {
  background-color: #717171;
}
	.mySlides img{
		width:100%;
		height:100%;
	}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
	height:100%;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .text {font-size: 11px}
}
</style>
</head>

<body class="inner-bg">
<?php include 'registration.php';
$courseNav_id = 5;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
    <script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js?v=1"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="gallerySlider/owl.carousel.js"></script>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
      <?php $getNavContent=$Dao->getCourseNavContent($courseNav_id); ?>
<!--    <article class="mci-banner">-->
        <article style="width:100%;float:left;height:318px;background: url('https://damsdelhi.com/images/background_images/<?php echo $getNavContent[0][2]; ?>') right top no-repeat;">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left">
      <?php echo $getNavContent[0][1]; ?>
<!--        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>-->
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Yellow alert box Here By Anurag Tiwari -->
<section class="alertbox alertbox1">
    <div class="wrapper">
  <div class="alert_box">
        <div class="alert_title_box">
            <div class="alert_title"><h2 style="font-size: 20px;color: #ffffff;line-height: 24px">VACATION 
COURSE FOR FMG STUDENTS</h2></div>
        </div>
        <div class="alert_btn_box">
            <div class="alert_btn">
                <a class="alert-button" id="yellow" title="" href="doorstep.php">Please Click Here <img src="images/angle-arrow-pointing.png" alt="Right Arrow"></a>
            </div>
        </div>
  </div>
    </div>
</section>
<script >
    $(document).ready(function(){
    //     Sign In Form
	$('#yellow-alert').click(function() {
            $('#backPopup').show();
		$('#yellow-alert-pop').show();
    });
	$('#yellow-alert-close').click(function() {
            $('#backPopup').hide();
		$('#yellow-alert-pop').hide();
    });
    
    });
</script>
<div id="backPopup" class="backPopup" style="display: none;"></div>
<div id="yellow-alert-pop" class="yellow-alert-pop" style="display: none;">
<form >
<div class="enquiry-main">
    <div class="career-form-heading yellow-form-heading" style="font-size: 14px;">DAMS   ANNOUNCES VACATION 
        COURSE FOR FMG STUDENTS <br>(FOREIGN MEDICAL   GRADUATES)<span class="career-form-close" id="yellow-alert-close"></span></div>
<div class="enquiry-content-main">
<div class="enquiry-content">
<div class="enquiry-content-more yellow-content">

    <div class="yellow-hedder">Any WHERE IN INDIA   LIVE <b style="color:#00A651">/</b> T&D <b style="color:#00A651">/</b> EVENING CLASSESOF INDIVIDUAL SUBJECTS</div>
    <h2 class="yellow-head-title">WHY WASTE TIME?</h2>
    <div class="yellow-box-left">
            <div class="yellow-content-box">
        <h4 class="">Requirements   :</h4>
        <ul>
            <li>COLLAGE   ID  CARD </li>
            <li>VACATION  LATER </li>
            <li>COPY  OF  PASSPORT </li>
        </ul>
    </div>
    <div class="yellow-content-box-right">
        <h4>REGISTER AT  <a href="">mci@damsdelhi.com</a> With   ABOVE </h4>
    </div>
        <div class="yellow-content-box-full">
        <ul>
            <li>DOCUMENTS.     choice   OF CENTRE   (MIN THREE) 1.2.3</li>
            <li>Fees    =   60,000 + ST. </li>
            <li>Faculty   =   1 TIME BEFORE   EXAM (IN DELHI) + ANY subjects   DURING VACATION   .  At   any  center in   dams ,  india</li>
            <li>Centre  list :</li>
        </ul>
    </div>
    </div>
    <div class="yellow-box-left">
        <img src="images/POSTER1.jpg">
    </div>

</div>
</div>
<div class="enquiry-bottom"></div>
</div>
</div>
</form>
</div>
<!-- alert box end -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
    <?php echo $getNavContent[0][0]; ?>
<!--        <p><br>
          <strong> 88% PASS RATE IN MCI SCREENING MARCH 2013<br>
          79% PASS RATE IN MCI SCREENING SEPTEMBER 2012<br>
          </strong> </p>
        <br>
        <p>DAMS has arrived in style in this segment.</p>
        <p> MCI screening passing rates were generally believed to be low, before we started exploring this side. Our focused efforts have consistently given high percentage selection in MCI screening. Poor quality coaching academies functional in this segment are largely believed to be the reason for students not passing this exam. Also this year the MCI screening exam showed a variance in trend with more trickier MCQs and PGME like questions which small tuition house are generally not able to cater. DAMS being a consortium of more than 100 post graduate specialist teachers has the knack of predicting MCQS &amp; helping FMG’s achieve their dreams. Moreover, we have even helped many foreign graduates to become PG in various subjects in India, unlike others who are actually a dead end for foreign medical graduates.<br>
          <br>
          So, our request to FMGE ASPIRANT is choose the leaders in medical education, who are in pg medical exam segment for last 15 years and have presence across India &amp; we promise you will pass the exam in the FIRST ATTEMPT WITH US. Premier institute for PG medical entrance in India, rated by students as number one consortium of educationists with branches all over India since 2000, spearheaded by Sumer Sethi, MD Radiologist and topper in various PG medical entrances now offers unique courses for MCI screening, high yielding and affordable as well. Registered privated limited firm having served more than 50,000 Indian medical graduates now offers its unique courses for FMGs as well. Easly reachable and well located in Gautam Nagar.<br>
          <br>
          Why BEAT ABOUT THE BUSH when we know exactly what is important for you?<br>
          Why waste money when you can get a 5month course with the best in the business?<br>
          Have you tried all other so far and yet results have not been there?<br>
          Do you know most of these so called institutes are not even run by Doctors?<br>
          Clearing MCI is still a dead end if you don’t get through PG special combo packages for PG as well. For PG Medical entrance this year MCI has started new national level exam called as AIPG(NBE/NEET) Pattern which DAMS is the pioneer for providing insight into it. Secure your future with these experienced professionals.<br>
          <br>
          <strong>COURSE DETAILS</strong><br>
          <strong>MCI Regular Course</strong><br>
          <strong>4 Months Programme</strong><br>
          &raquo;   Study Material<br>
          &raquo;   Full day sessions<br>
          &raquo;   Class Tests<br>
          &raquo;   &ldquo; High yielding Golden points &rdquo; after each discussion<br>
          &raquo;   Revision Test Series<br>
        </p>
        <p>So come and join DAMS to fulfill your dreams by preparing for FMGs entrance exams.</p>-->
      </div>
        <?php include 'middle-accordion.php'; ?>

 <?php include 'recentAchievement.php'; ?>
		
		<div style="width:100%;margin:10px 0px;float: left;">
				<div class="slideshow-container">

				<div class="mySlides fade">
				  <img src="images/received_582577045591540.jpeg" style="width:100%">
				</div>

				<div class="mySlides fade">
				  <img src="images/received_334503713847272.jpeg" style="width:100%">
				</div>
				<div class="mySlides fade">
				  <img src="images/received_1993790800914596.jpeg" style="width:100%">
				</div>
				</div>
				<div style="text-align:center;margin-top:5px;">
				  <span class="dot"></span> 
				  <span class="dot"></span>
				<span class="dot"></span> 
</div>
	    </div>
    </aside>
    <aside class="content-right">
    <?php include 'right-accordion.php'; ?>
    <?php include 'newsRight.php'; ?>
    <?php include 'studentInterview.php'; ?>
     <?php include 'mci-screeningform.php'; ?>
		
    </aside>
  </div>
</section>
<!-- Midle Content End Here -->

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
	
	<script>
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 6000); // Change image every 2 seconds
}
</script>
</body>
</html>
