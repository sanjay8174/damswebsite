<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, DAMS MDS Quest</title>
<meta name="description" content="DAMS - Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Institute in India offering regular course for PG Medical Entrance Examination  like AIIMS, AIPG, and PGI Chandigarh. " />
<meta name="keywords" content="PG Medical Entrance Exam, Post Graduate Medical Entrance Exam, best coaching for PG Medical Entrance, best coaching for Medical Entrance Exam" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</head>

<body class="inner-bg">
<?php include 'registration.php';
require("config/autoloader.php");
Logger::configure('config/log4php.xml');

$course_id = 3;
$courseNav_id = 10;
?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="satellite-banner">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left banner-left-postion">
        <h2>India's First Satellite Based PG Medical Classes</h2>
        <h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants,<br>
          who otherwise couldn't take the advantage of the DAMS teaching<br>
          because of various reasons like non-availability of DAMS centre<br>
          in the vicinity would be greatly benefited.</h3>
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="dams-mds-quest.php" title="MDS Quest">MDS Quest</a></li>
          <li><a title="Satellite Classes" class="active-link">Satellite Classes</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading paddin-zero">
            <h4>Satellite Classes</h4>
            <article class="showme-main">
              <ul class="idTabs">
                <li><a href="#satellite">Satellite Classes</a></li>
                <li><a href="#concept">Concept</a></li>
                <li><a href="#technology">Technology</a></li>
                <li><a href="#faq">FAQ</a></li>
              </ul>
              <div id="satellite">
                <div class="satellite-content">
                  <div class="idams-box1"> <span>India's First Satellite Based PG Medical Classes</span>
                    <p>Ancient times Guru Shishya Pranali of imparting education was by the Guru at Guru's premises /Ashrams. Ages passed, social pattern changed, villages turned to towns, towns to cities &amp; metros. Civilization progressed with "Education". For good education, students have to migrate to better places &amp; this is the history as of yesterday. Now technology development removed these constraints and barriers. The Best Education of metros is available in House anywhere including the remote area. Quality education by teaching from the renowned professors and faculty is available at recipient's facilities. Best education at environment of individual's choice.</p>
                    <p>Medical Tele-Education is the Brain Child of Dr Sumer Sethi, our Director, widely known for his vision and innovations in field of PG Medical Education vertical. He is also one of the pioneers in field of Teleradiology &amp; Tablet based learning programme called as iDAMS. </p>
                  </div>
                  <div class="franchisee-box paddin-zero">
                    <p><span class="price_font">Dental Career Counselling for MDS :</span> 09999158131, 09999322163</p>
                  </div>
                  <div class="satellite-video">
                    <iframe width="100%" height="300" src="//www.youtube.com/embed/EAuRgGnUgJU" class="border_none"></iframe>
                  </div>
                </div>
              </div>
              <div id="concept">
                <div class="satellite-content">
                  <div class="idams-box1"> <span>Concept of Satellite Classes</span>
                    <p>One acute problem faced by the coaching class industry, is paucity of competent faculty. This constraint imposes limitations on expansion beyond geographical boundaries. Satellite Learning Programme addresses this problem effectively without losing quality of coaching. An out of box Learning Experience; Satellite Learning Programme has two way audio video facilities consisting of three major elements:</p>
                    <ul class="franchisee-list">
                      <li><span>&nbsp;</span>Teaching End.</li>
                      <li><span>&nbsp;</span>Remote Learning Centers called "Classrooms".</li>
                      <li><span>&nbsp;</span>The Satellite.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="technology">
                <div class="satellite-content">
                  <div class="idams-box1"> <span>Number 1 Technology being offered by DAMS-SKY classes</span>
                    <p>Studio transmissions are at high bandwidth rates of approx 1000 Kbps. This leads to Superior audio and video quality. DAMS-SKY system is two-way interactive for voice, video and data, wherein several classes are visible in full clarity to the Faculty at the same time. The Faculty can zoom into any classroom and see the students clearly, and talk to them. Each Center has two additional voice lines which can be used by Center/Students to communicate the studio or Faculty. Through the Smart Assessment System Student's attendance, MCQ-response and text messages reach the Faculty instantly.</p>
                  </div>
                  <div class="idams-box1"> <span>Features</span>
                    <ul class="franchisee-list">
                      <li><span>&nbsp;</span>DAMS -Sky leads to Superior audio and video quality.</li>
                      <li><span>&nbsp;</span>DAMS-SKY system is two-way interactive for voice, video and data.</li>
                      <li><span>&nbsp;</span>DAMS Sky classes are visible in full clarity to the Faculty at the same time. The Faculty can zoom into any classroom and see the students clearly, and talk to them.</li>
                      <li><span>&nbsp;</span>Each Center has two additional voice lines which can be used by Center/Students to communicate the studio or Faculty.</li>
                    </ul>
                  </div>
                  <div class="idams-box1"> <span>Why DAMS Sky Satellite Classes Not Internet Based Classes</span>
                    <p>Internet Video classes use the public internet which is not consistent because of variable internet speed and thus results in streaming and buffering causing loss of sync between voice and picture; whereas DAMS satellite classes use a private internet where transmission is at a consistent high bandwidth rate, thus leading to superior quality video &amp; audio. Satellite classes are more reliable than internet videoconferencing as it uses lesser terrestrial infrastructure. </p>
                  </div>
                  <div class="idams-box1"> <span>Interaction</span>
                    <ul class="franchisee-list">
                      <li><span>&nbsp;</span>Common ways to interact is talking over MIC with faculty, he can hear you and answer you back. This interaction could be heard in all centres.</li>
                      <li><span>&nbsp;</span>Text Message through our real time collaboration system which reaches the faculty instantly.</li>
                    </ul>
                  </div>
                  <div class="idams-box1"> <span>Doubt Solving</span>
                    <ul class="franchisee-list">
                      <li><span>&nbsp;</span>Normally class would have a doubt clearing section, this could be in the middle or at the end of class.</li>
                      <li><span>&nbsp;</span>You would also be provided with the membership to DAMS Exclusive club on Facebook which is famous for connecting students with faculty where you can put the queries and interact.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="faq">
                <div class="satellite-content">
                  <div class="schedule-mini-series t-spacebig"> <span class="mini-heading">DAMS –SKY Centers are Located</span>
                    <div class="schedule-mini-top1"> <span class="one-part1">&nbsp;</span> <span class="two-part1">Face to Face Classes</span> <span class="three-part1">DAMS-SKY Classes</span> </div>
                    <div class="schedule-mini-content2">
                      <ul>
                        <li> <span style="height:70px;" class="one-parts1">1</span> <span style="height:70px;" class="two-parts1 schedule-left-line">I am a Girl, travel to a big city is always risky and time consuming.</span> <span style="height:70px" class="three-parts1 schedule-left-line">I am close of my residence. It is my city. I am comfortable here. I also know all the students and make friends with them.</span> </li>
                        <li> <span class="one-parts1">2</span> <span class="two-parts1 schedule-left-line">I have to come down from my hometown to Delhi to get the coaching from best faculty.</span> <span class="three-parts1 schedule-left-line">I attend the lectures from same/Better faculty at a center near to my house in my hometown.</span> </li>
                        <li> <span class="one-parts1">3</span> <span class="two-parts1 schedule-left-line">My coaching fees are costly in Metro.</span> <span class="three-parts1 schedule-left-line">My coaching fees are lesser compared to Metro fees.</span> </li>
                        <li> <span class="one-parts1">4</span> <span class="two-parts1 schedule-left-line">It is difficult and uncertain to get admission in the class as the seats are limited.</span> <span class="three-parts1 schedule-left-line">Centers are equipped with multiple classrooms, with Flexibility of expansion. I am assured of a seat.</span> </li>
                        <li> <span class="one-parts1">5</span> <span class="two-parts1 schedule-left-line">If I am absent in any of the classes, it becomes difficult to attend it.</span> <span class="three-parts1 schedule-left-line">If I miss the class for any reason, I get access to the same class from in rebroadcast.</span> </li>
                        <li> <span style="height:150px;" class="one-parts1">6</span> <span style="height:150px;" class="two-parts1 schedule-left-line">I spend approx 10,000/- per month at the Hostel in metros. The food quality is poor and rooms are cramped. The environment is also not good.</span> <span style="height:150px" class="three-parts1 schedule-left-line">I have the flexibility of home food, home environment. I am able to study and prepare better, which increase my chances of succeeding at exams. I am happy to be at Home, safe and secure. I save over Rs. 10,000 per month with DAMS-SKY Classes. Money saved is money earned. I feel my parent's hard-earned money is well-spent with DAMS-SKY Classes.</span> </li>
                      </ul>
                    </div>
                  </div>
                  <p>DAMS Delhi have comes with new featues and better Interface.</p>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
         <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry -->
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

<!--<script type="text/javascript" src="https://www.sunsean.com/idTabs/jquery.idTabs.min.js"></script>-->
</body>
</html>