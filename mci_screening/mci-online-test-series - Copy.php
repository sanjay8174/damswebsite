<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});	
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper"> 
    <!--<article class="mci-screening"> -->
    <article class="online-test-series">
      <div class="big-nav">
        <ul>
          <li class="face-face active"><a href="mci-screening.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="mci-satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="mci-test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="mciscreening_sep_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="mci-screening.php" title="MCI Screening">MCI Screening</a></li>
          <li><a href="mci-dams-store.php" title="DAMS Store">DAMS Store</a></li>
          <li><a title="Online Test Series" class="active-link">Online Test Series</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Online Test Series</h4>
            <article class="showme-main">
              <aside class="main-online-test">
                <div class="online-test-heading"><span></span>USMLE Step-2 Package</div>
                <div class="package-main-content">
                  <div class="package-content">
                    <p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern. With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner... <a href="#" title="read more">read more &raquo;</a> </p>
                    <div class="some-buttons">
                      <div class="add-to-cart" style="float:right; margin:0px;"><a href="#" title="Add to cart">Add&nbsp;to&nbsp;cart</a></div>
                      <div class="view-schedule"><a href="#" title="View Schedule">View&nbsp;Schedule</a></div>
                      <div class="total-test"><a href="#" title="Total Test: 10">Total&nbsp;Test:&nbsp;10</a></div>
                      <div class="price-doller"><a href="#" title="Price: $500">Price:&nbsp;$500</a></div>
                    </div>
                  </div>
                </div>
              </aside>
              <aside class="main-online-test">
                <div class="online-test-heading"><span></span>USMLE Step-2 Package</div>
                <div class="package-main-content">
                  <div class="package-content">
                    <p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern. With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner... <a href="#" title="read more">read more &raquo;</a> </p>
                    <div class="some-buttons">
                      <div class="add-to-cart" style="float:right; margin:0px;"><a href="#" title="Add to cart">Add&nbsp;to&nbsp;cart</a></div>
                      <div class="view-schedule"><a href="#" title="View Schedule">View&nbsp;Schedule</a></div>
                      <div class="total-test"><a href="#" title="Total Test: 10">Total&nbsp;Test:&nbsp;10</a></div>
                      <div class="price-doller"><a href="#" title="Price: $500">Price:&nbsp;$500</a></div>
                    </div>
                  </div>
                </div>
              </aside>
              <aside class="main-online-test">
                <div class="online-test-heading"><span></span>USMLE Step-2 Package</div>
                <div class="package-main-content">
                  <div class="package-content">
                    <p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern. With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner... <a href="#" title="read more">read more &raquo;</a> </p>
                    <div class="some-buttons">
                      <div class="add-to-cart" style="float:right; margin:0px;"><a href="#" title="Add to cart">Add&nbsp;to&nbsp;cart</a></div>
                      <div class="view-schedule"><a href="#" title="View Schedule">View&nbsp;Schedule</a></div>
                      <div class="total-test"><a href="#" title="Total Test: 10">Total&nbsp;Test:&nbsp;10</a></div>
                      <div class="price-doller"><a href="#" title="Price: $500">Price:&nbsp;$500</a></div>
                    </div>
                  </div>
                </div>
              </aside>
              <aside class="main-online-test">
                <div class="online-test-heading"><span></span>USMLE Step-2 Package</div>
                <div class="package-main-content">
                  <div class="package-content">
                    <p>Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern. With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner... <a href="#" title="read more">read more &raquo;</a> </p>
                    <div class="some-buttons">
                      <div class="add-to-cart" style="float:right; margin:0px;"><a href="#" title="Add to cart">Add&nbsp;to&nbsp;cart</a></div>
                      <div class="view-schedule"><a href="#" title="View Schedule">View&nbsp;Schedule</a></div>
                      <div class="total-test"><a href="#" title="Total Test: 10">Total&nbsp;Test:&nbsp;10</a></div>
                      <div class="price-doller"><a href="#" title="Price: $500">Price:&nbsp;$500</a></div>
                    </div>
                  </div>
                </div>
              </aside>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="accor-wrapper">
            <div class="accordionButton" style="margin-top:0px;"><span></span>Classroom Course</div>
            <div class="accordionContent">
              <div class="inner-accor">
                <ul>
                  <ol>
                    <li><a href="../mci-regular_course_for_pg_medical.php" title="Regular Course"><span class="sub-arrow"></span> Regular Course</a></li>
                    <li><a href="../mci-crash_course.php" title="Crash course"><span class="sub-arrow"></span> Crash course</a></li>
                    <li><a href="mci-test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
                  </ol>
                </ul>
              </div>
            </div>
            <div class="accordionButton"><span></span>Distance Learning Course</div>
            <div class="accordionContent">
              <div class="inner-accor">
                <ul>
                  <ol>
                    <li><a href="mci-online-test-series.php" title="Online Test Series"><span class="sub-arrow"></span> Online Test Series</a></li>
                    <li><a href="../mci-postal.php" title="Postal Test Series"><span class="sub-arrow"></span> Postal Test Series</a></li>
                    <!--<li><a href="#" title="Mock Test Series"><span class="sub-arrow"></span> Mock Test Series</a></li>-->
                    <li><a href="mci-idams.php" title="iDAMS Tablet Based Course"><span class="sub-arrow"></span> iDAMS Tablet Based Course</a></li>
                  </ol>
                </ul>
              </div>
            </div>
          </div>
          <div class="enquiry-main">
            <div class="enquiry-heading">Enquiry Form</div>
            <div class="enquiry-content-main">
              <div class="enquiry-content">
                <div class="enquiry-content-more">
                  <form action="" method="get">
                    <input name="" type="text" class="enquiry-input" value="Your Name" onfocus="if(this.value=='Your Name')this.value='';" onblur="if(this.value=='')this.value='Your Name';" />
                    <input name="" type="text" class="enquiry-input" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';" />
                    <input name="" type="text" class="enquiry-input" value="Phone Number" onfocus="if(this.value=='Phone Number')this.value='';" onblur="if(this.value=='')this.value='Phone Number';" />
                    <select name="" class="select-input">
                      <option value="0">Select Course</option>
                      <option>MD/MS Entrance</option>
                      <option>MCI Screening</option>
                      <option>MDS Quest</option>
                      <option>USMLE Edge</option>
                    </select>
                    <select name="" class="select-input">
                      <option>Centre Interested</option>
                      <option>Centre Interested</option>
                      <option>Centre Interested</option>
                    </select>
                    <textarea name="" cols="" rows="3" class="enquiry-message"></textarea>
                    <div class="submit-enquiry"><a href="#" title="Submit"><span></span> Submit</a></div>
                  </form>
                </div>
              </div>
              <div class="enquiry-bottom"></div>
            </div>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>