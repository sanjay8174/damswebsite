<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCI Screening Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<meta name="description" content=" If you are a Foreign Medical Graduate join DAMS for MCI Screening Coaching in New Delhi, India." />
<meta name="keywords" content=" MCI Screening Coaching, MCI Screening Coaching Institute, MCI Screening Coaching Delhi, MCI Screening Coaching India, FMG Screening Coaching Institute" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 2;
$courseNav_id = 5;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mci-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="https://damsdelhi.com/index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="index.php" title="MCI Screening">MCI Screening</a></li>
          <li><a title="Regular Course" class="active-link">Regular Course</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>MCI Regular Course<span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main paddin-zero">
              <aside class="about-content">
                <p><br>
                  <strong> 88% PASS RATE IN MCI SCREENING MARCH 2013<br>
                  79% PASS RATE IN MCI SCREENING SEPTEMBER 2012<br>
                  <br>
                  </strong> DAMS has arrived in style in this segment.<br>
                  MCI screening passing rates were generally believed to be low, before we started exploring this side. Our focused efforts have consistently given high percentage selection in MCI screening. Poor quality coaching academies functional in this segment are largely believed to be the reason for students not passing this exam. Also this year the MCI screening exam showed a variance in trend with more trickier MCQs and PGME like questions which small tuition house are generally not able to cater. DAMS being a consortium of more than 100 post graduate specialist teachers has the knack of predicting MCQS &amp; helping FMG’s achieve their dreams. Moreover, we have even helped many foreign graduates to become PG in various subjects in India, unlike others who are actually a dead end for foreign medical graduates.<br>
                  <br>
                  So, our request to FMGE ASPIRANT is choose the leaders in medical education, who are in pg medical exam segment for last 15 years and have presence across India &amp; we promise you will pass the exam in the FIRST ATTEMPT WITH US. Premier institute for PG medical entrance in India, rated by students as number one consortium of educationists with branches all over India since 2000, spearheaded by Sumer Sethi, MD Radiologist and topper in various PG medical entrances now offers unique courses for MCI screening, high yielding and affordable as well. Registered privated limited firm having served more than 50,000 Indian medical graduates now offers its unique courses for FMGs as well. Easly reachable and well located in Gautam Nagar.<br>
                  <br>
                  Why BEAT ABOUT THE BUSH when we know exactly what is important for you?<br>
                  Why waste money when you can get a 5month course with the best in the business?<br>
                  Have you tried all other so far and yet results have not been there?<br>
                  Do you know most of these so called institutes are not even run by Doctors?<br>
                  Clearing MCI is still a dead end if you don’t get through PG special combo packages for PG as well. For PG Medical entrance this year MCI has started new national level exam called as AIPG(NBE/NEET) Pattern which DAMS is the pioneer for providing insight into it. Secure your future with these experienced professionals.<br>
                  <br>
                  <strong>COURSE - FEATURES (DURATION : 5 - 6 months) </strong><br>
                  <strong>MCI Regular Course</strong><br>
                  <!--<strong>4 Months Programme/2 MONTH CRASH COURSE</strong><br>-->
                  &raquo;   complete 19 subjects with the guidance of Best Mentors.<br>
                  &raquo;   Study material<br>
                  &raquo;   Test and discussion<br>
                  &raquo;   computer based test ( CBT )<br>
                  &raquo;   e- Medicoz<br>
                  &raquo;   class test <br>
                  &raquo;   eBook app 
                </p>
                <p class="paddin-zero">So come and join DAMS to fulfill your dreams by preparing for FMGs entrance exams.</p>
                 <div class="dams-data-nw">
                     <b>REGISTRATION AND ADMISSION </b>
                     <p> &raquo; The candidate must fill the application form which they can get it from centre</p>
                     <p> &raquo; 3 passport size photographs</p>
                     <p> &raquo; photostat of mark sheets of previous attempts ( if available ) for analysis and perks</p>
                     <p> &raquo; certificates with duly attached form </p>
                     
                     <b>PAYMENTS OF FEE AND DISCOUNT </b>
                     <p> &raquo; early bird discount</p>
                     <p> &raquo; discount for DAMSONIANS</p>
                     <p> &raquo; kindly attach your previous MCI results along with application form </p>
					 <b>SEMI-REGULAR BATCH</b>
                     <p> &raquo; 2 and half months duration </p>
                      <p> &raquo; weekly topic wise test </p>
                      <p> &raquo; Subject wise </p>
                      <p> &raquo; test and interaction with the clinical cas , conceptual , images questions. </p>
                      <p> This course focuses on covering the most important syllabus with 300hrs of teaching which reduces the stress on what actually required to know before the exams .</p>
                      <p> The classes are conducted by the highly experienced teachers who form the best faculties in India .</p>
<!--This course focuses on covering the most important syllabus with 300hrs of teaching which reduces the stress on what actually required to know before the exams .
The classes are conducted by the highly experienced teachers who form the best faculties in India .-->
                     
                     
                     
                     
                     
                     
                     
                     
                     
					 <!--<p>BRACE YOURSELVES TO CRACK MCI SCREENING EXAM WITH THE BEST MCI AND PG- PREPARATION ACADEMY IN THE COUNTRY --- DELHI ACADEMY OF MEDICAL SCIENCES. for more information 
                      contact us : ( 8588874949 + drtp@damsdelhi.com )</p>-->
					 <br>
					 <p>Brace yourselves to crack  MCI Screening Exam  with the Best MCI and PG Preparation Academy in the Country --- Delhi  Academy of Medical Sciences. For more Information 
                      Contact us : ( 8588874949 + drtp@damsdelhi.com )</p>
                     
                     <!--<b>SEMI-REGULAR BATCH</b>
                     <p> &raquo; join us - 14th September registrations open</p>-->
                    
                     <!--<b>TEST AND DISCUSSION </b>
                     <p> &raquo; Not sure how to go about your preparation ?</p>
                      <p> &raquo;  Want to develop a knack of solving multiple choice questions ?</p>
                     <p> &raquo;  Want to solve questions in a requisite time limit ?</p>
                     <p>&nbsp;&nbsp;&nbsp;LEARN THE ART OF SOLVING QUESTIONS 
                       JOIN US
                       23rd July </p>
                     
                     <b>COURSE FEATURES</b>
                     <p> &raquo; Subject wise Test and Discussion</p>
                     <p> &raquo; Updated version of Questions prepared by our experts</p>
                     <p> &raquo; Detailed discussion for choosing appropriate answer</p>
                     <p> &raquo; Time management for solving questions</p>
                     <p> &raquo; separate tests for clinical and non-clinical subjects</p>
                     <p> &raquo; 2 mock test</p>
                     <p> &raquo; 1 CBT ( computer based test )</p>-->
                     
                     <!--<b>VACATION COURSES</b>
                       <p> &raquo; Are you that kind of Doctor who believe in preparing for future career in medical school ?</p>
                       <p> &raquo; Want to prepare for the exam in Medical school itself ?</p>
                       <p> &raquo; Coming back home in vacations with the study plan but not possible at home ?</p> 
                       <p> &raquo; still wondering how to start preparing for this exam ?</p>
                       
                     <b>WHY TO WASTE TIME ....</b>
                     <p>We are offering all the undergraduate FMGs to join our vacation course and set yourself free from any chaos regarding clearing this Exam. Right Type of preparation at the Right Time. its never too late so,</p>
                     
                     <b>JOIN US :</b>
                     <p>Delhi branch - 15th July Onwards</p>
                     <p>Hyderabad Branch - 15th July Onwards</p>
                     <p>Ahmedabad Branch - 15th July Onwards </p>
                     <p>Jaipur Branch - 16 June Onwards</p>
                     
                     <b>course features:</b>
                     <p> &raquo; Courses are available for 2nd, 3rd , 4th, 5th , 6th year candidates</p>
                     <p> &raquo; completion of all the subjects and revision before 1st attempt of the exam</p>
                     <p> &raquo; updated version of study material</p>
                     <p> &raquo; updated version of test and discussions</p>
                     
                     <b>Benefits:</b>
                     <p> &raquo;  getting to know what is required to pass this exam </p>
                     <p> &raquo; preparation in your UG ( undergraduate ) time itself increases the probability of clearing this exam</p>
                     <p> &raquo; easy payment methods ( easy installments )</p>
                     <p> &raquo; getting study material and access to all e-book app , e-medicoz , membership in DAMS-Club</p>
                      <p> &raquo; In vacation you will have classes with us and back in university you will get time to revise with our study material.</p>
                      <p> &raquo; you can attend N number of classes once you join this program until your first attempt.</p>
                     <p>&nbsp;&nbsp;&nbsp;or more details contact : ( number and drtp@damsdelhi.com )</p>-->
                  </div>
                <div> <div style="margin-top: 30px;font-size: 15px;"><b>" Now MCI coaching are available in Hyderabad and Ahmedabad center</b></div>   <div class="book-ur-seat-btn"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></div></div>
              </aside>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'feedbackform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>