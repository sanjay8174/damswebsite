<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG </title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="director-message">
<aside class="banner-left">
<h2>Bringing Innovations in </h2>
<h3 class="page_title">PG Medical Education</h3>
</aside>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span>
<ul>
<li class="bg_none"><a href="dams.php" title="About Us">About Us</a></li>
<li><a title="About Director" class="active-link">About Director</a></li>
</ul>
</div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading">
<h4>BE GUIDED BY THE BEST GUIDE IN THE BUSINESS - DAMS</h4>
<article class="showme-main">
<div class="about-content">
<span class="director-heading">Lead by known innovator &amp; prominent educationist</span>
<ul class="benefits">
<li><span></span> Test &amp; Discussion batches which are immensely popular in Delhi &amp; Mumbai, ever wondered who started them first- Dr Sumer Sethi, course specially designed for good students who want the extra-edge from DAMS. Many AIIMS toppers have been a result of this phenomenon.</li>
<li><span></span> Originator of the idea popularly called as Foundation course. Maximum number of students join DAMS in foundation courses, as the brain behind getting the students to study early is our promoter. The foundation course is his brain child.</li>
<li><span></span> USMLE Edge- integrated foundation course of PG and USMLE combined particularly suited for final year students who are still not decided on which way to go.</li>
<li><span></span> AIPG(NBE/NEET) Pattern Based test papers and online question bank called as Destination AIPG(NBE/NEET) Pattern Capsule. Unique offering by DAMS, brain child of Dr Sumer Sethi, first institute to familiarize students with AIPG(NBE/NEET) Pattern pattern questions.</li>
<li><span></span> Tablet based learning called as iDAMS, another paradigm shift in PG medical entrance preparation by Dr.Sumer Sethi taking PG coaching to remotest locations using tablets.</li>
<li><span></span> With his internet and web 2.0 knowledge, he has taken PG medical education by storm with unique uses of FACEBOOK, TWITTER AND DAMS BLOG. DAMS EXCLUSVE CLUB on Facebook is immensely popular amongst students. DAMS is the coaching institute of YOUR GENERATION.</li>
<li><span></span> His book called Review of Radiology which came out in 2003 was first Review book in India written by an expert and took the market by storm; many people followed his idea of creating Review book by subject matter experts.</li>
<li><span></span> To his claim is also India's Premier Teleradiology Firm called as Teleradiology Providers, which is one of the pioneer in India in providing remote radiology reads.</li>
<li><span></span> He is also credited with world first and longest running Radiology Blog, critically acclaimed world over. </li>
<li><span></span> We have now connected satellite based class to help students in turns with no air connecting.</li>
</ul>
<span class="director-heading">
<strong>Board of Director</strong><br>
Dr. Rajiv Bhagi<br>
Dr. Sumer Sethi <br>
Dr. Deepti Sethi<br>
Dr. Ridhi Bhagi<br>
</span>
</div>
</article>
</div>
</aside>
<aside class="gallery-right">
<?php include 'about-right-accordion.php'; ?>
<!--for Enquiry popup  -->
<?php include 'enquiryform.php'; ?>
<!--for Enquiry popup  -->
</aside>
</section>
</div> 
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body></html>