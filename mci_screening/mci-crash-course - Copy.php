<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Crash Course for PG Medical Entrance Examination, AIPG(NBE/NEET) Pattern PG</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!-- [if gte IE8]>
<link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
		
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">

<div id="backPopup" style="display:none; background:#FFF; z-index:1000; width:100%; height:100%; opacity:0.8; top:0px; margin:0; position:fixed;"></div>

<div id="frontPopup1" style="display:none; float:left; margin:5% 0 0 34%; position:fixed; width:32%; z-index:1027;">
<div class="enquiry-main">
<div class="career-form-heading">
<span class="career-form-close" id="student-registration-close"></span>Registration to Online Exam</div>
<div class="enquiry-content-main">
<div class="enquiry-content">
<div class="enquiry-content-more">
<div class="career-box">
<div class="right-ip-1"><input name="" type="text" class="career-inp-1" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';" />
<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1"><input name="" type="text" class="career-inp-1" value="Password" onfocus="if(this.value=='Password')this.value='';" onblur="if(this.value=='')this.value='Password';" />
<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1"><input name="" type="text" class="career-inp-1" value="Confirm Password" onfocus="if(this.value=='Confirm Password')this.value='';" onblur="if(this.value=='')this.value='Confirm Password';" />
<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1"><input name="" type="text" class="career-inp-1" value="Name" onfocus="if(this.value=='Name')this.value='';" onblur="if(this.value=='')this.value='Name';" />
<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1"><input name="" type="text" class="career-inp-1" value="Mobile Number" onfocus="if(this.value=='Mobile Number')this.value='';" onblur="if(this.value=='')this.value='Mobile Number';" />
<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1"> 
<select name="" class="career-select-input-1">
<option>Course</option>
<option>Course</option>
<option>Course</option>
</select>

<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1"><input name="" type="text" class="career-inp-1" value="State" onfocus="if(this.value=='State')this.value='';" onblur="if(this.value=='')this.value='State';" />
<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1">
<input name="" type="text" class="career-inp-1" value="City" onfocus="if(this.value=='City')this.value='';" onblur="if(this.value=='')this.value='City';" />
<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1"><div class="submit-enquiry"><a href="#" title="Submit"> Submit</a></div>
</div></div>
</div></div>
<div class="enquiry-bottom"></div>
</div></div>
</div>

<div id="frontPopup2" style="display:none; float:left; margin:14% 0 0 34%; position:fixed; width:32%; z-index:1027;">
<div class="enquiry-main">
<div class="career-form-heading">
<span class="career-form-close" id="student-login-close"></span>Sign In</div>
<div class="enquiry-content-main">
<div class="enquiry-content">
<div class="enquiry-content-more">
<div class="career-box">
<div class="right-ip-1"><input name="" type="text" class="career-inp" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';" />
<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1"><input name="" type="text" class="career-inp" value="Password" onfocus="if(this.value=='Password')this.value='';" onblur="if(this.value=='')this.value='Password';" />
<span class="trans-1">Please enter your email address</span></div></div>

<div class="career-box">
<div class="right-ip-1"><div class="submit-enquiry"><a href="#" title="Submit"> Submit</a></div>
<div class="fgpassword">Forgot Password? <a href="javascript:void(0);" id="fg-password" title="Forgot Password"><b>Click Here</b></a></div>
</div></div>
</div>
</div>
<div class="enquiry-bottom"></div>
</div>

</div>
</div>

<div id="forgotpassword" style="display:none; float:left; margin:14% 0 0 34%; position:fixed; width:32%; z-index:1027;">
<div class="enquiry-main">
<div class="career-form-heading">
<span class="career-form-close" id="fg-close"></span>Forgot Password</div>
<div class="enquiry-content-main">
<div class="enquiry-content">
<div class="enquiry-content-more">
<div class="career-box">
<div class="right-ip-1"><input name="" type="text" class="career-inp-1" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';" />
<span class="trans-1">Please enter your email address</span></div></div>
<div class="career-box">
<div class="right-ip-1"><div class="submit-enquiry"><a href="#" title="Submit"> Submit</a></div>
</div></div>
</div></div>
<div class="enquiry-bottom"></div>
</div></div>
</div>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'coures-header.php'; ?>



<!-- Banner Start Here -->

<section class="inner-banner">
<div class="wrapper">
<article class="mci-banner">

<div class="big-nav">
<ul>
<li class="face-face active"><a href="mci-screening.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="mci-satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="mci-test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="mciscreening_sep_2013.php" title="Achievement">Achievement</a></li>
</ul>
</div>

<aside class="banner-left">
<h2>MCI Screening Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>

<?php include'mci-banner-btn.php'; ?>


</article>
</div>
</section> 

<!-- Banner End Here -->

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
<div class="wrapper">

<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
<ul>
<li style="background:none;"><a href="mci-screening.php" title="MCI Screening">MCI Screening </a></li>
<li><a title="Crash Course" class="active-link">Crash Course</a></li>
</ul>
</div>

<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading">
<h4>Crash Course <div class="book-ur-seat-btn"><a href="../cart.php" title="Book Your Seat"> <span>&nbsp;</span> Book Your Seat</a></div></h4>
<article class="showme-main">

<aside class="about-content">
<p>Crash Course Things have changed in the 2012 with coming of AIPG(NBE/NEET) Pattern &amp; DAMS is the only institute offering courses on the latest AIPG(NBE/NEET) Pattern pattern.  With our special offering like Classroom programmes based on AIPG(NBE/NEET) Pattern PG, Online AIPG(NBE/NEET) Pattern capsule, AIPG(NBE/NEET) Pattern LIVE TESTS, we are only trusted partner for AIPG(NBE/NEET) Pattern examinations.</p>

<p>We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI,  UPSC, DNB &amp; MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort.</p>

<p>Crash course starts on August / September month of every year. The duration of this course is 2 months; this course comprises of exclusive study material and will be based on class room discussions in depth &amp; revision tests. If you missed out on joining the coaching earlier or you just want to brush up everything with our famous DAMS Faculty, this is a course tailor made for you.</p>
</aside>


<aside class="how-to-apply">
<div class="how-to-apply-heading"><span></span> Course Highlights :-</div>

<ul class="benefits">
<li><span></span>We deal with all subjects in the crash course. ONLY DAMS COVERS ALL SUBJECTS IN THE CRASH COURSE.</li>
<li><span></span>We take frequent tests also in the crash course, we take our crash course seriously and in fact we produced quite a few rankers in AIIMS from this series this year.</li>
<li><span></span>Our Grand Tests and Bounce back tests are included.</li>
<li><span></span>Exclusive study material for crash course is also provided.</li>
<li><span></span>Things are done fast but in ENOUGH DETAILS.</li>
<li><span></span>GET THE COMPETITIVE EDGE BY JOINGING THIS COURSE</li>
</ul>

</aside>

</article>

<!--<div class="buy-now-course-btn"><a href="cart.php" title="Book Your Seat"> <span class="b-cart-basket">&nbsp;</span> Book Your Seat</a></div>-->
</div>

</aside>

<aside class="gallery-right">


<div id="accor-wrapper">
<div class="accordionButton" style="margin-top:0px;"><span></span>Classroom Course</div>
<div class="accordionContent">
<div class="inner-accor">
<ul>
<ol>
<li><a href="../mci-regular_course_for_pg_medical.php" title="Regular Course"><span class="sub-arrow"></span> Regular Course</a></li>
<li><a href="../mci-crash_course.php" title="Crash course"><span class="sub-arrow"></span> Crash course</a></li>
<li><a href="mci-test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
</ol>
</ul>
</div>
</div>
<div class="accordionButton"><span></span>Distance Learning Course</div>
<div class="accordionContent">
<div class="inner-accor">
<ul>
<ol>
<li><a href="mci-online-test-series.php" title="Online Test Series"><span class="sub-arrow"></span> Online Test Series</a></li>
<li><a href="../mci-postal.php" title="Postal Test Series"><span class="sub-arrow"></span> Postal Test Series</a></li>
<!--<li><a href="#" title="Mock Test Series"><span class="sub-arrow"></span> Mock Test Series</a></li>-->
<li><a href="mci-idams.php" title="iDAMS Tablet Based Course"><span class="sub-arrow"></span> iDAMS Tablet Based Course</a></li>
</ol> 
</ul>
</div>
</div>

</div>



<div class="enquiry-main">
<div class="enquiry-heading">Enquiry Form</div>
<div class="enquiry-content-main">
<div class="enquiry-content">
<div class="enquiry-content-more">
<form action="" method="get">
<input name="" type="text" class="enquiry-input" value="Your Name" onfocus="if(this.value=='Your Name')this.value='';" onblur="if(this.value=='')this.value='Your Name';" />
<input name="" type="text" class="enquiry-input" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';" />
<input name="" type="text" class="enquiry-input" value="Phone Number" onfocus="if(this.value=='Phone Number')this.value='';" onblur="if(this.value=='')this.value='Phone Number';" />



<select name="" class="select-input">
<option value="0">Select Course</option>
<option>MD/MS Entrance</option>
<option>MCI Screening</option>
<option>MDS Quest</option>
<option>USMLE Edge</option>
</select>


<select name="" class="select-input">
<option>Centre Interested</option>
<option>Centre Interested</option>
<option>Centre Interested</option>
</select>

<textarea name="" cols="" rows="3" class="enquiry-message"></textarea>
<div class="submit-enquiry"><a href="#" title="Submit"> <span></span> Submit</a></div>
</form>
</div>

</div>
<div class="enquiry-bottom"></div>
</div>
</div>
</aside>
</section>
</div>
</div>
</section>

<!-- Midle Content End Here -->


<!-- Footer Css Start Here -->

<?php include 'footer.php';?>

<!-- Footer Css End Here -->

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="../js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->
</body></html>