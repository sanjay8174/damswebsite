<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	


});
function ontab(val) {
	var len = $("#accordion > li").size();
    for(i=1;i<=len;i++)
	{
		if(val==i)
		{
			if(i==1)
			{
			  $('#accorsp'+i).removeClass('bgspanblk');
		      $('#aol'+i).slideDown();
		      $('#accorsp'+i).addClass('bgspan');
		      $('#accor'+i).addClass('light-blue');
			}
			else
			{
			  $('#accorsp'+i).removeClass('bgspanblk');
		      $('#aol'+i).slideDown();
		      $('#accorsp'+i).addClass('bgspan');
		      $('#accor'+i).addClass('light-blue-inner');				
			}
		}
		else
		{ 
		    if(i==1)
		    {
			  $('#accorsp'+i).removeClass('bgspan');
		      $('#aol'+i).slideUp();
		      $('#accorsp'+i).addClass('bgspanblk');
		      $('#accor'+i).removeClass('light-blue');
		    }
		    else
		    {
			  $('#accorsp'+i).removeClass('bgspan');
		      $('#aol'+i).slideUp();
		      $('#accorsp'+i).addClass('bgspanblk');
		      $('#accor'+i).removeClass('light-blue-inner');
		    }
		}
	}
};
</script>
</head>
<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="index.php" title="MCI Screening Course">MCI Screening Course</a></li>
          <li><a title="Achievement" class="active-link">Achievement</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li id="accor1" onClick="ontab('1');" class="light-blue-inner border_none"><span id="accorsp1" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
                <ol class="achievment-inner display_block" id="aol1">
                  <li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="mciscreening_sep_2013.php" title="MCI Screening September">MCI Screening September</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="mciscreening_mar_2013.php" title="MCI Screening March">MCI Screening March</a></li>
                </ol>
              </li>
              <li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
                <ol class="achievment-inner display_none" id="aol2">
                  <li><span class="mini-arrow">&nbsp;</span><a href="mciscreening_sep_2012.php" title="MCI Screening September">MCI Screening September</a></li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left right_pad0">
          <div class="inner-left-heading">
            <h4>MCI Screening September 2013</h4>
            <section class="showme-main"> 
              <script type="text/javascript" src="https://www.sunsean.com/idTabs/jquery.idTabs.min.js"></script>
              <link type="text/css" href="https://www.sunsean.com/idTabs/main.css" />
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="MCI Screening September 2013">MCI Screening September 2013</a></li>
                <li><a href="#official" title="Interview">Interview</a></li>
              </ul>
              <div id="jquery" class="display_block">
                <article class="interview-photos-section">
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="images/students/AYAN-SARKAR.jpg" alt="Dr. AYAN SARKAR" title="Dr. AYAN SARKAR" />
                        <p><span>Dr. AYAN SARKAR</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/E.LAKSHMAN-KUMAR.jpg" alt="Dr. E.LAKSHMAN KUMAR" title="Dr. E.LAKSHMAN KUMAR" />
                        <p><span>Dr. E.LAKSHMAN KUMAR</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/VINOD-NASPURI.jpg" alt="Dr. VINOD NASPURI" title="Dr. VINOD NASPURI" />
                        <p><span>Dr. VINOD NASPURI</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/NEELI-NEELIMA-RANI.jpg" alt="Dr. NEELI NEELIMA RANI" title="Dr. NEELI NEELIMA RANI" />
                        <p><span>Dr. NEELI NEELIMA RANI</span></p>
                      </div>
                    </li>
                    <li>
                      <div class="students-box border_left_none"> <img src="images/students/ARYA-TRILOK-SANKHLA.jpg" alt="Dr. ARYA TRILOK SANKHLA" title="Dr. ARYA TRILOK SANKHLA" />
                        <p><span>Dr. ARYA TRILOK SANKHLA</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/SANKI-AGRAWAL.jpg" alt="Dr. SANKI AGRAWAL" title="Dr. SANKI AGRAWAL" />
                        <p><span>Dr. SANKI AGRAWAL</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/VAISHNAVI-DUTTA-MISHRA.jpg" alt="Dr. VAISHNAVI DUTTA MISHRA" title="Dr. VAISHNAVI DUTTA MISHRA" />
                        <p><span>Dr. VAISHNAVI DUTTA MISHRA</span></p>
                      </div>
                      <div class="students-box"> </div>
                    </li>
                  </ul>
                  <br clear="all" />
                  <br clear="all" />
                  <div class="schedule-mini-series"> <span class="mini-heading">MCI Screening September - Result 2013</span>
                    <div class="schedule-mini-top"> <span class="one-part">S. No.</span> <span class="two-part">Student Name</span> <span class="three-part">&nbsp;</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"><span class="one-parts">1</span> <span class="two-parts schedule-left-line">Dr. AYAN SARKAR</span> </li>
                        <li><span class="one-parts">2</span> <span class="two-parts schedule-left-line">Dr. AKASH KAPPULA</span> </li>
                        <li><span class="one-parts">3</span> <span class="two-parts schedule-left-line">Dr. VINOD NASPURI</span> </li>
                        <li><span class="one-parts">4</span> <span class="two-parts schedule-left-line">Dr. ARYA TRILOK SANKHLA</span> </li>
                        <li><span class="one-parts">5</span> <span class="two-parts schedule-left-line">Dr. VAISHNAVI DUTTA MISHRA</span> </li>
                        <li><span class="one-parts">6</span> <span class="two-parts schedule-left-line">Dr. DIBYASHREE SAHU</span> </li>
                        <li><span class="one-parts">7</span> <span class="two-parts schedule-left-line">Dr. E.LAKSHMAN KUMAR</span> </li>
                        <li><span class="one-parts">8</span> <span class="two-parts schedule-left-line">Dr. NEELI NEELIMA RANI</span> </li>
                        <li><span class="one-parts">9</span> <span class="two-parts schedule-left-line">Dr. SANKI AGRAWAL</span> </li>
                        <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line" style="color:#000;">And many more..</span> </li>
                      </ul>
                    </div>
                  </div>
                </article>
              </div>
              <div id="official" class="display_none">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/r5ESos0mxyw" frameborder="0" allowfullscreen=""></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                            <p>Rank: 28th AIIMS May 2012</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List"> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/rWULb4XiI_0" frameborder="0" allowfullscreen=""></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                            <p>Rank: 28th AIIMS May 2012</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List"> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/9ghDyp0l-WE" frameborder="0" allowfullscreen=""></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                            <p>Rank: 28th AIIMS May 2012</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List"> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/DUfw47Ai5yM" frameborder="0" allowfullscreen=""></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bibhuti Kashyap</span></p>
                            <p>Rank: 28th AIIMS May 2012</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List"> </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </article>
              </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->
</body>
</html>