<!DOCTYPE html>
<?php error_reporting(0); ?>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCI Screening Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<meta name="description" content=" If you are a Foreign Medical Graduate join DAMS for MCI Screening Coaching in New Delhi, India." />
<meta name="keywords" content=" MCI Screening Coaching, MCI Screening Coaching Institute, MCI Screening Coaching Delhi, MCI Screening Coaching India, FMG Screening Coaching Institute" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$courseNav_id = 5;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
    <script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="gallerySlider/owl.carousel.js"></script>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
      <?php $getNavContent=$Dao->getCourseNavContent($courseNav_id); ?>
<!--    <article class="mci-banner">-->
        <article style="width:100%;float:left;height:318px;background: url('https://damsdelhi.com/images/background_images/<?php echo $getNavContent[0][2]; ?>') right top no-repeat;">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left">
      <?php echo $getNavContent[0][1]; ?>
<!--        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>-->
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here -->
<!-- Yellow alert box Here By Anurag Tiwari -->
<section class="alertbox">
    <div class="wrapper">
  <div class="alert_box">
        <div class="alert_title_box">
            <div class="alert_title"><h2 style="font-size: 20px;color: #ffffff;line-height: 24px">VACATION 
COURSE FOR FMG STUDENTS<span style="">Mobile number : 8847293566 (Whatsapp & Wechat only)</span></h2></div>
        </div>
<!--        <div class="alert_btn_box">
            <div class="alert_btn">
                <a class="alert-button" id="yellow" title="" href="doorstep.php">Please Click Here <img src="images/angle-arrow-pointing.png" alt="Right Arrow"></a>
            </div>
        </div>-->
  </div>
    </div>
</section>
<script >
    $(document).ready(function(){
    //     Sign In Form
	$('#yellow-alert').click(function() {
            $('#backPopup').show();
		$('#yellow-alert-pop').show();
    });
	$('#yellow-alert-close').click(function() {
            $('#backPopup').hide();
		$('#yellow-alert-pop').hide();
    });
    
    });
</script>
<div id="backPopup" class="backPopup" style="display: none;"></div>
<div id="yellow-alert-pop" class="yellow-alert-pop" style="display: none;">
<form >
<div class="enquiry-main">
    <div class="career-form-heading yellow-form-heading" style="font-size: 14px;">VACATION 
        COURSE FOR FMG STUDENTS <br>(FOREIGN MEDICAL   GRADUATES)<span class="career-form-close" id="yellow-alert-close"></span></div>
<div class="enquiry-content-main">
<div class="enquiry-content">
<div class="enquiry-content-more yellow-content">

    <div class="yellow-hedder">Any WHERE IN INDIA   LIVE <b style="color:#00A651">/</b> T&D <b style="color:#00A651">/</b> EVENING CLASSES OF INDIVIDUAL SUBJECTS</div>
    <h2 class="yellow-head-title">WHY WASTE TIME?</h2>
    <div class="yellow-box-left">
            <div class="yellow-content-box">
        <h4 class="">Requirements   :</h4>
        <ul>
            <li>COLLAGE   ID  CARD </li>
            <li>VACATION  LETTER </li>
            <li>COPY  OF  PASSPORT </li>
        </ul>
    </div>
    <div class="yellow-content-box-right">
        <h4>REGISTER AT  <a href="">mci@damsdelhi.com</a> With   ABOVE </h4>
    </div>
<!--        <div class="yellow-content-box-full">
        <ul>
            <li>DOCUMENTS.     choice   OF CENTRE   (MIN THREE) 1.2.3</li>
            <li>Fees    =   60,000 + ST. </li>
            <li>Faculty   =   1 TIME BEFORE   EXAM (IN DELHI) + ANY subjects   DURING VACATION   .  At   any  center in   dams ,  india</li>
            <li>Centre  list :</li>
        </ul>
    </div>-->
    </div>
    <div class="yellow-box-left">
        <img src="images/POSTER1.jpg">
    </div>

</div>
</div>
<div class="enquiry-bottom"></div>
</div>
</div>
</form>
</div>
<!-- alert box end -->
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box yellow-content">
          <div class="yellow-form-heading">VACATION 
        COURSE<span class="book-ur-seat-btn"><a title="Book Your Seat" href="online-registration.php"> <span>&nbsp;</span> Book Your Seat</a></span></div>
    <div class="dams-data-nw">
                       <p> &raquo; Are you that kind of Doctor who believe in preparing for future career in medical school ?</p>
                       <p> &raquo; Want to prepare for the exam in Medical school itself ?</p>
                       <p> &raquo; Coming back home in vacations with the study plan but not possible at home ?</p> 
                       <p> &raquo; still wondering how to start preparing for this exam ?</p>
                       
                     <b>WHY TO WASTE TIME ....</b>
                     <p>We are offering all the undergraduate FMGs to join our vacation course and set yourself free from any chaos regarding clearing this Exam. Right Type of preparation at the Right Time. its never too late so,</p>
                     
                     <br>
                     <p>Specially designed on the request of many students who are focused and sincere regarding this exam who wants to spend their vacation in learning and preparing for license exam  . </p>
                     <p>1- Preclinical Basic subjects - Anatomy Physiology, Biochem( can choose any two)</p>
                     <p>2- para clinicals - patho, pharma, Micro and FMT( Any two ) </p>
                     <p> 3- clinical subjects - PSM, ENT and Opthal - (Any one )</p>
                     <p>4- clincal subjects part 2- Medicine 1, 2 ,3 . General surgery 1 and 2 , Obgy , Paedtrics (Any two)</p>
                     <p>5- Image based clinical subjects- Ortho, Derma ,Psychiatry, Anaestesia  And Radio (Any two) </p>
                     <p>6- Proper conceptual based teaching and techniques for the exam </p>
                     <p>7-Test and evaluation performance assessment.</p>
                     <p>8- Duration is your vacation period.</p>
                     
                     
<!--                     <b>JOIN US :</b>
                     <p>Delhi branch - 15th July Onwards</p>
                     <p>Hyderabad Branch - 15th July Onwards</p>
                     <p>Ahmedabad Branch - 15th July Onwards </p>
                     <p>Jaipur Branch - 16 June Onwards</p>-->
                     
                     <b>course features:</b>
                     <p> &raquo; Courses are available for 2nd, 3rd , 4th, 5th , 6th year candidates</p>
                     <p> &raquo; completion of all the subjects and revision before 1st attempt of the exam</p>
                     <p> &raquo; updated version of study material</p>
                     <p> &raquo; updated version of test and discussions</p>
                     
                     <b>Benefits:</b>
                     <p> &raquo;  getting to know what is required to pass this exam </p>
                     <p> &raquo; preparation in your UG ( undergraduate ) time itself increases the probability of clearing this exam</p>
                     <p> &raquo; easy payment methods ( easy installments )</p>
                     <p> &raquo; getting study material and access to all e-book app , e-medicoz , membership in DAMS-Club</p>
                      <p> &raquo; In vacation you will have classes with us and back in university you will get time to revise with our study material.</p>
                      <p> &raquo; you can attend N number of classes once you join this program until your first attempt.</p>
                     <p>&nbsp;&nbsp;&nbsp;or more details contact : ( number and drtp@damsdelhi.com )</p>
        </div>
      </div>

        <?php //include 'middle-accordion.php'; ?>

 <?php //include 'recentAchievement.php'; ?>
    </aside>
    <aside class="content-right">
    <div class="yellow-box-left">
        <img src="images/POSTER1.jpg">
    </div>
        <div class="news-update-box" style="margin-top:25px;">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" style="display:block;" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/XLYmMbEn57U" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd1" style="display:none;" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/GUMZPGghqco" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd2" style="display:none;" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/xtAUAo5NKR8" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd3" style="display:none;" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/7X1ZGMKs0NQ" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="box-bottom-1"> 
<!--            <span class="mini-view-right"></span>-->
<!--          <div class="mini-view-midle"> 
              <a href="#" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>-->
<span class="mini-view-left" style="background: transparent;"></span> </div>
      </div>
    </aside>
  </div>
</section>
<!-- Midle Content End Here -->

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
</body>
</html>
