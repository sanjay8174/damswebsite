<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCI Screening Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<meta name="description" content=" If you are a Foreign Medical Graduate join DAMS for MCI Screening Coaching in New Delhi, India." />
<meta name="keywords" content=" MCI Screening Coaching, MCI Screening Coaching Institute, MCI Screening Coaching Delhi, MCI Screening Coaching India, FMG Screening Coaching Institute" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php
error_reporting(0); 
include 'registration.php';
$course_id = 2;
$courseNav_id = 5;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mci-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include 'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="https://damsdelhi.com/index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="mci_screening.php" title="MCI Screening">MCI Screening</a></li>
          <li><a title="Postal Course" class="active-link">Postal Course</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>MCI Postal Course <span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main">
<!--              <div class="test-series-content paddin-zero">
            <ul class="duration-content"><li><label>Price :</label><span>6000/-</span></li></ul>
          </div>-->
              <aside class="privacy-content">
                <p>If you can not come to DAMS, our courses will come to you. Join our Postal Series Delhi Academy of Medical Sciences (DAMS) provides postal courses. The study material is developed by DAMS, which has been working since more than one decade. The study material is compact &amp; effective which is neither bulky nor vague. Instead it is easy to understand and has unique presentation of all the subject as per requirements of examination. DAMS team has worked hard to provide error free text and smart &amp; shortcut methods to solve problems.</p>
              </aside>
              <aside class="shortcut-to-nimhans paddin-zero">
                <div class="how-to-apply-heading"><span></span> About the exam :-</div>
                <p>MCI SCREENING  for FMGEs: Conducted twice a year by NBE June &amp; December by a Computer Based Test.  This paper encompasses MCQ's of the objective nature. Questions of the graduate level shall be posed in this paper. The time period of this examination is based on the many departments. Aspirants who have a clear knowledge about the pattern and syllabus of the MBBS course will be able to perform admirably in this exam.</p>
              </aside>
              <aside class="shortcut-to-nimhans">
                <div class="how-to-apply-heading"><span></span>Course Highlights :-</div>
                <ul class="benefits">
                  <li><span></span>The study material includes full coverage of syllabus of PG medical entrance examination including high yielding text as well as cutting edge question banks.</li>
                  <li><span></span>The study material includes subject wise sequentially presented theory sections and practice sets which include solved questions with answers and explanations.</li>
                  <li><span></span>The previous feedback of the students is highly appreciable who have succeded in various examinations.</li>
                  <li><span></span>Study material is time to time updated and latest updation and current developments are sent time to time .</li>
                </ul>
              </aside>
            </article>
            <div class="book-ur-seat-btn margn-zero"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></div>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>