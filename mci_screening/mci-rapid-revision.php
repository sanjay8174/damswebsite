<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCI Screening Coaching Institute, Rapid Revision Series (RSS)</title>
<meta name="description" content=" If you are a Foreign Medical Graduate join DAMS for MCI Screening Coaching in New Delhi, India." />
<meta name="keywords" content=" MCI Screening Coaching, MCI Screening Coaching Institute, MCI Screening Coaching Delhi, MCI Screening Coaching India, FMG Screening Coaching Institute" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php';
$course_id = 2;
$courseNav_id = 5;
?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mci-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="http://damsdelhi.com/index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="mci_screening.php" title="MCI Screening">MCI Screening</a></li>
          <li><a title="Rapid Revision Series" class="active-link">Rapid Revision Series (RRS)</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading responc-left-heading paddin-zero">
            <h4>Rapid Revision Series (RRS) <span class="book-ur-seat-btn book-hide"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></span></h4>
            <article class="showme-main paddin-zero">
              <aside class="about-content">
                <div class="ne-pro-data">
                    <br>
                  <strong> RRS- Programme<br>
                  RAPID REVISION SERIES FOR THE VERY FIRST TIME DAMS-DVT now replicated in MCI format with MCI-RRS <br>
                  <br>
                  </strong>It is a programme which includes a rapid theory revision with our faculty experts in each subject. Test and discussion after finishing one subject and study material with high yielding points. It will provide an edge to the core to prepare for the exam.<br>
                  <br>
                  <strong>A MUST JOIN EVENT TO CROSS THE 150 BARRIER.</strong><br>
                   FEE RAPID REVISION SERIES (RRS):- 6500/- INCLUDE TAX <br>
                    DURATION OF COURSE: 05 DAYS<br>
                  <strong>DATE: - 1<sup>st</sup> MAY TO 5<sup>th</sup> MAY 2018</strong><br>
                  <strong>TIMING: - 9:00 AM TO 9:00 PM</strong><br>
                  <strong>LAST DATE OF REGISTRATION: 25TH APRIL 2018</strong><br><br>
                  <strong>SUBJECTS COVERED : 15 subjects</strong><br>
                   <div class="new-data-dam">
                       <div class="left-data">
                          PSM<br>
                          OBG<br>
                          SURGERY<br>
                          MEDICINE<br>
                          PSYCHIATRY<br>
                          RADIOLOGY<br>
                          ORTHO<br>
                          FMT<br> 
                       </div>
                       <div class="right-data">
                     DERMATOLOGY<br>
                     PATHOLOGY<br>
                     PHARMACOLOGY<br>
                     MICROBIOLOGY<br>
                     PAEDIATRICS<br>
                     ENT<br>
                     OPTHALMOLOGY<br> 
                       </div>
                    </div>
                     
                  
                 
                    <strong>PLACE : Delhi ( will update the exact location at The time of registration )</strong><br>
            
                </div>
                <div class="book-ur-seat-btn"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></div>
              </aside>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include 'right-accordion.php'; ?>
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>