<?php include 'openconnection.php'; $path = constant::$path; ?>
<style>
div#preloader { position: fixed; 
                left: 0; top: 0; z-index: 999; 
                width: 100%; 
                height: 100%; 
                overflow: visible; 
               // background: transparent url('<?php echo $path; ?>/images/dams.gif') no-repeat center center; 
                background: transparent url('../images/dams.gif') no-repeat center center; 
}
</style>
<div class="enquiry-main">
<div class="enquiry-heading">Feedback Form</div>
<div class="enquiry-content-main">
<div class="enquiry-content">
<div class="enquiry-content-more">
<form id="feedbackform" name="feedbackform"  method="post" action="#" onsubmit="return ValidateFeedBackForm();">
<input type="hidden" name="mode1" id="mode1" value="feedbackform" />
<input type="hidden" id="redirectUrl" name="redirectUrl" value="<?php $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url;?>"/>
<div id="preloader11" style="display: none;"></div>
<div class="career-box">
<div class="right-ip-1">
<input name="userName" type="text" class="enquiry-input" id="enrollNo" value="" onfocus="if(this.value=='Enrollment No')this.value='';" onblur="if(this.value=='')this.value='Enrollment No';" placeholder="Email" />
</div>
</div>
<div class="career-box">
<div class="right-ip-1">
<input name="userName" type="text" class="enquiry-input" id="name" value="" onfocus="if(this.value=='Name')this.value='';" onblur="if(this.value=='')this.value='Name';"  placeholder="Name" />
</div>
</div>
<!--<div class="career-box">
<div class="right-ip-1">
<input name="userName" type="text" class="enquiry-input" id="enquiryform1" value="Your Name" onfocus="if(this.value=='Your Name')this.value='';" onblur="if(this.value=='')this.value='Your Name';" />
</div></div>-->
<div class="career-box">
<div class="right-ip-1">
<input name="userEmail" type="text" class="enquiry-input" id="mobileFeedback" value="" onfocus="if(this.value=='Contact No')this.value='';" onblur="if(this.value=='')this.value='Contact No';" placeholder="Contact No" />
</div>
</div>
<div class="career-box">
<div class="right-ip-1">
<input name="userEmail" type="text" class="enquiry-input" id="mobilewhstappFeedback" value="" onfocus="if(this.value=='Contact No')this.value='';" onblur="if(this.value=='')this.value='Contact No';" placeholder="What's App Number" />
</div>
</div>
<div class="career-box">
<div class="right-ip-1 form-txt">
<p>Q1. Which subject was not given adequate time or study material??</p>
</div>
</div>
<div class="career-box">
<div class="right-ip-1">
<input name="userName" class="enquiry-input" id="question1" value=""  type="text" placeholder="Type your answer">
</div>
</div>
<div class="career-box">
<div class="right-ip-1 form-txt">
<p>Q2. Which professor needs a change in pattern of teaching and what it should be??</p>
</div>
</div>
<div class="career-box">
<div class="right-ip-1">
<input name="userName" class="enquiry-input" id="question2" value=""  type="text" placeholder="Type your answer">
</div>
</div>
	<div class="career-box">
<div class="right-ip-1 form-txt">
<p>Q3. Was it value for money, joining DAMS for MCI-coaching??</p>
</div>
</div>
<div class="career-box">
<div class="right-ip-1">
<input name="userName" class="enquiry-input" id="question3" value=""   type="text" placeholder="Type your answer">
</div>
</div>
	<div class="career-box">
<div class="right-ip-1 form-txt">
<p>Q4. How you will rate services and behavior of DAMS office members??</p>
</div>
</div>
<div class="career-box">
<div class="right-ip-1">
<input name="userName" class="enquiry-input" id="question4" value=""   type="text" placeholder="Type your answer">
</div>
</div>
	<div class="career-box">
<div class="right-ip-1 form-txt">
<p>Q5. How you will rate our study material and test provided online on cloud ID??</p>
</div>
</div>
<div class="career-box">
<div class="right-ip-1">
<input name="userName" class="enquiry-input" id="question5" value=""  type="text" placeholder="Type your answer">
</div>
</div>
	<div class="career-box">
<div class="right-ip-1 form-txt">
<p>Q6. Five things they Like and Dislike about DAMS.</p>
</div>
</div>
<div class="career-box">
<div class="right-ip-1 form-txt">
<p style="color:#000;font-weight:bold;">Likes</p>
</div>
</div>
<div class="career-box">
<div class="right-ip-1 right-new-cls">
<ol type="I">
<li>
<input name="userName" class="enquiry-input input-data-new" id="question61likes" value=""  type="text" placeholder="Type your answer">
</li>
<li>
<input name="userName" class="enquiry-input input-data-new" id="question62likes" value=""  type="text" placeholder="Type your answer">
</li>
<li>
<input name="userName" class="enquiry-input input-data-new" id="question63likes" value=""  type="text" placeholder="Type your answer">
</li>
<li>
<input name="userName" class="enquiry-input input-data-new" id="question64likes" value=""  type="text" placeholder="Type your answer">
</li>
<li>
<input name="userName" class="enquiry-input input-data-new" id="question65likes" value=""  type="text" placeholder="Type your answer">
</li>

</ol>
</div>
</div>
<div class="career-box">
<div class="right-ip-1 form-txt">
<p style="color:#000;font-weight:bold;">Dislikes</p>
</div>
</div>
<div class="career-box">
<div class="right-ip-1 right-new-cls">
<ol type="I">
<li>
<input name="userName" class="enquiry-input input-data-new" id="question61Dislikes" value=""  type="text" placeholder="Type your answer">
</li>
<li>
<input name="userName" class="enquiry-input input-data-new" id="question62Dislikes" value=""  type="text" placeholder="Type your answer">
</li>
<li>
<input name="userName" class="enquiry-input input-data-new" id="question63Dislikes" value="" type="text" placeholder="Type your answer">
</li>
<li>
<input name="userName" class="enquiry-input input-data-new" id="question64Dislikes" value=""  type="text" placeholder="Type your answer">
</li>
<li>
<input name="userName" class="enquiry-input input-data-new" id="question65Dislikes" value="" type="text" placeholder="Type your answer">
</li>

</ol>
</div>
</div>	
<div class="career-box">
<div class="right-ip-1 form-txt">
<p style="color:#000;">Your Comments:-</p>
</div>
</div>
<div class="career-box">
<div class="right-ip-1">
<textarea name="userMessage" rows="3" class="enquiry-message" id="comments" placeholder="Write your Comments here"></textarea>
</div></div><div class="submit-enquiry"><a href="javascript:void(0);" title="Submit" onclick="ValidateFeedBackForm();"><span></span> Submit</a></div>
<div class="error_msg_box" id="enquiryError" style="display:none"> Please enter correct email address. </div>
</form>
</div>
</div>
<div class="enquiry-bottom"></div>
</div>
</div>