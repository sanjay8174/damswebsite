<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS, AIPG(NBE/NEET) Pattern PG</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var fid=$('.apply-now > a').size();
//     Career Form
    for(a=1;a<=fid;a++)
	{
       $('#student-career'+a).click(function(e) {
		  $('#backPopup').show();
		 $('#frontPopup').show();       
    });
	}
	$('#student-career-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup').hide();
    });
		
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">

<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->

<?php include 'social-icon.php'; ?>

<?php include 'header.php'; ?>

<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="career-banner">

<aside class="banner-left banner-left-postion" style="padding-top:10%;">
<h2>Career</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>

<aside class="banner-right">
<div class="banner-right-btns">
<!--<a href="dams-store.php" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a>-->
<a href="http://damspublications.com/" target="_blank" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a>
<a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a>
<a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a>
</div></aside>
</article>
</div>
</section> 
<!-- Banner End Here -->

<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">

<div class="photo-gallery-main">

<section class="event-container">
<aside class="center-left">
<div class="inner-center">
<h4>Career at DAMS</h4>
<article class="showme-main">

<div class="career-content">
<p>For over 11 years Delhi Academy of Medical Sceiences (DAMS) has nutured and led hundreds of students to PG Medical Entrance Success.  At DAMS, we are working toward a future where everyone's potential can be fulfilled and we're growing fast through our impact.</p>
<p>&nbsp;</p>
<p>We are committed to being a great place to work. We are committed to making a big difference in the world. If you want substantial difference through your work, AND want to have fun in a challenging yet supportive environment, take a look at the positions we have currently available:</p>

<div class="main-position-set">
<h5>Positions Available</h5>

<ul>
<li>
<aside class="position-set-left">
<div class="position-box">
<div class="position-inner-box">
<div class="position-content-box">
<div class="position-heading"> <span></span> Position Heading</div>
<div class="position-content">
<p>Preferably well  educated female for working with our medical 
students and doctors. Explain our courses to them and 
motivate them. Salary is no bar for deserving candidates.</p>
<div class="apply-now"><a href="javascript:void(0);" id="student-career1" title="Apply Now">Apply&nbsp;Now</a></div>
</div>
</div>
</div></div>
</aside>

<aside class="position-set-right">
<div class="position-box">
<div class="position-inner-box">
<div class="position-content-box">
<div class="position-heading"> <span></span> Marketing Manager</div>
<div class="position-content">
<p>Preferably well  educated female for working with our medical 
students and doctors. Explain our courses to them and 
motivate them. Salary is no bar for deserving candidates.</p>
<div class="apply-now"><a href="javascript:void(0);" id="student-career2"  title="Apply Now">Apply&nbsp;Now</a></div>
</div>
</div>
</div></div>
</aside>
</li>

<li>
<aside class="position-set-left">
<div class="position-box">
<div class="position-inner-box">
<div class="position-content-box">
<div class="position-heading"> <span></span> Faculty to train students for PG Medical Entrance</div>
<div class="position-content">
<p>Preferably well  educated female for working with our medical 
students and doctors. Explain our courses to them and 
motivate them. Salary is no bar for deserving candidates.</p>
<div class="apply-now"><a href="javascript:void(0);" id="student-career3" title="Apply Now">Apply&nbsp;Now</a></div>
</div>
</div>
</div></div>
</aside>

<aside class="position-set-right">
<div class="position-box">
<div class="position-inner-box">
<div class="position-content-box">
<div class="position-heading"> <span></span>  Admin Executives</div>
<div class="position-content">
<p>Preferably well  educated female for working with our medical 
students and doctors. Explain our courses to them and 
motivate them. Salary is no bar for deserving candidates.</p>
<div class="apply-now"><a href="javascript:void(0);" id="student-career4" title="Apply Now">Apply&nbsp;Now</a></div>
</div>
</div>
</div></div>
</aside>
</li>
</ul>
</div>
</div>
</article>
</div> 
</aside> 
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->
</body></html>
