<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]>
<link href="css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">
<div class="big-nav">
<ul>
<li class="face-face"><a href="course.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement" class="a-achievement-active">Achievement</a></li>
</ul>
</div>
<aside class="banner-left">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include'md-ms-banner-btn.php'; ?>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span>
<ul>
<li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Achievement" class="active-link">Achievement</a></li>
</ul>
</div>

<section class="video-container">

<div class="achievment-left">
<div class="achievment-left-links">
<ul id="accordion">
<li class="border_none" id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
<ol class="achievment-inner display_none" id="aol1">
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_jun_2014.php" title="DNB June">DNB June</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2014.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2014.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2014.php" title="AIPGME">AIPGME</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="UPPG_2014.php" title="UPPG">UPPG</a></li>
</ol>
</li>

<li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
<ol class="achievment-inner display_none" id="aol2">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2013.php" title="AIPGME">AIPGME</a></li>
</ol>
</li>

<li id="accor3" onClick="ontab('3');"><span id="accorsp3" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
<ol class="achievment-inner display_none" id="aol3">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>
</ol>
</li>

<li id="accor4" onClick="ontab('4');" class="light-blue-inner"><span id="accorsp4" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
<ol class="achievment-inner display_block" id="aol4"> 
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
<li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
</ol>
</li>
</ul>
</div>
</div>

<aside class="gallery-left respo-achievement right_pad0">
<div class="inner-left-heading">
<h4>ASAM PG 2011</h4>
<section class="showme-main">
<ul class="idTabs idTabs1"> 
<li><a href="#jquery" class="selected" title="ASAM PG 2011">ASAM PG 2011</a></li>
<li><a href="#official" title="Interview">Interview</a></li>
</ul>

<div id="jquery">
<article class="interview-photos-section">
<div class="schedule-mini-series">
<div class="schedule-mini-top">
<span class="one-part">Rank</span>
<span class="two-part">Student Name</span>
<span class="three-part">&nbsp;</span>
</div>
<div class="schedule-mini-content">
<ul>
<li class="boder-none"><span class="one-parts">2</span>
<span class="two-parts schedule-left-line">Dr. ANKUR JINDAL</span>
</li>
<li><span class="one-parts">8</span>
<span class="two-parts schedule-left-line">Dr. KALPESH CHOUDHARY</span>
</li>
<li><span class="one-parts">10</span>
<span class="two-parts schedule-left-line">Dr. SUREKHA</span>
</li>
<li><span class="one-parts">11</span>
<span class="two-parts schedule-left-line">Dr. ARUN KUMAR</span>
</li>
<li><span class="one-parts">15</span>
<span class="two-parts schedule-left-line">Dr. NEHA SHEERA</span>
</li>
<li><span class="one-parts">18</span>
<span class="two-parts schedule-left-line">Dr. DANNY KUMAR</span>
</li>
<li><span class="one-parts">49</span>
<span class="two-parts schedule-left-line">Dr. YAMINI</span>
</li>
<li><span class="one-parts">55</span>
<span class="two-parts schedule-left-line">Dr. NEHA SAHAY</span>
</li>
<li><span class="one-parts">58</span>
<span class="two-parts schedule-left-line">Dr. ARPITA</span>
</li>
<li><span class="one-parts">62</span>
<span class="two-parts schedule-left-line">Dr. NANI TAGO</span>
</li>
<li><span class="one-parts">70</span>
<span class="two-parts schedule-left-line">Dr. GAURAV MUKSHI</span>
</li>
<li><span class="one-parts">87</span>
<span class="two-parts schedule-left-line">Dr. APELI SUKHALU</span>
</li>
<li><span class="one-parts">96</span>
<span class="two-parts schedule-left-line">Dr. TASSO BYAI</span>
</li>
<li><span class="one-parts">97</span>
<span class="two-parts schedule-left-line">Dr. DIVYJ PASRIJA</span>
</li>
<li><span class="one-parts">115</span>
<span class="two-parts schedule-left-line">Dr. MOHAN</span>
</li>
<li><span class="one-parts">121</span>
<span class="two-parts schedule-left-line">Dr. BHARTI BHANDRI</span>
</li>
<li><span class="one-parts">142</span>
<span class="two-parts schedule-left-line">Dr. MANOJ KUMAR TYAGI</span>
</li>
<li><span class="one-parts">156</span>
<span class="two-parts schedule-left-line">Dr. SARANSH JAIN</span>
</li>
<li><span class="one-parts">160</span>
<span class="two-parts schedule-left-line">Dr. PREETI SINDHU</span>
</li>
<li><span class="one-parts">161</span>
<span class="two-parts schedule-left-line">Dr. DIVYA VERMA</span>
</li>
<li><span class="one-parts">166</span>
<span class="two-parts schedule-left-line">Dr. RAJAT JAIN</span>
</li>
<li><span class="one-parts">167</span>
<span class="two-parts schedule-left-line">Dr. SAKSHI SACHDEVA</span>
</li>
<li><span class="one-parts">174</span>
<span class="two-parts schedule-left-line">Dr. GAURAV MENWAL </span>
</li>
<li><span class="one-parts">178</span>
<span class="two-parts schedule-left-line">Dr. DEEPAK GUPTA</span>
</li>
<li><span class="one-parts">234</span>
<span class="two-parts schedule-left-line">Dr. ANURODH DADARWAL</span>
</li>
<li><span class="one-parts">257</span>
<span class="two-parts schedule-left-line">Dr. SWATI SATTAVAN</span>
</li>
<li><span class="one-parts">280</span>
<span class="two-parts schedule-left-line">Dr. VIVEK SHARMA</span>
</li>
<li><span class="one-parts">288</span>
<span class="two-parts schedule-left-line">Dr. VILI SWN</span>
</li>
<li><span class="one-parts">291</span>
<span class="two-parts schedule-left-line">Dr. GANPAT</span>
</li>
<li><span class="one-parts">301</span>
<span class="two-parts schedule-left-line">Dr. HIMANSHU GUPTA</span>
</li>
<li><span class="one-parts">356</span>
<span class="two-parts schedule-left-line">Dr. MILLO ASHA</span>
</li>
<li><span class="one-parts">391</span>
<span class="two-parts schedule-left-line">Dr. LALTLANZOVI</span>
</li>
<li><span class="one-parts">402</span>
<span class="two-parts schedule-left-line">Dr. ARVIND KUMAR</span>
</li>
<li><span class="one-parts">413</span>
<span class="two-parts schedule-left-line">Dr. PRERNA GARG</span>
</li>
<li><span class="one-parts">418</span>
<span class="two-parts schedule-left-line">Dr. NITASHA AHIR , Dr. NITASHA AHIR</span>
</li>
<li><span class="one-parts">428</span>
<span class="two-parts schedule-left-line">Dr. VASUDHA GOYAL</span>
</li>
<li><span class="one-parts">493</span>
<span class="two-parts schedule-left-line">Dr. DEEPSHIKHA PARIHAR , Dr. DIVYA KUMAR</span>
</li>
<li><span class="one-parts">553</span>
<span class="two-parts schedule-left-line">Dr. TASHA AHIR</span>
</li>
<li><span class="one-parts">587</span>
<span class="two-parts schedule-left-line">Dr. LAKSHMI ANANDINI</span>
</li>
<li><span class="one-parts">618</span>
<span class="two-parts schedule-left-line">Dr. RICHA GUPTA</span>
</li>
<li><span class="one-parts">641</span>
<span class="two-parts schedule-left-line">Dr. PRASHANT GUPTA</span>
</li>
<li><span class="one-parts">648</span>
<span class="two-parts schedule-left-line">Dr. MANISH KUMAR</span>
</li>
<li><span class="one-parts">650</span>
<span class="two-parts schedule-left-line">Dr. JUHI BANSAL</span>
</li>
<li><span class="one-parts">699</span>
<span class="two-parts schedule-left-line">Dr. MOHIT GARG</span>
</li>
<li><span class="one-parts">719</span>
<span class="two-parts schedule-left-line">Dr. GAGANDEEP SINGH</span>
</li>
<li><span class="one-parts">760</span>
<span class="two-parts schedule-left-line">Dr. SUMAN SAURABH</span>
</li>
<li><span class="one-parts">773</span>
<span class="two-parts schedule-left-line">Dr. SHALINI AGARWAL</span>
</li>
<li><span class="one-parts">846</span>
<span class="two-parts schedule-left-line">Dr. ANAMIKA SHARMA</span>
</li>
<li><span class="one-parts">1029</span>
<span class="two-parts schedule-left-line">Dr. RUPESH KESHRI</span>
</li>
<li><span class="one-parts">1063</span>
<span class="two-parts schedule-left-line">Dr. KUMKUM GUPTA</span>
</li>
<li><span class="one-parts">1066</span>
<span class="two-parts schedule-left-line">Dr. MANDEEP KAUR</span>
</li>
<li><span class="one-parts">1099</span>
<span class="two-parts schedule-left-line">Dr. ANKIT VERMA</span>
</li>
<li><span class="one-parts">1182</span>
<span class="two-parts schedule-left-line">Dr. VIKRAM DESWAL</span>
</li>
<li><span class="one-parts">1328</span>
<span class="two-parts schedule-left-line">Dr. DEEPTI NAGAR</span>
</li>
<li><span class="one-parts">1388</span>
<span class="two-parts schedule-left-line">Dr. SWATI BANSAL</span>
</li>         
<li><span class="one-parts">1488</span>
<span class="two-parts schedule-left-line">Dr. SHILPI AGARWAL</span>
</li>
<li><span class="one-parts">1536</span>
<span class="two-parts schedule-left-line">Dr. PARUL GAUTAM , Dr. PARUL JAIN</span>
</li>
<li><span class="one-parts">1567</span>
<span class="two-parts schedule-left-line">Dr. NIKITA JINJAL</span>
</li>   
<li><span class="one-parts">1628</span>
<span class="two-parts schedule-left-line">Dr. VISHAL BANSAL</span>
</li>
<li><span class="one-parts">1800</span>
<span class="two-parts schedule-left-line">Dr. DEEPAK</span>
</li>
<li><span class="one-parts">1807</span>
<span class="two-parts schedule-left-line">Dr. ARUN KUMAR</span>
</li>
<li><span class="one-parts">1871</span>
<span class="two-parts schedule-left-line">Dr. NIKHIL VERMA</span>
</li>
<li><span class="one-parts">1899</span>
<span class="two-parts schedule-left-line">Dr. AMIT RANJAN</span>
</li>
<li><span class="one-parts">1979</span>
<span class="two-parts schedule-left-line">Dr. SUBHAM MEHTA</span>
</li>
<li><span class="one-parts">1988</span>
<span class="two-parts schedule-left-line">Dr. DEEPTI AGGARWAL</span>
</li>
<li><span class="one-parts">2021</span>
<span class="two-parts schedule-left-line">Dr. NEHA JAIN</span>
</li>
<li><span class="one-parts">2026</span>
<span class="two-parts schedule-left-line">Dr. SANTOSH KUMAR</span>
</li>
<li><span class="one-parts">2123</span>
<span class="two-parts schedule-left-line">Dr. SHIVANSHU MISRA</span>
</li>
<li><span class="one-parts">2176</span>
<span class="two-parts schedule-left-line">Dr. AJAY AGGARWAL</span>
</li>
<li><span class="one-parts">2193</span>
<span class="two-parts schedule-left-line">Dr. SONAL AGARWAL</span>
</li>
<li><span class="one-parts">2198</span>
<span class="two-parts schedule-left-line">Dr. VIDHI SAHRI</span>
</li>
<li><span class="one-parts">2419</span>
<span class="two-parts schedule-left-line">Dr. AIPG(NBE/NEET) PatternU</span>
</li>         
<li><span class="one-parts">2562</span>
<span class="two-parts schedule-left-line">Dr. ANKITA KABI</span>
</li>
<li><span class="one-parts">2595</span>
<span class="two-parts schedule-left-line">Dr. CHANDAN KUMAR</span>
</li>
<li><span class="one-parts">2672</span>
<span class="two-parts schedule-left-line">Dr. PAWAN KUMAR YADAV</span>
</li>   
<li><span class="one-parts">2695</span>
<span class="two-parts schedule-left-line">Dr. DEEPIKA</span>
</li>
<li><span class="one-parts">2725</span>
<span class="two-parts schedule-left-line">Dr. SAKSHI MAHAJAN</span>
</li>
<li><span class="one-parts">2764</span>
<span class="two-parts schedule-left-line">Dr. SUREKHA</span>
</li>
<li><span class="one-parts">2963</span>
<span class="two-parts schedule-left-line">Dr. MONIKA SINGH</span>
</li>
<li><span class="one-parts">3012</span>
<span class="two-parts schedule-left-line">Dr. MANVEEN KAUR</span>
</li>
<li><span class="one-parts">3044</span>
<span class="two-parts schedule-left-line">Dr. JYOTI</span>
</li>
<li><span class="one-parts">3146</span>
<span class="two-parts schedule-left-line">Dr. ABHISHEK HANDA</span>
</li>
<li><span class="one-parts">3244</span>
<span class="two-parts schedule-left-line">Dr. VARSHA NARULA</span>
</li>
<li><span class="one-parts">3495</span>
<span class="two-parts schedule-left-line">Dr. GAURAV KUMAR</span>
</li>
<li><span class="one-parts">3773</span>
<span class="two-parts schedule-left-line">Dr. SHWETA SHARMA</span>
</li>
<li><span class="one-parts">3794</span>
<span class="two-parts schedule-left-line">Dr. GOPINATH BHAGAT</span>
</li>
<li><span class="one-parts">3862</span>
<span class="two-parts schedule-left-line">Dr. RUPESH KUMAR</span>
</li>
<li><span class="one-parts">3900</span>
<span class="two-parts schedule-left-line">Dr. MISHA YADAV</span>
</li>
<li><span class="one-parts">3902</span>
<span class="two-parts schedule-left-line">Dr. APARNA ARYA</span>
</li>    
<li><span class="one-parts">4121</span>
<span class="two-parts schedule-left-line">Dr. SANJEET KUMAR</span>
</li>
<li><span class="one-parts">4208</span>
<span class="two-parts schedule-left-line">Dr. NIRMAL KHASA</span>
</li>
<li><span class="one-parts">4262</span>
<span class="two-parts schedule-left-line">Dr. NITIKA CHAWLA</span>
</li>
<li><span class="one-parts">4272</span>
<span class="two-parts schedule-left-line">Dr. DUSHYANT KUMAR</span>
</li>
<li><span class="one-parts">4328</span>
<span class="two-parts schedule-left-line">Dr. ANUPREET KAUR</span>
</li>
<li><span class="one-parts">4345</span>
<span class="two-parts schedule-left-line">Dr. SAPNA SINGH</span>
</li>
<li><span class="one-parts">4356</span>
<span class="two-parts schedule-left-line">Dr. SETU VARUN KUMAR</span>
</li>
<li><span class="one-parts">4499</span>
<span class="two-parts schedule-left-line">Dr. VIKAS KUMAR</span>
</li>
<li><span class="one-parts">4547</span>
<span class="two-parts schedule-left-line">Dr. TANISHA BHARARA</span>
</li>
<li><span class="one-parts">4672</span>
<span class="two-parts schedule-left-line">Dr. NIDHI PARASHAR</span>
</li>
<li><span class="one-parts">4747</span>
<span class="two-parts schedule-left-line">Dr. BHAVNA GULATI</span>
</li>
<li><span class="one-parts">4946</span>
<span class="two-parts schedule-left-line">Dr. NAVDEEP AHUJA</span>
</li>
<li><span class="one-parts">4997</span>
<span class="two-parts schedule-left-line">Dr. SHYAM MISHRA</span>
</li> 
<li><span class="one-parts">5024</span>
<span class="two-parts schedule-left-line">Dr. SANDEEP KUMAR CHAUDHARY</span>
</li>
<li><span class="one-parts">5147</span>
<span class="two-parts schedule-left-line">Dr. ADITI DUBEY</span>
</li>
<li><span class="one-parts">5174</span>
<span class="two-parts schedule-left-line">Dr. SUDIPTA JOY</span>
</li>
<li><span class="one-parts">5249</span>
<span class="two-parts schedule-left-line">Dr. SOUMYA RANJAN ACHARYA</span>
</li>
<li><span class="one-parts">5319</span>
<span class="two-parts schedule-left-line">Dr. RAKHEE SHARMA</span>
</li>
<li><span class="one-parts">5552</span>
<span class="two-parts schedule-left-line">Dr. RAHUL BHARGAWA</span>
</li>
<li><span class="one-parts">6294</span>
<span class="two-parts schedule-left-line">Dr. DIVYA VERMA</span>
</li>
<li><span class="one-parts">9369</span>
<span class="two-parts schedule-left-line">Dr. HIMANI</span>
</li>
<li><span class="one-parts">&nbsp;</span>
<span class="two-parts schedule-left-line" style="color:#000;">And many more...</span>
</li>
</ul>
</div>
</div>
</article></div>

<div id="official"> 
<article class="interview-photos-section"> 
<div class="achievment-videos-section">
<ul>
<li>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="boder-none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Tuhina Goel</span></p>
<p>Rank: 1st AIIMS Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="boder-none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Nishant Kumar</span></p>
<p>Rank: 56th AIIMS Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
</li>

<li>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="boder-none"></iframe> 
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Geetika Srivastava</span></p>
<p>Rank: 6th PGI Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="boder-none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Bhawna Attri</span></p>
<p>Rank: 24th PGI Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
</li>
</ul>
</div>
</article> 
</div>
</section>
</div>
</aside>
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>