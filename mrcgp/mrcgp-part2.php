<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>MRCGP Coaching Institute, MRCGP</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg no_bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
          <h2>MRCGP - Delhi</h2>
          <!--<h3>MEMBERSHIP OF THE ROYAL COLLEGES OF OBSTETRICS AND GYNAECOLOGISTS -UK</h3>-->
           <h3> INTERNATIONAL MEMBERSHIP OF ROYAL COLLEGES OF GENERAL PRACTITIONER</h3>
          <h3>MRCGP(International)EXAM COURSE PART-II,Delhi</h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<!--<div style="overflow:hidden;">-->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box mrcog_crsbox ">
       <ul class="idTabs responce-show">
        <li><a href="index.php?c=6&n=" >PART II</a></li>
        <li><a href="#part2">FAQ'S</a></li>
       </ul>
      
      </div>
        
       <div class="course-box mrcog_crsbox ">
      <div>
          <div style="border: 1px solid rgb(204, 204, 204); display: inline-block; padding: 20px; margin-top: -1px;">
        <!---  <h3><span class="book-ur-seat-btn"><a title="Book Your Seat" href="online-registration.php"> <span>&nbsp;</span> Book Your Seat</a></span></h3>-->
        
        
              <p class="fqa_qstyle"><b>Q.1</b>What is the eligibility criterion to apply for the examination?</p>
        <p class="fqa_ansstyle">The details of the all three categories of the eligibility criteria are given below.<br>
          <strong> Either:</strong><br>
          <b>. Certification of completion of a structured three years training course</b> inclusive of experience in Family Medicine/General Practice and accredited by the South Asia Region MRCGP [INT] Board.
           <br>
           <strong>Or</strong><br>
          <b> . Certification of completion of an accredited one year programme in Family Medicine with an additional three years clinical experience.</b> This should consist of either three years in FM or a minimum of two years in FM with a further year in a speciality/-ies allied to Family Medicine/General Practice that are certified by the local accrediting organisation and approved by the regional MRCGP [INT] Board.
           <br><strong>Or</strong><br>
           <b>. Minimum of five years clinical experience of which a minimum of three years should be in Family Medicine/General Practice </b>and the other years in speciality/-ies allied to Family Medicine/General Practice certified by the local accrediting organisation and approved by the regional MRCGP [INT] Board.
        </p>
        <p class="fqa_qstyle"><b>Q.2</b>What are the other sub-specialties accepted by the South Asia Board? </b></p>
        <p class="fqa_ansstyle">The acceptable sub-specialties for the remaining required clinical experience in eligibility criteria 2 & 3 include Internal medicine, Obs & Gynae, Paeds, Psychiatry, and Emergency Medicine.  Up to six months of Surgery experience is also accepted.  
        </p>
        <p class="fqa_qstyle"><b>Q.3</b>I have four years of clinical experience and one year house job / internship experience; Am I eligible to apply?</p>
        <p class="fqa_ansstyle">The internship experience is not counted in the required clinical experience duration so one another 1 year of clinical experience is required to apply under criteria 3.  
        </p> 
        <p class="fqa_qstyle"><b>Q.4</b> Tell me the dates of the Part I & OSCE examination in the current year, the fee structure and the registration dates. </p>
        <p class="fqa_ansstyle">Details of all examinations scheduled in the current year are available in examination calendar on the website.  The registration opening dates and fee details are also available in the same document. 
        </p> 
        <p class="fqa_qstyle"><b>Q.5</b>How can I get the application form and where should I submit it? </p>
        <p class="fqa_ansstyle"> Application form is published on the website on the registration opening date and can be downloaded.  Form along with the required documents can be sent directly to the South Asia admin office in Karachi or to any of the regional offices (details of all the offices are available on the website).
        </p> 
        <p class="fqa_qstyle"><b>Q.6</b>Does the South Asia Board offers exemption from any part of the exam on the basis of any qualification? </p>
        <p class="fqa_ansstyle">The MRCGP [INT] South Asia Board does not offer any exemption on any basis.
        </p> 
       <p class="fqa_qstyle"><b>Q.7</b>I have cleared Part I exam from MRCGP [INT] UAE / (any other approved site), Can I apply for the South Asian Part II OSCE exam?  </p>
        <p class="fqa_ansstyle">All parts of the examination are to be cleared from one accredited site. 
        </p> 
        <p class="fqa_qstyle"><b>Q.8</b>Please confirm the documents to be submitted along with the Part I application form.</p>
        <p class="fqa_ansstyle">An application guide is published on the website on the examination registration opening date and contains the details of all the documents to be attached with the application form.
        </p> 
        <p class="fqa_qstyle"><b>Q.9</b>I have previously appeared / cleared Part I exam. Do I need to send all experience certificates again with the next part I or OSCE application.  </p>
        <p class="fqa_ansstyle">Candidates already registered only need to send a filled and signed updated version of the application form, copy of any valid medical license, photographs and the bank drafts.  OSCE candidates must attach part I passing email.   

        </p> 
        <p class="fqa_qstyle"><b>Q.10</b>All queries regarding the International Membership of Royal College of General Practitioners (RCGP). </p>
        <p class="fqa_ansstyle">All membership queries are dealt by the membership department at RCGP and queries should be sent to <a style="font-size:15px;" href="https://www.gmail.com/membership@rcgp.org.uk">membership@rcgp.org.uk</a> 
        </p> 
        <p class="fqa_qstyle"><b>Q.11</b>For examination registration, Is it mandatory to get membership from any of the family medicine organization?   </p>
        <p class="fqa_ansstyle">Membership of any family medicine organization is not mandatory for application.  
        </p> 
        <p class="fqa_qstyle"><b>Q.12</b>What are the maximum number of attempts for the part I and part II exams?  </p>
        <p class="fqa_ansstyle">There is no limit for the part I exam.  On clearing part I exam, a maximum of three attempts are allowed for the part II OSCE exam and failing to which a candidate will have to re-sit part I exam.
        </p> 
        <p class="fqa_qstyle"><b>Q.13</b>I cleared part I exam three years back.  Can I apply for the upcoming OSCE?   </p>
        <p class="fqa_ansstyle">Both parts of the exam have to be cleared within 6 years of the receipt of the successful part I application.
        </p> 
        <p class="fqa_qstyle"><b>Q.14</b>I cleared the last AKT exam.  Can I appear in the upcoming OSCE exam?  </p>
        <p class="fqa_ansstyle">Success in part I exam makes a candidate eligible to apply for the next subsequent part II OSCE exam.
        </p>
         <p class="fqa_qstyle"><b>Q.15</b>I want my MRCGP [INT] degree to be verified.  How can I do that?   </p>
        <p class="fqa_ansstyle">RCGP has some understanding with British Council (BC) for verification of their degrees.  Please contact the local BC office or RCGP directly.    
        </p>
         <p class="fqa_qstyle"><b>Q.16</b>I am interested in working abroad.  How the examination can help me? </p>
        <p class="fqa_ansstyle">The qualification provides working opportunities in UAE, KSA and other Middle East countries.  The qualification is also recognized by the Royal Australian College of General Practitioners (RACGP).   Please see the “Why MRCGP [INT]” document on the website for complete details. 
        </p>
       
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
          </div>
    
         
  
 
 </div>
          
 

        
</div>
        
       </div>
      </div>
   </div>   
      
    </aside>    
    <aside class="content-right">
        <div class="content-royal-college res_css">
            <img src="images/logo-royal-college.gif" />
            <p>MEMBERSHIP OF THE ROYAL COLLEGES</p>
            <p>OF OBSTETRICS AND GYNAECOLOGISTS OF THE UNITED KINGDOM</p>
        </div>
    <!--    <div class="content-date-venue res_css">
            <h1>DATES</h1>
            <p style="font-weight:bold;font-size:24px;">Dates: 21<small class="th">st</small> and 22<small class="th">nd</small> <br>July 2015</p>
            <p style="border: 1px dotted #c4c4c4;margin:10px 0px;width:95%;"></p>
            <h1>VENUE</h1>
            <p>TBC, New Delhi</p>
        </div>-->
  <!--       <div class="content-date-venue res_css">
            <h1>COURSE FEES</h1>
            <p style="color:#00A652;;font-weight:bold;font-size:17px;">
 The course fee is INR <span style="color:rgb(226, 57, 57)">20,000</span> AED for both days and <span style="color:rgb(226, 57, 57)">10,000 </span>for one day.
</p>
        </div>-->
        
        <div>
      <div class="content-date-venue res_css">
            <h1 style="color:black">Who is this course for?</h1>
            <p style="color:black">
The course promises to be first of its kind in India and provides a golden opportunity to gain the prestigious qualification for General Practitioners and Residents in Family Medicine in Indiapar.</p>
        </div>
      
        </div>
        
        
       
          <?php include 'enquiryform.php'; ?>
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>