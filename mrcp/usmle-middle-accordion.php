<?php  $pageName= substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); ?>

<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Strengths</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>Intensive two day course helping prepare candidates for the MRCP part one exam.</li>
          <li><span class="sub-arrow"></span>Offers interactive, non-confrontational teaching using hundreds of sample questions in the format of the real exam.</li>
          <li><span class="sub-arrow"></span>We concentrate upon core topics as well as subjects that candidates often struggle with.</li>
          <li><span class="sub-arrow"></span>Particular emphasis is paid to exam technique and college favourite questions.</li> 
          <li><span class="sub-arrow"></span>The course is suited to both those just starting their preparation as well as experienced MRCP part 1 candidates who have sat the exam previously.</li>
          <li><span class="sub-arrow"></span>Well established course in London and Manchester and high pass rates (over 80%).</li>
          <li><span class="sub-arrow"></span>Teaching faculty direct from UK with world class credentials and several years’ experience.</li>
          <li><span class="sub-arrow"></span>Candidates from all over the world attended this course and gained success on first attempt.</li>
          <li><span class="sub-arrow"></span>Constantly updated teaching material with Multiple Choice Question based format to mirror the real exam</li>
          <li><span class="sub-arrow"></span>Non-confrontational teaching style with Use of repetition of difficult concepts to aid learning</li>
          <li><span class="sub-arrow"></span>Clear, well designed visual teaching aids (see figure for example slides).</li>
          <li><span class="sub-arrow"></span>No other course offers mock exam and marking similar to the real exam.</li>          
        </ul>
      </div>
    </div>
  </div></div>
  <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Highlight</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>According to New Format,Curriculum and Syllabus.</li>
          <li><span class="sub-arrow"></span>The Faculty is UK Based and has been Involved in Delivering this course both Nationally and Internationally.</li>  
        </ul>
      </div>
    </div>
  </div></div>

<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Facilitator</div>
  <div class="course-new-section">
   <div class="coures-list-box">
      <div class="coures-list-box-content">



  <div class="mrcp_boxess mrcp_boxess_cntr" style="margin-top:0;">
        <div class="mrcp_dr_img dr_imgbordr"><img src="images/Dr_Davenport.png"></div>
        <div class="mrcp_inr_boxes" style="float:left;">
            <h1 class="mrcp_dr_ttl">Dr Bex Davenport</h1>
<!--            <p class="mrcp_dr_sbtl">MRCS, MRCOG </p>-->
            <p class="mrcp_dr_sbtl">Consultant at the Central Manchester University Hospital<br> (Manchester Royal Infirmary)</p>          
            <p style="font-size:14px;">Dr Davenport graduated from Manchester Medical School. She completed her core medical training in
Central Manchester and gained her MRCP qualification in 2012.</p>
            <p style="font-size:14px;">She recently completed General Internal
Medicine and Geriatric Medicine Specialist Training in the North West Deanery and has taken a Consultant
post at the Central Manchester University Hospital (Manchester Royal Infirmary).</p>
            <p style="font-size:14px;">She describes herself as
a proud Geriatrician, Physician and Educator.</p>
        </div>

</div>
      </div>     
  <div class="mrcp_boxess mrcp_boxess_cntr" style="margin-top:0;">
        <div class="mrcp_dr_img dr_imgbordr"><img src="images/Thomas_Knight_Bio.png"></div>
        <div class="mrcp_inr_boxes" style="float:left;">
            <h1 class="mrcp_dr_ttl">Dr. Thomas Knight MBBS MRCP MRes</h1>
<p style="font-size:14px;">Dr. Knight is UK graduate working in the North West of England. He has over six years of postgraduate experience working in general medicine and critical care. Dr. Knight currently holds national training numbers in General Internal Medicine and Acute Internal Medicine. Dr. Knight successfully completed the MRCP diploma in 2015.
</p>
        </div>

</div>
       
       
        <div class="mrcp_boxess mrcp_boxess_cntr" style="margin-top:0;">
        <div class="mrcp_dr_img dr_imgbordr"><img src="images/N_Wreglesworth.jpeg"></div>
        <div class="mrcp_inr_boxes" style="float:left;">
            <h1 class="mrcp_dr_ttl">Dr. Nick Wreglesworth</h1>
<p style="font-size:14px;">Qualified from Newcastle University in 2011 and have a Post-Graduate Master in research in genomic's.
</p>
        </div>

</div>
      </div>     


 <!--      <div class="coures-list-box-content">
<div class="mrcp_boxess" style="margin-top:0;">
        <div class="mrcp_dr_img"><img src='images/dr_james.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr James Galloway</h1>
          <p class="mrcp_dr_sbtl">MBCHB, CHM, MSC, MRCP(UK), PH</p>
            <p class="mrcp_dr_sbtl">Course Director</p>
            <p style="font-size:14px;">Dr Galloway is a lecturer in rheumatology at King’s College London, and is also an honorary consultant rheumatologist at King’s College Hospital.He is currently lecturer at King’s medical school in London as well as delivers lectures to a broad range of post-graduate courses. He established the independent Manchester MRCP part one course with Dr Jones in 2006. He is an educational consultant course director and programme lead for MRCP part 1 and PACES.</p>
        </div>
</div></div>

 <div class="mrcp_boxess">
        <div class="mrcp_dr_img"><img src='images/dr_mathew.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Matthew Jones</h1>
            <p class="mrcp_dr_sbtl">MBChB, MD, MRCP(UK), MRCP(Neurology)</p>
            <p class="mrcp_dr_sbtl">Course Co-Director</p>
            <p style="font-size:14px;">Dr Jones is a consultant neurologist and clinical teaching fellow at Greater Manchester Neurosciences Centre, Salford Royal Foundation Trust, UK. His teaching interests lie in both undergraduate and postgraduate education. He is a lecturer at Manchester Medical School. He has been co-directing the Manchester MRCP course since 2006. He is Director of MRCP PACES course in Manchester as well. Dr Jones will be delivering the neurology presentation online for this course.</p>
        </div>

</div>   <div class="mrcp_boxess">
        <div class="mrcp_dr_img"><img src='images/dr_ashok.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr. Ashok Kumar Kapoor</h1>
            <p class="mrcp_dr_sbtl">M.B.B.S. MD ( lnternal Medicine). DM ( Cardiology),FICA(USA),F.Card(Germany)</p>
            <p class="mrcp_dr_sbtl">Specialist Cardiologist</p>
            <p class="mrcp_dr_sbtl">Consultant Cardiologist. Mediclinic Group of Hospitals, Dubai</p>
            <p class="mrcp_dr_sbtl">Consultant Cardiologist. International modern Hospital, Dubai</p>
            <p class="mrcp_dr_sbtl">Chairman-Getwell Medical Center- Bur Dubai, Dubai UAE</p>
            <p style="font-size:14px;">Dr Kapoor is a well-established consultant cardiologist in the UAE for more than 20 years. He is the founder of Arlington British Medical Academy – UK.</p>
        </div>

</div>
<div class="mrcp_boxess">
        <div class="mrcp_dr_img"><img src='images/dr_sumer.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr. Sumer K. Sethi</h1>
            <p class="mrcp_dr_sbtl">MBBS, MD (Radiology)</p>
            <p class="mrcp_dr_sbtl">Course Facilitator</p>
            <p class="mrcp_dr_sbtl">Director – DAMS (India)</p>
            <p class="mrcp_dr_sbtl">CEO – TELERAD providers – India</p>
            <p style="font-size:14px;">Dr Sethi is a senior Radiologist based in New Delhi. He has been very active in PG Medical Education for the last 16 years.</p>
        </div>

</div>
        <div class="mrcp_boxess" style="border-bottom:none;">
            <div class="mrcp_dr_img"><img src='images/dr_zubir.jpg'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Zubair Ahmad</h1>
            <p class="mrcp_dr_sbtl">MRCGP, DRCOG, DFFP, FRCGP, PCME</p>
            <p class="mrcp_dr_sbtl">Course Facilitator</p>
            <p style="font-size:14px;">Dr Zubair Ahmad, a GP Trainer from North West Deanery of England and a Primary Care Consultant based in Manchester UK for the last 16 years.  He has been heavily involved in both post graduate and undergraduate medical teaching since 2007. He has been facilitating these courses for many years. </p>
        </div>

</div>-->
    </div>
    </div>
    <div class="pg-medical-main" style="display:block;border: 0px solid #F0AC49;">
	<img src="images/mrcp_21sep.jpg" att="mrcp_part1" style="width: 100%;">
    </div>
  </div>

  </div>
</div>