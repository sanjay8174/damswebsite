<!DOCTYPE html>
<?php
$course_id = '5';
?>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>USMLE EDGE Coaching Institute, USMLE Edge</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false);">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>Online Test Series</h3>
        <p>It is getting progressively difficult each year to get into the US medical system, yet we at DAMS believe that it is the lack of information that is the biggest drawback that an IMG experiences to get to medical licensure in the United States. We at DAMS now take up that responsibility to provide the right information and guide to do the right thing at the right time to achieve success in United States Medical Licensing Examination.<br>
          <br>
          We propose our own unique course which is integrated <strong>Foundation PG course with USMLE EDGE. How many of us are sure in the prefinal-final year stage about USMLE? How many times have you asked this question-we need a course which gives us both? Yes DAMS now offers the only course which offers online simulated exams for both steps and counselling sessions for USMLE integrated with its very popular PG foundation course.</strong><br>
          <br>
          <strong>Another course, we are now offering is the USMLE Simulated Test Series with counselling sessions for people who want to appear for only USMLE. Very soon we will be launching our own class room programme for the USMLE as well.</strong><br>
          <br>
          The USMLE assesses a physician's ability to apply knowledge, concepts, and principles, and to demonstrate fundamental patient-centered skills, that are important in health and disease and that constitute the basis of safe and effective patient care. Each of the three Steps of the USMLE complements the others; no Step can stand alone in the assessment of readiness for medical licensure. </p>
        <p>The United States Medical Licensing Examination is a three-step examination for medical licensure in the United States and is sponsored by the Federation of State Medical Boards (FSMB) and the National Board of Medical Examiners (NBME).</p>
      </div>
      <div class="pg-medical-main">
        <div class="pg-heading"><span></span>Usmle Edge Courses</div>
        <div class="course-new-section">
          <div class="coures-list-box">
            <h5> Usmle Edge Courses</h5>
            <div class="coures-list-box-content">
              <ul class="course-new-list">
                <li><a href="usml-step1.php" title="USMLE STEP-1 (TS)"><span class="sub-arrow"></span>USMLE STEP-1 (TS)</a></li>
                <li><a href="usml-step2.php" title="USMLE STEP-2 (TS)"><span class="sub-arrow"></span>USMLE STEP-2 (TS)</a></li>
                <li><a href="usmle-egde-combo.php" title="USMLE Combo"><span class="sub-arrow"></span>USMLE Combo</a></li>
                <li><a href="../usmle-egde-full-package.php" title="USMLE Full Package"><span class="sub-arrow"></span>USMLE Full Package</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </aside>
    <aside class="content-right">
      <div id="store-wrapper">
        <div class="dams-store-link"><span></span>DAMS USMLE Edge</div>
        <div class="dams-store-content">
          <div class="inner-store">
            <ul>
              <li class="border_none"><a href="usml-step1.php" title="USMLE STEP-1 (TS)">USMLE STEP-1 (TS)</a></li>
              <li><a href="usml-step2.php" title="USMLE STEP-2 (TS)">USMLE STEP-2 (TS)</a></li>
              <li><a href="#" title="USMLE Combo">USMLE Combo</a></li>
              <li><a href="#" title="USMLE Full Package">USMLE Full Package</a></li>
            </ul>
          </div>
        </div>
      </div>
      <?php
    include 'openconnection.php';
    $count = 0;
    $i = 0;
    $sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
    while ($row = mysql_fetch_array($sql)) {
        $newsDetail[$count] = urldecode($row['HEADING']);
        $count++;
    }
    ?>
      <div class="news-update-box">
        <div class="n-heading"><span></span> News &amp; Updates</div>
        <div class="news-content-box">
          <?php
                        $j = 0;
                        $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) {
                        ?>
          <ul id="ul<?php echo $i; ?>" <?php if ($i == '0') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
          <li> <span></span>
            <p><?php echo $newsDetail[$j++]; ?></p>
          </li>
          <?php if ($newsDetail[$j] != '') {
 ?>
          <li class="orange"> <span></span>
            <p><?php echo $newsDetail[$j++]; ?></p>
          </li>
          <?php } ?>
          <?php if ($newsDetail[$j] != '') { ?>
          <li> <span></span>
            <p><?php echo $newsDetail[$j++]; ?></p>
          </li>
          <?php } ?>
          </ul>
          <?php
                        }
?>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="usmle-news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
            <div class="slider-mini-dot">
              <ul>
                <?php $i = 0;
                                    for ($i = 0; $i < ceil($count / 3); $i++) { ?>
                <li id="u<?php echo $i; ?>" <?php if ($i == '0') {
 ?> class="current" <?php } ?> onClick="news('<?php echo $i; ?>',<?php echo ceil($count / 3); ?>);"></li>
                <?php } ?>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
      <div class="news-update-box">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block" >
            <iframe width="314" height="236" src="//www.youtube.com/embed/z_xgJNXaWuQ" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd1" class="display_none" >
            <iframe width="314" height="236" src="//www.youtube.com/embed/VRJ89h2DkS0" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd2" class="display_none" >
            <iframe width="314" height="236" src="//www.youtube.com/embed/VRJ89h2DkS0" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="#" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
    </aside>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript">
 var m=0;
 var l=0;
 var n=0;
 var videointerval ='';
 var newsinterval = '';
 $(document).ready(function(){
   var count1=$("#recent li").length;
   $("#prev").click(function(){
	 if(m>0)
	 {
	   var j=m+3;
       $("#li"+j).hide('fast','linear');
	   m--;
	   $("#li"+m).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   $(".slide-arrow > a.left").css('background','url(images/right.png)');
	   if(m==0)
	   {
		 $(".slide-arrow > a.right").css('background','url(images/left.png)');   
	   }
	 }
   });
   $("#next").click(function(){	 
	 if(m<count1-4)
	 {
       $("#li"+m).hide('fast','linear');
	   var j=m+4;
	   $("#li"+j).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   m++;
	   if(m==(count1-4))
	   {
		 $(".slide-arrow > a.left").css('background','url(images/right-inactive.png)');
	   }
     }	 
   });
   
  //     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

   window.onload = function()
   {	  
	    var count2=$(".videos-content-box > img").length;
	    var count3=$(".news-content-box > ul").length;
        function setvideo()
		{
		   l=l%count2;		
		   $("#vd"+l).fadeOut('slow','linear');
		   $("#v"+l).removeClass("current");
	       var j=(l+1)%count2;			
		   $("#vd"+j).fadeIn('slow','linear');
		   $("#v"+j).addClass("current");        	
	       l++;	
        }
	    //function setnews()
//		{
//		   n=n%count3;		
//		   $("#ul"+n).slideUp(100,'swing');
//		   $("#u"+n).removeClass("current");
//	       var j=(n+1)%count3;
//		   $("#ul"+j).slideDown(4000,'linear').animate({zindex:'2'}, 100);
//		   $("#u"+j).addClass("current");        	
//	       n++;	
//        }
     videointerval = setInterval(setvideo, 5000);
	 //newsinterval = setInterval(setnews, 9000);
   }
 });
 function news(val)
 {
   //setTimeout('newsinterval', 9000);
   if(val=='0')
   {	   
	  $("#ul1").hide();
	  $("#u1").removeClass("current"); 
	  $("#ul2").hide();
	  $("#u2").removeClass("current");
	  $("#ul0").slideDown(1000,'linear');
	  $("#u0").addClass("current");   
   }
   if(val=='1')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul2").hide();
	 $("#u2").removeClass("current");
	 $("#ul1").slideDown(1000,'linear');
	 $("#u1").addClass("current"); 
   }
   if(val=='2')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul1").hide();
	 $("#u1").removeClass("current");
	 $("#ul2").slideDown(1000,'linear');
	 $("#u2").addClass("current");
   }
  // n=val;
//   newsinterval = setInterval(setnews, 9000);  
 }
 
 function video(val)
 {
   setTimeout('videointerval', 5000);
   if(val=='0')
   {	   
      $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd0").fadeIn('fast','linear');
	  $("#v0").addClass("current");
   }
   if(val=='1')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd1").fadeIn('fast','linear');
	  $("#v1").addClass("current");
   }
   if(val=='2')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd2").fadeIn('fast','linear');
	  $("#v2").addClass("current");
   }
   if(val=='3')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").fadeIn('fast','linear');
	  $("#v3").addClass("current");
   }
   l=val;
   videointerval = setInterval(setnews, 5000);  
 }
 
 function sliddes(val)
{
   var sp=$('.h2toggle > span').size();
   for(var d=1;d<=sp;d++)
   {   
       if(val!=d)
	   { 
    	 $('#s'+d).removeClass('minus-ico');
         $('#s'+d).addClass('plus-ico');
		 $('#di'+d).slideUp(400);
	   }      
   }
   
   $('#di'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#s'+val).removeClass('plus-ico');
            $('#s'+val).addClass('minus-ico');
       }
	   else
	   {
			$('#s'+val).removeClass('minus-ico');
            $('#s'+val).addClass('plus-ico');
       }
   });
}
</script> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<script src="navigation/TweenMax.min.js" type="text/javascript"></script> 
<!-- Others Packages -->
</body>
</html>