
<footer class="footernew">
<div class="qustion-chat"> <img src="images/question-chat.png" title="Question" alt="Question" /> </div>
<div class="main_wrappr info foot1">
<div class="footer-left">
 <div class="info_prt">
    <h4 class="ft_ttl thin_font first1">ABOUT US</h4>
    <ul>
        <li><a href="https://damsdelhi.com/dams.php?a=1">About DAMS</a></li>
        <li><a href="https://damsdelhi.com/dams_director.php?a=2">Directors Message</a></li>
        <li><a href="https://damsdelhi.com/about_director.php?a=3">Our Leadership</a></li>
        <li><a href="https://damsdelhi.com/mission_vision.php?a=4">Mission & Vision</a></li>
        <li><a href="https://damsdelhi.com/dams_faculty.php?a=5">Our Faculty</a></li>
        <li><a href="https://damsdelhi.com/about_award.php?a=14">Awards & Achievements</a></li>
        <li><a href="https://damsdelhi.com/about_news.php?a=7">News Media</a></li>
        <li><a href="disclaimer.php">Disclaimer</a></li>
         <li><a href="studentInfo.php">Student Info</a></li>
    </ul>
  </div>   
    <div class="info_prt">
    <h4 class="ft_ttl thin_font first1">DAMS COURSES</h4>
    <ul>
        <li><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1">MD/MS Entrance</a></li>
        <li><a href="https://mci.damsdelhi.com/index.php?c=2&n=5">MCI Screening</a></li>
        <li><a href="https://mds.damsdelhi.com/index.php?c=3&n=9">MDS Quest/NBDE</a></li>
        <li><a href="https://usmle.damsdelhi.com/index.php?c=4&n=">USMLE Edge</a></li>
        <li><a href="https://mrcp.damsdelhi.com/index.php?c=5&n=">MRCP</a></li>
        <li><a href="https://mrcog.damsdelhi.com/index.php?c=6&n=">MRCOG</a></li>
        <li><a href="https://plab.damsdelhi.com/index.php?c=7&n=">PLAB</a></li>
        <li><a href="https://mrcgp.damsdelhi.com/index.php?c=8&n=">MRCGP</a></li>
    </ul>
  </div>   
    <div class="info_prt">
    <h4 class="ft_ttl thin_font first1">DAMS STORE</h4>
    <ul>
        <li><a href="https://damspublications.com/" target="_blank" >MD/MS Entrance</a></li>
        <li><a href="https://damspublications.com/" target="_blank" >MCI Screening</a></li>
        <li><a href="https://damspublications.com/" target="_blank" >MDS Quest</a></li>
        <li><a href="https://damspublications.com/" target="_blank" >USMLE Edge</a></li>
        <li><a href="https://damspublications.com/">eBook Learning</a></li>
    </ul>
  </div>    
    <div class="info_prt">
    <h4 class="ft_ttl thin_font first1">FIND A CENTRE</h4>
    <ul>
        <li><a href="https://damsdelhi.com/usmle_edge/find-center.php?c=1">Face to Face Centres</a></li>
        <li><a href="https://damsdelhi.com/usmle_edge/find-center.php?c=2">Satellit Centres</a></li>
        <li><a href="https://damsdelhi.com/usmle_edge/find-center.php?c=3">Test Centres</a></li>
    </ul>
  </div>   
 
    
    
    
<!--<div class="footer-left-top"> <a href="privacy_policy.php" title="Privacy Policy ">Privacy Policy</a> <span class="v-line">|</span> <a href="disclaimer.php" title="Disclaimer">Disclaimer</a> <span class="v-line">|</span> <a href="terms_n_condition.php" title="Terms &amp; Conditions">Terms &amp; Conditions</a> <span class="v-line">|</span> <a href="contact.php" title="Contact">Contact</a> <span class="v-line">|</span> <a href="sitemap.php" title="Sitemap">Sitemap</a> </div>
<div class="footer-left-bottom">© Delhi Academy of Medical Sciences Pvt. Ltd. All rights reserved.<br />
<span>Website Design by <a href="https://www.gingerwebs.com">Ginger webs</a> | <a href="https://www.thinkexam.com">Online Exam Software</a> Powered by Think Exam</span></div>-->
</div>
<div class="footer-right">
<div>
    <h4 class="ft_ttl thin_font first1">DOWNLOAD APP</h4>
<!--<div class="follow google_plus"><a href="https://play.google.com/store/apps/details?id=com.emedicoz.app&hl=en_IN " target="_blank" title="Download DAMS Android App">Download DAMS Iphone App</a></div>
<div class="follow apple_store"><a href="https://apps.apple.com/in/app/emedicoz/id1263112084  " target="_blank" title="Download DAMS Iphone App">Download DAMS Iphone App</a></div>-->
<!--<div class="follow e-book_store"><a href="https://play.google.com/store/apps/details?id=com.spayee.dams.reader" target="_blank" title="Download DAMS Iphone App">Download DAMS Iphone App</a></div>-->
<!--<div class="follow CLOUD_Android"><a href="https://play.google.com/store/apps/details?id=com.dams.cloud" target="_blank" title="Download DAMS Iphone App">Download DAMS Iphone App</a></div>-->
<!--<div class="follow CLOUD_IOS"><a href="https://itunes.apple.com/in/app/dams-cloud/id974913714?mt=8" target="_blank" title="Download DAMS Iphone App">Download DAMS Iphone App</a></div>-->
<!--<div class="follow e-book_ios"><a href="https://itunes.apple.com/in/app/dams-ebooks-pg-medical-exam/id1221802898?mt=8" target="_blank" title="Download DAMS Iphone App">Download DAMS Iphone App</a></div>-->
<div class="follow medicos_Android"><a href="https://play.google.com/store/apps/details?id=com.emedicoz.app&hl=en_IN " target="_blank" title="Download DAMS Iphone App">Download DAMS Iphone App</a></div>
<div class="follow medicos_IOS"><a href="https://apps.apple.com/in/app/emedicoz/id1263112084 " target="_blank" title="Download DAMS Iphone App">Download DAMS Iphone App</a></div>
</div>

    <div class="followuson">
        <h4 class="ft_ttl">FOLLOW US</h4>
<ul>
   <li><a href="https://www.facebook.com/damsdelhiho/" target="_blank" class="fb"></a> </li>
    <li><a href="https://twitter.com/damsdelhi" target="_blank" class="twt"></a></li>
    <li><a href="https://www.youtube.com/user/damsdelhi" target="_blank" class="yt"></a></li>
    <li><a href="javascript:void(0);" class="linkedin"></a></li>
    <li><a href="https://plus.google.com/u/0/110912384815871611420/about" target="_blank" class="gpls"></a></li>
  </ul> 

    </div>
</div>
</div>
<div class="footer-bottom"> 
    <div class="main_wrappr footbot">
    <div class="bottom_left">
    <a href="privacy_policy.php" title="Privacy Policy ">Privacy Policy</a> <span class="v-line1">|</span>
     <a href="csr_policy.php" title="CSR Policy ">CSR Policy</a> <span class="v-line1">|</span>
    <a href="disclaimer.php" title="Disclaimer">Disclaimer</a> <span class="v-line1">|</span>
    <a href="terms_n_condition.php" title="Terms &amp; Conditions">Terms &amp; Conditions</a> <span class="v-line1">|</span> 
    <a href="contact.php" title="Contact">Contact</a> <span class="v-line1">|</span> 
    <a href="sitemap.php" title="Sitemap">Sitemap</a> 
    </div>
      <div class="bottom_right">© Delhi Academy of Medical Sciences Pvt. Ltd., DAMS SKY Pvt. Ltd., DAMS DENTAL Pvt. Ltd. All rights reserved.</div>
</div>
    </div>
</footer>
<!--<div class="fixsocialapp">
 <a href="https://play.google.com/store/apps/details?id=com.spayee.dams.reader" target="_blank" class="appicon icon3">
<i class="spritemob ebkandroid"></i>
<div class="iconname">Ebook Android</div>
</a>
<a href="https://itunes.apple.com/in/app/dams-cloud/id974913714?mt=8" target="_blank" class="appicon icon2">
<i class="spritemob android"></i>
<div class="iconname">CLOUD IOS</div>
</a>
<a href="https://play.google.com/store/apps/details?id=com.dams&hl=en" class="appicon icon1" target="_blank">
<i class="spritemob ios"></i>
<div class="iconname">CLOUD Android</div>
</a>
</div>-->
<script>(function(d,e,j,h,f,c,b){d.GoogleAnalyticsObject=f;d[f]=d[f]||function(){(d[f].q=d[f].q||[]).push(arguments)},d[f].l=1*new Date();c=e.createElement(j),b=e.getElementsByTagName(j)[0];c.async=1;c.src=h;b.parentNode.insertBefore(c,b)})(window,document,"script","//www.google-analytics.com/analytics.js","ga");ga("create","UA-37551965-1","damsdelhi.com");ga("send","pageview");</script>
<div style="position:absolute; bottom:0;">
<script type="text/javascript">
var google_tag_params = {
edu_pid: 'REPLACE_WITH_VALUE',
edu_plocid: 'REPLACE_WITH_VALUE',
edu_pagetype: 'REPLACE_WITH_VALUE',
edu_totalvalue: 'REPLACE_WITH_VALUE',
};
</script>

<script type="text/javascript">
var google_conversion_id = 1049334284;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1049334284/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</div>