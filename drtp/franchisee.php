<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php error_reporting(0); ?>
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<section class="inner-banner">
<div class="wrapper">
<article class="franchise-banner">
<aside class="banner-left">
<h3>Join hands with No. 1 PGME classes &amp;<br>Be a part of our success story <span>High return on investment &amp; best in the <br>business franchisee support.</span></h3>
</aside></article></div></section>
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
<ul>
<li class="bg_none"><a title="Franchisee Opportunity" class="active-link">Franchisee Opportunity</a></li></ul>
</div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading paddin-zero">
<h4>Franchisee Opportunity</h4>
<article class="showme-main">
<div class="idams-content">
<div class="franchisee-box"> <span>DAMS is all set to augment PG Medical domain via Franchising -</span>
<ul class="franchisee-list">
<li><span>&nbsp;</span>High return on investment.</li>
<li><span>&nbsp;</span>Proven track record with successfully running 30+ Face to Face Classrooms in India.</li>
<li><span>&nbsp;</span>Reputed Brand in this domain since last 15 years.</li>
<li><span>&nbsp;</span>1 Lac+ Doctors have already benefitted from our Execellent Academic Program.</li>
</ul>
<p>Delhi Academy of Medical Sciences (DAMS) is the India's leading Post Graduate Medical Entrance Coaching Institute, preparing students for All India PG Entrance, AIIMS PG Entrance, and State PG Entrance.  DAMS is currently appointing franchisees across India.</p><p>This is great opportunity to join hands with Delhi Academy of Medical Sciences to fulfill your business dreams.  We are also looking for entrepreneurs to run our Test Centres also in various cities across the country.</p></div>
<div class="idams-box"> <span>THE NEW GOLD STANDARD DISTANT LEARNING PROGRAMME</span>
<ul class="franchisee-list">
<li><span>&nbsp;</span>Association with the reputed DAMS brand of a respected business house.</li>
<li><span>&nbsp;</span>DAMS is the name that is well established in the PG Medical Entrance education world.</li>
<li><span>&nbsp;</span>Access to a proven business model.</li>
<li><span>&nbsp;</span>All necessary assistance and training to set up and start off.</li>
<li><span>&nbsp;</span>Effective advertising, promotion and publicity support.</li>
</ul></div>
<div class="submit-enquiry"><a href="javascript:void(0)" id="franchisee" title="Apply For Franchisee">Apply For Franchisee</a></div>
</div></article></div></aside>
<aside class="gallery-right">
<div class="franchise-addertisment"> <span>&nbsp;</span> </div>
<div class="franchise-fact">
<h4>FRANCHISE FACTS</h4>
<ul class="franchise-list">
<li class="border_none">
<label>Investment</label>
<span>10 to 20 Lacs</span></li>
<li>
<label>Area</label>
<span>1200-1500 sq. fts.</span></li>
<li>
<label>Expansion</label>
<span>Pan India</span></li>
<li>
<label>Industry</label>
<span>PG Medical</span></li>
<li class="last">Entrance Coaching/VSAT</li>
</ul></div></aside></section></div></div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
