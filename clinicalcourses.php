<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];

?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>Clinical Course in DENTISTRY</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg no_bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php';
$pathCMS = constant::$pathCMS;
?>
<!-- Banner Start Here -->
<section class="inner-banner clinical">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
          <h2 >CLINICAL COURSE IN DENTISTRY </h2>
          <h3><span>DAMS now offers CLINICAL COURSE IN DENTISTRY </span></h3>
          <h3><span>Starting From 23-May-2016 </span></h3>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<!--<div style="overflow:hidden;">-->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>CLINICAL COURSE IN DENTISTRY <span class="book-ur-seat-btn clinicalc"><a title="Book Your Seat " href="http://registration.damsdelhi.com" target="_blank"> <span>&nbsp;</span> Buy Now</a></span></h3>
        <p style="font-size:22px;padding:5px 0px;">Course Details Format</p><br>
        <p>The aim of this course is to hone the clinical skills and knowledge of graduates in the field of Conservative Dentistry and Endodontics and to familiarize them with the latest techniques and armamentaria and help them emerge a better clinician.</p>     
        <p style="margin-top: 15px;"><strong>Starting From 23-May-2016</strong></p>
            <div class="coures-list-box">
      <div class="coures-list-box-content">
        <ul class="course-new-list">
            <li><span class="sub-arrow"></span><span style="width:130px;display: inline-block;">Course Duration : </span><strong>&nbsp;2 Days</strong></li>
          <li><span class="sub-arrow"></span><span style="width:130px;display: inline-block;">Fees: </span><strong>&nbsp;Rs:5000<strong></li>
          <li><span class="sub-arrow"></span><strong><span style="width:98%;display: inline-block;"> 20% Discount to all the current or previous Damsonians</span></li>      
        </ul>
      </div>
    </div>      
</div>
       
<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Highlights</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
          <p style="font-size: 15px; padding: 10px 0px; color: rgb(0, 0, 0);"><strong>MODULE I: (BASIC ENDODONTICS)</strong></p>
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>Diagnosis and treatment planning</li>
          <li><span class="sub-arrow"></span>Access cavity preparations
              <ul class="course-new-list">
                  <li>a. Intact teeth</li>
                  <li>b. Decayed teeth</li>
              </ul>
          </li>
          <li><span class="sub-arrow"></span>Techniques for cleaning and shaping the root canals
              <ul class="course-new-list">
                  <li>a. Hand instrumentation</li>
                  <li>b. Rotary instrumentation</li>
              </ul>
          </li>
         <li><span class="sub-arrow"></span>Obturation methods and devices</li>
        </ul>
      </div>
    </div>
    <div class="coures-list-box">
      <div class="coures-list-box-content">
          <p style="font-size: 15px; padding: 10px 0px; color: rgb(0, 0, 0);"><strong>MODULE II: (ADVANCED ENDODONTICS)</strong></p>
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>Restorative needs of endodontically treated teeth
              <ul class="course-new-list">
                  <li>a. Post selection and placement</li>
                  <li>b. Tooth preparation</li>
                  <li>c. Core materials and fabrication</li>
              </ul>
          </li>
         <li><span class="sub-arrow"></span>Surgical aspects in Endodontics</li>
         <li><span class="sub-arrow"></span>Non surgical retreatment: principles and techniques</li>
        </ul>
      </div>
    </div>
    <div class="coures-list-box">
      <div class="coures-list-box-content">
          <p style="font-size: 15px; padding: 10px 0px; color: rgb(0, 0, 0);"><strong>MODULE III (AESTHETIC DENTISTRY)</strong></p>
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>Tooth preparation for composite and ceramic veneers</li>
          <li><span class="sub-arrow"></span>Aesthetic correction of teeth with direct composites
              <ul class="course-new-list">
                  <li>a. Fracture buildup</li>
                  <li>b. Diastema closure</li>
                  <li>c. Reattachment of fractured teeth.</li>
              </ul>
          </li>
        </ul>
      </div>
    </div>
  </div></div>

<div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Facilitator</div>
  <div class="course-new-section">
   <div class="coures-list-box">
      <div class="coures-list-box-content">
<div class="mrcp_boxess" style="margin-top:0;border-bottom: 0px dotted #ccc;">
        <div class="mrcp_dr_img"><img src='images/Image_3.png'/></div>
        <div class="mrcp_inr_boxes">
            <h1 class="mrcp_dr_ttl">Dr Nitin Kararia</h1>                      
            <p class="mrcp_dr_sbtl" style="font-size:14px;">DR NITIN KARARIA graduated from Government Dental College, Jaipur in 2005. He secured 3rd rank in Rajasthan state Pre PG entrance examination and completed post graduation in Conservative Dentistry and Endodontics from government dental college, Jaipur in 2009. He is presently working as Reader at Rajasthan Dental College and Hospital, Jaipur and keenly involved in academic and clinical activities. He has many scientific publications to his credit out of which three are in Pubmed indexed journals. He is also sharing his experience in coaching and training graduates for the post graduate entrance examinations.</p></br>        		   
        </div>
</div></div>
    </div>
    </div>
  </div>
<!--        <div class="pg-medical-main" style="display:block;margin:25px 0 0 0;border: 0px solid #F0AC49;line-height: 0px;">
<img src="images/course_1.jpg" style="margin:0px; border:0px solid #ccc;" width="100%">
</div>-->


    </aside>    
    <aside class="content-right">
        <div class="content-date-venue res_css" style="margin-top:0px;">
            <h1>DATES</h1>
            <p style="font-weight:bold;font-size:18px;">Starting From 23-May-2016</p>
            <p style="border: 1px dotted #c4c4c4;margin:10px 0px;width:95%;"></p>
            <h1>VENUE</h1>
            <p>4 B, Grover Chamber,Pusa Road</p>
            <p>Near Karol Bagh Metro Station</p>
            <p>New Delhi, Delhi - 110005</p>
            <p>Ph. No. : 011-40094009</p>
            <p>(Enquiry Timing:- 10am To 8pm)</p>
        </div>
          <?php include 'enquiryform.php'; ?>
<!--      <div class="content-date-venue res_css" style=" width: 75%; margin-top: 15px; display: inline-block; padding: 6px;line-height: 0px;">
        <img src="images/BLSACLS-LOGO.png" width="100%" style="margin:0px; border:0px solid #ccc;">
        </div>    -->
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
