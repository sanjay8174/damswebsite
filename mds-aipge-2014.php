<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>PG Medical Entrance Coaching Institute, MDS Achievement</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <?php include'mds-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mds-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="dams-mds-quest.php" title="MD/MS Course">MDS Quest</a></li>
          <li><a title="Achievement" class="active-link">Achievement</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li class="light-blue border_none" id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
                <ol class="achievment-inner display_block" id="aol1">
                  <li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="mds-aipge-2014.php" title="AIPGME">AIPGE 2014</a></li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading paddin-zero">
            <h4>AIPGE 2014</h4>
            <section class="showme-main">
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="AIIMS May 2011">AIPGE 2014</a></li>
                <li><a href="#official" title="Interview">Interview</a></li>
              </ul>
              <div id="jquery" class="display_block">
                <article class="interview-photos-section">
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="images/students/CHANDAN-JAIN.jpg" alt="Dr. CHANDAN JAIN" title="Dr. CHANDAN JAIN" />
                        <p><span>Dr. CHANDAN JAIN</span> Rank: 2</p>
                      </div>
                      <div class="students-box"> <img src="images/students/ANUSAR-GUPTA.jpg" alt="Dr. ANUSAR GUPTA" title="Dr. ANUSAR GUPTA" />
                        <p><span>Dr. ANUSAR GUPTA</span> Rank: 37</p>
                      </div>
                      <div class="students-box"> <img src="images/students/PRATEEK-PUNYANI.jpg" alt="Dr. PRATEEK PUNYANI" title="Dr. PRATEEK PUNYANI" />
                        <p><span>Dr. PRATEEK PUNYANI</span> Rank: 50</p>
                      </div>
                      <div class="students-box"> <img src="images/students/KHUSHBOO-RATHORE.jpg" alt="Dr. KHUSHBOO RATHORE" title="Dr. KHUSHBOO RATHORE" />
                        <p><span>Dr. KHUSHBOO RATHORE</span> Rank: 98</p>
                      </div>
                    </li>
                  </ul>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                  <div class="schedule-mini-series"> <span class="mini-heading">AIPGE - Result 2014</span>
                    <div class="schedule-mini-top"> 
                     <span class="one-part">Rank</span> <span class="two-part">Student Name</span> <span class="three-part">&nbsp;</span> 
                    </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"><span class="one-parts">2</span> <span class="two-parts schedule-left-line">Dr. CHANDAN JAIN</span> </li>
                        <li><span class="one-parts">37</span> <span class="two-parts schedule-left-line">Dr. ANUSAR GUPTA</span> </li>
                        <li><span class="one-parts">50</span> <span class="two-parts schedule-left-line">Dr. PRATEEK PUNYANI</span> </li>
                        <li><span class="one-parts">98</span> <span class="two-parts schedule-left-line">Dr. KHUSHBOO RATHORE</span> </li>
                        <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line" style="color:#000;">And many more...</span> </li>
                      </ul>
                    </div>
                  </div>
                </article>
              </div>
              <div id="official" class="display_none">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/MKs-1DttP4c?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr Chandan Jain</span></p>
                            <p>Rank: 2nd (AIPG)</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/xtAUAo5NKR8?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr Prateek R Punyani</span></p>
                            <p>Rank: 50th (AIPG)</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/GUMZPGghqco?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr Khushboo Rathore</span></p>
                            <p>Rank: 98th (AIPG)</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/7X1ZGMKs0NQ?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Topper Counselling 10-2-2014</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </article>
              </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>