<div class="pg-medical-main tab-hide">
  <div class="pg-heading"><span></span> Face to Face Courses</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <h5 class="h2toggle" onclick="sliddes('1');"><span class="plus-ico" id="s1"></span>Classroom Course</h5>
      <div class="coures-list-box-content"  id="di1" style="display:none;">
        <ul class="course-new-list">
          <li><a href="dams-mds-quest-dental-regular-course.php" title="Regular Course"><span class="sub-arrow"></span>Regular Course</a></li>
          <li><a href="mds-test-discussion-course.php" title="Test &amp; Discussion"><span class="sub-arrow"></span>Test &amp; Discussion Course</a></li>
          <li><a href="mds-crash-course.php" title="Crash Course"><span class="sub-arrow"></span>Crash Course</a></li>
          <li><a href="dams-mds-test-series.php" title="Test Series"><span class="sub-arrow"></span>Test Series</a></li>
        </ul>
      </div>
    </div>
    <div class="coures-list-box">
      <h5 class="h2toggle" onclick="sliddes('2');"><span class="plus-ico" id="s2"></span>Distance Learning Course</h5>
      <div class="coures-list-box-content" id="di2" style="display:none;" >
        <ul class="course-new-list">
          <li><a href="mds-postal-course.php" title="Postal Course"><span class="sub-arrow"></span>Postal Course</a></li>
          <li><a href="mds-postal-test-series.php" title="Postal Test Series"><span class="sub-arrow"></span> Postal Test Series</a></li>
          <li><a href="dams-publication.php?c=3" title="Online Test Series"><span class="sub-arrow"></span>Online Test Series</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
