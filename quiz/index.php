<!DOCTYPE html>
<?php
$course_id = '1';
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, National Quiz</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js?v=1"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="./gallerySlider/owl.carousel.js"></script>
<style>
    /*popup bottom css start*/
.slidepop{position: relative;bottom: 0;right: 0;z-index: 1000;}/*.slidepop { position: fixed;  bottom: 0; right: 0;width: 200px;   height:300px;}*/.cont_div_slidepopup { border-color: #ccc;position: absolute; bottom: -450px; height: auto;right: 0px;outline:0; text-align: center;  color: #777777;z-index: 100;   webkit-transition-duration: 1.5s;  -moz-transition-duration: 1.5s;  -o-transition-duration: 1.5s;   transition-duration: 1.5s; -webkit-transition-property: -webkit-transform;  -moz-transition-property: -moz-transform; -o-transition-property: -o-transform;  transition-property: transform;  transition-property: all;border: 1px solid transparent;}.slidepop .cont_div_slidepopup i { position: absolute;right: 0px; top: 26px; float: left; width: 25px;cursor:pointer;outline: 0;  height: 25px;margin-bottom: 3px;}.slidepop .cont_div_slidepopup .iconname { float: left;outline: 0; padding: 0px;}.slidepop .cont_div_slidepopup .iconname img {outline: 0;}/*.slidepop .cont_div_slidepopup:hover {  right: -1px; background: #fff;border-color: #ccc;}*/.appicon_show {position:fixed;bottom: -4px; }.appicon_hide {position:fixed;bottom: -450px;}.onloadPopup .imgholder .home_pop_close{width:100%;}.onloadPopup .imgholder  .pop_image img{width:100%;}.slidepop .cont_div_slidepopup .iconname img{width:auto;}
/*popup bottom css end*/

/*popup homepage css start*/
.onloadPopup.home_page { display: block;}
.onloadPopup {  width: 100%; height: 100%; text-align: center; vertical-align: middle; position: fixed; left: 0px; top: 0px; z-index: 10000; background: transparent none repeat scroll 0% 0%;}
.onloadPopup .imgholder { border-radius: 0px;  border: 5px solid #ccc; background: #fff; box-sizing: border-box;  width: 750px; height: 95%;
display: inline-block;
position: absolute;
margin: auto;
top: 0px;
bottom: 0px;
left: 0px;
right: 0px;}
.onloadPopup .imgholder img { width: 100%;  height: 100%;}
.onloadPopup .imgholder a#onloadPopup-close {display: inline-block;width: 28px;height: 28px;position: absolute;right: 3px;top: 2px;  z-index: 1124;}
.onloadPopup .imgholder img { width: 100%; height: 100%;}
.blackPopup {background: #363636 none repeat scroll 0% 0%; z-index: 9999; width: 100%;  height: 100%;  opacity: 0.8;  top: 0px; left: 0px;  margin: 0px; position: fixed;}

    
</style>
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>
    
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
  <!--  <article class="national-banner fsf">-->
      <?php // include 'md-ms-big-nav.php'; ?>
     <!-- <aside class="banner-left">-->
        <!--<h3>National Quiz<span>India's only national quiz for young doctors & dentists</span></h3>-->
     <!-- </aside>-->
      <?php // include'md-ms-banner-btn.php'; ?>
<!--    </article>-->
    <div class="slider-data-outer" style="margin-top:20px;">
        <div class="slideshow-container">
<div class="mySlides fade">
  <img src="images/dams-bnr1.jpg" style="width:100%">
</div>
<div class="mySlides fade">
  <img src="images/dams-bnr2.jpg" style="width:100%">
</div>


</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
</div>
      </div>
</div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
      <aside class="content-left">
<div class="course-box">
    <!--<span style="margin-top: 0px;" class="book-ur-seat-btn"><a href="http://quiztest.damsdelhi.com/" title="Book Your Seat" target="_blank" style="font-size: 16px;"> <span>&nbsp;</span> Sign Up</a></span>-->
<ul class="idTabs responce-show">
<li><a href="#MDCourses">GREY MATTER – DAMS National Quiz</a></li>
<li><a href="#DNB">Quest National Dental Quiz</a></li>
</ul>

<div id="MDCourses">
<div class="satellite-content course-detail-content">
<div class="idams-box1">
<!--<h3>National Quiz</h3>-->
<p>As you must be aware that Delhi Academy of Medical Sciences is organizing a NATIONAL quiz project “GREY MATTER &ndash; DAMS National Quiz” being one of its kind medical quiz program. The project is intended to find and honor the best medical brain in the country. This project is being implemented nationwide in 4 zones all over India i.e. EAST; WEST; NORTH; SOUTH.</p>
          <p>This year, the Season 6, preliminary round of the national grand event is transformed into an online quiz, making it available to almost all the young doctors of the country. It is going to provide an opportunity to medical students to enrich themselves by our experiences and further help them to explore the hidden talent in them.</p>
          <p>Another fact that needs to be applauded is that at any level of quiz, participants are not required to pay any amount towards the quiz. All administrative costs etc are borne by DAMS.</p></div>
</div>
<aside class="how-to-apply paddin-zero">
  <div class="course-detail-content">
    <span class="gry-course-box">All the Medical Colleges recognized by MCI can participate in this national quiz event.</span>
    <span class="blue-course-box">The online registration is going to kickoff very soon!!</span><br>
    <p><b>Who can participate:</b></p>
    <span class="gry-course-box">All the students in final year MBBS, and, All students undergoing internship can only participate.</span>
<span class="blue-course-box"><p><b>The process:</b></p>The contestants would need to register online through the provided link.<br> The quiz window will open for given days i.e. the quiz will be available for attempting on given days only.<br>On the basis of merit, the top two scoring students in each zone will comprise of the team from that zone.<br>The grand finale will be conducted in Delhi to decide the winner for the respective season.<br>It is your chance to participate and win fabulous prizes.<br><br>

<span class="gry-course-box"><strong>FIRST PRIZE: </strong> A Cash prize of Rs.50,000 /- (Fifty thousand only) to the Winner And FREE ADMISSION IN ANY course offered by DAMS.<br><br>

<strong>SECOND PRIZE:</strong> A Cash prize of Rs. 25,000/-(Twenty five thousand only) to the first runner up AND 50% discount on course fees on admission in any course offered by DAMS.<br><br>

<strong>THIRD PRIZE:</strong> A Cash prize of Rs. 15,000/-(Fifteen thousand only) to the second runner up AND 25% discount on course fees on admission in any course offered by DAMS.</span>


Get ready to be a part of this prestigious national event.<br><br>

All the best!!!</span>
  </div>
</aside>
<div class="pg-medical-main tab-hide">
        <div class="pg-heading"><span></span>National Quiz Courses</div>
        <div class="course-new-section">
<!--          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('1');"><span class="plus-ico" id="s1"></span>Classroom Course</h5>
            <div class="coures-list-box-content"  id="di1" style="display:none;">
              <ul class="course-new-list">
                <li><a href="gray-matter.php" title="Concept of Grey Matter"><span class="sub-arrow"></span>Concept of Grey Matter</a></li>
                <li><a href="gray-matter-testimonial.php" title="Testimonials"><span class="sub-arrow"></span>Testimonials</a></li>
              </ul>
            </div>
          </div>-->
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('1');"><span class="plus-ico" id="s1"></span>2018</h5>
            <div class="coures-list-box-content" id="di1" style="display:none;">
              <ul class="course-new-list">
                  <article class="interview-photos-section">
                  <div class="idams-box"><br />
 <p>  The Grand Finale of Grey Matter Dams National Quiz. Season 7,2018, was held at DAMS New Delhi. </p><br>
<p>Just like all previous seasons, it was a very exciting contest. </p><br>
<p>It was a roller coaster ride during the quiz. Team scores were going up and down after every round. </p><br>
<p>Finally, SOUTH ZONE team, represented by Mr. Bhavesh, from Bangalore Medical College & Ms. Abhirami Nair, from Govt. TD Medical College, Alaphuza, Kerala, were the proud WINNERS of the prestigious national quiz by DAMS. They won Rs. 50,000/- cash prize for the season 7 - 2018. </p><br>
<p>The team from WEST ZONE were the FIRST RUNNERS UP, represented by Ms. Nikhila Ghanta from AIIMS,  Bhopal & Kunal Kiran Marathe from Seth GS medical college, Mumbai, winning Rs.25,000/- as prize money. </p><br>
<p>The SECOND RUNNERS UP were the NORTH ZONE team represented by Vishal Garg from Maulana Azad Medical College, New Delhi & Sidharth Agarwal from Vardhman Mahavir Medical College, New Delhi  winning a cash prize of Rs.15,000/-.</p><br>
<p>All winners also sought free admission in any DAMS course, anywhere in India, in addition to the prize money.</p><br>
<p>The quiz was mastered by Dr Sumer Sethi. along with Dr Deepti Bahl, and a few other esteemed DAMS faculty.</p> <br>
<p>It was indeed a wonderful finale and was watched live by thousands of doctors all over the country on Facebook and YouTube.</p><br>
<p>All the Best for Season -8, in 2019.</p>

                  </div>
                  </article>


                </ul>
            </div>
          </div>

          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('7');"><span class="plus-ico" id="s7"></span>2017</h5>
            <div class="coures-list-box-content" id="di7" style="display:none;">
              <ul class="course-new-list">
                  <article class="interview-photos-section">
                  <div class="idams-box"><br />
                    <p>As we all know that the Grand Finale of Grey Matter Dams National Quiz. Season 6, 2017,  got over on 3/10/17. </p>
                    <p>It was a very thrilling contest till the end. Team representing North zone took lead from first round. It was closely followed by the wild card entry team from Maulana Azad Medical College, New Delhi. The West Zone team started well but then the lead diminished a bit as the quiz proceeded. South zone team started slowly but picked up later in the contest. East zone team started slow and also picked up in the end.</p>
                    <p>Till the last round it was a fight between north zone team , wild card entry and team from west zone. </p>
                    <p>Finally, North Zone team, represented by Ms. Rhea Ahuja & Umang Arora, both from AIIMS New Delhi, lifted the winners trophy and Rs. 50,000/- cash prize for the season 6 - 2017. Wild card team from Maulana Azad Medical College were the first runners up, represented by Sidharth S & Ankitesh winning Rs.25,000/- as prize money. The second runners up were the team from West zone represented by Hardik Nanavati & Shalin S Shah winning Rs.15,000/- cash prize. </p>
                    <p>All winners would also get free admission in any DAMS course they wish to join anywhere in India in addition to the prize money.</p>
                    <p>The quiz was convened by Dr Sumer Sethi himself along with Dr Deepti Bahl, Dr Arvind, Dr Sachin Arora and Dr Sidharth Mishra.</p>
                    <p>There were several audience prizes also given away for giving right answers to questions put to them.</p>
                    <p>It was a great contest and was watched live by thousands of doctors all over the country on Facebook and YouTube.</p>
                    <p>We will all the best to all the participants in their future endeavours.</p>
                    
                  </div>
                  </article>


                </ul>
            </div>
          </div>
                  <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('6');"><span class="plus-ico" id="s6"></span>2016</h5>
            <div class="coures-list-box-content" id="di6" style="display:none;">
              <ul class="course-new-list">
                  <article class="interview-photos-section">
                  <div class="idams-box"> <span>"1st Campus Round 2016"</span><br />
                    <p>The great Grand Finale of Grey Matter DAMS National Quiz, Season 5 took place on 13 Nov 2016.</p>
                    <p>The four teams were from four zones of the country.</p>
                    <p>The quiz was telecasted live on YouTube and Facebook. It was watched by more than 10000 doctors all over India. LATEST ADDITION TO THE CHAIN OF EVENTS  THAT DAMS HAS PIONEERED IN INDIA. Another plume in the hat.</p>
                    <p>The quiz format was unique and was presented by 4 quiz masters namely Dr. Sumer, Dr. Deepti, Dr. Sachin Garg, Dr. Siddharth Mishra.</p>
                    <p>The quiz lasted for 2 hrs and at the end of it teams were longing for more.</p>
                    <p>The second runners up was the team from R G Kar Medical College, Kolkata. Team represented by Subodh Shankar Behera & Indrashish Chaudhury.</p>
                    <p>They won a cash prize of Rs.15000/- and a discount of 50% in course fees for any DAMS course, valid nationwide.</p>
                    <p>The first runners up was the team from Thanjavur med college, team represented by M Sethuraman & Ragul Ajay BG. They won a cash prize of Rs.25000/- and 100% discount on course fees for any DAMS course, valid nationwide.</p>
                    <p>The WINNERS for the season 5 of this prestigious quiz were the students from AIIMS, New Delhi, represented by Umang Arora & Shubham Agarwal.</p>
                    <p>They lifted the WINNERS trophy and won a cash prize of Rs.50000/-(Fifty Thousand) and 100% discount on course fees.</p>
                  </div>
                  </article>
<!--                <li><a href="national_2016.php" title="1st Campus Round 2016"><span class="sub-arrow"></span>1st Campus Round 2016</a></li>-->
<!--                <li>
                    <a href="national_2016.php" title="1st Campus Round 2016">
                        <span class="sub-arrow"></span></a></li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span> </li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span> </li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span></li>-->
                </ul>
            </div>
          </div>
         <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('5');"><span class="plus-ico" id="s5"></span>2015</h5>
            <div class="coures-list-box-content" id="di5" style="display:none;">
              <ul class="course-new-list">
                <!--<li><a href="aiims-delhi.php" title="1st Campus Round 2015"><span class="sub-arrow"></span>1st Campus Round 2015</a></li>-->
                  <article class="interview-photos-section">
                  <div class="idams-box"> <span>"1st Campus Round 2015"</span><br />
                    <p>The Grand Finale of season 4 of Grey Matter DAMS National Quiz got over in PULSE 2015.</p>
                    <p>The Grand Finale was held in Conference Hall of JLN Auditorium, AIIMS campus, New Delhi on 17th September 2015.</p>
                    <p>This year too the winning team was from AIIMS, New Delhi.</p>
                    <p>The first runners up trophy was lifted by team from SMS Medical College, Jaipur.</p>
                    <p>The second runners up were the team from Govt. Medical College, Imphal, Manipur.</p>
                    <p>It was a nail-biting final, and all the teams had a great contest till the deciding moments.</p>
                    <p>Congratulations!! To all winning teams teams.</p>
                  </div>
                  </article>
<!--                  <div class="idams-box">      
                  <ul class="course-new-list">
                <li><a href="aiims-delhi.php" title="1st Campus Round 2015"><span class="sub-arrow"></span>1st Campus Round 2015</a></li>
                  <li>
                      <a href="national_2015.php" title="1st Campus Round 2015">
                          <span class="sub-arrow"></span></a></li>
                 <li><span class="sub-arrow"></span> </li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span></li>
                <li><span class="sub-arrow"></span> </li>

              </ul>
                  </div>-->

              </ul>
            </div></div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('4');"><span class="plus-ico" id="s4"></span>2014</h5>
            <div class="coures-list-box-content" id="di4" style="display:none;">
              <ul class="course-new-list">
                <!--<li><a href="aiims-delhi.php" title="1st Campus Round 2014"><span class="sub-arrow"></span>1st Campus Round 2014</a></li>-->
<!--                <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
                <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>-->
                      <article class="interview-photos-section">
                  <!--<div class="idams-box"> <span>FIRST ROUND – "CAMPUS ROUND"</span><br>
                    <p>This is a written qualifying round.
                      The first round will be conducted in the respective medical college itself.
                      Once the test is conducted the answer sheets shall be sent to us for evaluation and the result of the test would be compiled. The first two toppers of this round would qualify to represent their college in the next level i.e. the zonal level.</p>
                  </div>
                  <div class="idams-box"> <span>RESULTS !!!</span>
                    <p>The winners of GREY MATTER – DAMS NATIONAL QUIZ, ROUND – 1 are out!!! There has been a great battle for the top two spots in every college, as the top two would represent the college in the next round. The students have shown great character in Round – 1 and those with real mettle have triumphed. By this event we have kicked off healthy competitition amongst the best brains. Medical minds all over the country have shown great enthusiasm in both conducting and participating in this prestigious National event. Good show!! Thanks!!!</p>
                  </div>
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="images/students/student2.jpg" alt="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" title="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" />
                        <p><span>Dr. DEVANSHU BANSAL</span> Rank: 1</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student5.jpg" alt="Student" title="Student" />
                        <p><span>Dr. SHWETA SHUBHDARSHINI</span> Rank: 3</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student3.jpg" alt="Dr. SHEJOL SUMIT" title="Dr. SHEJOL SUMIT" />
                        <p><span>Dr. DEVIKA KIR </span> Rank: 2</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student4.jpg" alt="Dr. ATUL JAIN" title="Dr. ATUL JAIN" />
                        <p><span>Dr. ANURAG CHAHAL</span> Rank: 3</p>
                      </div>
                    </li>
                  </ul>-->
                  <div class="schedule-mini-series paddin-zero">
                    <!--<h4 class="sb-10 f-size25">ALL INDIA INSTITUTE OF MEDICAL SCIENCES NEW DElHI</h4>-->
                    <div class="schedule-mini-top"> <span class="two-part">Student Name</span> <span class="three-part">Marks</span> <span class="one-part">Rank</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"> <span class="two-parts schedule-left-line">LOKESH AGARWAL</span> <span class="one-parts">38</span> <span class="three-part">1</span> </li>
                        <li> <span class="two-parts schedule-left-line">ZAINAB VORA</span> <span class="one-parts">33</span> <span class="three-part">2</span> </li>
                        <li> <span class="two-parts schedule-left-line">SIDDHARTH JAIN</span> <span class="one-parts">31</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">KARTIK GUPTA</span> <span class="one-parts">30</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">RITURAJ UPADHAYAY</span> <span class="one-parts">30</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">DIVYA AGARWAL</span> <span class="one-parts">29</span> <span class="three-part">5</span> </li>
                        <li> <span class="two-parts schedule-left-line">TUNGISH BANSAL</span> <span class="one-parts">28</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">SRISHTI SAHA</span> <span class="one-parts">28</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">SELMA C.H.</span> <span class="one-parts">26</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">AMBER AMAR BHAYANA</span> <span class="one-parts">26</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">IPSITA SAHOO</span> <span class="one-parts">25</span> <span class="three-part">8</span> </li>
                        <li> <span class="two-parts schedule-left-line">SOUMYA SAGAR</span> <span class="one-parts">23</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">MUFEED A K</span> <span class="one-parts">18</span> <span class="three-part">10</span> </li>
                        <li> <span class="two-parts schedule-left-line">SREENATH V</span> <span class="one-parts">18</span> <span class="three-part">10</span> </li>
                        <li> <span class="two-parts schedule-left-line">SUNNY SINGHAL</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">NELLAI KRISHNANS</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">ALTHAF MAJEED RAHMAN</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">ROHITH K P</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">ANUPAM KANODIA</span> <span class="one-parts">16</span> <span class="three-part">12</span> </li>
                        <li> <span class="two-parts schedule-left-line">NAZWIN N K</span> <span class="one-parts">16</span> <span class="three-part">12</span> </li>
                        <li> <span class="two-parts schedule-left-line">CHNDRA KUMAR KHANDE</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">BASIL</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">VARUN T S</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">SHAFNEED C H</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">ALTHAF MAJEED</span> <span class="one-parts">14</span> <span class="three-part">14</span> </li>
                        <li> <span class="two-parts schedule-left-line">EBIN SEBASTIEN</span> <span class="one-parts">13</span> <span class="three-part">15</span> </li>
                        <li> <span class="two-parts schedule-left-line">FAZLU REHMAN</span> <span class="one-parts">13</span> <span class="three-part">15</span> </li>
                        <li> <span class="two-parts schedule-left-line">SACHIN JOSE</span> <span class="one-parts">13</span> <span class="three-part">15</span> </li>
                        <li> <span class="two-parts schedule-left-line">NITIN PADMANABAN</span> <span class="one-parts">12</span> <span class="three-part">16</span> </li>
                        <li> <span class="two-parts schedule-left-line">KESAVAN P S</span> <span class="one-parts">12</span> <span class="three-part">16</span> </li>
                        <li> <span class="two-parts schedule-left-line">FAWAZ YOUSUF</span> <span class="one-parts">11</span> <span class="three-part">17</span> </li>
                        <li> <span class="two-parts schedule-left-line">SYED SIYAZ BADR</span> <span class="one-parts">11</span> <span class="three-part">17</span> </li>
                        <li> <span class="two-parts schedule-left-line">SREELAL T V</span> <span class="one-parts">11</span> <span class="three-part">17</span> </li>
                        <li> <span class="two-parts schedule-left-line">RAGHU NANDHAN</span> <span class="one-parts">9</span> <span class="three-part">18</span> </li>
                        <li> <span class="two-parts schedule-left-line">AMAL RAZIK</span> <span class="one-parts">7</span> <span class="three-part">19</span> </li>
                      </ul>
                    </div>
                  </div>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                </article>
              </ul>
            </div>
          </div>
  
         <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('3');"><span class="plus-ico" id="s3"></span>2013</h5>
            <div class="coures-list-box-content" id="di3" style="display:none;">
              <ul class="course-new-list">
<!--                <li><a href="season2round1.php" title="1st Campus Round 2013"><span class="sub-arrow"></span>1st Campus Round 2013</a></li>
                <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
                <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>-->
                <article class="interview-photos-section">
                  <div class="idams-box"> <span>"The Grand Finale"</span><br />
                    <p>This shall be a quiz round and four teams from all the four zones will try their luck for the top spot. This round will be conducted at JLN Auditorium, AIIMS, New Delhi on 5th Feb. 2013. There shall be total six rounds comprising of quiz questions, visual round, and, rapid fire round. The four teams from all zones i.e. East, West, North, and, South have already reached the finals. One team is going to win the title for the SEASON -1. ALL THE BEST TO ALL PARTICIPATING TEAMS!!!!! </p>
                  </div>
                  <div class="idams-box"> <span>RESULTS !!!</span>
                    <p>SPLENDID CONTEST!! The result of the GRAND FINALE is out for GREY MATTER - DAMS NATIONAL QUIZ, SEASON - 1. Teams from all the zones had the most intriguing and tough fight. The quiz lasted for an hour and a half and saw some great moments where the teams were going great guns. There were six battling rounds and the war was won by the best team. The entire event was hosted by Dr. Sumer K Sethi. </p>
                    <span>The final results were: </span><br />
                    <span>The Winner: </span><br />
                    <span>NORTH ZONE TEAM</span><br />
                    <span>Team from</span><br />
                    <span>All India Institute of Medical Sciences ( AIIMS ) New Delhi.</span><br />
                    <p>Devanshu Bansal</p>
                    <p>Devika Kir</p>
                    <p>THE PRIZE: The winners went home with a cash prize of Rs.50,000/-(Fifty thousand only) and FREE ADMISSION in any course offered by DAMS. They also lifted the 'WINNER' trophy. </p>
                  </div>
                  <div class="idams-box"> <span>1st runner up: </span><br />
                    <span>East Zone Team</span><br />
                    <span>Team from</span><br />
                    <span>Veer Surendra Sai Medical Collage &amp; Hospital, Sambalpur, Odisha.</span><br />
                    <p>Deepamjyoti Nanda</p>
                    <p>Nalinkanta Ghosh</p>
                    <p>THE PRIZE: The first runner up team went home with a cash prize of Rs. 25,000/ -(Twenty five thousand only) and admission to any course offered by DAMS at a discount of 50% on course fees. They also lifted the 1st Runner-Up Trophy. </p>
                  </div>
                  <div class="idams-box"> <span>2nd runner up: </span><br />
                    <span>West Zone Team</span><br />
                    <span>Team from</span><br />
                    <span>M.P. Shah Medical Collage, Jamnager. </span><br />
                    <p>Ankit Madan</p>
                    <p>Shubham Dilipbhai Maheshwari</p>
                    <p>THE PRIZE: The second runner up team went home with a cash prize of Rs. 15,000/-(Fifteen thousand only) and admission to any course offered by DAMS at a discount of 25% on course fees. They also lifted the 2nd Runner-Up Trophy. </p>
                  </div>
                  <div class="idams-box"> <span>South Zone Team</span><br />
                    <span>Team from</span><br />
                    <span>Kasturba Medical Collage,Manipal,Karnatka</span><br />
                    <p>HIMAMSHU ACHARYA</p>
                    <p>ISHA GAMBHIR</p>
                    <p>They bagged the Participant Trophy for playing fair and making it to the Grand Finale, Season – 1. </p>
                    <span>Best Challenger Trophy</span>
                    <p>The Best Challenger Trophy for Season – 1 is won by SWETA SUBHADARSHINI of AIIMS. She has been giving a tough competition to the teams reaching the GRAND FINALE at all levels namely CAMPUS &amp; ZONAL ROUNDS. She missed the seat in the finals by a narrow margin. Congrats to her and all the best in life. PRIZE: She won the cash prize of Rs.10000/-(Ten thousand only) along with the "BEST CHALLENGER TROPHY"</p>
                  </div>
                  <br style="border:none;" />
                  <br style="border:none;" />
                </article>
              </ul>
            </div>
          </div>
  <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('2');"><span class="plus-ico" id="s2"></span>2012</h5>
            <div class="coures-list-box-content" id="di2" style="display:none;">
              <ul class="course-new-list">
<!--                <li><a href="round1.php" title="1st Campus Round 2012"><span class="sub-arrow"></span>1st Campus Round 2012</a></li>
                <li><a href="round2.php" title="2nd Campus Round 2012"><span class="sub-arrow"></span>2nd Campus Round 2012</a></li>-->
<!--                <li><a href="grand.php" title="Grand Finale 2012"><span class="sub-arrow"></span>Grand Finale 2012</a></li>-->
                     <article class="interview-photos-section">
                  <div class="idams-box"> <span>"The Grand Finale"</span><br>
                    <p>This shall be a quiz round and four teams from all the four zones will try their luck for the top spot. This round will be conducted at JLN Auditorium, AIIMS, New Delhi on 5th Feb. 2013. There shall be total six rounds comprising of quiz questions, visual round, and, rapid fire round. The four teams from all zones i.e. East, West, North, and, South have already reached the finals. One team is going to win the title for the SEASON -1. ALL THE BEST TO ALL PARTICIPATING TEAMS!!!!! </p>
                  </div>
<!--                  <div class="idams-box"> <span>RESULTS !!!</span>
                    <p>SPLENDID CONTEST!! The result of the GRAND FINALE is out for GREY MATTER - DAMS NATIONAL QUIZ, SEASON - 1. Teams from all the zones had the most intriguing and tough fight. The quiz lasted for an hour and a half and saw some great moments where the teams were going great guns. There were six battling rounds and the war was won by the best team. The entire event was hosted by Dr. Sumer K Sethi. </p>
                    <span>The final results were: </span><br>
                    <span>The Winner: </span><br>
                    <span>NORTH ZONE TEAM</span><br>
                    <span>Team from</span><br>
                    <span>All India Institute of Medical Sciences ( AIIMS ) New Delhi.</span><br>
                    <p>Devanshu Bansal</p>
                    <p>Devika Kir</p>
                    <p>THE PRIZE: The winners went home with a cash prize of Rs.50,000/-(Fifty thousand only) and FREE ADMISSION in any course offered by DAMS. They also lifted the 'WINNER' trophy. </p>
                  </div>
                  <div class="idams-box"> <span>1st runner up: </span><br>
                    <span>East Zone Team</span><br>
                    <span>Team from</span><br>
                    <span>Veer Surendra Sai Medical Collage &amp; Hospital, Sambalpur, Odisha.</span><br>
                    <p>Deepamjyoti Nanda</p>
                    <p>Nalinkanta Ghosh</p>
                    <p>THE PRIZE: The first runner up team went home with a cash prize of Rs. 25,000/ -(Twenty five thousand only) and admission to any course offered by DAMS at a discount of 50% on course fees. They also lifted the 1st Runner-Up Trophy. </p>
                  </div>
                  <div class="idams-box"> <span>2nd runner up: </span><br>
                    <span>West Zone Team</span><br>
                    <span>Team from</span><br>
                    <span>M.P. Shah Medical Collage, Jamnager. </span><br>
                    <p>Ankit Madan</p>
                    <p>Shubham Dilipbhai Maheshwari</p>
                    <p>THE PRIZE: The second runner up team went home with a cash prize of Rs. 15,000/-(Fifteen thousand only) and admission to any course offered by DAMS at a discount of 25% on course fees. They also lifted the 2nd Runner-Up Trophy. </p>
                  </div>
                  <div class="idams-box"> <span>South Zone Team</span><br>
                    <span>Team from</span><br>
                    <span>Kasturba Medical Collage,Manipal,Karnatka</span><br>
                    <p>HIMAMSHU ACHARYA</p>
                    <p>ISHA GAMBHIR</p>
                    <p>They bagged the Participant Trophy for playing fair and making it to the Grand Finale, Season – 1. </p>
                    <span>Best Challenger Trophy</span>
                    <p>The Best Challenger Trophy for Season – 1 is won by SWETA SUBHADARSHINI of AIIMS. She has been giving a tough competition to the teams reaching the GRAND FINALE at all levels namely CAMPUS &amp; ZONAL ROUNDS. She missed the seat in the finals by a narrow margin. Congrats to her and all the best in life. PRIZE: She won the cash prize of Rs.10000/-(Ten thousand only) along with the "BEST CHALLENGER TROPHY"</p>
                  </div>-->
                  <ul class="main-students-list ">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="images/students/student2.jpg" alt="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" title="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" />
                        <p><span>Dr. Devanshu Bansal</span> Rank: 1</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student5.jpg" alt="Student" title="Student" />
                        <p><span>Dr. Shweta Shubhdarshini</span> Rank: 3</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student3.jpg" alt="Dr. SHEJOL SUMIT" title="Dr. SHEJOL SUMIT" />
                        <p><span>Dr. Devika Kir</span> Rank: 2</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student4.jpg" alt="Dr. ATUL JAIN" title="Dr. ATUL JAIN" />
                        <p><span>Dr. Anurag Chahal</span> Rank: 3</p>
                      </div>
                    </li>
                  </ul>
<!--                  <div class="schedule-mini-series  t-space-20">
                    <h4 class="sb-10 f-size25">ALL INDIA INSTITUTE OF MEDICAL SCIENCES NEW DElHI</h4>
                    <div class="schedule-mini-top"> <span class="two-part">Student Name</span> <span class="three-part">Marks</span> <span class="one-part">Rank</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"> <span class="two-parts schedule-left-line">Devanshu Bansal</span> <span class="one-parts">40</span> <span class="three-part">1</span> </li>
                        <li> <span class="two-parts schedule-left-line">Devika Kir</span> <span class="one-parts">34</span> <span class="three-part">2</span> </li>
                        <li> <span class="two-parts schedule-left-line">Arjung Gupta</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Shweta Shubhdarshini</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Sarita Kumari</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Anurag Chahal</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Avin Goel</span> <span class="one-parts">32</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">Kaustav Majumder</span> <span class="one-parts">32</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">Savinay Kapur</span> <span class="one-parts">31</span> <span class="three-part">5</span> </li>
                        <li> <span class="two-parts schedule-left-line">Rituraj Upadhyay</span> <span class="one-parts">31</span> <span class="three-part">5</span> </li>
                        <li> <span class="two-parts schedule-left-line">Manpreet Uppal</span> <span class="one-parts">30</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">Nishant Gurnani</span> <span class="one-parts">30</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">Barun Bagga</span> <span class="one-parts">29</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">Sahil Gupta</span> <span class="one-parts">29</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">Devesh Kumawat</span> <span class="one-parts">29</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">Harshit Garg</span> <span class="one-parts">28</span> <span class="three-part">8</span> </li>
                        <li> <span class="two-parts schedule-left-line">Vineet Kumar</span> <span class="one-parts">28</span> <span class="three-part">8</span> </li>
                        <li> <span class="two-parts schedule-left-line">Varsha Mathews</span> <span class="one-parts">24</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">Neethu</span> <span class="one-parts">24</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">Ahmed Nawid Latifi</span> <span class="one-parts">24</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">ANUJ A</span> <span class="one-parts">20</span> <span class="three-part">10</span> </li>
                        <li> <span class="two-parts schedule-left-line">Mandeep Singh Virk</span> <span class="one-parts">18</span> <span class="three-part">11</span> </li>
                      </ul>
                    </div>
                  </div>-->
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                </article>
              </ul>
            </div>
          </div>
        </div>
      </div>
    
    <div class="news-update-box" style="margin-top:25px;">
        <div class="n-videos"><span></span> </div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block" >
            <iframe width="100%" height="390" src="//www.youtube.com/embed/z7Kc6oANcq0?wmode=transparent" class="border_none"></iframe>
          
          </div>
<!--          <div id="vd1" class="display_none" >
            <iframe width="100%" height="390" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd2" class="display_none" >
            <iframe width="100%" height="390" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd3" class="display_none" >
            <iframe width="100%" height="390" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
          </div>-->
        </div>
        <div class="box-bottom-1"> `
<!--            <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="photo-gallery.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span>-->
        </div>
      </div>
</div>

<div id="DNB">
<div class="satellite-content">
<div class="idams-box1">
<p>“QUEST – DAMS NATIONAL DENTAL QUIZ” is one of its kind DENTAL quiz program. The project is intended to find and honor the best brain in the country pursuing dentistry. The project is an initiative to mainstream the idea of “Dentistry is learning” in existing dental medicos, by awareness generation among students; creating empirical evidence of positive outcomes and advocating it through multi tier quiz contest.</p>
</div>
</div>
<aside class="how-to-apply paddin-zero">
<!--<div class="how-to-apply-heading"><span></span> Quiz Highlights</div>-->
<div class="course-detail-content">

<span class="gry-course-box">This project is being implemented nationwide in 4 zones all over India i.e. EAST; WEST; NORTH; SOUTH.
</span>

<span class="blue-course-box">However, only one team will emerge as the winner of the Grand Prize of Rs. 30,000/-(Rs. Thirty thousand only).
We organize this National quiz to seek and facilitate young talented brains studying dentistry, which shall further consolidate their learning through such ventures. It is going to provide an opportunity to students to enrich themselves by our experiences and further help them to explore the hidden talent in them.Another fact that needs to be applauded is that at any level of quiz, participants are not required to pay any amount towards the quiz. All administrative costs etc are borne by DAMS.</span>

<span class="gry-course-box">This year, being the SEASON 4, the format of the quiz is going to change from the conventional Round 1, Round 2 pattern to and online quiz pattern. 
</span>

<span class="blue-course-box">All the Dental Colleges recognized by DCI can participate in this national quiz event.</span>
<p><b>Who can participate:</b></p>

<span class="gry-course-box">All the students in FINAL Year in BDS can participate in this mega event.All INTERNS are also entitled to participate.</span>

<span class="blue-course-box"><p><b>The process:</b></p>The contestants would need to register online through the provided link.<br> The quiz window will open for given days i.e. the quiz will be available for attempting on given days only.<br>On the basis of merit, the top two scoring students in each zone will comprise of the team from that zone.<br>The grand finale will be conducted in Delhi to decide the winner for the respective season.<br>It is your chance to participate and win fabulous prizes.<br></br>

<span class="gry-course-box"><strong>FIRST PRIZE: </strong>A Cash prize of Rs.30,000/- (Thirty thousand only) to the Winner And FREE ADMISSION IN ANY course offered by DAMS.<br></br>

<strong>SECOND PRIZE:</strong>A Cash prize of Rs. 20,000/-(Twenty thousand only) to the first runner up AND 50% discount on course fees on admission in any course offered by DAMS.<br></br>

<strong>THIRD PRIZE:</strong> A Cash prize of Rs. 10,000/-(Ten thousand only) to the second runner up AND 25% discount on course fees on admission in any course offered by DAMS.</span>

So, all the best to all talented young dentists of the country.<br></br>

Get ready to be a part of this prestigious national event.<br></br>

All the best!!!</span>
</div>
</aside>

    <div class="pg-medical-main tab-hide">
        <div class="pg-heading"><span></span>National Quiz Courses</div>
        <div class="course-new-section">
<!--          <div class="coures-list-box">
            <h5 class="h22toggle" onclick="sliddes1('1');"><span class="plus-ico" id="ss1"></span>Classroom Course</h5>
            <div class="coures-list-box-content"  id="dii1" style="display:none;">
              <ul class="course-new-list">
                <li><a href="gray-matter.php" title="Concept of Grey Matter"><span class="sub-arrow"></span>Concept of Grey Matter</a></li>
                <li><a href="gray-matter-testimonial.php" title="Testimonials"><span class="sub-arrow"></span>Testimonials</a></li>
              </ul>
            </div>
          </div>-->
<!--          <div class="coures-list-box">
            <h5 class="h22toggle" onclick="sliddes1('6');"><span class="plus-ico" id="ss6"></span>2016</h5>
            <div class="coures-list-box-content" id="dii6" style="display:none;">
              <ul class="course-new-list">
                 
                <li><a href="national_2016.php" title="1st Campus Round 2016"><span class="sub-arrow"></span>1st Campus Round 2016</a></li>
                <li><a href="national_2016.php" title="1st Campus Round 2016"><span class="sub-arrow"></span>1st Campus Round 2016</a></li>
                <li><span class="sub-arrow"></span>The great Grand Finale of Grey Matter DAMS National Quiz, Season 5 took place on 13 Nov 2016.</li>
                <li><span class="sub-arrow"></span>The four teams were from four zones of the country.</li>
                <li><span class="sub-arrow"></span>The quiz was telecasted live on YouTube and Facebook. It was watched by more than 10000 doctors all over India. LATEST ADDITION TO THE CHAIN OF EVENTS  THAT DAMS HAS PIONEERED IN INDIA. Another plume in the hat. </li>
                <li><span class="sub-arrow"></span>The quiz format was unique and was presented by 4 quiz masters namely Dr. Sumer, Dr. Deepti, Dr. Sachin Garg, Dr. Siddharth Mishra.</li>
                <li><span class="sub-arrow"></span>The quiz lasted for 2 hrs and at the end of it teams were longing for more. </li>
                <li><span class="sub-arrow"></span>The second runners up was the team from R G Kar Medical College, Kolkata. Team represented by Subodh Shankar Behera & Indrashish Chaudhury.</li>
                <li><span class="sub-arrow"></span>They won a cash prize of Rs.15000/- and a discount of 50% in course fees for any DAMS course, valid nationwide.</li>
                <li><span class="sub-arrow"></span>The first runners up was the team from Thanjavur med college, team represented by M Sethuraman & Ragul Ajay BG. They won a cash prize of Rs.25000/- and 100% discount on course fees for any DAMS course, valid nationwide.</li>
                <li><span class="sub-arrow"></span>The WINNERS for the season 5 of this prestigious quiz were the students from AIIMS, New Delhi, represented by Umang Arora & Shubham Agarwal.</li>
                <li><span class="sub-arrow"></span>They lifted the WINNERS trophy and won a cash prize of Rs.50000/-(Fifty Thousand) and 100% discount on course fees.</li>
                                <article class="interview-photos-section">
                  <div class="idams-box"> <span>"1st Campus Round 2016"</span><br />
                    <p>The great Grand Finale of Grey Matter DAMS National Quiz, Season 5 took place on 13 Nov 2016.</p>
                    <p>The four teams were from four zones of the country.</p>
                    <p>The quiz was telecasted live on YouTube and Facebook. It was watched by more than 10000 doctors all over India. LATEST ADDITION TO THE CHAIN OF EVENTS  THAT DAMS HAS PIONEERED IN INDIA. Another plume in the hat.</p>
                    <p>The quiz format was unique and was presented by 4 quiz masters namely Dr. Sumer, Dr. Deepti, Dr. Sachin Garg, Dr. Siddharth Mishra.</p>
                    <p>The quiz lasted for 2 hrs and at the end of it teams were longing for more.</p>
                    <p>The second runners up was the team from R G Kar Medical College, Kolkata. Team represented by Subodh Shankar Behera & Indrashish Chaudhury.</p>
                    <p>They won a cash prize of Rs.15000/- and a discount of 50% in course fees for any DAMS course, valid nationwide.</p>
                    <p>The first runners up was the team from Thanjavur med college, team represented by M Sethuraman & Ragul Ajay BG. They won a cash prize of Rs.25000/- and 100% discount on course fees for any DAMS course, valid nationwide.</p>
                    <p>The WINNERS for the season 5 of this prestigious quiz were the students from AIIMS, New Delhi, represented by Umang Arora & Shubham Agarwal.</p>
                    <p>They lifted the WINNERS trophy and won a cash prize of Rs.50000/-(Fifty Thousand) and 100% discount on course fees.</p>
                  </div>
                  </article>  
              </ul>
            </div>
          </div>-->
               <div class="coures-list-box">
            <h5 class="h22toggle" onclick="sliddes1('5');"><span class="plus-ico" id="ss5"></span>2015</h5>
            <div class="coures-list-box-content" id="dii5" style="display:none;">
              <ul class="course-new-list">
                <!--<li><a href="aiims-delhi.php" title="1st Campus Round 2015"><span class="sub-arrow"></span>1st Campus Round 2015</a></li>-->
<!--                  <li><a href="national_2015.php" title="1st Campus Round 2015"><span class="sub-arrow"></span>1st Campus Round 2015</a></li>
                 <li><span class="sub-arrow"></span>The Grand Finale of season 4 of Grey Matter DAMS National Quiz got over in PULSE 2015. </li>
                <li><span class="sub-arrow"></span>The Grand Finale was held in Conference Hall of JLN Auditorium, AIIMS campus, New Delhi on 17th September 2015.</li>
                <li><span class="sub-arrow"></span>This year too the winning team was from AIIMS, New Delhi.</li>
                <li><span class="sub-arrow"></span>The first runners up trophy was lifted by team from SMS Medical College, Jaipur.</li>
                <li><span class="sub-arrow"></span>The second runners up were the team from Govt. Medical College, Imphal, Manipur.</li>
                <li><span class="sub-arrow"></span>It was a nail-biting final, and all the teams had a great contest till the deciding moments.</li>
                <li><span class="sub-arrow"></span>Congratulations!! To all winning teams teams. </li>-->
                                    <article class="interview-photos-section">
                  <div class="idams-box"> <span>"1st Campus Round 2015"</span><br />
                    <p>The Grand Finale of season 4 of Grey Matter DAMS National Quiz got over in PULSE 2015.</p>
                    <p>The Grand Finale was held in Conference Hall of JLN Auditorium, AIIMS campus, New Delhi on 17th September 2015.</p>
                    <p>This year too the winning team was from AIIMS, New Delhi.</p>
                    <p>The first runners up trophy was lifted by team from SMS Medical College, Jaipur.</p>
                    <p>The second runners up were the team from Govt. Medical College, Imphal, Manipur.</p>
                    <p>It was a nail-biting final, and all the teams had a great contest till the deciding moments.</p>
                    <p>Congratulations!! To all winning teams teams.</p>
                  </div>
                  </article>

              </ul>
            </div>
          </div>
<!--          <div class="coures-list-box">
            <h5 class="h22toggle" onclick="sliddes1('4');"><span class="plus-ico" id="ss4"></span>2014</h5>
            <div class="coures-list-box-content" id="dii4" style="display:none;">
              <ul class="course-new-list">
                <li><a href="aiims-delhi.php" title="1st Campus Round 2014"><span class="sub-arrow"></span>1st Campus Round 2014</a></li>
                <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
                <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>
                    <article class="interview-photos-section">
                  <div class="idams-box"> <span>FIRST ROUND – "CAMPUS ROUND"</span><br>
                    <p>This is a written qualifying round.
                      The first round will be conducted in the respective medical college itself.
                      Once the test is conducted the answer sheets shall be sent to us for evaluation and the result of the test would be compiled. The first two toppers of this round would qualify to represent their college in the next level i.e. the zonal level.</p>
                  </div>
                  <div class="idams-box"> <span>RESULTS !!!</span>
                    <p>The winners of GREY MATTER – DAMS NATIONAL QUIZ, ROUND – 1 are out!!! There has been a great battle for the top two spots in every college, as the top two would represent the college in the next round. The students have shown great character in Round – 1 and those with real mettle have triumphed. By this event we have kicked off healthy competitition amongst the best brains. Medical minds all over the country have shown great enthusiasm in both conducting and participating in this prestigious National event. Good show!! Thanks!!!</p>
                  </div>
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="images/students/student2.jpg" alt="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" title="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" />
                        <p><span>Dr. DEVANSHU BANSAL</span> Rank: 1</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student5.jpg" alt="Student" title="Student" />
                        <p><span>Dr. SHWETA SHUBHDARSHINI</span> Rank: 3</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student3.jpg" alt="Dr. SHEJOL SUMIT" title="Dr. SHEJOL SUMIT" />
                        <p><span>Dr. DEVIKA KIR </span> Rank: 2</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student4.jpg" alt="Dr. ATUL JAIN" title="Dr. ATUL JAIN" />
                        <p><span>Dr. ANURAG CHAHAL</span> Rank: 3</p>
                      </div>
                    </li>
                  </ul>
                  <div class="schedule-mini-series paddin-zero">
                    <h4 class="sb-10 f-size25">ALL INDIA INSTITUTE OF MEDICAL SCIENCES NEW DElHI</h4>
                    <div class="schedule-mini-top"> <span class="two-part">Student Name</span> <span class="three-part">Marks</span> <span class="one-part">Rank</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"> <span class="two-parts schedule-left-line">LOKESH AGARWAL</span> <span class="one-parts">38</span> <span class="three-part">1</span> </li>
                        <li> <span class="two-parts schedule-left-line">ZAINAB VORA</span> <span class="one-parts">33</span> <span class="three-part">2</span> </li>
                        <li> <span class="two-parts schedule-left-line">SIDDHARTH JAIN</span> <span class="one-parts">31</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">KARTIK GUPTA</span> <span class="one-parts">30</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">RITURAJ UPADHAYAY</span> <span class="one-parts">30</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">DIVYA AGARWAL</span> <span class="one-parts">29</span> <span class="three-part">5</span> </li>
                        <li> <span class="two-parts schedule-left-line">TUNGISH BANSAL</span> <span class="one-parts">28</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">SRISHTI SAHA</span> <span class="one-parts">28</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">SELMA C.H.</span> <span class="one-parts">26</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">AMBER AMAR BHAYANA</span> <span class="one-parts">26</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">IPSITA SAHOO</span> <span class="one-parts">25</span> <span class="three-part">8</span> </li>
                        <li> <span class="two-parts schedule-left-line">SOUMYA SAGAR</span> <span class="one-parts">23</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">MUFEED A K</span> <span class="one-parts">18</span> <span class="three-part">10</span> </li>
                        <li> <span class="two-parts schedule-left-line">SREENATH V</span> <span class="one-parts">18</span> <span class="three-part">10</span> </li>
                        <li> <span class="two-parts schedule-left-line">SUNNY SINGHAL</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">NELLAI KRISHNANS</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">ALTHAF MAJEED RAHMAN</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">ROHITH K P</span> <span class="one-parts">17</span> <span class="three-part">11</span> </li>
                        <li> <span class="two-parts schedule-left-line">ANUPAM KANODIA</span> <span class="one-parts">16</span> <span class="three-part">12</span> </li>
                        <li> <span class="two-parts schedule-left-line">NAZWIN N K</span> <span class="one-parts">16</span> <span class="three-part">12</span> </li>
                        <li> <span class="two-parts schedule-left-line">CHNDRA KUMAR KHANDE</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">BASIL</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">VARUN T S</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">SHAFNEED C H</span> <span class="one-parts">15</span> <span class="three-part">13</span> </li>
                        <li> <span class="two-parts schedule-left-line">ALTHAF MAJEED</span> <span class="one-parts">14</span> <span class="three-part">14</span> </li>
                        <li> <span class="two-parts schedule-left-line">EBIN SEBASTIEN</span> <span class="one-parts">13</span> <span class="three-part">15</span> </li>
                        <li> <span class="two-parts schedule-left-line">FAZLU REHMAN</span> <span class="one-parts">13</span> <span class="three-part">15</span> </li>
                        <li> <span class="two-parts schedule-left-line">SACHIN JOSE</span> <span class="one-parts">13</span> <span class="three-part">15</span> </li>
                        <li> <span class="two-parts schedule-left-line">NITIN PADMANABAN</span> <span class="one-parts">12</span> <span class="three-part">16</span> </li>
                        <li> <span class="two-parts schedule-left-line">KESAVAN P S</span> <span class="one-parts">12</span> <span class="three-part">16</span> </li>
                        <li> <span class="two-parts schedule-left-line">FAWAZ YOUSUF</span> <span class="one-parts">11</span> <span class="three-part">17</span> </li>
                        <li> <span class="two-parts schedule-left-line">SYED SIYAZ BADR</span> <span class="one-parts">11</span> <span class="three-part">17</span> </li>
                        <li> <span class="two-parts schedule-left-line">SREELAL T V</span> <span class="one-parts">11</span> <span class="three-part">17</span> </li>
                        <li> <span class="two-parts schedule-left-line">RAGHU NANDHAN</span> <span class="one-parts">9</span> <span class="three-part">18</span> </li>
                        <li> <span class="two-parts schedule-left-line">AMAL RAZIK</span> <span class="one-parts">7</span> <span class="three-part">19</span> </li>
                      </ul>
                    </div>
                  </div>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                </article>
              </ul>
            </div>
          </div>
     <div class="coures-list-box">
            <h5 class="h22toggle" onclick="sliddes1('3');"><span class="plus-ico" id="ss3"></span>2013</h5>
            <div class="coures-list-box-content" id="dii3" style="display:none;">
              <ul class="course-new-list">
                <li><a href="season2round1.php" title="1st Campus Round 2013"><span class="sub-arrow"></span>1st Campus Round 2013</a></li>
                <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
                <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>
                       <article class="interview-photos-section">
                  <div class="idams-box"> <span>"The Grand Finale 2013"</span><br />
                    <p>This shall be a quiz round and four teams from all the four zones will try their luck for the top spot. This round will be conducted at JLN Auditorium, AIIMS, New Delhi on 5th Feb. 2013. There shall be total six rounds comprising of quiz questions, visual round, and, rapid fire round. The four teams from all zones i.e. East, West, North, and, South have already reached the finals. One team is going to win the title for the SEASON -1. ALL THE BEST TO ALL PARTICIPATING TEAMS!!!!! </p>
                  </div>
                  <div class="idams-box"> <span>RESULTS !!!</span>
                    <p>SPLENDID CONTEST!! The result of the GRAND FINALE is out for GREY MATTER - DAMS NATIONAL QUIZ, SEASON - 1. Teams from all the zones had the most intriguing and tough fight. The quiz lasted for an hour and a half and saw some great moments where the teams were going great guns. There were six battling rounds and the war was won by the best team. The entire event was hosted by Dr. Sumer K Sethi. </p>
                    <span>The final results were: </span><br />
                    <span>The Winner: </span><br />
                    <span>NORTH ZONE TEAM</span><br />
                    <span>Team from</span><br />
                    <span>All India Institute of Medical Sciences ( AIIMS ) New Delhi.</span><br />
                    <p>Devanshu Bansal</p>
                    <p>Devika Kir</p>
                    <p>THE PRIZE: The winners went home with a cash prize of Rs.50,000/-(Fifty thousand only) and FREE ADMISSION in any course offered by DAMS. They also lifted the 'WINNER' trophy. </p>
                  </div>
                  <div class="idams-box"> <span>1st runner up: </span><br />
                    <span>East Zone Team</span><br />
                    <span>Team from</span><br />
                    <span>Veer Surendra Sai Medical Collage &amp; Hospital, Sambalpur, Odisha.</span><br />
                    <p>Deepamjyoti Nanda</p>
                    <p>Nalinkanta Ghosh</p>
                    <p>THE PRIZE: The first runner up team went home with a cash prize of Rs. 25,000/ -(Twenty five thousand only) and admission to any course offered by DAMS at a discount of 50% on course fees. They also lifted the 1st Runner-Up Trophy. </p>
                  </div>
                  <div class="idams-box"> <span>2nd runner up: </span><br />
                    <span>West Zone Team</span><br />
                    <span>Team from</span><br />
                    <span>M.P. Shah Medical Collage, Jamnager. </span><br />
                    <p>Ankit Madan</p>
                    <p>Shubham Dilipbhai Maheshwari</p>
                    <p>THE PRIZE: The second runner up team went home with a cash prize of Rs. 15,000/-(Fifteen thousand only) and admission to any course offered by DAMS at a discount of 25% on course fees. They also lifted the 2nd Runner-Up Trophy. </p>
                  </div>
                  <div class="idams-box"> <span>South Zone Team</span><br />
                    <span>Team from</span><br />
                    <span>Kasturba Medical Collage,Manipal,Karnatka</span><br />
                    <p>HIMAMSHU ACHARYA</p>
                    <p>ISHA GAMBHIR</p>
                    <p>They bagged the Participant Trophy for playing fair and making it to the Grand Finale, Season – 1. </p>
                    <span>Best Challenger Trophy</span>
                    <p>The Best Challenger Trophy for Season – 1 is won by SWETA SUBHADARSHINI of AIIMS. She has been giving a tough competition to the teams reaching the GRAND FINALE at all levels namely CAMPUS &amp; ZONAL ROUNDS. She missed the seat in the finals by a narrow margin. Congrats to her and all the best in life. PRIZE: She won the cash prize of Rs.10000/-(Ten thousand only) along with the "BEST CHALLENGER TROPHY"</p>
                  </div>
                  <br style="border:none;" />
                  <br style="border:none;" />
                </article>
              </ul>
            </div>
          

          <div class="coures-list-box">
            <h5 class="h22toggle" onclick="sliddes1('2');"><span class="plus-ico" id="ss2"></span>2012</h5>
            <div class="coures-list-box-content" id="dii2" style="display:none;">
              <ul class="course-new-list">
                <li><a href="round1.php" title="1st Campus Round 2012"><span class="sub-arrow"></span>1st Campus Round 2012</a></li>
                <li><a href="round2.php" title="2nd Campus Round 2012"><span class="sub-arrow"></span>2nd Campus Round 2012</a></li>
                <li><a href="grand.php" title="Grand Finale 2012"><span class="sub-arrow"></span>Grand Finale 2012</a></li>
                     <article class="interview-photos-section">
                  <div class="idams-box"> <span>"The Grand Finale"</span><br>
                    <p>This shall be a quiz round and four teams from all the four zones will try their luck for the top spot. This round will be conducted at JLN Auditorium, AIIMS, New Delhi on 5th Feb. 2013. There shall be total six rounds comprising of quiz questions, visual round, and, rapid fire round. The four teams from all zones i.e. East, West, North, and, South have already reached the finals. One team is going to win the title for the SEASON -1. ALL THE BEST TO ALL PARTICIPATING TEAMS!!!!! </p>
                  </div>
                  <div class="idams-box"> <span>RESULTS !!!</span>
                    <p>SPLENDID CONTEST!! The result of the GRAND FINALE is out for GREY MATTER - DAMS NATIONAL QUIZ, SEASON - 1. Teams from all the zones had the most intriguing and tough fight. The quiz lasted for an hour and a half and saw some great moments where the teams were going great guns. There were six battling rounds and the war was won by the best team. The entire event was hosted by Dr. Sumer K Sethi. </p>
                    <span>The final results were: </span><br>
                    <span>The Winner: </span><br>
                    <span>NORTH ZONE TEAM</span><br>
                    <span>Team from</span><br>
                    <span>All India Institute of Medical Sciences ( AIIMS ) New Delhi.</span><br>
                    <p>Devanshu Bansal</p>
                    <p>Devika Kir</p>
                    <p>THE PRIZE: The winners went home with a cash prize of Rs.50,000/-(Fifty thousand only) and FREE ADMISSION in any course offered by DAMS. They also lifted the 'WINNER' trophy. </p>
                  </div>
                  <div class="idams-box"> <span>1st runner up: </span><br>
                    <span>East Zone Team</span><br>
                    <span>Team from</span><br>
                    <span>Veer Surendra Sai Medical Collage &amp; Hospital, Sambalpur, Odisha.</span><br>
                    <p>Deepamjyoti Nanda</p>
                    <p>Nalinkanta Ghosh</p>
                    <p>THE PRIZE: The first runner up team went home with a cash prize of Rs. 25,000/ -(Twenty five thousand only) and admission to any course offered by DAMS at a discount of 50% on course fees. They also lifted the 1st Runner-Up Trophy. </p>
                  </div>
                  <div class="idams-box"> <span>2nd runner up: </span><br>
                    <span>West Zone Team</span><br>
                    <span>Team from</span><br>
                    <span>M.P. Shah Medical Collage, Jamnager. </span><br>
                    <p>Ankit Madan</p>
                    <p>Shubham Dilipbhai Maheshwari</p>
                    <p>THE PRIZE: The second runner up team went home with a cash prize of Rs. 15,000/-(Fifteen thousand only) and admission to any course offered by DAMS at a discount of 25% on course fees. They also lifted the 2nd Runner-Up Trophy. </p>
                  </div>
                  <div class="idams-box"> <span>South Zone Team</span><br>
                    <span>Team from</span><br>
                    <span>Kasturba Medical Collage,Manipal,Karnatka</span><br>
                    <p>HIMAMSHU ACHARYA</p>
                    <p>ISHA GAMBHIR</p>
                    <p>They bagged the Participant Trophy for playing fair and making it to the Grand Finale, Season – 1. </p>
                    <span>Best Challenger Trophy</span>
                    <p>The Best Challenger Trophy for Season – 1 is won by SWETA SUBHADARSHINI of AIIMS. She has been giving a tough competition to the teams reaching the GRAND FINALE at all levels namely CAMPUS &amp; ZONAL ROUNDS. She missed the seat in the finals by a narrow margin. Congrats to her and all the best in life. PRIZE: She won the cash prize of Rs.10000/-(Ten thousand only) along with the "BEST CHALLENGER TROPHY"</p>
                  </div>
                  <ul class="main-students-list ">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="images/students/student2.jpg" alt="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" title="Dr. SHAIKH ABDUL FAHIM ABDUL RAHIM" />
                        <p><span>Dr. Devanshu Bansal</span> Rank: 1</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student5.jpg" alt="Student" title="Student" />
                        <p><span>Dr. Shweta Shubhdarshini</span> Rank: 3</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student3.jpg" alt="Dr. SHEJOL SUMIT" title="Dr. SHEJOL SUMIT" />
                        <p><span>Dr. Devika Kir</span> Rank: 2</p>
                      </div>
                      <div class="students-box"> <img src="images/students/student4.jpg" alt="Dr. ATUL JAIN" title="Dr. ATUL JAIN" />
                        <p><span>Dr. Anurag Chahal</span> Rank: 3</p>
                      </div>
                    </li>
                  </ul>
                  <div class="schedule-mini-series  t-space-20">
                    <h4 class="sb-10 f-size25">ALL INDIA INSTITUTE OF MEDICAL SCIENCES NEW DElHI</h4>
                    <div class="schedule-mini-top"> <span class="two-part">Student Name</span> <span class="three-part">Marks</span> <span class="one-part">Rank</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"> <span class="two-parts schedule-left-line">Devanshu Bansal</span> <span class="one-parts">40</span> <span class="three-part">1</span> </li>
                        <li> <span class="two-parts schedule-left-line">Devika Kir</span> <span class="one-parts">34</span> <span class="three-part">2</span> </li>
                        <li> <span class="two-parts schedule-left-line">Arjung Gupta</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Shweta Shubhdarshini</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Sarita Kumari</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Anurag Chahal</span> <span class="one-parts">33</span> <span class="three-part">3</span> </li>
                        <li> <span class="two-parts schedule-left-line">Avin Goel</span> <span class="one-parts">32</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">Kaustav Majumder</span> <span class="one-parts">32</span> <span class="three-part">4</span> </li>
                        <li> <span class="two-parts schedule-left-line">Savinay Kapur</span> <span class="one-parts">31</span> <span class="three-part">5</span> </li>
                        <li> <span class="two-parts schedule-left-line">Rituraj Upadhyay</span> <span class="one-parts">31</span> <span class="three-part">5</span> </li>
                        <li> <span class="two-parts schedule-left-line">Manpreet Uppal</span> <span class="one-parts">30</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">Nishant Gurnani</span> <span class="one-parts">30</span> <span class="three-part">6</span> </li>
                        <li> <span class="two-parts schedule-left-line">Barun Bagga</span> <span class="one-parts">29</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">Sahil Gupta</span> <span class="one-parts">29</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">Devesh Kumawat</span> <span class="one-parts">29</span> <span class="three-part">7</span> </li>
                        <li> <span class="two-parts schedule-left-line">Harshit Garg</span> <span class="one-parts">28</span> <span class="three-part">8</span> </li>
                        <li> <span class="two-parts schedule-left-line">Vineet Kumar</span> <span class="one-parts">28</span> <span class="three-part">8</span> </li>
                        <li> <span class="two-parts schedule-left-line">Varsha Mathews</span> <span class="one-parts">24</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">Neethu</span> <span class="one-parts">24</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">Ahmed Nawid Latifi</span> <span class="one-parts">24</span> <span class="three-part">9</span> </li>
                        <li> <span class="two-parts schedule-left-line">ANUJ A</span> <span class="one-parts">20</span> <span class="three-part">10</span> </li>
                        <li> <span class="two-parts schedule-left-line">Mandeep Singh Virk</span> <span class="one-parts">18</span> <span class="three-part">11</span> </li>
                      </ul>
                    </div>
                  </div>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                </article>
              </ul>
            </div>
          </div>
     </div>-->
        </div>
      </div>
   
    <div class="news-update-box" style="margin-top:25px;">
        <div class="n-videos"><span></span> </div>
        <div class="videos-content-box">
          <!--<div id="vd0" class="display_block" >-->
            <!--<iframe width="100%" height="390" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>-->
          <!--</div>-->
<!--          <div id="vd1" class="display_none" >
            <iframe width="100%" height="390" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd2" class="display_none" >
            <iframe width="100%" height="390" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd3" class="display_none" >
            <iframe width="100%" height="390" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
          </div>-->
        </div>
        <div class="box-bottom-1" >
<!--            <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="photo-gallery.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> -->
        </div>
      </div>
</div>

<?php // include 'middle-accordion.php'; ?>
<?php // include 'recentAchievement.php'; ?>


<!--      <div class="course-box">
        <h3>National Quiz</h3>
        <div class="franchisee-box paddin-zero">
          <p>As you must be aware that Delhi Academy of Medical Sciences is organizing a NATIONAL quiz project “GREY MATTER – DAMS National Quiz” being one of its kind medical quiz program. The project is intended to find and honor the best medical brain in the country.</p>
          <p>This mega event kicked off recently, with two invites already sent to your respective colleges. You must have been informed of the same by now. It is going to provide an opportunity to medical students to enrich themselves by our experiences and further help them to explore the hidden talent in them.</p>
          <p>It is your chance to participate and win fabulous prizes.</p>
        </div>
      </div>-->
      
<!--      <div class="recent-pg-main">
        <div class="span12">
          <div class="recent-pg-heading1"> <a href="photo-gallery.php" class="photogal photogal_inline"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a></div>
          <div class="customNavigation"> <a class="btn prev" href="javascript:void(0);"><img src="images/left.png" alt="Left" /></a> <a class="btn next" href="javascript:void(0);"><img src="images/right-inactive.png" alt="Right" /></a> </div>
        </div>
        <div id="owl-demo" class="owl-carousel" style="margin-left:4px;">
          <div class="item"> <img src="images/topper/1.jpg" alt="Toppers" /> <span class="_name">Dr. Saba Samad Memon</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">AIPGE (2014)</span> </div>
          <div class="item"> <img src="images/topper/2.jpg" alt="Toppers" /> <span class="_name">Dr. Mayank Agarwal</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">ASSAM PG (2014)</span> </div>
          <div class="item"> <img src="images/topper/3.jpg" alt="Toppers" /> <span class="_name">Dr. Archana Badala</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">RAJASTHAN CET (2014)</span> </div>
          <div class="item"> <img src="images/topper/4.jpg" alt="Toppers" /> <span class="_name">Dr. Rahul Baweja</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">DELHI PG (2014)</span> </div>
          <div class="item"> <img src="images/topper/1-.jpg" alt="Toppers" /> <span class="_name">Dr. Saba Samad Memon</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">MAHCET (2013-14)</span> </div>
          <div class="item"> <img src="images/topper/5.jpg" alt="Toppers" /> <span class="_name">Dr. Tuhina Goel</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">AIIMS</span> </div>
          <div class="item"> <img src="images/topper/4.jpg" alt="Toppers" /> <span class="_name">Dr. Rahul Baweja</span> <span class="_rank">Rank : <strong>2</strong></span> <span class="_field">DNB CET (2013)</span> </div>
          <div class="item"> <img src="images/topper/6.jpg" alt="Toppers" /> <span class="_name">Dr. Pooja jain</span> <span class="_rank"><strong>Rank 5</strong></span> <span class="_field">DNB CET (2013)</span> </div>
        </div>
        <div class="view-all-photo"><a href="aipge_2014.php" title="View All">View&nbsp;All</a></div>
      </div>-->

    </aside>
      
    <aside class="content-right">
     <div class="signup-wrap">
         <div class="group-btn">
         <span style="margin-top: 0px;" class="book-ur-seat-btn">
             <a href="http://quiztest.damsdelhi.com/" title="Book Your Seat" target="_blank" style="font-size: 16px;">  Sign Up <span style="float:none;">&nbsp;</span></a>
             <a href="http://quiztest.damsdelhi.com/" title="Book Your Seat" target="_blank" style="font-size: 16px;">  Sign in <span style="float:none;">&nbsp;</span></a>
         </span>
         </div>
         </div>
        <div class="national-quiz-add">
<div  align="center" style="border: 1px solid rgb(204, 204, 204); padding: 5px; box-sizing: border-box;"><img style="width:100%;" src="images/GeryMatterLogo.jpg"></div>
<div style="margin-top: 15px;border: 1px solid rgb(204, 204, 204); padding: 5px; box-sizing: border-box;" align="center"><img style="width:100%;" src="images/DentalLogo.jpg"></div>
</div>
      <?php // include 'national-quiz-accordion.php'; ?>
      <?php
include 'openconnection.php';
$count = 0;
$i = 0;
$sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
while ($row = mysql_fetch_array($sql)) {
    $newsDetail[$count] = urldecode($row['HEADING']);
    $count++;
}
?>
        <div class="news-update-box" style="display: none;">
        <div class="n-heading"><span></span> News &amp; Updates</div>
        <div class="news-content-box">
          <div class="news_content_inline">
            <?php
                        $j = 0;
                        $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) {
                        ?>
            <ul id="ul<?php echo $i; ?>" <?php if ($i == '0') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php if ($newsDetail[$j] != '') { ?>
            <li class="orange"> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            <?php if ($newsDetail[$j] != '') { ?>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            </ul>
            <?php
                        }
                        ?>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
            <div class="slider-mini-dot">
              <ul>
                <?php $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) { ?>
                <li id="u<?php echo $i; ?>" <?php if ($i == '0') { ?> class="current" <?php } ?> onClick="news('<?php echo $i; ?>',<?php echo ceil($count / 3); ?>);"></li>
                <?php } ?>
                <!--                    <li id="u1" class="" onClick="news('1');"></li>
                                                        <li id="u2" class="" onClick="news('2');"></li>-->
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>        
      </div>
<!--      <div class="news-update-box">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd1" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd2" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd3" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="photo-gallery.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>-->
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<div style="display: none ;">
<div class="homeloader">
 <div id="onloadPopup" class="onloadPopup home_page">
    <div class="imgholder">
<a target="_blank" ><img onclick="hide1()" src="https://www.damsdelhi.com/images/damsposter21dec.jpg"></a>      <a href="javascript:void(0)" id="onloadPopup-close" onclick="hide1()"><img src="https://www.damsdelhi.com/images/crossbutton.png"></a>
    </div>
</div>
</div>
<div id="blackPopup1" class="blackPopup"></div>
 
 </div>  
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="gallerySlider/owl.carousel.js"></script>
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
      
});
</script>
<script type="text/javascript">
  function hide1(){ $("#onloadPopup,#blackPopup1").fadeOut(700)();
//$("#onloadPopup,#blackPopup")fadeOut(2000);
//$(".ui").hide(1000);
}
    $(document).ready(function(){
  
    $("#left").addClass('appicon_show');
	$("#left").removeClass("appicon_hide");
	
    });
  function hideRight(){
	$("#left").removeClass("appicon_show");
	$("#left").addClass("appicon_hide");
	}
</script>
<script>
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 5000); // Change image every 2 seconds
}
</script>
</body>
</html>