<!DOCTYPE html>
<?php
$course_id = '1';
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Coaching for PG Medical Entrance Exam, National Quiz</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="./gallerySlider/owl.carousel.js"></script>
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="national-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h3>National Quiz<span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-midle-content">
  <div class="wrapper">
      <aside class="content-left">
<div class="course-box">
    <span style="margin-top: 0px;" class="book-ur-seat-btn"><a href="https://quiztest.damsdelhi.com/" title="Book Your Seat" target="_blank" style="font-size: 16px;"> <span>&nbsp;</span> Sign Up</a></span>
<ul class="idTabs responce-show">
<li><a href="#MDCourses">GREY MATTER – DAMS National Quiz</a></li>
<li><a href="#DNB">Quest National Dental Quiz</a></li>
</ul>

<div id="MDCourses">
<div class="satellite-content">
<div class="idams-box1">
<!--<h3>National Quiz</h3>-->
<p>As you must be aware that Delhi Academy of Medical Sciences is organizing a NATIONAL quiz project “GREY MATTER – DAMS National Quiz” being one of its kind medical quiz program. The project is intended to find and honor the best medical brain in the country.</p>
          <p>This mega event kicked off recently, with two invites already sent to your respective colleges. You must have been informed of the same by now. It is going to provide an opportunity to medical students to enrich themselves by our experiences and further help them to explore the hidden talent in them.</p>
          <p>It is your chance to participate and win fabulous prizes.</p></div>
</div></div>

<div id="DNB">
<div class="satellite-content">
<div class="idams-box1">
<p>“QUEST – DAMS NATIONAL DENTAL QUIZ” is one of its kind DENTAL quiz
program. The project is intended to find and honor the best brain in
the country pursuing dentistry. The project is an initiative to
mainstream the idea of “Dentistry is learning” in existing dental
medicos, by awareness generation among students; creating empirical
evidence of positive outcomes and advocating it through multi tier
quiz contest.</p>
</div>

<aside class="how-to-apply paddin-zero">
<!--<div class="how-to-apply-heading"><span></span> Quiz Highlights</div>-->
<div class="course-detail-content">

<span class="gry-course-box">This project is being implemented nationwide in 4 zones all over India
i.e. EAST; WEST; NORTH; SOUTH. Around 300 dental colleges all over
India will be invited to participate in the quiz. However, only one
team will emerge as the winner of the Grand Prize of Rs. 30,000/-(Rs.
Thirty thousand only).
</span>

<span class="blue-course-box">You will be taught by well qualified and experienced teachers who care about you. Basic concepts to advanced level will be taught in a simple and easy to understand manner. Our teachers are specialists who have been teaching with us for long time now and know the current trend of questions. They have an uncanny ability to predict the questions which nobody else can. They know the current trends and advancement in their fields and often update the students of the facts which they themselves cannot know from routine books..</span>

<span class="gry-course-box">We organize this National quiz to seek and facilitate young talented
brains studying dentistry, which shall further consolidate their
learning through such ventures. It is going to provide an opportunity
to students to enrich themselves by our experiences and further help
them to explore the hidden talent in them.</span>

<span class="blue-course-box">Another fact that needs to be applauded is that at any level of quiz,
participants are not required to pay any amount towards the quiz. All
administrative costs etc are borne by DAMS.</span>

<span class="gry-course-box">All the Dental Colleges recognized by DCI can participate in this
national quiz event.<br>
All the students in FINAL Year in BDS can participate in this mega event.<br>
All INTERNS are also entitled to participate.</span>

<span class="blue-course-box"><strong>FIRST PRIZE: </strong>A Cash prize of Rs.30,000/- (Thirty thousand only) to the Winner
And FREE ADMISSION IN ANY course offered by DAMS.<br></br>

<strong>SECOND PRIZE:</strong> A Cash prize of Rs. 20,000/-(Twenty thousand only) to
the first runner up AND 50% discount on course fees on admission in
any course offered by DAMS.<br></br>

<strong>THIRD PRIZE:</strong> A Cash prize of Rs. 10,000/-(Ten thousand only) to the
second runner up AND 25% discount on course fees on admission in any
course offered by DAMS.</span>

<span class="gry-course-box">This year, being the SEASON 4, the format of the quiz is going to
change from the conventional Round 1, Round 2 pattern to and online
quiz pattern. Very soon a notification regarding the same would be
made available on our website.<br></br>

So, all the best to all talented young dentists of the country.<br></br>

Get ready to be a part of this prestigious national event.<br></br>

All the best!!!</span>
</div>
</aside>
</div>
</div>
</div>

<?php // include 'middle-accordion.php'; ?>
<?php // include 'recentAchievement.php'; ?>


<!--      <div class="course-box">
        <h3>National Quiz</h3>
        <div class="franchisee-box paddin-zero">
          <p>As you must be aware that Delhi Academy of Medical Sciences is organizing a NATIONAL quiz project “GREY MATTER – DAMS National Quiz” being one of its kind medical quiz program. The project is intended to find and honor the best medical brain in the country.</p>
          <p>This mega event kicked off recently, with two invites already sent to your respective colleges. You must have been informed of the same by now. It is going to provide an opportunity to medical students to enrich themselves by our experiences and further help them to explore the hidden talent in them.</p>
          <p>It is your chance to participate and win fabulous prizes.</p>
        </div>
      </div>-->
      <div class="pg-medical-main tab-hide">
        <div class="pg-heading"><span></span>National Quiz Courses</div>
        <div class="course-new-section">
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('1');"><span class="plus-ico" id="s1"></span>Classroom Course</h5>
            <div class="coures-list-box-content"  id="di1" style="display:none;">
              <ul class="course-new-list">
                <li><a href="gray-matter.php" title="Concept of Grey Matter"><span class="sub-arrow"></span>Concept of Grey Matter</a></li>
                <li><a href="gray-matter-testimonial.php" title="Testimonials"><span class="sub-arrow"></span>Testimonials</a></li>
              </ul>
            </div>
          </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('2');"><span class="plus-ico" id="s2"></span>2012</h5>
            <div class="coures-list-box-content" id="di2" style="display:none;">
              <ul class="course-new-list">
                <li><a href="round1.php" title="1st Campus Round 2012"><span class="sub-arrow"></span>1st Campus Round 2012</a></li>
                <li><a href="round2.php" title="2nd Campus Round 2012"><span class="sub-arrow"></span>2nd Campus Round 2012</a></li>
                <li><a href="grand.php" title="Grand Finale 2012"><span class="sub-arrow"></span>Grand Finale 2012</a></li>
              </ul>
            </div>
          </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('3');"><span class="plus-ico" id="s3"></span>2013</h5>
            <div class="coures-list-box-content" id="di3" style="display:none;">
              <ul class="course-new-list">
                <li><a href="season2round1.php" title="1st Campus Round 2013"><span class="sub-arrow"></span>1st Campus Round 2013</a></li>
                <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
                <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>
              </ul>
            </div>
          </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('4');"><span class="plus-ico" id="s4"></span>2014</h5>
            <div class="coures-list-box-content" id="di4" style="display:none;">
              <ul class="course-new-list">
                <li><a href="aiims-delhi.php" title="1st Campus Round 2014"><span class="sub-arrow"></span>1st Campus Round 2014</a></li>
<!--                <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
                <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>-->
              </ul>
            </div>
          </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('5');"><span class="plus-ico" id="s5"></span>2015</h5>
            <div class="coures-list-box-content" id="di5" style="display:none;">
              <ul class="course-new-list">
                <!--<li><a href="aiims-delhi.php" title="1st Campus Round 2015"><span class="sub-arrow"></span>1st Campus Round 2015</a></li>-->
                  <li><a href="national_2015.php" title="1st Campus Round 2015"><span class="sub-arrow"></span>1st Campus Round 2015</a></li>
                 <li><span class="sub-arrow"></span>The Grand Finale of season 4 of Grey Matter DAMS National Quiz got over in PULSE 2015. </li>
                <li><span class="sub-arrow"></span>The Grand Finale was held in Conference Hall of JLN Auditorium, AIIMS campus, New Delhi on 17th September 2015.</li>
                <li><span class="sub-arrow"></span>This year too the winning team was from AIIMS, New Delhi.</li>
                <li><span class="sub-arrow"></span>The first runners up trophy was lifted by team from SMS Medical College, Jaipur.</li>
                <li><span class="sub-arrow"></span>The second runners up were the team from Govt. Medical College, Imphal, Manipur.</li>
                <li><span class="sub-arrow"></span>It was a nail-biting final, and all the teams had a great contest till the deciding moments.</li>
                <li><span class="sub-arrow"></span>Congratulations!! To all winning teams teams. </li>

              </ul>
            </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('6');"><span class="plus-ico" id="s6"></span>2016</h5>
            <div class="coures-list-box-content" id="di6" style="display:none;">
              <ul class="course-new-list">
                 
<!--                <li><a href="national_2016.php" title="1st Campus Round 2016"><span class="sub-arrow"></span>1st Campus Round 2016</a></li>-->
                <li><a href="national_2016.php" title="1st Campus Round 2016"><span class="sub-arrow"></span>1st Campus Round 2016</a></li>
                <li><span class="sub-arrow"></span>The great Grand Finale of Grey Matter DAMS National Quiz, Season 5 took place on 13 Nov 2016.</li>
                <li><span class="sub-arrow"></span>The four teams were from four zones of the country.</li>
                <li><span class="sub-arrow"></span>The quiz was telecasted live on YouTube and Facebook. It was watched by more than 10000 doctors all over India. LATEST ADDITION TO THE CHAIN OF EVENTS  THAT DAMS HAS PIONEERED IN INDIA. Another plume in the hat. </li>
                <li><span class="sub-arrow"></span>The quiz format was unique and was presented by 4 quiz masters namely Dr. Sumer, Dr. Deepti, Dr. Sachin Garg, Dr. Siddharth Mishra.</li>
                <li><span class="sub-arrow"></span>The quiz lasted for 2 hrs and at the end of it teams were longing for more. </li>
                <li><span class="sub-arrow"></span>The second runners up was the team from R G Kar Medical College, Kolkata. Team represented by Subodh Shankar Behera & Indrashish Chaudhury.</li>
                <li><span class="sub-arrow"></span>They won a cash prize of Rs.15000/- and a discount of 50% in course fees for any DAMS course, valid nationwide.</li>
                <li><span class="sub-arrow"></span>The first runners up was the team from Thanjavur med college, team represented by M Sethuraman & Ragul Ajay BG. They won a cash prize of Rs.25000/- and 100% discount on course fees for any DAMS course, valid nationwide.</li>
                <li><span class="sub-arrow"></span>The WINNERS for the season 5 of this prestigious quiz were the students from AIIMS, New Delhi, represented by Umang Arora & Shubham Agarwal.</li>
                <li><span class="sub-arrow"></span>They lifted the WINNERS trophy and won a cash prize of Rs.50000/-(Fifty Thousand) and 100% discount on course fees.</li>
                </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="recent-pg-main">
        <div class="span12">
          <div class="recent-pg-heading1"> <a href="photo-gallery.php" class="photogal photogal_inline"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a></div>
          <div class="customNavigation"> <a class="btn prev" href="javascript:void(0);"><img src="images/left.png" alt="Left" /></a> <a class="btn next" href="javascript:void(0);"><img src="images/right-inactive.png" alt="Right" /></a> </div>
        </div>
        <div id="owl-demo" class="owl-carousel" style="margin-left:4px;">
          <div class="item"> <img src="images/topper/1.jpg" alt="Toppers" /> <span class="_name">Dr. Saba Samad Memon</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">AIPGE (2014)</span> </div>
          <div class="item"> <img src="images/topper/2.jpg" alt="Toppers" /> <span class="_name">Dr. Mayank Agarwal</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">ASSAM PG (2014)</span> </div>
          <div class="item"> <img src="images/topper/3.jpg" alt="Toppers" /> <span class="_name">Dr. Archana Badala</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">RAJASTHAN CET (2014)</span> </div>
          <div class="item"> <img src="images/topper/4.jpg" alt="Toppers" /> <span class="_name">Dr. Rahul Baweja</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">DELHI PG (2014)</span> </div>
          <div class="item"> <img src="images/topper/1-.jpg" alt="Toppers" /> <span class="_name">Dr. Saba Samad Memon</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">MAHCET (2013-14)</span> </div>
          <div class="item"> <img src="images/topper/5.jpg" alt="Toppers" /> <span class="_name">Dr. Tuhina Goel</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">AIIMS</span> </div>
          <div class="item"> <img src="images/topper/4.jpg" alt="Toppers" /> <span class="_name">Dr. Rahul Baweja</span> <span class="_rank">Rank : <strong>2</strong></span> <span class="_field">DNB CET (2013)</span> </div>
          <div class="item"> <img src="images/topper/6.jpg" alt="Toppers" /> <span class="_name">Dr. Pooja jain</span> <span class="_rank"><strong>Rank 5</strong></span> <span class="_field">DNB CET (2013)</span> </div>
        </div>
        <div class="view-all-photo"><a href="aipge_2014.php" title="View All">View&nbsp;All</a></div>
      </div>
    </aside>
      
    <aside class="content-right">
        <div class="national-quiz-add">
<div  align="center" style="border: 1px solid rgb(204, 204, 204); padding: 5px; box-sizing: border-box;"><img style="width:100%;" src="images/GeryMatterLogo.jpg"></div>
<div style="margin-top: 15px;border: 1px solid rgb(204, 204, 204); padding: 5px; box-sizing: border-box;" align="center"><img style="width:100%;" src="images/DentalLogo.jpg"></div>
</div>
      <?php // include 'national-quiz-accordion.php'; ?>
      <?php
include 'openconnection.php';
$count = 0;
$i = 0;
$sql = mysql_query("SELECT HEADING FROM NEWS WHERE COURSE_ID=$course_id AND ACTIVE=1 ORDER BY NEWS_ID DESC LIMIT 0,9");
while ($row = mysql_fetch_array($sql)) {
    $newsDetail[$count] = urldecode($row['HEADING']);
    $count++;
}
?>
      <div class="news-update-box">
        <div class="n-heading"><span></span> News &amp; Updates</div>
        <div class="news-content-box">
          <div class="news_content_inline">
            <?php
                        $j = 0;
                        $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) {
                        ?>
            <ul id="ul<?php echo $i; ?>" <?php if ($i == '0') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php if ($newsDetail[$j] != '') { ?>
            <li class="orange"> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            <?php if ($newsDetail[$j] != '') { ?>
            <li> <span></span>
              <p><?php echo $newsDetail[$j++]; ?></p>
            </li>
            <?php } ?>
            </ul>
            <?php
                        }
                        ?>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
            <div class="slider-mini-dot">
              <ul>
                <?php $i = 0;
                        for ($i = 0; $i < ceil($count / 3); $i++) { ?>
                <li id="u<?php echo $i; ?>" <?php if ($i == '0') { ?> class="current" <?php } ?> onClick="news('<?php echo $i; ?>',<?php echo ceil($count / 3); ?>);"></li>
                <?php } ?>
                <!--                    <li id="u1" class="" onClick="news('1');"></li>
                                                        <li id="u2" class="" onClick="news('2');"></li>-->
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>        
      </div>
      <div class="news-update-box">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd1" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd2" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
          </div>
          <div id="vd3" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="photo-gallery.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="gallerySlider/owl.carousel.js"></script>
<script>
$(document).ready(function() {
      var owl = $("#owl-demo");
      owl.owlCarousel();
   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })
});
</script>
</body>
</html>