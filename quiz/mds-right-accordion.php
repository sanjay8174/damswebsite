<script>
function sliddes3(val)
{
   var spp1=$('div.accordionButton > span').size();
   for(var d=1;d<=spp1;d++)
   {   
       if(val!=d)
	   { 
    	 $('#sq-mds'+d).removeClass('arrowup');
         $('#sq-mds'+d).addClass('arrowdwn');
		 $('#diMds'+d).slideUp(400);
	   }      
   }
   
   $('#diMds'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#sq-mds'+val).removeClass('arrowdwn');
            $('#sq-mds'+val).addClass('arrowup');
       }
	   else
	   {
			$('#sq-mds'+val).removeClass('arrowup');
            $('#sq-mds'+val).addClass('arrowdwn');
       }
   });
}
</script>

<div id="accor-wrapper">
  <div class="accordionButton" onclick="sliddes3('1')" style="margin:3px 0px 0px 0px;"><span class="arrowdwn" id="sq-mds1"></span>Classroom Course</div>
  <div class="accordionContent" id="diMds1" style="display: none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="dams-mds-quest-dental-regular-course.php" title="Regular Course"><span class="sub-arrow"></span> Regular Course</a></li>
          <li><a href="mds-test-discussion-course.php" title="Test &amp; Discussion"><span class="sub-arrow"></span> Test &amp; Discussion</a></li>
          <li><a href="mds-crash-course.php" title="Crash Course"><span class="sub-arrow"></span> Crash Course</a></li>
          <li><a href="dams-mds-test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddes3('2')"><span class="arrowdwn" id="sq-mds2"></span>Distance Learning Course</div>
  <div class="accordionContent" id="diMds2" style="display: none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="mds-postal-course.php" title="Postal Course"><span class="sub-arrow"></span> Postal Course</a></li>
          <li><a href="mds-postal-test-series.php" title="Postal Test Series"><span class="sub-arrow"></span> Postal Test Series</a></li>
          <li><a href="dams-publication.php?c=3" title="Online Test Series"><span class="sub-arrow"></span> Online Test Series</a></li>
        </ol>
      </ul>
    </div>
  </div>
</div>
