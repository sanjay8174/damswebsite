<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript">
$(document).ready(function(){		
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="dams-aboutus">
      <aside class="banner-left banner-left-postion">
        <h2>Bridging the gap in</h2>
        <h3 style="font-size:30px; padding-bottom:0px; padding-top:0px;">Medical Education by providing<br>
          Best Faculty &amp; Teaching Methodologies</h3>
        <h2 style="font-size:19px;">in Every City</h2>
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="https://damsdelhi.com/index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span></span>
        <ul>
          <li class="bg_none"><a href="dams.php" title="About Us">About Us</a></li>
          <li><a title="Mission &amp; Vision" class="active-link">Mission &amp; Vision</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Mission &amp; Vision</h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="idams-box">
                  <p>Delhi Academy of Medical Sciences (DAMS) is the only institute in the country which claims 85% success rate in PG entrance. Our mission is to ensure that every single student who joins us benefits to an extent that he comes out of the PG medical entrance in flying colors.  In all our efforts and ideas we sincerely try to ensure that you get the branch and institute of your choice.</p>
                  <p>&nbsp;</p>
                  <span style="text-align:center; display:block;">"GETTING INTO DAMS IS YOUR GATEWAY TO GETTING MD/MS SEAT OF YOUR CHOICE"
                  "WE TEACH SUCCESS AND WE ENSURE SUCCESS"</span> </div>
                <div class="idams-right-video" style="margin-bottom:20px;">
                  <iframe width="100%" height="436" src="//www.youtube.com/embed/3dS96gyIBWU" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="store-wrapper">
            <div class="dams-store-link"><span></span>About US</div>
            <div class="dams-store-content">
              <div class="inner-store">
                <ul>
                  <li class="border_none"><a href="dams.php" title="About DAMS">About DAMS</a></li>
                  <li><a href="dams_director.php" title="Director's Message">Director's Message</a></li>
                  <li><a href="about_director.php" title="About Director">About Director</a></li>
                  <li><a href="mission_vision.php" title="Mission &amp; Vision" class="active-store">Mission &amp; Vision</a></li>
                  <li><a href="dams_faculty.php" title="Our Faculty">Our Faculty</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!--for Enquiry popup  -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry popup  --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>
