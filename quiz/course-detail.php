<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="#" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left banner-left-postion">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <aside class="banner-right">
        <div class="banner-right-btns"> <a href="http://damspublications.com/" target="_blank" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>
        
        <!--shreya<div class="banner-right-btns"> <a href="dams-store.php" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a> <a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a> <a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a> </div>-->
      </aside>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li style="background:none;"><a href="https://mdms.damsdelhi.com/index.php?c=1&n=1" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Reguler Course" class="active-link">Reguler Course</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>Reguler Course</h4>
            <article class="showme-main">
              <aside class="course-icons">
                <div class="icons-1 icons-adjust"><span class="monitor"></span>Weekend<span>Classes</span></div>
                <div class="icons-2 icons-adjust"><span class="monitor"></span>Topic&nbsp;Wise <span>Tests</span></div>
                <div class="icons-3 icons-adjust"><span class="monitor"></span>Grand&nbsp;Tests<span>AIPG(NBE/NEET) Pattern&nbsp;Mock</span></div>
                <div class="icons-4 icons-adjust"><span class="monitor"></span>Dedicated<span>Study&nbsp;Material</span></div>
                <div class="icons-5 icons-adjust"><span class="monitor"></span>Revision<span>SWT</span></div>
              </aside>
              <aside class="course-detail">
                <p>Most popular pg course amongst students with comprehensive coverage of ALL SUBJECTS. We are the only medical coaching institute which teaches all subjects in manner that is required for your PG Medical Entrance examination. </p>
                <p>&nbsp;</p>
                <p>This course starts in Feburary and we have a fresh batch every month. Classes are held on weekends and are based on advanced audio-visual learning modules. This course further has a total of more than 50 subject wise tests and 24 Grand Test/Bounce Back tests to sharpen your skill for PG Medical Entrance Examination. We are the number 1 coaching institute for the PG medical entrance examinations AIPG(NBE/NEET) Pattern, AIIMS, PGI,  UPSC, DNB &amp;  MCI screening. DAMS provides specialized courses which are designed by experts in the respective fields lead by Dr. Sumer Sethi , who is a radiologist and was himself a topper in AIPG &amp; AIIMS before. We assure to provide best coaching for AIPG(NBE/NEET) Pattern, AIIMS PG entrance, and PGI Chandigarh by our sincere effort..</p>
              </aside>
              <aside class="how-to-apply">
                <div class="how-to-apply-heading"><span></span> Course Highlights</div>
                <div class="course-detail-content"> <span class="gry-course-box">We admit only limited number of students in each batch. You will receive excellent individual attention. This is a big advantage to you as you can learn better in a small class with limited students. Our class rooms are air conditioned and have state of the art audiovisual facilities </span> <span class="blue-course-box">You will be taught by You will be taught by well qualified and experienced teachers who care about you. Basic concepts to advanced level will be taught in a simple and easy to understand manner. Our teachers are specialists who have been teaching with us for long time now and know the current trend of questions. They have an uncanny ability to predict the questions which nobody else can.  They know the current trends and advancement in their fields and often update the students of the facts which they themselves cannot know from routine books and often such questions are asked in advanced examinations like AIIMS, AIPG(NBE/NEET) Pattern, and PGI Chandigarh...</span> <span class="gry-course-box">We provide detailed and easy to understand notes. Our notes are based on standard text books like Ganong, Williams, Harrison, and Robns pathology and are supplemented by question banks. These are as good as your text books in terms of authenticity and are extremely concise and easy to read. They are like concentrated protein mix required for your PG Medical Entrance Examination.</span> <span class="blue-course-box">We conduct periodic tests, evaluate your performance and conduct counselling sessions. This will help you in your regular preparation for your exams. In this course we provide practice with our monthly All India Grand Tests and Bounce Back Series. This course has a total of more than 50 subject wise tests and 24 Grand Test/Bounce Back tests to sharpen your skill for PG Medical Entrance examination.</span> </div>
              </aside>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <div id="accor-wrapper">
            <div class="accordionButton"><span></span>Interns/Post Intern Students</div>
            <div class="accordionContent" style="display: block;">
              <div class="inner-accor">
                <ul>
                  <li>Classroom Courses</li>
                  <ol>
                    <li><a href="regular_course_for_pg_medical.php" title="Regular Course (weekend)"><span class="sub-arrow"></span> Regular Course (weekend)</a></li>
                    <li><a href="t&d_md-ms.php" title="Test &amp; Discussion Course"><span class="sub-arrow"></span> Test &amp; Discussion Course</a></li>
                    <li><a href="crash_course.php" title="Crash Course"><span class="sub-arrow"></span> Crash Course</a></li>
                    <li><a href="combo-test-series.php" title="Combo Test Series"><span class="sub-arrow"></span> Combo Test Series</a></li>
                    <li><a href="mini-nlt-series.php" title="Mini Nlt Series"><span class="sub-arrow"></span> Mini Nlt Series</a></li>
                  </ol>
                  <li>Distant Learning Programme</li>
                  <ol>
                    <li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span> Postal Course</a></li>
                    <li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
                  </ol>
                  <li>Test Series</li>
                  <li>Online</li>
                  <ol>
                    <li><a href="http://registration.damsdelhi.com" target="_blank" title="Online Registration">Online Registration</a></li>
                  </ol>
                  <li>Offline</li>
                </ul>
              </div>
            </div>
            <div class="accordionButton"><span></span>Prefinal/Final Year Students</div>
            <div class="accordionContent" style="display: none;">
              <div class="inner-accor">
                <ul>
                  <li>Classroom Courses</li>
                  <ol>
                    <li><a href="foundation_course.php" title="Foundation Course"><span class="sub-arrow"></span> Foundation Course</a></li>
                    <li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span> Distant Learning Programme</a></li>
                    <li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
                    <li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
                  </ol>
                  <li>2nd Professional Students</li>
                  <li>Classroom Courses</li>
                  <ol>
                    <li><a href="prefoundation_course.php" title="Prefoundation Course"><span class="sub-arrow"></span> Prefoundation Course</a></li>
                  </ol>
                </ul>
              </div>
            </div>
            <div class="accordionButton"><span></span>Distant Learning Programme</div>
            <div class="accordionContent" style="display: none;">
              <div class="inner-accor">
                <ul>
                  <li>Tablet Based Course - IDAMS</li>
                  <li>Test Series</li>
                  <li>1st Professional Students</li>
                  <li>Classroom Courses</li>
                  <ol>
                    <li><a href="the_first_step.php" title="First Step Course"><span class="sub-arrow"></span> First Step Course</a></li>
                    <li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span> Distant Learning Programme</a></li>
                    <li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
                    <li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
                  </ol>
                </ul>
              </div>
            </div>
          </div>
          <div class="enquiry-main">
            <div class="enquiry-heading">Enquiry Form</div>
            <div class="enquiry-content-main">
              <div class="enquiry-content">
                <div class="enquiry-content-more">
                  <form action="" method="get">
                    <input name="" type="text" class="enquiry-input" value="Your Name" onfocus="if(this.value=='Your Name')this.value='';" onblur="if(this.value=='')this.value='Your Name';" />
                    <input name="" type="text" class="enquiry-input" value="Email Address" onfocus="if(this.value=='Email Address')this.value='';" onblur="if(this.value=='')this.value='Email Address';" />
                    <input name="" type="text" class="enquiry-input" value="Phone Number" onfocus="if(this.value=='Phone Number')this.value='';" onblur="if(this.value=='')this.value='Phone Number';" />
                    <select name="" class="select-input">
                      <option value="0">Select Course</option>
                      <option>MD/MS Entrance</option>
                      <option>MCI Screening</option>
                      <option>MDS Quest</option>
                      <option>USMLE Edge</option>
                    </select>
                    <select name="" class="select-input">
                      <option>Centre Interested</option>
                      <option>Centre Interested</option>
                      <option>Centre Interested</option>
                    </select>
                    <textarea name="" cols="" rows="3" class="enquiry-message"></textarea>
                    <div class="submit-enquiry"><a href="#" title="Submit"> <span></span> Submit</a></div>
                  </form>
                </div>
              </div>
              <div class="enquiry-bottom"></div>
            </div>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->

<?php include 'footer.php'; ?>

<!-- Footer Css End Here --> 
<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script> 
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->
</body>
</html>