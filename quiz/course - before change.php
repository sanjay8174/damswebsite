<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern PG</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/font-face.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->
</head>


<body class="inner-bg" onLoad="Menu.changeMenu(false);">

<?php include 'registration.php'; ?>

<!--for Quick Enquiry popup  -->
<?php include 'enquiry.php'; ?>
<!--for Quick Enquiry popup  -->
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
<div class="wrapper">
<article>

<div class="big-nav">
<ul>
<li class="face-face active"><a href="mdms_entrence/index.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="mdms_entrence/aiims_nov_2013.php" title="Achievement">Achievement</a></li>
</ul>
</div>

<aside class="banner-left">
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
<p>Ancient times Guru Shishya Pranali of imparting education was by <br />
the Guru at Guru's premises /Ashrams. Ages passed, social pattern<br />
changed, villages turned to towns, towns to cities &amp; metros.</p>
</aside>

<aside class="banner-right">
<div class="banner-right-btns">
<!--<a href="dams-store.php" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a>-->
    <a href="http://damspublications.com/" target="_blank" title="Dams Store"><span>&nbsp;</span>DAMS<b>Store</b></a>
<a href="find-center.php" title="Find a Center"><span>&nbsp;</span>Find&nbsp;a<b>Center</b></a>
<a href="photo-gallery.php" title="Virtual Tour"><span>&nbsp;</span>Virtual<b>Tour</b></a>
</div></aside>
</article>
</div>
</section> 

<!-- Banner End Here -->

<!-- Midle Content Start Here -->

<section class="inner-midle-content">
<div class="wrapper">

<aside class="content-left">
<div class="course-box">
<h3>MD/MS Courses</h3>
<p>Civilization progressed with "Education". For good education, students have to migrate to better places & this is the history as of yesterday. Now technology development removed these constraints and barriers. The Best Education of metros is available in House anywhere including the remote area. Quality education by teaching from the renowned professors and faculty is available at recipient's facilities. Best education at environment of individual's choice.
</p>
<p>The Best Education of metros is available in House anywhere including the remote area. Quality education by teaching from the renowned professors and faculty is available at recipient's facilities. Best education at environment of individual's choice.</p>
</div>


<div class="pg-medical-main">
<div class="pg-heading"><span></span>Face to Face Courses</div> 

<div class="course-new-section">

<div class="coures-list-box">
    <h5 class="h2toggle" onclick="sliddes('1');"><span class="plus-ico" id="s1"></span> Interns / Post Intern students</h5>
<div class="coures-list-box-content" id="di1" style="display:none;">
<ul class="course-new-list">
<h6>Classroom Courses</h6>
<li><a href="regular_course_for_pg_medical.php" title="Regular Course (weekend)"><span class="sub-arrow"></span>Regular Course (weekend)</a></li>
<li><a href="t&d_md-ms.php" title="Test &amp; Discussion Course"><span class="sub-arrow"></span>Test &amp; Discussion Course</a></li>
<li><a href="mdms_entrence/crash_index.php" title="Crash course"><span class="sub-arrow"></span>Crash course</a></li>
</ul>

<ul class="course-new-list">
<h6>Distant Learning Programme</h6>
<li><a href="postal.php" title="Postal course"><span class="sub-arrow"></span>Postal course</a></li>
<li><a href="idams.php" title="Tablet Based Course -IDAMS"><span class="sub-arrow"></span>Tablet Based Course -IDAMS</a></li>
</ul>

<ul class="course-new-list">
<h6>Test Series</h6>
<li><a href="#" title="Online"><span class="sub-arrow"></span>Online</a></li>
<li><a href="#" title="Offline"><span class="sub-arrow"></span>Offline</a></li>
</ul>



</div>

</div>

<div class="coures-list-box">
<h5 class="h2toggle" onclick="sliddes('2');"><span class="plus-ico" id="s2"></span> Prefinal/Final Year students</h5>
<div class="coures-list-box-content" id="di2" style="display:none;">
<ul class="course-new-list">
<h6>Classroom Courses</h6>
<li><a href="foundation_index.php" title="Foundation Course"><span class="sub-arrow"></span>Foundation Course</a></li>
<li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span>Distant Learning Programme</a></li>
<li><a href="idams.php" title="Tablet Based course - IDAMS"><span class="sub-arrow"></span>Tablet Based course - IDAMS</a></li>
<li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span>Test Series</a></li>
</ul>

</div></div>

<div class="coures-list-box">
<h5 class="h2toggle" onclick="sliddes('3');" ><span class="plus-ico" id="s3"></span> 2nd Professional Students</h5>
<div class="coures-list-box-content" id="di3" style="display:none;">
<ul class="course-new-list">
<h6>Classroom Courses</h6>
<li><a href="prefoundation_index.php" title="Prefoundation Course"><span class="sub-arrow"></span>Prefoundation Course</a></li>
<li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span>Postal Course</a></li>
<li><a href="idams.php" title="Tablet Based course - IDAMS"><span class="sub-arrow"></span>Tablet Based course - IDAMS</a></li>
<li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span>Test Series</a></li>
</ul>
</div></div>

<div class="coures-list-box">
<h5 class="h2toggle" onclick="sliddes('4');" ><span class="plus-ico" id="s4"></span> 1st Professional Students</h5>
<div class="coures-list-box-content" id="di4" style="display:none;">
<ul class="course-new-list">
<li><a href="the_first_step.php" title="First Step Course"><span class="sub-arrow"></span>First Step Course</a></li>
<li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span>Postal Course</a></li>
<li><a href="idams.php" title="Tablet Based course - IDAMS"><span class="sub-arrow"></span>Tablet Based course - IDAMS</a></li>
<li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span>Test Series</a></li>
</ul>
</div></div>

</div>

</div>


<div class="recent-pg-main">
<div class="recent-pg-heading">
<div class="slide-arrow" style="margin-right:20px;">
<a class="left" id="next">#</a>
<a class="right" id="prev">#</a>
</div>
<a href="photo-gallery.php" class="photogal photogal_title"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a></div>

<ul id="recent recent_inline">
<li id="li0">
<img src="images/topper/1.jpg" title="Topper" alt="Topper"/> 
<span class="_name">Dr. Saba Samad Memon</span>
<span class="_rank">Rank : <strong>1</strong></span>
<span class="_field">AIPGE (2014)</span>
</li>
<li id="li1">
<img src="images/topper/2.jpg" title="Topper" alt="Topper"/> 
<span class="_name">Dr. Mayank Agarwal</span>
<span class="_rank">Rank : <strong>1</strong></span>
<span class="_field">ASSAM PG (2014)</span>
</li>
<li id="li2">
<img src="images/topper/3.jpg" title="Topper" alt="Topper"/> 
<span class="_name">Dr. Archana Badala</span>
<span class="_rank">Rank : <strong>1</strong></span>
<span class="_field">RAJASTHAN CET (2014)</span>
</li>
<li id="li3">
<img src="images/topper/4.jpg" title="Topper" alt="Topper"/> 
<span class="_name">Dr. Rahul Baweja</span>
<span class="_rank">Rank : <strong>1</strong></span>
<span class="_field">DELHI PG (2014)</span>
</li>
<li id="li4">
<img src="images/topper/1.jpg" title="Topper" alt="Topper"/> 
<span class="_name">Dr. Saba Samad Memon</span>
<span class="_rank">Rank : <strong>1</strong></span>
<span class="_field">MAHCET (2013-14)</span>
</li>
<li id="li5">
<img src="images/topper/5.jpg" title="Topper" alt="Topper"/> 
<span class="_name">Dr. Tuhina Goel</span>
<span class="_rank">Rank : <strong>1</strong></span>
<span class="_field">AIIMS</span>
</li>
<li id="li6">
<img src="images/topper/4.jpg" title="Topper" alt="Topper"/> 
<span class="_name">Dr. Rahul Baweja</span>
<span class="_rank">Rank : <strong>2</strong></span>
<span class="_field">DNB CET (2013)</span>
</li>
<li id="li7">
<img src="images/topper/6.jpg" title="Topper" alt="Topper"/> 
<span class="_name">Dr. Pooja jain</span>
<span class="_rank"><strong>Rank 5</strong></span>
<span class="_field">DNB CET (2013)</span>
</li>
</ul>

<div class="view-all-photo"><a href="mdms_entrence/aiims_nov_2013.php" title="View All">View&nbsp;All</a></div>

</div>





</aside>

<aside class="content-right">

<div id="accor-wrapper">
<div class="accordionButton" style="margin-top:0px;"><span></span>Interns/Post Intern Students</div>
<div class="accordionContent">
<div class="inner-accor">
<ul>
<li>Classroom Courses</li>
<ol>
<li><a href="regular_course_for_pg_medical.php" title="Regular Course (weekend)"><span class="sub-arrow"></span> Regular Course (weekend)</a></li>
<li><a href="t&d_md-ms.php" title="Test &amp; Discussion Course"><span class="sub-arrow"></span> Test &amp; Discussion Course</a></li>
<li><a href="mdms_entrence/crash_index.php" title="Crash Course"><span class="sub-arrow"></span> Crash Course</a></li>
</ol>

<li>Distant Learning Programme</li>
<ol>
<li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span> Postal Course</a></li>
<li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
</ol>

<li>Test Series</li>
<ol>
<li><a href="#" title="Online"><span class="sub-arrow"></span> Online</a></li>
<li><a href="#" title="Offline"><span class="sub-arrow"></span> Offline</a></li>
</ol>
</ul>
</div>
</div>
<div class="accordionButton"><span></span>Prefinal/Final Year Students</div>
<div class="accordionContent">
<div class="inner-accor">
<ul>
<li>Classroom Courses</li>
<ol>
<li><a href="foundation_index.php" title="Foundation Course"><span class="sub-arrow"></span> Foundation Course</a></li>
<li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span> Distant Learning Programme</a></li>
<li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
<li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
</ol> 
</ul>
</div>
</div>

<div class="accordionButton"><span></span>2nd Professional Students</div>
<div class="accordionContent">
<div class="inner-accor">
<ul>
<li>Classroom Courses</li>
<ol>
<li><a href="prefoundation_index.php" title="Prefoundation Course"><span class="sub-arrow"></span> Prefoundation Course</a></li>
<li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span> Postal Course</a></li>
<li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
<li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
</ol>
</ul>
</div></div>

<div class="accordionButton"><span></span>1st Professional Students</div>
<div class="accordionContent">
<div class="inner-accor">
<ul>
<ol>
<li><a href="the_first_step.php" title="First Step Course"><span class="sub-arrow"></span> First Step Course</a></li>
<li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span> Postal Course</a></li>
<li><a href="idams.php" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span> Tablet Based Course - IDAMS</a></li>
<li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span> Test Series</a></li>
</ol>
</ul>
</div></div>



</div>


<div class="national-quiz-add">
<a href="national.php" title="National Quiz"><img src="images/national-quiz.jpg"  /></a>
</div>


<div class="news-update-box">
<div class="n-heading"><span></span> News &amp; Updates</div>
<div class="news-content-box">
<ul id="ul0" style="display:block;">
<li><span></span>
<p>DAMS family proudly congrats Dr Nishtha Yadav, our class room foundation course student from Delhi for getting Rank 2 in AIIMS Nov 2012.</p></li>
<li class="orange"><span></span>
<p>Fabulous results by DAMSONIANS in AIIMS Nov2012- Many selections and Rankers- Congratulations.</p></li>
<li><span></span>
<p>DAMSONIANS Rock the AIIMS NOV2012-Once again proving us to be the leader in PG Medical Education.</p></li>
</ul>
<ul id="ul1" style="display:none;">
<li><span></span>
<p>DAMSONIANS Rock the AIIMS NOV2012-Once again proving us to be the leader in PG Medical Education.</p></li>
<li class="orange"><span></span>
<p>DAMS family proudly congrats Dr Nishtha Yadav, our class room foundation course student from Delhi for getting Rank 2 in AIIMS Nov 2012.</p></li>
<li><span></span>
<p>Fabulous results by DAMSONIANS in AIIMS Nov2012- Many selections and Rankers- Congratulations.</p></li>
</ul>
<ul id="ul2" style="display:none;">
<li><span></span>
<p>DAMSONIANS Rock the AIIMS NOV2012-Once again proving us to be the leader in PG Medical Education.</p></li>
<li class="orange"><span></span>
<p>Fabulous results by DAMSONIANS in AIIMS Nov2012- Many selections and Rankers- Congratulations.</p></li>
<li><span></span>
<p>DAMSONIANS Rock the AIIMS NOV2012-Once again proving us to be the leader in PG Medical Education.</p></li>
</ul>
</div>

<div class="box-bottom-1">
<span class="mini-view-right"></span>
<div class="mini-view-midle">
<a href="news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
<div class="slider-mini-dot"><ul>
<li id="u0" class="current" onClick="news('0');"></li>
<li id="u1" class="" onClick="news('1');"></li>
<li id="u2" class="" onClick="news('2');"></li>
</ul></div>
</div>
<span class="mini-view-left"></span>
</div>

</div>


<div class="news-update-box">
<div class="n-videos"><span></span> Students Interview</div>
<div class="videos-content-box">

<div id="vd0" style="display:block;" >
<iframe width="100%" height="236" src="//www.youtube.com/embed/Atat9dha9RI" frameborder="0" allowfullscreen></iframe>
</div>

<div id="vd1" style="display:none;" >
<iframe width="100%" height="236" src="//www.youtube.com/embed/HIP7wjjGuoM" frameborder="0" allowfullscreen></iframe>
</div>

<div id="vd2" style="display:none;" >
<iframe width="100%" height="236" src="//www.youtube.com/embed/L-AnijPFb-I" frameborder="0" allowfullscreen></iframe>
</div>

<div id="vd3" style="display:none;" >
<iframe width="100%" height="236" src="//www.youtube.com/embed/M7L2-zLb560" frameborder="0" allowfullscreen></iframe>
</div>

</div>


<div class="box-bottom-1">
<span class="mini-view-right"></span>
<div class="mini-view-midle">
<a href="mdms_entrence/aiims_nov_2013.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
<div class="slider-mini-dot"><ul>
<li id="v0" class="current" onClick="video('0');"></li>
<li id="v1" class="" onClick="video('1');"></li>
<li id="v2" class="" onClick="video('2');"></li>
<li id="v3" class="" onClick="video('3');"></li>
</ul></div>
</div>
<span class="mini-view-left"></span>
</div>


</div>


</aside>


</div>
</section>

<!-- Midle Content End Here -->



<!-- Footer Css Start Here -->

<?php include 'footer.php';?>

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="navigation/TweenMax.min.js" type="text/javascript"></script>  
<script src="js/navigation.js" type="text/javascript"></script>  
<!-- Others Packages -->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
 var m=0;
 var l=0;
 var n=0;
 var videointerval ='';
 var newsinterval = '';
 $(document).ready(function(){
   var count1=$("#recent li").length;
   $("#prev").click(function(){
	 if(m>0)
	 {
	   var j=m+3;
       $("#li"+j).hide('fast','linear');
	   m--;
	   $("#li"+m).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   $(".slide-arrow > a.left").css('background','url(images/right.png)');
	   if(m==0)
	   {
		 $(".slide-arrow > a.right").css('background','url(images/left.png)');   
	   }
	 }
   });
   $("#next").click(function(){	 
	 if(m<count1-4)
	 {
       $("#li"+m).hide('fast','linear');
	   var j=m+4;
	   $("#li"+j).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   m++;
	   if(m==(count1-4))
	   {
		 $(".slide-arrow > a.left").css('background','url(images/right-inactive.png)');
	   }
     }	 
   });
   
   jQuery(document).ready(function(){
	jQuery('div.accordionButton').click(function() {
		jQuery('div.accordionContent').slideUp('normal');	
		jQuery(this).next().slideDown('normal');
	});		
	jQuery("div.accordionContent").hide();
	});		

//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });
	
	
   window.onload = function()
   {	  
	    var count2=$(".videos-content-box > img").length;
	    var count3=$(".news-content-box > ul").length;
        function setvideo()
		{
		   l=l%count2;		
		   $("#vd"+l).fadeOut('slow','linear');
		   $("#v"+l).removeClass("current");
	       var j=(l+1)%count2;			
		   $("#vd"+j).fadeIn('slow','linear');
		   $("#v"+j).addClass("current");        	
	       l++;	
        }
	    //function setnews()
//		{
//		   n=n%count3;		
//		   $("#ul"+n).slideUp(100,'swing');
//		   $("#u"+n).removeClass("current");
//	       var j=(n+1)%count3;
//		   $("#ul"+j).slideDown(4000,'linear').animate({zindex:'2'}, 100);
//		   $("#u"+j).addClass("current");        	
//	       n++;	
//        }
     videointerval = setInterval(setvideo, 5000);
	 //newsinterval = setInterval(setnews, 9000);
   }
 });
 
 
 function news(val)
 {
   //setTimeout('newsinterval', 9000);
   if(val=='0')
   {	   
	  $("#ul1").hide();
	  $("#u1").removeClass("current"); 
	  $("#ul2").hide();
	  $("#u2").removeClass("current");
	  $("#ul0").slideDown(1000,'linear');
	  $("#u0").addClass("current");   
   }
   if(val=='1')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul2").hide();
	 $("#u2").removeClass("current");
	 $("#ul1").slideDown(1000,'linear');
	 $("#u1").addClass("current"); 
   }
   if(val=='2')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul1").hide();
	 $("#u1").removeClass("current");
	 $("#ul2").slideDown(1000,'linear');
	 $("#u2").addClass("current");
   }
  // n=val;
//   newsinterval = setInterval(setnews, 9000);  
 }
 
 function video(val)
 {
   setTimeout('videointerval', 5000);
   if(val=='0')
   {	   
      $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd0").fadeIn('fast','linear');
	  $("#v0").addClass("current");
   }
   if(val=='1')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd1").fadeIn('fast','linear');
	  $("#v1").addClass("current");
   }
   if(val=='2')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd2").fadeIn('fast','linear');
	  $("#v2").addClass("current");
   }
   if(val=='3')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").fadeIn('fast','linear');
	  $("#v3").addClass("current");
   }
   l=val;
   videointerval = setInterval(setnews, 5000);  
 }


function sliddes(val)
{
   var sp=$('.h2toggle > span').size();
   for(var d=1;d<=sp;d++)
   {
       if(val==d)
       {
           $('#s'+d).removeClass('plus-ico');
           $('#s'+d).addClass('minus-ico');
           $('#di'+d).slideDown();
       }
       else
       {
           $('#s'+d).removeClass('minus-ico');
           $('#s'+d).addClass('plus-ico');
           $('#di'+d).slideUp();
       }
   }
}


</script>


<!-- Footer Css End Here -->
</body>
</html>