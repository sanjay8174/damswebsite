<div class="social-icon">
<a href="https://www.facebook.com/damsdelhiho" target="_blank" title="Connect with us on Facebook" class="l-facebook">Facebook</a>
<a href="https://twitter.com/damsdelhi" target="_blank" title="DAMS on Twitter" class="l-twitter">Twitter</a>
<a href="https://in.linkedin.com/pub/dams-delhi/40/415/570" target="_blank" title="DAMS on Linkedin" class="l-mail">LinkedIn</a>
<a href="https://www.youtube.com/user/damsdelhi" target="_blank" title="Watch DAMS Videos" class="l-print">YouTube</a>
<a href="https://plus.google.com/u/0/110912384815871611420/about" target="_blank" title="Follow DAMS on Google+" class="l-share">Google Plus</a>
</div>
