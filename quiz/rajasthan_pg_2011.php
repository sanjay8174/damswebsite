<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="index.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement" class="a-achievement-active">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="https://damsdelhi.com/" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="index.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Achievement" class="active-link">Achievement</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li class="border_none" id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
                <ol class="achievment-inner display_none" id="aol1">
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_jun_2014.php" title="DNB June">DNB June</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2014.php" title="PGI May">PGI May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2014.php" title="AIIMS May">AIIMS May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipge_2014.php" title="AIPGME">AIPGME</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="UPPG_2014.php" title="UPPG">UPPG</a></li>
                </ol>
              </li>
              <li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
                <ol class="achievment-inner display_none" id="aol2">
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipge_2013.php" title="AIPGME">AIPGME</a></li>
                </ol>
              </li>
              <li id="accor3" onClick="ontab('3');"><span id="accorsp3" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
                <ol class="achievment-inner display_none" id="aol3">
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>
                </ol>
              </li>
              <li id="accor4" onClick="ontab('4');" class="light-blue-inner"><span id="accorsp4" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
                <ol class="achievment-inner display_block" id="aol4">
                  <li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
                  <li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading">
            <h4>Rajasthan PG 2011</h4>
            <section class="showme-main">
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="Rajesthan PG 2011">Rajasthan PG 2011</a></li>
                <li><a href="#official" title="Interview">Interview</a></li>
              </ul>
              <div id="jquery">
                <article class="interview-photos-section">
                  <div class="schedule-mini-series">
                    <div class="schedule-mini-top"> <span class="one-part">Rank</span> <span class="two-part">Student Name</span> <span class="three-part">&nbsp;</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"><span class="one-parts">1</span> <span class="two-parts schedule-left-line">Dr. ANKUR JINDAL , Dr. RAHUL MANGAL , Dr. JYOTI PRAKASH</span> </li>
                        <li><span class="one-parts">2</span> <span class="two-parts schedule-left-line">Dr. VIKAS , Dr. KALPESH CHOUDAHRY</span> </li>
                        <li><span class="one-parts">3</span> <span class="two-parts schedule-left-line">Dr. NAGARWAL , Dr. OIAS SAINI , Dr. SURENDRA SINGH BHAKAL</span> </li>
                        <li><span class="one-parts">4</span> <span class="two-parts schedule-left-line">Dr. DANNY , Dr. MEENAKSHI , Dr. BABITA</span> </li>
                        <li><span class="one-parts">5</span> <span class="two-parts schedule-left-line">Dr. RAGHUNATH , Dr. ALKA GOEL</span> </li>
                        <li><span class="one-parts">6</span> <span class="two-parts schedule-left-line">Dr. JITENDRA</span> </li>
                        <li><span class="one-parts">7</span> <span class="two-parts schedule-left-line">Dr. GANPAT</span> </li>
                        <li><span class="one-parts">8</span> <span class="two-parts schedule-left-line">Dr. JUHI BANSAL</span> </li>
                        <li><span class="one-parts">9</span> <span class="two-parts schedule-left-line">Dr. YOGESH CHOUDAHRY , Dr. SUREKHA MEENA</span> </li>
                        <li><span class="one-parts">13</span> <span class="two-parts schedule-left-line">Dr. UMESH MEENA</span> </li>
                        <li><span class="one-parts">14</span> <span class="two-parts schedule-left-line">Dr. MAHESH YADAV , Dr. DEEPAK GUPTA</span> </li>
                        <li><span class="one-parts">16</span> <span class="two-parts schedule-left-line">Dr. SATVEER YADAV , Dr. MAHESH KULDEEP , Dr. NEHA SEEHRA , Dr. ANURODH DADARWAL</span> </li>
                        <li><span class="one-parts">17</span> <span class="two-parts schedule-left-line">Dr. JIVIKA , Dr. HANSRAJ</span> </li>
                        <li><span class="one-parts">18</span> <span class="two-parts schedule-left-line">Dr. KUMKUM GUPTA , Dr. LALIT KISHOR</span> </li>
                        <li><span class="one-parts">20</span> <span class="two-parts schedule-left-line">Dr. JEETENDRA KUMAR , Dr. STANDER KUMAR KICCHER</span> </li>
                        <li><span class="one-parts">21</span> <span class="two-parts schedule-left-line">Dr. NAVGEET MATHUR</span> </li>
                        <li><span class="one-parts">22</span> <span class="two-parts schedule-left-line">Dr. NARESH KUMAR</span> </li>
                        <li><span class="one-parts">24</span> <span class="two-parts schedule-left-line">Dr. SUNIL</span> </li>
                        <li><span class="one-parts">27</span> <span class="two-parts schedule-left-line">Dr. MANOJ KUMAR TAYAGI</span> </li>
                        <li><span class="one-parts">28</span> <span class="two-parts schedule-left-line">Dr. NAVAIPG(NBE/NEET) Pattern KALA</span> </li>
                        <li><span class="one-parts">30</span> <span class="two-parts schedule-left-line">Dr. MANISH CHAUDHARY</span> </li>
                        <li><span class="one-parts">31</span> <span class="two-parts schedule-left-line">Dr. SUNIL SUTHAR</span> </li>
                        <li><span class="one-parts">33</span> <span class="two-parts schedule-left-line">Dr. SHWETA GUPTA</span> </li>
                        <li><span class="one-parts">37</span> <span class="two-parts schedule-left-line">Dr. SHALINI AGARWAL</span> </li>
                        <li><span class="one-parts">38</span> <span class="two-parts schedule-left-line">Dr. SMITA GUPTA</span> </li>
                        <li><span class="one-parts">39</span> <span class="two-parts schedule-left-line">Dr. NAVRATAN SUTHAR</span> </li>
                        <li><span class="one-parts">40</span> <span class="two-parts schedule-left-line">Dr. DEEPESH AGARWAL</span> </li>
                        <li><span class="one-parts">41</span> <span class="two-parts schedule-left-line">Dr. LAKSHIMI</span> </li>
                        <li><span class="one-parts">42</span> <span class="two-parts schedule-left-line">Dr. SAMEER JAGRWAL , Dr. CHANDRAKANTA , Dr. MANGILAL DEGANURA , Dr. SANGITA KEDIA</span> </li>
                        <li><span class="one-parts">44</span> <span class="two-parts schedule-left-line">Dr. KAMAL KANT GUPTA</span> </li>
                        <li><span class="one-parts">47</span> <span class="two-parts schedule-left-line">Dr. PAYAL AGARWAL</span> </li>
                        <li><span class="one-parts">50</span> <span class="two-parts schedule-left-line">Dr. MUKESH</span> </li>
                        <li><span class="one-parts">53</span> <span class="two-parts schedule-left-line">Dr. HIMANSHU AWASTHI</span> </li>
                        <li><span class="one-parts">54</span> <span class="two-parts schedule-left-line">Dr. ASHOK SHARMA</span> </li>
                        <li><span class="one-parts">56</span> <span class="two-parts schedule-left-line">Dr. LALIT RATHI</span> </li>
                        <li><span class="one-parts">57</span> <span class="two-parts schedule-left-line">Dr. SHALINI AGGARWAL , Dr. MUKES KUMAR GUPTA</span> </li>
                        <li><span class="one-parts">59</span> <span class="two-parts schedule-left-line">Dr. SHALINI GARG</span> </li>
                        <li><span class="one-parts">62</span> <span class="two-parts schedule-left-line">Dr. MAHENDRA KUMAR LOHAR , Dr. LOKESH YADAV</span> </li>
                        <li><span class="one-parts">66</span> <span class="two-parts schedule-left-line">Dr. RAJURAM BISHNOI</span> </li>
                        <li><span class="one-parts">68</span> <span class="two-parts schedule-left-line">Dr. SAURAV SHARMA</span> </li>
                        <li><span class="one-parts">69</span> <span class="two-parts schedule-left-line">Dr. AVNEESH KHARE</span> </li>
                        <li><span class="one-parts">73</span> <span class="two-parts schedule-left-line">Dr. NIDHI GUPTA</span> </li>
                        <li><span class="one-parts">75</span> <span class="two-parts schedule-left-line">Dr. ANAMIKA DAMOR</span> </li>
                        <li><span class="one-parts">80</span> <span class="two-parts schedule-left-line">Dr. POOJA BHIWANI</span> </li>
                        <li><span class="one-parts">86</span> <span class="two-parts schedule-left-line">Dr. JAIVEER RENIWAL</span> </li>
                        <li><span class="one-parts">88</span> <span class="two-parts schedule-left-line">Dr. PURVA SHARMA</span> </li>
                        <li><span class="one-parts">89</span> <span class="two-parts schedule-left-line">Dr. SWTI KULHAR</span> </li>
                        <li><span class="one-parts">90</span> <span class="two-parts schedule-left-line">Dr. RAVINDER KUMAR SONI</span> </li>
                        <li><span class="one-parts">91</span> <span class="two-parts schedule-left-line">Dr. PANKAJ SHARMA</span> </li>
                        <li><span class="one-parts">92</span> <span class="two-parts schedule-left-line">Dr. MONICA CHETANI</span> </li>
                        <li><span class="one-parts">96</span> <span class="two-parts schedule-left-line">Dr. SUNITA</span> </li>
                        <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line" style="color:#000;">And many more...</span> </li>
                      </ul>
                    </div>
                  </div>
                </article>
              </div>
              <div id="official">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Tuhina Goel</span></p>
                            <p>Rank: 1st AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Nishant Kumar</span></p>
                            <p>Rank: 56th AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Geetika Srivastava</span></p>
                            <p>Rank: 6th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bhawna Attri</span></p>
                            <p>Rank: 24th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </article>
              </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script> 
</body>
</html>