<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="md-ms-banner">
      <?php include 'md-ms-big-nav.php'; ?>
      <aside class="banner-left">
        <h2>MD/MS Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="index.php" title="MD/MS Course">MD/MS Course</a></li>
          <li><a title="Achievement" class="active-link">Achievement</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li id="accor1" onClick="ontab('1');" class="light-blue border_none"><span id="accorsp1" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
                <ol class="achievment-inner display_block" id="aol1">
                  <li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="aiims-nov-2014.php" title="AIIMS NOV">AIIMS NOV</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_jun_2014.php" title="DNB June">DNB June</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2014.php" title="PGI May">PGI May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2014.php" title="AIIMS May">AIIMS May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipge_2014.php" title="AIPGME">AIPGME</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="UPPG_2014.php" title="UPPG">UPPG</a></li>
                </ol>
              </li>
              <li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
                <ol class="achievment-inner display_none" id="aol2">
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipge_2013.php" title="AIPGME">AIPGME</a></li>
                </ol>
              </li>
              <li id="accor3" onClick="ontab('3');"><span id="accorsp3" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
                <ol class="achievment-inner display_none" id="aol3">
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>
                </ol>
              </li>
              <li id="accor4" onClick="ontab('4');"><span id="accorsp4" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
                <ol class="achievment-inner display_none" id="aol4">
                  <li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
                  <li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading">
            <h4>AIIMS NOV 2014</h4>
            <section class="showme-main">
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="AIIMS Nov 2014">AIIMS Nov 2014</a></li>
                <li><a href="#official" title="Interview">Interview</a></li>
              </ul>
              <div id="jquery">
                <article class="interview-photos-section">
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> 
                        <img src="images/topper/SIDDHARTH_JAIN.jpg" width="100px" height="109px" alt="Dr. SIDDHARTH JAIN" title="Dr. SIDDHARTH JAIN" />
                        <p><span>Dr. SIDDHARTH JAIN</span></p>
                      </div>
                      <div class="students-box"> 
                        <img src="images/topper/RAVI_SHARMA.jpg" width="100px" height="109px" alt="Dr. RAVI SHARMA" title="Dr. RAVI SHARMA" />
                        <p><span>Dr. RAVI SHARMA</span></p>
                      </div>
                      <div class="students-box"> 
                        <img src="images/topper/VIVEK_LANKA.jpg" width="100px" height="109px" alt="Dr. VIVEK LANKA" title="Dr. VIVEK LANKA" />
                        <p><span>Dr. VIVEK LANKA</span></p>
                      </div>
                      <div class="students-box"> 
                        <img src="images/topper/ABHENIL_MITTAL.jpg" width="100px" height="109px" alt="Dr. ABHENIL MITTAL" title="Dr. ABHENIL MITTAL" />
                        <p><span>Dr. ABHENIL MITTAL</span></p>
                      </div>
                    </li>
                    <li>
                      <div class="students-box" style="border-left:none"> 
                        <img src="images/topper/AVIN_GOEL.jpg" width="100px" height="109px" alt="Dr. AVIN GOEL" title="Dr. AVIN GOEL" />
                        <p><span>Dr. AVIN GOEL</span></p> 
                      </div>
                      <div class="students-box"> 
                       <img src="images/topper/CHARANPREET_SINGH.jpg" width="100px" height="109px" alt="Dr. CHARANPREET SINGH" title="Dr. CHARANPREET SINGH" />
                       <p><span>Dr. CHARANPREET SINGH</span></p> 
                      </div>
                      <div class="students-box" style="border-bottom:1px dashed #B7B7B7;"> 
                        <img src="images/topper/SAMRAT_MANDAL.jpg" width="100px" height="109px" alt="Dr. SAMRAT MANDAL" title="Dr. SAMRAT MANDAL" />
                        <p><span>Dr. SAMRAT MANDAL</span></p> 
                      </div>
                      <div class="students-box" style="border-bottom:1px dashed #B7B7B7;"> 
                        <img src="images/topper/SAIF_MEMON.jpg" width="100px" height="109px" alt="Dr. SAIF MEMON" title="Dr. SAIF MEMON" />
                        <p><span>Dr. SAIF MEMON</span></p> 
                      </div>
                    </li>
                    <li style="margin-top: -5px;">
                      <div class="students-box" style="border-left:none"> 
                        <img src="images/topper/RITURAJ_UPADHYAY.jpg" width="100px" height="109px" alt="Dr. RITURAJ UPADHYAY" title="Dr. RITURAJ UPADHYAY" />
                        <p><span>Dr. RITURAJ UPADHYAY</span></p> 
                      </div>
                      <div class="students-box"> 
                        <img src="images/topper/RANJAN_KUMAR.jpg" width="100px" height="109px" alt="Dr. RANJAN KUMAR PATEL" title="Dr. RANJAN KUMAR PATEL" />
                        <p><span>Dr. RANJAN KUMAR PATEL</span></p> 
                      </div>
                    </li>
                  </ul>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                  <div class="schedule-mini-series"> <span class="mini-heading">AIIMS Nov - Result 2014</span>
                    <div class="schedule-mini-top"> <span class="one-part">Rank</span> <span class="two-part">Student Name</span> <span class="three-part">&nbsp;</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li style="border:none;"><span class="one-parts">1</span> <span class="two-parts schedule-left-line">Dr. SIDDHARTH JAIN</span> </li>
                        <li><span class="one-parts">2</span> <span class="two-parts schedule-left-line">Dr. RAVI SHARMA</span> </li>
                        <li><span class="one-parts">3</span> <span class="two-parts schedule-left-line">Dr. VIVEK LANKA</span> </li>
                        <li><span class="one-parts">5</span> <span class="two-parts schedule-left-line">Dr. AMIT GUPTA</span> </li>
                        <li><span class="one-parts">6</span> <span class="two-parts schedule-left-line">Dr. ABHENIL MITTAL</span> </li>
                        <li><span class="one-parts">12</span> <span class="two-parts schedule-left-line">Dr. VIVEK BAGARIA</span> </li>
                        <li><span class="one-parts">17</span> <span class="two-parts schedule-left-line">Dr. MANDULA PHANI PRIYA</span> </li>
                        <li><span class="one-parts">19</span> <span class="two-parts schedule-left-line">Dr. AVIN GOEL</span> </li>
                        <li><span class="one-parts">20</span> <span class="two-parts schedule-left-line">Dr. SAURAV VERMA</span> </li>
                        <li><span class="one-parts">23</span> <span class="two-parts schedule-left-line">Dr. NEHA TANEJA</span> </li>
                        <li><span class="one-parts">28</span> <span class="two-parts schedule-left-line">Dr. SHASHANK RAJ</span> </li>
                        <li><span class="one-parts">30</span> <span class="two-parts schedule-left-line">Dr. AAMIR MOHAMMAD</span> </li>
                        <li><span class="one-parts">33</span> <span class="two-parts schedule-left-line">Dr. CHARANJEET SINGH</span> </li>
                        <li><span class="one-parts">34</span> <span class="two-parts schedule-left-line">Dr. SAMRAT MANDAL</span> </li>
                        <li><span class="one-parts">35</span> <span class="two-parts schedule-left-line">Dr. MANIDIPA MAJUMDAR</span> </li>
                        <li><span class="one-parts">41</span> <span class="two-parts schedule-left-line">Dr. SAIF MEMON</span> </li>
                        <li><span class="one-parts">46</span> <span class="two-parts schedule-left-line">Dr. RITURAJ UPADHYAY</span> </li>
                        <li><span class="one-parts">49</span> <span class="two-parts schedule-left-line">Dr. RANJAN KUMAR PATEL</span> </li>
                        <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line" style="color:#000;">And many more...</span> </li>
                      </ul>
                    </div>
                  </div>
                </article>
              </div>
              <div id="official">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/OWv9mXT_fxI" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Abhenil Mittal</span></p>
                            <p>Rank: 6th AIIMS Nov 2014, Rank: 7th PGI</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/OFy6m-azegA" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Siddharth Jain</span></p>
                            <p>Rank: 1st AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/EbUbqGEiuVM?list=PLcou4I3N5obYaiRIyAx4TKlraHGS5VdHa" frameborder="0" allowfullscreen></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Ravi Sharma</span></p>
                            <p>Rank: 2nd AIIMS Nov 2014</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Tuhina Goel</span></p>
                            <p>Rank: 1st AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Nishant Kumar</span></p>
                            <p>Rank: 56th AIIMS Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Geetika Srivastava</span></p>
                            <p>Rank: 6th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Bhawna Attri</span></p>
                            <p>Rank: 24th PGI Nov 2013</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </article>
              </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
