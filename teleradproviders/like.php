<?php

	error_reporting(0);

	 // function to write the visitor data to the log file

				 function counter_write($filename, $filecontent, $mode='w')

			 { 

				     if($fp = fopen($filename,$mode))

			    { 

			         fwrite($fp, stripslashes($filecontent)); 

			         fclose($fp);

				         return true; 

			     }else{ 

			         return false; 

			     } 

			 } 

			  

			// function to handle the counting and tracking logic

				 function counter_count($filename, $ip, $timeframe=1800)

			 {

				    // if the file does not exists, we attempt to create it

			     if(!file_exists($filename))

				    {

				         counter_write($filename, '');

				     }

				     $counterstr = time().'|'.$ip.'|'."\n";

			     $counter = array($ip=>1);

			    $hits = 1;

				     $counterdata = file($filename);

			    $number = count($counterdata);

				     for($i = 0; $i < $number; $i++)

			   {

	       $userdata = explode('|',$counterdata[$i]);

		        if($userdata[0] > time()-$timeframe)

       {

			            if(array_key_exists($userdata[1],$counter))

				                 $counter[$userdata[1]]++;

				             else

		                 $counter[$userdata[1]] = 1;

	           $hits++;

			             $counterstr .= $counterdata[$i];

				         }

		    }

			   counter_write($filename, $counterstr);

			     $users = count($counter);

				 

			   // change this output to suit your taste

			     $output = '

			    <p>

				        '.$hits.' hit'.($hits > 1 ? 's' : '').' 

			      by '.$users.' user'.($users > 1 ? 's' : '').' 

			        in the last '.($timeframe/60).' minutes.

				     </p>';

 

		     return $output;

	 }

			  

			 // logs the current hit and displays the stats

				// make sure you point this to a writeable file

		 echo counter_count('tmp/site-hit-counter.txt', $_SERVER['REMOTE_ADDR']);

		 

				60 ?>