// JavaScript Document
$(function() {

	// load the modal window
	$('a.modal').click(function(){

		// scroll to top
		$('html, body').animate({scrollTop:0}, 'fast');

		// before showing the modal window, reset the form incase of previous use.
		$('.success, .error').hide();
		$('form#contactForm').show();

		// Reset all the default values in the form fields
	/*
		$('#name').val('Please enter your name');
		$('#email').val('Please enter your Email');
		$('#contact').val('Please enter your contact');
		$('#message').val('Enter your Message...');
		
		*/
		
		$('#scname').val('kodak');
		
		
		//show the mask and contact divs
		$('#mask').show().fadeTo('', 0.7);
		$('div#contact').fadeIn();

		// stop the modal link from doing its default action
		return false;
	});





	// close the modal window is close div or mask div are clicked.
	$('div#close, div#mask').click(function() {
		$('div#contact, div#mask').stop().fadeOut('slow');

	});

	$('#contactForm input').focus(function() {
		$(this).val(' ');
	});

	// when the Submit button is clicked...
	$('input#submit').click(function() {
		//Inputed Strings
		var msg='';
		var error_count=0;
		var username = $('#name').val(),
			email = $('#email').val(),
			cp = $('#cp').val(),
			quantity=$('#quantity').val(),
			comment = $('#message').val();
			scname=$('#scname').val()
		if(username=="")
		{
		error_count=error_count+1;
		alert('Enter User name');	
		return false;
		
		}
	
		if(email=="")
		{
		error_count=error_count+1;
		alert('Enter  email');	
		return false;
		}
		
		if(cp=="")
		{
		error_count=error_count+1;
		error_count=error_count+1;
		alert('Enter contact number');	
		return false;
		}
			
		if(quantity=="")
		{
			
		error_count=error_count+1;
		alert('Enter quantity');	
		return false;
		
		}	
		
		if(comment=="")
		{
		error_count=error_count+1;
		alert('Enter Comment');	
		return false;
		
		}	
		

	
		//Error Count
	
/*
		//Regex Strings
		var username_regex = /^[a-z0-9_-]{3,16}$/,
			email_regex = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;

			//Test Username
			if(!username_regex.test(name)) {
				$('#contact_header').after('<p class=error>Invalid username entered!</p>');
				error_count += 1;
					alert(error_count);
			}

			//Test Email
			if(!email_regex.test(email)) {
				$('#contact_header').after('<p class=error>Invalid email entered!</p>');
				error_count += 1;
			}

			//Blank Comment?
			if(comment == '') {
				$('#contact_header').after('<p class=error>No Comment was entered!</p>');
				error_count += 1;
			}
*/
			
			//No Errors?
			if(error_count === 0) {
				$.ajax({
					type: "post",
					url: "send.php",
			data: "name=" + name + "&email=" + email + "&comment=" +comment+"&contact="+cp+"&quantity="+quantity+"&scname="+scname,
					error: function() {
						$('.error').hide();
						$('#sendError').slideDown('slow');
					},
					success: function () {
						$('.error').hide();
						$('.success').slideDown('slow');
						$('form#contactForm').fadeOut('slow');
					}
				});
			}

			else {
                $('.error').show();
            }

		return false;
	});

});