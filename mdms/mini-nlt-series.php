<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern PG</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/font-face.css" rel="stylesheet" type="text/css" />
<link href="../css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="../js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="../iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.accordionButton').click(function() {
		$('div.accordionContent').slideUp('normal');	
		$(this).next().slideDown('normal');
	});		
	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include '../registration.php'; ?>
<?php include '../enquiry.php'; ?>
<?php include '../coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="../regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="#" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="../test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left">
        <h2>Be smart &amp;<br>
          take your future in Your Hand </h2>
        <h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants, who otherwise couldn't take the advantage of the DAMS teaching because of various reasons like non-availability of DAMS centre in the vicinity would be greatly benefited.</h3>
      </aside>
      <?php include'../md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="../index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li style="background:none;"><a href="../test-series.php" title="Test Series">Test Series</a></li>
          <li><a title="MINI-NLT Series" class="active-link">MINI-NLT Series</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left">
          <div class="inner-left-heading">
            <h4>MINI-NLT Series</h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="franchisee-box">
                  <p>After Grand Success of Our Grand Test, Subject-wise Test Series & AIPG(NBE/NEET) Pattern Live Test Series ,DAMS Now announcing Mini NLT Series , There would be 240 questions in each part . Part-I Series is for Pre- Foundation students(2nd prof students) and Part-II series is for Foundation students For non- DAMSONIANS</p>
                  <ul class="franchisee-list">
                    <li><span>&nbsp;</span>Cost of Mini NLT-Pre/Para Clinical- Rs 1000</li>
                    <li><span>&nbsp;</span>Cost of Mini NLT-Clinical- Rs 1000</li>
                    <li><span>&nbsp;</span>Combo Pack- Rs1500</li>
                    <li><span>&nbsp;</span>For Online Students Additional Charges Rs 1000</li>
                  </ul>
                </div>
                <div class="schedule-mini-series"> <span class="mini-heading">Schedule for Mini NLT Series</span>
                  <div class="schedule-mini-top"> <span class="one-part">Part-I</span> <span class="two-part">Mini NLT-Pre/Para Clinicals</span> <span class="three-part">&nbsp;</span> </div>
                  <div class="schedule-mini-content">
                    <ul>
                      <li style="border:none;"><span class="one-parts">30 June</span> <span class="two-parts schedule-left-line">Anatomy+Physio+Biochem</span> <span class="three-parts schedule-left-line">Mini NLT-I</span> </li>
                      <li><span class="one-parts">14 July</span> <span class="two-parts schedule-left-line">Patho+Pharma+Micro+Fmt</span> <span class="three-parts schedule-left-line">Mini NLT-2</span> </li>
                      <li><span class="one-parts">28 July</span> <span class="two-parts schedule-left-line">All 7 Subjects</span> <span class="three-parts schedule-left-line">Mini NLT-3</span> </li>
                    </ul>
                  </div>
                </div>
                <div class="schedule-mini-series">
                  <div class="schedule-mini-top"> <span class="one-part">Part-II</span> <span class="two-part">Mini NLT-Clinicals</span> <span class="three-part">&nbsp;</span> </div>
                  <div class="schedule-mini-content">
                    <ul>
                      <li class="border_none"><span class="one-parts">11 Aug</span> <span class="two-parts schedule-left-line">Ent+Opthal+PSM</span> <span class="three-parts schedule-left-line">Mini NLT-4</span> </li>
                      <li><span class="one-parts">25 Aug</span> <span class="two-parts schedule-left-line">Medicine+Surgery+Obg+Paeds</span> <span class="three-parts schedule-left-line">Mini NLT-5</span> </li>
                      <li><span class="one-parts">8 Sep</span> <span class="two-parts schedule-left-line">Skin+Anesthesia+Radio+Psychy+Ortho</span> <span class="three-parts schedule-left-line">Mini NLT-6</span> </li>
                      <li><span class="one-parts">22 Sep</span> <span class="two-parts schedule-left-line">All 12 Subjects</span> <span class="three-parts schedule-left-line">Mini NLT-7</span> </li>
                    </ul>
                  </div>
                </div>
                <div class="franchisee-box"> <span>HOW TO APPLY ?</span>
                  <p>Fees to be paid by a Demand Draft/Pay-Order (in favour of ‘DELHI ACADEMY OF MEDICAL SCIENCES PVT LTD’). The DD Pay should be payable at Delhi. When we receive your DD/Pay order, we will acknowledge immediately by SMS to you or E-mail to you. We will send you receipt by post also.</p>
                </div>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right">
          <?php include '../md-ms-right-accordion.php'; ?>
          
          <!--for Enquiry -->
          <?php include '../enquiryform.php'; ?>
          <!--for Enquiry --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include '../footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="../navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="../js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>