<script>
function sliddesqz(val)
{
   var spq=$('div.accordionButton > span').size();
   for(var d=1;d<=spq;d++)
   {   
       if(val!=d)
	   { 
    	 $('#sqz'+d).removeClass('arrowup');
         $('#sqz'+d).addClass('arrowdwn');
		 $('#dipz'+d).slideUp(400);
	   }      
   }
   
   $('#dipz'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#sqz'+val).removeClass('arrowdwn');
            $('#sqz'+val).addClass('arrowup');
       }
	   else
	   {
			$('#sqz'+val).removeClass('arrowup');
            $('#sqz'+val).addClass('arrowdwn');
       }
   });
}
</script>

<div id="accor-wrapper">
  <div class="accordionButton" onclick="sliddesqz('1')"><span class="arrowdwn" id="sqz1"></span>National Quiz</div>
  <div class="accordionContent" id="dipz1" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="gray-matter.php" title="Concept of Grey Matter" ><span class="sub-arrow"></span>Concept of Grey Matter</a></li>
          <li><a href="gray-matter-testimonial.php" title="Testimonials"><span class="sub-arrow"></span>Testimonials</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddesqz('2')"><span class="arrowdwn" id="sqz2"></span>2012</div>
  <div class="accordionContent" id="dipz2" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="round1.php" title="1st Campus Round 2012"><span class="sub-arrow"></span>1st Campus Round 2012</a></li>
          <li><a href="round2.php" title="2nd Campus Round 2012"><span class="sub-arrow"></span>2nd Campus Round 2012</a></li>
          <li><a href="grand.php" title="Grand Finale 2012"><span class="sub-arrow"></span>Grand Finale 2012</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddesqz('3')"><span class="arrowdwn" id="sqz3"></span>2013</div>
  <div class="accordionContent" id="dipz3" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="season2round1.php" title="1st Campus Round 2013"><span class="sub-arrow"></span>1st Campus Round 2013</a></li>
          <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
          <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>
        </ol>
      </ul>
    </div>
  </div>
  <div class="accordionButton" onclick="sliddesqz('4')"><span class="arrowdwn" id="sqz4"></span>2014</div>
  <div class="accordionContent" id="dipz4" style="display:none;">
    <div class="inner-accor">
      <ul>
        <ol>
          <li><a href="aiims-delhi.php" title="1st Campus Round 2014"><span class="sub-arrow"></span>1st Campus Round 2014</a></li>
        </ol>
      </ul>
    </div>
  </div>
</div>
