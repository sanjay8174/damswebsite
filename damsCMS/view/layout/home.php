<?php
include('dao.php');

?>
  <style>
.table td{text-align:center !important;}
.table th{text-align:center !important;}
thead tr:first-child th{width:1% !important;}
</style>
        <div   class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li  <?php if($_GET['ln']=='1' && $_GET['t']=='6'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=11&ln=1&t=6"><i class="icon-chevron-right"></i> Banner</a>
                        </li>
                        <li <?php if($_GET['ln']=='2' && $_GET['t']=='6'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=11&ln=2&t=6"><i class="icon-chevron-right"></i> Course</a>
                        </li>
                        <li <?php if($_GET['ln']=='6' && $_GET['t']=='6'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=11&ln=6&t=6"><i class="icon-chevron-right"></i>Useful Link</a>
                        </li>
                        <li <?php if($_GET['ln']=='3' && $_GET['t']=='6'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=11&ln=3&t=6"><i class="icon-chevron-right"></i> News & Event</a>
                        </li>
                        <li <?php if($_GET['ln']=='4' && $_GET['t']=='6'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=11&ln=4&t=6"><i class="icon-chevron-right"></i>Toppers Talk </a>
                        </li>
                        <li <?php if($_GET['ln']=='5' && $_GET['t']=='6'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=11&ln=5&t=6"><i class="icon-chevron-right"></i>Photo Gallery</a>
                        </li>
                    </ul>
                </div>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->

                      <div>
                          <div class="row-fluid" <?php if($_GET['ln']=='1' && ( $_GET['e']=='0' || !isset($_GET['e'])) ){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=11&ln=1&t=6&e=0">Banner</a></div>
                                    <div class="muted pull-left" style="margin-left:5px;"><a href="index.php?p=11&ln=1&t=6&e=1">Add Banner</a></div>
                                    <div class="pull-right"><span class="badge badge-info"><?php //echo $countCourse; ?></span></div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Banner</th>
                                                <th>Banner Text</th>
                                                <th>Order</th>
                                                <!--<th>Image</th>-->
                                                <th>Active</th>
                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="homeBannerData">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>

                     <div id="courseStart" class="row-fluid" <?php if($_GET['ln']=='1' &&( $_GET['e']=='1' || $_GET['e']=='2' ) ){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=11&ln=1&t=6&e=0">Banner</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
<form class="form-horizontal"   action="javascript:void(0)" onsubmit="callHomeData.saveData();">
                                      <input type="hidden" class="span6" id="modeBanner"  data-provide="typeahead" value="1">
                                      <input type="hidden" class="span6" id="idBanner"  data-provide="typeahead" value="">
                                        <fieldset>
                                        <legend><span id="editBanner"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> Banner</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Banner Name</label>
                                          <div class="controls">
                                              <input type="text" class="span6" name="bannerField1" id="bannerField1" valu='' />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Banner Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="bannerField2" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" data-provide="typeahead" data-items="4" >
                                          </div>
                                        </div>
                                         <div class="control-group" style="display:block;">
                                          <label class="control-label" for="fileInput"><span style="color: red;">*</span>Image upload</label>
                                          <div class="controls">
                                            <input class="input-file uniform_on" id="bannerField3" name="bannerImage" type="file" />
                                            <input class="input-file uniform_on" id="bannerFEdit3" name="bannerFEdit3" type="hidden" />
                                              </div>
                                        </div>
                                        <div class="control-group">
                                          <label style="margin-right:20px; vertical-align:top;padding-top: 24px" class="control-label" for="textarea2"><span style="color: red;">*</span>Content(Banner)</label>
                                          <div id="bannerField4"class="block-content collapse in">
		                               <textarea rows="4" class="span6" cols="50" id="bannerField4"></textarea>
		                          </div>
		                        </div>
                                             <div class="control-group">
                                          <label class="control-label" for="typeahead">Banner Link</label>
                                          <div class="controls">
                                              <input type="text" class="span6" name="bannerField6" id="bannerField6" value='' />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <div class="controls">
                                            <label class="uniform" style="width:27%;">
                                              <input class="uniform_on" type="checkbox" id="bannerField5" value="1" />
                                              Want to Active the Banner
                                            </label>
                                          </div>
                                        </div>
                                        <div class="form-actions">
                                          <button id="bannerSubmit" type="submit"  class="btn btn-primary">Save changes</button>
                                          <button type="reset"  id="cancelBannerData" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    </div>
                      
                      
                      
      <!--- course start -->                
                      
<div>
                      <div class="row-fluid" <?php if($_GET['ln']=='2' && ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=11&ln=2&t=6&e=0">Course</a></div>
                                    <div class="muted pull-left" style="margin-left:5px;"><a href="index.php?p=11&ln=2&t=6&e=1">Add Course</a>
                                    </div>
<!--                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countCourse; ?></span>

                                    </div>-->
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Course </th>
                                                <th> Order</th>
                                                <th>Url Link</th>
<!--                                                <th>Image</th>-->
<!--                                                <th>Sub Course Title</th>-->

                                                <th>Active</th>

                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="courseData">
                                       
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                     <div id="categoryStart" class="row-fluid" <?php if($_GET['ln']=='2' &&( $_GET['e']=='1' || $_GET['e']=='2' )){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=11&ln=2&t=6&e=0">Course</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form class="form-horizontal"   action="javascript:void(0)" onsubmit="callCourseData.saveData();">
                                      <input type="hidden" class="span6" id="modeCourse"  data-provide="typeahead" value="1">
                                      <input type="hidden" class="span6" id="idCourse"  data-provide="typeahead" value="">
                                        <fieldset>
                                        <legend><span id="editCourse">Add</span> Course</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Course Name</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="courseField1"  data-provide="typeahead" data-items="4" data-source='["DAMS SKY","MCI SCREENING","MDS QUEST","USMLE EDGE"]'>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Course Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="courseField2"  data-provide="typeahead" data-items="4" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');">
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Url Link </label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="courseField3"  data-provide="typeahead" data-items="4" data-source='["https://www.damsdelhi.com","https://sky.damsdelhi.com","https://mci.damsdelhi.com","https://mds.damsdelhi.com","https://localhost/damsCMS"]'>
                                          </div>
                                        </div>
                                        
                                         <div class="control-group" style="display: block;">
                                          <label class="control-label" for="fileInput">Image upload</label>
                                          <div class="controls">
                                            <input class="input-file uniform_on" id="courseField4" name="courseImage" type="file">
                                            <input class="input-file uniform_on" id="courseFEdit4" name="courseFEdit4" type="hidden">
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label style="margin-right:20px; vertical-align:top;padding-top: 24px" class="control-label" for="textarea2">Content(Home)</label>
                                          <div id="courseField5"class="block-content collapse in">
		                               <textarea id="ckeditor_standard" class="span6" cols="50" rows="4"></textarea>
		                            </div>
		                            </div>
                                        <div class="control-group" style="display:none;">
                                          <label style="margin-right:20px; vertical-align:top;padding-top: 24px" style="margin-right:20px" class="control-label" for="textarea2">Content</label>
                                          <div id="courseField6" class="block-content collapse in">
		                               <textarea id="ckeditor_standard" ></textarea>
		                            </div>
                                        </div>
                                        <div class="control-group" style="display:none;">
                                          <label class="control-label" for="typeahead">Sub Course Title</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="courseField7"  data-provide="typeahead" data-items="4" data-source='["https://www.damsdelhi.com","https://sky.damsdelhi.com","https://mci.damsdelhi.com","https://mds.damsdelhi.com","https://localhost/damsCMS"]'>
                                          </div>
                                        </div>
                                      <div class="control-group" style="display:none;">
                                          <label class="control-label" for="optionsCheckbox">Settings</label>
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="courseField8" value="option1">
                                              Course Navigation to be shown
                                            </label>
                                          </div>
                                        </div>
                                        <div class="control-group" style="display:none;">
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="courseField9" value="option1">
                                              Header block to be shown
                                            </label>
                                          </div>
                                        </div>
                                        <div class="control-group" style="display:none;">
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="courseField10" value="option1">
                                              Course block to be shown
                                            </label>
                                          </div>
                                        </div>
                                        <div class="control-group" style="display:none;">
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="courseField11" value="option1">
                                              Sub-Course block to be shown
                                            </label>
                                          </div>
                                        </div>
                                        <div class="control-group" style="display:none;">
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="courseField12" value="option1">
                                              Event Gallery block to be shown
                                            </label>
                                          </div>
                                        </div>
                                        <div class="control-group" style="display:none;">
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="courseField13" value="option1">
                                              News and Updates block to be shown
                                            </label>
                                          </div>
                                        </div>
                                        <div class="control-group" style="display:none;">
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="courseField14" value="option1">
                                              Video block to be shown
                                            </label>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <div class="controls">
                                            <label class="uniform" style="width:27%;">
                                              <input class="uniform_on" type="checkbox" id="courseField15" value="option1">
                                              Want to Active the Course
                                            </label>
                                          </div>
                                        </div>
                                        <div class="form-actions">
                                          <button id="courseSubmit" type="submit"  class="btn btn-primary">Save changes</button>
                                          <button type="reset"  id="cancelCourseData" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    </div>
      
                <!-- ******START: Useful Link********************************************* -->
                    <div>
                        <div class="row-fluid" <?php if($_GET['ln']=='6' && ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=11&ln=6&t=6&e=0">Useful Link</a></div>
                                    <div class="muted pull-left" style="margin-left:5px;"><a href="index.php?p=11&ln=6&t=6&e=1">Add Useful Link</a>
                                        
                                    <!--<div class="pull-right"><span class="badge badge-info"><?php echo $countCategory; ?></span>-->

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Link Text</th>
                                                <th>Url Link</th>
                                                <th>Sub Link</th>
                                                <th>Order</th>
                                                <th>Active</th>
                                                <th>New Tab</th>
                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="linkBannerData"></tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        
                        <div id="subcategoryStart" class="row-fluid" <?php if($_GET['ln']=='6'  &&( $_GET['e']=='1' || $_GET['e']=='2' )){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                           <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                   <div class="muted pull-left"><a href="index.php?p=11&ln=6&t=6&e=0">Useful Link</a></div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <form    class="form-horizontal"  action="javascript:void(0)" onsubmit="callCategory2.saveData();">
                                            <input type="hidden" class="span6" id="modeLink"  data-provide="typeahead" value="1">
                                            <input type="hidden" class="span6" id="linkId"  data-provide="typeahead" value="">

                                            <fieldset>
                                                <legend><span id="editLink"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> Useful Link</legend>
                                                <div class="control-group">
                                                    <label class="control-label" for="select01"><span style="color: red;">*</span>Link Name</label>
                                                    <div class="controls">
                                                        <input type="text" class="span6" id="linkField1"  name="linkField1"   />
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="select01"><span style="color: red;">*</span>Link Order</label>
                                                    <div class="controls" id="CourseNavDiv">
                                                        <input type="text" class="span6" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" id="linkField2" name="linkField2" />
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="typeahead"><span style="color: red;">*</span>URL Link</label>
                                                    <div class="controls">
                                                       <input type="text"  class="span6" id="linkField3" name="linkField3" data-provide="typeahead"  data-items="4" />
                                                        
                                                    </div>
                                                </div>  
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <label class="uniform">
                                                       <!-- <input class="uniform_on" type="checkbox" id="ck_linkField5" value="" onclick="ShowHideDiv(this)" />-->
                                                            <a id="a_linkField5" value="" style="" onclick="showDiv(this)">Want to Add the Sub Link ...</a>
                                                        </label>
                                                        
                                                        <div id="divHideShow" style="display: none">
                                                            <!--<input type="text" class="span6" id="linkField5" name="linkField5" data-provide="typeahead"  data-items="4" data-source='["1","2","3","4","5","6","7","8","9","10"]' /> -->

                                                        <div id='TextBoxesGroup'>
                                                                <div id="TextBoxDiv1" class="">
                                                                    <div ><label><span>Sub Link Text 1 :</span>  <span style="margin-left: 15%">Sub URL Link 1 :</span> </label></div>
                                                                        
                                                                        <input type='text' id='linkField51' name="subLinkText" >
                                                                        <input type='text' id='linkField61' name="subLink" data-provide="typeahead"  data-items="4"  >
                                                                        <input type="hidden" id="totalFilds" name="totalFilds" value="" />
                                                                        <input type='button' value='Add More...' id='addButton'>
                                                                        <input type='button' value='Remove...' id='removeButton'>
                                                                        
                                                                        <!--<input type='button' value='Get TextBox Value' id='getButtonValue'> -->
                                                                </div>
                                                        </div>
                                                        </div>        
                                                    </div>
                                                </div> 
<!--*******Added By Azizur ******************************** -->
<script type="text/javascript">

$(document).ready(function(){
     
    var counter = 2;
    var mode = $('#modeLink').val() ;
        if(mode  == '2'){
            counter = parseInt($("#totalFilds").val()) + 1;
        }
      		
    $("#addButton").click(function () {
	/*if(counter>10){ // For Limit The Text Field
            alert("Only 10 textboxes allow");
            return false;
	}   */
		
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
                
	newTextBoxDiv.after().html('<label><span>Sub Link Text '+ counter +' :</span>  <span style="margin-left: 15%">Sub Link Name '+ counter +' :</span> </label>' +
	      '<input type="text" name="subLinkText' + counter + '"  id="linkField5' + counter + '" value="" > <input type="text"  data-provide="typeahead" name="subLink' + counter + '" \n\
                id="linkField6' + counter + '" value="" >');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");			
        $("#totalFilds").val(counter);
	counter++;
        
     });

    $("#removeButton").click(function () {
        
	if(counter==2){ 
           $("#linkField51").val("");
           $("#linkField61").val("");
           hideDiv(a_linkField5);
           //$("#TextBoxDiv").clear();
          //alert("No more textbox to remove");
          return false;
       }   
	counter--;
        $("#totalFilds").val(counter);
        $("#TextBoxDiv" + counter).remove();		
    });
		
    /* $("#getButtonValue").click(function () { //For Getting the value of Text Fieids 
		
	var msg = '';
	for(i=1; i<counter; i++){
            if(i==1){
                 msg += "\n Sub Link " + i + " : " + $('#linkField5').val() + "\n Sub Link Text" + i + " : " + $('#linkField6').val();
            }else
   	  msg += "\n Sub Link " + i + " : " + $('#linkField5' + i).val() + "\n Sub Link Text" + i + " : " + $('#linkField6').val();
	}
    	  alert(msg);
     });*/
 });
                       
   function showDiv(a_linkField5) {
            var divHideShow = document.getElementById("divHideShow");
            divHideShow.style.display = a_linkField5.click ? "block" : "none";
            $("#totalFilds").val();
        }
                                     
   function hideDiv(a_linkField5) {
            var divHideShow = document.getElementById("divHideShow");
            divHideShow.style.display = a_linkField5.click ? "none" : "block";
        }
          
</script>
                                                
    
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <label class="uniform" style="width:26%;">
                                                            <input class="uniform_on" type="checkbox" id="linkField41" value="1" />
                                                            Want to Open in New Tab
                                                        </label>
                                                        
                                                        <label class="uniform" style="width:23%;">
                                                            <input class="uniform_on" type="checkbox" id="linkField4" value="1" />
                                                            Want to Active the Link
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-actions">
                                                    <button type="submit" onclick="callUseFulLink.saveData()" class="btn btn-primary">Save changes</button>
                                                    <button id="cancelCategory2" type="reset" id="cancelLinkData" class="btn">Cancel</button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                           <!-- /block -->
                        </div>
                    </div>
                <!-- *****END: Useful Link*************************************************** -->
                    
                    <div>
                    <div class="row-fluid" <?php if($_GET['ln']=='3'&& ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=11&ln=3&t=6&e=0">News&Event</a></div>
                                    <div style="margin-left:10px;" class="muted pull-left"><a href="index.php?p=11&ln=3&t=6&e=1">Add News&Event</a>
                                    </div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countSubCourse; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Date</th>
                                                <th>Text</th>
                                                <th>Description</th>
                                                <th> Order</th>                                               
                                                <th>Active</th>

                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="newsAndEventData">
                                       

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>

                     <div id="subCourseStart"class="row-fluid" <?php if($_GET['ln']=='3'&&( $_GET['e']=='1' || $_GET['e']=='2' )){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                         <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=11&ln=3&t=6&e=0">News&Event</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form  class="form-horizontal" action="javascript:void(0)" onsubmit="callnewsAndEvent.saveData();">
                                      <input type="hidden" class="span6" id="modeNewsAndEvent"  data-provide="typeahead" value="1" />
                                      <input type="hidden" class="span6" id="idNewsAndEvent"  data-provide="typeahead" value="" />
                                        <fieldset>
                                        <legend><span id="editNews"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> News&Event</legend>
                                        <div class="control-group">
                                            <label class="control-label" for="typeahead"><span style="color:red;">*</span>Date</label>
                                          <div class="controls">
                                            <input type="text" readonly class="span6" id="newsEventField3"  />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color:red;">*</span>Text</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="newsEventField4"  />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color:red;">*</span>Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="newsEventField5"  data-provide="typeahead" data-items="4" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');">
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label style="margin-right:20px; vertical-align:top;padding-top: 24px" class="control-label" for="textarea2">Description</label>
                                          <div id="newsEventField6" class="block-content collapse in">
		                               <textarea id="ckeditor_standard" class="span6" cols="50" rows="4"></textarea>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="optionsCheckbox">Settings</label>
                                          <div class="controls">
                                            <label class="uniform" style="width:32%;">
                                              <input class="uniform_on" type="checkbox" id="newsEventField7" value="1">
                                              Want to Active this News & Event
                                            </label>
                                          </div>
                                        </div>
                                        
                                        <div class="form-actions">
                                          <button type="submit"  class="btn btn-primary">Save changes</button>
                                          <button id="cancelNewsData" type="reset" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                  </div>
              <div class="row-fluid" <?php if($_GET['ln']=='4' && ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=11&ln=4&t=6&e=0">Toppers Talk</a></div>
                                    <div class="muted pull-left" style="margin-left:5px;"><a href="index.php?p=11&ln=4&t=6&e=1">Add Toppers Talk</a></div>
                                    <!--<div class="pull-right"><span class="badge badge-info"><?php echo $countCategory; ?></span></div>-->
                                
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>URL LINK</th>
                                                <th>Order</th>
                                                <th>Student Name</th>
                                                <th>Rank</th>
                                                <th>Text</th>
                                                <th>Active</th>
                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="topperTalkData"></tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                     <div id="PcategoryStart" class="row-fluid" <?php if($_GET['ln']=='4' &&( $_GET['e']=='1' || $_GET['e']=='2' )) { ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=11&ln=4&t=6&e=0">Toppers Talk</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form  class="form-horizontal"  action="javascript:void(0)" onsubmit="callProductCategory.saveData();">

                                      <input type="hidden" class="span6" id="modeTopperTalk"  data-provide="typeahead" value="1" />
                                      <input type="hidden" class="span6" id="idTopperTalk"  data-provide="typeahead" value="" />
                                     <fieldset>


                                        <legend ><span id="editTopperTalk"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> Topper Talks</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="select01"><span style="color: red;">*</span>Student Name </label>
                                          <div class="controls">
                                                <input type="text" class="span6" id="topperField10"   />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="select01"><span style="color: red;"></span>Rank</label>
                                          <div class="controls">
                                                <input type="text" class="span6" id="topperField11" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');"  />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="select01"><span style="color: red;">*</span>URL Link</label>
                                          <div class="controls">
                                                <input type="text" class="span6" id="topperField1"   />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Toper Talk Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" id="topperField2"  data-provide="typeahead" data-items="4"  />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Description</label>
                                          <div class="controls">
                                              <textarea  rows="4" class="span6" cols="50" id="topperField4"></textarea>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="optionsCheckbox">Settings</label>
                                          <div class="controls">
                                            <label class="uniform" style="width:29%;">
                                              <input class="uniform_on" type="checkbox" id="topperField3" value="1">
                                              Want to Active the Toper Talk
                                            </label>
                                          </div>
                                        </div>



                                        <div class="form-actions">
                                            <button type="submit" onclick="topperTalkData.saveData()" class="btn btn-primary">Save changes</button>
                                          <button id="cancelToperTalk" type="reset" class="btn">Cancel</button>

                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                            
              <div class="row-fluid" <?php if($_GET['ln']=='5' && ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=11&ln=5&t=6&e=0">Photo Gallery</a></div>
                                    <div class="muted pull-left" style="margin-left:5px;"><a href="index.php?p=11&ln=5&t=6&e=1">Add Photo Gallery</a></div>
<!--                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countCategory; ?></span>
                                      
                                    </div>-->
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>Student</th>
                                                <th>Rank</th>
                                                <th>Url Link</th>
                                                <th>Text</th>
                                                <th>Order</th>
                                                <th>Active</th>
                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>        
                                            </tr>
                                        </thead>
                                        <tbody id="photoGallery"> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                     <div id="productStart" class="row-fluid" <?php if($_GET['ln']=='5' &&( $_GET['e']=='1' || $_GET['e']=='2' )) { ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=11&ln=5&t=6&e=0">Photo Gallery</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form  class="form-horizontal"  action="javascript:void(0)" onsubmit="callPhotoGall.saveData();">
                                      <input type="hidden" class="span6" id="modePhotoGall"  data-provide="typeahead" value="1" />
                                      <input type="hidden" class="span6" id="idPhotoGall"  data-provide="typeahead" value="" />
                                      
                                        <fieldset>
                                        <legend ><span id="editPhotoGall"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> Photo Gallery</legend>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="select01"><span style="color: red;">*</span>Student Name</label>
                                          <div class="controls">
                                              <input type="text" class="span6" id="photoField11" name="photoField1"  />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="select01"><span style="color: red;">*</span>Rank</label>
                                          <div class="controls">
                                              <input type="text" class="span6" id="photoField12" name="photoField1" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" />
                                          </div>
                                        </div>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="select01"><span style="color: red;"></span>URL Link</label>
                                          <div class="controls">
                                              <input type="text" class="span6" id="photoField1" name="photoField1"  />
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Order</label>
                                          <div class="controls">
                                              <input type="text" class="span6" id="photoField2" name="photoField2" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" data-provide="typeahead" data-items="4"  />   
                                          </div>
                                        </div>
                                       <div class="control-group">
                                          <label class="control-label" for="fileInput"><span style="color: red;">*</span>Image upload (Image in jpeg, jpg or png)</label>
                                          <div class="controls">
                                            <input class="input-file uniform_on" id="photoField3" name="productImage" type="file">&nbsp;&nbsp;&nbsp;<b>Dimension should be Min(90X139) and Max(418X583) (in pixels)</b>
                                            <input class="input-file uniform_on" id="photoFEdit3" name="photoFEdit3" type="hidden">
                                          </div> 
                                          <input type="hidden" value='' id="upldproductImage"/>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Photo Description</label>
                                          <div class="controls">
                                              <textarea  rows="4" class="span6" cols="50" id="photoField4"></textarea>
                                          </div>
                                        </div>
                                        <!-- started for combo products -->
                                        <div class="control-group">
                                          <label class="control-label" for="optionsCheckbox">Settings</label>
                                          <div class="controls">
                                            <label class="uniform" style="width:31%;">
                                              <input class="uniform_on" type="checkbox" id="photoField5" value="1" />
                                              Want to Active the Photo Gallery
                                            </label>
                                          </div>
                                        </div>
                                         
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                            <button id="cancelPhoto" type="reset" onclick="window.location='index.php?p=11&ln=5&t=6&e=0'" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                            </div>
                      <div>
                 
                        </div>

                </div>
            </div>
            
            
         
<!--                <p>&copy; Shobhit 2013</p>-->
       
        <!--/.fluid-container-->
   <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">
    
        
     
        
        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">
 
        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>
        
        <!--
        <link href="vendors/datePicker/style.css">
        <link href="vendors/datePicker/jquery-ui.css">
        <script src="vendors/datePicker/jquery-ui.js"></script>
        <script src="vendors/datePicker/jquery-1.10.2.js"></script>
        -->
        
        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

        <script src="vendors/ckeditor/ckeditor.js"></script>
		<script src="vendors/ckeditor/adapters/jquery.js"></script>
        <script src="assets/scripts.js"></script>
        <script type="text/javascript" src="view/layout/layout.js" ></script>
        <script>
        $(function() {
            /* for combo products */
//             var addDiv = $('#addAttachdiv');
//            i = $('#addAttachdiv p').size() + 1;
//            $('#attachfile').on('click', function() {
//            $('<p> <input class="input-file uniform_on" id="comboproductimg_new' + i +'" name="comboproductimg_new' + i +'" type="file" multiple="true"><a id="remNew" href="#" onclick="removefunc(id);">Remove</a> </p>').appendTo(addDiv);
//            i++;
//            return false;
//	});

            $(".datepicker").datepicker();
            $("#newsEventField3").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();

//          $( 'textarea#ckeditor_standard' ).ckeditor({width:'90%', height: '150px', toolbar: [
//				{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
//				[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
//                                { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
//			]});
//          $( 'textarea#photoField4' ).ckeditor({width:'90%', height: '150px', toolbar: [
//				{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
//				[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
//                                { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
//			]});
//            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        /* for combo products */
//        function removefunc(id){
//           var par= document.getElementById(id).parentNode;
//            if( i > 2 ) {
//                 $(par).remove();
//                i--;
//                }
//	return false;
//	}

        <?php if ($_GET['ln'] == '1') { ?>
        var callHomeData = new homeBannerData();
            callHomeData.getDataAll();
            <?php if ($_GET['e'] == '2') { ?>
                      callHomeData.editData(<?php echo $_GET['id']; ?>);
            <?php }
        } ?>
            
        <?php if ($_GET['ln'] == '2') { ?>
                var callCourseData = new courseData();
                callCourseData.getDataAll();
                <?php if ($_GET['e'] == '2') { ?>
                      callCourseData.editData(<?php echo $_GET['id']; ?>);
                <?php } } ?>
                    

        <?php if ($_GET['ln'] == '3') { ?>

        var callnewsAndEvent= new newsAndEvent();
                callnewsAndEvent.getDataAll();
        <?php if ($_GET['e'] == '2') { ?>
                      callnewsAndEvent.editData(<?php echo $_GET['id']; ?>);
        <?php }} ?>
            
        <?php if ($_GET['ln'] == '4') { ?>
                var topperTalkData = new TopperTalkData();
                    topperTalkData.getDataAll();
                    <?php if ($_GET['e'] == '2') { ?>
                    topperTalkData.editData(<?php echo $_GET['id']; ?>);
        <?php } } ?>
            
            
        <?php if ($_GET['ln'] == '5') { ?>
                 var callPhotoGall = new PhotoGallery();
                     callPhotoGall.getDataAll();
                    <?php if ($_GET['e'] == '2') { ?>
                    callPhotoGall.editData(<?php echo $_GET['id']; ?>);
        <?php } } ?>
            
            
        <?php if ($_GET['ln'] == '6') { ?>
            var callUseFulLink = new homeUseFulLinkData();
                callUseFulLink.getDataAll();
                <?php if ($_GET['e'] == '2') { ?>
                    callUseFulLink.editData(<?php echo $_GET['id']; ?>);
        <?php } } ?>       
    
    
        </script>
    </body>

</html>