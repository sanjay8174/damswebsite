<?php
include('dao.php');
$countCourse = 4;

?>
<div class="container-fluid">
            <div class="row-fluid">
<div class="span9" id="content">

    <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                          <li <?php if($_GET['ln']=='16' && $_GET['t']=='5'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=8&ln=16&t=5"><i class="icon-chevron-right"></i> Achievement</a>
                        </li>
                     <li <?php if($_GET['ln']=='17' && $_GET['t']=='5'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=8&ln=17&t=5"><i class="icon-chevron-right"></i> Second</a>
                        </li>
                      </ul>

                      </div>
    <div class="row-fluid" <?php if($_GET['ln']=='16'){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Achievement</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countCourse; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>S. No</th>
                                                <th>Company Name</th>
                                                <th>Title</th>
                                                <th>Url Link</th>
                                                <th>Helpline Number</th>
                                                <th>Sms Enquiry</th>
                                                <th>Status</th>
                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="companyData">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        
    <div class="row-fluid" <?php if($_GET['ln']=='16'){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                         <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Company</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form class="form-horizontal" action="javascript:void(0)" onsubmit="callCompanyData.saveData();" >
                                      <input type="hidden" class="span6" id="modeCompany"  data-provide="typeahead" value="1">
                                      <input type="hidden" class="span6" id="idCompany"  data-provide="typeahead" value="">
                                      <fieldset>
                                        <legend><span id="editComp">Add </span> Company Information</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Company Name</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="companyField1"  data-provide="typeahead" >
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Title</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="companyField2"  data-provide="typeahead">
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Url Link</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="companyField3"  data-provide="typeahead">
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Helpline Number</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="companyField4"  data-provide="typeahead">
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">SMS Enquiry</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="companyField5"  data-provide="typeahead">
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="optionsCheckbox">Settings</label>
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="companyField6">
                                              Want to Active the Company
                                            </label>
                                          </div>
                                        </div>



                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary">Save Changes</button>
                                          <button type="reset" class="btn" id="cancelCompanyData">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>





</div>
                </div>
    </div>
<script type="text/javascript" src="view/layout/layout.js" ></script>
        <script>
        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();

             $( 'textarea#ckeditor_standard' ).ckeditor({width:'98%', height: '150px', toolbar: [
				{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
				[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
				{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
			]});
          $('.textarea').wysihtml5();
            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });




    <?php if($_GET['ln']=='16'){ ?>
    var callCompanyData = new companyData();
    callCompanyData.getDataAll();
    <?php } ?>
        </script>