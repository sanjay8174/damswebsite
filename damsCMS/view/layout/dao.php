<?php

class dao extends Database{

        public function insCompany($companyName,$title,$urlLink,$helplineNumber,$smsEnquiry,$active){
        $query = "INSERT INTO COMPANY (COMPANY_NAME,TITLE,URL_LINK,HELPLINE_NUMBER,SMS_ENQUIRY,ACTIVE) VALUES('".$companyName."','".$title."','".$urlLink."','".$helplineNumber."','".$smsEnquiry."',".$active.")";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function updCompany($companyName,$title,$urlLink,$helplineNumber,$smsEnquiry,$active,$id){
        $query = "UPDATE COMPANY SET COMPANY_NAME = '".$companyName."' , TITLE = '".$title."' , URL_LINK = '".$urlLink."' , HELPLINE_NUMBER = '".$helplineNumber."' , SMS_ENQUIRY = '".$smsEnquiry."' , ACTIVE = ".$active." WHERE COMPANY_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
 public function delCompany($id){
        $query = "DELETE FROM COMPANY WHERE COMPANY_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function selCompany($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " COMPANY_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT COMPANY_ID,COMPANY_NAME,TITLE,URL_LINK,HELPLINE_NUMBER,SMS_ENQUIRY,ACTIVE FROM COMPANY".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }

    public function countCompany($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " COMPANY_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT count(COMPANY_ID) AS TOTAL FROM COMPANY".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }

    public function delCourse($id){
        $query = "DELETE FROM COURSE WHERE COURSE_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

public function insCourse($courseName,$urlLink,$imageUpload,$contentHome,$content,$subCourseTitle,$sCourseNav,$sHeaderBlock,$sCourseBlock,$sSubCourseBlock,$sEventGallery,$sNews,$videos,$active,$courseOrder,$imgExt){
        $query = "INSERT INTO COURSE (COURSE_NAME,URL_LINK,IMAGE_UPLOAD,CONTENT_HOME,CONTENT,SUB_COURSE_TITLE,S_COURSE_NAV,S_HEADER_BLOCK,S_COURSE_BLOCK,S_SUB_COURSE_BLOCK,S_EVENT_GALLERY,S_NEWS,S_VIDEOS,ACTIVE,COURSE_ORDER,IMG_EXT) VALUES('".$courseName."','".$urlLink."','".$imageUpload."','".$contentHome."','".$content."','".$subCourseTitle."','".$sCourseNav."','".$sHeaderBlock."','".$sCourseBlock."','".$sSubCourseBlock."','".$sEventGallery."','".$sNews."','".$videos."','".$active."','".$courseOrder."','".$imgExt."')";
        $result = parent::executeQuery($query);
        return $result;

    }
    public function selCourse($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " COURSE_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT COURSE_ID,COURSE_NAME,URL_LINK,IMAGE_UPLOAD,CONTENT_HOME,CONTENT,SUB_COURSE_TITLE,S_COURSE_NAV,S_HEADER_BLOCK,S_COURSE_BLOCK,S_SUB_COURSE_BLOCK,S_EVENT_GALLERY,S_NEWS,S_VIDEOS,ACTIVE,COURSE_ORDER FROM COURSE".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }
public function updCourse($id,$courseName,$courseOrder,$urlLink,$imageUpload,$contentHome,$content,$subCourseTitle,$sCourseNav,$sHeaderBlock,$sCourseBlock,$sSubCourseBlock,$sEventGallery,$sNews,$videos,$active,$ext){
        $query = "UPDATE COURSE SET COURSE_NAME = '".$courseName."' , URL_LINK = '".$urlLink."' ,COURSE_ORDER = '".$courseOrder."' , IMAGE_UPLOAD = '".$imageUpload."' , CONTENT_HOME = '".$contentHome."' , CONTENT = '".$content."',SUB_COURSE_TITLE = '".$subCourseTitle."',S_COURSE_NAV = '".$sCourseNav."',S_HEADER_BLOCK = '".$sHeaderBlock."',S_COURSE_BLOCK = '".$sCourseBlock."',S_SUB_COURSE_BLOCK = '".$sSubCourseBlock."',S_EVENT_GALLERY = '".$sEventGallery."',S_NEWS = '".$sNews."',S_VIDEOS = '".$videos."',ACTIVE = '".$active."',IMG_EXT = '".$ext."'  WHERE COURSE_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }



   public function selectAllCourse(){

       return parent::executeQuery("SELECT COURSE_ID,COURSE_NAME FROM COURSE");

   }
 public function selPage($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " PAGE_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT PAGE_ID,PAGE_NAME,PAGE_URL,PAGE_CONTENT,PAGE_HEADER,ACTIVE FROM PAGE".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }

   public function insCategory($courseId,$categoryName,$categoryOrder,$active,$navId){
        $query = "INSERT INTO CATEGORY (COURSE_ID,CATEGORY_NAME,CATEGORY_ORDER,ACTIVE,COURSE_NAVIGATION_ID) VALUES('".$courseId."','".$categoryName."','".$categoryOrder."',".$active.",'".$navId."')";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function insCategory2($courseId,$category1,$categoryName,$categoryOrder,$active,$courseNav){
        $query = "INSERT INTO CATEGORY2 (COURSE_ID,CATEGORY1_ID,NAME,CATEGORY_ORDER,ACTIVE,COURSE_NAVIGATION_ID) VALUES('".$courseId."','".$category1."','".$categoryName."','".$categoryOrder."',".$active.",'".$courseNav."')";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function updCategory2($courseId,$category1,$categoryName,$categoryOrder,$active,$id,$courseNav){
        $query = "UPDATE CATEGORY2 SET  COURSE_ID = '".$courseId."' , CATEGORY1_ID = '".$category1."' , NAME = '".$categoryName."' , CATEGORY_ORDER = '".$categoryOrder."' , ACTIVE = ".$active.", COURSE_NAVIGATION_ID = '".$courseNav."' WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function insProductCategory($courseId,$categoryName,$categoryOrder,$active){
        $query = "INSERT INTO PRODUCT_CATEGORY (COURSE_ID,PRODUCT_CATEGORY_NAME,PRODUCT_CATEGORY_ORDER,ACTIVE) VALUES('".$courseId."','".$categoryName."','".$categoryOrder."',".$active.")";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function insProduct($courseId,$categoryId,$productName,$image,$description,$cost,$disCost,$taxCost,$order,$active,$author,$lang,$len,$publisher,$extension,$purchasethirdparty,$thirdpartylink,$likeProductIds,$comboProductStatus,$price,$link,$comboImageName){
        $productIds = implode(",",$likeProductIds);
        $query = "INSERT INTO PRODUCT (COURSE_ID,PRODUCT_CATEGORY_ID,PRODUCT_NAME,IMAGE,PRODUCT_DESCRIPTION,COST,PRODUCT_ORDER,ACTIVE,DISCOUNT_COST,TAX,AUTHOR,LANGUAGE,LENGTH,PUBLISHER,IMAGE_EXTENSION,PURCHASE_THIRD_PARTY,THIRD_PARTY_LINK,LIKE_PRODUCT_IDS,COMBO_STATUS,E_PRICE,E_LINK,COMBO_IMAGE_NAME) VALUES('".$courseId."','".$categoryId."','".$productName."','".$image."','".$description."','".$cost."','".$order."',".$active.",'".$disCost."','".$taxCost."','".$author."','".$lang."','".$len."','".$publisher."','".$extension."','".$purchasethirdparty."','".$thirdpartylink."','".$productIds."','".$comboProductStatus."','".$price."','".$link."','".$comboImageName."')";
        $result = parent::executeQuery($query);
        return $result;
    }

    public function updCategory($courseId,$categoryName,$categoryOrder,$active,$id,$navId){
        $query = "UPDATE CATEGORY SET  COURSE_ID = '".$courseId."' , CATEGORY_NAME = '".$categoryName."' , CATEGORY_ORDER = '".$categoryOrder."',COURSE_NAVIGATION_ID = '".$navId."' ,  ACTIVE = ".$active." WHERE CATEGORY_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }


    public function updProduct($courseId,$categoryId,$productName,$Image,$description,$cost,$discountCost,$taxCost,$productOrder,$active,$id,$author,$lang,$length,$publisher,$extension,$purchasethirdparty,$thirdpartylink,$likeProductIds){
        $productIds = implode(",",$likeProductIds);
        $query = "UPDATE PRODUCT SET  COURSE_ID = '".$courseId."' , PRODUCT_CATEGORY_ID = '".$categoryId."' , PRODUCT_NAME = '".$productName."' , IMAGE = '".$Image."', PRODUCT_DESCRIPTION = '".$description."' ,COST = '".$cost."' , DISCOUNT_COST = '".$discountCost."', TAX = '".$taxCost."' ,PRODUCT_ORDER = '".$productOrder."' ,ACTIVE = ".$active.",AUTHOR = '".$author."',LANGUAGE = '".$lang."',LENGTH='".$length."',PUBLISHER='".$publisher."',IMAGE_EXTENSION='".$extension."',PURCHASE_THIRD_PARTY='".$purchasethirdparty."',THIRD_PARTY_LINK='".$thirdpartylink."',LIKE_PRODUCT_IDS ='".$productIds."'  WHERE PRODUCT_ID = ".$id;
  //      $query = "UPDATE PRODUCT SET  COURSE_ID = '".$courseId."' , PRODUCT_CATEGORY_ID = '".$categoryId."' , PRODUCT_NAME = '".$productName."' , IMAGE = '".$Image."', PRODUCT_DESCRIPTION = '".$description."' ,COST = '".$cost."' , DISCOUNT_COST = '".$discountCost."' ,PRODUCT_ORDER = '".$productOrder."' ,ACTIVE = ".$active.",AUTHOR = '".$author."',LANGUAGE = '".$lang."',LENGTH='".$length."',PUBLISHER='".$publisher."',IMAGE_EXTENSION='".$extension."',PURCHASE_THIRD_PARTY='".$purchasethirdparty."',THIRD_PARTY_LINK='".$thirdpartylink."',LIKE_PRODUCT_IDS ='".$productIds."',COMBO_STATUS='".$comboProductStatus."',E_PRICE='".$comboProductPrice."',E_LINK='".$comboProductELink."'  WHERE PRODUCT_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }


//      public function updCategory2($courseId,$category1,$categoryname,$categoryOrder,$active,$id){
//        $query = "UPDATE CATEGORY2 SET  COURSE_ID = '".$courseId."' ,NAME='".$categoryname."', CATEGORY_ORDER = '".$categoryOrder."' ,CATEGORY1_ID = '".$category1."' , ACTIVE = ".$active." WHERE CATEGORY1_ID = ".$category1;
//        $result = parent::executeQuery($query);
//        return $result;
//    }

    public function updProductCategory($courseId,$categoryName,$categoryOrder,$active,$id){
        $query = "UPDATE PRODUCT_CATEGORY SET  COURSE_ID = '".$courseId."' , PRODUCT_CATEGORY_NAME = '".$categoryName."' , PRODUCT_CATEGORY_ORDER = '".$categoryOrder."' , ACTIVE = ".$active." WHERE PRODUCT_CATEGORY_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
//    public function updProduct($courseId,$productName,$productOrder,$Image,$active,$description,$cost,$discountCost,$id){
//        $query = "UPDATE PRODUCT  SET  COURSE_ID = '".$courseId."' , PRODUCT_NAME = '".$productName."' , PRODUCT_ORDER = '".$productOrder."' , PRODUCT_DESCRIPTION = '".$description."' , COST = '".$cost."' ,DISCOUNT_COST = '".$discountCost."' ,IMAGE = '".$Image."',ACTIVE = ".$active." WHERE PRODUCT_ID = ".$id;
//        $result = parent::executeQuery($query);
//        return $result;
//    }
////
   public function selProductCategory($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " PRODUCT_CATEGORY_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT CA.PRODUCT_CATEGORY_ID CATEGORY_ID,CA.COURSE_ID, CA.PRODUCT_CATEGORY_NAME CATEGORY_NAME,C.COURSE_NAME,CA.PRODUCT_CATEGORY_ORDER CATEGORY_ORDER,CA.ACTIVE FROM PRODUCT_CATEGORY CA LEFT JOIN COURSE C ON CA.COURSE_ID=C.COURSE_ID ORDER BY C.COURSE_NAME,CA.PRODUCT_CATEGORY_ID ASC $strWhere $str";
        $result = parent::executeQuery($query);
        return $result;
    }
   public function selProduct($id=null,$active=null,$startpoint=null,$perpage=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " PRODUCT_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;


        $query = "SELECT CA.PRODUCT_ID ,CA.PRODUCT_CATEGORY_ID,CA.PRODUCT_NAME CATEGORY_NAME,C.COURSE_NAME,CA.PRODUCT_ORDER CATEGORY_ORDER,CA.ACTIVE,CA.PRODUCT_DESCRIPTION,CA.IMAGE,CA.COURSE_ID,CA.COST,CA.DISCOUNT_COST,CA.TAX,CA.AUTHOR,CA.LANGUAGE,CA.LENGTH,CA.PUBLISHER,CA.PURCHASE_THIRD_PARTY,CA.THIRD_PARTY_LINK,CA.LIKE_PRODUCT_IDS FROM PRODUCT CA LEFT JOIN COURSE C ON CA.COURSE_ID=C.COURSE_ID ORDER BY C.COURSE_NAME,CA.PRODUCT_ID ASC $strWhere $str limit $startpoint,$perpage";
       //$query = "SELECT CA.PRODUCT_ID ,CA.PRODUCT_CATEGORY_ID,CA.PRODUCT_NAME CATEGORY_NAME,C.COURSE_NAME,CA.PRODUCT_ORDER CATEGORY_ORDER,CA.ACTIVE,CA.PRODUCT_DESCRIPTION,CA.IMAGE,CA.COURSE_ID,CA.COST,CA.DISCOUNT_COST,CA.AUTHOR,CA.LANGUAGE,CA.LENGTH,CA.PUBLISHER,CA.PURCHASE_THIRD_PARTY,CA.THIRD_PARTY_LINK,CA.LIKE_PRODUCT_IDS,CA.COMBO_STATUS,CA.E_PRICE,CA.E_LINK FROM PRODUCT CA LEFT JOIN COURSE C ON CA.COURSE_ID=C.COURSE_ID ORDER BY C.COURSE_NAME,CA.PRODUCT_ID ASC $strWhere $str";

        $result = parent::executeQuery($query);
        return $result;
    }
   public function selCategory($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " CATEGORY_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;
        $query=" SELECT CN.NAME,CA.CATEGORY_ID,CA.COURSE_ID, CA.CATEGORY_NAME,C.COURSE_NAME,CA.CATEGORY_ORDER,CA.ACTIVE,CA.COURSE_NAVIGATION_ID FROM CATEGORY CA LEFT JOIN COURSE C ON CA.COURSE_ID=C.COURSE_ID  left JOIN COURSE_NAVIGATION AS CN ON CN.COURSE_NAVIGATION_ID =CA.COURSE_NAVIGATION_ID AND C.COURSE_ID=CN.COURSE_ID $strWhere $str ORDER BY C.COURSE_NAME,CN.NAME,CA.CATEGORY_ID ASC";
        //$query = "SELECT CA.CATEGORY_ID,CA.COURSE_ID, CA.CATEGORY_NAME,C.COURSE_NAME,CA.CATEGORY_ORDER,CA.ACTIVE,CA.COURSE_NAVIGATION_ID FROM CATEGORY CA LEFT JOIN COURSE C ON CA.COURSE_ID=C.COURSE_ID $strWhere $str";
        $result = parent::executeQuery($query);
        return $result;
    }
   public function selCategory2($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;


        $query = "SELECT CA.ID,CA.COURSE_ID,CA.CATEGORY1_ID, CA.NAME,C.COURSE_NAME,CA.CATEGORY_ORDER,CA.ACTIVE,CA.COURSE_NAVIGATION_ID,C1.CATEGORY_NAME  FROM CATEGORY2 CA LEFT JOIN COURSE C  ON CA.COURSE_ID=C.COURSE_ID LEFT JOIN CATEGORY C1 ON CA.CATEGORY1_ID=C1.CATEGORY_ID ORDER BY  C.COURSE_NAME,C1.CATEGORY_NAME, CA.ID ASC  $strWhere $str";

        //$query = "SELECT CA.ID,CA.COURSE_ID,CA.CATEGORY1_ID, CA.NAME,C.COURSE_NAME,CA.CATEGORY_ORDER,CA.ACTIVE FROM CATEGORY2 CA LEFT JOIN COURSE C ON CA.COURSE_ID=C.COURSE_ID ORDER BY ID DESC $strWhere $str";

        $result = parent::executeQuery($query);
        return $result;
    }

    public function delCategory($id){
        $query = "DELETE FROM CATEGORY WHERE CATEGORY_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function delCategory2($id){
        $query = "DELETE FROM CATEGORY2 WHERE  ID= ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function delProductCategory($id){
        $query = "DELETE FROM PRODUCT_CATEGORY WHERE PRODUCT_CATEGORY_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }


    public function delProduct($id){
        $query = "DELETE FROM PRODUCT WHERE PRODUCT_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }




    public function selectAllCategory(){

       return parent::executeQuery("SELECT CATEGORY_ID,CATEGORY_NAME FROM CATEGORY");

   }

   public function insSubCourse($courseID,$categoryId,$categoryId2,$urlLink,$subCourseName,$content,$subCourseOrder,$active,$fee,$tax,$courseNav){
        $query = "INSERT INTO SUB_COURSE (COURSE_ID,CATEGORY_ID,CATEGORY_ID2,URL_LINK,SUB_COURSE_NAME,CONTENT,SUB_COURSE_ORDER ,ACTIVE,FEE,TAX,COURSE_NAVIGATION_ID) VALUES('".$courseID."','".$categoryId."','".$categoryId2."','".$urlLink."','".$subCourseName."','".$content."','".$subCourseOrder."','".$active."','".$fee."','".$tax."','".$courseNav."')";
        $result = parent::executeQuery($query);
        return $result;
    }


    public function selSubCourse($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " SUB_COURSE_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;
        $query = "SELECT S.SUB_COURSE_ID,S.COURSE_ID,S.CATEGORY_ID,S.CATEGORY_ID2,S.URL_LINK,S.SUB_COURSE_NAME,S.CONTENT,S.SUB_COURSE_ORDER,S.ACTIVE,S.FEE,S.TAX,S.COURSE_NAVIGATION_ID,C.CATEGORY_NAME,CO.COURSE_NAME,C2.NAME AS SUB_CATEGORY_NAME FROM SUB_COURSE S LEFT JOIN CATEGORY C ON S.CATEGORY_ID=C.CATEGORY_ID LEFT JOIN COURSE CO ON S.COURSE_ID=CO.COURSE_ID LEFT JOIN CATEGORY2 C2 ON S.CATEGORY_ID2=C2.ID  ORDER BY CO.COURSE_NAME,S.SUB_COURSE_ID ASC".$strWhere.$str;
    //$query = "SELECT S.SUB_COURSE_ID,S.COURSE_ID,S.CATEGORY_ID,S.CATEGORY_ID2,S.URL_LINK,S.SUB_COURSE_NAME,S.CONTENT,S.SUB_COURSE_ORDER,S.ACTIVE,S.FEE,S.COURSE_NAVIGATION_ID,C.CATEGORY_NAME,CO.COURSE_NAME,CN.NAME,C2.NAME SUB_CATEGORY_NAME FROM SUB_COURSE S LEFT JOIN CATEGORY C ON S.CATEGORY_ID=C.CATEGORY_ID LEFT JOIN COURSE CO ON S.COURSE_ID=CO.COURSE_ID LEFT JOIN COURSE_NAVIGATION CN ON S.COURSE_NAVIGATION_ID=CN.COURSE_NAVIGATION_ID LEFT JOIN CATEGORY2 C2 ON S.CATEGORY_ID2=C2.ID  ORDER  BY CO.COURSE_NAME,S.SUB_COURSE_ID ASC".$strWhere.$str;
         $result = parent::executeQuery($query);
        return $result;
    }

    public function delSubCourse($id){
        $query = "DELETE FROM SUB_COURSE WHERE SUB_COURSE_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

public function updSubCourse($courseID,$categoryId,$categoryId2,$urlLink,$subCourseName,$content,$subCourseOrder,$active,$id,$fee,$courseNav){
        $query = "UPDATE SUB_COURSE SET COURSE_ID = '".$courseID."' , CATEGORY_ID = '".$categoryId."' ,CATEGORY_ID2 = '".$categoryId2."' , URL_LINK = '".$urlLink."' , SUB_COURSE_NAME = '".$subCourseName."' , CONTENT = '".$content."' ,SUB_COURSE_ORDER = '".$subCourseOrder."', ACTIVE= '".$active."', FEE= '".$fee."', COURSE_NAVIGATION_ID= '".$courseNav."' WHERE SUB_COURSE_ID = ".$id;

        $result = parent::executeQuery($query);
        return $result;
    }

public function selNavigation($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " Top_NAV_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT TOP_NAV_ID,NAV_NAME,URL_LINK,TOP_NAVIGATION_ORDER,ACTIVE FROM TOP_NAVIGATION".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }

    public function insNavigation($navigationName,$navigationOrder,$urlLink,$active){
        $query = "INSERT INTO TOP_NAVIGATION (NAV_NAME,URL_LINK,TOP_NAVIGATION_ORDER,ACTIVE) VALUES('".$navigationName."','".$urlLink."','".$navigationOrder."',".$active.")";
        $result = parent::executeQuery($query);
        return $result;
    }


    public function delNavigation($id){
        $query = "DELETE FROM TOP_NAVIGATION WHERE TOP_NAV_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

     public function updNavigation($navigationName,$naviagtionOrder,$urlLink,$active,$id){
        $query = "UPDATE TOP_NAVIGATION SET NAV_NAME = '".$navigationName."' , TOP_NAVIGATION_ORDER = '".$naviagtionOrder."' , URL_LINK = '".$urlLink."'  , ACTIVE = '".$active."' WHERE TOP_NAV_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

    public function selectAllNavigation(){

       return parent::executeQuery("SELECT TOP_NAV_ID,NAV_NAME FROM TOP_NAVIGATION");

   }

   public function insMainNavigation($name,$navigationId,$order,$urlLink,$active){
        $query = "INSERT INTO MAIN_NAVIGATION (NAV_ID,NAME,URL_LINK,MAIN_NAVIGATION_ORDER,ACTIVE) VALUES('".$navigationId."','".$name."','".$urlLink."','".$order."','".$active."')";
        $result = parent::executeQuery($query);
        return $result;
    }


    public function selMainNavigation($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " MAIN_NAV_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT M.MAIN_NAV_ID,M.NAV_ID,M.NAME,M.URL_LINK,M.MAIN_NAVIGATION_ORDER,M.ACTIVE,N.NAV_NAME FROM MAIN_NAVIGATION M LEFT JOIN TOP_NAVIGATION N ON M.NAV_ID=N.TOP_NAV_ID".$strWhere.$str;

        $result = parent::executeQuery($query);
        return $result;
    }


    public function delMainNavigation($id){
        $query = "DELETE FROM MAIN_NAVIGATION WHERE MAIN_NAV_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }


    public function updMainNavigation($name,$navigationId,$order,$urlLink,$active,$mainNavigationId){
        $query = "UPDATE MAIN_NAVIGATION SET NAME = '".$name."' , NAV_ID = '".$navigationId."'  , MAIN_NAVIGATION_ORDER= '".$order."',URL_LINK= '".$urlLink."',ACTIVE= '".$active."'  WHERE MAIN_NAV_ID = ".$mainNavigationId;
        $result = parent::executeQuery($query);
        return $result;
    }




    public function insCourseNavigation($courseId,$name,$order,$urlLink,$content,$backgroundImagecontent,$active,$backgroundImage){
        $query = "INSERT INTO COURSE_NAVIGATION (NAME,URL_LINK,COURSE_NAVIGATION_ORDER,ACTIVE,COURSE_ID,CONTENT,BACKGROUND_IMAGE_UPLOAD,BACKGROUND_IMAGE_CONTENT) VALUES('".$name."','".$urlLink."','".$order."','".$active."','".$courseId."','".$content."','".$backgroundImage."','".$backgroundImagecontent."')";
        $result = parent::executeQuery($query);
        return $result;
    }


    public function selCourseNavigation($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " COURSE_NAVIGATION_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT CN.COURSE_NAVIGATION_ID,CN.NAME,CN.URL_LINK,CN.COURSE_NAVIGATION_ORDER,CN.ACTIVE,CN.BACKGROUND_IMAGE_UPLOAD,CN.BACKGROUND_IMAGE_CONTENT,CN.COURSE_ID,CN.ACTIVE,CN.CONTENT,C.COURSE_NAME FROM COURSE_NAVIGATION CN LEFT JOIN COURSE C ON CN.COURSE_ID=C.COURSE_ID".$strWhere.$str;

        $result = parent::executeQuery($query);
        return $result;
    }


     public function delCourseNavigation($id){
        $query = "DELETE FROM COURSE_NAVIGATION WHERE COURSE_NAVIGATION_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }


    public function updCourseNavigation($courseId,$order,$urlLink,$active,$id,$content,$backgroundImage,$backgroundImagecontent){
        $query = "UPDATE COURSE_NAVIGATION SET COURSE_ID = '".$courseId."' ,URL_LINK= '".$urlLink."'  , COURSE_NAVIGATION_ORDER= '".$order."',ACTIVE= '".$active."',CONTENT= '".$content."',BACKGROUND_IMAGE_UPLOAD ='".$backgroundImage."',BACKGROUND_IMAGE_CONTENT='".$backgroundImagecontent."'  WHERE COURSE_NAVIGATION_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }



  /****deepak pandey start here******/
    public function selSubCourseHighlight($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " COURSE_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT  CH.SUB_COURSE_H_ID,CH.SUB_COURSE_ID,CH.CONTENT  ,CH.SUB_COURSE_HIGHLIGHT_ORDER, CH.ACTIVE, SB.SUB_COURSE_NAME,SB.SUB_COURSE_ID FROM SUB_COURSE_HIGHLIGHT CH LEFT JOIN SUB_COURSE SB ON CH.SUB_COURSE_ID=SB.SUB_COURSE_ID".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }
//            $query = "SELECT CA.CATEGORY_ID,CA.COURSE_ID, CA.CATEGORY_NAME,C.COURSE_NAME,CA.CATEGORY_ORDER,CA.ACTIVE FROM CATEGORY CA LEFT JOIN COURSE C ON CA.COURSE_ID=C.COURSE_ID $strWhere $str";

     public function delSubCourseHighlight($id){
        $query = "DELETE FROM SUB_COURSE_HIGHLIGHT  WHERE SUB_COURSE_H_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

     public function insSubCourseHighlight($subCourseId,$order,$content,$setting){
        $query = "INSERT INTO SUB_COURSE_HIGHLIGHT (SUB_COURSE_ID,CONTENT,SUB_COURSE_HIGHLIGHT_ORDER,ACTIVE ) VALUES('".$subCourseId."','".$content."','".$order."','".$setting."')";
        $result = parent::executeQuery($query);
        return $result;
    }
     public function updSubCourseHighlight($subCourse,$order,$content,$setting,$id){
        $query = "UPDATE SUB_COURSE_HIGHLIGHT SET SUB_COURSE_ID = '".$subCourse."' , CONTENT = '".$content."' , SUB_COURSE_HIGHLIGHT_ORDER = '".$order."' , ACTIVE = '".$setting."'  WHERE SUB_COURSE_H_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

     public function getAllSubCourse(){
    $query="select SUB_COURSE_ID,SUB_COURSE_NAME FROM SUB_COURSE";
    $result = parent::executeQuery($query);
    return $result;
}
        /****deepak pandey end here******/
     /****deepak pandey start here for event******/

    public function selAddEvent($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " COURSE_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;
        $query = "SELECT E.EVENT_ID,E.COURSE_ID,E.EVENT_NAME,E.EVENT_ORDER,E.ACTIVE,E.YEAR,E.COURSE_NAVIGATION_ID,C.COURSE_ID,C.COURSE_NAME  FROM EVENT E LEFT JOIN COURSE C ON E.COURSE_ID=C.COURSE_ID order by C.COURSE_NAME,E.EVENT_ID ASC".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }

    public function insAddEvent($course,$name,$order,$setting,$coursenav,$year){
        $query = "INSERT INTO EVENT (COURSE_ID,EVENT_NAME,EVENT_ORDER,ACTIVE,COURSE_NAVIGATION_ID,YEAR ) VALUES('".$course."','".$name."','".$order."','".$setting."','".$coursenav."','".$year."')";
        $result = parent::executeQuery($query);
        return $result;
    }
     public function delAddEvent($id){
        $query = "DELETE FROM EVENT  WHERE EVENT_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

     public function updateEvent($courseName,$name,$order,$setting,$coursenav,$year,$id){
        $query = "UPDATE EVENT SET COURSE_ID = '".$courseName."' , EVENT_NAME = '".$name."' , EVENT_ORDER = '".$order."' ,COURSE_NAVIGATION_ID='".$coursenav."' ,YEAR = '".$year."' ,  ACTIVE = '".$setting."'  WHERE EVENT_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
     public function getAllCourse(){
    $query="select COURSE_ID,COURSE_NAME FROM COURSE";
    $result = parent::executeQuery($query);
    return $result;
}
    /************for add news**********************/
    public function selNews($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " COURSE_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT N.NEWS_ID,N.COURSE_ID,N.HEADING,N.DETAIL,N.DATE,N.NEWS_ORDER,N. ACTIVE,C.COURSE_ID,C.COURSE_NAME  FROM NEWS N LEFT JOIN COURSE C ON N.COURSE_ID=C.COURSE_ID ORDER BY C.COURSE_NAME,N.NEWS_ID ASC ".$strWhere.$str;
//        SELECT E.EVENT_ID,E.COURSE_ID,E.EVENT_NAME  ,E.EVENT_ORDER,E. ACTIVE,C.COURSE_ID,C.COURSE_NAME  FROM EVENT E LEFT JOIN COURSE C ON E.COURSE_ID=C.COURSE_ID
        $result = parent::executeQuery($query);
        return $result;
    }


    public function insNews($courseName,$heading,$detail,$date,$order,$active){
        $query = "INSERT INTO NEWS (COURSE_ID,HEADING,DETAIL,DATE,NEWS_ORDER,ACTIVE ) VALUES('".$courseName."','".$heading."','".$detail."','".$date."','".$order."','".$active."')";
        $result = parent::executeQuery($query);
        return $result;
    }
      public function delNews($id){
        $query = "DELETE FROM NEWS WHERE NEWS_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
     public function updateNews($course,$heading,$detail,$date,$order,$active,$id){
        $query = "UPDATE NEWS SET COURSE_ID = '".$course."' , HEADING = '".$heading."' , DETAIL = '".$detail."' , DATE = '".$date."',NEWS_ORDER = '".$order."',ACTIVE = '".$active."'  WHERE NEWS_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    /************for add news**********************/

    /****************start for Add File******************************/
     public function insFile($courseName,$fileName,$fileUpload,$order,$active,$rank,$eventid,$year){
        $query = "INSERT INTO FILE (COURSE_ID,NAME,FILE_NAME,FILE_ORDER,ACTIVE,RANK,EVENT_ID,EVENT_YEAR) VALUES('".$courseName."','".$fileName."','".$fileUpload."','".$order."','".$active."','".$rank."','".$eventid."','".$year."')";
        $result = parent::executeQuery($query);
        return $result;
    }

    public function updFile($course,$fileName,$fileUpload,$order,$active,$id,$year,$rank,$event){
        $query = "UPDATE FILE SET COURSE_ID = '".$course."' , NAME = '".$fileName."' , FILE_NAME = '".$fileUpload."' , FILE_ORDER = '".$order."' , ACTIVE = '".$active."', EVENT_YEAR = '".$year."', RANK = '".$rank."', EVENT_ID = '".$event."'  WHERE FILE_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

    public function selFile($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " FILE_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT F.FILE_ID,F.EVENT_ID,F.NAME,F.FILE_NAME,F.COURSE_ID,F.FILE_ORDER,F.ACTIVE,F.RANK,F.EVENT_YEAR,C.COURSE_ID,C.COURSE_NAME FROM FILE F LEFT JOIN COURSE C ON F.COURSE_ID=C.COURSE_ID order by C.COURSE_NAME,F.FILE_ID ASC ".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }
      public function delFile($id){
        $query = "DELETE FROM FILE WHERE FILE_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

    /**************** end for Add File******************************/
   /****************start for Add homePopup******************************/
     public function inshomePopupimg($homePopupUpload,$active,$urlLink){
        $query = "INSERT INTO HOME_POPUP (IMAGE_PATH,ACTIVE,URL_LINK) VALUES('".$homePopupUpload."','".$active."','".$urlLink."')";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function selhomePopup(){
         $query="SELECT * FROM HOME_POPUP";
         $result = parent::executeQuery($query);
         return $result;
    }
    public function updhomePopup($homePopupUpload,$active,$id,$urlLink){
        $query = "UPDATE HOME_POPUP SET IMAGE_PATH = '".$homePopupUpload."' ,ACTIVE = ".$active.",URL_LINK = '".$urlLink."'  WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function delhomePopup($id){
        $query = "DELETE FROM HOME_POPUP WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    /**************** end for Add hOME POP UP******************************/

    /****************start FOR Add DAMS STORE CONTENT******************************/
     public function insdamsstoreContent($backgroundimageUpload,$backgroundimagecontent,$active){
        $query = "INSERT INTO DAMSTORE_CONTENT(BACKGROUND_IMAGE_UPLOAD,BACKGROUND_IMAGE_CONTENT,ACTIVE) VALUES('".$backgroundimageUpload."','".$backgroundimagecontent."','".$active."')";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function seldamsstoreContent(){
         $query="SELECT * FROM DAMSTORE_CONTENT";
         $result = parent::executeQuery($query);
         return $result;
    }
    public function upddamsstoreContent($id,$backgroundImage,$active,$backgroundimgContent){
        $query = "UPDATE DAMSTORE_CONTENT SET BACKGROUND_IMAGE_UPLOAD = '".$backgroundImage."' ,ACTIVE = ".$active.",BACKGROUND_IMAGE_CONTENT = '".$backgroundimgContent."'  WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function deldamsstoreContent($id){
        $query = "DELETE FROM DAMSTORE_CONTENT  WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    /**************** end for Add DAMS STORE CONTENT******************************/

/****************start FOR Add Virtual Tour CONTENT******************************/
     public function insvirtualtourContent($backgroundimageUpload,$active,$backgroundimagecontent,$courseId){
        $query = "INSERT INTO VIRTUAL_TOUR_CONTENT(BACKGROUND_IMAGE_UPLOAD,BACKGROUND_IMAGE_CONTENT,ACTIVE,COURSE_ID) VALUES('".$backgroundimageUpload."','".$backgroundimagecontent."','".$active."','".$courseId."')";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function selvirtualtourContent(){
         $query = "SELECT V.ID ,V.BACKGROUND_IMAGE_UPLOAD,V.BACKGROUND_IMAGE_CONTENT,V.ACTIVE,V.COURSE_ID,C.COURSE_NAME  FROM VIRTUAL_TOUR_CONTENT V LEFT JOIN COURSE C ON V.COURSE_ID=C.COURSE_ID order by C.COURSE_NAME,V.ID ASC ";
         $result = parent::executeQuery($query);
         return $result;
    }
    public function updvirtualtourContent($id,$backgroundImage,$active,$backgroundimgContent,$courseId){
        $query = "UPDATE VIRTUAL_TOUR_CONTENT SET BACKGROUND_IMAGE_UPLOAD = '".$backgroundImage."' ,ACTIVE = ".$active.",BACKGROUND_IMAGE_CONTENT = '".$backgroundimgContent."',COURSE_ID='".$courseId."'  WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function delvirtualtourContent($id){
        $query = "DELETE FROM VIRTUAL_TOUR_CONTENT  WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    /**************** end for Add Virtual tour CONTENT******************************/

/****************start FOR Add Find Center CONTENT******************************/
     public function insfindcenterContent($backgroundimageUpload,$active,$backgroundimagecontent){
        $query = "INSERT INTO FIND_CENTER_CONTENT(BACKGROUND_IMAGE_UPLOAD,BACKGROUND_IMAGE_CONTENT,ACTIVE) VALUES('".$backgroundimageUpload."','".$backgroundimagecontent."','".$active."')";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function selfindcenterContent(){
         $query = "SELECT * FROM FIND_CENTER_CONTENT";
         $result = parent::executeQuery($query);
         return $result;
    }
    public function updfindcenterContent($id,$backgroundImage,$active,$backgroundimgContent){
        $query = "UPDATE FIND_CENTER_CONTENT  SET BACKGROUND_IMAGE_UPLOAD = '".$backgroundImage."' ,ACTIVE = ".$active.",BACKGROUND_IMAGE_CONTENT = '".$backgroundimgContent."'  WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function delfindcenterContent($id){
        $query = "DELETE FROM FIND_CENTER_CONTENT WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    /**************** end for Add Find Center CONTENT******************************/

    /*********** start for ABOUT US ********/
    public function insAboutUs($aboutusName,$urlLink,$contenthome,$backgroundimg,$backgroundimgcontent,$active){
        $query = "INSERT INTO ABOUT_US(ABOUT_US_NAME,URL_LINK,BACKGROUND_IMAGE_UPLOAD,BACKGROUND_IMAGE_CONTENT,CONTENT_HOME,ACTIVE) VALUES('".$aboutusName."','".$urlLink."','".$backgroundimg."','".$backgroundimgcontent."','".$contenthome."','".$active."')";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function selAboutus(){
        $query = "SELECT * FROM ABOUT_US";
        $result = parent::executeQuery($query);
        return $result;
    }


    public function delAboutus($id){
        $query = "DELETE FROM ABOUT_US WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

     public function updAboutus($aboutusName,$urlLink,$contenthome,$backgroundimg,$backgroundimgcontent,$active,$id){
         $query = "UPDATE ABOUT_US SET ABOUT_US_NAME='".$aboutusName."',URL_LINK = '".$urlLink."' ,BACKGROUND_IMAGE_UPLOAD = '".$backgroundimg."' , BACKGROUND_IMAGE_CONTENT = '".$backgroundimgcontent."',CONTENT_HOME = '".$contenthome."',ACTIVE ='".$active."' WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return  $result;
    }
/**********end for ABOUT US *******/

    /**************** start for Add HOME PHOTO GALLERY******************************/
     public function inshomePhotogallery($homePhotogallery,$active,$order){
        $query = "INSERT INTO HOME_PHOTO_GALLERY (IMAGE_PATH,ACTIVE, PHOTO_GALLERY_ORDER) VALUES('".$homePhotogallery."','".$active."','".$order."')";
        $result = parent::executeQuery($query);
        return $result;
    }
    
    public function selhomePhotogallery(){
        $query="SELECT * FROM HOME_PHOTO_GALLERY";
         $result = parent::executeQuery($query);
         return $result;
    }
   
    public function updhomePhotogallery($homePhotogallery,$id,$active,$order){
        $query = "UPDATE HOME_PHOTO_GALLERY SET IMAGE_PATH = '".$homePhotogallery."',ACTIVE = '".$active."',PHOTO_GALLERY_ORDER = '".$order."' WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    
    public function delhomePhotogallery($id){
        $query = "DELETE FROM HOME_PHOTO_GALLERY WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    /**************** end for Add HOME PHOTO GALLERY******************************/
    
    /**************** start for Add Video******************************/

     public function insVideo($courseName,$videoName,$videoUpload,$order,$active,$event,$rank,$url,$youtube,$year){
        $query = "INSERT INTO VIDEO (COURSE_ID,NAME,FILE_NAME,VIDEO_ORDER,ACTIVE,EVENT_ID,RANK,URL_LINK,YOU_TUBE,EVENT_YEAR) VALUES('".$courseName."','".$videoName."','".$videoUpload."','".$order."','".$active."','".$event."','".$rank."','".$url."','".$youtube."','".$year."')";
        $result = parent::executeQuery($query);
        return $result;
    }

    public function updVideo($course,$videoName,$videoUpload,$order,$active,$videoRank,$videoevent,$id,$url,$youtube,$year){
        $query = "UPDATE VIDEO SET COURSE_ID = '".$course."' , NAME = '".$videoName."' , FILE_NAME = '".$videoUpload."' , VIDEO_ORDER = '".$order."' ,RANK = '".$videoRank."' ,EVENT_ID = '".$videoevent."' , ACTIVE = ".$active.", URL_LINK = '".$url."', YOU_TUBE = '".$youtube."', EVENT_YEAR = '".$year."'  WHERE VIDEO_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

    public function selVideo($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " VIDEO_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;
      //SELECT F.FILE_ID,F.NAME,F.FILE_NAME,F.COURSE_ID,F.FILE_ORDER,F.ACTIVE,C.COURSE_ID,C.COURSE_NAME FROM FILE F LEFT JOIN COURSE C ON F.COURSE_ID=C.COURSE_ID
        $query = "SELECT  V.VIDEO_ID,V.COURSE_ID,V.EVENT_ID,V.NAME,V.FILE_NAME,V.VIDEO_ORDER,V.ACTIVE,V.RANK,V.URL_LINK,V.YOU_TUBE,V.EVENT_YEAR,C.COURSE_ID,C.COURSE_NAME FROM VIDEO V LEFT JOIN COURSE C ON V.COURSE_ID=C.COURSE_ID ORDER BY C.COURSE_NAME,V.VIDEO_ID ASC".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }
      public function delVideo($id){
        $query = "DELETE FROM VIDEO WHERE VIDEO_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }


    /**************** end for Add Video******************************/
      /**************** added by deepak pandey  for Add Center******************************/

    public function insCenter($courseName,$name,$address1,$address2,$address3,$address4,$phone,$mobile,$email,$lat,$long1,$order,$active,$state,$city,$keywords,$description,$title){
        $query = "INSERT INTO CENTRE (CENTRE_NAME,ADDRESS_LINE1,ADDRESS_LINE2,ADDRESS_LINE3,ADDRESS_LINE4,PHONE,MOBILE,EMAIL,GEO_LAT,GEO_LONG,CENTRE_ORDER,ACTIVE,STATE_ID,CITY_ID,KEYWORDS,DESCRIPTION,TITLE) VALUES('".$name."','".$address1."','".$address2."','".$address3."','".$address4."','".$phone."','".$mobile."','".$email."','".$lat."','".$long1."','".$order."','".$active."','".$state."','".$city."','".$keywords."','".$description."','".$title."')";
        $result = parent::executeQuery($query);
        $centreId=mysql_insert_id();

        $query1 = "INSERT INTO CENTRE_COURSE (COURSE_ID,CENTRE_ID) VALUES('".$courseName."','".$centreId."')";
             $result1 = parent::executeQuery($query1);
             return $result1;
    }

    public function updCenter($courseName,$name,$address1,$address2,$address3,$address4,$phone,$mobile,$email,$lat,$long1,$order,$active,$id,$state,$city,$keywords,$description,$title){
        $query = "UPDATE CENTRE SET CENTRE_NAME = '".$name."', ADDRESS_LINE1 = '".$address1."',ADDRESS_LINE2 = '".$address2."',ADDRESS_LINE3 = '".$address3."',CITY_ID = '".$city."',STATE_ID = '".$state."',ADDRESS_LINE4 = '".$address4."', PHONE = '".$phone."' , MOBILE = '".$mobile."' , EMAIL = '".$email."',GEO_LAT = '".$lat."',GEO_LONG='".$long1."' , CENTRE_ORDER = '".$order."',ACTIVE='".$active."',KEYWORDS='".$keywords."',DESCRIPTION='".$description."',TITLE='".$title."' WHERE CENTRE_ID = ".$id;
        $query1 = "UPDATE CENTRE_COURSE SET COURSE_ID = '".$courseName."' WHERE CENTRE_ID = ".$id;
        
        $result1 = parent::executeQuery($query1);
        $result = parent::executeQuery($query);
        return $result;
        return $result1;
    }

    public function selCenter($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " COURSE_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;
       $query = "SELECT CC.COURSE_ID,CO.COURSE_NAME ,C.* FROM CENTRE C LEFT JOIN CENTRE_COURSE CC ON C.CENTRE_ID=CC.CENTRE_ID LEFT JOIN COURSE CO ON CC.COURSE_ID=CO.COURSE_ID";
        $result = parent::executeQuery($query);
        return $result;
    }
      public function delCenter($id){
        $query = "DELETE FROM CENTRE WHERE CENTRE_ID = ".$id;
        //$query1 = "DELETE FROM CENTRE_COURSE WHERE CENTRE_ID = ".$id;
        $result = parent::executeQuery($query);
       // $result1 = parent::executeQuery($query1);
        return $result;

    }
      /**************** end for Add Center******************************/

     /**************** added for Add Contact Us******************************/

    public function insContact($name,$address1,$address2,$address3,$address4,$phone,$mobile,$email,$lat,$long1,$order,$active){
        $query = "INSERT INTO CONTACT_US (OFFICE_NAME,ADDRESS_LINE1,ADDRESS_LINE2,ADDRESS_LINE3,ADDRESS_LINE4,PHONE,MOBILE,EMAIL,GEO_LAT,GEO_LONG,CONTACT_US_ORDER,ACTIVE) VALUES('".$name."','".$address1."','".$address2."','".$address3."','".$address4."','".$phone."','".$mobile."','".$email."','".$lat."','".$long1."','".$order."','".$active."')";
        $result = parent::executeQuery($query);
        return $result;
    }

   public function updContact($name,$address1,$address2,$address3,$address4,$phone,$mobile,$email,$lat,$long1,$order,$active,$id){
        $query = "UPDATE CONTACT_US SET  OFFICE_NAME = '".$name."' ,ADDRESS_LINE1='".$address1."' ,ADDRESS_LINE2='".$address2."' ,ADDRESS_LINE3='".$address3."' ,ADDRESS_LINE4='".$address4."' , PHONE = '".$phone."' , MOBILE = '".$mobile."' , EMAIL = '".$email."',GEO_LAT = '".$lat."',GEO_LONG='".$long1."' , CONTACT_US_ORDER = '".$order."',ACTIVE='".$active."' WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }

    public function selContact($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " VIDEO_ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT ID,OFFICE_NAME,ADDRESS_LINE1,ADDRESS_LINE2,ADDRESS_LINE3,ADDRESS_LINE4,PHONE,MOBILE,EMAIL,GEO_LAT,GEO_LONG,CONTACT_US_ORDER,ACTIVE FROM CONTACT_US".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }
      public function delContact($id){
        $query = "DELETE FROM CONTACT_US WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function getNavigationOfCourse($id){
        $query="select COURSE_NAVIGATION_ID,NAME from COURSE_NAVIGATION where COURSE_ID=$id and ACTIVE=1";
         $result = parent::executeQuery($query);
        return $result;
    }
    public function getEventOfCourse($id,$cId){
        $query="select EVENT_ID,EVENT_NAME from EVENT where YEAR='$id' and COURSE_ID='$cId' and ACTIVE=1";
         $result = parent::executeQuery($query);
        return $result;
    }
    public function getYearOfCourse($id){
        $query="select distinct(YEAR) from EVENT where COURSE_ID=$id";
         $result = parent::executeQuery($query);
        return $result;
    }
    public function selectAllState(){
        $query="SELECT STATE_ID,STATE_NAME FROM STATE";
        $result=parent::executeQuery($query);
        return $result;
    }
    public function getCityOfState($id){
        $query="SELECT CITY_ID,CITY_NAME from CITY where STATE_ID=$id order by CITY_NAME";
        $result=parent::executeQuery($query);
        return $result;
    }
  public function getProductCOfCourse($id){
      $query="SELECT PRODUCT_CATEGORY_ID,PRODUCT_CATEGORY_NAME from PRODUCT_CATEGORY where COURSE_ID=$id AND ACTIVE=1";
      $result=parent::executeQuery($query);
        return $result;
  }
  public function getProductSOfCourse($id){
      $query="SELECT PRODUCT_ID,PRODUCT_NAME,COST from PRODUCT where PRODUCT_CATEGORY_ID=$id and ACTIVE=1 ";
      $result=parent::executeQuery($query);
        return $result;
  }
  public function getCategory1($id){
      $query="SELECT CATEGORY_ID,CATEGORY_NAME from CATEGORY where COURSE_ID=$id ";
        $result=parent::executeQuery($query);
        return $result;
  }
  public function getCategory1ByNav($id){
      $query="SELECT CATEGORY_ID,CATEGORY_NAME from CATEGORY where COURSE_NAVIGATION_ID=$id AND ACTIVE=1 ";
        $result=parent::executeQuery($query);
        return $result;
  }
  public function getCategory2($id){
      $query="SELECT ID,NAME from CATEGORY2 where CATEGORY1_ID=$id AND ACTIVE=1";
        $result=parent::executeQuery($query);
        return $result;
  }
  public function insPages($pname,$url,$pageContent,$pageHeader,$active){
      $query="INSERT INTO PAGE(PAGE_NAME,PAGE_CONTENT,PAGE_HEADER,PAGE_URL,ACTIVE) VALUES('".$pname."','".$pageContent."','".$pageHeader."','".$url."','".$active."')";
      $result=parent::executeQuery($query);
        return $result;
  }
public function updPages($pageName,$urlLink,$pageContent,$pageHeader,$active,$id){
        $query = "UPDATE PAGE SET PAGE_NAME = '".$pageName."' , PAGE_URL = '".$urlLink."' ,PAGE_CONTENT = '".$pageContent."' , PAGE_HEADER = '".$pageHeader."',ACTIVE = '".$active."' WHERE PAGE_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
public function delPages($id){
        $query = "DELETE FROM PAGE WHERE PAGE_ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    /**************** end for Add Center******************************/
 public function getAllBanner( $id = null){
     if($id!= null && $id!= '' ):
         $query = "SELECT * FROM HOME_BANNER where ID = '$id'";
       else:
         $query = "SELECT * FROM HOME_BANNER ";
       endif;  
        $result = parent::executeQuery($query);
        return $result;
 }
 public function delBanner($id) {
     $sql = "DELETE FROM HOME_BANNER WHERE ID = '$id'" ;
     $result = parent::executeQuery($sql);
     return $result;
  }
  
  public function getAllUseLink($id) {
      if($id!= null && $id!= '' ):
         $query = "SELECT * FROM USEFUL_LINK where ID = '$id'";
       else:
         $query = "SELECT * FROM USEFUL_LINK ";
       endif;  
        $result = parent::executeQuery($query);
        return $result;
  }
  public function getAllTopperTalk($id) {
      if ($id != null && $id != ''):
            $query = "SELECT * FROM TOPPER_TALKS where ID = '$id'";
        else:
            $query = "SELECT * FROM TOPPER_TALKS ";
        endif;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function delTopperTalk($id){
        $sql = "DELETE FROM TOPPER_TALKS WHERE ID = '$id'" ;
        $result = parent::executeQuery($sql);
        return $result;
    }
    
    public function getAllPhotoGallery($id) {
      if ($id != null && $id != ''):
            $query = "SELECT * FROM PHOTO_GALLERY where ID = '$id'";
        else:
            $query = "SELECT * FROM PHOTO_GALLERY ";
        endif;
        $result = parent::executeQuery($query);
        return $result;
    }
    
    public function delPhotoGall($id){
        $sql = "DELETE FROM PHOTO_GALLERY WHERE ID = '$id'" ;
        $result = parent::executeQuery($sql);
        return $result;
    }
    public function delUseFullLinK($id){
         $sql = "DELETE FROM USEFUL_LINK WHERE ID = '$id'" ;
        $result = parent::executeQuery($sql);
        return $result;
    }
    public function getAllNewsAndEvent($id){
        if ($id != null && $id != ''):
            $query = "SELECT * FROM HOME_NEWS where ID = '$id'";
        else:
            $query = "SELECT * FROM HOME_NEWS ";
        endif;
        
        $result    = parent::executeQuery($query);
        return $result;
    }
    
    public function delNewsAndEvent($id){
         $sql = "DELETE FROM HOME_NEWS WHERE ID = '$id'" ;
        $result = parent::executeQuery($sql);
        return $result;
    }
   public function getMaxId($table){
        $sql    = "SELECT MAX(ID) as ID FROM $table LIMIT 0,1 ";
        $result = parent::executeQuery($sql);
        $row    = mysql_fetch_object($result) ;
        return $row->ID;
   } 
   public function getPagination($limit, $path, $table, $column, $like) {
        return parent::Pagination($table, $limit, $path, $column, $like);
    }
}
?>
