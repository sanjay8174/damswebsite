<?php $objDatabase = new Database(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $objDatabase->getData("title"); ?></title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js" type="text/javascript"></script>
        <script src="vendors/jquery-1.9.1.js" type="text/javascript"></script>
    </head>

    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Admin Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                           
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user" style="background-position: -263px 0px;"></i> Select Panel<i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="index.php?p=11&ln=1&t=6">Dams Website</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href='index.php?p=12&ln=1&t=7'>Dams Test Series</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['USERNAME']; ?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="#">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href='view/login/logout.php'>Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <?php if ($_GET['t'] != '7') { ?>
                        <ul class="nav">
                                <li style="" class="active">    
                                    <a href="#" style="margin-right: 79px;">Dams Website</a>
                                </li>
                            
                            <li <?php if ($_GET['t'] == '6') { ?>class="active" <?php } ?>>
                                <a href="index.php?p=11&ln=1&t=6">Home</a>
                            </li>

                            <li <?php if ($_GET['t'] == '1') { ?>class="active" <?php } ?>>
                                <a href="index.php?p=1&ln=2&t=1">Category</a>
                            </li>
                            <li <?php if ($_GET['t'] == '2') { ?>class="active" <?php } ?>>
                                <a href="index.php?p=2&ln=6&t=2">Navigation</a>
                            </li>
                            <li style="display: none;" <?php if ($_GET['t'] == '3') { ?>class="active" <?php } ?>>
                                <a href="index.php?p=10&ln=1&t=3">Page</a>
                            </li>
                            <li <?php if ($_GET['t'] == '4') { ?>class="active" <?php } ?>>
                                <a href="index.php?p=3&ln=9&t=4">Content</a>
                            </li>
                            <li style="display: none;" <?php if ($_GET['t'] == '5') { ?>class="active" <?php } ?>>
                                <a href="index.php?p=8&ln=16&t=5">Achievement</a>
                            </li>
                        </ul>
                        <?php } if ($_GET['t'] == '7') { ?>
                            
                            <ul class="nav">
                            
                                <li style="" class="active" >
                                    <a href="#" style="margin-right: 79px;">Dams Test Series</a>
                                </li>
                             
                            <li <?php if ($_GET['t'] == '7') { ?>class="active" <?php } ?>>
                                <a href="index.php?p=12&ln=1&t=7">Home</a>
                            </li>
                            </ul>
                        <?php } ?>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>