<?php
include('dao.php');
//$countCourse = 4;






?>
<div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                         <li style="display: none;" <?php if($_GET['ln']=='4'  && $_GET['t']=='2'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=2&ln=4&t=2"><i class="icon-chevron-right"></i> Top Navigation</a>
                        </li>
                        <li style="display: none;" <?php if($_GET['ln']=='5'  && $_GET['t']=='2'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=2&ln=5&t=2"><i class="icon-chevron-right"></i> Main Navigation</a>
                        </li>
                        <li <?php if($_GET['ln']=='6' && $_GET['t']=='2'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=2&ln=6&t=2"><i class="icon-chevron-right"></i> Course Navigation</a>
                        </li>
                          </ul>
                </div>
                <!--/span-->

                 <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                      <div>
                      <div class="row-fluid" <?php if($_GET['ln']=='4'){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Top Navigation</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countCourse; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Navigation Name</th>
                                                <th>Url Link</th>
                                                <th>Navigation Order</th>
                                                <th>Active</th>

                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="navigationData">
                                       

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div id="navigationStart" class="row-fluid" <?php if($_GET['ln']=='4'){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                         <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Top Navigation</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form  class="form-horizontal" action="javascript:void(0)" onsubmit="callNavigationData.saveData();">
                                        <input type="hidden" class="span6" id="modeNavigation"  data-provide="typeahead" value="1">
                                        <input type="hidden" class="span6" id="idNavigation"  data-provide="typeahead" value="">
                                        <fieldset>
                                        <legend><span id="editNavigation">Add</span> Top Navigation</legend>

                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Navigation Name</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="navigationField1"  data-provide="typeahead" data-items="4" data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'>
                                          </div>
                                        </div>
                                        

                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Navigation Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="navigationField2"  data-provide="typeahead" data-items="4" data-source='["1","2","3","4","5","6","7","8","9","10"]'>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Url Link</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="navigationField3"  data-provide="typeahead" data-items="4" data-source='["https://www.damsdelhi.com","https://sky.damsdelhi.com","https://mci.damsdelhi.com","https://mds.damsdelhi.com","https://localhost/damsCMS"]'>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="optionsCheckbox">Settings</label>
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="navigationField4" value="option1">
                                              Want to Active the Navigation
                                            </label>
                                          </div>
                                        </div>



                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                          <button type="reset" id="cancelNavigationData" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                     </div>

                      <div>
                    <div class="row-fluid" <?php if($_GET['ln']=='5'){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Main Navigations</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countCourse; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Main Navigation Name</th>
                                                <th>Navigation Name</th>
                                                <th>Order</th>                                               
                                                <th>Url Link</th>
                                                <th>Active</th>

                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="mainNavigationData">
                                       

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    <div id="mainNavigationStart" class="row-fluid" <?php if($_GET['ln']=='5'){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                         <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Main Navigation</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form  class="form-horizontal" action="javascript:void(0)" onsubmit="callMainNavigationData.saveData();">
                                      <input type="hidden" class="span6" id="modeMainNavigation"  data-provide="typeahead" value="1">
                                      <input type="hidden" class="span6" id="idMainNavigation"  data-provide="typeahead" value="">
                                        <fieldset>
                                        <legend><span id="editMainNavigation">Add</span> Main Navigation</legend>

                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Navigation Name</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="mainNavigationField1"  data-provide="typeahead" data-items="4" data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="select01">Select Navigation</label>
                                          <div class="controls">
                                            <select id="mainNavigationField2" class="chzn-select">


                                              <option selected="selected" disabled='disabled' value="">Select Navigation</option>
             <?php $mainNavigationFieldDao= new dao();  $result=$mainNavigationFieldDao->selectAllNavigation();
                        while ($row=mysql_fetch_object($result)){?>
                                              <option   value="<?php echo $row->TOP_NAV_ID;?>"> <?php echo rawurldecode(html_entity_decode($row->NAV_NAME,ENT_QUOTES));?></option>

             <?php }?>

                                            </select>
                                          </div>
                                        </div>
<!--                                        <div class="control-group">
                                          <label class="control-label" for="select01">Select Level</label>
                                          <div class="controls">
                                            <select id="mainNavigationField3" class="chzn-select">
                                              <option>Select Level</option>
                                              <option>2</option>
                                              <option>3</option>
                                              <option>4</option>
                                              <option>5</option>
                                            </select>
                                          </div>
                                        </div>-->
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Navigation Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="mainNavigationField4"  data-provide="typeahead" data-items="4" data-source='["1","2","3","4","5","6","7","8","9","10"]'>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Url Link</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="mainNavigationField5"  data-provide="typeahead" data-items="4" data-source='["https://www.damsdelhi.com","https://sky.damsdelhi.com","https://mci.damsdelhi.com","https://mds.damsdelhi.com","https://localhost/damsCMS"]'>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="optionsCheckbox">Settings</label>
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="mainNavigationField6" value="option1">
                                              Want to Active the Navigation
                                            </label>
                                          </div>
                                        </div>



                                        <div class="form-actions">
                                          <button type="submit"  class="btn btn-primary">Save changes</button>
                                          <button type="reset" id="cancelMainNavigationData" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                     </div>
                     <div>
                    <div class="row-fluid" <?php if($_GET['ln']=='6'){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Course Navigations</div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countCourse; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Course Name</th>
                                                <th> Navigation Name</th>
                                                <th style="display: none;">Order</th>
                                                <th>Url Link</th>
                                                <th>Background Image</th>
                                                 <th>Active</th>
                                                <th style="text-align: center">Edit</th>
<!--                                                <th style="text-align: center">Delete</th>-->
                                            </tr>
                                        </thead>
                                        <tbody id="courseNavigationData">
                                       

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                     <div id="courseNavigationStart" class="row-fluid" <?php if($_GET['ln']=='6'){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                         <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Course Navigation</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form  class="form-horizontal"  action="javascript:void(0)" onsubmit="callCourseNavigationData.saveData();">
                                      <input type="hidden" class="span6" id="modeCourseNavigation"  data-provide="typeahead" value="1">
                                      <input type="hidden" class="span6" id="idCourseNavigation"  data-provide="typeahead" value="">
                                        <fieldset>
                                        <legend><span id="editCourseNavigation">Add</span> Course Navigation</legend>
                                         <div class="control-group">
                                          <label class="control-label" for="select01"><span style="color: red;">*</span>Select Course</label>
                                          <div class="controls">
                                            <select id="courseNavigationField1" class="chzn-select" onChange="callCourseNavigationData.changeCourseNav(this.value);">
                                              <option selected="selected" disabled='disabled' value="" >Select Course</option>
                                                <?php $courseDao= new dao();  $result=$courseDao->selectAllCourse();
                                                 while ($row=mysql_fetch_object($result)){?>
                                              <option   value="<?php echo $row->COURSE_ID;?>" > <?php echo rawurldecode(html_entity_decode($row->COURSE_NAME,ENT_QUOTES));?></option>

                                             <?php }?>

                                            </select>
                                          </div>
                                        </div>
                                        <div class="control-group" style="display: none;">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Navigation Name</label>
                                          <div class="controls">
<!--                                            <input type="text" class="span6" id="courseNavigationField2"  data-provide="typeahead" data-items="4" data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'>-->
                                          </div>
                                        </div>
                                        <div class="control-group" id="coursenavdiv">
                                          <label class="control-label" for="select01"><span style="color: red;">*</span>Select Course Navigation</label>
                                          <div class="controls">
                                              <select id="courseNavigationField2" class="chzn-select">
                                                    <option selected="selected" disabled='disabled' value="">Select Navigation</option>
                                              </select> </div>
                                          </div>
                                        <div class="control-group" style="display: none;">
                                          <label class="control-label" for="typeahead">Navigation Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="courseNavigationField3"  data-provide="typeahead" data-items="4" data-source='["1","2","3","4","5","6","7","8","9","10"]'>
                                          </div>
                                        </div>
                                        
                                        <!-- Added by shruti for background image upload for each navigation-->
                                        <div class="control-group">
                                          <label class="control-label" for="fileInput">Background Image upload<br>(Image in jpeg,jpg or png)</label>
                                          <div class="controls">
                                            <input class="input-file uniform_on" id="courseNavigationField7" name="courseNavigationImage" type="file">&nbsp;&nbsp;&nbsp;<b>Dimension should be Min(921X318) and Max(1280X500) (in pixels)</b>
                                          </div>
                                        </div>
                                        <input type="hidden" value='' id="upldbackgroundImage"/>
                                        <!-- Added by shruti for background image content for each navigation-->
                                        <div class="control-group">
                                          <label style="margin-right:20px; vertical-align:top;padding-top: 24px" class="control-label" for="textarea2">Background Image Content</label>
                                          <div id="courseNavigationField8"class="block-content collapse in">
		                               <textarea id="ckeditor_standard"></textarea>
		                            </div>
		                        </div>

                                        <!-- ADDED BY SHRUTI home content for each navigation-->
                                       <div class="control-group">
                                          <label style="margin-right:20px; vertical-align:top;padding-top: 24px" class="control-label" for="textarea2">Content</label>
                                          <div id="courseNavigationField6"class="block-content collapse in">
		                               <textarea id="ckeditor_standard"></textarea>
		                            </div>
		                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Url Link</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="courseNavigationField4"  data-provide="typeahead" data-items="4" data-source='["https://www.damsdelhi.com","https://sky.damsdelhi.com","https://mci.damsdelhi.com","https://mds.damsdelhi.com","https://localhost/damsCMS"]'>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="optionsCheckbox">Settings</label>
                                          <div class="controls">
                                            <label class="uniform">
                                              <input class="uniform_on" type="checkbox" id="courseNavigationField5" value="option1">
                                              Want to Active the Navigation
                                            </label>
                                          </div>
                                        </div>
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                          <button id="cancelCourseNavigationData" type="reset" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    </div>
                    </div>
                      </div>
            </div>
            <hr>
            <footer>
            </footer>
        </div>
        
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>

        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>
       <script src="vendors/ckeditor/ckeditor.js"></script> <!-- added by shruti from ckEditor in course navigation -->
        <script src="vendors/ckeditor/adapters/jquery.js"></script>


        <script src="assets/scripts.js"></script>
        <script type="text/javascript" src="view/layout/layout.js" ></script>
        <script>
        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
             $( 'textarea#ckeditor_standard' ).ckeditor({width:'90%', height: '150px', toolbar: [
				{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
				[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
                                { name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
                                { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
                                { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
                                '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
                                { name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow' } },
                                { name: 'links', items : [ 'Link','Unlink','Anchor' ] }
			]}); 
            $('.textarea').wysihtml5();
            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        <?php if($_GET['ln']=='4'){ ?>
    var callNavigationData = new navigationData();

    callNavigationData.getDataAll();
    
    <?php } ?>
        <?php if($_GET['ln']=='5'){ ?>
    var callMainNavigationData = new mainNavigationData();

    callMainNavigationData.getDataAll();

    <?php } ?>

        <?php if($_GET['ln']=='6'){ ?>
    var callCourseNavigationData = new courseNavigationData();

    callCourseNavigationData.getDataAll();

    <?php } ?>
        </script>
    </body>

</html>