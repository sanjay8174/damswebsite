<?php
ob_start();
include('dao.php');
include('preloader.html');
//echo "hello";
?>
  <style>
.table td{text-align:center !important;}
.table th{text-align:center !important;}
thead tr:first-child th{width:1% !important;}
.crossImg{
 width: 17px;
 margin-left: 18px;
 display: none;
}
</style>
        <div   class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li  <?php if($_GET['ln']=='1' && $_GET['t']=='7'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=12&ln=1&t=7"><i class="icon-chevron-right"></i> Banner</a>
                        </li>
                        <li <?php if($_GET['ln']=='2' && $_GET['t']=='7'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=12&ln=2&t=7"><i class="icon-chevron-right"></i> Course</a>
                        </li>
                        <li <?php if($_GET['ln']=='6' && $_GET['t']=='7'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=12&ln=6&t=7"><i class="icon-chevron-right"></i>Package</a>
                        </li>
<!--                        <li <?php// if($_GET['ln']=='4' && $_GET['t']=='7'){ ?>class="active" <?php// } ?>>
                            <a href="index.php?p=12&ln=4&t=7"><i class="icon-chevron-right"></i>Page Headings</a>
                        </li>-->
                        <li <?php if($_GET['ln']=='5' && $_GET['t']=='7'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=12&ln=5&t=7"><i class="icon-chevron-right"></i>FAQ</a>
                        </li>
                        <li <?php if($_GET['ln']=='3' && $_GET['t']=='7'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=12&ln=3&t=7"><i class="icon-chevron-right"></i>Images</a>
                        </li>
                        <li <?php if($_GET['ln']=='7' && $_GET['t']=='7'){ ?>class="active" <?php } ?>>
                            <a href="index.php?p=12&ln=7&t=7"><i class="icon-chevron-right"></i>Video</a>
                        </li>
                        
                    </ul>
                </div>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->

                      <div>
                          <div class="row-fluid" <?php if($_GET['ln']=='1' && ( $_GET['e']=='0' || !isset($_GET['e'])) ){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                            <input type="hidden" id="banner_StartPoint" value="0"> 
                            <input type="hidden" id="banner_EndPoint" value="0"> 
                            <input type="hidden" id="banner_total" value="0"> 
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=12&ln=1&t=7&e=0">Banner</a></div> 
                                    <div class="muted pull-left" style="margin-left:20px;"><a href="index.php?p=12&ln=1&t=7&e=1">Add Banner</a></div>
                                    <div class="pull-right"><span class="badge badge-info"><?php //echo $countCourse; ?></span></div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Banner</th>
                                                <!--<th>Banner Text</th>-->
                                                <th>Order</th>
                                                <!--<th>Image</th>-->
                                                <th>Active</th>
                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="homeBannerData">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>

                     <div id="courseStart" class="row-fluid" <?php if($_GET['ln']=='1' &&( $_GET['e']=='1' || $_GET['e']=='2' ) ){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                        <div class="block">
                            
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=12&ln=1&t=7&e=0">Banner</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                      <form class="form-horizontal"   action="javascript:void(0)" onsubmit="callHomeData.saveData();">
                                      <input type="hidden" class="span6" id="modeBanner"  data-provide="typeahead" value="1">
                                      <input type="hidden" class="span6" id="idBanner"  data-provide="typeahead" value="">
                                        <fieldset>
                                        <legend><span id="editBanner"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> Banner</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Banner Name</label>
                                          <div class="controls">
                                              <input type="text" class="span6" name="bannerField1" id="bannerField1" maxlength="25" value='' style="" placeholder="Add Banner Name"/><img id="bannerNameError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Banner Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" id="bannerField2" style="" maxlength="5" placeholder="Add Banner Order" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" data-provide="typeahead" data-items="4" ><img id="bannerOrderError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                          </div>
                                        </div>
                                         <div class="control-group" style="display:block;">
                                          <label class="control-label" for="fileInput"><span style="color: red;">*</span>Image upload</label>
                                          <div class="controls">
                                            <input class="input-file uniform_on" id="bannerField3" name="bannerImage" type="file" onchange="tabs.imageValidation(this,this.id);"/>
                                            <input class="input-file uniform_on" id="bannerFEdit3" name="bannerFEdit3" type="hidden" /><img id="bannerImgError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>(Dimension height:500px to 520px width:1350px to 1400px)
                                              </div>
                                        </div>
                                        <div class="control-group" style="display:none;">
                                          <label style="margin-right:20px; vertical-align:top;padding-top: 24px" class="control-label" for="textarea2">Content(Banner)</label>
                                          <div id="bannerField4"class="block-content collapse in">
                                              <textarea rows="4" class="span6" cols="50" id="bannerField4" maxlength="470" style="" placeholder="Add Banner Content"></textarea><img id="bannerContentError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
		                          </div>
		                        </div>
                                        <div class="control-group">
                                          <div class="controls">
                                            <label class="uniform" style="width:29%;">
                                              <input class="uniform_on" type="checkbox" id="bannerField5" value="1" />
                                              Want to Activate the Banner
                                            </label>
                                          </div>
                                        </div>
                                        <div class="form-actions">
                                          <input type="hidden" id="bannerHiddenSave" value="" />
                                          <button id="bannerSubmit1" type="submit"  class="btn btn-primary">Save</button>
                                          <button id="bannerSubmit2" type="submit"  class="btn btn-primary">Save & New</button>
                                          <button type="button"  id="cancelBannerData" class="btn" onclick="callHomeData.cancel();">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    </div>
                      
                      
                      
      <!--- course start -->                
                      
                    <div>
                      <div class="row-fluid" <?php if($_GET['ln']=='2' && ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                            <input type="hidden" id="course_StartPoint" value="0"> 
                            <input type="hidden" id="course_EndPoint" value="0"> 
                            <input type="hidden" id="course_total" value="0"> 
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=12&ln=2&t=7&e=0">Course</a></div>
                                    <div class="muted pull-left" style="margin-left:20px;"><a href="index.php?p=12&ln=2&t=7&e=1">Add Course</a>
                                    </div>
<!--                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countCourse; ?></span>

                                    </div>-->
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Course </th>
                                                <th> Order</th>
                                                <th>Active</th>

                                                <th style="text-align: center">Edit</th>
                                            </tr>
                                        </thead>
                                        <tbody id="courseData">
                                       
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                     <div id="categoryStart" class="row-fluid" <?php if($_GET['ln']=='2' &&( $_GET['e']=='1' || $_GET['e']=='2' )){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=12&ln=2&t=7&e=0">Course</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form class="form-horizontal"   action="javascript:void(0)" onsubmit="callCourseData.saveData();">
                                      <input type="hidden" class="span6" id="modeCourse"  data-provide="typeahead" value="1">
                                      <input type="hidden" class="span6" id="idCourse"  data-provide="typeahead" value="">
                                        <fieldset>
                                        <legend><span id="editCourse">Add</span> Course</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Course Name</label>
                                          <div class="controls">
                                              <input type="text" class="span6" id="courseField1" maxlength="25" placeholder="Add Course Name"  data-provide="typeahead" data-items="4" data-source='["DAMS SKY","MCI SCREENING","MDS QUEST","USMLE EDGE"]'><img id="courseNameError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                          </div>
                                        </div>
                                        
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Course Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" maxlength="5" id="courseField2" placeholder="Add Course Order" data-provide="typeahead" data-items="4" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$4'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');"><img id="CourseOrderError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                          </div>
                                        </div>
                                        
                                        <div class="control-group">
                                          <div class="controls">
                                            <label class="uniform" style="width:29%;">
                                              <input class="uniform_on" type="checkbox" id="courseField15" value="option1">
                                              Want to Activate the Course
                                            </label>
                                          </div>
                                        </div>
                                        <div class="form-actions">
                                          <input type="hidden" id="courseHiddenSave" value="" />
                                          <button id="courseSubmit1" type="submit"  class="btn btn-primary">Save</button>
                                          <button id="courseSubmit2" type="submit"  class="btn btn-primary">Save & New</button>
                                          <button type="button" onclick="callCourseData.cancel();"  id="cancelCourseData" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    </div>
      
                <!-- ******START: Package********************************************* -->
                    <div>
                        <div class="row-fluid" <?php if($_GET['ln']=='6' && ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                            <input type="hidden" id="package_StartPoint" value="0"> 
                            <input type="hidden" id="package_EndPoint" value="0"> 
                            <input type="hidden" id="package_total" value="0">     
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=12&ln=6&t=7&e=0">Package</a></div>
                                    <div class="muted pull-left" style="margin-left:20px;"><a href="index.php?p=12&ln=6&t=7&e=1">Add Package</a>
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Package Name</th>
                                                <th>Course</th>
                                                <th>Cost</th>
                                                <th>Order</th>
                                                <th>Total Tabs</th>
                                                <th>Active</th>
                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="packageData"></tbody>
                                        <input type="hidden" id="pacakgeshowCount" value="0">
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        
                        <div  id="subcategoryStart" class="row-fluid" <?php if($_GET['ln']=='6'  &&( $_GET['e']=='1' || $_GET['e']=='2' )){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                           <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                   <div class="muted pull-left"><a href="index.php?p=12&ln=6&t=7&e=0">Package</a></div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <form enctype="multipart/form-data"    class="form-horizontal" id="packageDataForm"  action="javascript:void(0)" onsubmit="packageData.saveData();">
                                            <input type="hidden" class="span6" id="modePackage"  data-provide="typeahead" value="1">
                                            <input type="hidden" class="span6" id="packageId"  data-provide="typeahead" value="">

                                            <fieldset>
                                                <legend><span id="editPackage"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> Package</legend>
                                                <div class="control-group">
                                                    <label class="control-label"><span style="color: red;">*</span>Course</label>
                                                    <div class="controls">
                                                        <select id="packageCourse"  name="packageCourse">
                                                         <option selected="selected" disabled='disabled' value="">Select Course</option>
                                                        </select><img id="packageCourseError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label"><span style="color: red;">*</span>Package Name</label>
                                                    <div class="controls">
                                                        <input type="text" class="span6" id="packageName"maxlength="145"   name="packageName" placeholder="Add Package Name" /><img id="packageNameError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label"><span style="color: red;">*</span>Package Detail Name</label>
                                                    <div class="controls">
                                                        <input type="text" class="span6" id="packageDetailName" maxlength="470"  name="packageDetailName" placeholder="Add Package Detail Name"   /><img id="packageDetailNameError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label"><span style="color: red;">*</span>Package Cost</label>
                                                    <div class="controls">
                                                        <input type="text" class="span6" maxlength="7" id="packageCost"  name="packageCost" placeholder="Add Package Cost" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$4'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');"  /><img id="packageCostError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label"><span style="color: red;">*</span>Package URL</label>
                                                    <div class="controls">
                                                        <input type="text" class="span6" id="packageLink"  name="packageLink" placeholder="Add Package Link" /><img id="packageLinkError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label"><span style="color: red;">*</span>Package Order</label>
                                                    <div class="controls">
                                                        <input type="text" class="span6" maxlength="5" id="packageOrder"  name="packageOrder" placeholder="Add Package Order" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$4'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');"  /><img id="packageOrderError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <label class="uniform" style="width:32%;">
                                                        <input class="uniform_on" type="checkbox" id="packageActive" value="1" />
                                                        Want to Activate the Package
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label"><b>Package Description</b></label>
                                                 </div>
                                                
                                                
                                                <div class="control-group">
                                                    <label class="control-label">Duration</label>
                                                    <div class="controls">
                                                        <input type="text" maxlength="100" class="span6" id="packageDuration"   name="packageDuration"   /><img id="packageDurationError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Total No of Test</label>
                                                    <div class="controls">
                                                        <input type="text" maxlength="100" class="span6" id="totalNoOfTest"  name="totalNoOfTest" /><img id="packagenoOfTestError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label style="margin-right:20px; vertical-align:top;padding-top: 24px" class="control-label" for="textarea2">Total No of Question Per Test</label>
                                                    <div id="bannerField4"class="block-content collapse in">
                                                        <textarea rows="4" maxlength="500" class="span6" cols="50" name="totalQuesPerTest" id="totalQuesPerTest" style="width:360px; margin-left: 1px;"></textarea><img id="packageTotalNoQuestionError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label style="margin-right:20px; vertical-align:top;padding-top: 24px" class="control-label" for="textarea2">Package Detail Text</label>
                                                    <div id="virtualtourContentField2"class="block-content collapse in">
                                                         <textarea id="packageDetailText" ></textarea>
                                                    </div>
                                                </div>
                                                    <div id="allTabDivs">
                                                        <div class="control-group" id ="addMoreDivs" style="display:none">
                                                    <label class="control-label"><b>Package Tabs</b></label>
                                                    
                                                 </div>
                                                  <div class="control-group" id ="addMoreDiv" >
                                                    <a onclick="tabs.addMore();"><label class="control-label" style=" cursor: pointer;color:#333; float:right;margin-right: 38%;text-align: left"><i class="icon-user" style="background-position: 0px -96px;"></i>&nbsp;Add Package Tabs</label></a>
                                                 </div>
                                                        <input type="hidden" name="tabcount" id="tabcount" value="0">
                                                        <input type="hidden" name="srNo" id="srNo" value="0">

                                                </div>
                                                <div class="form-actions">
                                                    <input type="hidden" id="packageHiddenSave" value="" />
                                                    <button id="packageSubmit1" type="submit"  class="btn btn-primary">Save</button>
                                                    <button id="packageSubmit2" type="submit"  class="btn btn-primary">Save & New</button>
                                                    <button type="button" id="cancelPackageData" class="btn" onclick="packageData.cancel();">Cancel</button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                           <!-- /block -->
                        </div>
                    </div>
                <!-- *****END: Package*************************************************** -->
                    
                    
              <div class="row-fluid" <?php if($_GET['ln']=='4' && ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>

                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=12&ln=4&t=7&e=0">Page Headings</a></div>
                                    <div class="muted pull-left" style="margin-left:5px;"><a href="index.php?p=12&ln=4&t=7&e=1">Add Page Headings</a></div>
                                    <!--<div class="pull-right"><span class="badge badge-info"><?php echo $countCategory; ?></span></div>-->
                                
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>Page Heading</th>
                                                <th>URL Link</th>
                                                <th>Order</th>
                                                <th>Active</th>
                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="pageData"></tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                     <div id="PcategoryStart" class="row-fluid" <?php if($_GET['ln']=='4' &&( $_GET['e']=='1' || $_GET['e']=='2' )) { ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=12&ln=4&t=7&e=0">Page Headings</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form  class="form-horizontal"  action="javascript:void(0)" onsubmit="pageData1.saveData();">

                                      <input type="hidden" class="span6" id="modePage"  data-provide="typeahead" value="1" />
                                      <input type="hidden" class="span6" id="idPage"  data-provide="typeahead" value="" />
                                     <fieldset>


                                        <legend ><span id="editTopperTalk"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> Page Headings</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="select01"><span style="color: red;">*</span>Page Name </label>
                                          <div class="controls">
                                              <input type="text" class="span6" id="pageField1" placeholder="Enter Page name"  /><img id="pageNameError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="select01"><span style="color: red;">*</span>URL Link</label>
                                          <div class="controls">
                                                <input type="text" class="span6" id="pageField2"  placeholder="Enter URL Link" /><img id="pageURLError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Page Order</label>
                                          <div class="controls">
                                            <input type="text" class="span6" placeholder="Enter Page Order" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" id="pageField3"  data-provide="typeahead" data-items="4"  /><img id="pageOrderError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="optionsCheckbox"></label>
                                          <div class="controls">
                                            <label class="uniform" style="width:35%;">
                                                <input class="uniform_on" type="checkbox" id="pageField4" value="1">
                                              Want to Activate the Page Heading
                                            </label>
                                          </div>
                                        </div>



                                        <div class="form-actions">
                                            <input type="hidden" id="pageHiddenSave" value="" />
                                            <button type="submit" id="pageSubmit1" class="btn btn-primary">Save</button>
                                            <button type="submit" id="pageSubmit2" class="btn btn-primary">Save & New</button>
                                            <button type="button"  id="cancelPageData" class="btn" onclick="pageData1.cancel();">Cancel</button>

                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    <!--Start: FAQ  Added by Azizur -->        
                    <div>
                          <div class="row-fluid" <?php if($_GET['ln']=='5' && ( $_GET['e']=='0' || !isset($_GET['e'])) ){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                            <input type="hidden" id="faq_StartPoint" value="0"> 
                            <input type="hidden" id="faq_EndPoint" value="0"> 
                            <input type="hidden" id="faq_total" value="0">     
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=12&ln=5&t=7&e=0">FAQ</a></div> 
                                    <div class="muted pull-left" style="margin-left:20px;"><a href="index.php?p=12&ln=5&t=7&e=1">Add FAQ</a></div>
                                    <div class="pull-right"><span class="badge badge-info"><?php //echo $countCourse; ?></span></div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Questions</th>
                                                <th>Answers</th>
                                                <th>Order</th>
                                                <th>Active</th>
                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="FAQData">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                     <div id="courseStart" class="row-fluid" <?php if($_GET['ln']=='5' &&( $_GET['e']=='1' || $_GET['e']=='2' ) ){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=12&ln=5&t=7&e=0">FAQ</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                      <form id="faqForm" class="form-horizontal"   action="javascript:void(0)" onsubmit="callFAQData.saveData();">
                                      <input type="hidden" class="span6" id="modeFAQ"  data-provide="typeahead" value="1">
                                      <input type="hidden" class="span6" id="idFAQ"  data-provide="typeahead" value="">
                                        <fieldset>
                                        <legend><span id="editFAQ"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> FAQ</legend>
                                        <div class="control-group">
                                          <label style="margin-right:20px; vertical-align:top;padding-top: 10px" class="control-label" for="textarea1"><span style="color: red;">*</span>Question</label>
                                          <div id="FAQField1"class="block-content collapse in">
		                               <textarea rows="4" class="span6" cols="50" id="FAQField1" style="resize:none;" placeholder="Add FAQ Question"></textarea><img id="FAQQuestionError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
		                          </div>
		                        </div>
                                        <div class="control-group">
                                          <label style="margin-right:20px; vertical-align:top;padding-top: 10px" class="control-label" for="textarea2"><span style="color: red;">*</span>Answer</label>
                                          <div id="FAQField2"class="block-content collapse in">
		                               <textarea rows="4" class="span6" cols="50" id="FAQField2" style="resize:none;" placeholder="Add Answer Content"></textarea><img id="FAQAnswerError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
		                          </div>
		                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead"><span style="color: red;">*</span>Question Order</label>
                                          <div class="controls">
                                            <input type="text" maxlength="5" class="span6" id="FAQField3" style="" placeholder="Add Question Order" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" data-provide="typeahead" data-items="4" ><img id="FAQOrderError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="optionsCheckbox"></label>
                                          <div class="controls">
                                            <label class="uniform" style="width:31%;">
                                                <input class="uniform_on" type="checkbox" id="FAQField4" value="1">
                                              Want to Activate the Question
                                            </label>
                                          </div>
                                        </div>
                                        <div class="form-actions">
                                          <input type="hidden" id="FAQHiddenSave" value="" />
                                          <button id="FAQSubmit1" type="submit"  class="btn btn-primary">Save</button>
                                          <button id="FAQSubmit2" type="submit"  class="btn btn-primary">Save & New</button>
                                          <button type="button" onclick="callFAQData.cancel();"  id="cancelFAQData" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    </div>
                    <!--End: FAQ  Added by Azizur -->
                    <div>
                    <div class="row-fluid" <?php if($_GET['ln']=='3'&& ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                            <input type="hidden" id="image_StartPoint" value="0"> 
                            <input type="hidden" id="image_EndPoint" value="0"> 
                            <input type="hidden" id="image_total" value="0">   
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=12&ln=3&t=7&e=0">Images</a></div>
                                    <div style="margin-left:10px;" class="muted pull-left"><a href="index.php?p=12&ln=3&t=7&e=1">Add Images</a>
                                    </div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countSubCourse; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Course</th>
                                                <th>Display Name</th>
                                                <th>Rank</th>
                                                <th>Order</th>                                               
                                                <th>Active</th>

                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="imageData">
                                       

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>

                     <div id="subCourseStart"class="row-fluid" <?php if($_GET['ln']=='3'&&( $_GET['e']=='1' || $_GET['e']=='2' )){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                         <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=12&ln=3&t=7&e=0">Images</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form id="imageForm"  class="form-horizontal" action="javascript:void(0)" onsubmit="imageData.saveData();">
                                      <input type="hidden" class="span6" id="modeimage"  data-provide="typeahead" value="1" />
                                      <input type="hidden" class="span6" id="idimage"  data-provide="typeahead" value="" />
                                        <fieldset>
                                        <legend><span id="editImage"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> Images</legend>
                                            <div class="control-group">
                                                <label class="control-label"><span style="color: red;">*</span>Course</label>
                                                <div class="controls">
                                                    <select id="imageCourse"  name="imageCourse">
                                                        <option selected="selected" disabled='disabled' value="">Select Course</option>
                                                    </select><img id="imageCourseError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                </div>
                                            </div>
                                        
                                            <div class="control-group">
                                                <label class="control-label"><span style="color: red;">*</span>Display Name</label>
                                                <div class="controls">
                                                    <input type="text" class="span6" maxlength="150" id="imageName"  name="imageName" placeholder="Add Display Name" /><img id="imageNameError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                </div>
                                            </div>
                                            
                                            <div class="control-group" style="display:block;">
                                                <label class="control-label" for="fileInput"><span style="color: red;">*</span>Image upload</label>
                                                <div class="controls">
                                                    <input  class="input-file uniform_on" id="imageupload" name="imageupload" type="file" onchange="imageData.imageValidation(this,this.id);"/>
                                                    <input  class="input-file uniform_on" id="imageEdit3" name="imageEdit3" type="hidden" /><img id="imageImgError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>(Dimension height:150px to 160px , width:120px to 130px)
                                                </div>
                                            </div>
                                        
                                            <div class="control-group">
                                                <label class="control-label"><span style="color: red;">*</span>Rank</label>
                                                <div class="controls">
                                                    <input type="text" class="span6"maxlength="100" id="imageRank"  name="imageRank" placeholder="Add Rank"  /><img id="imageRankError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                </div>
                                            </div>
                                        
                                            <div class="control-group">
                                                <label class="control-label"><span style="color: red;">*</span>Image Order</label>
                                                <div class="controls">
                                                    <input type="text" maxlength="5" class="span6" id="imageOrder"  name="imageOrder" placeholder="Add image Order" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$4'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');"  /><img id="imageOrderError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                            <label class="control-label" for="optionsCheckbox"></label>
                                                <div class="controls">
                                                    <label class="uniform" style="width:31%;">
                                                    <input class="uniform_on" type="checkbox" id="imageField4" value="1">
                                                        Want to Activate the Image
                                                    </label>
                                                </div>
                                            </div>    
                                        
                                        
                                        <div class="form-actions">
                                          <input type="hidden" id="imageHiddenSave" value="" />
                                          <button id="imageSubmit1" type="submit"  class="btn btn-primary">Save</button>
                                          <button id="imageSubmit2" type="submit"  class="btn btn-primary">Save & New</button>
                                          <button type="button" id="cancelimageData" class="btn" onclick="imageData.cancel();">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>

                  </div>
                    <div>
                    <div class="row-fluid" <?php if($_GET['ln']=='7'&& ( $_GET['e']=='0' || !isset($_GET['e']))){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                            <input type="hidden" id="video_StartPoint" value="0"> 
                            <input type="hidden" id="video_EndPoint" value="0"> 
                            <input type="hidden" id="video_total" value="0"> 
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="index.php?p=12&ln=7&t=7&e=0">Video</a></div>
                                    <div style="margin-left:10px;" class="muted pull-left"><a href="index.php?p=12&ln=7&t=7&e=1">Add Video</a>
                                    </div>
                                    <div class="pull-right"><span class="badge badge-info"><?php echo $countSubCourse; ?></span>

                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Course Name</th>
                                                <th>Video Name</th>
                                                <th>Rank</th>
                                                <th>Order</th>                                               
                                                <th>Active</th>

                                                <th style="text-align: center">Edit</th>
                                                <th style="text-align: center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="videoData">
                                       

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>

                     <div id="subCourseStart"class="row-fluid" <?php if($_GET['ln']=='7'&&( $_GET['e']=='1' || $_GET['e']=='2' )){ ?> style="display:block"<?php }else{ ?>style="display:none" <?php } ?>>
                        <!-- block -->
                         <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="index.php?p=12&ln=7&t=7&e=0">Video</a></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <form  class="form-horizontal" action="javascript:void(0)" onsubmit="videoData.saveData();">
                                      <input type="hidden" class="span6" id="modeVideo"  data-provide="typeahead" value="1" />
                                      <input type="hidden" class="span6" id="idVideo"  data-provide="typeahead" value="" />
                                        <fieldset>
                                        <legend><span id="editImage"><?php if($_GET['e']=='2'){ echo 'Edit'; }else {echo 'Add';} ?></span> Images</legend>
                                            <div class="control-group">
                                                <label class="control-label"><span style="color: red;">*</span>Course</label>
                                                <div class="controls">
                                                    <select id="videoCourse"  name="videoCourse">
                                                        <option selected="selected" disabled='disabled' value="">Select Course</option>
                                                    </select><img id="videoCourseError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                </div>
                                            </div>
                                        
                                            <div class="control-group">
                                                <label class="control-label"><span style="color: red;">*</span>Video Name</label>
                                                <div class="controls">
                                                    <input type="text" class="span6" id="videoName" maxlength="150" name="videoName" placeholder="Add Video Name" /><img id="videoNameError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"><span style="color: red;">*</span>Video Url</label>
                                                <div class="controls">
                                                    <input type="text" class="span6" id="videoUrl"maxlength="250"  name="videoUrl" placeholder="Add Video Url" /><img id="videoUrlError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label"><span style="color: red;">*</span>Rank</label>
                                                <div class="controls">
                                                    <input type="text" class="span6" id="videoRank" maxlength="100"  name="videoRank" placeholder="Add Rank"  /><img id="videoRankError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                </div>
                                            </div>
                                        
                                            <div class="control-group">
                                                <label class="control-label"><span style="color: red;">*</span>Video Order</label>
                                                <div class="controls">
                                                    <input type="text" maxlength="5" class="span6" id="videoOrder"  name="videoOrder" placeholder="Add Video Order" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$4'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');"  /><img id="videoOrderError"  src="view/layout_TestSeries/crossError.png" class="crossImg"/>
                                                </div>
                                            </div>
                                            
                                            <div class="control-group">
                                            <label class="control-label" for="optionsCheckbox"></label>
                                                <div class="controls">
                                                    <label class="uniform" style="width:31%;">
                                                    <input class="uniform_on" type="checkbox" id="videoField4" value="1">
                                                        Want to Activate the Video
                                                    </label>
                                                </div>
                                            </div>    
                                        
                                        
                                        <div class="form-actions">
                                          <input type="hidden" id="videoHiddenSave" value="" />
                                          <button id="videoSubmit1" type="submit"  class="btn btn-primary">Save</button>
                                          <button id="videoSubmit2" type="submit"  class="btn btn-primary">Save & New</button>
                                          <button type="button" id="cancelvideoData" class="btn" onclick="videoData.cancel();">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                        </div>
                    </div>
                </div>
                <input class="seemore"  id="seeMoreBtn" onclick="tabs.seeMore()"  type="button" value="See More..." style="display: none">
                </div>
            </div>
            
            
         
<!--                <p>&copy; Shobhit 2013</p>-->
       
        <!--/.fluid-container-->
        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">
    
        
     
        
        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">
 
        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>
     
        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

        <script src="vendors/ckeditor/ckeditor.js"></script>
		<script src="vendors/ckeditor/adapters/jquery.js"></script>
        <script src="assets/scripts.js"></script>
        <script type="text/javascript" src="view/layout_TestSeries/layout.js" ></script>
        <!--<script type="text/javascript" src="view/layout/layout.js" ></script>-->
        <script>
            
           
        $(function() {
          
            $(".datepicker").datepicker();
            $("#newsEventField3").datepicker();
                    $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $( 'textarea#packageDetailText' ).ckeditor({width:'98%', height: '150px', toolbar: [
				{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
				[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
				{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
                                { name: 'colors', items : [ 'TextColor','BGColor' ] },
                                { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
                                { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
                                '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
                                { name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow' } },
                                { name: 'links', items : [ 'Link','Unlink','Anchor' ] }
			]});
          $('.textarea').wysihtml5();
            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
  
        <?php if ($_GET['ln'] == '1') { ?>
        var callHomeData = new homeBannerData();
            callHomeData.getDataAll('0','15');
            <?php if ($_GET['e'] == '2') { ?>
                      callHomeData.editData(<?php echo $_GET['id']; ?>);
            <?php }
        } ?>
            
        <?php if ($_GET['ln'] == '2') { ?>
                var callCourseData = new courseData();
                callCourseData.getDataAll('0','15');
                <?php if ($_GET['e'] == '2') { ?>
                      callCourseData.editData(<?php echo $_GET['id']; ?>);
                <?php } } ?>
                    

        <?php if ($_GET['ln'] == '3') { ?>   
                    <?php if ($_GET['e'] == '1') { ?>
                    imageData.getCourse();
                    <?php }else if ($_GET['e'] == '2') { ?>
                    imageData.editData(<?php echo $_GET['id']; ?>);
                    <?php }else{ ?>
                    imageData.getDataAll('0','15');
        <?php } } ?>
            
        <?php if ($_GET['ln'] == '4') { ?>
                var pageData1 = new pageData();
                    pageData1.getDataAll('0','15');
                    <?php if ($_GET['e'] == '2') { ?>
                    pageData1.editData(<?php echo $_GET['id']; ?>);
        <?php } } ?>
            
        <?php if ($_GET['ln'] == '5') { ?>
                var callFAQData = new FAQData();
                    callFAQData.getDataAll('0','15');
                    <?php if ($_GET['e'] == '2') { ?>
                    callFAQData.editData(<?php echo $_GET['id']; ?>);
        <?php } } ?>
            
            
        <?php if ($_GET['ln'] == '6') { ?>   
                    <?php if ($_GET['e'] == '1') { ?>
                    packageData.getCourse();
                     $('#preloaderCenter').hide();
                   <?php }else if ($_GET['e'] == '2') { ?>
                    packageData.editData(<?php echo $_GET['id']; ?>);
        <?php }else{ ?>
            packageData.getDataAll('0','15');
        <?php } } ?>
            
            
        <?php if ($_GET['ln'] == '7') { ?>   
                    <?php if ($_GET['e'] == '1') { ?>
                    videoData.getCourse();
                    <?php }else if ($_GET['e'] == '2') { ?>
                    videoData.editData(<?php echo $_GET['id']; ?>);
                    <?php }else{ ?>
                    videoData.getDataAll('0','15');
        <?php } } ?>

$(document).ready(function(){
   
     
    var counter = 2;
    var mode = $('#modeLink').val() ;
        if(mode  == '2'){
            counter = parseInt($("#totalFilds").val()) + 1;
        }
      		
    $("#addButton").click(function () {
	var newTextBoxDiv = $(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);
                
	newTextBoxDiv.after().html('<label><span>Sub Link Text '+ counter +' :</span>  <span style="margin-left: 15%">Sub Link Name '+ counter +' :</span> </label>' +
	      '<input type="text" name="subLinkText' + counter + '"  id="linkField5' + counter + '" value="" > <input type="text"  data-provide="typeahead" name="subLink' + counter + '" \n\
                id="linkField6' + counter + '" value="" >');
            
	newTextBoxDiv.appendTo("#TextBoxesGroup");			
        $("#totalFilds").val(counter);
	counter++;
        
     });

    $("#removeButton").click(function () {
        
	if(counter==2){ 
           $("#linkField51").val("");
           $("#linkField61").val("");
           hideDiv(a_linkField5);
           //$("#TextBoxDiv").clear();
          //alert("No more textbox to remove");
          return false;
       }   
	counter--;
        $("#totalFilds").val(counter);
        $("#TextBoxDiv" + counter).remove();		
    });

 });
                       
   function showDiv(a_linkField5) {
            var divHideShow = document.getElementById("divHideShow");
            divHideShow.style.display = a_linkField5.click ? "block" : "none";
            $("#totalFilds").val();
        }
                                     
   function hideDiv(a_linkField5) {
            var divHideShow = document.getElementById("divHideShow");
            divHideShow.style.display = a_linkField5.click ? "none" : "block";
        }
        
    $('#bannerSubmit1').click(function(){
      $('#bannerHiddenSave').val('save');  
    });
    
    $('#bannerSubmit2').click(function(){
      $('#bannerHiddenSave').val('save&new');  
    });
    
    $('#courseSubmit1').click(function(){
      $('#courseHiddenSave').val('save');  
    });
    
    $('#courseSubmit2').click(function(){
      $('#courseHiddenSave').val('save&new');  
    });
    
    $('#pageSubmit1').click(function(){
      $('#pageHiddenSave').val('save');  
    });
    
    $('#pageSubmit2').click(function(){
      $('#pageHiddenSave').val('save&new');  
    });
    
    $('#packageSubmit1').click(function(){
      $('#packageHiddenSave').val('save');  
    });
    
    $('#packageSubmit2').click(function(){
      $('#packageHiddenSave').val('save&new');  
    }); 
    
    $('#FAQSubmit1').click(function(){
      $('#FAQHiddenSave').val('save');  
    });
    
    $('#FAQSubmit2').click(function(){
      $('#FAQHiddenSave').val('save&new');  
    }); 
    $('#imageSubmit1').click(function(){
      $('#imageHiddenSave').val('save');  
    });
    
    $('#imageSubmit2').click(function(){
      $('#imageHiddenSave').val('save&new');  
    }); 
    $('#videoSubmit1').click(function(){
      $('#videoHiddenSave').val('save');  
    });
    
    $('#videoSubmit2').click(function(){
      $('#videoHiddenSave').val('save&new');  
    }); 
        </script>
    </body>

</html>
