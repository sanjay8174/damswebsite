<?php
include('dao.php');
header('Content-type: application/json');
$data = json_decode($_POST['data']);

$mode = $data->mode;
if($mode=="getAllHomeData"):
    $obj = new dao();
    $homeBannerArr = array();
    $result        = $obj->getAllBanner('',$data->banner_start,$data->banner_end);
    $count         = 0;
    $total         = mysql_num_rows($result);
    while($rows =  mysql_fetch_object($result)):
       $homeBannerArr['ID'][$count]=$rows->ID;
       $homeBannerArr['BANNER_NAME'][$count]=$rows->BANNER_NAME;
       $homeBannerArr['BANNER_TEXT'][$count]=$rows->BANNER_TEXT;
       $homeBannerArr['FILE_NAME'][$count]=$rows->FILE_NAME;
       $homeBannerArr['TYPE'][$count]=$rows->BANNER_PATH;
       $homeBannerArr['ORDER_BANNER'][$count]=$rows->BANNER_ORDER;
       $homeBannerArr['ACTIVE'][$count]=$rows->ACTIVE;
       $count++;
    endwhile;
    $homeBannerArr['total'] = $total ;
    echo json_encode($homeBannerArr);
endif;


if($mode == 'saveHomeBanner'){
    $saveData = $data->data;
    $obj = new dao();
    $userfile_extn = substr($saveData->imageUpload, strrpos($saveData->imageUpload, '.')+1);
    $homeBArr = array(
        "BANNER_NAME"=>addslashes("$saveData->bannerName"),
        "BANNER_ORDER"=>"$saveData->bannerOrder",
        "BANNER_TEXT"=>addslashes("$saveData->contentBanner"),
        "FILE_NAME"=>addslashes("$saveData->imageUpload"),
        "TYPE"=>"$userfile_extn",
        "ACTIVE"=>"$saveData->active"
    );
    $funSaveCompany = $obj->insertAssociativeArray("BANNER_TESTSERIES" ,$homeBArr);
    $count = mysql_affected_rows();
    $lastInsertedId = mysql_insert_id(); 
    if($count>0):
        $banner['total']          = $count;
        $banner['lastInsertedId'] = $lastInsertedId ;
    else:
        $banner['total'] = 0;
    endif;
    echo json_encode($banner);
}
if($mode == "getEditHomeData"):
    $obj = new dao();
    $id  = $data->id ;
    $homeBannerArr = array();
    $result        = $obj->getAllBanner($id,'0','1');
   while($rows  =  mysql_fetch_object($result)):
       $homeBannerArr['ID']=$rows->ID;
       $homeBannerArr['BANNER_NAME']=$rows->BANNER_NAME;
       $homeBannerArr['BANNER_TEXT']=$rows->BANNER_TEXT;
       $homeBannerArr['FILE_NAME']=$rows->FILE_NAME;
       $homeBannerArr['BANNER_PATH']=$rows->TYPE;
       $homeBannerArr['ORDER_BANNER']=$rows->BANNER_ORDER;
       $homeBannerArr['ACTIVE']=$rows->ACTIVE;
    endwhile;
    echo json_encode($homeBannerArr);
endif;

if($mode == 'updateHomeBanner'){
    $updateData = $data->data;
    $obj      = new dao();
    $userfile_extn = substr($updateData->imageUpload, strrpos($updateData->imageUpload, '.')+1);
    $homeBArr = array(
        "BANNER_NAME"=>"$updateData->bannerName",
        "BANNER_ORDER"=>"$updateData->bannerOrder",
        "BANNER_TEXT"=>"$updateData->contentBanner",
        "FILE_NAME"=>"$updateData->imageUpload",
        "TYPE"=>"$userfile_extn",
        "ACTIVE"=>"$updateData->active"
    );
    $conditionarray = array("ID='$updateData->id'");
    $funSaveCompany = $obj->update("BANNER_TESTSERIES" ,$homeBArr,$conditionarray);
    $count = mysql_affected_rows();
    $lastInsertedId = mysql_insert_id(); 
    if($count>0):
        $banner['total']          = $count;
        $banner['lastInsertedId'] = $lastInsertedId ;
    else:
        $banner['total'] = 0;
    endif;
    echo json_encode($banner);
}
if($mode == 'deleteBanner'){
    $banner         = array();
    $obj            = new dao();
    $updateData     = $data->data;
    $id             = $updateData->id;
    $result      = $obj->getAllBanner($id,'0','1') ;
    $row         =  mysql_fetch_object($result);
    $delId       =$row->ID;
    $extension  =$row->TYPE;
    $funSaveCompany = $obj->delBanner($id) ;
    $count          = mysql_affected_rows();
    if($count>0):
        $banner['total']          = $count;
        $banner['delId']          = $delId;
        $banner['extension']   = $extension;
    
    else:
        $banner['total'] = 0;
    endif;
    
    echo json_encode($banner);
}
if($mode == 'getAllCourse'):
    $getData = $data->data;
    $courseID = $getData->id;
    $obj = new dao();
    $total=0;
    $count=0;
    $totalCourse=0;
    $funcGetCourse = $obj->selCourse($courseID,'',$data->course_start,$data->course_end);
    $count = mysql_num_rows($funcGetCourse);

    $totalCourse = 0;
    if($count>0):
        while($row = mysql_fetch_object($funcGetCourse)):
            $course['id'][$totalCourse] = $row->ID;
            $course['courseName'][$totalCourse] = html_entity_decode($row->COURSE_NAME,ENT_QUOTES);
            $course['courseOrder'][$totalCourse] = $row->COURSE_ORDER;
            $course['active'][$totalCourse] = $row->ACTIVE;
            


            $totalCourse++;
        endwhile;
        $course['total'] = $totalCourse;
    else:
        $course['total'] = $count;
    endif;    
        $course['totalCourse'] = $result_Count;
    echo json_encode($course);

endif;
if($mode == 'saveCourse'):
    $saveData = $data->data;
    $obj = new dao();
//    $checkCourseCount = $obj->checkCourse(htmlentities($saveData->courseName,ENT_QUOTES));
//    $rowCount=mysql_num_rows($checkCourseCount);
//    if($rowCount>0){
//         $course['dubCourse'] = $rowCount;
//    }else{
        $funSaveCourse = $obj->insCourse(htmlentities($saveData->courseName,ENT_QUOTES),$saveData->active,$saveData->courseOrder);
        $count = mysql_affected_rows();
        $lastInsertedId=mysql_insert_id();
//        $course['dubCourse'] = '0';
        if($count>0):
            $course['total'] = $count;
            $course['lastInsertedId'] = $lastInsertedId;
        else:
            $course['total'] = 0;
        endif;
            
//    }
    

    echo json_encode($course);

endif;
if($mode == 'updateCourse'):
    $saveData = $data->data;
    $obj = new dao();
    $checkCourseCount = $obj->checkCourse(htmlentities($saveData->courseName,ENT_QUOTES),$saveData->id);
    $rowCount=mysql_num_rows($checkCourseCount);
    if($rowCount>0){
         $course['dubCourse'] = $rowCount;
    }else{
    $funSaveCourse = $obj->updCourse($saveData->id,  htmlentities($saveData->courseName,ENT_QUOTES),$saveData->courseOrder,$saveData->active);
    $count = mysql_affected_rows();
    $lastInsertedId=mysql_insert_id();
    $course['dubCourse'] = "0";
    if($count>0):
         $course['total'] = $count;
          $course['lastInsertedId'] = $lastInsertedId;
    else:
         $course['total'] = 0;
    endif;
    }
    echo json_encode($course);

endif;
if($mode == 'deleteCourse'):
    $saveData = $data->data;
    $obj = new dao();
    $funSaveCourse = $obj->delCourse($saveData->id);
    $count = mysql_affected_rows();
    if($count>0):
        $course['total'] = $count;
    else:
        $course['total'] = 0;
    endif;

    echo json_encode($course);

endif;
if($mode == 'getCourseSelect'):
    $obj = new dao();
    $count=0;
    $funSaveCourse= $obj->selCourseSelect($data->id,'','0','1');
    while($row= mysql_fetch_object($funSaveCourse)){
            $course['name'][$count]=  html_entity_decode($row->COURSE_NAME,ENT_QUOTES);
            $course['id'][$count]=  html_entity_decode($row->ID,ENT_QUOTES);
            $count++;
        }
        $course[total]= $count;
    echo json_encode($course);
endif;
if($mode == 'saveHomePackage'){ 
    $saveData = $data->data;
    $obj = new dao();
    $packArr = array(
        "COURSE_ID"=>addslashes("$saveData->course_id"),
        "PACKAGE_NAME"=>addslashes("$saveData->package_name"),
        "PACKAGE_DETAILED_NAME"=>addslashes("$saveData->package_detail_Name"),
        "PACKAGE_COST"=>addslashes("$saveData->cost"),
        "PACKAGE_URL"=>addslashes("$saveData->package_Url"),        
        "PACKAGE_ORDER"=>addslashes("$saveData->order"),
        "ACTIVE"=>addslashes("$saveData->active")
    );
    
    $obj->insertAssociativeArray("PACKAGE_TESTSERIES" ,$packArr);
    $lastInsertedId = mysql_insert_id();
    $packDetailArr = array(
        "PACKAGE_ID"=>$lastInsertedId,
        "DURATION"=>addslashes("$saveData->package_duration"),
        "TOTAL_TEST"=>addslashes("$saveData->package_totalNoOfTest"),
        "TOTAL_QUESTIONS_DETAIL"=>addslashes("$saveData->package_totalQues"),
        "PACKAGE_DETAIL"=>addslashes("$saveData->package_detailText")
    );
        $obj->insertAssociativeArray("PACKAGE_DETAIL_TESTSERIES" ,$packDetailArr);
        $count = mysql_affected_rows();
    if($count>0):
        $package['total']          = $count;
        $package['lastInsertedId'] = $lastInsertedId ;
    else:
        $package['total'] = 0;
    endif;
    echo json_encode($package);
}
if($mode == 'package_Tabs'){
    $saveData = $data->data;
    $obj = new dao();
    $packTabArr = array(
        "PACKAGE_ID"=>addslashes("$saveData->Id"),
        "TAB_NAME"=>addslashes("$saveData->package_tabName"),
        "TAB_ORDER"=>addslashes("$saveData->package_tabOrder"),
        "FILE_NAME"=>addslashes("$saveData->package_excel"),
        "ACTIVE"=>addslashes("$saveData->package_tabActive")
    );
    
    $obj->insertAssociativeArray("PACKAGE_TABS_TESTSERIES" ,$packTabArr);
    $count = mysql_affected_rows();
    $lastInsertedId = mysql_insert_id();
    if($count>0):
        $package['total']          = $count;
        $package['lastInsertedId'] = $lastInsertedId ;
    else:
        $package['total'] = 0;
    endif;
    echo json_encode($package);
}
if($mode=="getAllPackageData"):
    $obj = new dao();
    $getPackageRec = array();
    $result        = $obj->getAllPackage('',$data->package_start,$data->package_end);
    $count         = 0;
    $total         = mysql_num_rows($result);
    while($rows =  mysql_fetch_object($result)):
       $getPackageRec['ID'][$count]=$rows->ID;
       $getPackageRec['PACKAGE_NAME'][$count]=$rows->PACKAGE_NAME;
       $getPackageRec['PACKAGE_COST'][$count]=$rows->PACKAGE_COST;
       $getPackageRec['COURSE_ID'][$count]=$rows->COURSE_ID;
       $getPackageRec['PACKAGE_ORDER'][$count]=$rows->PACKAGE_ORDER;
       $getPackageRec['ACTIVE'][$count]=$rows->ACTIVE;
       $getPackageRec['COURSE_NAME'][$count]=$rows->COURSE_NAME;
       $getPackageRec['TOTAL_TABS'][$count]=$rows->TOTAL_TABS;
       $count++;
    endwhile;
    
    $getPackageRec['total'] = $total ;
    echo json_encode($getPackageRec);
endif;
if($mode=="getPackageDataInEdit"):
    $obj = new dao();
    $id  = $data->id ;
    $editPackageArr = array();
    $result         = $obj->getAllPackage($id,'0','1');
   while($rows  =  mysql_fetch_object($result)):
       $editPackageArr['ID']=$rows->ID;
       $editPackageArr['PACKAGE_NAME']=$rows->PACKAGE_NAME;
       $editPackageArr['PACKAGE_URL']=stripslashes($rows->PACKAGE_URL);
       $editPackageArr['PACKAGE_COST']=$rows->PACKAGE_COST;
       $editPackageArr['COURSE_ID']=$rows->COURSE_ID;
       $editPackageArr['PACKAGE_ORDER']=$rows->PACKAGE_ORDER;
       $editPackageArr['PACKAGE_DETAILED_NAME']=$rows->PACKAGE_DETAILED_NAME;
       $editPackageArr['ACTIVE']=$rows->ACTIVE;
       $editPackageArr['COURSE_NAME']=$rows->COURSE_NAME;
       $editPackageArr['TOTAL_TABS']=$rows->TOTAL_TABS;
       $editPackageArr['DURATION']=$rows->DURATION;
       $editPackageArr['TOTAL_TEST']=$rows->TOTAL_TEST;
       $editPackageArr['TOTAL_QUESTIONS_DETAIL']=$rows->TOTAL_QUESTIONS_DETAIL;
       $editPackageArr['PACKAGE_DETAIL']=$rows->PACKAGE_DETAIL;
    endwhile;
    echo json_encode($editPackageArr);  
endif;
if($mode=="getAllPackageDataTabs"):
    $obj = new dao();
    $id  = $data->id ;
    $editTabArr = array();
    $result         = $obj->getAllPackageTabs($id);
    $count         = 1;
    $total         = mysql_num_rows($result);
   while($rows  =  mysql_fetch_object($result)):
       $editTabArr['ID'][$count]=$rows->ID;
       $editTabArr['TAB_NAME'][$count]=$rows->TAB_NAME;
       $editTabArr['TAB_ORDER'][$count]=$rows->TAB_ORDER;
       $editTabArr['ACTIVE'][$count]=$rows->ACTIVE;
       $editTabArr['FILE_NAME'][$count]=$rows->FILE_NAME;
       $count++;
    endwhile;
    $editTabArr['total'] = $total ;
    echo json_encode($editTabArr);  
endif;

if($mode == 'deleteTab'){
    
    $banner         = array();
    $obj            = new dao();
    $updateData     = $data->data;
    $id             = $updateData->id;
    $funSaveCompany = $obj->delPackageTab($id) ;
    $count          = mysql_affected_rows();
    if($count>0):
        $banner['total']          = $count;
    else:
        $banner['total'] = 0;
    endif;
    
    echo json_encode($banner);
}
if($mode == 'deletePackage'){
    
    $banner         = array();
    $obj            = new dao();
    $updateData     = $data->data;
    $id             = $updateData->id;
    $result = $obj->getPackageTabsId($id) ;
    $row=  mysql_fetch_object($result);
    $delTabIds=$row->ID;
    $funSaveCompany = $obj->delPackage($id,$delTabIds) ;
    $count          = mysql_affected_rows();
        
    if($count>0):
        $banner['total']          = $count;
        $banner['delTabIds']      = $delTabIds;
    else:
        $banner['total'] = 0;
    endif;
    
    echo json_encode($banner);
}
if($mode == 'updateHomePackage'){
    $updateData = $data->data;
    $obj      = new dao();
    $homePackage = array(
        "COURSE_ID"=>addslashes("$updateData->course_id"),
        "PACKAGE_NAME"=>addslashes("$updateData->package_name"),
        "PACKAGE_DETAILED_NAME"=>addslashes("$updateData->package_detail_Name"),
        "PACKAGE_COST"=>addslashes("$updateData->cost"),
        "PACKAGE_URL"=>addslashes("$updateData->package_Url"),
        "PACKAGE_ORDER"=>addslashes("$updateData->order"),
        "ACTIVE"=>addslashes("$updateData->active")
    );
    $conditionarray = array("ID='$updateData->id'");
    $obj->update("PACKAGE_TESTSERIES" ,$homePackage,$conditionarray);
    $count = mysql_affected_rows();
    $homePackageDetail = array(
        "DURATION"=>addslashes("$updateData->package_duration"),
        "TOTAL_TEST"=>addslashes("$updateData->package_totalNoOfTest"),
        "TOTAL_QUESTIONS_DETAIL"=>addslashes("$updateData->package_totalQues"),
        "PACKAGE_DETAIL"=>addslashes("$updateData->package_detailText")
    );
    $conditionarrayDetail = array("ID='$updateData->id'");
    $obj->update("PACKAGE_DETAIL_TESTSERIES" ,$homePackageDetail,$conditionarrayDetail);
    
    $countDetail = mysql_affected_rows();
    $lastInsertedId = mysql_insert_id(); 

        $package['total_package']          = $count;
        $package['total_package_Detail']   = $countDetail;

    echo json_encode($package);
}
//if($mode == 'updateHomePackage'){
//    $updateData = $data->data;
//    $obj      = new dao();
//    $homePackage = array(
//        "TAB_NAME"=>addslashes("$updateData->package_tabName"),
//        "TAB_ORDER"=>addslashes("$updateData->package_tabOrder"),
//        "FILE_NAME"=>addslashes("$updateData->package_excel"),
//        "ACTIVE"=>addslashes("$updateData->package_tabActive")
//    );
//    $conditionarray = array("ID='$updateData->id'");
//    $obj->update("PACKAGE_TABS_TESTSERIES" ,$homePackage,$conditionarray);
//    $count = mysql_affected_rows();
//    
//    $package['total_package']          = $count;
//    
//    echo json_encode($package);
//}
if($mode == 'package_TabsUpdate'){
    $updateData = $data->data;
    $obj      = new dao();
        $package_Tab[ID]=addslashes($updateData->id);
        $package_Tab[PACKAGE_ID]=addslashes($updateData->Packageid);
        $package_Tab[TAB_NAME]=addslashes($updateData->package_tabName);
        $package_Tab[TAB_ORDER]=addslashes($updateData->package_tabOrder);
        $package_Tab[FILE_NAME]=addslashes($updateData->package_excel);
        $package_Tab[ACTIVE]=addslashes($updateData->package_tabActive);
        
        $obj->update_Tabs($package_Tab);
        $count = mysql_affected_rows();
    $lastInsertedId = mysql_insert_id(); 
    $package['total_package']          = $count;
    $package['lastInsertedId']          = $lastInsertedId;
    
    echo json_encode($package);
}

//start for Page Heading

if($mode=="getAllPage"):
    $updateData = $data->data;
    $id = $updateData->id;
    $obj = new dao();
    $page = array();
    $result        = $obj->selPage($id);
    $count         = 0;
    $total         = mysql_num_rows($result);
    while($rows =  mysql_fetch_object($result)):
       $page['ID'][$count]=$rows->ID;
       $page['PAGE_NAME'][$count]=$rows->HEADING_NAME;
       $page['PAGE_URL'][$count]=$rows->HEADING_URL;
       $page['PAGE_ORDER'][$count]=$rows->HEADING_ORDER;
       $page['ACTIVE'][$count]=$rows->ACTIVE;
       $count++;
    endwhile;
    $page['total'] = $total ;
    echo json_encode($page);
endif;

if($mode == 'savePage'):
    $saveData           = $data->data;
    $obj                = new dao();
    $pageName           = htmlentities($saveData->pageName,ENT_QUOTES) ;
    $pageURL            = htmlentities($saveData->pageURL,ENT_QUOTES);
    $pageOrder          = $saveData->pageOrder;
    $active             = $saveData->active ;
    $page               = array();
    $dataArr = array(
       "HEADING_NAME"      => "$pageName",
       "HEADING_URL"       => "$pageURL",
       "HEADING_ORDER"     => "$pageOrder",
       "ACTIVE"         => "$active"
        
    );
    
    $funSavePage        = $obj->insPage($dataArr);
    $count              = mysql_affected_rows();
    if( intval($count) > 0):
        $page['total'] = $count;
    else:
        $page['total'] = 0;
    endif;
    
   echo json_encode($page);
endif;

if($mode == 'updatePage'):
    $saveData = $data->data;
    $obj = new dao();
    $funSavePage = $obj->updPage($saveData->id,  htmlentities($saveData->pageName,ENT_QUOTES),htmlentities($saveData->pageURL,ENT_QUOTES), $saveData->pageOrder, $saveData->active);
    $count = mysql_affected_rows();
    $page               = array();
    if(intval($count) > 0):
        $page['total'] = $count;
    else:
        $page['total'] = 0;
    endif;

    echo json_encode($page);

endif;

if($mode == 'deletePage'):
    $saveData = $data->data;
    $obj = new dao();
    $funSavePage = $obj->delPage($saveData->id);
    $count = mysql_affected_rows();
    $page               = array();
    if(intval($count) > 0):
        $page['total'] = $count;
    else:
        $page['total'] = 0;
    endif;

    echo json_encode($page);

endif;
// end for page haeading

//Start: FAQ Added by Azizur
if($mode=="getAllFAQ"):
    $updateData = $data->data;
    $id = $updateData->id;
    $obj = new dao();
    $FAQ = array();
    $total=0;
   
   
    $result        = $obj->selFAQ($id,$data->package_start,$data->package_end);
    $count         = 0;
    $total         = mysql_num_rows($result);
    while($rows =  mysql_fetch_object($result)):
       $FAQ['id'][$count]       =$rows->ID;
       $FAQ['question'][$count] =$rows->QUESTION_TEXT;
       $FAQ['answer'][$count]   =$rows->QUESTION_ANSWER;
       $FAQ['order'][$count]    =$rows->QUESTION_ORDER;
       $FAQ['active'][$count]   =$rows->ACTIVE;
       $count++;
    endwhile;
    $FAQ['total'] = $total ;
    echo json_encode($FAQ);
endif;

if($mode == 'saveFAQ'):
    $saveData          = $data->data;
    $obj               = new dao();
    $FAQQuestion       = htmlentities($saveData->FAQQuestion,ENT_QUOTES) ;
    $FAQAnswer            = htmlentities($saveData->FAQAnswer,ENT_QUOTES);
    $FAQOrder          = $saveData->FAQOrder;
    $active            = $saveData->active ;
    $FAQ               = array();
    $dataArr = array(
       "QUESTION_TEXT"       => "$FAQQuestion",
       "QUESTION_ANSWER"     => "$FAQAnswer",
       "QUESTION_ORDER"      => "$FAQOrder",
       "ACTIVE"              => "$active"
        
    );
    
    $funSaveFAQ        = $obj->insFAQ($dataArr);
    $count              = mysql_affected_rows();
    if( intval($count) > 0):
        $FAQ['total'] = $count;
    else:
        $FAQ['total'] = 0;
    endif;
    
   echo json_encode($FAQ);
endif;

if($mode == 'updateFAQ'):
    $saveData = $data->data;
    $obj      = new dao();
    $funSaveFAQ = $obj->updFAQ($saveData->id,  htmlentities($saveData->FAQQuestion,ENT_QUOTES),htmlentities($saveData->FAQAnswer,ENT_QUOTES), $saveData->FAQOrder, $saveData->active);
    $count    = mysql_affected_rows();
    $FAQ        = array();
    if(intval($count) > 0):
        $FAQ['total'] = $count;
    else:
        $FAQ['total'] = 0;
    endif;
    echo json_encode($FAQ);
endif;

if($mode == 'deleteFAQ'):
    $saveData = $data->data;
    $obj = new dao();
    $funSaveFAQ = $obj->delFAQ($saveData->id);
    $count = mysql_affected_rows();
    $FAQ               = array();
    if(intval($count) > 0):
        $FAQ['total'] = $count;
    else:
        $FAQ['total'] = 0;
    endif;

    echo json_encode($FAQ);

endif;
//End: FAQ Added by Azizur
if($mode == 'saveImages'):
   
    $saveData           = $data->data;
    $obj                = new dao();
    $userfile_extn      = substr($saveData->imageupload, strrpos($saveData->imageupload, '.')+1);
    $courseId           = htmlentities($saveData->imageCourse,ENT_QUOTES) ;
    $imageName          = htmlentities($saveData->imageName,ENT_QUOTES) ;
    $fileName           = htmlentities($saveData->imageupload,ENT_QUOTES) ;
    $rank               = htmlentities($saveData->imageRank,ENT_QUOTES) ;
    $ext               = $userfile_extn ;
    $imageOrder         = htmlentities($saveData->imageOrder,ENT_QUOTES);
    $Active             = htmlentities($saveData->active,ENT_QUOTES);
    $videoData                = array();
    $dataArr = array(
       "COURSE_ID"       => "$courseId",
       "DISPLAY_NAME"    => "$imageName",
       "FILE_NAME"       => "$fileName",
       "RANK"            => "$rank",
       "EXT"             => "$ext",
       "IMAGE_ORDER"     => "$imageOrder",
       "ACTIVE"          => "$Active"
    );
    $SaveImageData = $obj->insertAssociativeArray("IMAGE_TESTSERIES" ,$dataArr);
    $count = mysql_affected_rows();
    $lastInsertedId = mysql_insert_id(); 
    if($count>0):
        $imageData['total']          = $count;
        $imageData['lastInsertedId'] = $lastInsertedId ;
    else:
        $imageData['total'] = 0;
    endif;
    echo json_encode($imageData);
endif;
if($mode=="getAllImageData"):
    $updateData = $data->data;
    $id = $updateData->id;
    $obj = new dao();
    $imageData = array();
    $result        = $obj->getAllImageData($id,'',$data->image_start,$data->image_end);
    $count         = 0;
    $total         = mysql_num_rows($result);
    while($rows =  mysql_fetch_object($result)):
       $imageData['id'][$count]             =$rows->ID;
       $imageData['DISPLAY_NAME'][$count]   =$rows->DISPLAY_NAME;
       $imageData['FILE_NAME'][$count]      =$rows->FILE_NAME;
       $imageData['RANK'][$count]           =$rows->RANK;
       $imageData['IMAGE_ORDER'][$count]    =$rows->IMAGE_ORDER;
       $imageData['ACTIVE'][$count]         =$rows->ACTIVE;
       $imageData['COURSE_NAME'][$count]    =$rows->COURSE_NAME;
       $count++;
    endwhile;
    $imageData['total'] = $total ;
    echo json_encode($imageData);
endif;
if($mode=="getAllEditimageData"):
    $updateData = $data->data;
    $id = $updateData->id;
    $obj = new dao();
    $imageData = array();
    $result        = $obj->getAllImageData($id,'','0','1');
    $count         = 0;
    $total         = mysql_num_rows($result);
    while($rows =  mysql_fetch_object($result)):
       $imageData['id']             =$rows->ID;
       $imageData['DISPLAY_NAME']   =$rows->DISPLAY_NAME;
       $imageData['FILE_NAME']      =$rows->FILE_NAME;
       $imageData['RANK']           =$rows->RANK;
       $imageData['IMAGE_ORDER']    =$rows->IMAGE_ORDER;
       $imageData['ACTIVE']         =$rows->ACTIVE;
       $imageData['COURSE_ID']      =$rows->COURSE_ID;
       $count++;
    endwhile;
    $imageData['total'] = $total ;
    echo json_encode($imageData);
endif;

if($mode == 'updateImageData'){
    $updateData = $data->data;
    $obj      = new dao();
    $userfile_extn      = substr($updateData->imageupload, strrpos($updateData->imageupload, '.')+1);
    $courseId           = htmlentities($updateData->imageCourse,ENT_QUOTES) ;
    $imageName          = htmlentities($updateData->imageName,ENT_QUOTES) ;
    $fileName           = htmlentities($updateData->imageupload,ENT_QUOTES) ;
    $ext                = $userfile_extn;
    $rank               = htmlentities($updateData->imageRank,ENT_QUOTES) ;
    $imageOrder         = htmlentities($updateData->imageOrder,ENT_QUOTES);
    $Active             = htmlentities($updateData->active,ENT_QUOTES);
    $imageData                = array();
    $dataArr = array(
       "COURSE_ID"       => "$courseId",
       "DISPLAY_NAME"    => "$imageName",
       "FILE_NAME"       => "$fileName",
       "EXT"             => "$ext",
       "RANK"            => "$rank",
       "IMAGE_ORDER"     => "$imageOrder",
       "ACTIVE"          => "$Active"
    );
    $conditionarray = array("ID='$updateData->id'");
    $updateImageData = $obj->update("IMAGE_TESTSERIES" ,$dataArr,$conditionarray);
    $count = mysql_affected_rows();
    $lastInsertedId = mysql_insert_id(); 
    if($count>0):
        $imageData['total']          = $count;
    else:
        $imageData['total'] = 0;
    endif;
    echo json_encode($imageData);
}
if($mode == 'deleteImageData'){
    $imageData         = array();
    $obj            = new dao();
    $updateData     = $data->data;
    $id             = $updateData->id;
    $result      = $obj->getAllImageData($id,'','0','1');
    $row         =  mysql_fetch_object($result);
    $delId       =$row->ID;
    $extension  =$row->EXT;
    $funSaveCompany = $obj->delImage($id) ;
    $count          = mysql_affected_rows();
    if($count>0):
        $imageData['total']          = $count;
        $imageData['delId']          = $delId;
        $imageData['extension']   = $extension;
    
    else:
        $imageData['total'] = 0;
    endif;
    
    echo json_encode($imageData);
}
if($mode == 'saveVideo'):
   
    $saveData           = $data->data;
    $obj                = new dao();
    $courseId           = htmlentities($saveData->videoCourse,ENT_QUOTES) ;
    $videoName          = htmlentities($saveData->videoName,ENT_QUOTES) ;
    $videoUrl           = htmlentities($saveData->videoUrl,ENT_QUOTES) ;
    $rank               = htmlentities($saveData->videoRank,ENT_QUOTES) ;
    $videoOrder         = htmlentities($saveData->videoOrder,ENT_QUOTES);
    $Active             = htmlentities($saveData->active,ENT_QUOTES);
    $videoData                = array();
    $dataArr = array(
       "COURSE_ID"       => "$courseId",
       "DISPLAY_NAME"    => "$videoName",
       "URL"             => "$videoUrl",
       "RANK"            => "$rank",
       "VIDEO_ORDER"     => "$videoOrder",
       "ACTIVE"          => "$Active"
    );
    $SaveImageData = $obj->insertAssociativeArray("VIDEO_TESTSERIES" ,$dataArr);
    $count = mysql_affected_rows();
    $lastInsertedId = mysql_insert_id(); 
    if($count>0):
        $videoData['total']          = $count;
        $videoData['lastInsertedId'] = $lastInsertedId ;
    else:
        $videoData['total'] = 0;
    endif;
    echo json_encode($videoData);
endif;
if($mode=="getAllVideoData"):
    $saveData = $data->data;
    $id = $saveData->id;
    $obj = new dao();
    $videoData = array();
    $total=0;
    $resultCount        = $obj->getAllVideoDataPagination();
    if($resultCount>0):
    $result        = $obj->getAllVideoData('','',$data->video_start,$data->video_end);
    $count         = 0;
    $total         = mysql_num_rows($result);
    while($rows =  mysql_fetch_object($result)):
       $videoData['id'][$count]             =$rows->ID;
       $videoData['VIDEO_NAME'][$count]     =$rows->DISPLAY_NAME;
       $videoData['URL'][$count]            =$rows->URL;
       $videoData['RANK'][$count]           =$rows->RANK;
       $videoData['VIDEO_ORDER'][$count]    =$rows->VIDEO_ORDER;
       $videoData['ACTIVE'][$count]         =$rows->ACTIVE;
       $videoData['COURSE_NAME'][$count]    =$rows->COURSE_NAME;
       $count++;
    endwhile;
    endif;
    $videoData['total'] = $total ;
    $videoData['count'] = $resultCount ;
    echo json_encode($videoData);
endif;
if($mode=="getAllEditVideoData"):
    $updateData = $data->data;
    $id = $updateData->id;
    $obj = new dao();
    $videoData = array();
    $result        = $obj->getAllVideoData($id,'','0','1');
    $count         = 0;
    $total         = mysql_num_rows($result);
    while($rows =  mysql_fetch_object($result)):
       $videoData['id']             =$rows->ID;
       $videoData['VIDEO_NAME']     =$rows->DISPLAY_NAME;
       $videoData['URL']            =$rows->URL;
       $videoData['RANK']           =$rows->RANK;
       $videoData['VIDEO_ORDER']    =$rows->VIDEO_ORDER;
       $videoData['ACTIVE']         =$rows->ACTIVE;
       $videoData['COURSE_ID']      =$rows->COURSE_ID;
       $count++;
    endwhile;
    $videoData['total'] = $total ;
    echo json_encode($videoData);
endif;

if($mode == 'updateVideoData'){
    $updateData = $data->data;
    $obj      = new dao();
    $courseId           = htmlentities($updateData->videoCourse,ENT_QUOTES) ;
    $videoName          = htmlentities($updateData->videoName,ENT_QUOTES) ;
    $videoUrl           = htmlentities($updateData->videoUrl,ENT_QUOTES) ;
    $rank               = htmlentities($updateData->videoRank,ENT_QUOTES) ;
    $videoOrder         = htmlentities($updateData->videoOrder,ENT_QUOTES);
    $Active             = htmlentities($updateData->active,ENT_QUOTES);
    $videoData          = array();
    $dataArr = array(
       "COURSE_ID"       => "$courseId",
       "DISPLAY_NAME"    => "$videoName",
       "URL"             => "$videoUrl",
       "RANK"            => "$rank",
       "VIDEO_ORDER"     => "$videoOrder",
       "ACTIVE"          => "$Active"
    );
    $conditionarray = array("ID='$updateData->id'");
    $updateImageData = $obj->update("VIDEO_TESTSERIES" ,$dataArr,$conditionarray);
    $count = mysql_affected_rows();
    $lastInsertedId = mysql_insert_id(); 
    if($count>0):
        $videoData['total'] = $count;
    else:
        $videoData['total'] = 0;
    endif;
    echo json_encode($videoData);
}
if($mode == 'deleteVideoData'){
    $videoData         = array();
    $obj            = new dao();
    $updateData     = $data->data;
    $id             = $updateData->id;
    $delData        = $obj->delVideo($id) ;
    $count          = mysql_affected_rows();
    if($count>0):
        $videoData['total']          = $count;
    else:
        $videoData['total'] = 0;
    endif;
    
    echo json_encode($videoData);
}

?>