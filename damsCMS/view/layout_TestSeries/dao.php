<?php

class dao extends Database{
    
    public function getAllBanner( $id = null,$start,$end){
        if($id!= null && $id!= '' ):
            $query = "SELECT * FROM BANNER_TESTSERIES where ID = '$id' order by ID desc limit $start,$end";
        else:
            $query = "SELECT * FROM BANNER_TESTSERIES order by ID desc limit $start,$end";
        endif;  
        
        $result = parent::executeQuery($query);
        return $result;
    }
    public function delBanner($id) {
     $sql = "DELETE FROM BANNER_TESTSERIES WHERE ID = '$id'" ;
     $result = parent::executeQuery($sql);
     return $result;
  }
  public function selCourse($id=null,$active=null,$start,$end){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;

        $query = "SELECT * FROM COURSE_TESTSERIES".$strWhere.$str." order by ID desc limit $start,$end";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function insCourse($courseName,$active,$courseOrder){
        $query = "INSERT INTO COURSE_TESTSERIES(COURSE_NAME,ACTIVE,COURSE_ORDER) VALUES('".$courseName."','".$active."','".$courseOrder."')";
        $result = parent::executeQuery($query);
        return $result;

    }
    public function checkCourse($courseName,$id){
        $where='';
        if($id!='' || $id!= NULL){
            $where=" and ID!='$id'";
        }
        $query = "select COURSE_NAME from COURSE_TESTSERIES where COURSE_NAME='$courseName' $where ";
        $result = parent::executeQuery($query);
        return $result;

    }
    public function updCourse($id,$courseName,$courseOrder,$active){
        $query = "UPDATE COURSE_TESTSERIES SET COURSE_NAME = '".$courseName."',COURSE_ORDER = '".$courseOrder."' ,ACTIVE = '".$active."'  WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    
     public function delCourse($id){
        $query = "DELETE FROM COURSE_TESTSERIES WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function selCourseSelect($id){
        if($id==NULL ||$id=='' ){
            $query = "SELECT * FROM COURSE_TESTSERIES where ACTIVE='A'";
        }else{
            $query = "SELECT * FROM COURSE_TESTSERIES where ID='$id'";
        }
        $result = parent::executeQuery($query);
        return $result;
        
    }
    
    public function getAllPackage( $id = null,$start,$end){
        if($id== null && $id== '' ){
            $query = " select P.PACKAGE_NAME,P.ID,P.PACKAGE_COST,P.COURSE_ID,P.PACKAGE_ORDER,P.ACTIVE,C.COURSE_NAME,count(PT.PACKAGE_ID) as TOTAL_TABS from PACKAGE_TESTSERIES P left join COURSE_TESTSERIES C on P.COURSE_ID=C.ID left join PACKAGE_TABS_TESTSERIES PT on P.ID=PT.PACKAGE_ID $where  group by P.ID order by P.ID desc limit $start,$end";
        }else{
            $query="select P.*,PD.DURATION,PD.TOTAL_TEST,PD.TOTAL_QUESTIONS_DETAIL,PD.PACKAGE_DETAIL,C.COURSE_NAME from PACKAGE_TESTSERIES P left join PACKAGE_DETAIL_TESTSERIES PD ON PD.PACKAGE_ID=P.ID left join  COURSE_TESTSERIES C on P.COURSE_ID=C.ID where P.ID=$id order by P.ID desc limit $start,$end";
        }
        $result = parent::executeQuery($query);
        return $result;
        
    }
    public function getAllPackageTabs( $id = null){
        
        $query = " select * FROM PACKAGE_TABS_TESTSERIES where PACKAGE_ID=$id";
        
        $result = parent::executeQuery($query);
        return $result;
        
    }
    public function delPackageTab($id) {
     $sql = "DELETE FROM PACKAGE_TABS_TESTSERIES WHERE ID = '$id'" ;
     $result = parent::executeQuery($sql);
     return $result;
  }
    public function delPackage($id,$mode) {
     if($mode==NULL ||$mode==''){
     $sql = "delete PD.*,P.*  from PACKAGE_TESTSERIES as P ,PACKAGE_DETAIL_TESTSERIES as PD where P.ID='$id' and PD.PACKAGE_ID='$id' ";    
     }else{
     $sql = "delete PT.*,PD.*,P.*  from PACKAGE_TESTSERIES as P ,PACKAGE_DETAIL_TESTSERIES as PD,PACKAGE_TABS_TESTSERIES as  PT where P.ID='$id' and PD.PACKAGE_ID='$id' and PT.PACKAGE_ID='$id'" ;    
     }
     
     $result = parent::executeQuery($sql);
     return $result;
  }
    public function getPackageTabsId($id) {
     
     $sql = "select group_concat(ID) as ID from PACKAGE_TABS_TESTSERIES where PACKAGE_ID='$id'" ;
     $result = parent::executeQuery($sql);
     return $result;
  }
    public function update_Tabs($tabArr) {
     $sql = "insert into PACKAGE_TABS_TESTSERIES set ID='$tabArr[ID]',PACKAGE_ID='$tabArr[PACKAGE_ID]' , TAB_NAME='$tabArr[TAB_NAME]',ACTIVE='$tabArr[ACTIVE]',FILE_NAME='$tabArr[FILE_NAME]',TAB_ORDER='$tabArr[TAB_ORDER]' ON DUPLICATE KEY update ID='$tabArr[ID]',PACKAGE_ID='$tabArr[PACKAGE_ID]',TAB_NAME='$tabArr[TAB_NAME]',ACTIVE='$tabArr[ACTIVE]',FILE_NAME='$tabArr[FILE_NAME]',TAB_ORDER='$tabArr[TAB_ORDER]';" ;
     $result = parent::executeQuery($sql);
     return $result;
  }
   
  // Start For Page Heading
  public function selPage($id=null,$active=null){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        if($active!=null || $active!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ACTIVE = ".$active;
            $strWhere = " WHERE ";
        endif;
       
        $query = "SELECT * FROM PAGE_HEADINGS_TESTSERIES".$strWhere.$str;
        $result = parent::executeQuery($query);
        return $result;
    }
      public function insPage($dataArray){  
        $result = parent::insertAssociativeArray("PAGE_HEADINGS_TESTSERIES", $dataArray) ;
        return $result;

    }
    public function updPage($id, $pageName, $pageURL, $pageOrder, $active){
        $query = "UPDATE PAGE_HEADINGS_TESTSERIES SET HEADING_NAME = '".$pageName."', HEADING_URL = '".$pageURL."' ,HEADING_ORDER = '".$pageOrder."', ACTIVE = '".$active."'  WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    
     public function delPage($id){
        $query = "DELETE FROM PAGE_HEADINGS_TESTSERIES WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
  //End start Page
  
  public function selFAQ($id=null,$start,$end){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        $query = "SELECT * FROM FAQ_TESTSERIES".$strWhere.$str." order by ID desc limit $start,$end" ;
        $result = parent::executeQuery($query);
        return $result;
    }
      public function insFAQ($dataArray){  
        $result = parent::insertAssociativeArray("FAQ_TESTSERIES", $dataArray) ;
        return $result;

    }
    public function updFAQ($id, $FAQQuestion, $FAQAnswer, $FAQOrder, $active){
        $query = "UPDATE FAQ_TESTSERIES SET QUESTION_TEXT = '".$FAQQuestion."', QUESTION_ANSWER = '".$FAQAnswer."' ,QUESTION_ORDER = '".$FAQOrder."', ACTIVE = '".$active."'  WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    
     public function delFAQ($id){
        $query = "DELETE FROM FAQ_TESTSERIES WHERE ID = ".$id;
        $result = parent::executeQuery($query);
        return $result;
    }
    public function getAllImageData($id=null,$active=null,$start,$end){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " I.ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        
        $query = "select I.*,C.COURSE_NAME from IMAGE_TESTSERIES I left join COURSE_TESTSERIES C on C.ID=I.COURSE_ID".$strWhere.$str." order by I.ID desc limit $start,$end";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function getAllVideoDataPagination(){
        $query="select count(*) as COUNT from VIDEO_TESTSERIES";
        $result = parent::executeQuery($query);
        $row=  mysql_fetch_array($result);
        $count=$row['COUNT'];    
        return $count;
    }
    public function getAllVideoData($id=null,$active=null,$start,$end){
        $str = "";
        $strWhere = "";
        if($id!=null || $id!=''):
            if($str!=""): $str = $str."AND"; endif;
            $str = " V.ID IN (".$id.")";
            $strWhere = " WHERE ";
        endif;
        
        $query = "select V.*,C.COURSE_NAME from VIDEO_TESTSERIES V left join COURSE_TESTSERIES C on C.ID=V.COURSE_ID".$strWhere.$str." order by ID desc limit $start,$end";
        $result = parent::executeQuery($query);
        return $result;
    }
    public function delImage($id) {
     $sql = "DELETE FROM IMAGE_TESTSERIES WHERE ID = '$id'" ;
     $result = parent::executeQuery($sql);
     return $result;
    }
    public function delVideo($id) {
     $sql = "DELETE FROM VIDEO_TESTSERIES WHERE ID = '$id'" ;
     $result = parent::executeQuery($sql);
     return $result;
    }
    
}

    

?>
