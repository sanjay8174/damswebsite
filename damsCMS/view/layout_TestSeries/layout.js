    function htmlDecode(value) {
        if (value) {
            var newval=$('<p />').html(value).text();
            return newval;

        } else {
            return '';
        }
    }

    function replacePercentile22(value){
        if (value){
        return value.replace("%22",'"');

        }else
            return "";

    }
    function clearAll(){

        $(' .uniform div span').attr('class','');
    }
    
//getDataAjax function for json get and send made by Dheeraj Batra

        var getDataAjax = function(url,type){
            this.url = url;
            this.type = type;
            this.getData = function(str){
                //alert(str);
                var resultData;
                var dataToPass = "data="+str;
                $.ajax({
                    url : this.url,
                    data : dataToPass,
                    type : 'post',
                    cache : false,
                    async : false,
                    traditional: true,
                    dataType: type,
                    success : function(result){
//                        alert(result);
                        resultData = result;
                    },
                    error : function(result){
//                        alert(result);
                    }
                });
                return resultData;
            }

            this.convertToJson = function(str){
                var jsonStr = JSON.stringify(str);
                return jsonStr;
            }

            this.convertToArray = function(str){
                var jsonStr = JSON.parse(str);
                return jsonStr;
            }

        }


         var get = function(arr,url,type){
//             alert(arr+'---'+url+'--'+type);
                   var obj = new getDataAjax(url,type);
                   var convJsonData = obj.convertToJson(arr);
                   var gotData = obj.getData(convJsonData);
                   return gotData;
         } 
               
var homeBannerData = function(){
    // this.getDataAll function used to get all the Banner's data
                        this.getDataAll = function(start,end){
                            $('#preloaderCenter').show();
                                var home = {};
                                home['mode'] = "getAllHomeData";
                                home['banner_start'] =start;
                                home['banner_end'] = end;
                                var homeDataRec = get(home,'index.php?p=13','json');
                                $('#banner_StartPoint').val(parseInt(start)+parseInt(homeDataRec.total));
                                 if(homeDataRec.total>0){
                                     $('#seeMoreBtn').show();
                                     var str = "";
                                     $('#homeBannerData').html('');
                                     for(var i=0;i< homeDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (i+1) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(homeDataRec.BANNER_NAME[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(homeDataRec.ORDER_BANNER[i])) + '</td>';
                                        if(homeDataRec.ACTIVE[i] == 'A'){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<a href="index.php?p=12&ln=1&t=7&e=2&id='+homeDataRec.ID[i]+'"><i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callHomeData.deleteData('+homeDataRec.ID[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';

                                        $('#homeBannerData').append(str);
                                     }
                                     if(homeDataRec.total<end || homeDataRec.total<=14){
                                         $('#seeMoreBtn').hide();
                                     }
                                    $('#preloaderCenter').hide();
                                 }else{
                                     $('#homeBannerData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                                      $('#preloaderCenter').hide();
                                      $('#seeMoreBtn').hide();
                                 }
                            }
                        this.getDataAllPagination = function(start,end){
                                var home = {};
                                home['mode'] = "getAllHomeData";
                                home['banner_start'] =start;
                                home['banner_end'] = end;
                                var homeDataRec = get(home,'index.php?p=13','json');
                                $('#banner_StartPoint').val(parseInt(start)+parseInt(homeDataRec.total));
//                                $('#banner_total').val(homeDataRec.count);
                                 if(homeDataRec.total>0){
                                     var str = "";
                                     var startCount=parseInt(start)+1;
                                     for(var i=0;i< homeDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td>' + (parseInt(i)+parseInt(startCount)) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(homeDataRec.BANNER_NAME[i])) + '</td>';
                                        str = str + '<td>' + unescape(decodeURIComponent(homeDataRec.ORDER_BANNER[i])) + '</td>';
                                        if(homeDataRec.ACTIVE[i] == 'A'){
                                            str = str + '<td>Active</td>';
                                        }else{
                                            str = str + '<td>Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<a href="index.php?p=12&ln=1&t=7&e=2&id='+homeDataRec.ID[i]+'"><i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callHomeData.deleteData('+homeDataRec.ID[i]+');"></i>' + '</td>';
                                        str = str + '</tr>';

                                        $('#homeBannerData').append(str).show('slow');
                                     }
                                     $('#preloader').hide();
                                      if(homeDataRec.total<end || homeDataRec.total<=14){
                                         $('#seeMoreBtn').hide();
                                     }
                                 }else{
                                        $('#homeBannerData').append("<tr><td colspan=8><br><br>No More Data.</td></tr>");
                                        $('#seeMoreBtn').hide();
                                        $('#preloader').hide();
                                }
                            }
                        this.saveData = function(){     
                        var banner = {};
                        var homeDataRec;
                        if ($('#modeBanner').val() == 1) {
                            banner['mode'] = "saveHomeBanner";
                            banner['data'] = {};
                            banner['data']['bannerName']    =escape(encodeURIComponent(($.trim($('#bannerField1').val()))));
                            banner['data']['bannerOrder']   =$.trim($('#bannerField2').val());
                            banner['data']['imageUpload']   =$.trim($('#bannerField3').val());  //for imageupload
                            banner['data']['contentBanner'] =$.trim($('#bannerField4 textarea').val());
                            banner['data']['contentBanner'] =$.trim(escape(encodeURIComponent(banner['data']['contentBanner'])));
                            banner['data']['active'] = ($('#bannerField5').is(':checked')) ? "A" : "I";
                                
                                
                                
                                
                                
                            if (banner['data']['bannerName'] == '') {
                                $("#bannerField1").focus();
                                $('#bannerField1').css('border-color', 'red');
                                $('#bannerNameError').show(300);
                                $('#bannerField1').keypress(function(){
                                $('#bannerField1').css('border-color', '');
                                $('#bannerNameError').hide(300);
                                });
                                return false;
                            } else if (banner['data']['bannerOrder'] == '') {
                                $("#bannerField2").focus();
                                $('#bannerField2').css('border-color', 'red');
                                $('#bannerOrderError').show(300);
                                $('#bannerField2').keypress(function(){
                                $('#bannerField2').css('border-color', '');   
                                $('#bannerOrderError').hide(300);
                                });
                                return false;
                            } else if (banner['data']['imageUpload'] == '') {
                                $('#bannerImgError').show(300);
                                $('#bannerField3').change(function(){
                                $('#bannerImgError').hide(300); 
                                });
                                return false;
                            }  else {
                                $('#preloaderCenter').show();
                                homeDataRec = get(banner, 'index.php?p=13', 'json');
                            }
                            if (homeDataRec.total > 0) {
                                var lastInsertedId = homeDataRec.lastInsertedId;
                                var file_data = $('#bannerField3').prop('files')[0];
                                var form_data = new FormData();
                                form_data.append('file', file_data);
                                var url = "index.php?p=14&m=ajaxUpload&lastInsertedId=" + lastInsertedId;
                               
                                    $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: form_data,
                                    async: false,
                                    processData: false,
                                    contentType: false,
                                    error: function (res) {
                                        alert("Unsuccessful.");
                                        return false;
                                    },
                                    success: function (res) {
                                        //alert(res);
                                        $('#preloaderCenter').hide();
                                    }
                                });
                                alert("Record inserted successfully.");
                                if(hidden == 'save'){
                                     location.href='index.php?p=12&ln=1&t=7';
                                }
                                $('#bannerField1').val("");
                                $('#bannerField2').val("");
                                $('#bannerField3').val("");
                                $('.filename').html("No file selected");
                                $('#bannerField4 textarea').val('');
                                var hidden = $('#bannerHiddenSave').val();
                               
                               
                            } else {
                                alert("No record inserted.");
                            }
                           
                           
                        }


                if ($('#modeBanner').val() == 2) {
                            var banner = {};
                            var homeDataRec;

                            banner['mode'] = "updateHomeBanner"; 
                            banner['data'] = {};
                            banner['data']['id']                = $.trim($("#idBanner").val());
                            banner['data']['bannerName']        = escape(encodeURIComponent(($.trim($('#bannerField1').val()))));
                            banner['data']['bannerOrder']       = $.trim($('#bannerField2').val());
                            if($('#bannerField3').val()== ''){
                                 banner['data']['imageUpload']  = $('#bannerFEdit3').val();  //for imageupload
                            }else {
                                 banner['data']['imageUpload']  = $('#bannerField3').val();  //for imageupload
                            }
                            banner['data']['contentBanner'] = $.trim($('#bannerField4 textarea').val());
                            banner['data']['contentBanner'] = $.trim(escape(encodeURIComponent(banner['data']['contentBanner'])));
                            banner['data']['active'] = ($('#bannerField5').is(':checked')) ? "A" : "I";
                            if (banner['data']['bannerName'] == '') {
                                $("#bannerField1").focus();
                                $('#bannerField1').css('border-color', 'red');
                                $('#bannerNameError').show(300);
                                $('#bannerField1').keypress(function(){
                                $('#bannerField1').css('border-color', '');
                                $('#bannerNameError').hide(300);
                                });
                                return false;
                            } else if (banner['data']['bannerOrder'] == '') {
                                $("#bannerField2").focus();
                                $('#bannerField2').css('border-color', 'red');
                                $('#bannerOrderError').show(300);
                                $('#bannerField2').keypress(function(){
                                $('#bannerField2').css('border-color', '');   
                                $('#bannerOrderError').hide(300);
                                });
                                return false;
                            } else if (banner['data']['imageUpload'] == '') {
                                $('#bannerImgError').show(300);
                                $('#bannerField3').change(function(){
                                $('#bannerImgError').hide(300); 
                                });
                                return false;
                            }  else {
                                $('#preloaderCenter').show();
                                homeDataRec = get(banner, 'index.php?p=13', 'json');
                            }
                                var Id = $("#idBanner").val();
                                var file_data = $('#bannerField3').prop('files')[0];
                                var form_data = new FormData();
                                form_data.append('file', file_data);
                                var url = "index.php?p=14&m=ajaxUpload&lastInsertedId=" + Id;
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: form_data,
                                    async: false,
                                    processData: false,
                                    contentType: false,
                                    error: function (res) {
                                        alert("Unsuccessful.");
                                        return false;
                                    },
                                    success: function (res) {
                                        //alert(res);
                                        $('#preloaderCenter').hide();
                                    }
                                });
                                if(homeDataRec.total>0){
                                    alert("Record updated successfully.");
                                    $('#bannerField1').val("");
                                    $('#bannerField2').val("");
                                    $('#bannerField3').val('');
                                    $('.filename').html('');
                                    $('#bannerField4 textarea').val("No file selected");
                                    $('#cancelBannerData').click();
                                    window.location="index.php?p=12&ln=1&t=7" ;
                                }else{
                                    alert("No Records Updated.");
                                }
                        }

                    }

                $("#bannerField3").change(function (){
                var imgWidth = '';
                var imgHeight = '';
                var input = document.getElementById("bannerField3");
                var fname = $("#bannerField3").val();
                var extension = fname.substr(fname.lastIndexOf(".") + 1);
                if (extension != 'png' && extension != 'jpg' && extension != 'jpeg') {
                    alert("Only Upload png,jpeg or jpg type images");
                    $("#bannerField3").val("");
                    $("#bannerField3").focus();
                    return false;
                }   

            });       
            this.editData = function(id){
                $('#preloaderCenter').show();
                    setTimeout(function() {
                    $("#uniform-bannerField3 span.filename").html(unescape(decodeURIComponent(homeDataRec.FILE_NAME)));
                    $('#bannerFEdit3').val(homeDataRec.FILE_NAME);
                    $('#preloaderCenter').hide();
                       }, 1000);
                   var home = {};
                   home['mode'] = "getEditHomeData";
                   home['id']   = id ;
                   var homeDataRec = get(home,'index.php?p=13','json');
                    $('#editBanner').html('Edit');
                    $('#bannerSubmit2').hide();
                    $('#modeBanner').val(2);
                    $('#idBanner').val(id);
                    $('#bannerField1').val(unescape(decodeURIComponent(homeDataRec.BANNER_NAME)));
                    $('#bannerField2').val(homeDataRec.ORDER_BANNER);
                    $('#bannerField4 textarea').val(unescape(decodeURIComponent(homeDataRec.BANNER_TEXT)));
                    if( homeDataRec.ACTIVE == 'A'){
                    $('#bannerField5').attr('class','checked');
                    $('#bannerField5').attr('checked','checked');
                    }else{
                    $('#bannerField5').attr('class','');
                    $('#bannerField5').removeAttr('checked');
                    }
            }

            this.deleteData = function(id){
                var result=confirm("Are you sure you want to delete this record ?");
                if(!result){
                    return false;
                }else{
                     $('#preloaderCenter').show();
                var banner = {};
                banner['mode'] = "deleteBanner";
                banner['data'] = {};
                banner['data']['id'] = id;
                var bannerDataRec = get(banner,'index.php?p=13','json');
                if(bannerDataRec.total>0){
                    var url = "index.php?p=14&m=ajaxUploadDelete&delId=" + bannerDataRec.delId +"&extension=" + bannerDataRec.extension;
                     $.ajax({
                               type: "POST",
                               url: url,
                               data: "",
                               async: false,
                               processData: false,
                               contentType: false,
                               error: function (res) {
                                   alert("Unsuccessful.");
                                   return false;
                               },
                               success: function (res) {
                                          
                               }
                           });
                   alert("Record Deleted successfully.");
                   var start=$('#banner_StartPoint').val();
                   this.getDataAll('0',parseInt(start));
                   
                }else{
                     $('#preloaderCenter').hide();
                   alert("No record deleted.");
                }
            }
   },
           this.cancel=function(id){
                  location.href='index.php?p=12&ln=1&t=7';      
                    
                }  

}

var courseData = function(){ 
                            this.getDataAll = function(start,end){
                                 
                                 $('#preloaderCenter').show();
                                var course = {};
                                course['mode'] = "getAllCourse";
                                course['course_start'] =start;
                                course['course_end'] = end;
                                var courseDataRec = get(course,'index.php?p=13','json');
                                $('#course_StartPoint').val(parseInt(start)+parseInt(courseDataRec.total)); 
                                 if(courseDataRec.total>0){
                                     $('#seeMoreBtn').show();
                                     var str = "";
                                     $('#courseData').html('');
                                     for(var i=0;i<courseDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td style="width:12%">' + (i+1) + '</td>';
                                        str = str + '<td style="width:40%">' + unescape(decodeURIComponent(courseDataRec.courseName[i])) + '</td>';
                                        str = str + '<td style="width:12%">' + unescape(decodeURIComponent(courseDataRec.courseOrder[i])) + '</td>';
                                        
                                        if(courseDataRec.active[i] == 'A'){
                                            str = str + '<td style="width:12%">Active</td>';
                                        }else{
                                            str = str + '<td style="width:12%">Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;width:12%">' +'<a href="index.php?p=12&ln=2&t=7&e=2&id='+courseDataRec.id[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '</tr>';

                                        $('#courseData').append(str);
                                     }
                                      $('#preloaderCenter').hide();
                                      if(courseDataRec.total<end || courseDataRec.total<=14){
                                         $('#seeMoreBtn').hide();
                                      }
                                 }else{
                                     $('#courseData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                                     $('#preloaderCenter').hide();
                                     $('#seeMoreBtn').hide();
                                 }
                            }
                            this.getDataAllPagination = function(start,end){
                                var course = {};
                                course['mode'] = "getAllCourse";
                                course['course_start'] =start;
                                course['course_end'] = end;
                                var courseDataRec = get(course,'index.php?p=13','json');
                                $('#course_StartPoint').val(parseInt(start)+parseInt(courseDataRec.total));  
                                 if(courseDataRec.total>0){
                                     var str = "";
                                     var startCount=parseInt(start)+1;
                                     for(var i=0;i<courseDataRec.total;i++){
                                        str = "";
                                        str = str + '<tr>';
                                        str = str + '<td style="width:12%">' + (i+startCount) + '</td>';
                                        str = str + '<td style="width:40%">' + unescape(decodeURIComponent(courseDataRec.courseName[i])) + '</td>';
                                        str = str + '<td style="width:12%">' + unescape(decodeURIComponent(courseDataRec.courseOrder[i])) + '</td>';
                                        
                                        if(courseDataRec.active[i] == 'A'){
                                            str = str + '<td style="width:12%">Active</td>';
                                        }else{
                                            str = str + '<td style="width:12%">Inactive</td>';
                                        }

                                        str = str + '<td style="text-align: center;cursor:pointer;width:12%">' +'<a href="index.php?p=12&ln=2&t=7&e=2&id='+courseDataRec.id[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                                        str = str + '</tr>';

                                        $('#courseData').append(str);
                                     }
                                     $('#preloader').hide();
                                     if(courseDataRec.total<end || courseDataRec.total<=14){
                                         $('#seeMoreBtn').hide();
                                      }
                                 }else{
                                     $('#courseData').append("<tr><td colspan=8><br><br>No More Data.</td></tr>");
                                     $('#seeMoreBtn').hide();
                                     $('#preloader').hide();
                                 }
                            }

                         this.saveData = function(){

                             var course = {};
                             var courseDataRec;
                             
                            if($('#modeCourse').val() == 1){
                                    
                                     course['mode'] = "saveCourse";
                                     course['data'] = {};
                                     course['data']['courseName'] = escape(encodeURIComponent(($.trim($('#courseField1').val()))));  //for name
                                     course['data']['courseOrder'] = $.trim($('#courseField2').val());  //for order
                                     course['data']['active'] = ($('#uniform-courseField15 span').attr('class')=='checked') ? "A" : "I";  //for settings
                                     
                                     if (course['data']['courseName']==''){
                                            $("#courseField1").focus();
                                            $('#courseField1').css('border-color', 'red');
                                            $('#courseField1').attr("placeholder","Add Course Name");
                                            $('#courseNameError').show(300);
                                            $('#courseField1').keypress(function(){
                                            $('#courseField1').css('border-color', '');
                                            $('#courseField1').attr("placeholder","Add Course Name");
                                            $('#courseNameError').hide(300);
                                            });
                                            
                                     }else if(course['data']['courseOrder']==''){
                                            $("#courseField2").focus();
                                            $('#courseField2').css('border-color', 'red');
                                            $('#CourseOrderError').show(300);
                                            $('#courseField2').keypress(function(){
                                            $('#courseField2').css('border-color', '');   
                                            $('#CourseOrderError').hide(300);
                                            });
                                     }
                                     else{
                                          $('#preloaderCenter').show();
                                         courseDataRec = get(course,'index.php?p=13','json');
                                     }
                                        if(courseDataRec.dubCourse=="0"){
                                            if(courseDataRec.total>0){

                                                alert(courseDataRec.total+" Record inserted successfully.");
                                               // location.href='index.php?p=1&ln=1&t=1';
                                                $('#courseField1').val("");
                                                $('#courseField2').val("");

                                                var hidden = $('#courseHiddenSave').val();
                                                if(hidden == 'save'){
                                                    location.href='index.php?p=12&ln=2&t=7';
                                                }
                                                if(hidden == 'save&new'){
                                                     $('#preloaderCenter').hide();
                                                   document.getElementById("myForm").reset();
                                                 }         

                                            }else{
                                                alert("No record inserted.");
                                            }
                                        }else{
                                            $("#courseField1").val('');
                                            $("#courseField1").focus();
                                            $('#courseField1').css('border-color', 'red');
                                            $('#courseNameError').show(300);
                                            $('#courseField1').attr("placeholder","Course Name allready exist");
                                            $('#courseField1').keypress(function(){
                                            $('#courseField1').css('border-color', ''); 
                                            $('#courseNameError').hide(300);
                                            });
                                            $('#preloaderCenter').hide();
                                            
                                            
                                        }
                                    }    


                               if($('#modeCourse').val() == 2){

                                     course['mode'] = "updateCourse";
                                     course['data'] = {};
                                     course['data']['courseName'] = escape(encodeURIComponent(($.trim($('#courseField1').val())))); 
                                     course['data']['courseOrder'] =$.trim($('#courseField2').val());  //for order
                                     course['data']['active'] = ($('#uniform-courseField15 span').attr('class')=='checked') ? 'A' : "I";  //for settings
                                     course['data']['id'] = $('#idCourse').val();  //for id
                                     if (course['data']['courseName']==''){
                                        $("#courseField1").focus();
                                            $('#courseField1').css('border-color', 'red');
                                            $('#courseNameError').show(300);
                                            $('#courseField1').keypress(function(){
                                            $('#courseField1').css('border-color', ''); 
                                            $('#courseNameError').hide(300);
                                            });
                                     }else if(course['data']['courseOrder']==''){
                                            $("#courseField2").focus();
                                            $('#courseField2').css('border-color', 'red');
                                            $('#CourseOrderError').show(300);
                                            $('#courseField2').keypress(function(){
                                            $('#courseField2').css('border-color', '');   
                                            $('#CourseOrderError').hide(300);
                                            });
                                     }
                                     else{
                                          $('#preloaderCenter').show();
                                         courseDataRec = get(course,'index.php?p=13','json');
                                     }
                                     if(courseDataRec.dubCourse=='0'){
                                        if(courseDataRec.total>0){
                                           alert(" Record Updated successfully.");
                                            $('#preloaderCenter').hide();
                                           location.href='index.php?p=12&ln=2&t=7';
                                           $('#uniform-courseField4 span.filename').html("");
                                           $('#cancelCourseData').click();
                                        }else{
                                           alert("No record updated.");
                                            $('#preloaderCenter').hide();
                                        }
                                 }else{
                                             $('#preloaderCenter').hide();
                                            $("#courseField1").val('');
                                            $("#courseField1").focus();
                                            $('#courseField1').css('border-color', 'red');
                                            $('#courseNameError').show(300);
                                            $('#courseField1').attr("placeholder","Course Name allready exist");
                                            $('#courseField1').keypress(function(){
                                            $('#courseField1').css('border-color', ''); 
                                            $('#courseNameError').hide(300);
                                            });
                                           
                                     
                                     }
                              }
                         }
                         $('#cancelCourseData').click(function(){

                         $('#modeCourse').val(1);
                         $('#idCourse').val("");
                         $('#uniform-courseField4 span.filename').html("");
                         $('#editCourse').html('Add');
                         $('#courseField5 textarea').val(unescape(decodeURIComponent('')));
                         $('#courseField6 textarea').val(unescape(decodeURIComponent('')));
                         clearAll();
                     });



                         this.editData = function(id){
                            $('#preloaderCenter').show();
                            $('#editCourse').html('Edit');
                            $('#courseSubmit2').hide();
                            $('#modeCourse').val(2);
                            $('#idCourse').val(id);
                             
                             var courseDataRec = {} ;
                             var course = {} ;
                             course['mode'] = "getAllCourse";
                             course['data'] = {};
                             course['data']['id'] = id ;
                             course['course_start'] =0;
                             course['course_end'] = 1;
                             courseDataRec = get(course,'index.php?p=13','json');
                             setTimeout(function() {
                            $("#uniform-courseField4 span.filename").html(unescape(decodeURIComponent(id + '.' + courseDataRec.imageUpload)));
                            $('#courseFEdit4').val(id + '.' + courseDataRec.imageUpload);
                                  }, 1000);
                            $('#courseField1').val(unescape(decodeURIComponent(courseDataRec.courseName[0])));
                            $('#courseField2').val(courseDataRec.courseOrder[0]);
                            if(courseDataRec.active[0] == "A"){
                                $('#uniform-courseField15 span').attr('class','checked');
                                $('#courseField15').attr('checked','checked');
                            }else{
                                $('#uniform-courseField15 span').attr('class','');
                                $('#courseField15').removeAttr('checked');
                            }
                             $('#preloaderCenter').hide();

                         }

                         this.deleteData = function(id){
                              $('#preloaderCenter').show();
                             var result=confirm("Are you sure you want to delete this record ?");
                             if(!result){
                                 return false;
                             }else{
                             $('#preloaderCenter').hide();
                             var course = {};
                             course['mode'] = "deleteCourse";
                             course['data'] = {};
                             course['data']['id'] = id;
                             var courseDataRec = get(course,'index.php?p=13','json');
                             if(courseDataRec.total>0){
                                alert(" Record Deleted successfully.");
                                var end=parseInt($('#course_StartPoint').val());
                                this.getDataAll(0,end);
                             }else{
                                alert("No record Deleted.");
                             }
                        }
                    }
                    this.cancel=function(id){
                    location.href='index.php?p=12&ln=2&t=7';      
                    
                }
         }
         
         
         //start for page heading............
         
            var pageData = function(){
            this.getDataAll = function(){
                var page = {};
                page['mode'] = "getAllPage";
                var pageDataRec = get(page,'index.php?p=13','json');
                if(pageDataRec.total>0){
                    var str = "";
                    $('#pageData').html('');
                    for(var i=0;i<pageDataRec.total;i++){
                        str = "";
                        str = str + '<tr>';
                        str = str + '<td>' + (i+1) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(pageDataRec.PAGE_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(pageDataRec.PAGE_URL[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(pageDataRec.PAGE_ORDER[i])) + '</td>';

                        if(pageDataRec.ACTIVE[i] == 'A'){
                            str = str + '<td>Active</td>';
                        }else{
                            str = str + '<td>Inactive</td>';
                        }

                        str = str + '<td style="text-align: center;cursor:pointer;">' +'<a href="index.php?p=12&ln=4&t=7&e=2&id='+pageDataRec.ID[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="pageData1.deletePage('+pageDataRec.ID[i]+');"></i>' + '</td>';
                        str = str + '</tr>';

                        $('#pageData').append(str);
                     }
                }else{
                     $('#pageData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                }
            }


            this.saveData = function(){  
                var page = {};
                var pageDataRec;
                if($('#modePage').val() == 1){
                    page['mode'] = "savePage";
                    page['data'] = {};
                    page['data']['pageName'] = escape(encodeURIComponent(($.trim($('#pageField1').val()))));  //for name
                    page['data']['pageURL'] =  escape(encodeURIComponent(($.trim($('#pageField2').val()))));
                    page['data']['pageOrder'] =$.trim($('#pageField3').val());  //for order
                    page['data']['active'] = ($('#uniform-pageField4 span').attr('class')=='checked') ? "A" : "I";  //for settings
                   
                    if (page['data']['pageName']==''){
                        $("#pageField1").focus();
                        $('#pageField1').css('border-color', 'red');
                        $('#pageNameError').show(300);
                        $('#pageField1').keypress(function(){
                            $('#pageField1').css('border-color', ''); 
                            $('#pageNameError').hide(300);
                        });
                    }else if(page['data']['pageURL']==''){
                            $("#pageField2").focus();
                            $('#pageField2').css('border-color', 'red');
                            $('#pageURLError').show(300);
                            $('#pageField2').keypress(function(){
                                $('#pageField2').css('border-color', '');   
                                $('#pageURLError').hide(300);
                            });
                    }else if(page['data']['pageOrder']==''){
                            $("#pageField3").focus();
                            $('#pageField3').css('border-color', 'red');
                            $('#pageOrderError').show(300);
                            $('#pageField3').keypress(function(){
                                $('#pageField3').css('border-color', '');   
                                $('#pageOrderError').hide(300);
                            });
                    }else{
                        pageDataRec = get(page,'index.php?p=13', 'json'); 
                     }
                     
                    if( parseInt(pageDataRec.total) > 0){
                        alert(pageDataRec.total+" Record inserted successfully.");
                        $('#pageField1').val("");
                        $('#pageField2').val("");
                        $('#pageField3').val("");

                        var hidden = $('#pageHiddenSave').val();
                        if(hidden == 'save'){
                            $('#cancelPageData').click();
                            location.href='index.php?p=12&ln=4&t=7';
                        }
                        if(hidden == 'save&new'){
//                            $('#cancelPageData').click();
                            this.getDataAll();
                         }         

                    }else{
                        alert("No record inserted.");
                    }
                }

                if($('#modePage').val() == 2){ 
                page['mode'] = "updatePage";
                page['data'] = {};
                page['data']['pageName']  =escape(encodeURIComponent(($.trim($('#pageField1').val()))));  //for name
                page['data']['pageURL']   =escape(encodeURIComponent(($.trim($('#pageField2').val()))));
                page['data']['pageOrder'] =$.trim( $('#pageField3').val());  //for order
                page['data']['active']    = ($('#uniform-pageField4 span').attr('class')=='checked') ? 'A' : "I";  //for settings
                page['data']['id']        = $('#idPage').val();  //for id
                if (page['data']['pageName']==''){
                        $("#pageField1").focus();
                        $('#pageField1').css('border-color', 'red');
                        $('#pageNameError').show(300);
                        $('#pageField1').keypress(function(){
                            $('#pageField1').css('border-color', ''); 
                            $('#pageNameError').hide(300);
                        });
                    }else if(page['data']['pageURL']==''){
                            $("#pageField2").focus();
                            $('#pageField2').css('border-color', 'red');
                            $('#pageURLError').show(300);
                            $('#pageField2').keypress(function(){
                                $('#pageField2').css('border-color', '');   
                                $('#pageNameError').hide(300);
                            });
                    }else if(page['data']['pageOrder']==''){
                            $("#pageField3").focus();
                            $('#pageField3').css('border-color', 'red');
                            $('#pageOrderError').show(300);
                            $('#pageField3').keypress(function(){
                                $('#pageField3').css('border-color', '');   
                                $('#pageOrderError').hide(300);
                            });
                    } else{
                        pageDataRec = get(page,'index.php?p=13','json');
                    }
                    if(pageDataRec.total>0){
                        alert(" Record Updated successfully.");
                        location.href='index.php?p=12&ln=4&t=7';
                        $('#uniform_on-pageField4 span.filename').html("");
                        $('#cancelPageData').click();
                        this.getDataAll();
                    }else{
                        alert("No record updated.");
                    }
              }
         }
         
         $('#cancelPageData').click(function(){
            $('#modePage').val(1);
            $('#idPage').val("");
            $('#uniform-pageField4 span.filename').html("");
            $('#editPage').html('Add');

            clearAll();
        });

        this.editData = function(id){
            $('#editPage').html('Edit');
            $('#pageSubmit2').hide();
            $('#modePage').val(2);
            $('#idPage').val(id);
            var pageDataRec = {} ;
            var page = {} ;
            page['mode'] = "getAllPage";
            page['data'] = {};
            page['data']['id'] = id ;
            pageDataRec = get(page,'index.php?p=13','json');
            $('#pageField1').val(unescape(decodeURIComponent(pageDataRec.PAGE_NAME[0])));
            $('#pageField2').val(unescape(decodeURIComponent(pageDataRec.PAGE_URL[0])));
            $('#pageField3').val(pageDataRec.PAGE_ORDER[0]);
            if(pageDataRec.ACTIVE[0] == 'A'){
                $('#uniform_on-pageField4 span').attr('class','checked');
                $('#pageField4').attr('checked','checked');
            }else{
                $('#uniform_on-pageField4 span').attr('class','');
                $('#pageField4').removeAttr('checked');
            }

         }

        this.deletePage = function(id){
            var result=confirm("Are you sure you want to delete this record ?");
            if(!result){
                return false;
            }else{
                var page = {};
                page['mode'] = "deletePage";
                page['data'] = {};
                page['data']['id'] = id;
                var pageDataRec = get(page,'index.php?p=13','json');
                if(pageDataRec.total>0){
                    alert(" Record Deleted successfully.");
                    this.getDataAll();
                }else{
                        alert("No Recods Deleted.");
                    }
                }
            },
            this.cancel=function(id){
                    location.href='index.php?p=12&ln=2&t=7';      
                    
                }
    }
         
         //end for page heading
   var packageData={
        getDataAll:function(start,end){
            $('#preloaderCenter').show();
                var getPackage = {};
                getPackage['mode']         = "getAllPackageData";
                getPackage['package_start'] =start;
                getPackage['package_end']   = end;
                var getPackageRec = get(getPackage,'index.php?p=13','json');
                $('#package_StartPoint').val(parseInt(start)+parseInt(getPackageRec.total));
                if(getPackageRec.total>0){
                     $('#seeMoreBtn').show();
                    var str = "";
                    $('#pacakgeshowCount').val(getPackageRec.total);
                    for(var i=0;i< getPackageRec.total;i++){
                        str = "";
                        var m=1+parseInt(i);
                        str = str + "<tr id='packageShow"+getPackageRec.ID[i]+"'>";
                        str = str + "<td id='packagTd"+m+"'>"+m+"</td>";
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.PACKAGE_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.COURSE_NAME[i]))  + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.PACKAGE_COST[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.PACKAGE_ORDER[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.TOTAL_TABS[i])) + '</td>';
                        if(getPackageRec.ACTIVE[i] == 'A'){
                            str = str + '<td>Active</td>';
                        }else{
                            str = str + '<td>Inactive</td>';
                        }

                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<a href="index.php?p=12&ln=6&t=7&e=2&id='+getPackageRec.ID[i]+'"><i class="icon-edit"></i></a>' + '</td>';
                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="packageData.deleteData.deletePackage('+getPackageRec.ID[i]+');"></i>' + '</td>';
                        str = str + '</tr>';

                        $('#packageData').append(str);
                    }
                    $('#preloaderCenter').hide();
                        if(getPackageRec.total<end || getPackageRec.total<=14){
                            $('#seeMoreBtn').hide();
                        }
                }else{
                     $('#packageData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                     $('#preloaderCenter').hide();
                 }
        },
        getDataAllPagination:function(start,end){
                var getPackage = {};
                getPackage['mode']         = "getAllPackageData";
                getPackage['package_start'] =start;
                getPackage['package_end']   = end;
                var getPackageRec = get(getPackage,'index.php?p=13','json');
                $('#package_StartPoint').val(parseInt(start)+parseInt(getPackageRec.total));
                if(getPackageRec.total>0){
                    var str = "";
                    var countPackageAll=$('#pacakgeshowCount').val();
                    $('#pacakgeshowCount').val(parseInt(getPackageRec.total)+parseInt(countPackageAll));
                    var startCount=parseInt(start);
                    for(var i=0;i< getPackageRec.total;i++){
                        str = "";
                        var m=1+parseInt(i)+parseInt(countPackageAll);
                        str = str + "<tr id='packageShow"+getPackageRec.ID[i]+"'>";
                        str = str + "<td id='packagTd"+m+"'>"+m+"</td>";
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.PACKAGE_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.COURSE_NAME[i]))  + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.PACKAGE_COST[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.PACKAGE_ORDER[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(getPackageRec.TOTAL_TABS[i])) + '</td>';
                        if(getPackageRec.ACTIVE[i] == 'A'){
                            str = str + '<td>Active</td>';
                        }else{
                            str = str + '<td>Inactive</td>';
                        }

                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<a href="index.php?p=12&ln=6&t=7&e=2&id='+getPackageRec.ID[i]+'"><i class="icon-edit"></i></a>' + '</td>';
                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="packageData.deleteData.deletePackage('+getPackageRec.ID[i]+');"></i>' + '</td>';
                        str = str + '</tr>';

                        $('#packageData').append(str);
                    }
                    $('#preloader').hide();
                    if(getPackageRec.total<end || getPackageRec.total<=14){
                            $('#seeMoreBtn').hide();
                    }
                }else{
                     $('#packageData').append("<tr><td colspan=8><br><br>No More Data.</td></tr>");
                      $('#seeMoreBtn').hide();
                      $('#preloader').hide();
                 }
        },
        
        saveData:function(){
            var package = {};
            var homeDataRec;
            if ($('#modePackage').val() == 1) {
                package['mode'] = "saveHomePackage";
                package['data'] = {};
                package['data']['course_id']            =$.trim($('#packageCourse').val());
                package['data']['package_name']         =escape(encodeURIComponent(($.trim($('#packageName').val()))));
                package['data']['package_detail_Name']  =escape(encodeURIComponent(($.trim($('#packageDetailName').val()))));  
                package['data']['cost']                 =$.trim($('#packageCost').val());
                package['data']['package_Url']          =escape(encodeURIComponent($.trim($('#packageLink').val())));
                package['data']['order']                =$.trim($('#packageOrder').val());
                package['data']['active']               =($('#packageActive').is(':checked')) ? "A" : "I";
                package['data']['package_duration']     =escape(encodeURIComponent(($.trim($('#packageDuration').val()))));
                package['data']['package_totalNoOfTest']=escape(encodeURIComponent(($.trim($('#totalNoOfTest').val()))));
                package['data']['package_totalQues']    =escape(encodeURIComponent(($.trim($('#totalQuesPerTest').val()))));
                package['data']['package_detailText']   =escape(encodeURIComponent(($.trim($('#packageDetailText').val()))));

                if(package['data']['course_id']=='' || package['data']['course_id']==null){
                  $('#packageCourseError').show(300);
                  $('#packageCourse').focus();
                  $('#packageCourse').css('border-color','red');
                  $('#packageCourse').change(function(){
                  $('#packageCourse').css('border-color', '');  
                  $('#packageCourseError').hide(300);
                  });
                  return false;
                }
                else if(package['data']['package_name']=='' || package['data']['package_name']==null){
                  $("#packageName").focus();
                  $('#packageName').css('border-color', 'red');
                  $('#packageNameError').show(300);
                  $('#packageName').keypress(function(){
                  $('#packageName').css('border-color', '');  
                  $('#packageNameError').hide(300);
                  });
                   return false;
                }
                else if(package['data']['package_detail_Name']==''||package['data']['package_detail_Name']==null){
                  $("#packageDetailName").focus();
                  $('#packageDetailName').css('border-color', 'red');
                  $('#packageDetailNameError').show(300);
                  $('#packageDetailName').keypress(function(){
                  $('#packageDetailName').css('border-color', '');   
                  $('#packageDetailNameError').hide(300);
                  });
                   return false;
                }
                else if(package['data']['cost']==''||package['data']['cost']==null){
                  $("#packageCost").focus();
                  $('#packageCost').css('border-color', 'red');
                  $('#packageCostError').show(300);
                  $('#packageCost').keypress(function(){
                  $('#packageCost').css('border-color', '');   
                  $('#packageCostError').hide(300);
                  });
                   return false;
                }
                else if(package['data']['package_Url']==''||package['data']['package_Url']==null){
                  $("#packageLink").focus();
                  $('#packageLink').css('border-color', 'red');
                  $('#packageLinkError').show(300);
                  $('#packageLink').keypress(function(){
                  $('#packageLink').css('border-color', '');   
                  $('#packageLinkError').hide(300);
                  });
                   return false;
                }

                else if(package['data']['order']==''||package['data']['order']==null){
                    $("#packageOrder").focus();
                    $('#packageOrder').css('border-color', 'red');
                    $('#packageOrderError').show(300);
                    $('#packageOrder').keypress(function(){
                    $('#packageOrder').css('border-color', '');   
                    $('#packageOrderError').hide(300);
                    });
                   return false;
                }
                else{
                    $('#preloaderCenter').show();
                homeDataRec = get(package, 'index.php?p=13', 'json');  
            }

                var tabCount=parseInt($('#srNo').val());
                var package_Tabs_Data;
                var package_Tabs = {};
                package_Tabs['mode'] = "package_Tabs";
                package_Tabs['data'] = {}; 

                if (homeDataRec.total > 0) {
                    package_Tabs['data']['Id'] =homeDataRec.lastInsertedId;
                    var divIds=new Array();
                    var p=1;
                    $('.damsCms').each(function (index, value) { 
                        divIds[p]=$(this).attr('name');
                        p++;
                    });
                    for(var k=1;k<=tabCount;k++){
                        package_Tabs['data']['package_tabName']     =escape(encodeURIComponent(($.trim($('#tabName'+divIds[k]).val()))));
                        package_Tabs['data']['package_tabOrder']    =$.trim($('#tabOrder'+divIds[k]).val());
                        package_Tabs['data']['package_excel']       =$('#excelField'+divIds[k]).val();
                        package_Tabs['data']['package_tabActive']   =($('#tabActive'+divIds[k]).is(':checked')) ? "A" : "I";
                 
                        if(package['data']['course_id']=='' || package['data']['course_id']==null){
                        $('#preloaderCenter').hide();
                        $('#packageCourseError').show(300);
                        $('#packageCourse').focus();
                        //$('#packageCourse').scrollTop(100);
                        $('#packageCourse').change(function(){
                        $('#packageCourse').css('border-color', '');  
                        $('#packageCourseError').hide(300);
                        });
                        return false;
                        }else if(package_Tabs['data']['package_tabName']=='' || package_Tabs['data']['package_tabName']==null){
                           $('#preloaderCenter').hide();
                           $('#tabName'+divIds[k]).focus();
                           $('#tabName'+divIds[k]).css('border-color', 'red');
                           $('#tabNameError'+divIds[k]).show(300);
                           $('#tabName'+divIds[k]).keypress(function(){
                           $('#tabName'+divIds[k]).css('border-color', '');
                           $('#tabNameError'+divIds[k]).hide(300);
                           });
                           return false;
                        }else if(package_Tabs['data']['package_tabOrder']=='' || package_Tabs['data']['package_tabOrder']==null){
                           $('#preloaderCenter').hide();
                           $('#tabOrder'+divIds[k]).focus();
                           $('#tabOrder'+divIds[k]).css('border-color', 'red');
                           $('#tabOrderError'+divIds[k]).show(300);
                           $('#tabOrder'+divIds[k]).keypress(function(){
                           $('#tabOrder'+divIds[k]).css('border-color', '');   
                           $('#tabOrderError'+divIds[k]).hide(300);
                           });
                           return false;
                        }else if(package_Tabs['data']['package_excel']=='' || package_Tabs['data']['package_excel']==null){
                           $('#preloaderCenter').hide();
                           $('#excelField'+divIds[k]).focus();
                           $('#excelField'+divIds[k]).css('border-color', 'red');
                           $('#tabFileError'+divIds[k]).show(300);
                           $('#excelField'+divIds[k]).change(function(){
                           $('#excelField'+divIds[k]).css('border-color', '');   
                           $('#tabFileError'+divIds[k]).hide(300);
                           });
                           return false;
                        }
                    }
                    $('#preloaderCenter').show();
                    for(var k=1;k<=tabCount;k++){
                        package_Tabs['data']['package_tabName'] =escape(encodeURIComponent(($('#tabName'+divIds[k]).val())));
                        package_Tabs['data']['package_tabOrder'] =$('#tabOrder'+divIds[k]).val();
                        package_Tabs['data']['package_excel'] =$('#excelField'+divIds[k]).val();
                        package_Tabs['data']['package_tabActive'] =($('#tabActive'+divIds[k]).is(':checked')) ? "A" : "I";
                        package_Tabs_Data = get(package_Tabs, 'index.php?p=13', 'json');
                        
                        if (package_Tabs_Data.total > 0) {
                            var lastInsertedId_tab = package_Tabs_Data.lastInsertedId;
                            var file_data = $('#excelField'+divIds[k]).prop('files')[0];
                            var form_data = new FormData();
                            form_data.append('file', file_data);
//                            alert(form_data.file);
//                            return false;
                            var url = "index.php?p=14&m=excelUpload&lastInsertedId=" + lastInsertedId_tab;
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: form_data,
                                async: false,
                                processData: false,
                                contentType: false,
                                error: function (res) {
                                    alert("Unsuccessful.");
                                    return false;
                                },
                                success: function (res) {
                                      $('#preloaderCenter').hide();
                                    }
                                });

                                }
                            }
                            $('#preloaderCenter').hide();
                            alert("Record inserted successfully.");
                            var hidden = $('#packageHiddenSave').val();
                            if(hidden == 'save'){
                                location.href='index.php?p=12&ln=6&t=7';
                            }
                            if(hidden == 'save&new'){
                              $('.form-horizontal').trigger("reset");
                                
                            }         
  
                            }else {
                                $('#preloaderCenter').hide();
                                alert("No record inserted.");
                            }
                     }
                     
                     
        if ($('#modePackage').val() == 2) {
            package['mode'] = "updateHomePackage";
            package['data'] = {};
            package['data']['id']                   = $.trim($('#packageId').val());
            package['data']['course_id']            = $.trim($('#packageCourse').val());
            package['data']['package_name']         = escape(encodeURIComponent(($.trim($('#packageName').val()))));
            package['data']['package_detail_Name']  = escape(encodeURIComponent(($.trim($('#packageDetailName').val()))));  
            package['data']['cost']                 = $.trim($('#packageCost').val());
            package['data']['package_Url']          = escape(encodeURIComponent($.trim($('#packageLink').val())));
            package['data']['order']                = $.trim($('#packageOrder').val());
            package['data']['active']               = ($('#packageActive').is(':checked')) ? "A" : "I";
            package['data']['package_duration']     = escape(encodeURIComponent(($.trim($('#packageDuration').val()))));
            package['data']['package_totalNoOfTest']= escape(encodeURIComponent(($.trim($('#totalNoOfTest').val()))));
            package['data']['package_totalQues']    = escape(encodeURIComponent(($.trim($('#totalQuesPerTest').val()))));
            package['data']['package_detailText']   = escape(encodeURIComponent(($.trim($('#packageDetailText').val()))));

            if(package['data']['course_id']==''||package['data']['course_id']==null){
                    $("#packageCourse").focus();
                    $('#packageCourse').css('border-color', 'red');
                    $('#packageCourse').keypress(function(){
                    $('#packageCourse').css('border-color', '');   
                });
                return false;
            }else if(package['data']['package_name']==''||package['data']['package_name']==null){
                    $("#packageName").focus();
                    $('#packageName').css('border-color', 'red');
                    $('#packageNameError').show(300);
                    $('#packageName').keypress(function(){
                    $('#packageName').css('border-color', '');  
                    $('#packageNameError').hide(300);
                });
                return false;
            }else if(package['data']['package_Url']==''||package['data']['package_Url']==null){
                    $("#packageLink").focus();
                    $('#packageLink').css('border-color', 'red');
                    $('#packageLinkError').show(300);
                    $('#packageLink').keypress(function(){
                    $('#packageLink').css('border-color', '');  
                    $('#packageLinkError').hide(300);
                });
                return false;
            }else if(package['data']['package_detail_Name']==''||package['data']['package_detail_Name']==null){
                    $("#packageDetailName").focus();
                    $('#packageDetailName').css('border-color', 'red');
                    $('#packageDetailNameError').show(300);
                    $('#packageDetailName').keypress(function(){
                    $('#packageDetailName').css('border-color', '');   
                    $('#packageDetailNameError').hide(300);
                  });
                return false;
            }else if(package['data']['cost']==''||package['data']['cost']==null){
                    $("#packageCost").focus();
                    $('#packageCost').css('border-color', 'red');
                    $('#packageCostError').show(300);
                    $('#packageCost').keypress(function(){
                    $('#packageCost').css('border-color', '');   
                    $('#packageCostError').hide(300);
                  });
                return false;
            }else if(package['data']['order']==''||package['data']['order']==null){
                    $("#packageOrder").focus();
                    $('#packageOrder').css('border-color', 'red');
                    $('#packageOrderError').show(300);
                    $('#packageOrder').keypress(function(){
                    $('#packageOrder').css('border-color', '');   
                    $('#packageOrderError').hide(300);
                    });
                return false;
//            }else if(package['data']['package_duration']==''||package['data']['package_duration']==null){
//                    $("#packageDuration").focus();
//                    $('#packageDuration').css('border-color', 'red');
//                    $('#packageDurationError').show(300);
//                    $('#packageDuration').keypress(function(){
//                    $('#packageDuration').css('border-color', '');   
//                    $('#packageDurationError').hide(300);
//                    }); 
//                return false;
//            }else if(package['data']['package_totalNoOfTest']==''||package['data']['package_totalNoOfTest']==null){
//                    $("#totalNoOfTest").focus();
//                    $('#totalNoOfTest').css('border-color', 'red');
//                    $('#packagenoOfTestError').show(300);
//                    $('#totalNoOfTest').keypress(function(){
//                    $('#totalNoOfTest').css('border-color', '');
//                    $('#packagenoOfTestError').hide(300);
//                    });
//                return false;
//            }else if(package['data']['package_totalQues']==''||package['data']['package_totalQues']==null){
//                    $("#totalQuesPerTest").focus();
//                    $('#totalQuesPerTest').css('border-color', 'red');
//                    $('#packageTotalNoQuestionError').show(300);
//                    $('#totalQuesPerTest').keypress(function(){
//                    $('#totalQuesPerTest').css('border-color', '');   
//                    $('#packageTotalNoQuestionError').hide(300);
//                    });
//                return false;
            }else{
                $('#preloaderCenter').show();
                homeDataRec = get(package, 'index.php?p=13', 'json');  
            }   
                    var toatlTabCount=0;
                    if(parseInt($('#srNo').val())>0){
                    var tabCount=parseInt($('#srNo').val());
                    var package_Tabs_Data;
                    var package_Tabs = {};
                    package_Tabs['mode'] = "package_TabsUpdate";
                    package_Tabs['data'] = {}; 

                    package_Tabs['data']['Id'] =homeDataRec.lastInsertedId;
                    var divIds=new Array();
                    var p=1;
                    $('.damsCms').each(function (index, value) { 
                            divIds[p]=$(this).attr('name');
                            p++;
                    });
                        for(var k=1;k<=tabCount;k++){
                            var fileNameExcel='';
                            if($('#excelField'+divIds[k]).val()==undefined || $('#excelField'+divIds[k]).val()==''){
                                fileNameExcel=$('#excelField'+divIds[k]).attr('value');
                            }else{
                                fileNameExcel=$('#excelField'+divIds[k]).val();
                            }
                            package_Tabs['data']['id']                  =divIds[k];
                            package_Tabs['data']['Packageid']           =$.trim($('#packageId').val());
                            package_Tabs['data']['package_tabName']     =escape(encodeURIComponent(($.trim($('#tabName'+divIds[k]).val()))));
                            package_Tabs['data']['package_tabOrder']    =$.trim($('#tabOrder'+divIds[k]).val());
                            package_Tabs['data']['package_excel']       =fileNameExcel;
                            package_Tabs['data']['package_tabActive']   =($('#tabActive'+divIds[k]).is(':checked')) ? "A" : "I";
                                if(package['data']['course_id']=='' || package['data']['course_id']==null){
                                $('#preloaderCenter').hide();
                                $('#packageCourseError').show(300);
                                $('#courseFocus').focus();
                                $('#packageCourse').scrollTop(100);
                                $('#packageCourse').change(function(){
                                $('#packageCourse').css('border-color', '');  
                                $('#packageCourseError').hide(300);
                                });
                                return false;
                            }else if(package_Tabs['data']['package_tabName']=='' || package_Tabs['data']['package_tabName']==null){
                                   $('#preloaderCenter').hide();
                                   $('#tabName'+divIds[k]).focus();
                                   $('#tabName'+divIds[k]).css('border-color', 'red');
                                   $('#tabNameError'+divIds[k]).show(300);
                                   $('#tabName'+divIds[k]).keypress(function(){
                                   $('#tabName'+divIds[k]).css('border-color', '');
                                   $('#tabNameError'+divIds[k]).hide(300);
                                   
                                   });
                                   return false;
                            }else if(package_Tabs['data']['package_tabOrder']=='' || package_Tabs['data']['package_tabOrder']==null){
                                   $('#preloaderCenter').hide();
                                   $('#tabOrder'+divIds[k]).focus();
                                   $('#tabOrder'+divIds[k]).css('border-color', 'red');
                                   $('#tabOrderError'+divIds[k]).show(300);
                                   $('#tabOrder'+divIds[k]).keypress(function(){
                                   $('#tabOrder'+divIds[k]).css('border-color', '');   
                                   $('#tabOrderError'+divIds[k]).hide(300);
                                   });
                                   return false;
                            }else if(package_Tabs['data']['package_excel']=='' || package_Tabs['data']['package_excel']==null){
                                   $('#preloaderCenter').hide();
                                   $('#excelField'+divIds[k]).focus();
                                   $('#excelField'+divIds[k]).css('border-color', 'red');
                                   $('#tabFileError'+divIds[k]).show(300);
                                   $('#excelField'+divIds[k]).change(function(){
                                   $('#excelField'+divIds[k]).css('border-color', '');   
                                   $('#tabFileError'+divIds[k]).hide(300);
                                   });
                                   return false;
                                }
                            }
                            $('#preloaderCenter').show();
                        for(var k=1;k<=tabCount;k++){
                            var fileNameExcel='';
                            if($('#excelField'+divIds[k]).val()==undefined || $('#excelField'+divIds[k]).val()==''){
                                fileNameExcel=$('#excelField'+divIds[k]).attr('value');
                            }else{
                                fileNameExcel=$('#excelField'+divIds[k]).val();
                            }
                            package_Tabs['data']['id']                  =$.trim($('#hiddenTabId'+divIds[k]).val());
                            package_Tabs['data']['Packageid']           =$.trim($('#packageId').val());
                            package_Tabs['data']['package_tabName']     =$.trim(escape(encodeURIComponent(($('#tabName'+divIds[k]).val()))));
                            package_Tabs['data']['package_tabOrder']    =$.trim($('#tabOrder'+divIds[k]).val());
                            package_Tabs['data']['package_excel']       =fileNameExcel;
                            package_Tabs['data']['package_tabActive']   =($('#tabActive'+divIds[k]).is(':checked')) ? "A" : "I";
                            package_Tabs_Data = get(package_Tabs, 'index.php?p=13', 'json');
                            toatlTabCount=toatlTabCount+package_Tabs_Data.total_package;
                            if (package_Tabs_Data.total_package > 0) {
                                var lastInsertedId_tab = package_Tabs_Data.lastInsertedId;
                                var file_data = $('#excelField'+divIds[k]).prop('files')[0];
                                var form_data = new FormData();
                                form_data.append('file', file_data);
                                var url = "index.php?p=14&m=excelUpload&lastInsertedId=" + lastInsertedId_tab;
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: form_data,
                                    async: false,
                                    processData: false,
                                    contentType: false,
                                    error: function (res) {
                                        alert("Unsuccessful.");
                                        return false;
                                    },
                                    success: function (res) {
                                                      $('#preloaderCenter').hide();
                                    }
                                });

                                }

                            }
                        }
                            if(homeDataRec.total_package>0 ||toatlTabCount>0 || homeDataRec.total_package_Detail>0){
                                alert("Record updated successfully.");
                                window.location="index.php?p=12&ln=6&t=7" ;

                            }else{
                                 $('#preloaderCenter').hide();
                                alert("No record updated");
                            }


                }
    },
                    editData : function(id){
                                    $('#preloaderCenter').show();
                                    var editPackage = {};
                                    editPackage['mode'] = "getPackageDataInEdit";
                                    $('#packageSubmit2').hide();
                                    editPackage['id']   = id ;
                                    var editPackageRec = get(editPackage,'index.php?p=13','json');
                                    $('#editPackage').html('Edit');
                                    $('#modePackage').val(2);
                                    $('#packageId').val(id);
                                    this.getCourse(editPackageRec.COURSE_ID);
                                    $('#packageName').val(unescape(decodeURIComponent(editPackageRec.PACKAGE_NAME)));
                                    $('#packageLink').val(unescape(decodeURIComponent(editPackageRec.PACKAGE_URL)));
                                    $('#packageDetailName').val(unescape(decodeURIComponent(editPackageRec.PACKAGE_DETAILED_NAME)));
                                    $('#packageCost').val(unescape(decodeURIComponent(editPackageRec.PACKAGE_COST)));
                                    $('#packageOrder').val(unescape(decodeURIComponent(editPackageRec.PACKAGE_ORDER)));
                                    
                                    if( editPackageRec.ACTIVE == 'A'){
                                       $('#packageActive').attr('class','checked');
                                       $('#packageActive').attr('checked','checked');
                                   }else{
                                       $('#packageActive').attr('class','');
                                       $('#packageActive').removeAttr('checked');
                                   }
                                   $('#packageDuration').val(unescape(decodeURIComponent(editPackageRec.DURATION)));
                                   $('#totalNoOfTest').val(unescape(decodeURIComponent(editPackageRec.TOTAL_TEST)));
                                   $('#totalQuesPerTest').val(unescape(decodeURIComponent(editPackageRec.TOTAL_QUESTIONS_DETAIL)));
                                   $('#packageDetailText').val(unescape(decodeURIComponent(editPackageRec.PACKAGE_DETAIL)));
                                   
                                    var getPackageTab = {};
                                    getPackageTab['mode'] = "getAllPackageDataTabs";
                                    getPackageTab['id'] = id;
                                    var getPackageTabRec = get(getPackageTab,'index.php?p=13','json');
                                    if(getPackageTabRec.total>0){
                                    var str = "";
                                    $('#addMoreDiv').hide();
                                     for(var i=1;i<=getPackageTabRec.total;i++){
                                         var id=getPackageTabRec.ID[i];
                                         var tabName=unescape(decodeURIComponent(getPackageTabRec.TAB_NAME[i]));
                                         var tabOrder=getPackageTabRec.TAB_ORDER[i];
                                         var file=unescape(decodeURIComponent(getPackageTabRec.FILE_NAME[i]));
                                         var active=getPackageTabRec.ACTIVE[i];
                                          str = '<div id="tabDiv'+id+'"name="'+id+'" title="uo5" class="damsCms">\n\
                                            <input type="hidden" id="hiddenTabId'+id+'" value="'+id+'">\n\
                                        <div class="control-group"><label class="control-label"><span style="color: red;">*</span>Tab No</label><label id="tabNo'+i+'" class="control-label" style="text-align:center" name="tabNo'+i+'"><b>'+i+'</b></label><a name="'+id+'" id="removeId'+id+'" onclick="packageData.deleteData.deleteTab(this.name);"><label class="control-label" style=" cursor: pointer;color:#333; float:left;margin-right: -21px;text-align: left"><i class="icon-user" style="background-position: -457px 0;"></i>&nbsp;Remove Tab</label></a><a id="addmorediv'+id+'" onclick="tabs.addMore();" style="float:right;"><label class="control-label" style=" cursor: pointer;color:#333;text-align: left"><i class="icon-user" style="background-position: 0px -96px;"></i>&nbsp;Add More</label></a></div>\n\
                                        <div class="control-group"><label class="control-label"><span style="color: red;">*</span>Tab Name</label><div class="controls"><input maxlength="150" id="tabName'+id+'" class="span6" type="text" name="tabName'+id+'" value="'+tabName+'"><img id="tabNameError'+id+'"  src="view/layout_TestSeries/crossError.png" class="crossImg"/></div></div>\n\
                                        <div class="control-group"><label class="control-label"><span style="color: red;">*</span>Tab Order</label><div class="controls"><input id="tabOrder'+id+'" class="span6" type="text" name="tabOrder'+id+'"  value="'+tabOrder+'" ><img id="tabOrderError'+id+'"  src="view/layout_TestSeries/crossError.png" class="crossImg"/></div></div>\n\
                                        <div class="control-group"><label class="control-label" for="fileInput"><span style="color: red;">*</span>Upload File (.xls)</label><div class="controls" ><div id="uniform-excelField'+id+'" class="uploader" style="float:left"><input id="excelField'+id+'" class="input-file uniform_on" type="file" value="'+file+'" name="'+id+'"  onchange="packageData.excelUploadVali_More(this.name);"><span class="filename" id="filename'+id+'" style="-moz-user-select: none;" >'+file+'</span><span class="action" style="-moz-user-select: none;">Choose File</span></div><img id="tabFileError'+id+'"  src="view/layout_TestSeries/crossError.png" class="crossImg"/><input id="excelFEdit'+id+'" class="input-file uniform_on" type="hidden" name="excelFEdit'+id+'"></div><input id="upldproductexcel'+id+'" type="hidden" value=""><label class="control-label" style="cursor: pointer; color: rgb(51, 51, 51); text-align: left;  width: 9%; margin-left: 8%;" onclick="tabs.downloadExcel();"><i class="icon-user" style="background-position: -97px -24px;"></i>Sample</label></div>\n\
                                        <div class="control-group"><div class="controls"><a style="text-decoration:none;color:rgb(70,70,70);" id="label'+id+'" name="'+id+'" onclick="tabs.addEvent(this.name);"><label   class="uniform" style="width:35%;" ><div id="uniform-tabActive" class="checker"><span id="span'+id+'" class=""><input id="tabActive'+id+'" class="uniform_on" type="checkbox" value="1"></span></div>Want to Activate the Current Tab</label></a></div></div>\n\
                                        </div>';
                                        $('#allTabDivs').append(str);
                                            if( active == 'A'){
                                                $('#tabActive'+id).attr('class','checked');
                                                $('#tabActive'+id).attr('checked','checked');
                                                $('#span'+id).addClass('checked');
                                            }else{
                                                $('#tabActive'+id).attr('class','');
                                                $('#tabActive'+id).removeAttr('checked');
                                            }
                                          $('excelField'+id).val(file);
                                          if(i!=getPackageTabRec.total){
                                              $('#addmorediv'+id).hide();
                                            }
                                          this.fileUpload(id,file); 
                                          $('#tabOrder'+id).attr('oninput',"this.value = this.value.replace(/[^0-9]/g, '');  this.value = this.value.replace(/^(-?).*?(\d+\.\d{5}).*$/, '$1$2')");
                                        }
                                     $('#tabcount').val(getPackageTabRec.ID[getPackageTabRec.total]);
                                     $('#srNo').val(getPackageTabRec.total);
                                    
//                                     $('#preloaderCenter').hide();
                                  }else{
                                      $('#preloaderCenter').hide();
                                  }
                                   
                    },
                    deleteData : {
                        deleteTab:function(count){
                             var result=confirm("Are you sure you want to delete this record ?");
                             if(!result){
                                 return false;
                             }else{
                                   
                                    var mode=$('#tabDiv'+count).attr('title');
                                    $('#tabDiv'+count).remove();
                                    var totalTabs=parseInt($('#srNo').val());
                                    $('#srNo').val(totalTabs-1);
                                    if(mode=="uo5"){
                                         $('#preloaderCenter').show();
                                        var tab = {};
                                        tab['mode'] = "deleteTab";
                                        tab['data'] = {};
                                        tab['data']['id'] = count;
                                        var tabDataRec = get(tab,'index.php?p=13','json');
                                        if(tabDataRec.total>0){
                                            
                                            
                                        var url = "index.php?p=14&m=excelsingleDelete&delId=" + count;
                                            $.ajax({
                                            type: "POST",
                                            url: url,
                                            data: "",
                                            async: false,
                                            processData: false,
                                            contentType: false,
                                            error: function (res) {
                                                alert("Unsuccessful.");
                                                return false;
                                            },
                                            success: function (res) {
                                               $('#preloaderCenter').hide();
                                            }
                                        });
                                           alert("Record Deleted successfully.");
                                            $('#preloaderCenter').hide();
                                           
                                         }else{
                                           alert("No record deleted.");
                                            $('#preloaderCenter').hide();
                                            }
                                        }
                                        if($('#srNo').val()>0){
                                            $('#addMoreDiv').hide();
                                            $('#addMoreDivs').show();
                                        }else{
                                            $('#addMoreDivs').hide();
                                            $('#addMoreDiv').show();
                                        }
                                    for(var i=1;i<totalTabs;i++){
                                        
                                            var iPlusOne=parseInt(i)+1;
                                            try{
                                            if($('#tabNo'+i).html()=="<b>"+i+"</b>"){
                                            }else{
                                                $('#tabNo'+iPlusOne).html("<b>"+i+"</b>"); 
                                                $('#tabNo'+iPlusOne).attr("id","tabNo"+i); 
                                                }
                                            }catch(error){
                                                $('#tabNo'+iPlusOne).html("<b>"+i+"</b>"); 
                                                $('#tabNo'+iPlusOne).attr("id","tabNo"+i); 
                                            }   
                                        }
                                        var divIds=new Array();
                                var p=1;
                                    $('.damsCms').each(function (index, value) { 
                                    divIds[p]=$(this).attr('name');
                                    p++;
                                    });
                                    var totalDivs=$('#srNo').val();
                                    for(var m=1;m<=totalDivs;m++){
                                         if(m==totalDivs){
                                            $('#addmorediv'+divIds[m]).fadeIn(50); 
                                        }else if(m!=totalDivs){
                                            $('#addmorediv'+divIds[m]).hide();
                                        }
                                    }
                             
                         }
                },
                deletePackage:function(count){
                             var result=confirm("Are you sure you want to delete this Package ?");
                             if(!result){
                                 return false;
                             }else{
                                 var totalTabs=parseInt($('#pacakgeshowCount').val());
                                    $('#preloaderCenter').show();
                                    var tab = {};
                                    tab['mode'] = "deletePackage";
                                    tab['data'] = {};
                                    tab['data']['id'] = count;
                                    var tabDataRec = get(tab,'index.php?p=13','json');
                                    if(tabDataRec.total>0){
                                        var url = "index.php?p=14&m=excelmultipleDelete&delId=" + tabDataRec.delTabIds;
                                            $.ajax({
                                            type: "POST",
                                            url: url,
                                            data: "",
                                            async: false,
                                            processData: false,
                                            contentType: false,
                                            error: function (res) {
                                                alert("Unsuccessful.");
                                                return false;
                                            },
                                            success: function (res) {
                                               $('#preloaderCenter').hide();
                                            }
                                        });
                                        alert("Record Deleted successfully.");
                                    $('#packageShow'+count).remove();
                                    var totalTabs=parseInt($('#pacakgeshowCount').val());
                                    $('#pacakgeshowCount').val(totalTabs-1);
                                    for(var i=1;i<totalTabs;i++){
                                        var iPlusOne=parseInt(i)+1;
                                        try{
                                            if($('#packagTd'+i).html()==i){
                                        }else{
                                            $('#packagTd'+iPlusOne).html(i); 
                                            $('#packagTd'+iPlusOne).attr("id","packagTd"+i); 
                                            }
                                        }catch(error){
                                                $('#packagTd'+iPlusOne).html(i);
                                                $('#packagTd'+iPlusOne).attr("id","packagTd"+i);
                                            }   
                                        }
                                        
                                    }else{
                                        alert("No record deleted.");
                                        $('#preloaderCenter').hide();
                                        }
                                    }
                                    
                             
                         }
                         
                
            },
                 
                getCourse:function(id){
                    var course = {};
                            course['mode'] = "getCourseSelect";
                            course['data'] = {}; 
                            course['id'] = id;
                            var courseDataRec = get(course,'index.php?p=13','json');
                            var option='';
                            var selected='';
                            if(id!='' && id!=null){
                                selected="selected";
                            }else{
                                selected="";
                            }
                            if(courseDataRec.total){
                                for(var i=0;i<courseDataRec.total;i++){
                                    var course_id=courseDataRec.id[i];
                                 option+="<option "+selected+ " value="+course_id+">"+unescape(decodeURIComponent(courseDataRec.name[i]))+"</option>";    
                                }
                            }
                            $('#packageCourse').append(option);   
                    
                },
                excelUploadVali_More:function(id){
                    
                    var fileName=$('#excelField'+id).val();
                    $('#filename'+id).html(fileName);
                        var ext=fileName.substr(fileName.indexOf(".")+1);
                        if(ext!=='xls'){
                        alert("Please Upload the .xls file onley.");
                        $('#filename'+id).html("No file selected");
                        $('#preloaderCenter').hide();
                        return false;
                        }else{
                            $('#filename'+id).html(fileName);
                        }
                    
                    
                },
                fileUpload:function(id,file){
                        setTimeout(function() {
                        $("#uniform-excelField"+id+" span.filename").html(file);
                        $('#preloaderCenter').hide();
                        }, 2000);
                    },
                cancel:function(id,file){
                    location.href='index.php?p=12&ln=6&t=7';      
                    }
                }
                
    //Start: FAQ added by Azizur................................................
    var FAQData = function(){
            this.getDataAll = function(start,end){
                $('#preloaderCenter').show();
                var FAQ = {};
                FAQ['mode'] = "getAllFAQ";
                FAQ['package_start'] =start;
                FAQ['package_end']   = end;
                var FAQDataRec = get(FAQ,'index.php?p=13','json');
                $('#faq_StartPoint').val(parseInt(start)+parseInt(FAQDataRec.total));
                if(FAQDataRec.total>0){
                    $('#seeMoreBtn').show();
                    var str = "";
                    $('#FAQData').html('');
                    for(var i=0;i<FAQDataRec.total;i++){ 
                        var str = "";
                        str = str + '<tr>';
                        str = str + '<td>' + (i+1) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(FAQDataRec.question[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(FAQDataRec.answer[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(FAQDataRec.order[i])) + '</td>';

                        if(FAQDataRec.active[i] == 'A'){
                            str = str + '<td>Active</td>';
                        }else{
                            str = str + '<td>Inactive</td>';
                        }

                        str = str + '<td style="text-align: center;cursor:pointer;">' +'<a href="index.php?p=12&ln=5&t=7&e=2&id='+FAQDataRec.id[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callFAQData.deleteFAQ('+FAQDataRec.id[i]+');"></i>' + '</td>';
                        str = str + '</tr>';
                        $('#FAQData').append(str);
                     }
                     $('#preloaderCenter').hide();
                        if(FAQDataRec.total<end || FAQDataRec.total<=14){
                            $('#seeMoreBtn').hide();
                        }
                }else{
                     $('#FAQData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                     $('#preloaderCenter').hide();
                }
            }
            this.getDataAllPagination = function(start,end){
                var FAQ = {};
                FAQ['mode'] = "getAllFAQ";
                FAQ['package_start'] =start;
                FAQ['package_end']   = end;
                var FAQDataRec = get(FAQ,'index.php?p=13','json');
                $('#faq_StartPoint').val(parseInt(start)+parseInt(FAQDataRec.total));
                if(FAQDataRec.total>0){
                    var str = "";
                    var startCount=parseInt(start)+1;
                    for(var i=0;i<FAQDataRec.total;i++){ 
                        var str = "";
                        str = str + '<tr>';
                        str = str + '<td>' + (i+startCount) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(FAQDataRec.question[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(FAQDataRec.answer[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(FAQDataRec.order[i])) + '</td>';

                        if(FAQDataRec.active[i] == 'A'){
                            str = str + '<td>Active</td>';
                        }else{
                            str = str + '<td>Inactive</td>';
                        }

                        str = str + '<td style="text-align: center;cursor:pointer;">' +'<a href="index.php?p=12&ln=5&t=7&e=2&id='+FAQDataRec.id[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="callFAQData.deleteFAQ('+FAQDataRec.id[i]+');"></i>' + '</td>';
                        str = str + '</tr>';
                        $('#FAQData').append(str);
                     }
                    $('#preloader').hide();
                    if(FAQDataRec.total<end || FAQDataRec.total<=14){
                            $('#seeMoreBtn').hide();
                        }
                }else{
                      $('#FAQData').append("<tr><td colspan=8><br><br>No More Data.</td></tr>");
                      $('#seeMoreBtn').hide();
                      $('#preloader').hide();
                }
            }


            this.saveData = function(){  
                var FAQ = {};
                var FAQDataRec;
                if($('#modeFAQ').val() == 1){
                    FAQ['mode'] = "saveFAQ";
                    FAQ['data'] = {};
                    FAQ['data']['FAQQuestion']  = escape(encodeURIComponent(($.trim($('#FAQField1 textarea').val()))));
                    FAQ['data']['FAQAnswer']    = escape(encodeURIComponent(($.trim($('#FAQField2 textarea').val()))));
                    FAQ['data']['FAQOrder']     = $.trim($('#FAQField3').val());  //for order
                    FAQ['data']['active']       = ($('#uniform-FAQField4 span').attr('class')=='checked') ? "A" : "I";  //for settings
                   
                    if (FAQ['data']['FAQQuestion']==''){
                        $("#FAQField1 textarea").focus();
                        $('#FAQField1 textarea').css('border-color', 'red');
                        $('#FAQQuestionError').show(300);
                        $('#FAQField1 textarea').keypress(function(){
                            $('#FAQField1 textarea').css('border-color', ''); 
                            $('#FAQQuestionError').hide(300);
                        });
                        return false;
                    }else if(FAQ['data']['FAQAnswer']==''){
                            $("#FAQField2 textarea").focus();
                            $('#FAQField2 textarea').css('border-color', 'red');
                            $('#FAQAnswerError').show(300);
                            $('#FAQField2 textarea').keypress(function(){
                                $('#FAQField2 textarea').css('border-color', '');   
                                $('#FAQAnswerError').hide(300);
                            });
                            return false;
                    }else if(FAQ['data']['FAQOrder']==''){
                            $("#FAQField3").focus();
                            $('#FAQField3').css('border-color', 'red');
                            $('#FAQOrderError').show(300);
                            $('#FAQField3').keypress(function(){
                                $('#FAQField3').css('border-color', '');   
                                $('#FAQOrderError').hide(300);
                            });
                            return false;
                    }else{
                        $('#preloaderCenter').show();
                        FAQDataRec = get(FAQ,'index.php?p=13', 'json'); 
                     }
                     
                    if( parseInt(FAQDataRec.total) > 0){
                        $('#preloaderCenter').hide();
                        alert("Record inserted successfully.");
                        $('textarea').val("");
                        $('#FAQField3').val("");

                        var hidden = $('#FAQHiddenSave').val();
                       
                        if(hidden == 'save'){
                            $('#cancelFAQData').click();
                            location.href='index.php?p=12&ln=5&t=7';
                        }
                        if(hidden == 'save&new'){
                            document.getElementById("faqForm").reset();
                            $('#preloaderCenter').hide();
                         }         

                    }else{
                        $('#preloaderCenter').hide();
                        alert("No record inserted.");
                    }
                }

                if($('#modeFAQ').val() == 2){ 
                
                    FAQ['mode'] = "updateFAQ";
                    FAQ['data'] = {};
                    FAQ['data']['FAQQuestion']  = escape(encodeURIComponent(($.trim($('#FAQField1 textarea').val()))));
                    FAQ['data']['FAQAnswer']    = escape(encodeURIComponent(($.trim($('#FAQField2 textarea').val()))));
                    FAQ['data']['FAQOrder']     = $.trim($('#FAQField3').val());  //for order
                    FAQ['data']['active']       = ($('#uniform-FAQField4 span').attr('class')=='checked') ? "A" : "I";  //for settings
                    FAQ['data']['id'] = $('#idFAQ').val();
                    if (FAQ['data']['FAQQuestion']==''){
                        $("#FAQField1 textarea").focus();
                        $('#FAQField1 textarea').css('border-color', 'red');
                        $('#FAQQuestionError').show(300);
                        $('#FAQField1 textarea').keypress(function(){
                            $('#FAQField1 textarea').css('border-color', ''); 
                            $('#FAQQuestionError').hide(300);
                        });
                        return false;
                    }else if(FAQ['data']['FAQAnswer']==''){
                            $("#FAQField2 textarea").focus();
                            $('#FAQField2 textarea').css('border-color', 'red');
                            $('#FAQAnswerError').show(300);
                            $('#FAQField2 textarea').keypress(function(){
                                $('#FAQField2 textarea').css('border-color', '');   
                                $('#FAQAnswerError').hide(300);
                            });
                            return false;
                    }else if(FAQ['data']['FAQOrder']==''){
                            $("#FAQField3").focus();
                            $('#FAQField3').css('border-color', 'red');
                            $('#FAQOrderError').show(300);
                            $('#FAQField3').keypress(function(){
                                $('#FAQField3').css('border-color', '');   
                                $('#FAQOrderError').hide(300);
                            });
                            return false;
                    }else{
                        $('#preloaderCenter').show();
                        FAQDataRec = get(FAQ,'index.php?p=13', 'json');                 
                     }
                    if(parseInt(FAQDataRec.total) > 0){
                        alert(" Record Updated successfully.");
                        location.href='index.php?p=12&ln=5&t=7';
                    }else{
                        $('#preloaderCenter').hide();
                        alert("No record inserted.");
                    }
              }
         }
         
         $('#cancelFAQData').click(function(){
            $('#modeFAQ').val(1);
            $('#idFAQ').val("");
            $('#uniform-FAQField4 span.filename').html("");
            $('#editFAQ').html('Add');

            clearAll();
        });

        this.editData = function(id){
            $('#preloaderCenter').show();
            $('#editFAQ').html('Edit');
            $('#FAQSubmit2').hide();
            $('#modeFAQ').val(2);
            $('#idFAQ').val(id);
            var FAQDataRec = {} ;
            var FAQ = {} ;
            FAQ['mode'] = "getAllFAQ";
            FAQ['data'] = {};
            FAQ['data']['id'] = id ;
            FAQ['package_start'] =0;
            FAQ['package_end']   = 1;
            FAQDataRec = get(FAQ,'index.php?p=13','json');
            $('#FAQField1 textarea').val(unescape(decodeURIComponent(FAQDataRec.question[0])));
            $('#FAQField2 textarea').val(unescape(decodeURIComponent(FAQDataRec.answer[0])));
            $('#FAQField3').val(FAQDataRec.order[0]);
            if(FAQDataRec.active[0] == 'A'){
                $('#uniform_on-FAQField4 span').attr('class','checked');
                $('#FAQField4').attr('checked','checked');
            }else{
                $('#uniform_on-FAQField4 span').attr('class','');
                $('#FAQField4').removeAttr('checked');
            }
            $('#preloaderCenter').hide();

         }

        this.deleteFAQ = function(id){
            var result=confirm("Are you sure you want to delete this record ?");
            if(!result){
                return false;
            }else{
                $('#preloaderCenter').show();
                var FAQ = {};
                FAQ['mode'] = "deleteFAQ";
                FAQ['data'] = {};
                FAQ['data']['id'] = id;
                var FAQDataRec = get(FAQ,'index.php?p=13','json');
                if(FAQDataRec.total>0){
                    $('#preloaderCenter').hide();
                    alert(" Record Deleted successfully.");
                    var start=$('#faq_StartPoint').val();
                    this.getDataAll('0',parseInt(start));
                }else{
                        $('#preloaderCenter').hide();
                        alert("No Recods Deleted.");
                    }
                }
            }
             this.cancel=function(id){
                    location.href='index.php?p=12&ln=5&t=7';      
                    
                }
    }
    var imageData = {
            getDataAll : function(start,end){
                $('#preloaderCenter').show();
                var imageData = {};
                imageData['mode'] = "getAllImageData";
                imageData['image_start'] =start;
                imageData['image_end']   = end;
                var imageDataRec = get(imageData,'index.php?p=13','json');
                $('#image_StartPoint').val(parseInt(start)+parseInt(imageDataRec.total));
                if(imageDataRec.total>0){
                     $('#seeMoreBtn').show();
                    var str = "";
                    $('#imageData').html('');
                    for(var i=0;i<imageDataRec.total;i++){ 
                        var str = "";
                        str = str + '<tr>';
                        str = str + '<td>' + (i+1) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(imageDataRec.COURSE_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(imageDataRec.DISPLAY_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(imageDataRec.RANK[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(imageDataRec.IMAGE_ORDER[i])) + '</td>';

                        if(imageDataRec.ACTIVE[i] == 'A'){
                            str = str + '<td>Active</td>';
                        }else{
                            str = str + '<td>Inactive</td>';
                        }

                        str = str + '<td style="text-align: center;cursor:pointer;">' +'<a href="index.php?p=12&ln=3&t=7&e=2&id='+imageDataRec.id[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="imageData.deleteImage('+imageDataRec.id[i]+');"></i>' + '</td>';
                        str = str + '</tr>';
                        $('#imageData').append(str);
                     }
                     $('#preloaderCenter').hide();
                     if(imageDataRec.total<end || imageDataRec.total<=14){
                            $('#seeMoreBtn').hide();
                        }
                }else{
                    $('#preloaderCenter').hide();
                     $('#imageData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                }
            },
            getDataAllPagination : function(start,end){
                var imageData = {};
                imageData['mode'] = "getAllImageData";
                imageData['image_start'] =start;
                imageData['image_end']   = end;
                var imageDataRec = get(imageData,'index.php?p=13','json');
                $('#image_StartPoint').val(parseInt(start)+parseInt(imageDataRec.total));
                $('#image_total').val(imageDataRec.count);
                if(imageDataRec.total>0){
                    var str = "";
                    var startCount=parseInt(start)+1;
                    for(var i=0;i<imageDataRec.total;i++){ 
                        var str = "";
                        str = str + '<tr>';
                        str = str + '<td>' + (i+startCount) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(imageDataRec.COURSE_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(imageDataRec.DISPLAY_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(imageDataRec.RANK[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(imageDataRec.IMAGE_ORDER[i])) + '</td>';

                        if(imageDataRec.ACTIVE[i] == 'A'){
                            str = str + '<td>Active</td>';
                        }else{
                            str = str + '<td>Inactive</td>';
                        }

                        str = str + '<td style="text-align: center;cursor:pointer;">' +'<a href="index.php?p=12&ln=3&t=7&e=2&id='+imageDataRec.id[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="imageData.deleteImage('+imageDataRec.id[i]+');"></i>' + '</td>';
                        str = str + '</tr>';
                        $('#imageData').append(str);
                     }
                      $('#preloader').hide();
                       if(imageDataRec.total<end || imageDataRec.total<=14){
                            $('#seeMoreBtn').hide();
                        }
                }else{
                       $('#imageData').append("<tr><td colspan=8><br><br>No More Data.</td></tr>");
                       $('#seeMoreBtn').hide();
                       $('#preloader').hide();
                }
            },
            saveData : function(){
                var Images = {};
                var ImagesDataRec;
                if($('#modeimage').val() == 1){
                    
                    Images['mode'] = "saveImages";
                    Images['data'] = {};
                    Images['data']['imageCourse']   = escape(encodeURIComponent(($.trim($('#imageCourse').val()))));
                    Images['data']['imageName']     = escape(encodeURIComponent(($.trim($('#imageName').val()))));
                    Images['data']['imageupload']   = $.trim($('#imageupload').val());  //for order
                    Images['data']['imageRank']     = $.trim($('#imageRank').val());  //for order
                    Images['data']['imageOrder']    = $.trim($('#imageOrder').val());  //for order
                    Images['data']['active'] =  ($('#imageField4').is(':checked')) ? "A" : "I";
//                    alert("image Course ="+Images['data']['imageCourse']+"------name="+Images['data']['imageName']+'----imageupload='+Images['data']['imageupload']+"--------rank="+Images['data']['imageRank']+"------order"+Images['data']['imageOrder']+"------Active"+Images['data']['active']);
//                    return false;
                    if (Images['data']['imageCourse']=='' || Images['data']['imageCourse']=='null'){
                        $("#imageCourse").focus();
                        $('#imageCourse').css('border-color', 'red');
                        $('#imageCourseError').show(300);
                        $('#imageCourse').change(function(){
                        $('#imageCourse').css('border-color', ''); 
                        $('#imageCourseError').hide(300);
                        });
                        return false;
                    }else if(Images['data']['imageName']==''){
                            $("#imageName").focus();
                            $('#imageName').css('border-color', 'red');
                            $('#imageNameError').show(300);
                            $('#imageName').keypress(function(){
                            $('#imageName').css('border-color', '');   
                            $('#imageNameError').hide(300);
                            });
                            return false;
                    }else if(Images['data']['imageupload']==''){
                            $("#imageupload").focus();
                            $('#imageupload').css('border-color', 'red');
                            $('#imageImgError').show(300);
                            $('#imageupload').change(function(){
                            $('#imageupload').css('border-color', '');   
                            $('#imageImgError').hide(300);
                            });
                            return false;
                    }else if(Images['data']['imageRank']==''){
                            $("#imageRank").focus();
                            $('#imageRank').css('border-color', 'red');
                            $('#imageRankError').show(300);
                            $('#imageRank').keypress(function(){
                            $('#imageRank').css('border-color', '');   
                            $('#imageRankError').hide(300);
                            });
                            return false;
                    }else if(Images['data']['imageOrder']==''){
                            $("#imageOrder").focus();
                            $('#imageOrder').css('border-color', 'red');
                            $('#imageOrderError').show(300);
                            $('#imageOrder').keypress(function(){
                            $('#imageOrder').css('border-color', '');   
                            $('#imageOrderError').hide(300);
                            });
                            return false;
                    }else{
                        $('#preloaderCenter').show();
                        ImagesDataRec = get(Images,'index.php?p=13', 'json'); 
                     }
                     
                    if( parseInt(ImagesDataRec.total) > 0){
                            var lastInsertedId = ImagesDataRec.lastInsertedId;
                            var file_data = $('#imageupload').prop('files')[0];
                            var form_data = new FormData();
                            form_data.append('file', file_data);
                            var url = "index.php?p=14&m=ajaxUploadImage&lastInsertedId=" + lastInsertedId;
                               
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: form_data,
                                    async: false,
                                    processData: false,
                                    contentType: false,
                                    error: function (res) {
                                        alert("Unsuccessful.");
                                        return false;
                                    },
                                    success: function (res) {
                                       $('#preloaderCenter').hide();
                                    }
                                });
                        alert(" Record inserted successfully.");
                        $('#imageCourse').val("");
                        $('#imageName').val("");
                        $('#imageupload').val("");
                        $('.filename').html("No file selected");
                        $('#imageRank').val("");
                        $('#imageOrder').val("");
                        
                        var hidden = $('#imageHiddenSave').val();
                        if(hidden == 'save'){
                            $('#cancelFAQData').click();
                            location.href='index.php?p=12&ln=3&t=7';
                        }
                        if(hidden == 'save&new'){
                           document.getElementById("imageForm").reset();
                            $('#preloaderCenter').hide();  
                         }         

                    }else{
                        alert("No record inserted.");
                    }
                }

                if($('#modeimage').val() == 2){ 
                    Images['mode'] = "updateImageData";
                    Images['data'] = {};
                    Images['data']['imageCourse']   = escape(encodeURIComponent(($.trim($('#imageCourse').val()))));
                    Images['data']['imageName']     = escape(encodeURIComponent(($.trim($('#imageName').val()))));
                    Images['data']['imageupload']   = $.trim($('#imageEdit3').val());  //for order
                    Images['data']['imageRank']     = $.trim($('#imageRank').val());  //for order
                    Images['data']['imageOrder']    = $.trim($('#imageOrder').val());  //for order
                    Images['data']['active']        =  ($('#imageField4').is(':checked')) ? "A" : "I";
                    Images['data']['id'] = $('#idimage').val();
//                    alert("image Course ="+Images['data']['imageCourse']+"------name="+Images['data']['imageName']+'----imageupload='+Images['data']['imageupload']+"--------rank="+Images['data']['imageRank']+"------order"+Images['data']['imageOrder']+"------Active"+Images['data']['active']);
//                    return false;
                    if (Images['data']['imageCourse']=='' || Images['data']['imageCourse']=='null'){
                        $("#imageCourse").focus();
                        $('#imageCourse').css('border-color', 'red');
                        $('#imageCourseError').show(300);
                        $('#imageCourse').change(function(){
                        $('#imageCourse').css('border-color', ''); 
                        $('#imageCourseError').hide(300);
                        });
                        return false;
                    }else if(Images['data']['imageName']==''){
                            $("#imageName").focus();
                            $('#imageName').css('border-color', 'red');
                            $('#imageNameError').show(300);
                            $('#imageName').keypress(function(){
                            $('#imageName').css('border-color', '');   
                            $('#imageNameError').hide(300);
                            });
                            return false;
                    }else if(Images['data']['imageupload']==''){
                            $("#imageupload").focus();
                            $('#imageupload').css('border-color', 'red');
                            $('#imageImgError').show(300);
                            $('#imageupload').change(function(){
                            $('#imageupload').css('border-color', '');   
                            $('#imageImgError').hide(300);
                            });
                            return false;
                    }else if(Images['data']['imageRank']==''){
                            $("#imageRank").focus();
                            $('#imageRank').css('border-color', 'red');
                            $('#imageRankError').show(300);
                            $('#imageRank').keypress(function(){
                            $('#imageRank').css('border-color', '');   
                            $('#imageRankError').hide(300);
                            });
                            return false;
                    }else if(Images['data']['imageOrder']==''){
                            $("#imageOrder").focus();
                            $('#imageOrder').css('border-color', 'red');
                            $('#imageOrderError').show(300);
                            $('#imageOrder').keypress(function(){
                            $('#imageOrder').css('border-color', '');   
                            $('#imageOrderError').hide(300);
                            });
                            return false;
                    }else{
                        $('#preloaderCenter').show();
                        ImagesDataRec = get(Images,'index.php?p=13', 'json'); 
                        }
                        if( parseInt(ImagesDataRec.total) > 0){
                            var lastInsertedId = $('#idimage').val();;
                            var file_data = $('#imageupload').prop('files')[0];
                            var form_data = new FormData();
                            form_data.append('file', file_data);
                            var url = "index.php?p=14&m=ajaxUploadImage&lastInsertedId=" + lastInsertedId;
                               
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: form_data,
                                    async: false,
                                    processData: false,
                                    contentType: false,
                                    error: function (res) {
                                        alert("Unsuccessful.");
                                        return false;
                                    },
                                    success: function (res) {
                                        $('#preloaderCenter').hide();
                                    }
                                });
                        alert(" Record updated successfully.");
                        $('#imageCourse').val("");
                        $('#imageName').val("");
                        $('#imageupload').val("");
                        $('.filename').html("No file selected");
                        $('#imageRank').val("");
                        $('#imageOrder').val("");
                        location.href='index.php?p=12&ln=3&t=7';
                     }else{
                         $('#preloaderCenter').hide();
                       alert("No record Updated.");
                    }    
              }
         },
            editData : function(id){
            $('#preloaderCenter').show();    
            $('#editImage').html('Edit');
            $('#imageSubmit2').hide();
            $('#modeimage').val(2);
            $('#idimage').val(id);
            this.getCourse();
            var imageDataRec = {} ;
            var imageData = {} ;
            imageData['mode'] = "getAllEditimageData";
            imageData['data'] = {};
            imageData['data']['id'] = id ;
            imageDataRec = get(imageData,'index.php?p=13','json');
            $('#imageCourse').val(unescape(decodeURIComponent(imageDataRec.COURSE_ID)));
            $('#imageName').val(unescape(decodeURIComponent(imageDataRec.DISPLAY_NAME)));
            setTimeout(function() {
            $("#uniform-imageupload span.filename").html(unescape(decodeURIComponent(imageDataRec.FILE_NAME)));
            $('#imageEdit3').val(imageDataRec.FILE_NAME);
            $('#preloaderCenter').hide();
            }, 1000);
            $('#imageRank').val(unescape(decodeURIComponent(imageDataRec.RANK)));
            $('#imageRank').val(unescape(decodeURIComponent(imageDataRec.RANK)));
            $('#imageOrder').val(unescape(decodeURIComponent(imageDataRec.IMAGE_ORDER)));
            if(imageDataRec.ACTIVE == 'A'){
                $('#uniform-imageField4 span').attr('class','checked');
                $('#imageField4').attr('checked','checked');
            }else{
                $('#uniform-imageField4 span').attr('class','');
                $('#imageField4').removeAttr('checked');
            }
            

         },

            deleteImage : function(id){
            var result=confirm("Are you sure you want to delete this record ?");
            if(!result){
                return false;
            }else{
                $('#preloaderCenter').show();
                var ImageData = {};
                ImageData['mode'] = "deleteImageData";
                ImageData['data'] = {};
                ImageData['data']['id'] = id;
                var imageDataRec = get(ImageData,'index.php?p=13','json');
                if(imageDataRec.total>0){
                    var url = "index.php?p=14&m=ajaxUploadImageDelete&delId=" + imageDataRec.delId +"&extension=" + imageDataRec.extension;
                     $.ajax({
                               type: "POST",
                               url: url,
                               data: "",
                               async: false,
                               processData: false,
                               contentType: false,
                               error: function (res) {
                                   alert("Unsuccessful.");
                                   return false;
                               },
                               success: function (res) {
//                                                alert(res);
                               }
                           });
                   alert("Record Deleted successfully.");
                   $('#preloaderCenter').hide();
                   var start=$('#image_StartPoint').val();
                    this.getDataAll('0',parseInt(start));
                }else{
                    $('#preloaderCenter').hide();
                   alert("No record deleted.");
                }
                }
            },
            imageValidation:function(input,id){
            if (input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    var value=$("#"+id).val();
                    var ext =value.split('.').pop().toLowerCase();
                    if($.inArray(ext, ['jpg','jpeg','png']) == -1) {
                        alert( value+ ' is invalid File!');
                        $("#"+id).val("");
                        $(".filename").html("No file selected");
                        $('#preloaderCenter').hide();
                    return false;
                    }
                    if(input.files[0].size>1048576){
                         alert("The image must be less than 500 kb.");
                         $('#preloaderCenter').hide();
                    return false;
                    }
                    if (typeof (input.files) != "undefined") {
                        var reader = new FileReader();
                            reader.readAsDataURL(input.files[0]);
                            reader.onload = function (e) {
                            var image = new Image();
                            image.src = e.target.result;
                            image.onload = function () {
                            var height = this.height;
                            var width = this.width;
                                if (height > 160 || width > 130) {
                                    alert("image resolution should be less then 130"+'\xD7'+"160 px");
                                    $('#preloaderCenter').hide();
                                    $("#"+id).val("");
                                    $(".filename").html("No file selected");
                                    $('#preloaderCenter').hide();
                                return false;
                                }else if(height < 150 || width < 120){
                                    alert("image resolution should be greater then 120"+'\xD7'+"150 px");
                                    $('#preloaderCenter').hide();
                                    $("#"+id).val("");
                                    $(".filename").html("No file selected");
                                return false;
                                }
                            };
                        }
                    }    
                };
                reader.readAsDataURL(input.files[0]);
            }
        },
        getCourse:function(id){
                    var course = {};
                            course['mode'] = "getCourseSelect";
                            course['data'] = {}; 
                            course['id'] = id;
                            var courseDataRec = get(course,'index.php?p=13','json');
                            var option='';
                            var selected='';
                            if(id!='' && id!=null){
                                selected="selected";
                            }else{
                                selected="";
                            }
                            if(courseDataRec.total){
                                for(var i=0;i<courseDataRec.total;i++){
                                    var course_id=courseDataRec.id[i];
                                 option+="<option "+selected+ " value="+course_id+">"+unescape(decodeURIComponent(courseDataRec.name[i]))+"</option>";    
                                }
                            }
                            $('#imageCourse').append(option);  
                            $('#preloaderCenter').hide();
                    
                },        
                cancel:function(id){
                  location.href='index.php?p=12&ln=3&t=7';      
                    
                }        
    }
    var videoData = {
            getDataAll : function(start,end){
                $('#preloaderCenter').show();
                var videoData = {};
                videoData['mode'] = "getAllVideoData";
                videoData['video_start'] =start;
                videoData['video_end']   = end;
                var videoDataRec = get(videoData,'index.php?p=13','json');
                $('#video_StartPoint').val(parseInt(start)+parseInt(videoDataRec.total));
                if(videoDataRec.total>0){
                     $('#seeMoreBtn').show();
                    var str = "";
                    $('#videoData').html('');
                    for(var i=0;i<videoDataRec.total;i++){ 
                        var str = "";
                        str = str + '<tr>';
                        str = str + '<td>' + (i+1) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.COURSE_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.VIDEO_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.RANK[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.VIDEO_ORDER[i])) + '</td>';

                        if(videoDataRec.ACTIVE[i] == 'A'){
                            str = str + '<td>Active</td>';
                        }else{
                            str = str + '<td>Inactive</td>';
                        }

                        str = str + '<td style="text-align: center;cursor:pointer;">' +'<a href="index.php?p=12&ln=7&t=7&e=2&id='+videoDataRec.id[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="videoData.deleteVideo('+videoDataRec.id[i]+');"></i>' + '</td>';
                        str = str + '</tr>';
                        $('#videoData').append(str);
                     }
                     $('#preloaderCenter').hide();
                     $('#preloader').hide();
                       if(videoDataRec.total<end || videoDataRec.total<=14){
                            $('#seeMoreBtn').hide();
                        }
                }else{
                     $('#videoData').html("<tr><td colspan=8>No Content Found.</td></tr>");
                     $('#preloaderCenter').hide();
                }
            },
            getDataAllPagination : function(start,end){
                var videoData = {};
                videoData['mode'] = "getAllVideoData";
                videoData['video_start'] =start;
                videoData['video_end']   = end;
                var videoDataRec = get(videoData,'index.php?p=13','json');
                $('#video_StartPoint').val(parseInt(start)+parseInt(videoDataRec.total));
                if(videoDataRec.total>0){
                    var str = "";
                    var startCount=parseInt(start)+1;
                    for(var i=0;i<videoDataRec.total;i++){ 
                        var str = "";
                        str = str + '<tr>';
                        str = str + '<td>' + (i+startCount) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.COURSE_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.VIDEO_NAME[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.RANK[i])) + '</td>';
                        str = str + '<td>' + unescape(decodeURIComponent(videoDataRec.VIDEO_ORDER[i])) + '</td>';

                        if(videoDataRec.ACTIVE[i] == 'A'){
                            str = str + '<td>Active</td>';
                        }else{
                            str = str + '<td>Inactive</td>';
                        }

                        str = str + '<td style="text-align: center;cursor:pointer;">' +'<a href="index.php?p=12&ln=7&t=7&e=2&id='+videoDataRec.id[i]+'">'+'<i class="icon-edit"></i></a>' + '</td>';
                        str = str + '<td style="text-align: center;cursor:pointer;">' + '<i class="icon-remove" onclick="videoData.deleteVideo('+videoDataRec.id[i]+');"></i>' + '</td>';
                        str = str + '</tr>';
                        $('#videoData').append(str);
                     }
                     $('#preloader').hide();
                     if(videoDataRec.total<end || videoDataRec.total<=14){
                            $('#seeMoreBtn').hide();
                        }
                }else{
                      $('#videoData').append("<tr><td colspan=8><br><br>No More Data.</td></tr>");
                      $('#seeMoreBtn').hide();
                      $('#preloader').hide();
                }
            },
            saveData : function(){
                var video = {};
                var videoDataRec;
                if($('#modeVideo').val() == 1){
                    
                    video['mode'] = "saveVideo";
                    video['data'] = {};
                    video['data']['videoCourse']    = escape(encodeURIComponent(($.trim($('#videoCourse').val()))));
                    video['data']['videoName']      = escape(encodeURIComponent(($.trim($('#videoName').val()))));
                    video['data']['videoUrl']       = escape(encodeURIComponent(($.trim($('#videoUrl').val()))));
                    video['data']['videoRank']      = $.trim($('#videoRank').val());  //for order
                    video['data']['videoOrder']     = $.trim($('#videoOrder').val());  //for order
                    video['data']['active']         = ($('#videoField4').is(':checked')) ? "A" : "I";
//                    alert("image Course ="+video['data']['videoCourse']+"------name="+video['data']['videoName']+'----imageupload='+video['data']['videoUrl']+"--------rank="+video['data']['videoRank']+"------order="+video['data']['videoOrder']+"------Active="+video['data']['active']);
//                    return false;
                    if (video['data']['videoCourse']=='' || video['data']['videoCourse']=='null'){
                            $("#videoCourse").focus();
                            $('#videoCourse').css('border-color', 'red');
                            $('#videoCourseError').show(300);
                            $('#videoCourse').change(function(){
                            $('#videoCourse').css('border-color', ''); 
                            $('#videoCourseError').hide(300);
                            });
                        return false;
                    }else if(video['data']['videoName']==''){
                            $("#videoName").focus();
                            $('#videoName').css('border-color', 'red');
                            $('#videoNameError').show(300);
                            $('#videoName').keypress(function(){
                            $('#videoName').css('border-color', '');   
                            $('#videoNameError').hide(300);
                            });
                            return false;
                    }else if(video['data']['videoUrl']==''){
                            $("#videoUrl").focus();
                            $('#videoUrl').css('border-color', 'red');
                            $('#videoUrlError').show(300);
                            $('#videoUrl').change(function(){
                            $('#videoUrl').css('border-color', '');   
                            $('#videoUrlError').hide(300);
                            });
                            return false;
                    }else if(video['data']['videoRank']==''){
                            $("#videoRank").focus();
                            $('#videoRank').css('border-color', 'red');
                            $('#videoRankError').show(300);
                            $('#videoRank').keypress(function(){
                            $('#videoRank').css('border-color', '');   
                            $('#videoRankError').hide(300);
                            });
                            return false;
                    }else if(video['data']['videoOrder']==''){
                            $("#videoOrder").focus();
                            $('#videoOrder').css('border-color', 'red');
                            $('#videoOrderError').show(300);
                            $('#videoOrder').keypress(function(){
                            $('#videoOrder').css('border-color', '');   
                            $('#videoOrderError').hide(300);
                            });
                            return false;
                    }else{
                        $('#preloaderCenter').show();
                        videoDataRec = get(video,'index.php?p=13', 'json'); 
                     }
                     
                    if( parseInt(videoDataRec.total) > 0){
                        alert(" Record inserted successfully.");
                        var hidden = $('#videoHiddenSave').val();
                        if(hidden == 'save'){
                            location.href='index.php?p=12&ln=7&t=7';
                        }else if(hidden == 'save&new'){
                            $('#videoCourse').val("");
                            $('#videoName').val("");
                            $('#videoUrl').val("");
                            $('#videoRank').val("");
                            $('#videoOrder').val("");
                            $('#preloaderCenter').hide();
                        }
                     }else{
                        alert("No record inserted.");
                        $('#preloaderCenter').hide();
                    }
                }

                if($('#modeVideo').val() == 2){ 
                    
                    video['mode'] = "updateVideoData";
                    video['data'] = {};
                    video['data']['videoCourse'] = escape(encodeURIComponent(($.trim($('#videoCourse').val()))));
                    video['data']['videoName'] = escape(encodeURIComponent(($.trim($('#videoName').val()))));
                    video['data']['videoUrl'] = escape(encodeURIComponent(($.trim($('#videoUrl').val()))));
                    video['data']['videoRank'] = $.trim($('#videoRank').val());  //for order
                    video['data']['videoOrder'] = $.trim($('#videoOrder').val());  //for order
                    video['data']['active'] =  ($('#videoField4').is(':checked')) ? "A" : "I";
                    video['data']['id'] = $('#idVideo').val();
//                    alert("image Course ="+Images['data']['imageCourse']+"------name="+Images['data']['imageName']+'----imageupload='+Images['data']['imageupload']+"--------rank="+Images['data']['imageRank']+"------order"+Images['data']['imageOrder']+"------Active"+Images['data']['active']);
//                    return false;
                    if (video['data']['videoCourse']=='' || video['data']['videoCourse']=='null'){
                        $("#videoCourse").focus();
                        $('#videoCourse').css('border-color', 'red');
                        $('#videoCourseError').show(300);
                        $('#videoCourse').change(function(){
                        $('#videoCourse').css('border-color', ''); 
                        $('#videoCourseError').hide(300);
                        });
                        return false;
                    }else if(video['data']['videoName']==''){
                            $("#videoName").focus();
                            $('#videoName').css('border-color', 'red');
                            $('#videoNameError').show(300);
                            $('#videoName').keypress(function(){
                            $('#videoName').css('border-color', '');   
                            $('#videoNameError').hide(300);
                            });
                            return false;
                    }else if(video['data']['videoUrl']==''){
                            $("#videoUrl").focus();
                            $('#videoUrl').css('border-color', 'red');
                            $('#videoUrlError').show(300);
                            $('#videoUrl').change(function(){
                            $('#videoUrl').css('border-color', '');   
                            $('#videoUrlError').hide(300);
                            });
                            return false;
                    }else if(video['data']['videoRank']==''){
                            $("#videoRank").focus();
                            $('#videoRank').css('border-color', 'red');
                            $('#videoRankError').show(300);
                            $('#videoRank').keypress(function(){
                            $('#videoRank').css('border-color', '');   
                            $('#videoRankError').hide(300);
                            });
                            return false;
                    }else if(video['data']['videoOrder']==''){
                            $("#videoOrder").focus();
                            $('#videoOrder').css('border-color', 'red');
                            $('#videoOrderError').show(300);
                            $('#videoOrder').keypress(function(){
                            $('#videoOrder').css('border-color', '');   
                            $('#videoOrderError').hide(300);
                            });
                            return false;
                    }else{
                        $('#preloaderCenter').show();
                        videoDataRec = get(video,'index.php?p=13', 'json'); 
                        }
                        if( parseInt(videoDataRec.total) > 0){
                        alert(" Record updated successfully.");
                        location.href='index.php?p=12&ln=7&t=7';
                     }else{
                         $('#preloaderCenter').hide();
                       alert("No record Updated.");
                    }    
              }
         },
            editData : function(id){
            
            $('#preloaderCenter').show();
            $('#editVideo').html('Edit');
            $('#videoSubmit2').hide();
            $('#modeVideo').val(2);
            $('#idVideo').val(id);
            this.getCourse();
            var videoDataRec = {} ;
            var videoData = {} ;
            videoData['mode'] = "getAllEditVideoData";
            videoData['data'] = {};
            videoData['data']['id'] = id ;
            videoDataRec = get(videoData,'index.php?p=13','json');
            $('#videoCourse').  val(unescape(decodeURIComponent(videoDataRec.COURSE_ID)));
            $('#videoName').    val(unescape(decodeURIComponent(videoDataRec.VIDEO_NAME)));
            $('#videoUrl').     val(unescape(decodeURIComponent(videoDataRec.URL)));
            $('#videoRank').    val(unescape(decodeURIComponent(videoDataRec.RANK)));
            $('#videoOrder').   val(unescape(decodeURIComponent(videoDataRec.VIDEO_ORDER)));
            if(videoDataRec.ACTIVE == 'A'){
                $('#uniform-videoField4 span').attr('class','checked');
                $('#videoField4').attr('checked','checked');
            }else{
                $('#uniform-videoField4 span').attr('class','');
                $('#videoField4').removeAttr('checked');
            }

         },

            deleteVideo : function(id){
            var result=confirm("Are you sure you want to delete this record ?");
            if(!result){
                return false;
            }else{
                $('#preloaderCenter').show();
                var vedioData = {};
                vedioData['mode'] = "deleteVideoData";
                vedioData['data'] = {};
                vedioData['data']['id'] = id;
                var imageDataRec = get(vedioData,'index.php?p=13','json');
                if(imageDataRec.total>0){
                   alert("Record Deleted successfully.");
                   var start=$('#video_StartPoint').val();
                   this.getDataAll('0',parseInt(start));
                }else{
                    $('#preloaderCenter').hide();
                   alert("No record deleted.");
                }
                }
            },
            getCourse:function(id){
                    var course = {};
                            course['mode'] = "getCourseSelect";
                            course['data'] = {}; 
                            course['id'] = id;
                            var courseDataRec = get(course,'index.php?p=13','json');
                            var option='';
                            var selected='';
                            if(id!='' && id!=null){
                                selected="selected";
                            }else{
                                selected="";
                            }
                            if(courseDataRec.total){
                                for(var i=0;i<courseDataRec.total;i++){
                                    var course_id=courseDataRec.id[i];
                                 option+="<option "+selected+ " value="+course_id+">"+unescape(decodeURIComponent(courseDataRec.name[i]))+"</option>";    
                                }
                            }
                            $('#videoCourse').append(option);   
                            $('#preloaderCenter').hide();
                    
                },        
                cancel:function(id){
                  location.href='index.php?p=12&ln=7&t=7';      
                    
                }        
    }
    
                var tabs={
                    
                addMore:function(){
                var count=parseInt($('#tabcount').val())+1;
                var srNo=parseInt($('#srNo').val())+1;
                $('#tabcount').val(count);
                $('#srNo').val(srNo);
                if(srNo>0){
                    $('#addMoreDiv').hide();
                    $('#addMoreDivs').show();
                }else{
                    $('#addMoreDivs').hide();
                    $('#addMoreDiv').show();
                }
            var div='<div style="display:none;" id="tabDiv'+count+'" name="'+count+'" title="u05" class="damsCms">\n\
            <input type="hidden" id="hiddenTabId'+count+'" value="0">\n\
            <div class="control-group"><label class="control-label">Tab No</label><label id="tabNo'+srNo+'" class="control-label" style="text-align:center" ><b>'+srNo+'</b></label><a name="'+count+'" id="removeId'+count+'" onclick="packageData.deleteData.deleteTab(this.name);"><label class="control-label" style=" cursor: pointer;color:#333; float:left;margin-right: -21px;text-align: left"><i class="icon-user" style="background-position: -457px 0;"></i>&nbsp;Remove Tab</label></a><a id="addmorediv'+count+'" onclick="tabs.addMore();"style="float:right;"><label class="control-label" style=" cursor: pointer;color:#333; text-align: left"><i class="icon-user" style="background-position: 0px -96px;"></i>&nbsp;Add More</label></a></div>\n\
            <div class="control-group"><label class="control-label">Tab Name</label><div class="controls"><input maxlength="150" placeholder="Add Tab Name" id="tabName'+count+'" class="span6" type="text" name="tabName'+count+'"><img id="tabNameError'+count+'"  src="view/layout_TestSeries/crossError.png" class="crossImg"/></div></div>\n\
            <div class="control-group"><label class="control-label">Tab Order</label><div class="controls"><input placeholder="Add Tab Order" id="tabOrder'+count+'"  class="span6" type="text" name="tabOrder'+count+'" data-provide="typeahead" data-items="4" ><img id="tabOrderError'+count+'"  src="view/layout_TestSeries/crossError.png" class="crossImg"/></div></div>\n\
            <div class="control-group"><label class="control-label" for="fileInput">Upload File (.xls)</label><div class="controls" ><div id="uniform-excelField1" class="uploader" style="float:left"><input id="excelField'+count+'" class="input-file uniform_on" type="file" name="'+count+'" onchange="packageData.excelUploadVali_More(this.name);"><span class="filename" id="filename'+count+'" style="-moz-user-select: none;" >No file selected</span><span class="action" style="-moz-user-select: none;">Choose File</span></div><img id="tabFileError'+count+'"  src="view/layout_TestSeries/crossError.png" class="crossImg"/><input id="excelFEdit'+count+'" class="input-file uniform_on" type="hidden" value="" name="excelFEdit'+count+'"><label class="control-label" style="cursor: pointer; color: rgb(51, 51, 51); text-align: left;  width: 11%; margin-left: 9%;" onclick="tabs.downloadExcel();"><i class="icon-user" style="background-position: -97px -24px;"></i> &nbsp;Sample</label></div><input id="upldproductexcel'+count+'" type="hidden" value=""></div>\n\
            <div class="control-group"><div class="controls"><a style="text-decoration:none;color:rgb(70,70,70);" id="label'+count+'" name="'+count+'" onclick="tabs.addEvent(this.name);"><label   class="uniform" style="width:35%;" ><div id="uniform-tabActive" class="checker"><span id="span'+count+'" class=""><input id="tabActive'+count+'" class="uniform_on" type="checkbox" value="1"></span></div>Want to Activate the Current Tab</label></a></div></div>\n\
         </div>';
            $('#allTabDivs').append(div);
            $('#tabDiv'+count).show(400);
             var divIds=new Array();
                        var p=1;
                        $('.damsCms').each(function (index, value) { 
                            divIds[p]=$(this).attr('name');
                            p++;
            });
            for(var m=1;m<=srNo;m++){
                  if(m!=srNo){
                    $('#addmorediv'+divIds[m]).hide();
                }
            }
                    $('#tabOrder'+count).attr('oninput',"this.value = this.value.replace(/[^0-9]/g, '');  this.value = this.value.replace(/^(-?).*?(\d+\.\d{5}).*$/, '$1$2')");
    },


        addEvent:function(id){
            if($('#span'+id).attr('class')=='checked'){
            $('#span'+id).removeClass('checked');    
            }else{
            $('#span'+id).addClass('checked');
            }
        },
        downloadExcel:function(id){
       window.location=file="files/excel/sample.xls";  
        },
        
        imageValidation:function(input,id){
            if (input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    var value=$("#"+id).val();
                    var ext =value.split('.').pop().toLowerCase();
                    if($.inArray(ext, ['jpg','jpeg','png']) == -1) {
                        alert( value+ ' is invalid File!');
                        $('#preloaderCenter').hide();
                    return false;
                    }
                    if(input.files[0].size>1048576){
                         alert("The image must be less than 500 kb.");
                         $('#preloaderCenter').hide();
                    return false;
                    }
                    if (typeof (input.files) != "undefined") {
                        var reader = new FileReader();
                            reader.readAsDataURL(input.files[0]);
                            reader.onload = function (e) {
                            var image = new Image();
                            image.src = e.target.result;
                            image.onload = function () {
                            var height = this.height;
                            var width = this.width;
                                if (height > 520 || width > 1500) {
                                    alert("image resolution should be less then 1400"+'\xD7'+"520 px");
                                    $('#preloaderCenter').hide();
                                    $("#"+id).val("");
                                    $(".filename").html("No file selected");
                                return false;
                                }else if(height < 500 || width < 1350){
                                    alert("image resolution should be greater then 1350"+'\xD7'+"500 px");
                                    $('#preloaderCenter').hide();
                                    $("#"+id).val("");
                                    $(".filename").html("No file selected");
                                return false;
                                }
                            };
                        }
                    }    
                };
                reader.readAsDataURL(input.files[0]);
            }
        },
        seeMore:function(id){
        qs=document.URL;
            qs = qs.split('+').join(' ');
            var params = {},
            tokens,
            re = /[?&]?([^=]+)=([^&]*)/g;
            while (tokens = re.exec(qs)) {
            params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
            }
            if(params['ln']=='1'){
                var start =$('#banner_StartPoint').val();
                var end   =15;
                var total =$('#banner_total').val();
                    $('#preloader').show();
                    banner=new homeBannerData();
                    banner.getDataAllPagination(start,end);    
            }
            if(params['ln']=='2'){
                var start =$('#course_StartPoint').val();
                var end   =15;
                var total =$('#course_total').val();
                    $('#preloader').show();
                    course=new courseData();
                    course.getDataAllPagination(start,end);
            }
            if(params['ln']=='6'){
                var start =$('#package_StartPoint').val();
                var end   =15;
                var total =$('#package_total').val();
                    $('#preloader').show();
                    packageData.getDataAllPagination(start,end);    
            }
            if(params['ln']=='5'){
                var start =$('#faq_StartPoint').val();
                var end   =15;
                var total =$('#faq_total').val();
                    $('#preloader').show();
                    faq=new FAQData();
                    faq.getDataAllPagination(start,end);    
            }
            if(params['ln']=='3'){
                var start =$('#image_StartPoint').val();
                var end   =15;
                var total =$('#image_total').val();
                    $('#preloader').show();
                    imageData.getDataAllPagination(start,end);    
            }
            if(params['ln']=='7'){
                var start =$('#video_StartPoint').val();
                var end   =15;
                var total =$('#video_total').val();
                    $('#preloader').show();
                    videoData.getDataAllPagination(start,end);    
            }
        },
    }