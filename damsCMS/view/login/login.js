

var login = function(){

    this.trim  = function(val){
        return val.replace(/^\s+|\s+$/g,'');
    }

    this.validation  = function(type,value){
        var ck_name = /^[A-Za-z0-9. ]{3,50}$/;
        var ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i ;
        var ck_username = /^[A-Za-z0-9_]{1,20}$/;
        var ck_password =  /^[A-Za-z0-9.!@#$%^&*()_]{6,20}$/;
        var ck_contact = /^\d{10}$/;
        var check  = true;

        switch(type){
            case 'name':
                        check=ck_name.test(value);
                        break;
            case 'email':
                        check=ck_email.test(value);
                        break;
            case 'password':
                        check=ck_password.test(value);
                        break;
            case 'username':
                        check=ck_username.test(value);
                        break;
            case 'contact':
                        check=ck_contact.test(value);
                        break;
            case 'null':
                        if(value==''){
                            check=false;
                        }
                        break;
            default:
                        check=false;
        }
        return check;

    }


    this.isValid  = function(){
        
        var check = false;
        var email = $('#email').val().toLowerCase();
        var pass = $('#pass').val().toLowerCase();
        
        if(this.validation('null',this.trim(email))==false){
            alert('Please enter your E-mail Id');
            $('#email').focus();
            check=false;
        }else if(!this.validation('email',this.trim(email))){
            alert('Please enter valid Email Address');
            $('#email').focus();
            check=false;
        }else if(this.validation('null',this.trim(pass))==false){
            alert('Please enter your Password');
            $('#pass').focus();
            check=false;
        }else if(!this.validation('password',this.trim(pass))){
            alert('Please enter valid Password');
            $('#pass').focus();
            check=false;
        }else{
           
            var data = "email="+email+"&pass="+pass+"&mode=loginCheck";
            $.ajax({
                type:'post',
                url:'index.php?p=view/login/ajax.php',
                data:data,
                success:function(result){
                     var objValid = new login();
                    result = objValid.trim(result);
                 
                    if(result == 'true'){
                       location.href="index.php?p=12&ln=1&t=7";
                    }else{
                        alert("Email or Password doesn't match");
                        check = false;
                    }
                }
           
            });
        }
      return check;

    }



}

$("#loginForm").submit(function(){

    var objValid = new login();
    
    return objValid.isValid();

});