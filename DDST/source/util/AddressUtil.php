<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/* This class is define for show variable on page */
//HERE COUNTRY_NAME,iNSTITUTE_NAME,STATE_NAME,CITY_NAME add by navneet
class AddressUtil{

    public static function Itam($text, $value=NULL, $where=NULL) {
        $database = new Database();

        switch ($text) {
            case country:
                $database->getListBox(COUNTRY, 'COUNTRY_ID,COUNTRY_NAME','','COUNTRY_NAME');
                break;
            case countrySelect:
                $database->getListBox(DST_COUNTRY, 'COUNTRY_ID,COUNTRY_NAME','','COUNTRY_NAME');
                break;
            case state:
                $database->getListBox(STATE, 'STATE_ID,STATE_NAME', "COUNTRY_ID='1'","STATE_NAME");
                break;
            case stateSelect: // added by Shubham
                $database->getListBox(DST_STATE, 'STATE_ID,STATE_NAME', "ACTIVE=1 AND COUNTRY_ID='$value'","STATE_NAME");
                break;
            case city:
                $database->getListBox(CITY, 'CITY_ID,CITY_NAME', "STATE_ID='7'","CITY_NAME");
                break;
            case citySelect:
                $database->getListBox(DST_CITY, 'CITY_ID,CITY_NAME', "ACTIVE=1 AND STATE_ID='$value'","CITY_NAME");
                break;
            case source:
                $database->getListBox(CONSTANT_VALUE, 'CONSTANT_VALUE_ID,CONSTANT_DETAIL', "CONSTANT_ID='2'");
                break;
            case enquiry:
                $database->getListBox(CONSTANT_VALUE, 'CONSTANT_VALUE_ID,CONSTANT_DETAIL', "CONSTANT_ID='1'");
                break;
            case status:
                $database->getListBox(CONSTANT_VALUE, 'CONSTANT_VALUE_ID,CONSTANT_DETAIL', "CONSTANT_ID='3'");
                break;
            case studentEnquiry:
                $database->getListBox(CONSTANT_VALUE, 'CONSTANT_VALUE_ID,CONSTANT_DETAIL', "CONSTANT_ID='5'");
                break;
            case course:
                $database->getListBox(COURSE, 'COURSE_ID,COURSE_NAME');
                break;
            case batchSelect:
                $database->getListBox(BATCH, 'BATCH_ID,BATCH_NAME', "COURSE_ID='$value'");
                break;
            case citySelectValue:
                $database->getListBox(CITY, 'CITY_ID,CITY_NAME', "$where", 'CITY_NAME', "$value");
                break;
            case stateSelectValue:
                $database->getListBox(STATE, 'STATE_ID,STATE_NAME', '', "STATE_NAME", "$value");
                break;
            case countrySelectValue:
                $database->getListBox(COUNTRY, 'COUNTRY_ID,COUNTRY_NAME', '', 'COUNTRY_NAME', "$value");
                break;
            case enpuirySelectValue:
                $database->getListBox(CONSTANT_VALUE, 'CONSTANT_VALUE_ID,CONSTANT_DETAIL', "CONSTANT_ID='1'", '', "$value");
                break;
            case sourceSelectValue:
                $database->getListBox(CONSTANT_VALUE, 'CONSTANT_VALUE_ID,CONSTANT_DETAIL', "CONSTANT_ID='2'", '', "$value");
                break;
            case statusSelectValue:
                $database->getListBox(CONSTANT_VALUE, 'CONSTANT_VALUE_ID,CONSTANT_DETAIL', "CONSTANT_ID='3'", '', "$value");
                break;
            case studentEnquirySelectValue:
                $database->getListBox(CONSTANT_VALUE, 'CONSTANT_VALUE_ID,CONSTANT_DETAIL', "CONSTANT_ID='5'",'',"$value");
                break;
            case courseSelectValue:
                $database->getListBox(COURSE, 'COURSE_ID,COURSE_NAME', '', '', "$value");
                break;
            case batchSelectValue:
                $database->getListBox(BATCH, 'BATCH_ID,BATCH_NAME', "COURSE_ID='$where'", '', "$value");
                break;
            case subject:
                $database->getListBox(SUBJECT, 'ID,SUBJECT_NAME', '', 'SUBJECT_NAME', "$value", $where);
                break;
            case topic:
                $database->getListBox(TOPICS, 'ID,TOPIC_NAME', "SUBJECT_ID='$value'", '', "$where");
                break;
            case questionType:
                $database->getListBox(QUESTION_TYPE, 'ID,QUESTION_NAME', 'STATUS=1 AND ID NOT IN (1,6)', ' Question_Order ', "$value");
                break;
            case difficultyLevel:
                $database->getListBox(DIFFICULTY_LEVEL, 'ID,DIFFCULTY_NAME', '', 'DIFFCULTY_NAME', "$value");
                break;
            case questionYear:
                $database->getListBox(QUESTION_YEAR, 'ID,YEAR', '', 'YEAR', "$value");
                break;
            case yn:
                $database->getListBox(CONSTANT_VALUE, 'CONSTANT_VALUE_ID,CONSTANT_DETAIL', "CONSTANT_ID='4'", '', "$value");
                break;
             case institute:
                $database->getListBox(INSTITUTE, 'INSTITUTE_ID,INSTITUTE_NAME','','INSTITUTE_NAME');
                break;
            case instituteSelectValue:
                $database->getListBox(INSTITUTE, 'INSTITUTE_ID,INSTITUTE_NAME', '', 'INSTITUTE_NAME', "$value");
                break;
            case benchMark:
                $database->getListBox(BENCHMARK, 'BENCHMARK_ID, BENCHMARK_NAME', '', '', "$value");
                break;
            case instruction:
                $database->getListBox(INSTRUCTION, 'INSTRUCTION_ID, INSTRUCTION_NAME', '', '', "$value");
                break;
            case package:
                $database->getListBox(PACKAGE, 'PACKAGE_ID, PACKAGE_NAME', '', '', "$value");
                break;
            case profile:
                $database->getListBox(PROFILE, 'DISTINCT(PROFILE_ID) AS PROFILE_ID, PROFILE_NAME', "PROFILE_NAME!='Ginger'", '', "$value");
                break;
            case student:
                $database->getListBox(STUDENT, 'STUDENT_ID, EMAIL', '', '', "$value");
                break;
            case selsectStatecountryWise:
                $database->getListBox(STATE, 'STATE_ID,STATE_NAME', "COUNTRY_ID=$where", "STATE_NAME", "$value");
                break;
            case questionExam:
                $database->getListBox('EXAM_TYPE', 'ID,EXAM_NAME', '', 'EXAM_NAME', "$value");
                break;
            case stream:
                ?><option <?php if ($value == NULL) { ?>selected='selected'<?php } ?> value=''>Select Stream</option>
                <?php
                $database->getListBox('STREAM', 'STREAM_ID,STREAM_NAME','','STREAM_ID', "$value");
                break;
            case courseStream:
                $database->getListBox('COURSE AS C,STREAM AS ST', 'COURSE_ID, CONCAT(COURSE_NAME,\'(\',STREAM_NAME,\')\')', 'C.STREAM_ID=ST.STREAM_ID', '', "$value");
                break;
            case testType:
                ?><option <?php if ($value == NULL) { ?>selected='selected'<?php } ?> value=''>Select Test Type</option>
                ?><option <?php if ($value == 1) { ?>selected='selected'<?php } ?> value='1'>Practice</option>
                ?><option <?php if ($value == 2) { ?>selected='selected'<?php } ?> value='2'>Examination</option>
<?php
                break;
        }
    }

    public static function DisplayRecord($text){
        $str="-";
        if($text!="")
            return $text;
        else
            return $str;
    }

    public static function convertDate($text) {
        if ($text == "") {
            return $text;
        } else {
            list($d, $m, $y) = explode('-', $text);
            $text = mktime(0, 0, 0, $m, $d, $y);
            $text = strftime('%Y-%m-%d', $text);
            return $text;
        }
    }

public static function displayDate($text){
    if($text=="0000-00-00"){
        return "";
    }else{
        list($y, $m, $d) = explode('-', $text);
        $text=mktime(0, 0, 0, $m, $d, $y);
        $text=strftime('%d-%m-%Y',$text);
        return $text;
        }
    }

public static function displayDateWithMonth($text){ // ADDED BY sHUBHAM
    if($text=="0000-00-00"){
        return "";
    }else{
        list($y, $m, $d) = explode('-', $text);
        $text=mktime(0, 0, 0, $m, $d, $y);
        $text=strftime('%d-%b-%Y',$text);
        return $text;
        }
    }


public static function displayQuePdf($text,$serial){
                $st=str_replace('/document', './../../../../document', html_entity_decode(stripslashes($text), ENT_QUOTES,'UTF-8'));
                $str=explode(">",$st);
                if($st[0]=="<"){
                    $str[1]="<b>$serial.</b> ".$str[1];
                    $str1= implode(">", $str);
                    return stripslashes($str1);
                }else {
                    return stripslashes("<b>$serial.</b> ".$st);
                }
}
public static function displayEssayPdf($text,$serial){
                $st=str_replace('/examener/document', './../../../../document', html_entity_decode(stripslashes($text), ENT_QUOTES,'UTF-8'));
                $str=explode(">",$st);
                if($st[0]=="<"){
                    $str[1]="<b>$serial :</b> <br/> ".$str[1];
                    $str1= implode(">", $str);
                    return stripslashes($str1);
                }else {
                    return stripslashes("<b>$serial :</b> <br/> ".$st);
                }
}
public static function sendSms($userName,$password,$mobile,$msg){
     $msg=urlencode($msg);
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, "https://sms.gingerwebs.com/WebServiceSMS.aspx?User=$userName&passwd=$password&mobilenumber=$mobile&message=$msg&sid=thinkexam&mtype=N");
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch, CURLOPT_POST, 1);
     curl_setopt($ch, CURLOPT_POSTFIELDS, "Hello=World&Foo=Bar&Baz=Wombat");
     $contents = curl_exec($ch);
     curl_close($ch);

}
public static function sendMobileSms($mobile,$msg){/*added by deepak*/
        $ch = curl_init();
        $url = "https://web.insignsms.com/api/sendsms?username=damstrans&password=damstrans&senderid=DAMSPG&message=$msg&numbers=$mobile&dndrefund=1";
        //$url = "https://opt.smsapi.org/SendSMS.aspx?UserName=optdamsdelhi&password=optdamsdelhi&MobileNo=$mobile&SenderID=DAMSPG&Message=$msg";
        $url = str_replace(" ", '%20', $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        $contents=curl_exec($ch);
        curl_close($ch);

}
public static function displayQueXml($text){//add by Gaurav for generate xml on-25/02
                $text=html_entity_decode($text, ENT_QUOTES);
                $unicode=array("&#34;", "&#39;", "&#160;", "&#161;", "&#162;", "&#163;", "&#164;", "&#165;", "&#166;", "&#167;", "&#168;", "&#169;", "&#170;", "&#171;", "&#172;", "&#173;", "&#174;", "&#175;", "&#176;", "&#177;", "&#178;", "&#179;", "&#180;", "&#181;", "&#182;", "&#183;", "&#184;", "&#185;", "&#186;", "&#187;", "&#188;", "&#189;", "&#190;", "&#191;", "&#215;", "&#247;", "&#192;", "&#193;", "&#194;", "&#195;", "&#196;", "&#197;", "&#198;", "&#199;", "&#200;", "&#201;", "&#202;", "&#203;", "&#204;", "&#205;", "&#206;", "&#207;", "&#208;", "&#209;", "&#210;", "&#211;", "&#212;", "&#213;", "&#214;", "&#216;", "&#217;", "&#218;", "&#219;", "&#220;", "&#221;", "&#222;", "&#223;", "&#224;", "&#225;", "&#226;", "&#227;", "&#228;", "&#229;", "&#230;", "&#231;", "&#232;", "&#233;", "&#234;", "&#235;", "&#236;", "&#237;", "&#238;", "&#239;", "&#240;", "&#241;", "&#242;", "&#243;", "&#244;", "&#245;", "&#246;", "&#248;", "&#249;", "&#250;", "&#251;", "&#252;", "&#253;", "&#254;", "&#255;", "&#8704;", "&#8706;", "&#8707;", "&#8709;", "&#8711;", "&#8712;", "&#8713;", "&#8715;", "&#8719;", "&#8721;", "&#8722;", "&#8727;", "&#8730;", "&#8733;", "&#8734;", "&#8736;", "&#8743;", "&#8744;", "&#8745;", "&#8746;", "&#8747;", "&#8756;", "&#8764;", "&#8773;", "&#8776;", "&#8800;", "&#8801;", "&#8804;", "&#8805;", "&#8834;", "&#8835;", "&#8836;", "&#8838;", "&#8839;", "&#8853;", "&#8855;", "&#8869;", "&#8901;", "&#913;", "&#914;", "&#915;", "&#916;", "&#917;", "&#918;", "&#919;", "&#920;", "&#921;", "&#922;", "&#923;", "&#924;", "&#925;", "&#926;", "&#927;", "&#928;", "&#929;", "&#931;", "&#932;", "&#933;", "&#934;", "&#935;", "&#936;", "&#937;", "&#945;", "&#946;", "&#947;", "&#948;", "&#949;", "&#950;", "&#951;", "&#952;", "&#953;", "&#954;", "&#955;", "&#956;", "&#957;", "&#958;", "&#959;", "&#960;", "&#961;", "&#962;", "&#963;", "&#964;", "&#965;", "&#966;", "&#967;", "&#968;", "&#969;", "&#977;", "&#978;", "&#982;", "&#338;", "&#339;", "&#352;", "&#353;", "&#376;", "&#402;", "&#710;", "&#732;", "&#8194;", "&#8195;", "&#8201;", "&#8204;", "&#8205;", "&#8206;", "&#8207;", "&#8211;", "&#8212;", "&#8216;", "&#8217;", "&#8218;", "&#8220;", "&#8221;", "&#8222;", "&#8224;", "&#8225;", "&#8226;", "&#8230;", "&#8240;", "&#8242;", "&#8243;", "&#8249;", "&#8250;", "&#8254;", "&#8364;", "&#8482;", "&#153;", "&#8592;", "&#8593;", "&#8594;", "&#8595;", "&#8596;", "&#8629;", "&#8968;", "&#8969;", "&#8970;", "&#8971;", "&#9674;", "&#9824;", "&#9827;", "&#9829;", "&#9830;");
                $code=array("&quot;", "&apos;", "&nbsp;", "&iexcl;", "&cent;", "&pound;", "&curren;", "&yen;", "&brvbar;", "&sect;", "&uml;", "&copy;", "&ordf;", "&laquo;", "&not;", "&shy;", "&reg;", "&macr;", "&deg;", "&plusmn;", "&sup2;", "&sup3;", "&acute;", "&micro;", "&para;", "&middot;", "&cedil;", "&sup1;", "&ordm;", "&raquo;", "&frac14;", "&frac12;", "&frac34;", "&iquest;", "&times;", "&divide;", "&Agrave;", "&Aacute;", "&Acirc;", "&Atilde;", "&Auml;", "&Aring;", "&AElig;", "&Ccedil;", "&Egrave;", "&Eacute;", "&Ecirc;", "&Euml;", "&Igrave;", "&Iacute;", "&Icirc;", "&Iuml;", "&ETH;", "&Ntilde;", "&Ograve;", "&Oacute;", "&Ocirc;", "&Otilde;", "&Ouml;", "&Oslash;", "&Ugrave;", "&Uacute;", "&Ucirc;", "&Uuml;", "&Yacute;", "&THORN;", "&szlig;", "&agrave;", "&aacute;", "&acirc;", "&atilde;", "&auml;", "&aring;", "&aelig;", "&ccedil;", "&egrave;", "&eacute;", "&ecirc;", "&euml;", "&igrave;", "&iacute;", "&icirc;", "&iuml;", "&eth;", "&ntilde;", "&ograve;", "&oacute;", "&ocirc;", "&otilde;", "&ouml;", "&oslash;", "&ugrave;", "&uacute;", "&ucirc;", "&uuml;", "&yacute;", "&thorn;", "&yuml;", "&forall;", "&part;", "&exist;", "&empty;", "&nabla;", "&isin;", "&notin;", "&ni;", "&prod;", "&sum;", "&minus;", "&lowast;", "&radic;", "&prop;", "&infin;", "&ang;", "&and;", "&or;", "&cap;", "&cup;", "&int;", "&there4;", "&sim;", "&cong;", "&asymp;", "&ne;", "&equiv;", "&le;", "&ge;", "&sub;", "&sup;", "&nsub;", "&sube;", "&supe;", "&oplus;", "&otimes;", "&perp;", "&sdot;", "&Alpha;", "&Beta;", "&Gamma;", "&Delta;", "&Epsilon;", "&Zeta;", "&Eta;", "&Theta;", "&Iota;", "&Kappa;", "&Lambda;", "&Mu;", "&Nu;", "&Xi;", "&Omicron;", "&Pi;", "&Rho;", "&Sigma;", "&Tau;", "&Upsilon;", "&Phi;", "&Chi;", "&Psi;", "&Omega;", "&alpha;", "&beta;", "&gamma;", "&delta;", "&epsilon;", "&zeta;", "&eta;", "&theta;", "&iota;", "&kappa;", "&lambda;", "&mu;", "&nu;", "&xi;", "&omicron;", "&pi;", "&rho;", "&sigmaf;", "&sigma;", "&tau;", "&upsilon;", "&phi;", "&chi;", "&psi;", "&omega;", "&thetasym;", "&upsih;", "&piv;", "&OElig;", "&oelig;", "&Scaron;", "&scaron;", "&Yuml;", "&fnof;", "&circ;", "&tilde;", "&ensp;", "&emsp;", "&thinsp;", "&zwnj;", "&zwj;", "&lrm;", "&rlm;", "&ndash;", "&mdash;", "&lsquo;", "&rsquo;", "&sbquo;", "&ldquo;", "&rdquo;", "&bdquo;", "&dagger;", "&Dagger;", "&bull;", "&hellip;", "&permil;", "&prime;", "&Prime;", "&lsaquo;", "&rsaquo;", "&oline;", "&euro;", "&trade;", "&larr;", "&uarr;", "&rarr;", "&darr;", "&harr;", "&crarr;", "&lceil;", "&rceil;", "&lfloor;", "&rfloor;", "&loz;", "&spades;", "&clubs;", "&hearts;", "&diams;");
                for ($index = 0; $index < count($code); $index++) {
                    $text = str_replace($code[$index], $unicode[$index], $text);
                }
                $st=$text;
                return $st;
}
//public static function getCCavenueStatus($merchantId,$orderId){
//    /*--Add by Gaurav for Payment Gateway on-4/6--*/
//     $str = file_get_contents("https://www.ccavenue.com/servlet/new_txn.OrderStatusTracker?Merchant_Id=$merchantId&Order_Id=".$orderId);
//     if(strpos($str, "&AuthDesc=")!==false){
//                $startPoint = strpos($str, "&AuthDesc=");
//                $findValue = substr($str, $startPoint+10,1);
//                return $findValue;
//     } else if(strpos($str, "error=ABANDONED_TRACKING_ID")!==false) {
//                return "N";
//
//     }else {
//                return "NotAvailable";
//     }
//
//}
public static function getCCavenueStatus($merchantId,$orderId){
    /*--Add by Gaurav for Payment Gateway on-4/6--*/
     $str = file_get_contents("https://www.ccavenue.com/servlet/new_txn.OrderStatusTracker?Merchant_Id=$merchantId&Order_Id=".$orderId);
     if(strpos($str, "&AuthDesc=")!==false){
                $startPoint = strpos($str, "&AuthDesc=");
                $findValue = substr($str, $startPoint+10,1);
                return $findValue;
//     } else if(strpos($str, "error=ABANDONED_TRACKING_ID")!==false) {
//                return "N";

     }else if(strpos($str, "error=")!==false && trim($str)=="error=ABANDONED_TRACKING_ID"){//added by Shubham 4-04-2014
                $startPoint = strpos($str, "error=");
                $findValue = substr($str, $startPoint+6,21);
                return $findValue;
     }else {
                return "NotAvailable";
     }

}
    /****************this function is added by Shubham for conver number to k,m,b***********/
public static function standerdMoney($n){


        $n = (0+str_replace(",","",$n));

        // is this a number?
        if(!is_numeric($n)) return false;

        // now filter it;
        if($n>1000000000000) return round(($n/1000000000000),1).' T';//trillion
        else if($n>1000000000) return round(($n/1000000000),1).' B';//billion
        else if($n>1000000) return round(($n/1000000),1).' M';//million
        else if($n>1000) return round(($n/1000),1).' K';//thousand

        return number_format($n);


    }
    public static function dataTrim($trimValue){
      $trimValue=preg_replace('/\s+/', '', $trimValue);
      return $trimValue;
    }
    /*****************close by shubham*********************/
    
    
    public static $ADMIN_MAIL = "donotreply@damsdelhi.com";
}
?>
