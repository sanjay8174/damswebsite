<?php 

ob_start();
session_start();
require_once("config/autoloader.php");
Logger::configure('./config/log4php.xml');
//echo "hiiiii";exit;
$log = Logger::getLogger(__CLASS__);
$timezone = "Asia/Calcutta";
if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set($timezone);
}
$queryDao = new queryDao();
$p = $_REQUEST['p'];

if($p == '' && $p !='ajax'){
    include './dashboard.php';
} else if($p == 'submit' || $p == 'submit1' || $p == 'ajax' || $p == 'payment' || $p == 'onlinePytm'){
    include "./$p.php";
}
?>

