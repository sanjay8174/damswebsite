<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, National Quiz</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->
<!--<script type="text/javascript">
$(document).ready(function(){
	//$('div.accordionButton').click(function() {
//		$('div.accordionContent').slideUp('normal');	
//		$(this).next().slideDown('normal');
//	});		
//	$("div.accordionContent").hide();
	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>-->
<link href="gallerySlider/owl.carousel.css" rel="stylesheet">
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false);">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article class="national-banner">
      <div class="big-nav">
        <ul>
          <li class="face-face"><a href="regular_course_for_pg_medical.php" title="Face To Face Classes">Face To Face Classes</a></li>
          <li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
          <li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
          <li class="a-achievement"><a href="aipge_2014.php" title="Achievement">Achievement</a></li>
        </ul>
      </div>
      <aside class="banner-left banner-left-postion">
        <h3>GREY MATTER DAMS National Quiz <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box">
        <h3>National Quiz</h3>
        <div class="franchisee-box" style="padding-bottom:0px">
          <p>As you must be aware that Delhi Academy of Medical Sciences is organizing a NATIONAL quiz project “GREY MATTER – DAMS National Quiz” being one of its kind medical quiz program. The project is intended to find and honor the best medical brain in the country.</p>
          <p>This mega event kicked off recently, with two invites already sent to your respective colleges. You must have been informed of the same by now. It is going to provide an opportunity to medical students to enrich themselves by our experiences and further help them to explore the hidden talent in them.</p>
          <p>It is your chance to participate and win fabulous prizes.</p>
        </div>
      </div>
      <div class="pg-medical-main tab-hide">
        <div class="pg-heading"><span></span>National Quiz Courses</div>
        <div class="course-new-section">
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('1');"><span class="plus-ico" id="s1"></span>Classroom Course</h5>
            <div class="coures-list-box-content"  id="di1" style="display:none;">
              <ul class="course-new-list">
                <li><a href="gray-matter.php" title="Concept of Grey Matter"><span class="sub-arrow"></span>Concept of Grey Matter</a></li>
                <li><a href="gray-matter-testimonial.php" title="Testimonials"><span class="sub-arrow"></span>Testimonials</a></li>
              </ul>
            </div>
          </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('2');"><span class="plus-ico" id="s2"></span>2012</h5>
            <div class="coures-list-box-content" id="di2" style="display:none;">
              <ul class="course-new-list">
                <li><a href="round1.php" title="1st Campus Round 2012"><span class="sub-arrow"></span>1st Campus Round 2012</a></li>
                <li><a href="round2.php" title="2nd Campus Round 2012"><span class="sub-arrow"></span>2nd Campus Round 2012</a></li>
                <li><a href="grand.php" title="Grand Finale 2012"><span class="sub-arrow"></span>Grand Finale 2012</a></li>
              </ul>
            </div>
          </div>
          <div class="coures-list-box">
            <h5 class="h2toggle" onclick="sliddes('3');"><span class="plus-ico" id="s3"></span>2013</h5>
            <div class="coures-list-box-content" id="di3" style="display:none;">
              <ul class="course-new-list">
                <li><a href="season2round1.php" title="1st Campus Round 2013"><span class="sub-arrow"></span>1st Campus Round 2013</a></li>
                <li><a href="season2round2.php" title="2nd Campus Round 2013"><span class="sub-arrow"></span>2nd Campus Round 2013</a></li>
                <li><a href="grand2013.php" title="Grand Finale 2013"><span class="sub-arrow"></span>Grand Finale 2013</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="recent-pg-main">
        <div class="span12">
          <div class="recent-pg-heading1"> 
           <a href="photo-gallery.php" class="photogal photogal_inline"><span>&nbsp;</span> &nbsp;Recent Achievement in DAMS</a>
          </div>
          <div class="customNavigation"> <a class="btn prev" href="javascript:void(0);"><img src="images/left.png"></a> <a class="btn next" href="javascript:void(0);"><img src="images/right-inactive.png"></a> </div>
        </div>
        <div id="owl-demo" class="owl-carousel" style="margin-left:4px;">
          <div class="item"> <img src="images/topper/1.jpg"> <span class="_name">Dr. Saba Samad Memon</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">AIPGE (2014)</span> </div>
          <div class="item"> <img src="images/topper/2.jpg"> <span class="_name">Dr. Mayank Agarwal</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">ASSAM PG (2014)</span> </div>
          <div class="item"> <img src="images/topper/3.jpg"> <span class="_name">Dr. Archana Badala</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">RAJASTHAN CET (2014)</span> </div>
          <div class="item"> <img src="images/topper/4.jpg"> <span class="_name">Dr. Rahul Baweja</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">DELHI PG (2014)</span> </div>
          <div class="item"> <img src="images/topper/1-.jpg"> <span class="_name">Dr. Saba Samad Memon</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">MAHCET (2013-14)</span> </div>
          <div class="item"> <img src="images/topper/5.jpg"> <span class="_name">Dr. Tuhina Goel</span> <span class="_rank">Rank : <strong>1</strong></span> <span class="_field">AIIMS</span> </div>
          <div class="item"> <img src="images/topper/4.jpg"> <span class="_name">Dr. Rahul Baweja</span> <span class="_rank">Rank : <strong>2</strong></span> <span class="_field">DNB CET (2013)</span> </div>
          <div class="item"> <img src="images/topper/6.jpg"> <span class="_name">Dr. Pooja jain</span> <span class="_rank"><strong>Rank 5</strong></span> <span class="_field">DNB CET (2013)</span> </div>
        </div>
        <div class="view-all-photo"><a href="aipge_2014.php" title="View All">View&nbsp;All</a></div>
      </div>
    </aside>
    <aside class="content-right">
      <?php include 'national-quiz-accordion.php'; ?>
      <div class="news-update-box">
        <div class="n-heading"><span></span> News &amp; Updates</div>
        <div class="news-content-box">
          <div style="width:100%; float:left; height:228px; overflow:hidden;">
            <ul id="ul0" style="display:block;">
              <li><span></span>
                <p>DAMS family proudly congrats Dr Nishtha Yadav, our class room foundation course student from Delhi for getting Rank 2 in AIIMS Nov 2012.</p>
              </li>
              <li class="orange"><span></span>
                <p>Fabulous results by DAMSONIANS in AIIMS Nov2012- Many selections and Rankers- Congratulations.</p>
              </li>
              <li><span></span>
                <p>DAMSONIANS Rock the AIIMS NOV2012-Once again proving us to be the leader in PG Medical Education.</p>
              </li>
            </ul>
            <ul id="ul1" class="display_none">
              <li><span></span>
                <p>DAMSONIANS Rock the AIIMS NOV2012-Once again proving us to be the leader in PG Medical Education.</p>
              </li>
              <li class="orange"><span></span>
                <p>DAMS family proudly congrats Dr Nishtha Yadav, our class room foundation course student from Delhi for getting Rank 2 in AIIMS Nov 2012.</p>
              </li>
              <li><span></span>
                <p>Fabulous results by DAMSONIANS in AIIMS Nov2012- Many selections and Rankers- Congratulations.</p>
              </li>
            </ul>
            <ul id="ul2" class="display_none">
              <li><span></span>
                <p>DAMSONIANS Rock the AIIMS NOV2012-Once again proving us to be the leader in PG Medical Education.</p>
              </li>
              <li class="orange"><span></span>
                <p>Fabulous results by DAMSONIANS in AIIMS Nov2012- Many selections and Rankers- Congratulations.</p>
              </li>
              <li><span></span>
                <p>DAMSONIANS Rock the AIIMS NOV2012-Once again proving us to be the leader in PG Medical Education.</p>
              </li>
            </ul>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="news.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More </a>
            <div class="slider-mini-dot">
              <ul>
                <li id="u0" class="current" onClick="news('0');"></li>
                <li id="u1" class="" onClick="news('1');"></li>
                <li id="u2" class="" onClick="news('2');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
      <div class="news-update-box">
        <div class="n-videos"><span></span> Students Interview</div>
        <div class="videos-content-box">
          <div id="vd0" class="display_block" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/cC2a-En6XK4" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd1" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/5jaHSl8EHPE" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd2" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/zvRSvxwOfwc" frameborder="0" allowfullscreen></iframe>
          </div>
          <div id="vd3" class="display_none" >
            <iframe width="100%" height="236" src="//www.youtube.com/embed/YJaxKtFiAMA" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
        <div class="box-bottom-1"> <span class="mini-view-right"></span>
          <div class="mini-view-midle"> <a href="photo-gallery.php" class="view-more-btn" title="View More"><span></span>View&nbsp;More</a>
            <div class="slider-mini-dot">
              <ul>
                <li id="v0" class="current" onClick="video('0');"></li>
                <li id="v1" class="" onClick="video('1');"></li>
                <li id="v2" class="" onClick="video('2');"></li>
                <li id="v3" class="" onClick="video('3');"></li>
              </ul>
            </div>
          </div>
          <span class="mini-view-left"></span> </div>
      </div>
    </aside>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages --> 

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript">
 var m=0;
 var l=0;
 var n=0;
 var videointerval ='';
 var newsinterval = '';
 $(document).ready(function(){
   var count1=$("#recent li").length;
   $("#prev").click(function(){
	 if(m>0)
	 {
	   var j=m+3;
       $("#li"+j).hide('fast','linear');
	   m--;
	   $("#li"+m).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   $(".slide-arrow > a.left").css('background','url(images/right.png)');
	   if(m==0)
	   {
		 $(".slide-arrow > a.right").css('background','url(images/left.png)');   
	   }
	 }
   });
   $("#next").click(function(){	 
	 if(m<count1-4)
	 {
       $("#li"+m).hide('fast','linear');
	   var j=m+4;
	   $("#li"+j).show('fast','linear');
	   $(".slide-arrow > a.right").css('background','url(images/left-active.png)');
	   m++;
	   if(m==(count1-4))
	   {
		 $(".slide-arrow > a.left").css('background','url(images/right-inactive.png)');
	   }
     }	 
   });
   

	
//     Registration Form
    $('#student-registration').click(function() {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });

//     Quick Enquiry Form
	$('#student-enquiry').click(function(e) {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });	

//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

   window.onload = function()
   {	  
	    var count2=$(".videos-content-box > img").length;
	    var count3=$(".news-content-box > ul").length;
        function setvideo()
		{
		   l=l%count2;		
		   $("#vd"+l).fadeOut('slow','linear');
		   $("#v"+l).removeClass("current");
	       var j=(l+1)%count2;			
		   $("#vd"+j).fadeIn('slow','linear');
		   $("#v"+j).addClass("current");        	
	       l++;	
        }
	    //function setnews()
//		{
//		   n=n%count3;		
//		   $("#ul"+n).slideUp(100,'swing');
//		   $("#u"+n).removeClass("current");
//	       var j=(n+1)%count3;
//		   $("#ul"+j).slideDown(4000,'linear').animate({zindex:'2'}, 100);
//		   $("#u"+j).addClass("current");        	
//	       n++;	
//        }
     videointerval = setInterval(setvideo, 5000);
	 //newsinterval = setInterval(setnews, 9000);
   }
 });
 
 function news(val)
 {
   //setTimeout('newsinterval', 9000);
   if(val=='0')
   {	   
	  $("#ul1").hide();
	  $("#u1").removeClass("current"); 
	  $("#ul2").hide();
	  $("#u2").removeClass("current");
	  $("#ul0").slideDown(1000,'linear');
	  $("#u0").addClass("current");   
   }
   if(val=='1')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul2").hide();
	 $("#u2").removeClass("current");
	 $("#ul1").slideDown(1000,'linear');
	 $("#u1").addClass("current"); 
   }
   if(val=='2')
   {
	 $("#ul0").hide();
	 $("#u0").removeClass("current"); 
	 $("#ul1").hide();
	 $("#u1").removeClass("current");
	 $("#ul2").slideDown(1000,'linear');
	 $("#u2").addClass("current");
   }
  // n=val;
//   newsinterval = setInterval(setnews, 9000);  
 }
 
 function video(val)
 {
   setTimeout('videointerval', 5000);
   if(val=='0')
   {	   
      $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd0").fadeIn('fast','linear');
	  $("#v0").addClass("current");
   }
   if(val=='1')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd1").fadeIn('fast','linear');
	  $("#v1").addClass("current");
   }
   if(val=='2')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd3").hide();
	  $("#v3").removeClass("current");
	  $("#vd2").fadeIn('fast','linear');
	  $("#v2").addClass("current");
   }
   if(val=='3')
   {	   
      $("#vd0").hide();
	  $("#v0").removeClass("current");
	  $("#vd1").hide();
	  $("#v1").removeClass("current");
	  $("#vd2").hide();
	  $("#v2").removeClass("current");
	  $("#vd3").fadeIn('fast','linear');
	  $("#v3").addClass("current");
   }
   l=val;
   videointerval = setInterval(setnews, 5000);  
 }
 
function sliddes(val)
{
   var sp=$('.h2toggle > span').size();
   for(var d=1;d<=sp;d++)
   {   
       if(val!=d)
	   { 
    	 $('#s'+d).removeClass('minus-ico');
         $('#s'+d).addClass('plus-ico');
		 $('#di'+d).slideUp(400);
	   }      
   }
   
   $('#di'+val).slideToggle(400, function(){
	   if($(this).is(':visible'))
	   {
			$('#s'+val).removeClass('plus-ico');
            $('#s'+val).addClass('minus-ico');
       }
	   else
	   {
			$('#s'+val).removeClass('minus-ico');
            $('#s'+val).addClass('plus-ico');
       }
   });
}
</script> 

<!--gallary-----> 
<script src="gallerySlider/jquery-1.9.1.min.js"></script> 
<script src="gallerySlider/owl.carousel.js"></script> 
<script>
    $(document).ready(function() {

      var owl = $("#owl-demo");

      owl.owlCarousel();

   // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
   $(".play").click(function(){
        owl.trigger('owl.play',1000);
      })
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      })


   });
    </script>
</body>
</html>