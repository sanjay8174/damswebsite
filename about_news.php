<!DOCTYPE html>
<?php
$aboutUs_Id = $_REQUEST['a'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, NEET PG</title>
<meta name="description" content="Delhi Academy of Medical Sciences is one of the best PG Medical Coaching Centre in India offering regular course, crash course, postal course for PG Medical Student" />
<meta name="keywords" content="PG Medical Coaching India, PG Medical Coaching New Delhi, PG Medical Coaching Centre, PG Medical Coaching Centre New Delhi, PG Medical Coaching Centre India, PG Medical Coaching in Delhi NCR" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
</head>
<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>
<?php  $getAboutusContent = $Dao->getAboutusContent($aboutUs_Id);
$getAboutus = $Dao->getAboutus();
?>
<section class="inner-banner">
<div class="wrapper">
<article style="background:url('images/background_images/<?php echo $getAboutusContent[0][0];  ?>') right top no-repeat;">
<aside class="banner-left">
<?php echo  $getAboutusContent[0][1]; ?>
</aside></article></div></section>
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
<ul>
<li class="bg_none"><a href="<?php echo $getAboutus[0][1]."?a=".$getAboutus[0][0] ?>" title="<?php echo $getAboutus[0][2]?>"><?php echo $getAboutus[0][2]?></a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading">
<h4>News Media</h4>
<article class="showme-main">
<div class="about-content">
<div class="course-box nw_wpr" style="padding:0; box-shadow:none;">
<ul class="idTabs responce-show"> 
<li><a href="#print">Print Coverage</a></li>
<li><a href="#online">Electronic Coverage</a></li> 
</ul>
<div id="print">
<div class="satellite-content satellite-content1">
<div class="idams-box1"> 
<div class="image-row">
<div class="gallery-list1 display_block" id="gallery1">
<ul id="ul1" class="display_block">
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Indian_Express_Edex.png" data-lightbox="example-set" title="17th Oct 2016">
<img class="example-image" src="images/news_award/Indian_Express_Edex.png" >
<p><strong>Indian Express Edex</strong><br>17th Oct 2016<br></p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/TRINITYMIRROR_7.jpg" data-lightbox="example-set" title="17th Oct 2016, Chennai">
<img class="example-image" src="images/news_award/TRINITYMIRROR_7.jpg" >
<p><strong>TRINITYMIRROR</strong><br>17th Oct 2016<br>Chennai</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Navabharat_Pg02_10_Oct_2016.jpg" data-lightbox="example-set" title="10th Oct 2016, PUNE">
<img class="example-image" src="images/news_award/Navabharat_Pg02_10_Oct_2016.jpg" >
<p><strong>Navabharat(Pune Plus)</strong><br>10th Oct 2016<br>PUNE</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Navarashtra_Pg02_09_Oct_2016.jpg" data-lightbox="example-set" title="09th Oct 2016, PUNE">
<img class="example-image" src="images/news_award/Navarashtra_Pg02_09_Oct_2016.jpg" >
<p><strong>Navarashtra(Pune Plus)</strong><br>09th Oct 2016<br>PUNE</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Yashobhoom_Pg04_09_Oct_2016.jpg" data-lightbox="example-set" title="09th Oct 2016, PUNE">
<img class="example-image" src="images/news_award/Yashobhoom_Pg04_09_Oct_2016.jpg" >
<p><strong>Yashobhoomi(Just Pune)</strong><br>09th Oct 2016<br>PUNE</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/DNA_Ahm_DAMS_09_10_16_Pg03.jpg" data-lightbox="example-set" title="09th Oct 2016,  Ahmadabad">
<img class="example-image" src="images/news_award/DNA_Ahm_DAMS_09_10_16_Pg03.jpg" >
<p><strong>DNA - Ahmadabad</strong><br>09th Oct 2016<br> Ahmadabad</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Akali_Patrika_9_Oct_16_Page-8.jpg" data-lightbox="example-set" title="09th Oct 2016,  Chandigarh">
<img class="example-image" src="images/news_award/Akali_Patrika_9_Oct_16_Page-8.jpg" >
<p><strong>Akali Patrika</strong><br>09th Oct 2016<br> Chandigarh</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Punjab_Times_9_Oct_16_Page-8.jpg" data-lightbox="example-set" title="09th Oct 2016,  Chandigarh">
<img class="example-image" src="images/news_award/Punjab_Times_9_Oct_16_Page-8.jpg" >
<p><strong>Punjab Times</strong><br>09th Oct 2016<br> Chandigarh</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Daily_Post_13_Oct.jpg" data-lightbox="example-set" title="13th Oct 2016,  Chandigarh">
<img class="example-image" src="images/news_award/Daily_Post_13_Oct.jpg" >
<p><strong>Daily Post</strong><br>13th Oct 2016<br> Chandigarh</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Yugmarg_13_Oct.jpg" data-lightbox="example-set" title="13th Oct 2016,  Chandigarh">
<img class="example-image" src="images/news_award/Yugmarg_13_Oct.jpg" >
<p><strong>Yugmarg</strong><br>13th Oct 2016<br> Chandigarh</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Dabang_Duniya_08_Oct_2016_Page_11.jpg" data-lightbox="example-set" title="08th Oct 2016,  Mumbai">
<img class="example-image" src="images/news_award/Dabang_Duniya_08_Oct_2016_Page_11.jpg" >
<p><strong>Dabang Duniya</strong><br>08th Oct 2016<br> Mumbai</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Rashtriya_Adhika_08_Oct_2016_Page-11.jpg" data-lightbox="example-set" title="08th Oct 2016,  Mumbai">
<img class="example-image" src="images/news_award/Rashtriya_Adhika_08_Oct_2016_Page-11.jpg" >
<p><strong>Rashtriya Adhikar</strong><br>08th Oct 2016<br> Mumbai</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Punyanagari_Date_18_Oct.jpg" data-lightbox="example-set" title="18th Oct 2016,  Mumbai">
<img class="example-image" src="images/news_award/Punyanagari_Date_18_Oct.jpg" >
<p><strong>Punyanagari </strong><br>18th Oct 2016<br> Mumbai</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Navshakti_Date_18_Oct.jpg" data-lightbox="example-set" title="18th Oct 2016,  Mumbai">
<img class="example-image" src="images/news_award/Navshakti_Date_18_Oct.jpg" >
<p><strong>Navshakti </strong><br>18th Oct 2016<br> Mumbai</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/DAMS11-10-2016.png" data-lightbox="example-set" title="11st Oct 2016,  Delhi">
<img class="example-image" src="images/news_award/DAMS11-10-2016.png" >
<p><strong>Navodaya Times</strong><br>11st Oct 2016<br> Delhi</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/DAMS11-10-2016-HT.png" data-lightbox="example-set" title="11st Oct 2016,  Delhi">
<img class="example-image" src="images/news_award/DAMS11-10-2016-HT.png" >
<p><strong>Hindustan Times</strong><br>11st Oct 2016<br> Delhi</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Surya_Pgno_05_OCt_11.jpg" data-lightbox="example-set" title="05th Oct 2016,  Hyderabad">
<img class="example-image" src="images/news_award/Surya_Pgno_05_OCt_11.jpg" >
<p><strong>Surya</strong><br>05th Oct 2016<br> Hyderabad</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/The_Hans_India_12_Oct_11.jpg" data-lightbox="example-set" title="12th Oct 2016,  Hyderabad">
<img class="example-image" src="images/news_award/The_Hans_India_12_Oct_11.jpg" >
<p><strong>The Hans India</strong><br>12th Oct 2016<br> Hyderabad</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/TheNewIndian_Express_13_October.jpg" data-lightbox="example-set" title="13th Oct 2016,  Hyderabad">
<img class="example-image" src="images/news_award/TheNewIndian_Express_13_October.jpg" >
<p><strong>The New Indian Express</strong><br>13th Oct 2016<br> Hyderabad</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Andhra_Jyothi_Oct_10_2016_Page_08.jpg" data-lightbox="example-set" title="10th Oct 2016,  Bengaluru">
<img class="example-image" src="images/news_award/Andhra_Jyothi_Oct_10_2016_Page_08.jpg" >
<p><strong>Andhra Jyothi</strong><br>10th Oct 2016<br> Bengaluru</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Samyuktha_Karnataka_Oct_10_2016_Page_04.jpg" data-lightbox="example-set" title="10th Oct 2016,  Bengaluru">
<img class="example-image" src="images/news_award/Samyuktha_Karnataka_Oct_10_2016_Page_04.jpg" >
<p><strong>Samyuktha Karnataka</strong><br>10th Oct 2016<br> Bengaluru</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Dinakaran_Oct_10_2016_Page_02.jpg" data-lightbox="example-set" title="10th Oct 2016,  Bengaluru">
<img class="example-image" src="images/news_award/Dinakaran_Oct_10_2016_Page_02.jpg" >
<p><strong>Dinakaran</strong><br>10th Oct 2016<br> Bengaluru</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Dinasudar_Oct_09_2016_Page_03.jpg" data-lightbox="example-set" title="09th Oct 2016,  Bengaluru">
<img class="example-image" src="images/news_award/Dinasudar_Oct_09_2016_Page_03.jpg" >
<p><strong>Dinasudar</strong><br>09th Oct 2016<br> Bengaluru</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Eenadu_Oct_08_2016_Page_04.jpg" data-lightbox="example-set" title="08th Oct 2016,  Bengaluru">
<img class="example-image" src="images/news_award/Eenadu_Oct_08_2016_Page_04.jpg" >
<p><strong>Eenadu</strong><br>08th Oct 2016<br> Bengaluru</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Parivarthana_Prabha_Oct_08_2016_Page_03.jpg" data-lightbox="example-set" title="08th Oct 2016,  Bengaluru">
<img class="example-image" src="images/news_award/Parivarthana_Prabha_Oct_08_2016_Page_03.jpg" >
<p><strong>Parivarthana Prabha</strong><br>08th Oct 2016<br> Bengaluru</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Suvarna_Times_of_Karnataka_Oct_08_2016_Page_03.jpg" data-lightbox="example-set" title="08th Oct 2016,  Bengaluru">
<img class="example-image" src="images/news_award/Suvarna_Times_of_Karnataka_Oct_08_2016_Page_03.jpg" >
<p><strong>Suvarna Times of Karnataka</strong><br>08th Oct 2016<br> Bengaluru</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Aaj_Di_Awaz_9_October_16_Page-4.jpg" data-lightbox="example-set" title="09th Oct 2016,  Ludhiana">
<img class="example-image" src="images/news_award/Aaj_Di_Awaz_9_October_16_Page-4.jpg" >
<p><strong>Aaj Di Awaz</strong><br>09th Oct 2016<br> Ludhiana</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Pehredaar_11_October_16_Page-2.jpg" data-lightbox="example-set" title="11st Oct 2016,  Ludhiana">
<img class="example-image" src="images/news_award/Pehredaar_11_October_16_Page-2.jpg" >
<p><strong>Pehredaar</strong><br>11st Oct 2016<br> Ludhiana</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Sach_Di_Patari_11_October_16_Page-4.jpg" data-lightbox="example-set" title="11st Oct 2016,  Ludhiana">
<img class="example-image" src="images/news_award/Sach_Di_Patari_11_October_16_Page-4.jpg" >
<p><strong>Sach Di Patari</strong><br>11st Oct 2016<br> Ludhiana</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Daily_Post_13_October_16_Page-2.jpg" data-lightbox="example-set" title="13th Oct 2016,  Ludhiana">
<img class="example-image" src="images/news_award/Daily_Post_13_October_16_Page-2.jpg" >
<p><strong>Daily Post</strong><br>13th Oct 2016<br> Ludhiana</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Hindustan_Times_13_October_16_Page-2.jpg" data-lightbox="example-set" title="13th Oct 2016,  Ludhiana">
<img class="example-image" src="images/news_award/Hindustan_Times_13_October_16_Page-2.jpg" >
<p><strong>Hindustan Times</strong><br>13th Oct 2016<br> Ludhiana</p>
</a></li>   
 <li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Dainik_Bhaskar_13_October_16_Page-5.jpg" data-lightbox="example-set" title="13th Oct 2016,  Ludhiana">
<img class="example-image" src="images/news_award/Dainik_Bhaskar_13_October_16_Page-5.jpg" >
<p><strong>Dainik Bhaskar</strong><br>13th Oct 2016<br> Ludhiana</p>
</a></li>   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Amar-Ujala.jpg" data-lightbox="example-set" title="Amar Ujala,25th Dec 2015,Kanpur">
<img class="example-image" src="images/news_award/Amar-Ujala.jpg" >
<p><strong>Amar Ujala</strong><br>25th Dec 2015<br>Kanpur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/DAMS_25_Dec_Final.jpeg" data-lightbox="example-set" title="25th Dec 2015">
<img class="example-image" src="images/news_award/DAMS_25_Dec_Final.jpeg" >
<p><strong></strong><br>25th Dec 2015<br></p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Hindustan.jpg" data-lightbox="example-set" title="Hindustan,25th Dec 2015,kanpur">
<img class="example-image" src="images/news_award/Hindustan.jpg" >
<p><strong>Hindustan</strong><br>25th Dec 2015<br>kanpur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/DAMS_Indian_Express.png" data-lightbox="example-set" title="Indian Express,25th Dec 2015,Kanpur">
<img class="example-image" src="images/news_award/DAMS_Indian_Express.png" >
<p><strong>Indian Express</strong><br>25th Dec 2015<br>Kanpur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/mail_today_8_de_DAMS.jpeg" data-lightbox="example-set" title="Mail Today,8th Dec 2015,Delhi">
<img class="example-image" src="images/news_award/mail_today_8_de_DAMS.jpeg" >
<p><strong>Mail Today</strong><br>8th Dec 2015<br>Delhi</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Rashtriya_Sahara.jpg" data-lightbox="example-set" title="Rashtriya Sahara,25th Dec 2015,Kanpur">
<img class="example-image" src="images/news_award/Rashtriya_Sahara.jpg" >
<p><strong>Rashtriya Sahara</strong><br>25th Dec 2015<br>Kanpur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/The_Pioneer_Hindi.jpg" data-lightbox="example-set" title="Pioneer Hindi,25th Dec 2015,Kanpuri">
<img class="example-image" src="images/news_award/The_Pioneer_Hindi.jpg" >
<p><strong> Pioneer Hindi</strong><br>25th Dec 2015<br>Kanpur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/The_Pioneer_Kanpur.jpg" data-lightbox="example-set" title="The Pioneer,25th Dec 2015,Kanpur">
<img class="example-image" src="images/news_award/The_Pioneer_Kanpur.jpg" >
<p><strong>The Pioneer</strong><br>25th Dec 2015<br>Kanpur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/Dabang_Dunia_bhopal.jpg" data-lightbox="example-set" title="Daband Dunia,  28th Jan 2015, Bhopal">
<img class="example-image" src="images/news_award/Dabang_Dunia_bhopal.jpg" >
<p><strong>Daband Dunia</strong><br>28th Jan 2015<br>Bhopal</p>
</a></li>
<li class="publication-box-new nw_cntr" >
<a class="example-image-link" href="images/news_award/TheHitavadaBhopal28Jan2015.jpg" title="The Hitavada, 28th Jan 2015, Bhopal">
<img class="example-image" src="images/news_award/TheHitavadaBhopal28Jan2015.jpg" >
<p><strong>The Hitavada</strong><br>28th Jan 2015<br>Bhopal</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/ThePioneerBhopal28Jan2015.jpg" title="The Pioneer, 28th Dec 2015, Bhopal">
<img class="example-image" src="images/news_award/ThePioneerBhopal28Jan2015.jpg">
<p><strong>The Pioneer</strong><br>28th Dec 2015<br>Bhopal</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-3.jpg" data-lightbox="example-set" title="Rajasthan Patrika, 25th Dec 2014, Jaipur">
<img class="example-image" src="images/news_award/nws-3.jpg">
<p><strong>Rajasthan Patrika</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-2.jpg" title="Dainik Bhor, 25th Dec 2014, Jaipur">
<img class="example-image" src="images/news_award/nws-2.jpg">
<p><strong>Dainik Bhor</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-4.jpg" title="Aaspass, 25th Dec 2014, Jaipur">
<img class="example-image" src="images/news_award/nws-4.jpg">
<p><strong>Aaspass</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-1.jpg" title="Hukumnama Samachar, 25th Dec 2014, Jaipur">
<img class="example-image" src="images/news_award/nws-1.jpg">
<p><strong style="white-space:nowrap">Hukumnama Samachar</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-5.jpg" title="Dainik Navajyoti, 25th Dec 2014, Jaipur">
<img class="example-image" src="images/news_award/nws-5.jpg">
<p><strong>Dainik Navajyoti</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-6.jpg" title="Daily News, 26th Dec 2014, Jaipur">
<img class="example-image" src="images/news_award/nws-6.jpg">
<p><strong>Daily News</strong><br>26th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-7.jpg" title="Hindustan Times, 18th Dec 2014, Delhi/NCR">
<img class="example-image" src="images/news_award/nws-7.jpg">
<p><strong>Hindustan Times</strong><br>18th Dec 2014<br>Delhi/NCR</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-8.jpg" title="The Statesman, 12th Dec 2014, Kolkata">
<img class="example-image" src="images/news_award/nws-8.jpg">
<p><strong>The Statesman</strong><br>12th Dec 2014<br>Kolkata</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-9.jpg" title="The Hindu, 15th Dec 2014, New Delhi">
<img class="example-image" src="images/news_award/nws-9.jpg">
<p><strong>The Hindu</strong><br>15th Dec 2014<br>New Delhi</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/chandi-1.jpeg" title=" AAJ SAMAJ, Chandigadh">
<img class="example-image" src="images/news_award/chandi-1.jpeg">
<p><strong>Aaj Samaj</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/chandi-2.jpeg" title="Dainik Bhaskar, Chandigadh">
<img class="example-image" src="images/news_award/chandi-2.jpeg">
<p><strong>Dainik Bhaskar</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/chandi-3.jpeg" title=" HIM PRABHA, Chandigadh">
<img class="example-image" src="images/news_award/chandi-3.jpeg">
<p><strong>Him Prabha</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/chandi-4.jpeg" title="NAYA LOKYUG, New Delhi">
<img class="example-image" src="images/news_award/chandi-4.jpeg">
<p><strong>Naya Lokyug</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/chandi-5.jpeg" title="THE INDIAN EXPRESS, Chandigadh">
<img class="example-image" src="images/news_award/chandi-5.jpeg">
<p><strong>The Indian Express</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/chandi-6.jpeg" title="PUNJAB KESARI, Delhi/NCR">
<img class="example-image" src="images/news_award/chandi-6.jpeg">
<p><strong>Punjab Keshari</strong><br>Chandigadh</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-11.jpg" title="Adhikar, 25th Dec 2014, Jaipur">
<img class="example-image" src="images/news_award/nws-11.jpg">
<p><strong>Adhikar</strong><br>25th Dec 2014<br>Jaipur</p>
</a></li>
<li class="publication-box-new nw_cntr">
<a class="example-image-link" href="images/news_award/nws-15.jpg" title="Metro Bytes, 26th Dec 2014, Jaipur">
<img class="example-image" src="images/news_award/nws-15.jpg">
<p><strong>Metro Bytes</strong><br>26th Dec 2014<br>Jaipur</p>
</a></li>











</ul></div></div></div></div></div>
<div id="online">
<div class="satellite-content">
<div class="idams-box1">
<div class="image-row">
<div class="gallery-list1 display_block">
    <ul>
        <li class="publication-box-new nw_cntr nw_cntr_video">
          <a class="example-image-link" href="https://www.youtube.com/embed/fDjs8NZaiTE"  target="_blank" title="DAMS SKY featured on Zee Business ">
          <img class="example-image" src="images/Zee_1.png">
          <p><strong>DAMS SKY featured on Zee Business </strong></p>
        </a></li>
        <li class="publication-box-new nw_cntr nw_cntr_video">
        <a class="example-image-link" href="https://www.youtube.com/embed/ukllyhy1NkM" target="_blank" title="Story done on the scope and preparations for PG in medical sciences">
        <img class="example-image" src="images/DD_2.png">
        <p><strong>Story done on the scope and preparations for PG in medical sciences</strong></p>
        </a></li>
        
    </ul>  
</div></div></div></div></div></div></div></article></div></aside>
<aside class="gallery-right">
<?php include 'about-right-accordion.php'; ?>
<?php include 'enquiryform.php'; ?>
</aside></section></div></div></section>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/html5.js"></script> 
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="js/registration.js"></script> 
<script type="text/javascript" src="js/add-cart.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.6.js"></script>
<script type="text/javascript">
$(function() {
$('#gallery1 a').lightBox();
});
$(function() {
$('#gallery2 a').lightBox();
});
$(function() {
$('#gallery3 a').lightBox();
});
$(function() {
$('#gallery4 a').lightBox();
});
function blockchange1(val, ids)
{
var str=String(ids);
var galarr = ["gallery1", "gallery2", "gallery3", "gallery4"];
var countul1 = $('#gallery1 > ul').size();
var countul2 = $('#gallery2 > ul').size();
var countul3 = $('#gallery3 > ul').size();
var countul4 = $('#gallery4 > ul').size();
if(ids=="gallery1")
{
for(var g=1;g<=countul1;g++)
{
if(val==g)
{
$("#ul"+g).fadeIn('slow','swing');
$("#ul"+g).removeClass('display_none');
$("#ul"+g).addClass('display_block');
$("#ula"+g).addClass("active-dot"); 
}
else
{
$("#ul"+g).fadeOut('fast','linear');
$("#ul"+g).removeClass('display_block');
$("#ul"+g).addClass('display_none');
$("#ula"+g).removeClass("active-dot"); 
}
}
}
else if(ids=="gallery2")
{
for(var g=4;g<=(countul2+3);g++)
{
if(val==g)
{
$("#ul"+g).fadeIn('slow','swing');
$("#ul"+g).removeClass('display_none');
$("#ul"+g).addClass('display_block');
$("#ula"+g).addClass("active-dot"); 
}
else
{
$("#ul"+g).fadeOut('fast','linear');
$("#ul"+g).removeClass('display_block');
$("#ul"+g).addClass('display_none');
$("#ula"+g).removeClass("active-dot"); 
}
}
}
else if(ids=="gallery3")
{
for(var g=7;g<=(countul3+6);g++)
{
if(val==g)
{
$("#ul"+g).fadeIn('slow','swing');
$("#ul"+g).removeClass('display_none');
$("#ul"+g).addClass('display_block');
$("#ula"+g).addClass("active-dot"); 
}
else
{
$("#ul"+g).fadeOut('fast','linear');
$("#ul"+g).removeClass('display_block');
$("#ul"+g).addClass('display_none');
$("#ula"+g).removeClass("active-dot"); 
}
}
}
else if(ids=="gallery4")
{
for(var g=10;g<=(countul4+9);g++)
{
if(val==g)
{
$("#ul"+g).fadeIn('slow','swing');
$("#ul"+g).removeClass('display_none');
$("#ul"+g).addClass('display_block');
$("#ula"+g).addClass("active-dot"); 
}
else
{
$("#ul"+g).fadeOut('fast','linear');
$("#ul"+g).removeClass('display_block');
$("#ul"+g).addClass('display_none');
$("#ula"+g).removeClass("active-dot"); 
}
}
}
};
function divchange(val)
{
var countdiv= $('#changerdiv > li').size();
for(var d=1;d<=countdiv;d++)
{
if(val==d)
{
$("#divgal"+d).addClass("present");
$("#gallery"+d).fadeIn('slow','swing');
$("#gallery"+d).removeClass('display_none');
$("#gallery"+d).addClass('display_block');
$("#galdots"+d).fadeIn('slow','swing');
$("#galdots"+d).removeClass('display_none');
$("#galdots"+d).addClass('display_block');
}
else
{
$("#divgal"+d).removeClass("present");
$("#gallery"+d).fadeOut('fast','linear');
$("#gallery"+d).removeClass('display_block');
$("#gallery"+d).addClass('display_none');
$("#galdots"+d).fadeOut('fast','linear');  
$("#galdots"+d).removeClass('display_block');
$("#galdots"+d).addClass('display_none');    
}
}
}
</script>
</body>
</html>