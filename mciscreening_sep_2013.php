<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS Achievment, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="mci-banner">
      <?php include 'mci-big-nav.php'; ?>
      <aside class="banner-left banner-l-heading">
        <h2>MCI Screening Courses</h2>
        <h3>Best teachers at your doorstep <span>India's First Satellite Based PG Medical Classes</span></h3>
      </aside>
      <?php include'mci-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"> <a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a> </span>
        <ul>
          <li class="bg_none"><a href="mci_screening.php" title="MCI Screening ">MCI Screening </a></li>
          <li><a title="Achievement" class="active-link">Achievement</a></li>
        </ul>
      </div>
      <section class="video-container">
        <div class="achievment-left">
          <div class="achievment-left-links">
            <ul id="accordion">
              <li class="light-blue border_none" id="accor1" onClick="ontab('1');">
              <span id="accorsp1" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
              <ol class="achievment-inner display_block" id="aol1">
                <li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="mciscreening_sep_2013.php" title="MCI Screening September">MCI Screening September</a></li>
                <li><span class="mini-arrow">&nbsp;</span><a href="mciscreening_mar_2013.php" title="MCI Screening March">MCI Screening March</a></li>
              </ol>
              </li>
              <li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
                <ol class="achievment-inner display_none" id="aol2">
                  <li><span class="mini-arrow">&nbsp;</span><a href="mciscreening_sep_2012.php" title="MCI Screening September">MCI Screening September</a></li>
                </ol>
              </li>
            </ul>
          </div>
        </div>
        <aside class="gallery-left respo-achievement right_pad0">
          <div class="inner-left-heading">
            <h4>MCI Screening September 2013</h4>
            <section class="showme-main">
              <ul class="idTabs idTabs1">
                <li><a href="#jquery" class="selected" title="MCI Screening September 2013">MCI Screening September 2013</a></li>
                <li><a href="#official" title="Interview">Interview</a></li>
              </ul>
              <div id="jquery">
                <article class="interview-photos-section">
                  <ul class="main-students-list">
                    <li class="tp-border">
                      <div class="students-box border_left_none"> <img src="images/students/AYAN-SARKAR.jpg" alt="Dr. AYAN SARKAR" title="Dr. AYAN SARKAR" />
                        <p><span>Dr. AYAN SARKAR</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/E.LAKSHMAN-KUMAR.jpg" alt="Dr. E.LAKSHMAN KUMAR" title="Dr. E.LAKSHMAN KUMAR" />
                        <p><span>Dr. E.LAKSHMAN KUMAR</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/VINOD-NASPURI.jpg" alt="Dr. VINOD NASPURI" title="Dr. VINOD NASPURI" />
                        <p><span>Dr. VINOD NASPURI</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/NEELI-NEELIMA-RANI.jpg" alt="Dr. NEELI NEELIMA RANI" title="Dr. NEELI NEELIMA RANI" />
                        <p><span>Dr. NEELI NEELIMA RANI</span></p>
                      </div>
                    </li>
                    <li>
                      <div class="students-box border_left_none"> <img src="images/students/ARYA-TRILOK-SANKHLA.jpg" alt="Dr. ARYA TRILOK SANKHLA" title="Dr. ARYA TRILOK SANKHLA" />
                        <p><span>Dr. ARYA TRILOK SANKHLA</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/SANKI-AGRAWAL.jpg" alt="Dr. SANKI AGRAWAL" title="Dr. SANKI AGRAWAL" />
                        <p><span>Dr. SANKI AGRAWAL</span></p>
                      </div>
                      <div class="students-box"> <img src="images/students/VAISHNAVI-DUTTA-MISHRA.jpg" alt="Dr. VAISHNAVI DUTTA MISHRA" title="Dr. VAISHNAVI DUTTA MISHRA" />
                        <p><span>Dr. VAISHNAVI DUTTA MISHRA</span></p>
                      </div>
                      <div class="students-box"> 
                        <!--<img src="images/students/SHANTANU-KUMAR-GUPTA.jpg" alt="Dr. SHANTANU KUMAR GUPTA" title="Dr. SHANTANU KUMAR GUPTA" />
<p><span>Dr. SHANTANU KUMAR GUPTA</span> Rank: -</p>--> 
                      </div>
                    </li>
                  </ul>
                  <br style="clear:both;" />
                  <br style="clear:both;" />
                  <div class="schedule-mini-series"> <span class="mini-heading">MCI Screening September - Result 2013</span>
                    <div class="schedule-mini-top"> <span class="one-part">S. No.</span> <span class="two-part">Student Name</span> <span class="three-part">&nbsp;</span> </div>
                    <div class="schedule-mini-content">
                      <ul>
                        <li class="border_none"><span class="one-parts">1</span> <span class="two-parts schedule-left-line">Dr. AYAN SARKAR</span> </li>
                        <li><span class="one-parts">2</span> <span class="two-parts schedule-left-line">Dr. AKASH KAPPULA</span> </li>
                        <li><span class="one-parts">3</span> <span class="two-parts schedule-left-line">Dr. VINOD NASPURI</span> </li>
                        <li><span class="one-parts">4</span> <span class="two-parts schedule-left-line">Dr. ARYA TRILOK SANKHLA</span> </li>
                        <li><span class="one-parts">5</span> <span class="two-parts schedule-left-line">Dr. VAISHNAVI DUTTA MISHRA</span> </li>
                        <li><span class="one-parts">6</span> <span class="two-parts schedule-left-line">Dr. DIBYASHREE SAHU</span> </li>
                        <li><span class="one-parts">7</span> <span class="two-parts schedule-left-line">Dr. E.LAKSHMAN KUMAR</span> </li>
                        <li><span class="one-parts">8</span> <span class="two-parts schedule-left-line">Dr. NEELI NEELIMA RANI</span> </li>
                        <li><span class="one-parts">9</span> <span class="two-parts schedule-left-line">Dr. SANKI AGRAWAL</span> </li>
                        <li><span class="one-parts">&nbsp;</span> <span class="two-parts schedule-left-line" style="color:#000;">And many more...</span> </li>
                      </ul>
                    </div>
                  </div>
                </article>
              </div>
              <div id="official">
                <article class="interview-photos-section">
                  <div class="achievment-videos-section">
                    <ul>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/cC2a-En6XK4?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Sonam Verma</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/VK0ol9h2h8c?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Rup Narayan Jha</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/YJaxKtFiAMA?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Rashad Rahib</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                        <div class="video-box-1">
                          <iframe width="100%" height="247" src="//www.youtube.com/embed/zvRSvxwOfwc?wmode=transparent" class="border_none"></iframe>
                          <div class="video-content">
                            <p><strong>Name:</strong> <span>Dr. Rakesh Kela</span></p>
                            <p>&nbsp;</p>
                            <div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </article>
              </div>
            </section>
          </div>
        </aside>
      </section>
    </div>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
<!--<script type="text/javascript">
function ontab(val) {
	var len = $("#accordion > li").size();
    for(i=1;i<=len;i++)
	{
		if(val==i)
		{
			if(i==1)
			{
			  $('#accorsp'+i).removeClass('bgspanblk');
		      $('#aol'+i).slideDown();
		      $('#accorsp'+i).addClass('bgspan');
		      $('#accor'+i).addClass('light-blue');
			}
			else
			{
			  $('#accorsp'+i).removeClass('bgspanblk');
		      $('#aol'+i).slideDown();
		      $('#accorsp'+i).addClass('bgspan');
		      $('#accor'+i).addClass('light-blue-inner');				
			}
		}
		else
		{ 
		    if(i==1)
		    {
			  $('#accorsp'+i).removeClass('bgspan');
		      $('#aol'+i).slideUp();
		      $('#accorsp'+i).addClass('bgspanblk');
		      $('#accor'+i).removeClass('light-blue');
		    }
		    else
		    {
			  $('#accorsp'+i).removeClass('bgspan');
		      $('#aol'+i).slideUp();
		      $('#accorsp'+i).addClass('bgspanblk');
		      $('#accor'+i).removeClass('light-blue-inner');
		    }
		}
	}
};
</script>-->
</body>
</html>