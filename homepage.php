<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>DAMS, PG Medical Coaching Centre, New Delhi, India, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href=images/favicon.ico type=image/x-icon />
<link rel=icon href=images/favicon.ico type=image/x-icon />
<link href=css/style.css rel=stylesheet type=text/css />
<link href=css/responcive_css.css rel=stylesheet type=text/css />
<!--<link href=css/stylenew.css rel=stylesheet type=text/css />-->
<link href=css/stylehome.css rel=stylesheet type=text/css />
<link href=css/screenhome.css rel=stylesheet type=text/css />
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body class="inner-bg">
<?php include 'registration.php';?>
<?php include 'social-icon.php'; ?>
<?php include 'headernew.php'; ?>
<?php echo include 'dao.php'; ?>
<?php 
    $Dao = new dao();
	$homeBanneeSql  = $Dao->getHomeBanner();
	//$projectName = 'damswebadmin' ; // for local Or li.gingertab.com 
       $projectName = 'damsCMS' ;    // for live
    //  $new = "/damswebsiteNew" ;
?> 
    <section class="sld_section" >
<div class="demo-centering">
    
    <div id="sliderB" class="slider">

    <?php while ( $rowHomeBanner = mysql_fetch_object($homeBanneeSql)): ?>
        <div>
                <img src="<?php echo 'https://'.$_SERVER['HTTP_HOST'].'/'.$projectName.'/images/bannerImage/'.$rowHomeBanner->ID.'.'.$rowHomeBanner->BANNER_PATH ; ?>"/>
                <div class="main_wrappr1 slidecont">
                    <div class="bnr_ovr_txt">
                        <div class="bnr_over_info">
                            <h2 class="bold_font"><?php echo  html_entity_decode(rawurldecode($rowHomeBanner->BANNER_NAME)); ?></h2>
                            <span class="test_dateicon"><?php echo rawurldecode($rowHomeBanner->BANNER_TEXT); ?></span> 
                        </div>
                    </div>
                </div>
            </div>
   <?php endwhile; ?>         
        

        
     </div>
   </div>
   
</div> 
 </section>
 <section class="cont">
     <div class="shadow"></div>
 <div class="main_wrappr course"> 
     <div class="course_panel">
         <?php  
            $homePageCourse = $Dao->getHomeCourse();
            while ($rowHomeCourse = mysql_fetch_object($homePageCourse)): 
                $getCourseNav = $Dao->getCourseNav($rowHomeCourse->COURSE_ID);
                ?>
         <div class="course_div">
             <div class="course_detail">
                 <div class="course_img"> 
                     <img src="<?php echo 'https://'.$_SERVER['HTTP_HOST'].'/'.$projectName.'/images/courseImages/'.$rowHomeCourse->COURSE_ID.'.'.$rowHomeCourse->IMG_EXT ; ?>"/>

                 </div>
                 <div class="course_des">
                     <h3><?php echo urldecode(html_entity_decode($rowHomeCourse->COURSE_NAME)); ?></h3>    
                     <p class="course_desp"><?php echo rawurldecode($rowHomeCourse->CONTENT_HOME); ?></p>
                 </div>
                 <div class="course_viewmore">
                     <a href="<?php echo urldecode($rowHomeCourse->URL_LINK).'?c='.$rowHomeCourse->COURSE_ID.'&n='.$getCourseNav[0][0]; ?>"> <span>READ MORE</span></a>
                 </div> 
             </div>
         </div>   
         <?php endwhile; ?>
         
         <div class="course_div ">
             <div class="course_detail uk">
                 <div class="course_img">
                     <img src="images/Courses_5.jpg"/>
                 </div>
                 <div class="course_des ukdetail">
                     <h3>UK COURSES </h3>    
                     <ul>

                         <li><a class="course_name" href="https://mrcp.damsdelhi.com/index.php"><i></i><span>MRCP</span></a><div class="vmbot1"> <a class="more_btn" href="https://mrcp.damsdelhi.com/index.php">READ MORE</a></div></li>
                         <li><a class="course_name" href="https://mrcog.damsdelhi.com/index.php"><i></i><span>MRCOG</span></a><div class="vmbot1"> <a class="more_btn" href="https://mrcog.damsdelhi.com/index.php">READ MORE</a></div></li>
                         <li><a class="course_name" href="https://plab.damsdelhi.com/index.php"><i></i><span>PLAB</span></a><div class="vmbot1"><a class="more_btn" href="https://plab.damsdelhi.com/index.php">READ MORE</a></div></li>
                         <li><a class="course_name" href="https://mrcgp.damsdelhi.com/index.php" class="last"><i></i><span>MRCGP</span></a><div class="vmbot1"> <a class="more_btn" href="https://mrcgp.damsdelhi.com/index.php">READ MORE</a></div></li>

                     </ul>
        
                 </div>
                 
             </div>
         </div>
          <div class="course_div ">
         <div class="course_detail uk">
             <div class="course_img">
                 <img src="images/9.jpg"/>
             </div>
             <div class="course_des ukdetail">
                 <h3>CLINICAL COURSES </h3>    
                 <ul>
                     <li><a class="course_name" href=""><i></i><span>Bls/Acls </span></a><div class="vmbot1"> <a class="more_btn" href="">READ MORE</a></div></li>

                 </ul>
             </div>

         </div>
         </div>
     </div>
 </div>
<!--     <div class="shadow1"></div>-->
      </section>
    <section class="cont">

      <div class="shadow"></div>  
     <div class="main_wrappr info">
  <div id="push" class="mnu_icon_lft">
   <span class="horizontal1"></span>
   <span class="horizontal2"></span>
   <span  class="horizontal3" style="margin-bottom:0;"></span>
 </div>
         
         
 <!-- start:Aziz   --> 
      
<div id="slide-menu">
    <div class="leftsection">
        <div id="close"> <h3 class="inr_title"><i class="spritehome_new usefulllink"></i>Useful Link</h3></div>
        <div id='cssmenu'>
            <ul>    
                <?php
                    $getUseFullLink = $Dao->getUseFullLink() ;
                    while ($rowUseLink = mysql_fetch_object($getUseFullLink)):
                       
                        $subLinkText = $rowUseLink->SUB_LINK_TEXT;
                        $subLinkTextarr = explode("#", $subLinkText);  
                       
                        $subLink = $rowUseLink->SUB_LINK;
                        $subLinkarr = explode("#", $subLink);  
                       
                       if(count($subLinkTextarr) > 0 && $subLinkTextarr[0] !=="" && $subLinkTextarr[0] !=="NULL"){
                            echo "<li class ='active has-sub'><a href='".$rowUseLink->URL_LINK."'><span>".rawurldecode($rowUseLink->LINK_TEXT)."</span></a><ul>";
                            
                            for ( $i=0; $i<count($subLinkTextarr); $i++){
                            echo "<li class = 'has-sub'><a href='".$subLinkarr[$i]."'>".rawurldecode($subLinkTextarr[$i])."</a></li>";
                            }
                            
                        echo "</ul>";
                       }else{
                           echo "<li><a href='".$rowUseLink->URL_LINK."'>".rawurldecode($rowUseLink->LINK_TEXT)."</a>";   
                       }
 
                      echo "</li>";
                   endwhile;   
                ?>
            </ul>
        </div>
   </div></div>

 <!-- End:Aziz   --> 
         
     <div class="rgtsection">
        <div class="sec_cont">
            <div class="news_sec ">
                <div class="news_detail">
                    <div> <h3 class="inr_title"><i class="spritehome_new news"></i>News & Events</h3></div>
                    <?php 
                        $getAllNewsAndEvent = $Dao->getAllNewsAndEvent();
                        while($rows = mysql_fetch_object($getAllNewsAndEvent)):
                        $dateArr    = explode(" ",date("jS M", strtotime($rows->DATE)));
                       
                            $retWord = $rows->NEWS_TEXT;
                            
                            $array = explode(" ", $retWord);
                            if (count($array)<=6){
                                    $retWord = $rows->NEWS_TEXT;
                                }else{
                                     array_splice($array, 6);
                                     $retWord = implode(" ", $array)." ...";
                                }
                         
                    ?>
                    <div class="news_cont" >
                        <div class="news_cont1" >
                            <div class="news_date_div" >
                                <div class="news_month" ><span class="month"style=""><?php echo $dateArr[1]; ?></span></div>
                                <div class="news_date"  ><span class="date"style=""><?php echo $dateArr[0]; ?><sup style="font-size:11px;"></sup></span></div>
                            </div>
                            <div class="news_info">
                                
                                <div class="news_cont_detail"><?php echo  $retWord; ?></div>
                                <div class="rade_more1"><span class="read_more" ><a href="<?php echo 'https://'.$_SERVER['HTTP_HOST'].$new.'/home-news-detail.php?&id='.$rows->ID ; ?>" >READ MORE</a></span></div>
                            </div>
                        </div>
                    </div>
                   <?php  endwhile; ?> 
                    

<div  class="view_more"><a class="view_more_a" href="<?php echo 'https://'.$_SERVER['HTTP_HOST'].$new.'/home-news.php' ; ?>">View More<span></span></a></div>

                </div>                   
            </div>
            <div class="enquery_form">
                <div class="form_detail">
                 <div> <h3 class="inr_title"><i class="spritehome_new form"></i>Quick Enquiry Form</h3></div>
                 <div class="enquiry_content_main">
<div class="enquiry_content">
<div class="enquiry_content_more">
<div class="career_box">
<div class="right_ip_1">
<input id="quickEnquiry1" name="userName" class="career_inp_1" value="Your Name" onfocus="if(this.value=='Your Name')this.value=''" onblur="if(this.value=='')this.value='Your Name'" type="text">
</div>
</div>
<div class="career_box">
<div class="right_ip_1">
<input id="quickEnquiry2" name="userEmail" class="career_inp_1" value="Email Address" onfocus="if(this.value=='Email Address')this.value=''" onblur="if(this.value=='')this.value='Email Address'" type="text">
</div>
</div>
<div class="career_box">
<div class="right_ip_1">
<input id="quickEnquiry3" name="userMobile" class="career_inp_1" value="Phone Number" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" onfocus="if(this.value=='Phone Number')this.value=''" onblur="if(this.value=='')this.value='Phone Number'" type="text">
</div>
</div>
<div class="career_box">
<div class="right_ip_1">
<select name="userCourse" class="career_select_input_1" id="quickEnquiry4">
<option value="">Select Course</option>
<option>DRTP</option>
<option>MD/MS ENTRANCE</option>
<option>MCI SCREENING</option>
<option>MDS QUEST</option>
<option>USMLE EDGE</option>
<option>MRCP</option>
<option>MRCOG</option>
<option>PLAB</option>
<option>MRCGP</option>
</select>
</div>
</div>
<div class="career_box">
<div class="right_ip_1">
    <select name="userCentre" class="career_select_input_1" id="quickEnquiry5">
        <option value="">Centre Interested</option>
        <option>Ahmedabad</option><option>Amritsar</option>
        <option>Aurangabad</option><option>Bengaluru</option><option>Bhopal</option><option>Bhuvneshwar</option><option>Chandigarh</option>
        <option>Chennai</option><option>Guwahati</option><option>Hyderabad</option><option>Indore</option><option>
            Jaipur</option><option>Jodhpur</option><option>Kanpur </option><option>Kolkata</option><option>Lucknow</option><option>
            Mangalore</option><option>Manipal</option><option>Nagpur</option><option>Patna</option><option>Pune</option>
        <option>Raipur</option><option>Rohtak</option><option>Surat</option><option>Udaipur</option><option>Vadodara</option>
        <option>Vijayawada</option><option>Agartala</option><option>Bathinda</option><option>Belgaum</option><option>Bikaner</option>
        <option>Bilaspur</option><option>Davangere</option><option>Dhule</option><option>Gorakhpur</option><option>Haldwani</option><option>
            Hyderabad</option><option>Kota</option><option>Latur</option><option>Lucknow</option><option>Ludhiana</option><option>
            Mangalore</option><option>Miraj</option><option>Mumbai</option<option>Pune</option><option>Rohtak</option><option>
            Vijayawada</option><option>Vishakapatnam</option><option>Agroha</option><option>Aligarh</option><option>Allahabad</option><option>
            Amravati</option><option>Berhampur</option><option>Bikaner</option><option>Cuttack</option><option>Warangal</option><option>
            Dhule</option><option>Faridkot</option><option>Goa</option><option>Gwalior</option><option>Kakinada</option><option>
            Karad</option><option>Karim Nagar </option><option>Khamam</option><option>Kolhapur</option><option>Kurnool</option>
        <option>Latur</option><option>Loni</option><option>Ludhiana</option><option>Meerut</option><option>Miraj</option><option>
            Patiala</option><option>Solapur</option><option>Talegaon</option><option>Tirupati</option><option>Varanasi</option><option>
            Vishakhapatnam</option><option>Tanda</option><option>Maysore</option><option>Agra</option><option>Bagalkot</option><option>Hubli</option><option>
            Dehradun</option><option>Shimla</option><option>New Delhi (Karol Bagh)</option><option>New Delhi (Gautam Nagar)</option><option>Nasik</option>
        <option>Jammu</option><option>Agroha</option><option>Dibrugrah</option><option>Gulbarga</option><option>Aligarh</option><option>
            Manipal</option><option>Cochin</option><option>Ahemadabad</option><option>Aurangabad</option><option>
            Bareilly</option><option>Chennai</option><option>Cochin</option><option>Dehradun</option><option>Saifai</option><option>
            Jaipur</option><option>Jagdal Pur</option><option>Jabalpur</option><option>Kanpur</option><option>Kohlapur</option>
        <option>Kolkata</option><option>Mysore</option><option>Meerut</option><option>Pondicherry</option><option>Ranchi</option><option>
            Mumbai</option><option>JALANDHAR</option><option>RABINDRANATH FOUNDATION</option><option>Srinagar J&amp;K</option><option>
            Tamilnadu</option><option>Siliguri</option><option>Solapur</option><option>Ahemadnagar </option><option>mphal</option><option>
            Ajmer</option><option>Satara</option><option>Srinagar UK</option><option>DAMS (Delhi Accademy of Medical Sciences Pvt.Ltd.)</option><option>
            Silchar</option><option>DAMS Jorhat</option><option>Nanded</option><option>Rewa</option>
    </select></div></div>
    <div class="right-ip-1">
<textarea id="quickEnquiry6" class="career-inp-1" placeholder="Write your Query here" rows="1" name="userMessage"></textarea>
</div>
    
    
<div class="career_box1">
<div class="right_ip_1">
<div class="career_box"><div class="right_ip_1">
<div class="submit_enquiry"><a href="javascript:void(0)" title="Submit" onclick="validateQuickEnquiry()"> SUBMIT</a></div><div class="error_msg_box" id="quickEnquiryerror" style="display:none">
Please enter correct email address.</div></div></div></div></div>
<div class="enquiry_bottom"></div>
</div>
                </div>
             </div>
                </div>
                 </div>   
            </div>
                
        <div class="sec_cont">   

           
            <div class="news_sec ">
                <div class="news_detail topper">
                    <div> <h3 class="inr_title"><i class="spritehome_new topper"></i>Toppers Talk</h3></div>
                    <div class="video_cont">
                    <div id="owl-demo4" class="owl-carousel">
                         <?php 
                       $topperTalk = $Dao->getAllToperTalk();
                       while( $rowTopper = mysql_fetch_object($topperTalk)):  ?>
                        <div class="item">
                            <div class="left_prt">
                                <a target="_blank" href="<?php echo rawurldecode($rowTopper->URL);?>">    <img src="<?php echo 'https://'.$_SERVER['HTTP_HOST'].'/'.$projectName.'/images/youTubeThumbs/'.$rowTopper->ID.'.jpg' ; ?>"/>

                                </a>
                            </div>
                            <div class="rgt_prt">
                                <div class="stu_name">
                                    <p><?php echo $rowTopper->STUDENT_NAME ;?></p>
                                    <p>Rank-<?php echo $rowTopper->RANK ; ?></p>
                                </div>
                                <!--<div class="stu_info"><?php  //rawurldecode($rowTopper->TEXT); ?></div> --> 
                                <p> <?php echo '"'.substr(rawurldecode($rowTopper->TEXT),0,100).'"'; ?> </p>   
                            </div>
                        </div>
                       <?php endwhile; ?> 
                   </div>

                    </div>
                </div>                   
            </div>
            
            <div class="enquery_form">
                <div class="form_detail gallary">
                 <div> <h3 class="inr_title"><i class="spritehome_new photo"></i>Photo Gallery</h3></div>

                 
                <div class="inr_cnttr">
                <div class="arwsldcntr">
                    <div id="owl-demo3" class="owl-carousel">
                      <?php  $getAllPhotoGal = $Dao->getAllHomePhotoGall();  
                            while($rowPhotoGall = mysql_fetch_object($getAllPhotoGal)):
                        ?>  
                        <div class="item">
                            <div class="left_prt">
                                <img src="<?php echo 'https://'.$_SERVER['HTTP_HOST'].'/'.$projectName.'/images/photoGall/'.$rowPhotoGall->ID.'.'.$rowPhotoGall->PHOTO_PATH ; ?>"/>
                            </div>
                            <div class="rgt_prt">
                                <div class="stu_name">
                                    <p><?php echo $rowPhotoGall->STUDENT_NAME; ?></p>
                                    <p>Rank-<?php echo $rowPhotoGall->RANK;?></p>
                                </div>
                                <div class="stu_info"><?php echo rawurldecode($rowPhotoGall->PHOTO_TEXT);?></div>

                            </div>
                        </div>
                        <?php endwhile; ?>

                    </div>
                </div>
            </div>
                 
                 
                </div>
             </div>
     </div>
    
      
         </div>    </div>
    <div class="shadow1"></div>
       </section> 
    
    
<!--<section class="inner-banner">
<div class="wrapper">
<article class="dams-aboutus">
<aside class="banner-left banner-left-postion">
<h2>Number 1 Post Graduate Medical &amp;<br>Dental Test Prep Company in India </h2>
<h3 class="page_title">Since Last 15 Years </h3>
</aside></article></div></section> -->
<!--<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">
<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span><ul>
<li class="bg_none"><a title="About Us" class="active-link">About Us</a></li>
</ul></div>
<section class="event-container">
<aside class="gallery-left">
<div class="inner-left-heading responc-left-heading">

<?php// include 'enquiryform.php'; ?>
</aside></section></div> </div>
</section>-->
<?php include 'footernew.php'; ?>
<script type=text/javascript async src=js/html5.js></script>
<script type=text/javascript src=js/jquery-1.10.2.min.js></script>
<script type=text/javascript async src=js/registration.js></script>
<script type=text/javascript async src=js/add-cart.js></script>
<script type=text/javascript async src=js/script.js></script>
</body>

<script type="text/javascript" src="js/jquery.excoloSlider.js"></script>
<script type="text/javascript" src="js/jquery.excoloSlider.min.js"></script>
<script type="text/javascript">
	 $(function () {
            $("#sliderA").excoloSlider();
            $("#sliderB").excoloSlider();
        });

</script>
<script>
    $(document).ready(function() {
      $("#owl-demo3,#owl-demo4").owlCarousel({
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true

      });
      
      $("#topperTalks").owlCarousel({

      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true

      });
    });
    </script>
    <!--<script type="text/javascript" src="/js/jquery-uinew.js"></script>-->
<script type="text/javascript"src="js/owl.carousel.js"></script>
<script>
        $(document).ready(function () {
                  $("#push").click(function(){
                          if($(this).hasClass( "active" )){
                                   $(this).removeClass("active");   
                          } else {
                                   $(this).addClass("active");   
                          }
                  });
//                  $(".active").removeClass("active");
                 $('#push, #close').click(function () {
                var $navigacia = $('#slide-menu'),
                val = $navigacia.css('left') === '250px' ? '0px' : '250px';
                $navigacia.animate({
                           left: val
                           }, 300)
		
                });	
});
var button = document.getElementsByClassName("navButton");

button.click = function() {
button.setAttribute("class", "active");
button.setAttribute("src", "images/arrows/top_o.png");
}

</script> 
</html>
