<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>PG Medical Entrance Coaching Institute, AIPG(NBE/NEET) Pattern PG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<body class="inner-bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'coures-header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
<div class="wrapper">
<article class="md-ms-banner">
<div class="big-nav">
<ul>
<li class="face-face"><a href="course.php" title="Face To Face Classes">Face To Face Classes</a></li>
<li class="satelite-b"><a href="satellite-classes.php" title="Satelite Classes">Satelite Classes</a></li>
<li class="t-series"><a href="test-series.php" title="Test Series">Test Series</a></li>
<li class="a-achievement"><a href="aiims_nov_2013.php" title="Achievement" class="a-achievement-active">Achievement</a></li>
</ul>
</div>
<aside class="banner-left">
<h2>MD/MS Courses</h2>
<h3>Best teachers at your doorstep
<span>India's First Satellite Based PG Medical Classes</span></h3>
</aside>
<?php include 'md-ms-banner-btn.php'; ?>
</article>
</div>
</section> 
<!-- Banner End Here -->
<!-- Midle Content Start Here -->
<section class="inner-gallery-content">
<div class="wrapper">
<div class="photo-gallery-main">

<div class="page-heading">
<span class="home-vector">
<a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a>
</span>
<ul>
<li class="bg_none"><a href="course.php" title="MD/MS Course">MD/MS Course</a></li>
<li><a title="Achievement" class="active-link">Achievement</a></li>
</ul>
</div>

<section class="video-container">

<div class="achievment-left">
<div class="achievment-left-links">
<ul id="accordion">
<li class="border_none" id="accor1" onClick="ontab('1');"><span id="accorsp1" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2014">2014</a>
<ol class="achievment-inner display_none" id="aol1">
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_jun_2014.php" title="DNB June">DNB June</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2014.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2014.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2014.php" title="AIPGME">AIPGME</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="UPPG_2014.php" title="UPPG">UPPG</a></li>
</ol>
</li>

<li id="accor2" onClick="ontab('2');"><span id="accorsp2" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2013">2013</a>
<ol class="achievment-inner display_none" id="aol2">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2013.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2013.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="hp_pg_2013.php" title="HP Toppers">HP Toppers</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2013.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="odisha_cet_2013.php" title="ODISHA CET">ODISHA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipge_2013.php" title="AIPGME">AIPGME</a></li>
</ol>
</li>

<li id="accor3" onClick="ontab('3');" class="light-blue-inner"><span id="accorsp3" class="bgspan">&nbsp;</span><a href="javascript:void(0);" title="2012">2012</a>
<ol class="achievment-inner display_block" id="aol3">
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2012.php" title="AIIMS November">AIIMS November</a></li>
<li class="active-t"><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2012.php" title="AIIMS May">AIIMS May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2012.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2012.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_aug_2012.php" title="DNB August">DNB August</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="jipmer_2012.php" title="JIPMER">JIPMER</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_dec_2012.php" title="PGI Chandigarh December">PGI Chandigarh December</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_may_2012.php" title="PGI Chandigarh May">PGI Chandigarh May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="maharastra_cet_2012.php" title="MAHARASTRA CET">MAHARASTRA CET</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipgmee_2012.php" title="AIPG MEE">AIPGMEE</a></li>
</ol>
</li>

<li id="accor4" onClick="ontab('4');"><span id="accorsp4" class="bgspanblk">&nbsp;</span><a href="javascript:void(0);" title="2011">2011</a>
<ol class="achievment-inner display_none" id="aol4"> 
<li><span class="mini-arrow">&nbsp;</span><a href="delhi_pg_2011.php" title="Delhi PG">Delhi PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="punjab_pg_2011.php" title="Punjab PG">Punjab PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="westbengal_pg_2011.php" title="West Bengal PG">West Bengal PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="rajasthan_pg_2011.php" title="Rajasthan PG">Rajasthan PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="asam_pg_2011.php" title="ASAM PG">ASAM PG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="dnb_nov_2011.php" title="DNB November">DNB November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="uppgmee_2011.php" title="UPPG MEE">UPPG MEE</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_chandigarh_nov_2011.php" title="PGI Chandigarh November">PGI Chandigarh November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="pgi_may_2011.php" title="PGI May">PGI May</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aipg_2011.php" title="AIPG">AIPG</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_nov_2011.php" title="AIIMS November">AIIMS November</a></li>
<li><span class="mini-arrow">&nbsp;</span><a href="aiims_may_2011.php" title="AIIMS May">AIIMS May</a></li>
</ol>
</li>
</ul>
</div>
</div>

<aside class="gallery-left respo-achievement right_pad0">
<div class="inner-left-heading">
<h4>AIIMS May 2012</h4>
<section class="showme-main">
<ul class="idTabs idTabs1"> 
<li><a href="#jquery" class="selected" title="AIIMS May 2012">AIIMS May 2012</a></li>
<li><a href="#official" title="Interview">Interview</a></li>
</ul>

<div id="jquery">
<article class="interview-photos-section">

<ul class="main-students-list">
<li class="tp-border">
<div class="students-box border_left_none">
<img src="images/students/ADITI-CHATURVEDI.jpg" alt="Dr. ADITI CHATURVEDI" title="Dr. ADITI CHATURVEDI" />
<p><span>Dr. ADITI CHATURVEDI</span> Rank: 1</p>
</div>

<div class="students-box">
<img src="images/students/AKASH-TIWARI.jpg" alt="Dr. AKASH TIWARI" title="Dr. AKASH TIWARI" />
<p><span>Dr. AKASH TIWARI</span> Rank: 4</p>
</div>

<div class="students-box">
<img src="images/students/BIBHUTI-KASHYAP.jpg" alt="Dr. BIBHUTI KASHYAP" title="Dr. BIBHUTI KASHYAP" />
<p><span>Dr. BIBHUTI KASHYAP</span> Rank: 28</p>
</div>

<div class="students-box">
<img src="images/students/JUJHAR-SINGH.jpg" alt="Dr. JUJHAR SINGH" title="Dr. JUJHAR SINGH" />
<p><span>Dr. JUJHAR SINGH</span> Rank: 51</p>
</div>
</li>

<li>
<div class="students-box border_left_none">
<img src="images/students/PRASHANTH-B-GANDHI.jpg" alt="Dr. PRASHANTH B GANDHI" title="Dr. PRASHANTH B GANDHI" />
<p><span>Dr. PRASHANTH B GANDHI</span> Rank: 61</p>
</div>

<div class="students-box">
<img src="images/students/GAJANAN-ASHOK-PANANDIKAR.jpg" alt="Dr. GAJANAN ASHOK PANANDIKAR" title="Dr. GAJANAN ASHOK PANANDIKAR" />
<p><span>Dr. GAJANAN ASHOK PANANDIKAR</span> Rank: 68</p>
</div>

<div class="students-box">
<!--<img src="images/students/MOHIT-AGRAWAL.jpg" alt="Dr. MOHIT AGRAWAL" title="Dr. MOHIT AGRAWAL" />
<p><span>Dr. MOHIT AGRAWAL</span> Rank: 32</p>-->
</div>

<div class="students-box">
<!--<img src="images/students/PHIRKE-MANDAR-PANDURANG.jpg" alt="Dr. PHIRKE MANDAR PANDURANG" title="Dr. PHIRKE MANDAR PANDURANG" />
<p><span>Dr. PHIRKE MANDAR PANDURANG</span> Rank: 36</p>-->
</div>

</li>

</ul>
<br style="clear:both;" />
<br style="clear:both;" />
<div class="schedule-mini-series">

<span class="mini-heading">AIIMS May - Result 2012</span>

<div class="schedule-mini-top">
<span class="one-part">Rank</span>
<span class="two-part">Student Name</span>
<span class="three-part">&nbsp;</span>
</div>
<div class="schedule-mini-content">
<ul>
<li class="border_none"><span class="one-parts">1</span>
<span class="two-parts schedule-left-line">Dr. ADITI CHATURVEDI</span>
</li>
<li><span class="one-parts">4</span>
<span class="two-parts schedule-left-line">Dr. AKASH TIWARI</span>
</li>
<li><span class="one-parts">14</span>
<span class="two-parts schedule-left-line">Dr. SIDDHARTH DUBEY</span>
</li>
<li><span class="one-parts">28</span>
<span class="two-parts schedule-left-line">Dr. BIBHUTI KASHYAP</span>
</li>
<li><span class="one-parts">34</span>
<span class="two-parts schedule-left-line">Dr. ABHISHEK GUPTA</span>
</li>
<li><span class="one-parts">38</span>
<span class="two-parts schedule-left-line">Dr. TUSHAR ADITYA NARAIN</span>
</li>
<li><span class="one-parts">47</span>
<span class="two-parts schedule-left-line">Dr. TANVI GUPTA</span>
</li>
<li><span class="one-parts">51</span>
<span class="two-parts schedule-left-line">Dr. JUJHAR SINGH</span>
</li>
<li><span class="one-parts">56</span>
<span class="two-parts schedule-left-line">Dr. SANTOSH GUPTA</span>
</li>
<li><span class="one-parts">61</span>
<span class="two-parts schedule-left-line">Dr. PRASHANTH B GANDHI</span>
</li>
<li><span class="one-parts">67</span>
<span class="two-parts schedule-left-line">Dr. VENUS DALAL</span>
</li>
<li><span class="one-parts">68</span>
<span class="two-parts schedule-left-line">Dr. GAJANAN ASHOK PANANDIKAR</span>
</li>
<li><span class="one-parts">76</span>
<span class="two-parts schedule-left-line">Dr. ANURADHA</span>
</li>
<li><span class="one-parts">80</span>
<span class="two-parts schedule-left-line">Dr. PUAIPG(NBE/NEET) Pattern JAIN</span>
</li>
<li><span class="one-parts">81</span>
<span class="two-parts schedule-left-line">Dr. AJEET KUMAR</span>
</li>
<li><span class="one-parts">84</span>
<span class="two-parts schedule-left-line">Dr. HEMANT GROVER</span>
</li>
<li><span class="one-parts">88</span>
<span class="two-parts schedule-left-line">Dr. JASKARAN SINGH</span>
</li>
<li><span class="one-parts">94</span>
<span class="two-parts schedule-left-line">Dr. SANKALAP DUDEJA</span>
</li>
<li><span class="one-parts">102</span>
<span class="two-parts schedule-left-line">Dr. HAMEED OBEDULLA</span>
</li>
<li><span class="one-parts">117</span>
<span class="two-parts schedule-left-line">Dr. MANVI SINGH</span>
</li>
<li><span class="one-parts">120</span>
<span class="two-parts schedule-left-line">Dr. CHINMOY KUMAR BISWAL</span>
</li>
<li><span class="one-parts">123</span>
<span class="two-parts schedule-left-line">Dr. NIKHIL SHARMA</span>
</li>
<li><span class="one-parts">124</span>
<span class="two-parts schedule-left-line">Dr. SHILPA ARORA</span>
</li>
<li><span class="one-parts">128</span>
<span class="two-parts schedule-left-line">Dr. SUMIT ARORA</span>
</li>
<li><span class="one-parts">135</span>
<span class="two-parts schedule-left-line">Dr. PIYUSH MITTAL</span>
</li>
<li><span class="one-parts">142</span>
<span class="two-parts schedule-left-line">Dr. NEHA MISHRA</span>
</li>
<li><span class="one-parts">148</span>
<span class="two-parts schedule-left-line">Dr. SHAHSWAT SARIN</span>
</li>
<li><span class="one-parts">152</span>
<span class="two-parts schedule-left-line">Dr. JASKARAN SINGH GUJRAL</span>
</li>
<li><span class="one-parts">185</span>
<span class="two-parts schedule-left-line">Dr. NITIN BANSAL</span>
</li>
<li><span class="one-parts">201</span>
<span class="two-parts schedule-left-line">Dr. HINA ALI</span>
</li>
<li><span class="one-parts">203</span>
<span class="two-parts schedule-left-line">Dr. SAMEER M HALAGERI</span>
</li>
<li><span class="one-parts">204</span>
<span class="two-parts schedule-left-line">Dr. HEMIKA AGRAWAL</span>
</li>
<li><span class="one-parts">205</span>
<span class="two-parts schedule-left-line">Dr. ROHIT SINGH</span>
</li>
<li><span class="one-parts">221</span>
<span class="two-parts schedule-left-line">Dr. RAHUL RANJAN</span>
</li>
<li><span class="one-parts">239</span>
<span class="two-parts schedule-left-line">Dr. SUNIL KUMAR</span>
</li>
<li><span class="one-parts">243</span>
<span class="two-parts schedule-left-line">Dr. AKSHAY GOEL</span>
</li>
<li><span class="one-parts">256</span>
<span class="two-parts schedule-left-line">Dr. MANOJ KUMAR VERMA</span>
</li>
<li><span class="one-parts">257</span>
<span class="two-parts schedule-left-line">Dr. PRIYANKA MINOCHA</span>
</li>
<li><span class="one-parts">290</span>
<span class="two-parts schedule-left-line">Dr. NIMISHA JAIN</span>
</li>
<li><span class="one-parts">298</span>
<span class="two-parts schedule-left-line">Dr. VINAY KUMAR</span>
</li>
<li><span class="one-parts">312</span>
<span class="two-parts schedule-left-line">Dr. DEEPIKA</span>
</li>
<li><span class="one-parts">326</span>
<span class="two-parts schedule-left-line">Dr. PAVAIPG(NBE/NEET) Pattern SINGH KOHLI</span>
</li>
<li><span class="one-parts">344</span>
<span class="two-parts schedule-left-line">Dr. SANJAY SEHRAWAT</span>
</li>
<li><span class="one-parts">345</span>
<span class="two-parts schedule-left-line">Dr. ABHISHEK KUMAR</span>
</li>
<li><span class="one-parts">353</span>
<span class="two-parts schedule-left-line">Dr. PARUL GUPTA</span>
</li>
<li><span class="one-parts">&nbsp;</span>
<span class="two-parts schedule-left-line" style="color:#000;">And many more...</span>
</li>
</ul>
</div>
</div>

</article></div>

<div id="official"> 
<article class="interview-photos-section"> 

<div class="achievment-videos-section">
<ul>
<li>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/M7L2-zLb560?wmode=transparent" class="border_none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Tuhina Goel</span></p>
<p>Rank: 1st AIIMS Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/L-AnijPFb-I?wmode=transparent" class="border_none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Nishant Kumar</span></p>
<p>Rank: 56th AIIMS Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
</li>

<li>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/HIP7wjjGuoM?wmode=transparent" class="border_none"></iframe> 
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Geetika Srivastava</span></p>
<p>Rank: 6th PGI Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
<div class="video-box-1">
<iframe width="100%" height="247" src="//www.youtube.com/embed/Atat9dha9RI?wmode=transparent" class="border_none"></iframe>
<div class="video-content">
<p><strong>Name:</strong> <span>Dr. Bhawna Attri</span></p>
<p>Rank: 24th PGI Nov 2013</p>
<div class="social-list"><img src="images/social-list.jpg" title="List" alt="List" /> </div>
</div>
</div>
</li>
</ul>
</div>
</article> 
</div>
</section>
</div>
</aside>
</section>
</div>
</div>
</section>
<!-- Midle Content End Here -->
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here -->
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>