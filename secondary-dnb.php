<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAMS Coaching for PG Medical Entrance Exam, AIPG(NBE/NEET) Pattern PG</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!-- [if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- HTML5 -->
<script type="text/javascript" src="js/html5.js"></script>
<!-- HTML5 -->

<!--Iphone Js-->
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link media="only screen and (max-device-width:320px)"href="iPhone.css" type="text/css" rel="stylesheet"/>
<!--Iphone Js-->

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
//     Registration Form
    $('#student-registration').click(function(e) {
		$('#backPopup').show();
		$('#frontPopup1').show();       
    });
	$('#student-registration-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup1').hide();
    });

//     Sign In Form
	$('#student-login').click(function() {
		$('#backPopup').show();
		$('#frontPopup2').show();
    });
	$('#student-login-close').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
    });
	
//     Cloud Login Form
	$('#cloud-login').click(function() {
		$('#backPopup').show();
		$('#dams-cloud').show();
    });
	$('#cloud-login-close').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
    });	

//     Quick Enquiry Form
	$('#student-enquiry').click(function() {
		$('#backPopup').show();
		$('#quickenquiry').show();
    });
	$('#student-enquiry-close').click(function() {
		$('#backPopup').hide();
		$('#quickenquiry').hide();
    });
	
//     Forgot Password Form
	$('#fg-password').click(function() {
		$('#backPopup').hide();
		$('#frontPopup2').hide();
		$('#backPopup').show();
		$('#forgotpassword').show();
    });
	$('#fg-close').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword').hide();
    });

//     Forgot Password DAMS Cloud Form
	$('#fg-password2').click(function() {
		$('#backPopup').hide();
		$('#dams-cloud').hide();
		$('#backPopup').show();
		$('#forgotpassword2').show();
    });
	$('#fg-close2').click(function() {
		$('#backPopup').hide();
		$('#forgotpassword2').hide();
    });

});
</script>
</head>

<body class="inner-bg" onLoad="Menu.changeMenu(false)">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'social-icon.php'; ?>
<?php include 'header.php'; ?>

<!-- Banner Start Here -->

<section class="inner-banner">
  <div class="wrapper">
    <article>
      <aside class="banner-left banner-left-postion">
        <h2>Be smart &amp;<br>
          take your future in Your Hand </h2>
        <h3 class="with_the_launch">With the launch of iDAMS a large number of aspirants, who otherwise couldn't take the advantage of the DAMS teaching because of various reasons like non-availability of DAMS centre in the vicinity would be greatly benefited.</h3>
      </aside>
      <?php include'md-ms-banner-btn.php'; ?>
    </article>
  </div>
</section>

<!-- Banner End Here --> 

<!-- Midle Content Start Here -->

<section class="inner-gallery-content">
  <div class="wrapper">
    <div class="photo-gallery-main">
      <div class="page-heading"> <span class="home-vector"><a href="index.php" title="Delhi Academy of Medical Sciences">&nbsp;</a></span>
        <ul>
          <li class="bg_none"><a href="dnb.php" title="DNB">DNB</a></li>
          <li><a title="Secondary DNB" class="active-link">Secondary DNB</a></li>
        </ul>
      </div>
      <section class="event-container">
        <aside class="gallery-left respo-achievement">
          <div class="inner-left-heading">
            <h4>Secondary DNB</h4>
            <article class="showme-main">
              <div class="idams-content">
                <div class="franchisee-box">
                  <p>This is some basic information for people pursuing DMRD or any Diploma be it DCH, DGO, DORTH, DOMS, DMRT, DA, DVD, DPM, DNM.</p>
                  <span>Pattern of examination :-</span>
                  <p>MCQ based examination comprising of 180 MCQs of 1mark each with following scheme:</p>
                </div>
                <ul class="dnb-list">
                  <h5>CONTENTS-</h5>
                  <li><span>&nbsp;</span>Applied Basic sciences</li>
                  <li><span>&nbsp;</span> Anatomy, Biochemistry, Physiology 30 MCQs.</li>
                  <li><span>&nbsp;</span>Pathology, microbiology, pharmacology 30 MCQs</li>
                  <li><span>&nbsp;</span>Medicine and allied sciences 30MCQs</li>
                  <li><span>&nbsp;</span>Surgery and allied sciences 30MCQs</li>
                  <li><span>&nbsp;</span>Obstretic &amp; Gynecology 30MCQs</li>
                  <li><span>&nbsp;</span>Community Medicine, basic statistics, research methods.</li>
                </ul>
                <ul class="dnb-list">
                  <h5>Duration 3 hours-</h5>
                  <p>Level of FINAL MBBS, No negative marking. Merit list will be published speciality wise</p>
                  <p>so we at DAMS have been the pioneers for post graduate medical entrance examination including AIPG(NBE/NEET) Pattern exam which has been purposed recently. We offer our various courses including regular class room, test & discussion, distant learning programmes to aspirants of PG CET as well. It is clearly mentioned in National Board Website that this exam will be at level of FINAL MBBS so joining us makes SENSE and we can guarantee success and help you secure HIGHER in the MERIT LIST. Join DAMS at this crucial stage of our career and get the famous DAMS EDGE.</p>
                </ul>
              </div>
            </article>
          </div>
        </aside>
        <aside class="gallery-right"> 
          
          <!--for Enquiry -->
          <?php include 'enquiryform.php'; ?>
          <!--for Enquiry --> 
          
        </aside>
      </section>
    </div>
  </div>
</section>

<!-- Midle Content End Here --> 

<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 

<!-- Principals Packages  -->
<link href="navigation/tinyMCE.css" rel="stylesheet" type="text/css">
<script src="js/navigation.js" type="text/javascript"></script> 
<!-- Others Packages -->

</body>
</html>