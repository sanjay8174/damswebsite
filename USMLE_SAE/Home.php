<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>DAMS USMLE Self Assessment Exam</title>
<link rel="shortcut icon" href=images/favicon.ico type=image/x-icon />
<link rel=icon href=images/favicon.ico type=image/x-icon />
<link href="css/styleHome.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function show(){
	$("#dialog").css("display" , "block");
	$(".res_main_pop").removeClass("pop_hide");
	}
function showhide(){
	$(".res_main_pop").addClass("pop_hide");
	setTimeout(function(){  
	    $("#dialog").css("display" , "none");
	}, 300);
	
	}
	
	function regsw(){
	$("#regdlg").css("display" , "block");
    $("#dialog").css("display" , "none");
	}
function regswhide(){
	$(".res_main_pop").addClass("pop_hide");
	setTimeout(function(){  
	    $("#regdlg").css("display" , "none");
	}, 300);
	}
function back(){
	$("#regdlg").css("display" , "none");
	$("#dialog").css("display" , "block");
	}
 
 function Select(id){       
   //alert(id);
  $(".nav a").removeClass("selected");
   $("#"+id).addClass("selected");
  //alert(url); 
////var url = "https://localhost/mdscbt2015/index.php#"+id;
//$(location).attr('href',url);
   
}
</script>
</head>

<body>

<!--Login popup start--> 
 <div id="dialog" style="display: none">
  <div class="res_pop_bg"></div>     
    <div class="res_main_pop" style="top:57%;">
     <span onClick="showhide()" ></span>
     <div class="pop_title">Login with your Account</div>
     <div class="reg_cls_strm">
      <aside style="width:100%;">
       <label>Select Year</label>
       <select>
        <option>2015</option>
       </select>
      </aside>
     </div>
     <p>
      <label>Roll No. / Email</label>
      <input type="text">
     </p>
     <p>
      <label>Password</label>
      <input type="text">
     </p>
     <p>
      <a href="#" class="forgot_pwd">Forgot your Password ?</a>
      <input type="submit" value="Login">
     </p>
     <p>
      <a href="#" class="forgot_pwd" onClick="regsw()">Don't have an account ? <b>Click here for Registration</b></a>
      <!--<input type="submit" id="btnModalPopup" onclick="regsw()" class="register_btn" value="Register">-->
     </p>
    </div>
</div>
<!--Login popup end-->

<!--Registration popup start-->
<div id="regdlg" style="display: none">
  <div class="res_pop_bg"></div>     
    <div class="res_main_pop">
     <b class="reg_back" onClick="back()"></b>
     <span onClick="regswhide()" ></span>
     <div class="pop_title">Registration for Online Exam</div>
     <p>
      <label>Name</label>
      <input type="text">
     </p>
     <p>
      <label>Email Address</label>
      <input type="text">
     </p>
     <p>
      <label>Phone / Mob.</label>
      <input type="text">
     </p>
     <div class="reg_cls_strm">
      <aside>
       <label>Password</label>
       <input type="text">
      </aside>
      <aside style="float:right;">
       <label>Confirm Password</label>
       <input type="text">
      </aside>
     </div>
     <div class="reg_cls_strm">
     <aside style="width:100%;">
       <label>Select Course</label>
       <select>
        <option>Select</option>
       </select>
      </aside>
     </div>
     <div class="reg_cls_strm">
      <aside>
       <label>Select State</label>
       <select>
        <option>Select City</option>
       </select>
      </aside>
      <aside style="float:right;">
       <label>Stream</label>
       <select>
        <option>Please Select</option>
       </select>
      </aside>
     </div>
     <p>
      <!--<a href="#" class="acpt_trms"><input type="checkbox" class="chkbx">I accept the <br><small>terms and conditions</small></a>-->
      <input type="submit" value="Register">
     </p>
    </div>
</div>
<!--Registration popup end-->



<header id="nav-anchor" class="main_cont">
<div class="nav_fixed">
 <div class="top_bar"></div>
 <div class="main_wrappr main_cont_head" >
  <a href="#" class="logo">
   <img src="images/logo.png">
  </a>
  <nav class="nav_cntr" >
    <a class="toggleMenu" href="#"></a>
     <ul class="nav" id="nav1">
         <li class="test"><a href="#Overview_cont" id="Overview" class="selected" onClick="Select(this.id);"  >Overview </a></li>
      <li><a href="#features" id="Features" onClick="Select(this.id);"  >Features</a></li>
      <li><a href="#contact" id="Contact" onClick="Select(this.id);" >Contact</a></li>
      <li><a href="index.php?p=registration"class="last" >Registration</a></li>
<!--      <li><a href="index.php?p=Login" class="last" >Login</a></li>-->
     </ul>
   </nav>
 </div>
 </div>
</header>
<section class="sld_section main_cont">
<!--<div class="demo-centering">
    
    <div id="sliderB" class="slider">
        <!--<div><img src="images/Slider_2.jpg"/>
      <div class="main_wrappr">
      <div class="bnr_ovr_txt" style="position:absolute; top:0px;" >
       <h2 class="bold_font">Best Coaching Institute for <br>Dental Medical Entrance Examination</h2>
       <div class="test_date">
        <span class="test_dateicon"></span>
         <strong class="bold_font">Registration Start Date</strong><br>
       </div>
       <div class="test_date">
        <span class="test_dateicon"></span>
         <strong class="bold_font">Registration Closing Date</strong><br> 05 November 2016
       </div>
       <div class="test_date">
        <span class="test_dateicon"></span>
         <strong class="bold_font">OBS Start Date</strong><br> 30 November 2015
       </div>
       <div class="test_date">
        <span class="test_dateicon"></span>
         <strong class="bold_font">Admit Card Date</strong><br> 12 November 2016
       </div>
       <div class="test_date">
        <span class="test_dateicon"></span>
         <strong class="bold_font">Test Date</strong><br> 20  November 2016
       </div>
       
      </div>
      </div>
     </div>
      <div><img src="images/Slider_1.jpg"/></div>
   </div>-->
   <div class="demo-centering">
    
    <div  class="sliderFIX_IMG">

      <div><img src="images/Slider_new.jpg"/></div>
   </div>
    
</div>
    
</div>
    <div  id="Overview_cont" class="wpb_column vc_column_container vc_col-sm-12" id="schedule">
                                                                <div class="wpb_wrapper">
				<div class="event-infos">
			
									
				<div class="event-info-col"  id="myDiv1" onMouseOver="changeID(this.id)">
					<div class="event-info-ico"><span class="fa fa-calendar"></span></div>
					<h3 class="main-title3">Registration Start Date</h3>
					<p> 14 August 2016</p>
				</div>
				<div class="event-info-col" id="myDiv2"  onmouseover="changeID(this.id)">
					<div class="event-info-ico"><span class="fa fa-calendar" ></span></div>
					<h3 class="main-title3">Registration Closing Date</h3>
					<p>21 August 2016   </p>
				</div>
<!--				<div class="event-info-col"  id="myDiv3" onMouseOver="changeID(this.id)">
					<div class="event-info-ico"><span class="fa fa-calendar" ></span></div>
					<h3 class="main-title3">Admit Card Date</h3>
					<p>16 August 2016</p>
				</div>-->
				<div class="event-info-col"  id="myDiv3" onMouseOver="changeID(this.id)">
					<div class="event-info-ico"><span class="fa fa-calendar" ></span></div>
					<h3 class="main-title3">Center</h3>
					<p>Delhi (Karol Bagh)</p>
				</div>
                                                                    <div class="event-info-col"  id="myDiv4"  onmouseover="changeID(this.id)">
					<div class="event-info-ico"><span class="fa fa-calendar"></span></div>
					<h3 class="main-title3"> Test Date</h3>
					<p>22 to 29 August 2016</p>
				</div>
								
				</div></div></div>


</section>
                                   <section class="main_cont">
                                      <div class="vc_row wpb_row vc_row-fluid section-about">
                                          <div class="wpb_column vc_column_container vc_col-sm-12">
                                              <div class="wpb_wrapper">
			<div class="about-picture-wrapper">
			         <div class="about-picture" id="about-picture">
				<img src="images/About_DVT.png" alt="">
			          </div>
			</div>
		
		     <div class="about-text-wrapper"> 
		         <div class="about-text" id="about-text">
			<h1 class="title3 title-border"><i class="fa fa-dot-circle-o"></i>About MOCK USMLE Self assessment Full length exam</h1>
			  <div class="about-text-content">
			       <p>DAMS announces , COMPUTER BASED TEST Self assessment full length USMLE exam. The ultimate MOCK test will be conducted in our designated centers on USMLE exam software. This will be based on the Item response theory and other guidelines provided by ECFMG for the USMLE Exam.</br>
 Exam will be conducted over an 8 hours with 7 slots. Each Slot will be for an hour and the break time of 1 hour can be taken by the student as in the real exam.</br>
 
 The results will be analysed with equating and scaling as per the actual exam guidelines. It will be an opportunity to see and predict your final performance in an exam like set up.</br>
The MOCK USMLE Exam will predict the relative importance of all the subjects that comprise the USMLE syllabus. The relative importance of each subject depends on not only the number of questions asked from each subject but also on the merit given to each question of that subject.The software will compute and tell you based on your performance the level of expertise in each subject.</p></div>
                                                              <a href="index.php?p=registration" class="btn_reg btn-nobg"><i class="fa fa-paper-plane"></i> Register</a>
                         </div></div></div></div></div>
 
                                      <div class="vc_row mvenue_img">
                                           <div class="v_date">
                                               <div class="inr_wpr package_cntr">
                                                   <div class="aftr_inr_cntr package_cntr_pad">
                                                      <div class="pkg_red">
                                                         <h2 class="bold_font">DAMSONians(2016)</h2>
                                                              <div class="pkg_dtls">
                                                               <p><span class="spanleft ">Payment:</span> <span>Online</span></p>
                                                                <p> <span class="spanleft ">Price:</span><span>Rs. 1500</span> </p>
                                                                 <p class="last">   <a href="index.php?p=registration" class="buynow bold_font">Register</a></p>
                                                              </div>
                                                         </div>
                                                     <div class="pkg_green">
                                                           <h2 class="bold_font">Non-DAMSONians</h2>
                                                               <div class="pkg_dtls">
                                                                 <p><span class="spanleft ">Payment:</span> <span>Online</span></p>
                                                                 <p><span class="spanleft ">Price:</span> <span>Rs. 2500</span></p>
                                                                  <p class="last"><a href="index.php?p=registration" class="buynow bold_font" style="color: rgb(7, 118, 192); border-color: rgb(7, 118, 192);">Register</a> </p>
                                                       </div>
                                               </div>
                                          </div>
                                      </div>
        
                             </div>
                     </div>
                      <div class="main_wrappr feature_new">
                            <div class="inr_wpr package_cntr">
                                       <div class="ctnt_heading scrl_prnt">
                                           <div id="features" class="scrl_point"></div>
                                                <h1 class="cont_feat_h">Features</h1>
                                        </div>
                                        <div class="aftr_inr_cntr featrs_wpr feature_cen_cntl feat_top">
                                          <div class="cntr_feature">
    <ul>
     <li><span></span> Detailed Analysis in different Subject areas of your preparation.</li>
     <li><span></span> First ever initiative by any coaching institute in India.</li>
     <li><span></span>Chance to get a real feel of exam just before the D day. The practice of going to an unfamiliar exam centre to write a full 8 hour test, to be able to retain concentration over the entire span of time during the test, to read carefully all questions and managing time gives our students an extra advantage. The test helps to improve the overall speed of answering questions in an exam like environment. Students who experience problem in maintaining the answering speed in the last half of the examination period have reported to have been greatly helped by our CBT in the past.t</li>
    </ul>
            </div>
   </div>
  </div>
 </div>
 <div class="main_wrappr feature_new ">
<!-- <div class="inr_wpr package_cntr back_color_cen back_image">
   <div class="ctnt_heading scrl_prnt">
    <div id="features" class="scrl_point"></div>
    <h1 class="cont_feat_h ce">Centres</h1>
   </div>
   <div class="cntr_list">
    <ul class="online_centers">
        <//?php //$ob = new funDams();
       // $getCentres  = $ob->getExamCentre();
               $i=0; 
      //while($row = mysql_fetch_array($getCentres)){ ?>
      <li><//?php //echo $row[NAME]; ?></li>
      <//?php } ?>
    </ul>
   </div>
   
  </div>-->
  <div class="inr_wpr footer_links_wpr ">
   <div class="ftr_inr_cntr ftr_controler">
  <article class="info_prt info_first1">
    <h3 class="ft_ttl bold_font">Imporatant Dates</h3>
    <ul>
        <li>Date of Registration : <span>14 August 2016</span></li>
     <li>Last Date of Registration :<span> 21 August 2016</span></li>
<!--     <li>ONLINE BOOKING SYSTEM OPEN : 30 NOV, 2015</li>
     <li>ONLINE BOOKING SYSTEM CLOSE : 3 DEC, 2015</li>-->
<!--     <li>Issuing of Admit Card :<span>21 August 2016</span></li>-->
     <li>DAMS USMLE SA Exam:<span>22 to 29 August 2016</span></li>
    </ul>
  </article>
  
<article class="info_prt info_prt2">
    <h3 class="ft_ttl bold_font cont_us_detail" id="contact">Contact Us</h3>   
      <ul>
     <li> Landline No :<span>011 - 40094009, 9873314110, 9811102312</span></li>
     <li>Email :<span>usmle@damsdelhi.com</span></li>
     <li>Website : <span>www.usmlemock.damsdelhi.com</span></li>
    </ul>

  </article>
<!--  <article class="info_prt info_prt2">
    <h3 class="ft_ttl bold_font" id="contact">Contact Us</h3>   
   <div class="cpyrt_dtls">
    Mobile : <strong>+91 9873314110, 9811862082</strong> <br>
    Landline No : <strong>011 - 40464046</strong> <br> 
    Email : <strong> helpdesk@damsdelhi.com</strong> <br> 
    Website : <strong>www.damsdelhi.com</strong>
   </div>
  </article>-->
<!--  <article class="links">
   <h3 class="ft_ttl bold_font">Quick Tour</h3>
<a href="pdf/RDETAIL.pdf" target="_blank"><img src="images/pdf.png" style="padding:2px">How to Register ?</a><br>
<a href="pdf/FP.pdf" target="_blank"><img src="images/pdf.png" style="padding:2px">Forgot Password Help ?</a><br>
<a href="pdf/IDATES.pdf" target="_blank"><img src="images/pdf.png" style="padding:2px">Important Dates</a><br>
<a href="pdf/FAQ.pdf" target="_blank"><img src="images/pdf.png" style="padding:2px">Frequently Asked Questions.</a>
  </article>-->
<article class="followuson info_first2">
   <h3 class="ft_ttl bold_font foll_us">Follow us on</h3>
   <a href="https://www.facebook.com/damsdelhiho" class="fb" target="_blank"></a>
   <a href="https://twitter.com/damsdelhi" class="twt" target="_blank"></a>
   <a href="https://plus.google.com/u/0/110912384815871611420/about" class="gpls target="_blank""></a>
   <a href="https://www.youtube.com/user/damsdelhi" class="yt" target="_blank"></a>
  </article>
  </div>
  </div>
  
  </div>

</section>
<footer>
 <div class="main_wrappr">
  <p class="foot_txt">© Delhi Academy of Medical Sciences Pvt. Ltd. All rights reserved.</p>
 </div>
</footer>
<!--Nav fixed js start-->
<script src="js/jquery-1.9.1.min.js"></script> 
<script type="text/javascript">
         
    $(document).ready(function(){
       
        $(window).scroll(function(){
            var window_top = $(window).scrollTop() + 0; 
            var div_top = $('#nav-anchor').offset().top;
                if (window_top > div_top) {
                    $('.nav_fixed').addClass('stick');
                } else {
                    $('.nav_fixed').removeClass('stick');
                }
        });
		
    });

</script> 
<!--Nav fixed js end-->
<script type="text/javascript">
function tabshow1(){
	$("#tab2").css("height" , "0px");
        $("#tab3").css("height" , "0px");
	$("#tab1").css("height" , "auto");
	$("#tabsac1").addClass("current");
	$("#tabsac2").removeClass("current");
        $("#tabsac3").removeClass("current");
	$("#tb1arow").css("visibility" , "visible");
	$("#tb2arow").css("visibility" , "hidden");
        $("#tb3arow").css("visibility" , "hidden");
	
	}
function tabshow2(){
	$("#tab1").css("height" , "0px");
        $("#tab3").css("height" , "0px");
	$("#tabsac1").removeClass("current");
        $("#tabsac3").removeClass("current");
	$("#tabsac2").addClass("current");
	$("#tab2").css("height" , "auto");
	$("#tb1arow").css("visibility" , "hidden");
	$("#tb2arow").css("visibility" , "visible");
        $("#tb3arow").css("visibility" , "hidden");
	}
function tabshow3(){
	$("#tab1").css("height" , "0px");
	$("#tabsac1").removeClass("current");
        $("#tab2").css("height" , "0px");
	$("#tabsac2").removeClass("current");
	$("#tabsac3").addClass("current");
	$("#tab3").css("height" , "auto");
	$("#tb1arow").css("visibility" , "hidden");
        $("#tb2arow").css("visibility" , "hidden");
	$("#tb3arow").css("visibility" , "visible");
	}
</script>
<!--Main navigation start-->
<script type="text/javascript" src="js/script.js"></script>
<script>
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top-50
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
</script>
<!--Main navigation end-->
<!--Tabs script start-->
<!--<script>
$(document).ready(function() {
	$("#content .sw_hd").hide(); 
	$("#tabs li:first").attr("id","current");
	$("#content .sw_hd:first").fadeIn();
    
    $('#tabs a').click(function(e) {
        e.preventDefault();        
        $("#content .sw_hd").hide();
        $("#tabs li").attr("id","");
        $(this).parent().attr("id","current");
        $('#' + $(this).attr('title')).fadeIn();
    });
})();
</script>-->

<!--Tabs video slider code start-->
<script type="text/javascript">
//1. set ul width 
//2. image when click prev/next button
var ul;
var li_items;
var imageNumber;
var imageWidth;
var prev, next;
var currentPostion = 0;
var currentImage = 0;


function init(){
	ul = document.getElementById('image_slider');
	li_items = ul.children;
	imageNumber = li_items.length;
	imageWidth = li_items[0].children[0].clientWidth;
	ul.style.width = parseInt(imageWidth * imageNumber) + 'px';
	prev = document.getElementById("prev");
	next = document.getElementById("next");
	//.onclike = slide(-1) will be fired when onload;
	/*
	prev.onclick = function(){slide(-1);};
	next.onclick = function(){slide(1);};*/
	prev.onclick = function(){ onClickPrev();};
	next.onclick = function(){ onClickNext();};
}

function animate(opts){
	var start = new Date;
	var id = setInterval(function(){
		var timePassed = new Date - start;
		var progress = timePassed / opts.duration;
		if (progress > 1){
			progress = 1;
		}
		var delta = opts.delta(progress);
		opts.step(delta);
		if (progress == 1){
			clearInterval(id);
			opts.callback();
		}
	}, opts.delay || 17);
	//return id;
}

function slideTo(imageToGo){
	var direction;
	var numOfImageToGo = Math.abs(imageToGo - currentImage);
	// slide toward left

	direction = currentImage > imageToGo ? 1 : -1;
	currentPostion = -1 * currentImage * imageWidth;
	var opts = {
		duration:500,
		delta:function(p){return p;},
		step:function(delta){
			ul.style.left = parseInt(currentPostion + direction * delta * imageWidth * numOfImageToGo) + 'px';
		},
		callback:function(){currentImage = imageToGo;}	
	};
	animate(opts);
}

function onClickPrev(){
	if (currentImage == 0){
		slideTo(imageNumber - 1);
	} 		
	else{
		slideTo(currentImage - 1);
	}		
}

function onClickNext(){
	if (currentImage == imageNumber - 1){
		slideTo(0);
	}		
	else{
		slideTo(currentImage + 1);
	}		
}

window.onload = init;
</script>
<script type="text/javascript">
var ul1;
var li_items1;
var imageNumber1;
var imageWidth1;
var prev1, next1;
var currentPostion1 = 0;
var currentImage1 = 0;

var ul2;
var li_items2;
var imageNumber2;
var imageWidth2;
var prev2, next2;
var currentPostion2 = 0;
var currentImage2 = 0;


function init1(){
	ul1 = document.getElementById('image_slider1');
	li_items1 = ul1.children;
	imageNumber1 = li_items1.length;
	imageWidth1 = li_items1[0].children[0].clientWidth;
	ul1.style.width = parseInt(imageWidth1 * imageNumber1) + 'px';
	prev1 = document.getElementById("prev1");
	next1 = document.getElementById("next1");
	prev1.onclick = function(){ onClickPrev1();};
	next1.onclick = function(){ onClickNext1();};
	
	ul = document.getElementById('image_slider');
	li_items = ul.children;
	imageNumber = li_items.length;
	imageWidth = li_items[0].children[0].clientWidth;
	ul.style.width = parseInt(imageWidth * imageNumber) + 'px';
	prev = document.getElementById("prev");
	next = document.getElementById("next");
	prev.onclick = function(){ onClickPrev();};
	next.onclick = function(){ onClickNext();};
	
	
}





function slideTo1(imageToGo){
	var direction;
	var numOfImageToGo = Math.abs(imageToGo - currentImage1);
	// slide toward left

	direction = currentImage1 > imageToGo ? 1 : -1;
	currentPostion1 = -1 * currentImage1 * imageWidth1;
	var opts = {
		duration:500,
		delta:function(p){return p;},
		step:function(delta){
			ul1.style.left = parseInt(currentPostion1 + direction * delta * imageWidth1 * numOfImageToGo) + 'px';
		},
		callback:function(){currentImage1 = imageToGo;}	
	};
	animate1(opts);
}
function animate1(opts){
	var start = new Date;
	var id = setInterval(function(){
		var timePassed = new Date - start;
		var progress = timePassed / opts.duration;
		if (progress > 1){
			progress = 1;
		}
		var delta = opts.delta(progress);
		opts.step(delta);
		if (progress == 1){
			clearInterval(id);
			opts.callback();
		}
	}, opts.delay || 17);
	//return id;
}
function onClickPrev1(){
	if (currentImage1 == 0){
		slideTo1(imageNumber1 - 1);
	} 		
	else{
		slideTo1(currentImage1 - 1);
	}		
}

function onClickNext1(){
	if (currentImage1 == imageNumber1 - 1){
		slideTo1(0);
	}		
	else{
		slideTo1(currentImage1 + 1);
	}		
}


window.onload = init1;
</script>
<script type="text/javascript">
var ul2;
var li_items2;
var imageNumber2;
var imageWidth2;
var prev2, next2;
var currentPostion2 = 0;
var currentImage2 = 0;


function init2(){
	ul2 = document.getElementById('image_slider2');
	li_items2 = ul2.children;
	imageNumber2 = li_items2.length;
	imageWidth2 = li_items2[0].children[0].clientWidth;
	ul2.style.width = parseInt(imageWidth2 * imageNumber2) + 'px';
	prev2 = document.getElementById("prev2");
	next2 = document.getElementById("next2");
	prev2.onclick = function(){ onClickPrev2();};
	next2.onclick = function(){ onClickNext2();};
	
	ul = document.getElementById('image_slider');
	li_items = ul.children;
	imageNumber = li_items.length;
	imageWidth = li_items[0].children[0].clientWidth;
	ul.style.width = parseInt(imageWidth * imageNumber) + 'px';
	prev = document.getElementById("prev");
	next = document.getElementById("next");
	prev.onclick = function(){ onClickPrev();};
	next.onclick = function(){ onClickNext();};
	
        ul1 = document.getElementById('image_slider1');
	li_items1 = ul1.children;
	imageNumber1 = li_items1.length;
	imageWidth1 = li_items1[0].children[0].clientWidth;
	ul1.style.width = parseInt(imageWidth1 * imageNumber1) + 'px';
	prev1 = document.getElementById("prev1");
	next1 = document.getElementById("next1");
	prev1.onclick = function(){ onClickPrev1();};
	next1.onclick = function(){ onClickNext1();};
}


	
	
	
function slideTo2(imageToGo){
	var direction;
	var numOfImageToGo = Math.abs(imageToGo - currentImage2);
	// slide toward left

	direction = currentImage2 > imageToGo ? 1 : -1;
	currentPostion2 = -1 * currentImage2 * imageWidth2;
	var opts = {
		duration:500,
		delta:function(p){return p;},
		step:function(delta){
			ul2.style.left = parseInt(currentPostion2 + direction * delta * imageWidth2 * numOfImageToGo) + 'px';
		},
		callback:function(){currentImage2 = imageToGo;}	
	};
	animate1(opts);
}
function animate2(opts){
	var start = new Date;
	var id = setInterval(function(){
		var timePassed = new Date - start;
		var progress = timePassed / opts.duration;
		if (progress > 1){
			progress = 1;
		}
		var delta = opts.delta(progress);
		opts.step(delta);
		if (progress == 1){
			clearInterval(id);
			opts.callback();
		}
	}, opts.delay || 17);
	//return id;
}
function onClickPrev2(){
	if (currentImage2 == 0){
		slideTo2(imageNumber2 - 1);
	} 		
	else{
		slideTo2(currentImage2 - 1);
	}		
}

function onClickNext2(){
	if (currentImage2 == imageNumber2 - 1){
		slideTo2(0);
	}		
	else{
		slideTo2(currentImage2 + 1);
	}		
}


window.onload = init2;
</script>
<!--Tabs video slider code start-->
<!--Tabs script end-->

<!--Testimonila script start-->
<script type="text/javascript">
$(document).ready(function(fadeLoop) {
  
  var fad = $('.fader');
  var counter = 0;
  var divs = $('.fader').hide();
  var dur = 500;
  
  fad.children().filter('.fader').each(function(fad) {
    
    function animator(currentItem) {
      
      animator(fad.children(":first"));
      
      fad.mouseenter(function(){
        $(".fader").stop(); 
      });
      fad.mouseleave(function(){
        animator(fad.children(":first"));
      });
    };
    
  });
  
  function showDiv() {
    divs.fadeOut(dur)
    .filter(function(index) {
      return index == counter % divs.length;
    }) 
    .delay(dur) 
    .fadeIn(dur);
    counter++;
  };
  
  showDiv();
  
  return setInterval(function() { 
    showDiv(); 
  }, 2 * 5000);
});

</script>
<!--Testimonial script end-->
<script type="text/javascript" src="js/jquery.js"></script>

<script type="text/javascript" src="js/jquery.excoloSlider.js"></script>
<script type="text/javascript" src="js/jquery.excoloSlider.min.js"></script>
<script type="text/javascript">
	 $(function () {
            $("#sliderA").excoloSlider();
            $("#sliderB").excoloSlider();
        });
</script>
</body>
</html>
