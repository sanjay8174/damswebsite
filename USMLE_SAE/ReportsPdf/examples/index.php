<?php
session_start();
ini_set("max_execution_time", "8000");
ini_set("memory_limit","1024M");
ob_start();
require ('../../../../config/autoLoaderStudent.php');
require ('../../student/Constantstudent.php');
include("../mpdf.php");
$database = new Database();

$database->connect();
$reportsdao = new ReportsDao();
$adminquizdao = new AdminQuizDao();

$companyInfo = $reportsdao->getCompanyInfo();
while($rowsss = mysql_fetch_array($companyInfo)){
    $address = $rowsss['ADDRESS'];
    $logo = $rowsss['LOGO'];
    $companyName = $rowsss['NAME'];
    $companyEmail = $rowsss['EMAIL'];
}
$testid = $_REQUEST['testId'];
$studentid = $_REQUEST['studentId'];
$testType = $_REQUEST['testType'];
$testname = $reportsdao->getTestName($testid);

$reportSetting = $testname[3];
$reportSetting = explode("#", $reportSetting);

$getMergeTestId = $reportsdao->getMergeTestIds();
$getMergeTestString = '';$getMergeTestString1 = array();
while($row2 = mysql_fetch_array($getMergeTestId)){
    $meregeString = explode("#",$row2['MERGE_TEST_ID']);
    $getMergeTestString = explode(",",$meregeString[0]);
    if(in_array($testid, $getMergeTestString)){
        $getMergeTestString1 = $meregeString;
        break;
    }
}

if($testname[9]!=2){
   $testAnalysis=$reportsdao->getYourTestAnalysis($studentid,$testid,$testType,$getMergeTestString1[1],$getMergeTestString1);
}else{
   $testAnalysis=$adminquizdao->getYourAdminQuizTestAnalysis($studentid,$testid,$testType,$getMergeTestString1[1],$getMergeTestString1);
}
if($testname[9] != 2) {
   $arr = $reportsdao->getTimeManagReport($studentid,$testid,$testType);
} else {
   $arr = $adminquizdao->getAdminQuizTimeManagReport($studentid,$testid,$testType);
}

$idString1 = '';$m=0;
for($m=0;$m<count($arr);$m++){
    if($idString1 == ''){
         if($m<(count($arr)-1)){
            $idString1 = $arr[$m][10].",";
         }else{
            $idString1 = $arr[$m][10];
         }
    }else{
         if($m<(count($arr)-1)){
            $idString1 = $idString1.$arr[$m][10].",";
         }else{
            $idString1 = $idString1.$arr[$m][10];
         }
    }
}

$topperId = $reportsdao->getTopperTime($studentid, $testid, $testType);

// for difficulty Report
if($reportSetting[5] == '1'){
    $difficultArrayTopper="";$topperid="";$difficultArray="";
    if($testname[9]!="2"){
       $difficultArray = $reportsdao->getDifficultyData($studentid, $testid,$testType);
       if($topperId[0] == 2 && $topperId[1] != "") {
          $difficultArrayTopper = $reportsdao->getDifficultyData($topperId[1], $testid,$testType);
       }
    } else{
       $difficultArray = $adminquizdao->getAdminQuizDifficultyData($studentid, $testid,$testType);
       if($topperId[0] == 2 && $topperId[1] != "") {
          $difficultArrayTopper = $adminquizdao->getAdminQuizDifficultyData($topperId[1], $testid,$testType);
       }
    }
}
// for difficulty Report
// for topic Report
if($reportSetting[6] == '1'){
    if($testname[9]!="2"){
       $getTopicReport=$reportsdao->getTopicReport($studentid, $testid, $testType);
       if($topperId[0] == 2 && $topperId[1] != "") {
          $topperMarkTopicWise = $reportsdao->getTopicReport($topperId[1], $testid, $testType);
       }
    } else{
       $getTopicReport=$adminquizdao->getAdminQuizTopicReport($studentid, $testid, $testType);
       if($topperId[0] == 2 && $topperId[1] != "") {
          $topperMarkTopicWise = $adminquizdao->getAdminQuizTopicReport($topperId[1], $testid, $testType);
       }
    }
}
// for topic Report
$zerocount = 0;
for($i = 0; $i < count($arr); $i++) {
    $accrosspercentage = round(((($arr[$i][5] - $arr[$i][6]) / $arr[$i][9]) * 100), 2);
    if($accrosspercentage < 0) {
       $accrosspercentage = 0;
    }
    if($accrosspercentage == 0) {
       $zerocount++;
    }
}
if($reportSetting[4] == '1'){
    $publishResult=$reportsdao->forPublishResult($testid);
    $arrscript=array('st','nd','rd','th','th','th','th','th','th','th');
    if ($testType== 1) {
        $topperIds = $reportsdao->getTopperIdForPractice($testid,$getMergeTestString1[1],$getMergeTestString1);
        $checkteststatus=1;
    } else {
        if($publishResult>=1) {
           $topperIds = $reportsdao->getTopperId($testid,$getMergeTestString1[1],$getMergeTestString1);
           $checkteststatus=1;
        }
    }

    if ($testType ==2 && $publishResult==0 ) {
         "<center style='padding:30px 0px 0px 0px;'></br></br></br></br></br></br>Result Is Not Publish</center>";
    } else {
        for ($i = 0; $i < count($topperIds); $i++) {
            if(count($getMergeTestString1)=='0'){
                if($testname[9]!=2){
                   $studentArr1=$reportsdao->getTimeManagReport($topperIds[$i][0], $testid,$testType);
                }else{
                   $studentArr1=$adminquizdao->getAdminQuizTimeManagReport($topperIds[$i][0], $testid,$testType);
                }
                $studentArr2 = $reportsdao->getSubjectWiseRank($idString1,$topperIds[$i][0],$topperIds[$i][3],$testType);
            }
            //$studentArr1 = $reportsdao->getTimeManagReport($topperIds[$i][0], $testid,$testType);
            if ($testType == 1) {
                $studentArray1 = $reportsdao->getStudentDataForPractice($topperIds[$i][0], $topperIds[$i][3], $topperIds[$i][1],$topperIds[$i][2],$getMergeTestString1[1]);
            } else {
                $studentArray1 = $reportsdao->getStudentData1($topperIds[$i][0], $topperIds[$i][3],2,$topperIds[$i][2],$getMergeTestString1[1]);
            }
            if ($studentid==$topperIds[$i][0]){
                $comeintopper=1;
            }
            $otherTopperArray[$i][0]=$studentArray1;
            $otherTopperArray[$i][1]=$studentArr1;
            $otherTopperArray[$i][2]=$studentArr2;
        }
    }
    if ($comeintopper!=1){
        $studentArray = $reportsdao->getStudentData($studentid, $testid, $testType);
        $subjectWiseRank=array();
        if(count($getMergeTestString1)=='0'){
           $studentArr =  $arr;
           $subjectWiseRank = $reportsdao->getSubjectWiseRank($idString1,$studentid,$testid,$testType);
        }
        $m1=0;
        for($m1=0;$m1<count($arr);$m1++){$l1=0;
           for($l1=0;$l1<count($subjectWiseRank);$l1++){
              if($arr[$m1][10] == $subjectWiseRank[$l1][2]){
                 $subWiseRnk[$m1][0] = $subjectWiseRank[$l1][0];
                 $subWiseRnk[$m1][1] = $subjectWiseRank[$l1][1];
              }
           }
        }
    }
}

// for question Report
if($reportSetting[2] == '1' && $publishResult>=1){
   $quetionReport = $reportsdao->getQuestionAjax($studentid, $testid, $testType,$testname);
}
// for question Report
$html = "";
$html = $html ."<html><head>";
$html = $html ."<link type='text/css' media='all' rel='stylesheet' href='../../../../student/css/style.css'><title>Reports</title>
<style>
.nvClass{
display: block;
color:#000000;
padding: 8px 8px 8px 8px;
font-weight:bold;
}

table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #FFFAFA;
        color:#305491;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
table.gridtable1 {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable1 th {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #FFFAFA;
}
table.gridtable1 td {
	border-width: 1px;
	padding: 2px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
</style>
</head>";
$html = $html ."<body style='background-color:#ffffff'><div class='report-profile-coloumn'>";
 $html = $html ."<div class='profile-middle-white'>";
    $html = $html ."<div id='about' class='drop-coloumn'>";
    
      $html = $html ."<div style='border-bottom:1px solid black;padding:0px 0px 7px 0px'>";
         $html = $html ."<div style='width:50%;float:left'>";
           $html = $html ."<table cellPadding='5'>";
             $html = $html ."<tr>";
               $html = $html ."<td><img src='../../../../document/company/".$logo."'></td>";
             $html = $html ."</tr>";
             $html = $html ."<tr>";
               $html = $html ."<td style='width:180px'><strong style='color:#535352'>Address</strong> :-".$address."</td>";
             $html = $html ."</tr>";
           $html = $html ."</table>";
         $html = $html ."</div>";
         $html = $html ."<div style='width:50%;float:right'>";
           $html = $html ."<table cellPadding='0' align='right' style='font-size:10pt;margin-top:10px'>";
             $html = $html ."<tr>";
               $html = $html ."<td></td>";
             $html = $html ."</tr>";
             $html = $html ."<tr>";
               $html = $html ."<td style='color:#535352'><strong>Student Name : </strong></td><td>".$_SESSION['studentName']."</td>";
             $html = $html ."</tr>";
             $html = $html ."<tr>";
               $html = $html ."<td style='color:#535352'><strong>Test Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong></td><td>".$testname[0]."</td>";
             $html = $html ."</tr>";
             $html = $html ."<tr>";
               $html = $html ."<td style='color:#535352'><strong>Test Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong></td><td>".date('d M Y', strtotime($testAnalysis[16]))."</td>";
             $html = $html ."</tr>";
           $html = $html ."</table>";
         $html = $html ."</div>";
      $html = $html ."</div>";

      //Score card start here
      $html = $html ."<table cellPadding='9'>";
         $html = $html ."<tr>";
           $html = $html ."<td><img src='../../../../student/images/all-report.gif'></td>";
             $html = $html ."<td><strong>Score Card</strong><br> <div style='font-size:11px;color:gray'>Represent your performance against goals.</div></td>";
         $html = $html ."</tr>";
      $html = $html ."</table>";
      
      $html = $html ."<div class='drop' id='load'>";
         $html = $html ."<div class='dropcontent' id='testAnalysis'  style='font-size:11px;border-radius:6px;border:1px solid #b0b0b0'>";
           $html = $html ."<div class='view-report-row' style='padding:7px'>";

             $html = $html ."<div class='view-report-coloumn' style='width:50%;float:left'>";

               $html = $html ."<div style=' width:54%;float:left'><table width='100%'>";
                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left' >Total No. of Student</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[0]."</td>";
                 $html = $html ."</tr>";

                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>Total Marks of Test</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[1]."</td>";
                 $html = $html ."</tr>";
                 
                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>Total Question in Test </td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[2]."</td>";
                 $html = $html ."</tr>";

                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>My Marks</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[4]."</td>";
                 $html = $html ." </tr>";
                 
               $html = $html ."</table></div>";

               $html = $html ."<div style=' width:45%;float:right'><table width='100%'>";
                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>".Constantstudent::$CORRECT_QUESTION."</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[8]."</td>";
                 $html = $html ." </tr>";

                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>".Constantstudent::$RIGHT_MARKS."</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[10]."</td>";
                 $html = $html ."</tr>";

                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>". Constantstudent::$LEFT_QUESTION."</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;color:#C21C1C;'><strong>".$testAnalysis[12]."</strong></td>";
                 $html = $html ." </tr>";

                 if($testAnalysis[5]==0){ $percen = "-NA-";} else{ $percen = $testAnalysis[5];}
                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>My Percentile</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$percen."</td>";
                 $html = $html ." </tr>";

               $html = $html ."</table></div>";

             $html = $html ."</div>";

             $html = $html ."<div class='view-report-coloumn' style='width:49%;float:right'>";

               $html = $html ."<div style=' width:44.4%;float:left'><table width='100%'>";
                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>".Constantstudent::$INCORRECT_QUESTION."</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[9]."</td>";
                 $html = $html ." </tr>";

                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>".Constantstudent::$NEGATIVE_MARKS."</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[11]."</td>";
                 $html = $html ."</tr>";

                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>". "Left Marks"."</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;color:#C21C1C;'><strong>".$testAnalysis[13]."</strong></td>";
                 $html = $html ." </tr>";

                 if($testAnalysis[6]==0){ $rank = "-NA-";} else{ $rank = $testAnalysis[6];}
                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>My Rank</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$rank."</td>";
                 $html = $html ."</tr>";

               $html = $html ."</table></div>";

               $html = $html ."<div style=' width:55.5%;float:right'><table width='100%'>";

                 $html = $html ." <tr>";
                   $html = $html ."<td class='scorecard-left'>Total Time of Test </td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[3]." min</td>";
                 $html = $html ." </tr>";

                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>".Constantstudent::$UNRPODUCTIVE_TIME."</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'><span style='color:#ED8219'>".$testAnalysis[14]."</span> min</td>";
                 $html = $html ." </tr>";

                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>".Constantstudent::$IDLE_TIME."</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'><span style='color:#ED8219'>".$testAnalysis[15]."</span> min</td>";
                 $html = $html ." </tr>";
                 $html = $html ."<tr>";
                   $html = $html ."<td class='scorecard-left'>My Time</td>";
                   $html = $html ."<td class='view-report-colour' style='text-align:right;'>".$testAnalysis[7]." min</td>";
                 $html = $html ." </tr>";
               $html = $html ."</table></div>";
             $html = $html ."</div>";

             $html = $html ."<div class='clearboth'></div>";

           $html = $html ." </div>";
         $html = $html ."  </div>";
      $html = $html ."</div>";

      $html = $html ."<div class='chart-heading'><ul><li class=''><span class='nvClass'>Overview</span></li></ul></div>";
      $html = $html ."<div class='student-detail'>";
          $html = $html ."<table class='gridtable' width='100%'>";
             $html = $html ."<tr>";
               $html = $html ."<th>"."S.No"."</th>";
               $html = $html ."<th style='text-align:left;'>"."Subject Name"."</th>";
               $html = $html ."<th>"."Subject Marks"."</th>";
               $html = $html ."<th>"."Score"."</th>";
               $html = $html ."<th>"."Percentage"."</th>";
             $html = $html ."</tr>";
             $sum = "";
             $sum1 = "";
             $check = 1;
             for ($i = 0; $i < count($arr); $i++) {
               $html = $html ."<tr>";
                 $html = $html ."<td class='scorecard-left' style='text-align:center;'>".($i+1)."</td>";
                 $html = $html ."<td class='scorecard-left' ><strong>".$arr[$i][0]."</strong></td>";
                 $html = $html ."<td class='scorecard-left' style='text-align:center;'>".round($arr[$i][9],2)."</td>";
                 $html = $html ."<td class='scorecard-left'  style='text-align:center;'>".($arr[$i][5]-$arr[$i][6])."</td>";
                 if($sum1 == "") {
                    $sum1 = $arr[$i][9];
                 } else {
                    $sum1 = $sum1 + $arr[$i][9];
                 }
                 if($sum == "") {
                    $sum = $arr[$i][5]-$arr[$i][6];
                 } else {
                    $sum = $sum + $arr[$i][5]-$arr[$i][6];
                 }
                 if(($arr[$i][5]-$arr[$i][6])<0){
                    $per = "0%";
                 } else{
                    $per = round(((($arr[$i][5]-$arr[$i][6])/$arr[$i][9])*100),2). "%";
                 }
                 $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$per."</td>";
               $html = $html ."</tr>";
             }
             $html = $html ."<tr>";
               $html = $html ."<td style='text-align:center;'></td>";
               $html = $html ."<td style='text-align:left;'><strong>"."Total"."</strong></td>";
               $html = $html ."<td style='text-align:center;'>".round($sum1,2)."</td>";
               $html = $html ."<td style='text-align:center;'>".round($sum,2)."</td>";
               if($sum<0){
                   $percentage2 = round(($sum/$sum1)*100,2);
               } else{
                   $percentage2 = round(($sum/$sum1)*100,2);
               }
               $html = $html ."<td style='text-align:center;'>".$percentage2."%</td>";
             $html = $html ."</tr>";
          $html = $html ."</table>";
          $html = $html ."<div class='clearboth'></div>";
      $html = $html ."</div>";

      $html = $html ."<br><div class='all-report-map'>";
          $html = $html ."<div class='list-left width'>";
             $html = $html ."<table>";
               $html = $html ."<tr class='chart-heading'><td><span class='nvClass'>Marks Distribution</span></td>";
                 $html = $html ."<td class='help' style='float:right'>";
                 $html = $html ."</td>";
               $html = $html ."   </tr>";

               if($zerocount!=count($arr)){
                 $html = $html ."<tr><td id='demo-content' class='report'>";
                   $html = $html .$_REQUEST['chart31'];
                 $html = $html ."</td></tr>";
               } else{
                 $html = $html ."<tr><td id='demo-content' class='report down' width='100%' colspan='8'>";
                   $html = $html ."<center >You have zero or negative marks in all subjects</center>";
                 $html = $html ."</td></tr>";
               }
             $html = $html ."</table>";
          $html = $html ."</div>";
      $html = $html ."</div>";

      $html = $html ."<br><div class='all-report-map'>";
          $html = $html ."<div class='list-left width'>";
             $html = $html ."<table>";
               $html = $html ."<tr class='chart-heading'><td><span class='nvClass'>Time Distribution</span></td>";
                 $html = $html ."<td class='help' style='float:right'>";
                 $html = $html ."</td>";
               $html = $html ."   </tr>";
               $html = $html ."<tr><td id='demo-content' class='report'>";
                 $html = $html .$_REQUEST['chart81'];
               $html = $html ."</td></tr>";
             $html = $html ."</table>";
          $html = $html ."</div>";
      $html = $html ."</div>";
      $html = $html ."<pagebreak />";
   //Time Report start here
      if($reportSetting[1]=='1'){
      $html = $html . "<table cellPadding='9'>";
          $html = $html . "<tr>";
          $html = $html . "<td><img src='../../../../student/images/all-report.gif' /></td>";
          $html = $html . "<td class='login-top'><strong>Time Management Report</strong><br /><div style='font-size:11px;color:gray'>Represent conscious control over the amount of time spent on specific activities, especially to increase effectiveness.</div>";
      $html = $html . "</td> </tr></table>";
      $html = $html . "<table width='100%'  class='gridtable' align='center'>";
          $html = $html . "<tr>";
             $html = $html . "<td width='20%' valign='middle' align='center' class='coloumn-black'>Subject</td>";
             $html = $html . "<td valign='middle' align='center' class='coloumn-black'>Attempts[#<span>Wrong</span>]</td>";
             $html = $html . "<td valign='middle' align='center' class='coloumn-black'>Percentage</td>";
             $html = $html . "<td valign='middle' align='center' class='coloumn-black'>Score and Time(in min)</td>";
             $html = $html . "<td valign='middle' align='center' class='coloumn-black'>How did Topper do?</td>";
          $html = $html . "</tr>";
          $toppertimearray = "";
          if($testname[9]!=2){
             $timearray = $arr;
             $topperTime = $topperId;
             if($topperTime[1] != "" || $topperTime[1] != 0) {
                $topperRank = 1;
                $toppertimearray = $reportsdao->getTimeManagReport($topperTime[1], $testid, $testType);
             }
          } else{
             $timearray = $arr;
             $topperTime = $topperId;
             if($topperTime[1] != "" || $topperTime[1] != 0) {
                $topperRank = 1;
                $toppertimearray = $adminquizdao->getAdminQuizTimeManagReport($topperTime[1], $testid, $testType);
             }
          }
          for($i = 0; $i < count($timearray); $i++) {
          $html = $html . "<tr>";
             $html = $html . "<td valign='middle' align='center' class='coloumn-gray'><strong>".$timearray[$i][0]."</strong></td>";
             $html = $html . "<td valign='middle' align='center' class='coloumn-gray'>".(floatval($timearray[$i][1])-  floatval($timearray[$i][4]))."[<span style='color:red'>".$timearray[$i][3]."</span>]</td>";
             if((floatval($timearray[$i][5])-  floatval($timearray[$i][6]))<=0){
                $percentage= "0";
             } else{
                $percentage= (((floatval($timearray[$i][5])-  floatval($timearray[$i][6]))/$timearray[$i][9]) * 100);
             }
             $html = $html . "<td valign='middle' align='center' class='coloumn-gray'>".round($percentage,2)."%"."</td>";
             $html = $html . "<td valign='middle' align='center' class='coloumn-gray'>".(floatval($timearray[$i][5])-floatval($timearray[$i][6])) ." In ".floor($timearray[$i][8]/60).":".($timearray[$i][8]%60) ." Min</td>";
             if($topperTime[0]==1){
                $data="You";
             } else if($topperTime[0] == 2 && $topperTime[1] != NULL){
                $data=(floatval($toppertimearray[$i][5])-floatval($toppertimearray[$i][6]))." In ".floor($toppertimearray[$i][8]/60).":".($toppertimearray[$i][8]%60)."Min";
             } else
                $data="-NA-";
             if($testType!="3"){
                $html = $html . "<td valign='middle' align='center' class='coloumn-gray'>".$data."</td>";
             }
          $html = $html . "</tr>";
          }
      $html = $html . "</table>";
      }
      //Time Report Ends here
//   $html = $html ."<pagebreak />";
      //question report start here
      if($reportSetting[2] == '1' && $publishResult>=1){
      $html = $html ."<table cellPadding='9'><tr>";
      $html = $html ."<td><img src='../../../../student/images/all-report.gif' /></td>";
      $html = $html ." <td class='login-top'><strong>Questions Report</strong><div style='font-size:11px;color:gray'>Represent your time management, efiiciency, question status, score and topper comparison to specify your goals.</div>";
      $html = $html ." </td></tr></table>";

      $html = $html ."<table width='100%' class='gridtable1' align='center'>";
        $html = $html ."<tr>";
          $html = $html ."<td width='8%' valign='middle' align='center' class='coloumn-black'>S. No</td>";
          $html = $html ."<td   width='7%' valign='middle' align='center' class='coloumn-black'>Question Status</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Your Answer</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Correct Answer</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Your Score</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Your Time(in min.)</td>";
          if($testType == "1" || $testType == "2"){
            $html = $html ." <td valign='middle' align='center' class='coloumn-black'>Topper's Time(in min.)</td>";
          }
          $html = $html ."<td    valign='middle' align='center' class='coloumn-black' id='divtext-blanke'>Level</td>";
        $html = $html ."</tr>";
        $l = 1;
        $m = 0;
        $answer = $quetionReport[0];
        $responsetime = $quetionReport[2];
        $topperTime = $quetionReport[1];
        $responsetimeTopper = $quetionReport[3];
        for($i = 0; $i < count($answer); $i++) {
            $serialno = $answer[$i][5];
            $html = $html ."<tr>";
            for($j = 0; $j < 8; $j++) {
                if($testType== "3"){
                    if($j == 6){
                        continue;
                    }
                }
                if ($j == 1) {
                    $html = $html ." <td valign='middle' align='center' class='coloumn-gray' id='divtext'>";
                }else {
                    $html = $html ." <td valign='middle' align='center' class='coloumn-gray'>";
                }
                if ($j == 5) {
                    $html = $html .floor($responsetime[$i]/60).":".$responsetime[$i]%60;
                }
                if ($j == 6) {
                    if ($testType == '1') {
                        if ($topperTime[0] == 1) {
                            $html = $html . 'You';
                        } else if ($topperTime[1] != null && $topperTime[0] == '2') {
                            $html = $html . floor($responsetimeTopper[$i]/60). ":" . $responsetimeTopper[$i]%60;
                        } else {
                            $html = $html . '-NA-';
                        }
                    } else {
                        if ($topperTime[0] == 1) {
                            $html = $html . 'You';
                        } else if ($topperTime[1] != null && $topperTime[0] == 2) {
                            $html = $html . floor($responsetimeTopper[$i]/60) . ":" . $responsetimeTopper[$i]%60;
                        } else {
                            $html = $html . '-NA-';
                        }
                    }
                } else if ($j == 0) {
                    $html = $html . $l;
                } else if ($j == "1" || $j == "2" || $j == "3" || $j == "4") {
                    if($j == "1"){
                       $m = 0;
                    }if($j == "2"){
                       $m = 1;
                    }if($j == "3"){
                       $m = 2;
                    }if($j == "4"){
                       $m = 3;
                    }if($j != "1"){
                       $html = $html . $answer[$i][$m];
                    }
                } else if($j == "7"){
                    $html = $html . $answer[$i][4];
                } else{

                }
                if ($j == "1") {
                    if ($answer[$i][$m] == 'R') {
                        $html = $html ." <a href='#' id='trigger".$i."'>";
                        $html = $html ." <img id='triggerA".$i."'  src='../../../../student/images/wright.gif'/>";
                        $html = $html ." </a>";
                    }if ($answer[$i][$m] == 'W') {
                        $html = $html ." <a href='#' id='trigger".$i."'>";
                        $html = $html ." <img id='triggerA".$i."' src='../../../../student/images/wrong.gif' />";
                        $html = $html ." </a>";
                    }if ($answer[$i][$m] == '') {
                        $html = $html ."<a href='#' id='trigger".$i."'>";
                        $html = $html ."<img id='triggerA".$i."' src='../../../../student/images/left-question.gif' />";
                        $html = $html ."</a>";
                    }
                }
                $html = $html ." </td>";
            }$l++;
            $html = $html ."</tr>";
        }
      $html = $html ."</table>";
      }
      //question report ends here
      $html = $html ."<pagebreak />";
      //compare your self start here
      if($reportSetting[4] == '1'){
      $html = $html ."<table cellPadding='9'>";
      $html = $html ."<tr>";
      $html = $html ."<td><img src='../../../../student/images/all-report.gif' /></td>";
      $html = $html ."<td class='login-top'><strong>Compare Report</strong><br/><div style='font-size:11px;color:gray'>Represent your performance in comparison of toppers.</div></td>";
      $html = $html ."</tr>";
      $html = $html ."</table>";

      $html = $html ."<table width='100%'  class='gridtable' align='center'>";
        $html = $html ."<tr>";
          $html = $html ."<td valign='middle' align='center' width='7%' class='coloumn-black'>S.No</td>";
          $html = $html ."<td valign='middle' align='center' width='20%' class='coloumn-black'>Student Name</td>";
          $html = $html ."<td valign='middle' align='center' width='8%' class='coloumn-black'>TQ</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>MM</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>AQ</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>UQ</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>CQ</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>IQ</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>TS</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>(%)age</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>(%)ile</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Rank</td>";
        $html = $html ."</tr>";
        $i=0;
        for ($i = 0; $i < count($otherTopperArray); $i++) {
          $result1="";$percentage1='';$percentile1="";$rank1='';
          $studentArray1 = $otherTopperArray[$i][0];
          $studentarr1   = $otherTopperArray[$i][1];
          $studentarr2   = $otherTopperArray[$i][2];
          $html = $html ."<tr>";
            if($topperIds[$i][0] == $studentid){
               $showtopper = "*";
            }else{
               $showtopper = " ";
            }
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".($i+1)."<span style='color:red'>".$showtopper."</span></td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'><strong>".strtoupper($topperIds[$i][4])."</strong></td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$studentArray1[7]."</td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$studentArray1[8]."</td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$studentArray1[6]."</td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$studentArray1[2]."</td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$studentArray1[0]."</td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'><span style='color:red'>".$studentArray1[1]."</span></td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'><strong>".$studentArray1['3']."/".$studentArray1[8]."</strong></td>";
            if($studentArray1[3]<=0){
               $percentage1= "0";
            } else{
               $percentage1= round((($studentArray1[3] / $studentArray1[8]) * 100),2);
            }
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$percentage1."%"."</td>";
            if($studentArray1[4] == 0) {
               $percentile1 = "-NA-";
            } else {
               $percentile1 = $studentArray1[4];
            }
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$percentile1."</td>";
            if($studentArray1[5] == 0) {
               $rank1 = "-NA-";
            } else {
               $rank1 = $studentArray1[5] ."<sup>".$arrscript[$studentArray1[5]-1]."</sup>";
            }
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$rank1."</td>";
          $html = $html ."</tr>";
          $sum = "";
          $sum1 = "";
          $check = 1;
          for($cnt = 0; $cnt < count($studentarr1); $cnt++) {
             $html = $html ."<tr>";
               if($cnt == 0){
                 $html = $html ."<td class='scorecard-left' rowspan='".count($studentarr1)."'></td>";
               }
               $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$studentarr1[$cnt][0]."</td>";
               $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$studentarr1[$cnt][1]."</td>";
               $html = $html ."<td class='scorecard-left' style='text-align:center;'>".round($studentarr1[$cnt][9],2)."</td>";
               $html = $html ."<td class='scorecard-left' style='text-align:center;'>".($studentarr1[$cnt][2]+$studentarr1[$cnt][3])."</td>";
               $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$studentarr1[$cnt][4]."</td>";
               $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$studentarr1[$cnt][2]."</td>";
               $html = $html ."<td class='scorecard-left' style='text-align:center;color:red'>".$studentarr1[$cnt][3]."</td>";
               $html = $html ."<td class='scorecard-left' style='text-align:center;'><strong>".($studentarr1[$cnt][5]-$studentarr1[$cnt][6])."/".$studentarr1[$cnt][9]."</strong></td>";
               if(($studentarr1[$cnt][5]-$studentarr1[$cnt][6])<0){
                 $percen = "0%";
               } else{
                 $percen = round(((($studentarr1[$cnt][5]-$studentarr1[$cnt][6])/$studentarr1[$cnt][9])*100),2). "%";
               }
               $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$percen."</td>";
               if($studentarr2[$cnt][1]!=''){
                   $percentile12 = $studentarr2[$cnt][1];
               }else{
                   $percentile12 = "-NA-";
               }
               $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$percentile12."</td>";
               if($studentarr2[$cnt][0]!=''){
                   $rank12 = $studentarr2[$cnt][0];
               }else{
                   $rank12 = "-NA-";
               }
               $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$rank12."</td>";
             $html = $html ."</tr>";
          }
        }

        if($comeintopper!=1){
          $result1="";$percentage1='';$percentile1="";$rank1='';
          $html = $html ."<tr>";
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'>".($i+1)."</td>";
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'><strong>".strtoupper($_SESSION['studentName'])."</strong></td>";
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'>".$studentArray[7]."</td>";
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'>".$studentArray[8]."</td>";
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'>".$studentArray[6]."</td>";
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'>".$studentArray[2]."</td>";
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'>".$studentArray[0]."</td>";
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'><span style='color:red'>".$studentArray[1]."</span></td>";
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'><strong>".$studentArray['3']."/".$studentArray[8]."</strong></td>";
             if($studentArray[3]<=0){
                $percentage1= "0";
             } else{
                $percentage1= round((($studentArray[3] / $studentArray[8]) * 100),2);
             }
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'>".$percentage1."%"."</td>";
             if($studentArray[4] == 0) {
                $percentile1 = "-NA-";
             } else {
                $percentile1 = $studentArray[4];
             }
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'>".$percentile1."</td>";
             if($studentArray[5] == 0) {
                $rank1 = "-NA-";
             } else {
                $rank1 = $studentArray[5] ."<sup>".$arrscript[$studentArray[5]-1]."</sup>";
             }
             $html = $html ."<td valign='middle' align='center' style='background-color:#FFFFCC' class='coloumn-gray'>".$rank1."</td>";
          $html = $html ."</tr>";
          $sum = "";
          $sum1 = "";
          $check = 1;
          for($ct = 0; $ct < count($studentArr); $ct++) {
              $html = $html ."<tr>";
                if($ct == 0){
                  $html = $html ."<td class='scorecard-left' rowspan='".count($studentArr)."'></td>";
                }
                $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$studentArr[$ct][0]."</td>";
                $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$studentArr[$ct][1]."</td>";
                $html = $html ."<td class='scorecard-left' style='text-align:center;'>".round($studentArr[$ct][9],2)."</td>";
                $html = $html ."<td class='scorecard-left' style='text-align:center;'>".($studentArr[$ct][2]+$studentArr[$ct][3])."</td>";
                $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$studentArr[$ct][4]."</td>";
                $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$studentArr[$ct][2]."</td>";
                $html = $html ."<td class='scorecard-left' style='text-align:center;color:red'>".$studentArr[$ct][3]."</td>";
                $html = $html ."<td class='scorecard-left'  style='text-align:center;'><strong>".($studentArr[$ct][5]-$studentArr[$ct][6])."/".$studentArr[$ct][9]."</strong></td>";
                if(($studentArr[$ct][5]-$studentArr[$ct][6])<0){
                  $percen1 = "0%";
                } else{
                  $percen1 = round(((($studentArr[$ct][5]-$studentArr[$ct][6])/$studentArr[$ct][9])*100),2). "%";
                }
                $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$percen1."</td>";
                if($subWiseRnk[$ct][1]!='' && $subWiseRnk[$ct][1]!=0){
                   $prcntile = $subWiseRnk[$ct][1];
                }else{
                   $prcntile = '-NA-';
                }
                $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$prcntile."</td>";
                if($subWiseRnk[$ct][0]!='' && $subWiseRnk[$ct][0]!=0){
                   $subRank2 = $subWiseRnk[$ct][0];
                }else{
                   $subRank2 = '-NA-';
                }
                $html = $html ."<td class='scorecard-left' style='text-align:center;'>".$subRank2."</td>";
              $html = $html ."</tr>";
          }
        }
      $html = $html ."</table>";
      
      $html = $html ."<br><div style='margin-left:20px;'><span style='font-weight:bold;color:red'>*</span>Indicate student come in topper<br><span style='font-weight:bold'>Abbreviations :-</span><br><span style='font-weight:bold'>TQ  :-</span>TOTAL QUESTION<br><span style='font-weight:bold'>MM :-</span>MAXIMUM MARKS<br><span style='font-weight:bold'>AQ :-</span>ATTEMPTED QUESTION<br><span style='font-weight:bold'>UQ :-</span>UNATTEMPTED QUESTION<br><span style='font-weight:bold'>CQ :-</span>CORRECT QUESTION<br><span style='font-weight:bold'>IQ :-</span>INCORRECT QUESTION<br><span style='font-weight:bold'>TS :-</span>TOTAL SCORE</div>";

      $html = $html ."<table>";
        $html = $html ."<tr class='chart-heading'><td><span class='nvClass'>Compare with Toppers</span></td>";
        $html = $html ."   </tr>";
        if($testType == 2 && $publishResult == 0 ){
          $html = $html ."<tr><td id='demo-content' class='report down' width='100%' colspan='8'>";
          $html = $html ."<center>Result Is Not Publish</center>";
          $html = $html ."</td></tr>";
        } else{
          $html = $html ."<br><tr id='demo-content'><td>";
          $html = $html .$_REQUEST['chart21'];
          $html = $html ."</td></tr>";
        }
      $html = $html ."</table>";
      $html = $html ."<pagebreak />";
      }
      //Difficulty Reports start here
      if($reportSetting[5] == '1'){
      $html = $html ."<table cellPadding='9'>";
        $html = $html ."<tr>";
          $html = $html ."<td><img src='../../../../student/images/all-report.gif' /></td>";
          $html = $html ."<td class='login-top'><strong>Difficulty Level Report</strong><br/><div style='font-size:11px;color:gray'>Represent your excellence according to raising difficult level.</div></td>";
        $html = $html ."</tr>";
      $html = $html ."</table>";

      $html = $html ."<table width='100%' class='gridtable' align='center' >";
        $html = $html ."<tr>";
          $html = $html ."<td width='14%' valign='middle' align='center' class='coloumn-black'>Level</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Total Questions</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>".Constantstudent::$CORRECT_INCORRECT_QUESTION."</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>".Constantstudent::$RIGHT_MARKS_NEGATIVE_MARKS."</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Total Marks</td>";
          if($testType== "1" || $testType == "2"){
             $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Topper Marks</td>";
          }
        $html = $html ."</tr>";
        $totalque="";$negativemark="";$rightmark="";$data="";$topperTotalmarks="";$incorecctque="";$correctquest="";
        for($i=0;$i<count($difficultArray);$i++){
          if($totalque == " ") {
            $totalque = floatval($difficultArray[$i][1]);
          } else {
            $totalque = $totalque + floatval($difficultArray[$i][1]);
          }
          if($correctquest == " ") {
            $correctquest = floatval($difficultArray[$i][2]);
          } else {
            $correctquest = $correctquest + floatval($difficultArray[$i][2]);
          }
          if($incorecctque == " ") {
            $incorecctque = floatval($difficultArray[$i][3]);
          } else {
            $incorecctque = $incorecctque + floatval($difficultArray[$i][3]);
          }
          if($rightmark == " ") {
            $rightmark = floatval($difficultArray[$i][4]);
          } else {
            $rightmark = $rightmark + floatval($difficultArray[$i][4]);
          }
          if ($negativemark == " ") {
            $negativemark = floatval($difficultArray[$i][5]);
          } else {
            $negativemark = $negativemark + floatval($difficultArray[$i][5]);
          }
          if($testType== "1" || $testType == "2"){
            if($testType == "2") {
              if($topperId[1] != NULL) {
                if($topperTotalmarks == 0) {
                   $topperTotalmarks = floatval($difficultArrayTopper[$i][4])-floatval($difficultArrayTopper[$i][5]);
                } else {
                   $topperTotalmarks = $topperTotalmarks + floatval($difficultArrayTopper[$i][4])-floatval($difficultArrayTopper[$i][5]);
                }
              } else if ($topperId[0] == 2) {
                $topperTotalmarks = "-NA-";
              } else {
                $topperTotalmarks = "You";
              }
            } else {
              if($topperId[1] != NULL) {
                if($topperTotalmarks == 0) {
                  $topperTotalmarks = floatval($difficultArrayTopper[$i][4]) - floatval($difficultArrayTopper[$i][5]);
                } else {
                  $topperTotalmarks = $topperTotalmarks + floatval($difficultArrayTopper[$i][4]) - floatval($difficultArrayTopper[$i][5]);
                }
              } else if ($topperId[0] == 1) {
                $topperTotalmarks = "You";
              }
            }
            if($testType == '2') {
              if($topperId[0] == '1') {
                $data = "You";
              } else if ($topperId[0] == '2' && $topperId[1] != NULL) {
                $data = round((floatval($difficultArrayTopper[$i][4]) - floatval($difficultArrayTopper[$i][5])),2);
              } else {
                $data = "-NA-";
              }
            } else {
              if($topperId[0] == 1) {
                $data = "You";
              } else {
                $data = round((floatval($difficultArrayTopper[$i][4]) - floatval($difficultArrayTopper[$i][5])),2);
              }
            }
          }
          $html = $html ."<tr>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'><strong>".$difficultArray[$i][0]."</strong></td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$difficultArray[$i][1]."</td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$difficultArray[$i][2]."/<span style='color:#D10D0D'>".$difficultArray[$i][3]."</span></td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$difficultArray[$i][4]."/<span style='color:#D10D0D'>".$difficultArray[$i][5]."</td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".round(($difficultArray[$i][4]-$difficultArray[$i][5]),2)."</td>";
            if($testType == "1" || $testType == "2"){
              $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$data."</td>";
            }
          $html = $html ."</tr>";
        }
        if($topperTotalmarks!="-NA-" && $topperTotalmarks!="You"){
          $topperTotalmarks = round($topperTotalmarks,2);
        }
        $html = $html ."<tr>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'><strong>Total</strong></td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".$totalque."</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".$correctquest."/<span style='color:#D10D0D'>".$incorecctque."</span></td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".round($rightmark,2)."/<span style='color:#D10D0D'>".round($negativemark,2)."</span></td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".round(($rightmark-$negativemark),2)."</td>";
          if($testType== "1" || $testType == "2"){
            $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".$topperTotalmarks."</td>";
          }
        $html = $html ."</tr>";
      $html = $html ."</table>";
      }
      //Difficulty Reports ends here

      //Topic Reports start here
      if($reportSetting[6] == '1'){
      $html = $html ."<table cellPadding='9' style='margin-top:100px'>";
        $html = $html ."<tr>";
          $html = $html ."<td><img src='../../../../student/images/all-report.gif' /></td>";
          $html = $html ."<td class='login-top'><strong>Topic Report</strong><br/><div style='font-size:11px;color:gray'>Represent your excellence according to characterized Topics.</div></td>";
        $html = $html ."</tr>";
      $html = $html ."</table>";

      $html = $html ."<table width='100%' class='gridtable' align='center'>";

        $html = $html ."<tr>";
          $html = $html ."<td width='14%' valign='middle' align='center' class='coloumn-black'>Name</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Total Questions</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>".Constantstudent::$CORRECT_INCORRECT_QUESTION."</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>".Constantstudent::$RIGHT_MARKS_NEGATIVE_MARKS."</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>". Constantstudent::$LEFT_QUESTION_AND_LEFT_QUESTION_MARKS."</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Total Time (in min)</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Total Marks</td>";
          if($testType == "1" || $testType == "2"){
            $html = $html ."<td valign='middle' align='center' class='coloumn-black'>Topper Marks</td>";
          }
        $html = $html ."</tr>";
        $result="";$totalQue = "";$correctQue = "";$incorrectQue = "";$rightMarks = "";$negativeMarks = "";$leftQue = "";$leftMarks = "";$topperTotalMarks="";$testtime="";
        for($i=0;$i<count($getTopicReport);$i++){
          if($totalQue==""){
            $totalQue=floatval($getTopicReport[$i][1]);
          } else
            $totalQue=$totalQue+floatval($getTopicReport[$i][1]);
          if($correctQue==""){
            $correctQue =floatval($getTopicReport[$i][2]);
          } else
            $correctQue=$correctQue+floatval($getTopicReport[$i][2]);
          if($incorrectQue==""){
            $incorrectQue=floatval($getTopicReport[$i][3]);
          } else
            $incorrectQue=$incorrectQue+floatval($getTopicReport[$i][3]);

          if($rightMarks==""){
            $rightMarks=floatval($getTopicReport[$i][5]);
          } else
            $rightMarks=$rightMarks+floatval($getTopicReport[$i][5]);

          if($negativeMarks==""){
            $negativeMarks=floatval($getTopicReport[$i][6]);
          } else
            $negativeMarks=$negativeMarks+floatval($getTopicReport[$i][6]);

          if($leftQue==""){
            $leftQue=floatval($getTopicReport[$i][4]);
          } else
            $leftQue=$leftQue+floatval($getTopicReport[$i][4]);

          if($leftMarks==""){
            $leftMarks=floatval($getTopicReport[$i][7]);
          } else
            $leftMarks=$leftMarks+floatval($getTopicReport[$i][7]);
          if($getTopicReport[$i][8] == null){
            $getTopicReport[$i][8] = 0;
          }
          if($testtime==""){
              $testtime= intval($getTopicReport[$i][8]);
           }
           else
              $testtime=$testtime+intval($getTopicReport[$i][8]);
           
          if($testType == "1" || $testType == "2"){
            if($testType == 2) {
              if($topperId[0] == "1") {
                $data = "You";
              } else if($topperId[0] == 2 && $topperId[1] != NULL){
                if($topperTotalMarks == ""){
                  $topperTotalMarks = floatval($topperMarkTopicWise[$i][5]) - floatval($topperMarkTopicWise[$i][6]);
                } else{
                  $topperTotalMarks = $topperTotalMarks+floatval($topperMarkTopicWise[$i][5]) - floatval($topperMarkTopicWise[$i][6]);
                }
                $data = round((floatval($topperMarkTopicWise[$i][5]) - floatval($topperMarkTopicWise[$i][6])),2);
              } else {
                $data ="-NA-";
              }
            } else {
              if($topperId[0] == "1") {
                $data = "You";
              } else {
                if($topperTotalMarks==""){
                  $topperTotalMarks = floatval($topperMarkTopicWise[$i][5]) - floatval($topperMarkTopicWise[$i][6]);
                } else{
                  $topperTotalMarks = $topperTotalMarks+floatval($topperMarkTopicWise[$i][5]) - floatval($topperMarkTopicWise[$i][6]);;
                }
                $data = round((floatval($topperMarkTopicWise[$i][5]) - floatval($topperMarkTopicWise[$i][6])),2);
              }
            }
            if($topperId[0] == "1") {
              $data1= "You";
            } else if($topperId[0] == "2" && $topperId[1] != null){
              $data1=round($topperTotalMarks,2);
            } else
              $data1="-NA-";
          }
          $html = $html ."<tr>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'><strong>".$getTopicReport[$i][0]."</strong></td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$getTopicReport[$i][1]."</td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$getTopicReport[$i][2]."/<span style='color:#D10D0D'>".$getTopicReport[$i][3]."</span></td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$getTopicReport[$i][5]."/<span style='color:#D10D0D'>".$getTopicReport[$i][6]."</span></td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$getTopicReport[$i][4]."/<span style='color:#D10D0D'>".$getTopicReport[$i][7]."</span></td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".floor($getTopicReport[$i][8]/60).":".($getTopicReport[$i][8]%60)."</td>";
            $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".round((floatval($getTopicReport[$i][5])- floatval($getTopicReport[$i][6])),2)."</td>";
            if($testType == "1" || $testType == "2"){
              $html = $html ."<td valign='middle' align='center' class='coloumn-gray'>".$data."</td>";
            }
          $html = $html ."</tr>";
        }
        $html = $html ."<tr>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'><strong>Total</strong></td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".$totalQue."</td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".$correctQue."/<span style='color:#D10D0D'>".$incorrectQue."</span></td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".round($rightMarks,2)."/<span style='color:#D10D0D'>".round($negativeMarks,2)."</span></td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".$leftQue."/<span style='color:#D10D0D'>".round($leftMarks,2)."</span></td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".floor($testtime/60).":".($testtime%60)."</span></td>";
          $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".round(($rightMarks-$negativeMarks),2)."</td>";
          if($testType == "1" || $testType == "2"){
            $html = $html ."<td valign='middle' align='center' class='coloumn-yellow'>".$data1."</td>";
          }
        $html = $html ."</tr>";
      $html = $html ."</table>";
      }
$html = $html ."</div></div></div></body></html>";


$mpdf=new mPDF('utf-8', 'A4','','',6,6,6,6,0,0,'P');
$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins

$mpdf->defaultheaderfontsize = 10;	/* in pts */
$mpdf->defaultheaderfontstyle = B;	/* blank, B, I, or BI */
$mpdf->defaultheaderline = 1; 	/* 1 to include line below header/above footer */
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->defaultfooterfontsize = 12;	/* in pts */
$mpdf->defaultfooterfontstyle = B;	/* blank, B, I, or BI */
$mpdf->defaultfooterline = 1; 	/* 1 to include line below header/above footer */


//$mpdf->SetHeader('{DATE j-m-Y}|{PAGENO}/{nb}|My document');
//$mpdf->SetFooter('{PAGENO}/{nb}');	/*{DATE j-m-Y H:m} defines footer for Odd and Even Pages - placed at Outer margin */
$header = '
<table width="100%" style="border-top: 1px solid #000000; vertical-align: bottom; font-family:
Arial; font-size: 8pt; color: #000000;margin-bottom:10px"><tr>
<td width="50%" style="text-align: left ;"><span>Powered By Think Exam</span></td>
<td width="50%" style="text-align: right;"><span style="font-weight: bold;">{PAGENO}/{nb}</span></td>
</tr></table>';
$mpdf->SetHTMLFooter($header);
$mpdf->SetHTMLFooter($header,'E');
//$mpdf->SetFooter(array(
//	'L' => array(
//		'content' => '',
//		'font-family' => 'sans-serif',
//		'font-style' => 'B',	/* blank, B, I, or BI */
//		'font-size' => '10',	/* in pts */
//	),
//	'C' => array(
//		'content' => '- {PAGENO}/{nb} -',
//		'font-family' => 'serif',
//		'font-style' => 'BI',
//		'font-size' => '18',	/* gives default */
//	),
//	'R' => array(
//		'content' => 'Powered By Thinkexam',
//		'font-family' => 'monospace',
//		'font-style' => '',
//		'font-size' => '10',
//	),
//	'line' => 1,		/* 1 to include line below header/above footer */
//), 'O','E'	/* defines footer for Even Pages */
//);

$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);
$mpdf->showImageErrors = true;
$mpdf->Output('Reports.pdf','I');
exit;
ob_end_flush();
session_destroy();
?>