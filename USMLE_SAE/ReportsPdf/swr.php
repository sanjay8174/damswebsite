<?php
session_start();
ini_set("max_execution_time", "8000");
ini_set("memory_limit","1024M");
ob_start();
require ('../../../config/autoLoaderStudent.php');
require ('../student/Constantstudent.php');
include("../../sComponents/profile/ProfileDao.php");
include("mpdf.php");

$database = new Database();
$database->connect();
$studentid = $_REQUEST['stuid'];
if(isset($_REQUEST['course_id'])){
    $course_id = $_REQUEST['course_id'];
} else{
    $course_id = $_SESSION['course_id'];
}
$profiledao = new ProfileDao();
$testIdLists = "";
$mergeTestArray = array();$mergeTestString='';
$courseName = '';
$courseName = $profiledao->getCourseName($course_id);
$getStudentProfile = $profiledao->getStudentProfile($studentid);
while ($row1 = mysql_fetch_array($getStudentProfile)) {
    $studentNamenew = $row1['STUDENT_NAME'];
    $batchName = $row1['BATCH_NAME'];
    $enrollMentNo = $row1['ENROLLMENT_NO'];
    $courseName = $row1['COURSE_NAME'];
}

$i=0;$km=0;$mn=0;$mergeTestString='';
$studentTest = array();$studentDataArray = array();$unmergeTest=array();
$studentTest = $profiledao->studentTest($studentid);
while($row = mysql_fetch_object($studentTest)){
    $countArray[$i] = count(explode(",", $row->TEST_ID));
    $studentDataArray[$i][0] = $row->TEST_ID;
    $studentDataArray[$i][1] = $row->IIT_TYPE;
    $studentDataArray[$i][2] = $row->MERGE_TEST_ID;
    $studentDataArray[$i][3] = $row->START_DATE;
    $studentDataArray[$i][4] = $row->TEST_NAME;
    $studentDataArray[$i][5] = $row->EXAM_TYPE;
    $studentDataArray[$i][6] = round($row->TOTAL_MARKS,2);
    $studentDataArray[$i][7] = round($row->TEST_MARKS,2);
    $studentDataArray[$i][8] = $row->TEST_PI;
    $studentDataArray[$i][9] = $row->RANK;
    $studentDataArray[$i][10] = $row->PERCENTILE;
    if($row->MERGE_TEST_ID == 0){
        $unmergeTest[$km] = $row->TEST_ID;
        $km++;
    } else{
        $mergeTestArray[$mn][0] = $row->MERGE_TEST_ID;
        $mergeTestArray[$mn][1] = $row->TWM;
        $mergeTestArray[$mn][2] = $row->SUBJECT_IDS;
        $mergeTestArray[$mn][3] = $profiledao->getTestInfo("$row->TEST_ID",$studentid);
        $mn++;
        if($mergeTestString == ''){
            $mergeTestString = $row->MERGE_TEST_ID;
        } else{
            $mergeTestString = $mergeTestString.",".$row->MERGE_TEST_ID;
        }
    }
    $testNameAbbreviation[$row->TEST_ID] = "T0".($i+1);
    if($testIdLists == ""){
        $testIdLists = $row->TEST_ID;
    } else{
        $testIdLists = $testIdLists .",".$row->TEST_ID ;
    }
    $i++;
}

$subjectArray = array();
$subjectArray = $profiledao->subjectArray(explode(",", $testIdLists));

//exit();
$subjectWiseMarks = array();$subjectWiseArray = array();$getTestMixData=array();
if(count($unmergeTest)>0){
    $subjectWiseMarks = $profiledao->subjectWisemarks($unmergeTest,$studentid);
    $getTestMixData   = $profiledao->getTestMixData($unmergeTest);
}
$totealTest = explode(",", $testIdLists);
$subjectWiseMaxArray = $profiledao->subjectWiseMaxArray($totealTest,$studentid);
$subjectWiseArray = $profiledao->subWiseOverall($totealTest);

$getMixDataArray = array();$lm=0;
$getMixData = $profiledao->getMixData("$mergeTestString");
while($rows1 = mysql_fetch_object($getMixData)){
    $getMixDataArray[$lm][0] = $rows1->TEST_ID;
    $getMixDataArray[$lm][1] = round($rows1->TOTAL,2);
    $getMixDataArray[$lm][2] = round($rows1->TOTAL_AVG,2);
    $lm++;
}
$kl=0;$getAverageForMergeArray=array();
$getAverageForMerge = $profiledao->getAllDataforMergeTest("$mergeTestString");
while($rows12 = mysql_fetch_object($getAverageForMerge)){
    $getAverageForMergeArray[$kl][0] = $rows12->TEST_ID;
    $getAverageForMergeArray[$kl][1] = $rows12->TEST_WISE_MARKS;
    $kl++;
}

if($batchName=='E'){
    $stream = "JEE(Main)";
} else if($batchName == 'T'){
    $stream = "JEE(Main + Advanced)";
} else if($batchName == 'M'){
    $stream = "Pre-Medical";
} else if($batchName == 'A'){
    $stream = "Advanced Engineering";
} else if ($batchName == "") {
    $stream = "NA";
} else {
    $stream = "Pre-Nurture";
}


$colorArray = array();
$colorArray[0] = "background-color:#ffff00 !important;";
$colorArray[1] = "background-color:#E0FFFF !important;";
$colorArray[2] = "background-color:#ffcc99 !important;";
//exit();
$html = "";
$html = $html ."<html><head>";
$html = $html ."
<style>
    body{
        margin:0px ;
        padding:0px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:11px;
        background-color:#ffffff
    }
    .ineerDiv{
        position: relative;
        padding:0px;
        margin-top:0px;
        width:auto;
        font-size:11px;
        height:auto;
    }
    .header{
        color:#424242;font-weight: bold;font-size:9px;margin-top:20px
    }
    .header1{font-size: 16px;width:100%;color:#183883;}
    .header2{font-size: 10px;width:100%;color:#444444;font-weight: bold;margin-top:0px}
    .innerTable{
        border-collapse:collapse;
        font-size: 11px;
        line-height: 20px;
        text-align: center;
        border-right:1px solid #d4d4d4;
        border-top:1px solid #d4d4d4;
        color: black;
        background-color: white;
        vertical-align: middle
    }
    .innerTable td {
        line-height: 15px;
        border: 1px solid #d4d4d4;
        padding:8px
    }
    .class1{
        background-color: #f4f4f4;
    }
    .class1 td{font-weight:normal;padding:3px}
    .class2{background-color: #fe0ffff;}
    .class3 td{padding:3px}
    .class4 td{font-weight:normal;padding:3px;background-color: #e0ffff;}
    .addCss{ border-bottom: 1px solid #D4D4D4;border-top: 1px solid #D4D4D4; border-left: 1px solid #d4d4d4;font-weight: bold}
    .addCss1{border-top: 1px solid #D4D4D4;border-right: 1px solid #D4D4D4;border-left: 1px solid #D4D4D4;}
    .addCss2{border-top: 1px solid #D4D4D4;border-left: 1px solid #D4D4D4;}
    .addCss3{border-right: 1px solid #D4D4D4;}
</style><title>Reports</title><link rel='shortcut icon' href='../../../".Constantstudent::$favicon_image."' />
</head>";
$html = $html ."<body>";
    $html = $html ."<div class='ineerDiv' id='ineerDiv13'>";

         $html = $html."<div class='header'>";
                $html = $html."<div style='float:left;width:150px'>";
                    
                $html = $html."</div>";
                $html = $html."<div style='float:right;margin-right: 0px;text-align: right'>";
                    
                $html = $html."</div>";
        $html = $html."</div>";

        $html = $html."<div class='header1'>";
            $html = $html."<div style='margin: 12px 0px 12px 450px;'>";
                $html = $html."<b>Student Performance Report</b>";
            $html = $html."</div>";
        $html = $html."</div>";

        $html = $html."<div class='header2'>";
            $html = $html."<div style='position: absolute;margin-left: 0px;'>";
                $html = $html."<div style='width:15%;float:left'>REG/ROLL NO&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;".$enrollMentNo."</div><div style='width:25%;float:left'>STUDENT NAME&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;".ucwords(strtolower($studentNamenew))."</div><div style='width:21%;float:left'>COURSE&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;".$courseName."</div><div style='width:20%;float:left'>STREAM&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;".$stream."</div><div style='text-align:right;width:18%;float:left;'>STUDY CENTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".Constantstudent::$STUDY_CENTER."</div>";
            $html = $html."</div>";
            $html = $html."<div style='margin-top:10px;display:none'><div style='float:left;width:10%;'>Test Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</div>";
                $html = $html."<div style='float:right;width:90%;'>";
                    $m4=0;for($m4=0;$m4<count($studentDataArray);$m4++){
                        $noOfMergeTestids = explode(",",$studentDataArray[$m4][0]);
                        if(count($noOfMergeTestids) == 1){
                            $html = $html ."<b>".$testNameAbbreviation[$studentDataArray[$m4][0]]."=<span>".$studentDataArray[$m4][4]."</span></b>&nbsp;&nbsp;&nbsp;&nbsp;";
                        } else{
                            $html = $html ."<b>".$testNameAbbreviation[$studentDataArray[$m4][0]]."=<span>".$studentDataArray[$m4][4]."(Paper1 & Paper2)</span></b>&nbsp;&nbsp;&nbsp;&nbsp;";
                        }
                    }
                $html = $html."</div>";
            $html = $html."</div>";
        $html = $html."</div>";

        $html = $html ."<div style='margin-top:20px;'>";
            $html = $html ."<table class='innerTable' id='tblRow1' width='100%' >";
                        $html = $html ."<tr>";
                            $html = $html ."<td class='addCss' rowspan='2'>Test Detail</td>";
                            $html = $html ."<td class='addCss' rowspan='2'>Date</td>";
                            $html = $html ."<td class='addCss' rowspan='2'>Pattern</td>";
                            if(count($mergeTestArray)>0){
                                $html = $html ."<td class='addCss' rowspan='2'>Paper#</td>";
                            }
                            $lm=0;
                            for($lm=0;$lm<count($subjectArray);$lm++){
                                $backGroundColor = $colorArray[$lm%3];
                                    $html = $html ."<td class='addCss2' colspan='4' bgcolor='#424242' color='#ffffff'><b>" .$subjectArray[$lm][1]. "</b></td>";
                                }
                            $html = $html ."<td class='addCss' rowspan='2'  title='Total Marks'>T.M.</td>";
                            $html = $html ."<td class='addCss' rowspan='2'  title='Maximum Marks'>M.M.</td>";
                            $html = $html ."<td class='addCss addCss3'  rowspan='2' title='Rank'>Rank</td>";
                            $html = $html ."<td class='addCss1' bgcolor='#424242' color='#ffffff'  colspan='7' align='center'><b>OVERALL</b></td>
                        </tr>";
                        $html = $html ."<tr>";
                            $lm=0;
                            for($lm=0;$lm<count($subjectArray);$lm++){
                                $backGroundColor = $colorArray[$lm%3];
                                $html = $html ."<td class='addCss' title='Score'>Score</td>";
                                $html = $html ."<td class='addCss' title='Average Marks'>Avg.</td>";
                                $html = $html ."<td class='addCss' title='Highest Marks'>H.M.</td>";
                                $html = $html ."<td class='addCss' title='Maximum Marks'>M.M.</td>";
                            }
                            $html = $html ."<td class='addCss' align='center' title='Total Marks'>T.M.</td>";
                            $html = $html ."<td class='addCss' align='center' title='Maximum Marks'>M.M.</td>";
                            $html = $html ."<td class='addCss' align='center' title='Highest Marks'>H.M.</td>";
                            $html = $html ."<td class='addCss' align='center' title='Average Marks'>Avg.</td>";
                            $html = $html ."<td class='addCss' align='center' title='Percentage'>(%)age</td>";
                            $html = $html ."<td class='addCss' align='center' title='Percentile'>(%)ile</td>";
                            $html = $html ."<td class='addCss' align='center' title='Rank'>Rank</td>
                        </tr>";
                        $ct=0;$count=0;
                        for($ct=0;$ct<count($studentDataArray);$ct++){
                            $examTypeVar = '';$acronym= '';
                            if($studentDataArray[$ct][5] == '1'){
                                if($studentDataArray[$ct][1] == '1'){
                                    $examTypeVar = "JEE(Main)";
                                    $acronym = "JEE(Main)";
                                }  else{
                                    $examTypeVar = "JEE(Main+Advanced)";
                                    $acronym = "JEE(M+A)";
                                }
                            } else if($studentDataArray[$ct][5] == '2'){
                                $examTypeVar = "JEE(Main)";
                                $acronym = "JEE(Main)";
                            } else if($studentDataArray[$ct][5] == '3'){
                                $examTypeVar = "AIEEE";
                                $acronym = "AIEEE";
                            } else if($studentDataArray[$ct][5] == '4'){
                                $examTypeVar = "NEET-UG";
                                $acronym = "NEET-UG";
                            } else if($studentDataArray[$ct][5] == '5'){
                                $examTypeVar = "AIPMT";
                                $acronym = "AIPMT";
                            } else if($studentDataArray[$ct][5] == '6'){
                                $examTypeVar = "AIIMS";
                                $acronym = "AIIMS";
                            } else if($studentDataArray[$ct][5] == '7'){
                                $examTypeVar = "GUJCET";
                                $acronym = "GUJCET";
                            } else if($studentDataArray[$ct][5] == '8'){
                                $examTypeVar = "STATE - PMT";
                                $acronym = "PMT";
                            } else if($studentDataArray[$ct][5] == '9'){
                                $examTypeVar = "NTSE";
                                $acronym = "NTSE";
                            } else if($studentDataArray[$ct][5] == '10'){
                                $examTypeVar = "KVPY";
                                $acronym = "KVPY";
                            } else if($studentDataArray[$ct][5] == '11'){
                                $examTypeVar = "Other";
                                $acronym = "Other";
                            }
                            $cumlative = $studentDataArray[$ct][8];
                            if($studentDataArray[$ct][6]>0){
                                $per = round(($studentDataArray[$ct][6]/$studentDataArray[$ct][7])*100,2);
                            }else{
                                $per = '0';
                            }
                            $perArray[$ct][1] = $per;

                            if($studentDataArray[$ct][2]!=0){
                                $testIdArray = explode(",",$studentDataArray[$ct][0]);
                                $index=0;
                                for($index=0;$index<count($testIdArray);$index++){
                                    if($count % 2 == 0)
                                    $html = $html . "<tr class='class1'>";
                                    else
                                    $html = $html . "<tr class='class2 class4'>";
                                        if($index == 0){ 
                                            $html = $html ."<td rowspan='". count($testIdArray). "'>". $testNameAbbreviation[$studentDataArray[$ct][0]]. "</td>";
                                            $date1 = date('d M', strtotime($studentDataArray[$ct][3]));
                                            $html = $html ."<td style='font-size:11px' rowspan='". count($testIdArray)."'>" .$date1. "</td>";
                                            $html = $html ."<td align='center' rowspan='". count($testIdArray)."'>". $acronym. "</td>";
                                        }
                                        $html = $html ."<td>Paper". ($index+1). "</td>";
                                        $lm2=0;
                                        for($lm2=0;$lm2<count($mergeTestArray);$lm2++){
                                            if($studentDataArray[$ct][2] == $mergeTestArray[$lm2][0]){
                                                $getTestInfo = array();
                                                $twm = array();
                                                $subIds = array();
                                                $mergeOneTestId = $testIdArray[$index];
                                                $getTestInfo = $mergeTestArray[$lm2][3];
                                                $twm = explode("#",$mergeTestArray[$lm2][1]);
                                                $subIds = explode(",",$mergeTestArray[$lm2][2]);
                                                $individualMarks = explode(",",$twm[$index]);
                                                $cl1=0;$testTotalMarks='';
                                                for($cl1=0;$cl1<count($subjectArray);$cl1++){
                                                    $backGroundColor = $colorArray[$cl1%3];
                                                    $cl2=0;$subP1='';$avgP1='';
                                                    if(in_array($subjectArray[$cl1][0], $subIds)){
                                                        for($cl2=0;$cl2<count($subIds);$cl2++){
                                                            if($subIds[$cl2] == $subjectArray[$cl1][0]){
                                                                $km1=0;
                                                                for($km1=0;$km1<count($subjectWiseArray);$km1++){
                                                                    if($subjectWiseArray[$km1][0] == $mergeOneTestId && $subjectWiseArray[$km1][1] == $subjectArray[$cl1][0]){
                                                                        $avgP1 = round($subjectWiseArray[$km1][3],2);
                                                                        $highMarks = round($subjectWiseArray[$km1][2],2);
                                                                    }
                                                                }
                                                                for($km1=0;$km1<count($subjectWiseMaxArray);$km1++){
                                                                      if($subjectWiseMaxArray[$km1][2] == $mergeOneTestId && $subjectWiseMaxArray[$km1][0] == $subjectArray[$cl1][0]){
                                                                        $maxMarks = round($subjectWiseMaxArray[$km1][3],2);

                                                                    }

                                                                }

                                                                $subP1 = round($individualMarks[$cl2],2);
                                                                if($testTotalMarks == ''){
                                                                    $testTotalMarks = $individualMarks[$cl2];
                                                                }else{
                                                                    $testTotalMarks = $testTotalMarks+$individualMarks[$cl2];
                                                                }
                                                                $html = $html ."<td style='". $backGroundColor. "'>". $subP1. "</td>";
                                                                $html = $html ."<td style='". $backGroundColor. "'>". $avgP1. "</td>";
                                                                $html = $html ."<td style='". $backGroundColor. "'>". $highMarks. "</td>";
                                                                $html = $html ."<td style='". $backGroundColor. "'>". $maxMarks. "</td>";
                                                            }
                                                        }
                                                    } else{
                                                        $html = $html ."<td style='". $backGroundColor. "'>-</td>";
                                                        $html = $html ."<td style='". $backGroundColor. "'>-</td>";
                                                        $html = $html ."<td style='". $backGroundColor. "'>-</td>";
                                                        $html = $html ."<td style='". $backGroundColor. "'>-</td>";
                                                    }
                                                }
                                                $t=0;
                                                $testMarks1='';$rankTest1='';
                                                for($t=0;$t<count($getTestInfo);$t++){
                                                    if($getTestInfo[$t][0] == $testIdArray[$index]){
                                                        $testMarks1 = round($getTestInfo[$t][2],2);
                                                        $rankTest1 = $getTestInfo[$t][3];
                                                    }
                                                }
                                                $html = $html . "<td>".round($testTotalMarks,2). "</td>";
                                                if( $testMarks1!=''){
                                                    $html = $html . "<td>". $testMarks1. "</td>";                                                    
                                                } else{
                                                    $html = $html . "<td>-</td>";
                                                }

                                                if($rankTest1!=''){
                                                    $html = $html . "<td>". $rankTest1. "</td>";
                                                } else{
                                                    $html = $html . "<td></td>";
                                                }
                                            }
                                        }
                                        if($index == 0){
                                            $lm1=0;
                                            for($lm1=0;$lm1<count($getMixDataArray);$lm1++){
                                                if($studentDataArray[$ct][2] == $getMixDataArray[$lm1][0]){
                                                    $highestmarks = $getMixDataArray[$lm1][1];
                                                    $totalAvg = $getMixDataArray[$lm1][2];
                                                }
                                            }
                                            $html = $html ."<td rowspan='2' align='center'>" .round($studentDataArray[$ct][6],2). "</td>";
                                            $html = $html ."<td rowspan='2' align='center'>" .round($studentDataArray[$ct][7],2). "</td>";
                                            $html = $html ."<td rowspan='2' align='center'>" .round($highestmarks,2). "</td>";
                                            $html = $html ."<td rowspan='2' align='center'>"  .round($totalAvg,2). "</td>";
                                            $html = $html ."<td rowspan='2' align='center'>". $per. "%</td>";
                                            $html = $html ."<td rowspan='2' align='center'>". round($studentDataArray[$ct][10],2). "</td>";
                                            $html = $html ."<td rowspan='2' align='center'>". $studentDataArray[$ct][9]. "</td>";
                                        }
                                  $html = $html.  "</tr>"; } $count++;
                            }   else {
                                if($count % 2 == 0)
                                       $html = $html.  "<tr class='class1'>";
                                   else
                                       $html = $html. "<tr class='class2 class4'>";
                                    $html = $html. "<td>". $testNameAbbreviation[$studentDataArray[$ct][0]]. "</td>";
                                    $date1 = date('d M', strtotime($studentDataArray[$ct][3]));
                                    $html = $html ."<td style='font-size:11px'>".$date1."</td>";
                                    $html = $html ."<td align='center' title='". $examTypeVar. "'>". $acronym. "</td>";
                                    if(count($mergeTestArray)>0){
                                        $html = $html ."<td align='center' >-</td>";
                                    }
                                    $ln=0;
                                    for($ln=0;$ln<count($subjectArray);$ln++){
                                        $backGroundColor = $colorArray[$ln%3];
                                        $kl1=0;
                                        $sMarks = '';$sAvg='';
                                        $find = 0;
                                        for($kl1=0;$kl1<count($subjectWiseMarks);$kl1++){
                                            if($subjectWiseMarks[$kl1][2] == $studentDataArray[$ct][0]){
                                                if($subjectArray[$ln][0] == $subjectWiseMarks[$kl1][0]){
                                                    $find = 1;
                                                    $sMarks = round($subjectWiseMarks[$kl1][1],2);
                                                    $sMaxMarks = round($subjectWiseMarks[$kl1][3],2);
                                                }
                                            }
                                        }
                                        $kn1 = 0;
                                        for($kn1=0;$kn1<count($subjectWiseArray);$kn1++){
                                            if($subjectWiseArray[$kn1][0] == $studentDataArray[$ct][0]){
                                                if($subjectArray[$ln][0] == $subjectWiseArray[$kn1][1]){
                                                    $sAvg = round($subjectWiseArray[$kn1][3],2);
                                                    $highmarks = round($subjectWiseArray[$kn1][2],2);
                                                }
                                            }
                                        }
                                        if($find ==1){
                                            $html = $html ."<td style='".$backGroundColor."'>".$sMarks."</td>";
                                        }else{
                                            $html = $html ."<td style='".$backGroundColor."'>-</td>";
                                        }
                                        if($find ==1)
                                            $html = $html ."<td style='". $backGroundColor. "'>".$sAvg. "</td>";
                                            else
                                            $html = $html ."<td style='". $backGroundColor. "'>-</td>";
                                        if($find ==1)
                                            $html = $html ."<td style='". $backGroundColor. "'>".$highmarks. "</td>";
                                        else
                                           $html = $html ."<td style='". $backGroundColor. "'>-</td>";
                                        if($find ==1)
                                            $html = $html ."<td style='". $backGroundColor. "'>".$sMaxMarks. "</td>";
                                        else
                                           $html = $html ."<td style='". $backGroundColor. "'>-</td>";
                                    } 
                                    $html = $html ."<td>". $studentDataArray[$ct][6]. "</td>";
                                    $html = $html ."<td>". $studentDataArray[$ct][7]. "</td>";
                                    $html = $html ."<td>". $studentDataArray[$ct][9]. "</td>";
                                    $highestmarks='';$totalAvg='';$cumlative=0;
                                    $kl=0;
                                    for($kl=0;$kl<count($getTestMixData);$kl++){
                                        if($getTestMixData[$kl][0] == $studentDataArray[$ct][0]){
                                            $highestmarks = $getTestMixData[$kl][2];
                                            $totalAvg     = round($getTestMixData[$kl][1],2);
                                        }
                                    }
                                    $html = $html ."<td align='center'>". round($studentDataArray[$ct][6],2). "</td>";
                                    $html = $html ."<td align='center'>". round($studentDataArray[$ct][7],2). "</td>";
                                    $html = $html ."<td align='center'>". round($highestmarks,2). "</td>";
                                    $html = $html ."<td align='center'>". round($totalAvg,2). "</td>";
                                    $html = $html ."<td align='center'>". $per. "%</td>";
                                    $html = $html ."<td align='center'>". round($studentDataArray[$ct][10],2). "</td>";
                                    $html = $html ."<td align='center'>". $studentDataArray[$ct][9]. "</td>
                                </tr>";
                                $count++;
                            }
                        } 
                  $html = $html ."</table>";
        $html = $html ."<br><div>";
        
    $html = $html ."</div>";
    $html = $html."<div style='padding:10px'><span style='color:red;font-weight:bold'>Note:- </span><b>M.M. - Maximum Marks | H.M. - Highest Marks | Avg. - Average Marks | T.M. - Total Marks</b></div>";
$html = $html ."</body></html>";
//echo $html;
//exit();
$mpdf=new mPDF('utf-8', 'A4-L','','',10,10,10,10,10,10,'L');
$mpdf->mirrorMargins = 1;	

$mpdf->defaultheaderfontsize = 10;	
$mpdf->defaultheaderfontstyle = B;	
$mpdf->defaultheaderline = 1; 
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->defaultfooterfontsize = 12;	
$mpdf->defaultfooterfontstyle = B;
$mpdf->defaultfooterline = 1; 	

$mpdf->SetHTMLFooter($header);
$mpdf->SetHTMLFooter($header,'E');
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);
$mpdf->showImageErrors = true;
$mpdf->Output('Reports.pdf','I');
ob_end_flush();
?>