<!doctype html>
<html>
<head>
    <?php session_destroy(); ?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<title>DAMS USMLE Self Assessment Exam</title>
<link rel="shortcut icon" href=images/favicon.ico type=image/x-icon />
<link rel=icon href=images/favicon.ico type=image/x-icon />
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/font/stylesheet.css" rel="stylesheet" type="text/css">
<link href="css/screens.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery1.7.1.js"></script>
<script type="text/javascript" src="js/register.js"></script>
<!--[if IE 7]><link rel="stylesheet" href="css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="css/ie9.css" type="text/css" /><![endif]-->
<!--[if IE 10]><link rel="stylesheet" href="css/ie10.css" type="text/css" /><![endif]-->
<script type="text/javascript">
function forgot_pwd(){
    	$("#dialog").css("display" , "block");
	$(".res_main_pop").removeClass("pop_hide");
	}
function showhide(){
	$(".res_main_pop").addClass("pop_hide");
	setTimeout(function(){  
	    $("#dialog").css("display" , "none");
	}, 300);
	
	}
</script>
</head>

<body>
  <div class="pre_loader" id="preloader" style="display:none">
<div class="pre_loader_bg"></div>
<div class="ldr_line"></div>
<span class="icon"></span>
</div>  
  <div id="dialog" style="display: none">
  <div class="res_pop_bg"></div>     
    <div class="res_main_pop">
     <span onclick="showhide()" ></span>
     <div class="pop_title">Please enter your email id</div>
     <p>
      <label>Email / User Id</label>
      <input type="text" id="forget_email">
     </p>
     <!--<p>
      <label>Password</label>
      <input type="text">
     </p>-->
     <p>
      <!--<a href="#" class="forgot_pwd">Forgot your Password ?</a>-->
      <input type="submit" value="Submit" id="forget_login" class="button">
     </p>
    </div>
</div>
<!--<div class="top_hdr">
 <div align="center" class="main_wpr no_border">
  <div class="hdr_ctnt">
   <h3>Online Application</h3>
   <h1>Registrati<span class="reg_o"><img src="images/reg_o.png"></span>n</h1>
  </div>
  <div class="pg_title">
   <span>Candidate Login</span>
  </div>
 </div>
</div>-->
<div class="header">
 <div class="main_wpr no_border">
  <a href="index.php?p=Home" class="logo">
   <img src="images/logo.png">
  </a>
 </div>
</div>
<form action="index.php?p=profile&c=0" method="post" name="success" id="success">
    <input type="hidden" name="studentIdConfirm" id="studentIdConfirm" value="" >
</form>
<div class="main_wpr no_border" style="margin-bottom:0;">
 <h2 class="inr_pg_hdr">Candidate Login</h2>
</div>
<div class="main_wpr">
 <div class="after_wpr">
  <div class="login_left">
   <img src="images/smily.jpg"><br>
   <h2 class="lgn_ttl">Login to Fill/Submit/View Application Form</h2>
  </div>
  <div class="login_right">
  <form name="loginForm" method="post" id="loginForm" action="index.php?p=submit">
   <div class="inr_login">
    <div class="flds_cntr">
    <label>Email Id</label>
    <input type="text" autocomplete="off" id="userId" name="userId">
    </div>
    <div class="flds_cntr">
    <label>Password</label>
    <input type="password" id="password" name="password" autocomplete="off">
    </div>
    <div class="register_cntr flds_cntr" style="margin-bottom:0;">
     <a href="#" class="forgot_pwd" onclick="forgot_pwd()">Forgot Password ?</a>
     <a id="loginSubmit" class="reg_btn">Login</a>
     <input type="hidden" name="mode" id="mode" value="" >
     <input type="hidden" name="studentId" id="studentId" value="" >
    </div>
   </div>
  </form>
  </div>
 </div>
</div>
</div>
</body>
</html>
