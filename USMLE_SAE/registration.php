<!doctype html>
<?php
session_start();
$ob = new funDams();
$time = date("d-m-Y");
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<title>DAMS USMLE Self Assessment Exam</title>
<link rel="shortcut icon" href=images/favicon.ico type=image/x-icon />
<link rel=icon href=images/favicon.ico type=image/x-icon />
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/font/stylesheet.css" rel="stylesheet" type="text/css">
<link href="css/screens.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery1.7.1.js"></script>
<script type="text/javascript" src="js/register.js"></script>
<script src="cal/jquery.js"></script>   
<link rel="stylesheet" href="calci/css/pickmeup.css" type="text/css" />
<script type="text/javascript" src="calci/js/jquery.pickmeup.js"></script>
<script type="text/javascript" src="calci/js/demo.js"></script>
<!--[if IE 7]><link rel="stylesheet" href="css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="css/ie9.css" type="text/css" /><![endif]-->
<!--[if IE 10]><link rel="stylesheet" href="css/ie10.css" type="text/css" /><![endif]-->

<script type="text/javascript">
$(document).ready(function(){
    $("#changecpatch").click(function(){
        $("#captchaimg").attr("src", "captcha/captcha.php?"+(new Date()).getTime()); // browser will save the captcha image in its cache so add '?some_unique_char' to set new URL for each click
    });
});
</script>


</head>

<body>
<div class="pre_loader" id="preloader" style="display:none">
<div class="pre_loader_bg"></div>
<div class="ldr_line"></div>
<span class="icon"></span>
</div>
    <form action="index.php?p=submit" method="post" name="registration" id="registration">
<div class="header">
 <div class="main_wpr no_border">
  <a href="index.php?p=Home" class="logo">
   <img src="images/logo.png">
  </a>
 </div>
</div>
<div class="main_wpr no_border" style="margin-bottom:0;">
 <h2 class="inr_pg_hdr">REGISTRATION FORM</h2>
</div>
<div class="main_wpr">
 <div class="inst_wpr">
  <div class="ins_online_sub"><img src="images/instruction.png" class="img">Instructions For Submitting Online Application</div>
  <p class="inst_p">To fill the Registration Form, Kindly use Internet Explorer (Minimum Version 8) Mozilla (Minimum Version 14)  or Google Chrome (Minimum Version 20)</p>
  <b>Read the below instruction carefully, before filling the form: </b>

  <ul class="inst_points">
      <li>To confirm your seat please fill up the form you will get it on your mail id for the payment. If you have not received any mail for the payment then please mail us on <b>usmle@damsdelhi.com</b> 
         <p><b>Or</b></p> 
         <p>contact undersigned</p>         
          <p>DAMS USMLE Edge:<b>011-40094009</b></p></li>
<!---   <li>Candidate has to fill in the below mentioned details to receive the User Id and Password.</li>
   <li>Candidate will receive the User Id and Password in the registered email address and through SMS on the registered mobile number.</li>
   <li>Candidate can login with the User Id and Password to complete the application for CBT 2016.</li>
   <li>Candidate must provide correct Name, Date of Birth, Mobile Number and Email Address as these details cannot be changed after the registration complete.</li>-->
  </ul>
 </div>
 <div class="reg_form_wpr">
  <fieldset>
   <legend>Register to get User Id and Password</legend>
    <div class="flds_cntr">
    <label>Student Type<b>*</b></label>
    <select id="StudentType" name="StudentType" onchange="showRollNo()">
     <option value="" selected>--Select--</option>
     <option value="Damsonians">Damsonians</option>
     <option value="Non_Damonsian">Non Damonsian</option>
    </select>
    </div>
   <div class="flds_cntr" style="display:none;" id="rollNoShow">
    <label>Roll No<b></b></label>
    <input type="text" autocomplete="off" id="rollNo" name="rollNo">
   </div>
   <div class="flds_cntr">
    <label>Candidate Name<b>*</b></label>
    <input type="text" autocomplete="off" id="candidateName" name="candidateName" onblur="disableNextField('candidateName','dob')">
   </div>
   <div class="flds_cntr">
    <label>Date of Birth<b>*</b></label>
    <input type="text" autocomplete="off" readonly id="dob" name="dob" value="">
    <input type="hidden" autocomplete="off" onpaste="return false" readonly id="dobH" name="dobH"  value="<?php echo $time; ?>">
   </div>
   <div class="flds_cntr">
    <label>Email Address<b>*</b></label>
    <input type="text" autocomplete="off" onpaste="return false" id="emailAddress" name="emailAddress" onblur="disableNextField('emailAddress','confirmEmail')" onchange="checkDuplicateEmail()">
   </div>
   <div class="flds_cntr">
    <label>Confirm Email Address<b>*</b></label>
    <input type="text" autocomplete="off" onpaste="return false" id="confirmEmail" name="confirmEmail" onblur="disableNextField('confirmEmail','mobile')">
   </div>
   <div class="flds_cntr">
    <label>Mobile<b>*</b></label>
    <input type="text" autocomplete="off" onpaste="false" id="mobile" maxlength="10" name="mobile" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" onblur="disableNextField('mobile','confirmMobile')">
   </div>
   <div class="flds_cntr">
    <label>Confirm Mobile<b>*</b></label>
    <input type="text" autocomplete="off" onpaste="false" id="confirmMobile" maxlength="10" name="confirmMobile" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');" onblur="disableNextField('confirmMobile','country')">
   </div>
      <div class="flds_cntr">
    <label>Gender<b>*</b></label>
    <select class="gender_css" id="gender" name="gender">
     <option value="" selected>--Select--</option>
     <option value="male">Male</option>
     <option value="female">Female</option>
    </select>
   </div>
   <div class="flds_cntr">
    <label>College Name<b>*</b></label>
     <input type="text" autocomplete="off" onpaste="return false" id="college" name="college">
    
   </div>
     
  </fieldset>
  <fieldset>
   <legend>Security Code</legend>
   <div class="flds_cntr">
    <label>Enter Captcha code<b>*</b></label>
    <input type="text" autocomplete="off" value="" id="securityCode" name="securityCode" onblur="disableNextField('securityCode','iagree')" onpaste="return false">
   </div>
 <div class="flds_cntr" style="margin-left:0;">
       <img style=" float:left;" id="captchaimg" name="captchaimg" src="captcha/captcha.php" style="vertical-align: middle;" alt="" />
       <p id="changecpatch" style="float:left; margin-top:11px; cursor:pointer;"><img src="images/refresh1.png" title="New Captcha"></p>
   </div>
  </fieldset>
  
  <fieldset>
   <legend>Declaration</legend>
   <div class="flds_cntr declaration">
    <div class="cbox_agree">
     <input type="checkbox" class="checkbox" id="iagree" name="iagree">
     <label class="i_agree" for="iagree">I Agree<b>*</b></label>
    </div>
    
    <p>I hereby declare that I have carefully read the instructions. All particulars started in this Registration Form are true correct to best of my
    knowledge and belief. If any information provided is false or incorrect, I shall abide by the actions and decisons taken by USMLE MOCK.</p>
   </div>
  </fieldset>
  <div class="register_cntr">
   <a id="checkRegisterationForm" class="reg_btn">Register Now</a>
   <!--<input type="submit" value="Register Now" class="reg_btn">-->
  </div>
  </div>
 </div>
</div>
<input type="hidden" value="" name="mode" id="mode" >
<input type="hidden" value="0" name="emailValid" id="emailValid" >
</form>
</body>
</html>
<script>
   function showRollNo(){
       if(($('#StudentType').val()) == 'Damsonians'){
           $('#rollNoShow').show();
       }else if(($('#StudentType').val()) == 'Non_Damonsian'){
           $('#rollNoShow').hide();
       }else{
           $('#rollNoShow').hide();
       }
   }    
   <?php if($_SESSION['success']==1){?>
       alert('Thank you for Registration in DAMS-MOCK USMLE');
   <?php } ?>
</script>
