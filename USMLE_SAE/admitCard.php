<?php
include_once 'ReportsPdf/mpdf.php';
include('Barcode39.php');
$studentId = $_REQUEST['id'];
$eventId = constant::$eventId;
$database = new Database();
$database->connect();
$sql = "SELECT S.STUDENT_NAME,ST.CBT_ID,C.NAME centreName,C.ADDRESS,TS.START_TIME,TS.END_TIME,C.LANDMARK,C.CONTACT_PERSON,C.CONTACT_NUMBER,R.NAME regionName FROM STUDENT S LEFT JOIN STUDENT_TEST ST ON S.STUDENT_ID = ST.STUDENT_ID"
        . " LEFT JOIN CENTRE C ON ST.CENTRE_ID=C.CENTRE_ID LEFT JOIN LAB L ON C.CENTRE_ID=L.LAB_ID LEFT JOIN REGION R ON C.REGION_ID=R.REGION_ID LEFT JOIN TEST_SESSION TS ON ST.TEST_SESSION_ID=TS.TEST_SESSION_ID"
        . " LEFT JOIN EVENT_MANAGE EM ON TS.EVENT_MANAGE_ID=EM.EVENT_MANAGE_ID WHERE S.STUDENT_ID='$studentId'AND EM.EVENT_ID='$eventId' limit 0,1";

$result = $database->executeQuery($sql);
$database->executeQuery("UPDATE STUDENT SET ACTIVE = 2 WHERE STUDENT_ID='$studentId'");

while($row = mysql_fetch_object($result)){
    $studentName = $row->STUDENT_NAME;
    $appId = $row->CBT_ID;
    $centre = $row->centreName;
    $address = $row->ADDRESS;
    $city = $row->regionName;
    //date('h:i:s a', strtotime($row->END_TIME));
    $time  = date('h:i a', strtotime($row->START_TIME))." - ".date('h:i a', strtotime($row->END_TIME));
    $examDate = constant::$exam_date;
    $landmark = $row->LANDMARK;
    $coordinator = $row->CONTACT_PERSON;
    $coordinatorNo = $row->CONTACT_NUMBER;
}
if($studentName=="")$studentName='-NA-';
if($appId=="")$appId="-NA-";
if($centre=="")$centre="-NA-";
if($address=="")$address="-NA-";
if($city=="")$city="-NA-";
if($time=="")$time="-NA-";
if($examDate=="")$examDate="-NA-";
if($landmark=="")$landmark="-NA-";
if($coordinator=="")$coordinator="-NA-";
if($coordinatorNo=="")$coordinatorNo="-NA-";

//Barcode Create Here for Application ID
$nameApp = "CBT".$appId.".gif";
$bc = new Barcode39("CBT".$appId);
$bc->draw("BarCode/$nameApp");
//Ends

if(file_exists("PHOTO/".$studentId.".jpg")){
    $image = "PHOTO/$studentId".".jpg";
}else{
    $image = "images/student_icon.jpg";
}

$mpdf=new mPDF('utf-8', 'A4','','',10,10,10,10,10,10,'P');
$mpdf->mirrorMargins = 1;    // Use different Odd/Even headers and footers and mirror margins
$mpdf->defaultheaderfontsize = 10;    /* in pts */
$mpdf->defaultheaderfontstyle = B;    /* blank, B, I, or BI */
$mpdf->defaultheaderline = 1;     /* 1 to include line below header/above footer */
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->defaultfooterfontsize = 12;    /* in pts */
$mpdf->defaultfooterfontstyle = B;    /* blank, B, I, or BI */
$mpdf->defaultfooterline = 1;     /* 1 to include line below header/above footer */
$mpdf->SetTitle('My Application Form');
$mpdf->SetDisplayMode('fullpage');
$html='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Admit Card</title>
<style type="text/css">
body{ margin:0; padding:0; font-family:Arial, sans-serif; font-size:12px; color:#333;}
.outer_wpr{ width:800px; margin:0 auto; overflow:hidden; border:1px solid #ccc;margin-top:50px; }
.logo_hdr, .admain_hding{ text-align:center; border-bottom:1px solid #ccc;}
.logo_hdr{ padding:10px 0;}
.admain_hding{ font-size:16px; font-weight:bold; padding:15px 0;}
.imgdtls_dntr{ clear:both; border-bottom:1px solid #ccc; overflow: hidden; height: 200px;}
.imgdtls_dntr .prof_img{ float:left; width:20%; height: 200px;}
.imgdtls_dntr .prof_img img{ width:100%;}
.imgdtls_dntr .signtr_cntr{ height:20px;}
.imgdtls_dntr .signtr_cntr img{ width:100%;}
.imgdtls_dntr .stdnt_dtls{ float:left; width:43%; height: 200px; box-shadow:0px 0px 0px 1px #ccc;}
.imgdtls_dntr .centr_dtls{ float:right; width:37%; height: 200px;}
.imgdtls_dntr h2{ text-align:center; line-height:40px; margin:0; font-size:14px; border-bottom:1px solid #ccc;}
.candetails, .exam_centr{ display:inline-block; width:100%; text-align:left;}
.candetails p{ display:block; margin:10px 0 20px 10px;}
.exam_centr{ padding:10px;}
.inr_hding{ vertical-align:top; line-height:20px; height:20px;}
.invdetails{ display: inline-block; width: 100%; background:#eee; border-bottom:1px solid #ccc;}
.invdetails h2{ font-size:16px; margin-bottom:20px; text-align:center;}
.invdetails p{ float:left; width:48%; margin:5px 0 15px 1%; text-align:left;}
.invdetails p b{ width:100px;}
.notes{ text-align:left;margin-left:4px}
.notes h3{color:#a4531e; margin:10px 0 15px 10px;}
.notes ul{}
.notes ul li{ width:100%; margin-bottom:15px; list-style:decimal;}
.signature{ height:40px;}
.signature img{ width:100%;}




</style>
</head>

<body>
<img src="BarCode/'.$nameApp.'" height="50" style="float:right"/>
<div class="outer_wpr">
 <div class="logo_hdr">
  <img src="images/logo.png">
  <div class="signtr_cntr"></div>
 </div>
 <div class="admain_hding">ADMIT CARD - DAMS-CBT 2015</div>
 <div class="imgdtls_dntr">
  <div class="prof_img"><img src="'.$image.'" height="200" width="130">
  <div style="padding-left:30px;padding-top:20px; ">Signature</div>
  </div>
  <div class="stdnt_dtls">
   <h2>Candidate Details</h2>
   <div class="candetails">
    <p><b>Name : </b>'.$studentName.'</p>
    <p><b>Application Id. : </b>'.$appId.'</p>
    <p><b>Centre : </b>'.$centre.' ('.$city.') </p>
    <p><b>Exam Date : </b>'.$examDate.'</p>
    <p><b>Exam Time : </b>'.$time.'</p>
    <p><b>Reporting Time : </b>30 min prior the exam.</p>
   </div>
  </div>
  <div class="centr_dtls">
   <h2>Centre for Examination</h2>
   <div class="exam_centr">
    <b>Address: </b>'.$address.'
        <br><b>Landmark: </b>'.$landmark.'
   </div>
  </div>
 </div>
 <div class="invdetails" style="display:none;">
   <h2>Invigilator Details</h2>
   <p><b>Name :'.$coordinator.'</b></p>
   <p><b>Mobile :'.$coordinatorNo.'</b></p>
 </div>
 <div class="notes">
 <h3>Note :</h3>
 <ul>
  <li><b>If photograph is Missing:</b> You can paste the color passport size photograph in the given area.</li>
  <li>The candidates are requested to carry at least 1 ID proof with them along with this admit card.</li>
  <li>Candidates will be allowed to appear for the examination after verifying the credentials from the records available with the Presiding Officer.</li>
  <li>Candidates are advised to reach the test center 30 minutes prior to the exam. Candidates coming late at the test centre will not be permitted to
   enter the examination hall.</li>
   <li>Candidates will not be allowed to give exam in any other shift timings apart from which is alloted to the candidate.</li>
 </ul>
</div>
<div style="text-align:center;margin-top:40px;margin-bottom:10px;margin-right:10px;">
For any queries contact us: - <a href="https://damsdelhi.com" style="float:left">www.damsdelhi.com</a> or mail to 
cbt@damsdelhi.com
</div>
</div>
</body>
</html>';
$mpdf->SetHTMLFooter($header);
$mpdf->SetHTMLFooter($header,'E');
$mpdf->WriteHTML($html);
$mpdf->showImageErrors = true;
$mpdf->Output('Admit Card.pdf','D');
ob_end_flush();

?>