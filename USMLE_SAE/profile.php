<!DOCTYPE html>
<?php
session_start();
$ob = new funDams();
$studentId = $_REQUEST['studentIdConfirm'];
if($studentId=='0' || $studentId== ''){
    header("Location:index.php?p=Login");
}
$getSubmitData = $ob->getStudentAllData($studentId); 
$_SESSION['studIdReg'] = $studentId;
$address1 = $getSubmitData->ADDRESS . "," . $getSubmitData->cCITY;
$eventId = $getSubmitData->CBT_EVENT_ID;
$appId = $getSubmitData->APPLICATION_ID;
$centreId = $getSubmitData->CENTRE_ID;

?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <script type="text/javascript" src="js/fbShare.js"></script>
        <script type="text/javascript" src="js/jquery1.7.1.js"></script>
        <script type="text/javascript" src="js/register.js"></script>
        <title>DAMS USMLE Self Assessment Exam</title>
        <link rel="shortcut icon" href=images/favicon.ico type=image/x-icon />
        <link rel=icon href=images/favicon.ico type=image/x-icon />
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="css/font/stylesheet.css" rel="stylesheet" type="text/css">
        <link href="css/screens.css" rel="stylesheet" type="text/css">
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/responsiveTabs.js"></script>
        <!--[if IE 7]><link rel="stylesheet" href="css/ie7.css" type="text/css" /><![endif]-->
        <!--[if IE 8]><link rel="stylesheet" href="css/ie8.css" type="text/css" /><![endif]-->
        <!--[if IE 9]><link rel="stylesheet" href="css/ie9.css" type="text/css" /><![endif]-->
        <!--[if IE 10]><link rel="stylesheet" href="css/ie10.css" type="text/css" /><![endif]-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript">
            function showtultip() {
                $(".logout_tultip").css("display", "block");
            }
            function hidetultip() {
                $(".logout_tultip").css("display", "none");
            }
        </script>
    </head>

    <body>
        <div class="header">
            <div class="main_wpr no_border">
                <p  class="info"><b style="margin-left:95px;">For any query contact on: <span style="color:#0089D0;">helpdesk@damsdelhi.com</span></b></p>
                
                <a href="javascript:void(0)" class="logo">
                    <img src="images/logo.png">
                </a>
                <div class="lgout_prof">
                    <div class="pf_nm_cntr" onMouseOver="showtultip()" onMouseOut="hidetultip()">
                        <a href="#" class="prof_pic_name">
                            <?php if (file_exists("PHOTO/" . $studentId . ".jpg")) { ?>
                                <img src="PHOTO/<?php echo $studentId . '.jpg' ?>">
                            <?php } else { ?>
                                <img src="PHOTO/photo.jpg">
                            <?php } ?>
                            <span><?php echo $getSubmitData->STUDENT_NAME ?></span>
                        </a>
                        <div class="logout_tultip">
                            <ul>
                                <li><a href="index.php?p=Login" class="last">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_wpr no_border" style="margin-bottom:0;">
            <h2 class="inr_pg_hdr">Online Application</h2>
            <div class="hding_pgination">Home&nbsp;/&nbsp;Profile - Your payment has been received successfully.</div>
        </div>
        <div class="main_wpr" style="border:none;">
            <div class="inst_wpr" style="display:none">
                <p class="inst_p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text ever.</p>
                <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit</b>
                <ul class="inst_points">
                    <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </li>
                    <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea </li>
                    <li>Commodo consequat. Duis aute irure dolor in reprehenderit </li>
                    <li>In voluptate velit esse cillum dolore eu fugiat nulla pariatur</li>
                </ul>
            </div>

            <div class="responsive-tabs">
                <h2>Application Details</h2>
                <div class="tabsshowhide">
                    <div class="tabs_ctnt_wpr">
                        <h2 class="inr_tb_ttl1">Personal Details</h2>
                        <div class="flds_cntr">
                            <label>Candidate Name</label>
                            <input type="hidden" name="studentId" id="studentId" value="<?php echo $studentId; ?>   ">
                            <input type="hidden" name="mode" id="infoSave" value="infoSave">
                            <input type="text" id="sName" readonly name='sName' value="<?php if ($getSubmitData->STUDENT_NAME != "") {
                                echo $getSubmitData->STUDENT_NAME;
                            } else {
                                echo "-NA-";
                            } ?>" >
                        </div>
                        <div class="flds_cntr">
                            <label>Mobile Number</label>
                            <input maxlength="10" readonly type="text" id="mobile" name='mobile' value="<?php if ($getSubmitData->MOBILE != "") {
                                echo $getSubmitData->MOBILE;
                            } else {
                                echo "-NA-";
                            } ?>">
                        </div>
                        <div class="flds_cntr">
                            <label>DOB</label>
                            <input type="text" id="dob" readonly name='dob' value="<?php if ($getSubmitData->DOB != "") {
                                echo date("d-m-Y", strtotime($getSubmitData->DOB));
                            } else {
                                echo "-NA-";
                            } ?>">
                        </div>
                        <div class="flds_cntr">
                            <label>Gender</label>
                            <input type="text" id="gender" readonly name="gender" value="<?php if ($getSubmitData->GENDER != "") {
                                echo ucfirst($getSubmitData->GENDER);
                            } else {
                                echo "-NA-";
                            } ?>">
                        </div>
                        <h2 class="inr_tb_ttl1">Current Address Detail</h2>
                        <div class="flds_cntr">
                            <label>Address 1</label>
                            <input type="text" id="address1" readonly name='address1' value="<?php if ($getSubmitData->ADDRESS != "") {
                                echo $getSubmitData->ADDRESS;
                            } else {
                                echo "-NA-";
                            } ?>">
                        </div>
                        <div class="flds_cntr">
                            <label>Address 2</label>
                            <input type="text" id="address2" readonly name='address2' value="<?php if ($getSubmitData->ADDRESS1 != "") {
                                echo $getSubmitData->ADDRESS1;
                            } else {
                                echo "-NA-";
                            } ?>">
                        </div>
                        <div class="flds_cntr">
                            <label>City</label>
                            <input type="text" id="cCity" readonly name='cCity' value="<?php if ($getSubmitData->cCITY != "") {
                                echo $getSubmitData->cCITY;
                            } else {
                                echo "-NA-";
                            } ?>">
                        </div>

                        <div class="flds_cntr">
                            <label>Pincode</label>
                            <input type="text" maxlength="6" readonly id="pincode" name='pincode' value="<?php if ($getSubmitData->cPINCODE != "") {
                                    echo $getSubmitData->cPINCODE;
                                } else {
                                    echo "-NA-";
                                } ?>">
                        </div>

                        <div class="flds_cntr">
                            <label>State</label>
                            <input type="text" id="CstateName" name="CstateName" readonly value="">
                            <select id="Cstate" name="Cstate" disabled style="display: none">
                                <option value="0">Please select your State</option>
<?php
$state = $ob->getState();
while ($row = mysql_fetch_array($state)) {
    echo "<option value='$row[STATE_ID]'>$row[STATE_NAME]</option>";
}
?>
                            </select>
                        </div>
                        <div class="flds_cntr">
                            <label>Country</label>
                            <input type="text" id="CnationalityName" name="CnationalityName" readonly value="">
                            <select id="Cnationality" name="Cnationality" disabled style="display:none">
                                <option value="0">Please select your country</option>
<?php
$country = $ob->getCountry();
while ($row = mysql_fetch_array($country)) {
    echo "<option value='$row[COUNTRY_ID]'>$row[COUNTRY_NAME]</option>";
}
?>
                            </select>
                        </div>


                        <div class="register_cntr" style="clear:both;">
                        </div>
                    </div>
                </div>
                    <?php 
                        if($getSubmitData->REGION1 == 31)
                            constant::$BOS = 0;
                    if (constant::$BOS == 1) {
                        $getRegion = $ob->getExamCentre();
                        ?>
                    <h2>Online Booking System</h2>
                    <div class="tabsshowhide">
                    <?php if ($getSubmitData->REGION1 != 0) { ?>
                      <!--<script src="https://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAA7j_Q-rshuWkc8HyFI4V2HxQYPm-xtd00hTQOC0OXpAMO40FHAxT29dNBGfxqMPq5zwdeiDSHEPL89A" type="text/javascript"></script>
                      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&libraries=geometry"></script>-->

                            <div class="choos_center_wpr">
                                <h2>Hurry Up Choose Your Test Centre</h2>
                                <div style="padding:25px; display: none;" id="centreLockedMsg">
                                    Congratulations! you have locked your centre. 
                                    <br />
                                </div>
                                <div class="slct_yr_loc">
                                    <label>Your preferred city</label>
                                    <input type="hidden" id="mySelectCentre" value="<?php echo $getSubmitData->CENTRE_ID; ?>" />
                                    <input type="hidden" id="mySelectSession" value="<?php echo $getSubmitData->SESSION_ID; ?>" />
                                    <select onchange="getCentres('1')" name="region" id="region" disabled>
                                        <option value="0">SELECT YOUR REGION.</option>
        <?php while ($row = mysql_fetch_array($getRegion)) { ?>
                                            <option value="<?php echo $row['REGION_ID']; ?>"><?php echo $row['NAME']; ?></option>
        <?php } ?>
                                    </select>
                                </div>
                                <div class="flds_cntr appform_full" style="margin:0;">
                                    <div class="appform_note"><b>Note:</b>&nbsp;&nbsp;You can select only one centre into your preferred test location .<br ><label style="margin-left:40px;">Once OBS will close you can't change your test centre.</label></div>
                                </div>
                                
                                <div class="centr_change">
                                    <h3 class="yr_center_hding">Centres in your city</h3>
                                    <label id="msgForCentre"></label>
                                </div>
                                
                                <input type="hidden" id="address11" value="<?php echo $address1; ?>">
                                <input type="hidden" id="address21" value="kashmiri gate,New Delhi">
                                <div class="choos_center" id="centres" style="height:430px; overflow-y: auto;outline: none">
                                </div>
                                
                                <div class="centr_change">
                                    <a href="#" id="changeCentre" class="cchange_btn" style="display: none">Lock My Centre.<i class="sprite"></i></a>
                                    <input type="hidden" id="centreAvailable" value="0"/>
                                    <label id="msgForCentre"></label>
                                </div>
                                <div id="distance" style="float:right;margin-right:16%;position: static;"></div>

                            </div>
    <?php
    } else if ($getSubmitData->REGION1 == 0) {
        echo '<div class="choos_center_wpr">You have not selected any centre location at the time of registration.</div>';
    }
    ?>
                    </div>
                    
                    
<?php } 
if (constant::$admitCard == 1) { ?>
                    <h2>Download Admit Card</h2>
                    <div class="tabsshowhide">
                        <div class="tabs_ctnt_wpr">
                            <div class="dwnld_admit">
                                <a href="javascript:void(0);" class="dwnlMyAdmitCard" >Click here</a> to download your Admit Card
                            </div>
                        </div>
                    </div>
<?php } ?>
<?php  

        $studentCentreCount = $ob->executeQuery("SELECT * FROM STUDENT WHERE STUDENT_ID = '$studentId' AND REGION1 IN(14,24,27,35,39,40,54,64,71)");
        $studentCentreCount = mysql_num_rows($studentCentreCount);
        if($studentCentreCount >=1)
            constant::$myTestCentre = 0;
        
           
if (constant::$myTestCentre == 1) { ?>
                    <h2>My Test Centre</h2>
                    <div class="tabsshowhide">
                        
                        <div class="tabs_ctnt_wpr">
<?php                              
        $studentId = $_REQUEST['studentIdConfirm'];
        $getSubmitDataForCentre = $ob->getStudentCentre($studentId);
        $centreAlloted = mysql_num_rows($getSubmitDataForCentre);
        if($centreAlloted == 0) :
            echo '<div class="stuMsg"><p>Dear Student,</p><p> You centre will be available soon, Please be patient. Thanks for your cooperation! Please download your admit card as you centre will be available in your city
                   </p></div>';
            
        else :
        $rowData = mysql_fetch_object($getSubmitDataForCentre);    
        $centreName = $rowData->CENTRE_NAME;
        $address = $rowData->ADDRESS;
        $startDate = $rowData->START_TIME;
        $endDate = $rowData->END_TIME;
        $time  = date('h:i a', strtotime($startDate))." - ".date('h:i a', strtotime($endDate));
        echo '<div class="boldData">Dear Student, Congratulations your seat has been locked! You can download the admit card on 30th September 2015.
                </div><div class="myCentre">'
                . '<div><b>Centre Name:</b> '.$centreName.'</div>'
                . '<div><b>Address:</b> '.$address.'</div>'
                . '<div><b>Time:</b> '.$time.'</div>'
                . '<div><b>Date of CBT Exam: '.constant::$exam_date.'</b></div>'
            . '</div>';
        endif;
        ?>
                        </div>
                        <div class="boundry" style="display: none;">
                            <p class="boldParaData"><b style="color:red;">Note:</b> For Pondicherry, Mangalore and Manipal candidates. </p>      
                        <p class="boldParaData">Your centre will be available in next 1-2 Days, Thanks for your cooperation! Please download your admit card on 30/09/2015</p>
                        </div>
                        </div>
                    
<?php  } if(constant::$checkMyResult == 1) { ?>
<!--**************************ADDED BY SUMIT, CHECK RESULT************************************-->
 <h2>Check My Result</h2>
  <div class="tabsshowhide">
    <div class="tabs_ctnt_wpr">
 
<style type="text/css">
 .slctno{user-select: none;
      -khtml-user-select: none;
      -o-user-select: none;
      -moz-user-select: -moz-none;
      -webkit-user-select: none;
    }
.body { font-family: Arial, sans-serif; margin:50px 0 50px 0px; padding:0; }
.main_wpr1{ width:850px; margin:0 auto; overflow:hidden;}
.logo_section, .blubar, .rnk_dtls, .stdnt_dtls{ display:inline-block; width:100%; text-align:center;}
.logo_section{ padding:10px 0;}
.blubar{background: rgb(239, 112, 63) none repeat scroll 0% 0%;<!--background:#009e39;-->
color: rgb(255, 255, 255);
color:#fff;
font-weight: bold;
text-transform: uppercase;
font-size: 18px;
padding: 15px 0px;
}
.rnk_dtls{ border-bottom:1px solid #ccc; height:90px; position:relative;}
.rnk_dtls.rank{background-color:#fff0f1;}
.rnk_dtls .rnk{ width: 33.3%;float:left; height:90px; line-height:90px; color: #3b5a7d; font-weight:bold; font-size:40px; border-right:1px solid #ccc; text-transform:uppercase;}
.rnk_dtls .rnk span{ font-size:18px; }
.rnk_dtls .name_rank{ float:left; text-align:left; width:49%;}
.rnk_dtls .name_rank p{ display:inline-block; width:100%; margin: 17px 0 0 0;}
.rnk_dtls .name_rank p span{ float: left; width:150px; margin-left: 12px;}
.rnk_dtls p.abc{background: #5197D2;padding: 25px 27px 25px 25px;color: #fff;margin:0; line-height: 22px;
font-weight: 600;}
.rnk_dtls p{margin-bottom:10px;margin-top:15px;margin-left:15px;margin-right:27px;}
.stdnt_dtls{ background: #e8fcfc;}
.stdnt_dtls .imgcntr{ float:left; width:30%; text-align:center;}
.stdnt_dtls .imgcntr img{ display:inline-block; width:170px; height: 198px; border:1px solid #ddd;}
.stdnt_dtls .student_dtls{ float:left; text-align:left; width:50%; min-height:198px; color: #505050; position:relative;padding: 25px;
box-sizing: border-box;}
.stdnt_dtls .student_dtls h3{ margin:0; padding-left:12px; }
.stdnt_dtls .student_dtls h3.gdprf{ position:absolute; bottom:0; font-size:18px; color: #3b5a7d;}
.stdnt_dtls .student_dtls ul{ display:inline-block; width:100%; margin: 14px 0 0 0; list-style:none; padding:0;}
.stdnt_dtls .student_dtls ul li{ float:left; margin-top:15px; width:100%;}
.stdnt_dtls .student_dtls ul li span{ float: left; width:162px; margin-left:12px;}
.toprnks{ display:inline-block; width:100%;}
.toprnks .tprnkdhding{ background:#3b5a7d; color: #fff; font-weight: bold; width:200px; height:35px; line-height:35px; text-align:center; text-transform:uppercase;}
.toprnks .tprnkdhding:after{ float: right; content:""; border: 17px solid transparent; border-top: 0; border-left: 0; border-bottom: 35px solid #3b5a7d;  margin-right: -17px;}
.toprnks ul{ display:inline-block; width:100%; margin:0; padding:0; list-style:none; border-top:1px solid #ccc;}
.toprnks ul li{ float:left; width:33%; height:215px; text-align:center; border-right:1px solid #ccc; padding:15px 0;}
.toprnks ul li.last{ border:none;}
.toprnks ul li img{ width:100px; height:118px; border:1px solid #ddd;}
.toprnks ul li .rnkrs_dtls{}
.toprnks ul li .rnkrs_dtls h4{ font-size:16px; margin:10px 0 0 0;}
.toprnks ul li .rnkrs_dtls p{ display:inline-block; width:100%; margin:0; margin-top:7px; font-size:14px;}
.ftrtxt{ display: none;font-weight:normal; font-size:14px; margin-top:-5px;  border-bottom:none;}
.imgcntr{padding: 25px 0px;text-align:center;overflow: hidden;}
.imgcntr .stdnt_info{width: 33.3%; height:335px; padding: 10px; float:left;   box-sizing: border-box;border: 1px solid #ef703f;}.imgcntr label{display:block;font-weight: bold;   border: 1px solid #ef703f;background: #ef703f;color: #fff !important;padding: 8px 0;}
.imgcntr img{padding:5px;height:120px;width:100px;border: 1px solid #eee;}
.imgcntr .student_rankdetails{text-align: left;}
.imgcntr .student_rankdetails p{float: left; width:100%;   padding: 7px;  margin: 0;  box-sizing: border-box;}
.imgcntr .student_rankdetails p span{float:left;padding:0;}
.imgcntr .student_rankdetails p span.span1_inf{width:114px !important;}

.imgcntr span{padding:5px;}
.student_dtls_video{ float:left; text-align:left; width:0%;  min-height:198px; color: #505050;  position:relative;margin-left:90px; margin-top: 20px;}
.imgcntr .stdnt_info:nth-child(1){ border-left:none; }
.imgcntr .stdnt_info:nth-child(2){ border-left:none;  border-right:none; }
.imgcntr .stdnt_info:nth-child(3){ border-right:none; }
.student_rankdetails b{ float:left; width:120px; margin-right:5px; font-weight:normal;}



/*Responsive WebPage*/

@media screen and (max-width: 800px) {
body{ margin:1% 0;}
.main_wpr{ width:98%;}
.main_wpr1{ width:100%;}
.rnk_dtls{ height:auto !important;}
table, tr, td, th{ text-align:left !important;}
.stdnt_dtls .student_dtls{width:100%;}
.stdnt_dtls .student_dtls ul li{ width:100%;}
.stdnt_dtls .imgcntr, .stdnt_dtls .student_dtls, .toprnks ul li{ width:100%;}
.rnk_dtls .rnk {   width: 100%;   border-right: medium none;   border-bottom: 1px solid #CCC;   height: 65px;   line-height: 65px;}
.rnk_dtls .name_rank {   width: 100%;   padding: 3px 25px;}
.imgcntr .student_rankdetails p {   padding: 11px 5px 5px 5px !important;}
.imgcntr .stdnt_info {padding: 3px;}
.rnk_dtls .name_rank p {
    margin: 7px 0px;
}
}
@media screen and (max-width: 768px) {
.tabs_ctnt_wpr{ padding:0; padding-right:4px;}
  
}
@media screen and (max-width: 640px) {
.stdnt_dtls .student_dtls h3{ font-size:17px;}
.stdnt_dtls .student_dtls ul{ margin-top:0;}
.header {    height: 108px;}
.imgcntr .stdnt_info {
    display: inline-block;
    float: none;
    width: 45%;
    padding: 10px;margin: 10px 5px 4px 10px;
    box-sizing: border-box;}
.imgcntr .stdnt_info:nth-child(1) {  border-left: 1px solid #EF703F;}
.imgcntr .stdnt_info:nth-child(2) {  border-left: 1px solid #EF703F; border-right: 1px solid #EF703F;}
.imgcntr .stdnt_info:nth-child(3) {  border-right: 1px solid #EF703F;}

}
@media screen and (max-width: 600px) {
	.imgcntr .stdnt_info {    width: 48%;   }
.imgcntr .stdnt_info:nth-child(1) {  border-left: 1px solid #EF703F; margin: 10px 10px 4px 0px;}
.imgcntr .stdnt_info:nth-child(2) {  border-left: 1px solid #EF703F; border-right: 1px solid #EF703F; margin: 10px 0px 4px 0px;}
.imgcntr .stdnt_info:nth-child(3) {  border-right: 1px solid #EF703F; margin: 10px 5px 4px 10px;}
}
@media screen and (max-width: 550px){.imgcntr .stdnt_info {
    display: inline-block;
    float: none;
    width: 95%;
    padding: 10px;
    box-sizing: border-box;
}
.imgcntr .stdnt_info:nth-child(1) { margin: 10px 10px 10px 10px;}
.imgcntr .stdnt_info:nth-child(2) {margin: 10px 10px 10px 10px;}
.imgcntr .stdnt_info:nth-child(3) {  margin: 10px 10px 10px 10px;}
.imgcntr .student_rankdetails p {   padding: 11px 0px 5px 22% !important;}
.imgcntr .student_rankdetails p span.span1_inf {    width: 152px !important;}
}
@media screen and (max-width: 480px) {
.rnk_dtls{ height:auto;}
.rnk_dtls .rnk { width:100%; border-right: none; border-bottom:1px solid #ccc; height: 65px; line-height: 65px;}
.rnk_dtls .name_rank{ width:100%; padding:3px 25px;}
.rnk_dtls .name_rank p { margin: 7px 0;}
.stdnt_dtls .imgcntr, .stdnt_dtls .student_dtls, .toprnks ul li{ width:100%;}
.stdnt_dtls .imgcntr img{ height:auto; width:auto;}
.stdnt_dtls .student_dtls h3.gdprf{ position:static;}
.stdnt_dtls .student_dtls h3{ padding:10px 0; background:#505050; color:#fff; text-align:center; }
.stdnt_dtls .student_dtls h3.gdprf{ color:#fff; font-size:16px;}
.stdnt_dtls .student_dtls ul li{ margin:7px 0;}
.tabs_ctnt_wpr{ padding:0; padding-right:10px;}
table, th, td{ font-size:12px; word-break:break-all;}

.logo{ width:50%;}
.logo img{ width:90%;}}
@media screen and (max-width: 400px){
	.stdnt_dtls .student_dtls ul li span {width: 163px;}
	.imgcntr .student_rankdetails p {
    padding: 11px 0px 5px 9% !important;}
}
@media screen and (max-width: 360px) {
.stdnt_dtls .student_dtls {padding: 25px 0px 25px 0px;}
.rnk_dtls .name_rank {   width: 100%;   padding: 3px 10px;}
.student_dtls_video {margin-left: 45px;}
  .imgcntr .student_rankdetails p {padding: 11px 0px 5px 1% !important;}
  .imgcntr .stdnt_info{width:99%;}
  .imgcntr .stdnt_info:nth-child(1) { margin: 10px 1px 10px 1px;}
.imgcntr .stdnt_info:nth-child(2) {margin: 10px 1px 10px 1px;}
.imgcntr .stdnt_info:nth-child(3) {  margin: 10px 1px 10px 1px;}
}

@media screen and (max-width: 320px) {	
.logo_section img{ width:80%;}
.stdnt_dtls .student_dtls ul li{ width:100%;}
.stdnt_dtls .student_dtls ul li span{ width: 144px;}
.stdnt_dtls .student_dtls h3, .stdnt_dtls .student_dtls h3.gdprf{ font-size:13px;}
}


</style>

<?php 

    $result = $ob->getStudentResult($studentId);
    while($resultData = mysql_fetch_array($result)) :
        $rightQue = $resultData['RIGHT_QUE'];
        $wrongQue = $resultData['WRONG'];
        $leftQue = $resultData['LEFT_QUE'];
        $totalMrks = $resultData['NBE_TOTAL_MARKS'];
        $rank = $resultData['NBE_RANK'];
    endwhile;
?>
         <div class="body">
         <div class="main_wpr1">
 <div class="blubar">DAMS CBT 2015 - Result</div>
 <div  style="border:3px solid #558EB3; border-top:0;">
<div class="rnk_dtls " style="text-align: left;text-align: justify;display: inline-block;">
    <p class="abc">Dear Candidate,<br>Thank you for attempting the DAMS-CBT 2015 on 4/Oct/2015. Best of luck for the exam !!.<br />You can check your result below.
    <br />
    The scores have been calculated by IRT method using two parameter model. The scores are displayed till 4th point of decimal. If in case there was a tie after 4th decimal calculation, scores were calculated to 5th decimal.
    </p>
    
    <div class="imgcntr">

    <div class="stdnt_info"><label style="color: #505050;">Rank 1</label><img src="PHOTO/<?php echo '5936.jpg'?>">
	
	<div class="student_rankdetails">
	

    <p><span class="span1_inf">Right Question:</span><span  style="color: green; width:107px;">237</span></p>
    <p><span class="span1_inf">Wrong Question:</span><span style="color: red; width:107px;">63</span></p>
    <p><span class="span1_inf">Unattempted Question:</span><span style="color: blue; width:107px;">0</span></p>
    <p><span class="span1_inf">Total Marks:</span><span style="color: green; width:107px;">1447.6937&nbsp;/&nbsp;1500</span></p>

   
	</div></div>
	
    <div class="stdnt_info"><label style="color: #505050;">Rank 2</label><img src="PHOTO/<?php echo '13481.jpg'?>">
	<div class="student_rankdetails">
	

    <p><span class="span1_inf">Right Question: </span><span  style="color: green; width:107px;">238</span></p>
    <p><span class="span1_inf">Wrong Question: </span><span style="color: red; width:107px;">62</span></p>
    <p><span class="span1_inf">Unattempted Question:</span><span style="color: blue; width:107px;">0</span></p>
    <p><span class="span1_inf">Total Marks: </span><span style="color: green; width:107px;">1428.2305&nbsp;/&nbsp;1500</span></p>

	</div></div>
    <div class="stdnt_info"><label style="color: #505050;">Rank 3</label><img src="PHOTO/<?php echo '5619.jpg'?>">
	<div class="student_rankdetails">
	

    <p><span class="span1_inf">Right Question: </span><span  style="color: green; width:107px;">230</span></p>
    <p><span class="span1_inf">Wrong Question: </span><span style="color: red; width:107px;">70</span></p>
    <p><span class="span1_inf">Unattempted Question:</span><span style="color: blue; width:107px;">0</span></p>
    <p><span class="span1_inf">Total Marks: </span><span style="color: green; width:107px;">1427.6531&nbsp;/&nbsp;1500</span></p>

   
	</div></div>
  </div>        
  <div class="name_rank">
  </div>
  <div style="margin-top:25px;display:none" title="download pdf"><a href="index.php?p=profilePdf&studentId=<?php echo $studentId; ?>&t=<?php echo $totalStudent; ?>"><img src="img/pdf1.png" width="45px" height="35px"></a></div>
 </div>
 <div class="rnk_dtls rank" style="float:left">
  <div class="rnk" style="display:block">
   R<span>ank</span> - <b><?php if($totalMrks == '0' || $totalMrks == 0) :
       echo 'N/A';
   else :
       echo $rank;
       endif;
   ?></b>
  </div>
  <div class="name_rank">
      <p style="color: #505050;"><span><b>Candidate Name </b> </span><b>:&nbsp;&nbsp;</b><?php echo ucwords(strtolower($getSubmitData->STUDENT_NAME)); ?></p>
   <p style="color: #505050;"><span><b>Application Id </b> </span><b>:&nbsp;&nbsp;</b><?php echo $appId;?></p>
  </div>
  

  <div style=" line-height:32px; margin-top:0px; width:105px; float:right; color:black;display:none;" title="download pdf">
      <a href="https://thinkadmission.com/BankPowerCbt/paper/<?php echo $testId."CBTS"; ?>.pdf">
          <label style="margin-top:10px;color:black;cursor: pointer;">Solution:<img src="img/pdf1.png" style="float:right" width="45px" height="35px"></label></a></div>
   <div style=" line-height:32px; margin-top:17px; width:120px; float:right; color:black; display: none;" title="download pdf">
      <a href="https://thinkadmission.com/BankPowerCbt/paper/<?php echo $testId."CBT"; ?>.pdf">
          <label style="margin-top:10px;color:black;cursor: pointer;">Questions:<img src="img/pdf1.png" style="float:right" width="45px" height="35px"></label></a></div>
   
 </div>
<div class="dwnldCntn">
    <div><a href="downloads/DAMS_CBT_2015_PAPER_1.pdf" target="_blank"><img src="images/pdf.png" /> DAMS CBT 2015 PAPER 1 SOLUTION</a></div>
    <div><a href="downloads/DAMS_CBT_2015_PAPER_2.pdf" target="_blank"><img src="images/pdf.png" /> DAMS CBT 2015 PAPER 2 SOLUTION</a></div>
                 
</div>
<div class="dwnldCntn">
    <div class="dwnBr"><a href="downloads/DAMS_CBT_2015_Rank_List.pdf" target="_blank"><img src="images/pdf.png" /> DAMS CBT 2015 RANK LIST </a></div>      
</div>     
     
 <div class="stdnt_dtls">
     <div class="imgcntr" style="display: none;">
   <?php
      if(file_exists("PHOTO/".$studentId.".jpg")){ ?>
    <img src="PHOTO/<?php echo $studentId.'.jpg'?>">
      <?php }else { ?>
        <img src="PHOTO/photo.jpg">
      <?php }?>
  </div>
  <div class="student_dtls">
   <h3 style="display: none">Your Rank is declare among <?php echo $totalStudent;?> Candidates.</h3>
   <ul style="font-size:14px;">
    <li><span><b>Right Question: </b></span><span  style="color: green; width:107px;"><?php echo $rightQue; ?></span></li>
    <li><span><b>Wrong Question: </b></span><span style="color: red; width:107px;"><?php echo $wrongQue; ?></span></li>
    <li><span><b>Unattempted Question:</b></span><span style="color: blue; width:107px;"><?php echo $leftQue; ?></span></li>
    <li><span><b>Total Marks: </b></span><span style="color: green; width:107px;"><?php echo $totalMrks.'&nbsp;/&nbsp;<b style="color: #505050;">1500</b>'; ?></span></li>
   </ul>
    <div style="border-top: 2px solid #EEEEEE;margin-top:5px;display: none;">
   <h3 style="margin-top:8px;">Subject Wise Marks</h3>     
    <ul style="margin-top:0px;">
    <li><span>Reasoning :</span><strong><?php echo $reasoningMarks; ?></strong></li>
    <li><span>Quant :</span><strong><?php echo $quantMarks; ?></strong></li>
    <li><span>English :</span><strong><?php echo $english; ?></strong></li>
    </ul>
    </div>
   <br><br>
  </div>
     
 <div class="student_dtls_video">
     <iframe src="https://player.vimeo.com/video/142210954" width="240" height="150" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
  </div>     
 </div>
 <div class="blubar ftrtxt"></div></div>
</div>
     </div>
    
    </div>
  </div>
 <!--**************************END HERE************************************-->
 <?php  } ?>                                  
            </div>

        </div>
    </div>
    <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
    <script>
             $(document).ready(function () {
                 $("#region").val("<?php echo $getSubmitData->REGION1; ?>");
                 //getCentres(); //call the function to get detail of all centres
                 RESPONSIVEUI.responsiveTabs();
                 $("#gender").val("<?php echo $getSubmitData->GENDER; ?>");
                 $("#state").val("<?php echo $getSubmitData->STATE_ID; ?>");
                 $("#stateName").val($("#state option[value='<?php echo $getSubmitData->STATE_ID; ?>']").text());
                 $("#nationalityName").val($("#nationality option[value='<?php echo $getSubmitData->COUNTRY_ID; ?>']").text());
                 $("#CstateName").val($("#Cstate option[value='<?php echo $getSubmitData->cSTATE_ID; ?>']").text());
                 $("#CnationalityName").val($("#Cnationality option[value='<?php echo $getSubmitData->cCOUNTRY_ID; ?>']").text());
                 
       
        $("input[class = selectSession]").click(function(){
            $("input[id = selectCentre]").removeAttr("checked");
            $(this).parent().parent().parent().find("input[id = selectCentre]").attr("checked",true);
        });
       
        $("input[id = selectCentre]").click(function() {
            $("input[class = selectSession]").removeAttr("checked");
        })
       
       
                 //While click on save my centre button
                 $("#changeCentre").click(function() {

                     var checkedCentre = $("input[name=selectCentre]:checked").val();
                     var checkedSession = $("input[id = selectSession" + checkedCentre + "]:checked").val();
                    if(checkedCentre == undefined) {
                         alert("Please select your centre");
                         return false;
                      }
                         
                     if (checkedCentre == parseInt($("#mySelectCentre").val()) && checkedSession == parseInt($("#mySelectSession").val())) {
                         alert("You have already selected this centre and session.");
                         return false;
                     }

                     ids = "input[id = selectSession" + checkedCentre + "]:checked";
                     var region = $("#region").val();
                     
                     var chkedDis = $("input[id = selectSession" + checkedCentre + "]").is(":disabled");
                     var chkedDisLength = $("input[id = selectSession" + checkedCentre + "]").length;
                     var radioCount = 0;checkCount = 0;
                     $("input[id = selectSession" + checkedCentre+"]").each(function(){
                         var radVal = $(this).val();
                         if($(this).is(":disabled") == true){
                           checkCount++;  
                         }
                         radioCount++;
                    });
                    
                     if(radioCount == checkCount) {
                         alert("There is no seat left in this centre.");
                         return false;
                     }
                     
                     if ($(ids).is(":checked") == false) {
                         alert("Please select your session in your selected centre.");
                         $("input:radio[name=selectSession]").removeAttr("checked");
                         return false;
                     }
                     if (region != 0) {
                         var urlData = "mode=getCentresCountSessionWise&id=" + checkedCentre + "&selectedSession=" + checkedSession;
                         $.ajax({
                             type: "post",
                             url: "index.php?p=submit",
                             data: urlData,
                             async: false,
                             error: function (result) {
                                 alert("It seems you are not connected to the internet");
                                 return false;
                             },
                             success: function (result) {
                                 var sessionCount = JSON.parse(result);
                                 //sessionCount[0][1] - seats left;
                                 //sessionCount[0][0] - seats available;
                                 if (parseInt(sessionCount[0][0]) > parseInt(sessionCount[0][1])) {
                                     insertStudentInCentre(checkedCentre, checkedSession, region);
                                 } else {
                                     alert("There is no seat left in this session");
                                     return false;
                                 }
                             }

                         });
                     } else {
                         alert("No centre is alloted yet!.");
                         return false;
                     }
                 });
             
             });
   
             function insertStudentInCentre(checkedCentre, checkedSession, regionId) {
                 var urlDataPass = "mode=insertStudent&id=" + checkedCentre + "&sessionId=" + checkedSession + "&eventId=" +<?php echo $eventId; ?> + "&studentId=" +<?php echo $studentId; ?> + "&application_Id=" +<?php echo $appId; ?> + "&regionId=" + regionId;
                 ///alert(urlDataPass);
                 $.ajax({
                     type: "post",
                     url: "index.php?p=submit",
                     data: urlDataPass,
                     async: false,
                     error: function (result) {
                         alert("It seems you are not connected to the internet");
                         return false;
                     },
                     success: function (result) {
                         if (result == 0) {
                             alert("Your centre has been locked.");
                             window.location.reload();
                         } else if (result == 1) {
                             alert("There is no seat left in this session");
                             window.location.reload();
                         }
                     }
                 });
             }
             function getCentres(get) {
                 $("#centres").html("");
                 var region = $("#region").val();
                 urlData = "mode=getCentres&id=" + region;
                 var html = "";
                 var session = "";
                 $.ajax({
                     type: "post",
                     url: "index.php?p=submit",
                     data: urlData,
                     async: false,
                     error: function (result) {
                         alert("It seems you are not connected to the internet.");
                         return false;
                     },
                     success: function (result) {
                         var data = JSON.parse(result);
                         if (data.length == 0) {
                             html = html + '<div class="center" id="centresDetail" style="text-align:justify;"><strong>Dear Candidate,<br /><br />We appreciate your patience and we would like to apologize for the delay caused to you for booking your center. We are in process of providing you the best center for <b style="color:red;">DAMS CBT  4th October 2015 exam </b>and it will be available in next 2-3 Days.<br /><br /><b style="color:red;">Note: </b>Even if the OBS will be closed we ensure that you will be allotted the best center for your DAMS CBT 2015 exam.<br /><br />Thank you once again for your co-operation.</strong></div>';
                             $("#centreAvailable").val("1");
                         } else {
                             var sessionCount = Array();
                             for (i = 0; i < data.length; i++) {
                                 var sessionIds = data[i][6].split(",");
                                 var sessionName = data[i][7].split(",");
                                 var sessionTime = data[i][8].split(",");
                                 if(data.length == 1 && sessionIds.length == 1)
                                     sessionCount[0] = data[i][9];
                                 else
                                     sessionCount = data[i][9].split(",");
                                 
                                 
                                 for (j = 0; j < sessionIds.length; j++) {
                                     
                                  if(sessionCount[j] == 0) {
                                     session = session + '<div class="sesion_wpr_input">\n\
                                               <input type="radio" disabled class="selectSession" value="' + sessionIds[j] + '" name="selectSession" id="selectSession' + data[i][0] + '" style="opacity:1">\n\
                                               &nbsp;<span>' + sessionName[j] + '<br>&nbsp;&nbsp;&nbsp;(' + sessionTime[j] + ')' + '<br>&nbsp;&nbsp;&nbsp;Availability -></span><span style="color:green"> ' + sessionCount[j] + ''+'</span>\n\
                                               </div>';
                                    } else {
                                     session = session + '<div class="sesion_wpr_input">\n\
                                               <input type="radio" class="selectSession" value="' + sessionIds[j] + '" name="selectSession" id="selectSession' + data[i][0] + '" style="opacity:1">\n\
                                               &nbsp;<span>' + sessionName[j] + '<br>&nbsp;&nbsp;&nbsp;(' + sessionTime[j] + ')' + '<br>&nbsp;&nbsp;&nbsp;Availability -></span><span style="color:green"> ' + sessionCount[j] + ''+'</span>\n\
                                               </div>';
                                    }
                                 }

                                 html = html + '<div class="center" id="centresDetail">\n\
                                 <input class="address_input" type="radio" id="selectCentre" name="selectCentre" value="' + data[i][0] + '" style="opacity:1">\n\
                                       <strong>' + data[i][1] + '</strong>\n\
                                 <div class="centr_adrs" id="addressDiv' + data[i][0] + '">\n\
                                <b>Address:&nbsp;&nbsp;</b>' + data[i][2] + '</div><b>\n\
                                Landmark:&nbsp;&nbsp;</b>' + data[i][3] + '<br><br>\n\
                                <b>Availability</b> -> <span style="color:green" id="consume">' + (parseInt(data[i][4]) - parseInt(data[i][5])) + ' Seats</span>\n\
                                <div class="sesion_wpr">' + session + '</div></div>'
                                 session = "";
                             }
                         }
                         $("#centres").html(html);
                         
                         //if their is only one centre and one session into the region
                         if (data.length == 1 && sessionIds.length == 1) {
                             $("input[name=selectCentre][value=" + data[0][0] + "]").attr('checked', 'checked');
                             var centreId = $("input[name=selectCentre]:checked").val();
                             ids = "#selectSession" + centreId;
                             checkedSession = $(ids).val();
                             oneCentreLocked(centreId, checkedSession);
                             get = 1;
                         } else if (data.length == 1) { //if has more then one session but only one centre 
                             $("input[name=selectCentre][value=" + data[0][0] + "]").attr('checked', 'checked');
                             getSaveData();
                         }else {
                             getSaveData();
                         }
                     }
                 });
             }
             function oneCentreLocked(centreId, checkedSession) {
                 var urlDataPass = "mode=insertStudent&id=" + centreId + "&sessionId=" + checkedSession + "&eventId=" +<?php echo $eventId; ?> + "&studentId=" +<?php echo $studentId; ?> + "&application_Id=" +<?php echo $appId; ?>;
                 //alert(urlDataPass);
                 $.ajax({
                     type: "post",
                     url: "index.php?p=submit",
                     data: urlDataPass,
                     async: false,
                     error: function (result) {
                         alert("It seems you are not conneted to the internet");
                         return false;
                     },
                     success: function (result) {
                         if (result == 1) {
                             alert("There is no seat left in this centre");
                             $("input[id = selectSession]").removeAttr('checked');
                             $(".selectSession").removeAttr("checked");
                             return false;
                         } else {
                             getOneCentreData(centreId,checkedSession);
                         }
                     }
                 });
             }
             
    function getOneCentreData(centreId,checkedSession) {
        var value = centreId;
        var sessioValue =checkedSession;       
                 $("input[name=selectCentre][value=" + value + "]").attr('checked', 'checked');
                 $("input[id = selectSession" + value + "][value=" + sessioValue + "]").attr('checked', 'checked');
                if($("#centreAvailable").val() == 0)             
                    $("#changeCentre").show();
<?php if (($getSubmitData->REGION1 != NULL || $getSubmitData->REGION1 != '') &&
          ($getSubmitData->CENTRE_ID != NULL || $getSubmitData->CENTRE_ID != '') &&
           ($getSubmitData->SESSION_ID != NULL || $getSubmitData->SESSION_ID != '')) { ?>
                     $("#changeCentre").html("Save My Centre");
                     $("#centreLockedMsg").show();
<?php } ?>
                 if (value == 0) {
                     $("input[id = selectSession]").removeAttr('checked');
                     $(".selectSession").removeAttr("checked");
                 }
                 showHideCentre(value,sessioValue);
    }
             
             
             function getSaveData(chkCentre,sessionId) {
                 var value = <?php if ($getSubmitData->CENTRE_ID == NULL || $getSubmitData->CENTRE_ID == "") {
    echo '0';
} else {
    echo $getSubmitData->CENTRE_ID;
} ?>;
                 var sessioValue = <?php if ($getSubmitData->SESSION_ID == NULL || $getSubmitData->SESSION_ID == "") {
    echo '0';
} else {
    echo $getSubmitData->SESSION_ID;
} ?>;
            
                
                 $("input[name=selectCentre][value=" + value + "]").attr('checked', 'checked');
                 $("input[id = selectSession" + value + "][value=" + sessioValue + "]").attr('checked', 'checked');
                if($("#centreAvailable").val() == 0)             
                    $("#changeCentre").show();
<?php if (($getSubmitData->REGION1 != NULL || $getSubmitData->REGION1 != '') &&
          ($getSubmitData->CENTRE_ID != NULL || $getSubmitData->CENTRE_ID != '') &&
           ($getSubmitData->SESSION_ID != NULL || $getSubmitData->SESSION_ID != '')) { ?>
                     $("#changeCentre").html("Save My Centre");
                     $("#centreLockedMsg").show();
<?php } ?>
                 if (value == 0) {
                     $("input[id = selectSession]").removeAttr('checked');
                     $(".selectSession").removeAttr("checked");
                 }             
                 showHideCentre(value,sessioValue);
                 //createMap();
             }
             
 var showHideCentre = function(value,sessioValue){
      $("div[id^='centresDetail']").each(function(){
                     var allCentreId = $(this).find("input[id = selectCentre]").val();
                     if(allCentreId != value && value!=0)
                         $(this).hide();
                     if(allCentreId == value && value!=0) {
                         $(this).find("input[name=selectSession]").parent("div").hide();
                         $(".yr_center_hding").text("Your selected centre");                 
                         $(this).find("input[id = selectCentre]").attr("disabled","true");
                         $(this).find("input[name=selectSession]").attr("disabled","true");
                         $(".centr_change").hide();
                         $("input[id = selectSession" + value + "][value=" + sessioValue + "]").parent("div").show();
                     }
                 });
 }            
             
    </script>
	<script type="text/javascript" src="js/jquery.excoloSlider.js"></script>
<script type="text/javascript" src="js/jquery.excoloSlider.min.js"></script>
<script type="text/javascript">
	 $(function () {
            $("#sliderA").excoloSlider();
            $("#sliderB").excoloSlider();
        });

</script>

</body>
</html>