<!doctype html>
<?php
session_start();
$ob = new funDams();
if(trim($_POST['studentId'])!="" && isset($_POST['studentId'])) {
    $studentId = trim($_POST['studentId']);
}else if(trim($studentId) =="" && isset ($_SESSION['studIdReg']) && trim($_SESSION['studIdReg'])!=''){
    $studentId = trim($_SESSION['studIdReg']);
}else if(trim($studentId) =="" && isset($_SESSION['STUDENT_ID']) && trim($_SESSION['STUDENT_ID']!='')){
    $studentId = trim($_SESSION['STUDENT_ID']);
}

//if($_REQUEST['c']=="")
//$studentId=$_POST['studentId'];
//else 
//$studentId = $_SESSION['studIdReg'];
if($studentId=='0' || $studentId== ''){
    header("Location:index.php?p=Login");
}

$getSubmitData = $ob->getStudentAllData($studentId);

$tab1=$getSubmitData->TAB1;
$tab2=$getSubmitData->TAB2;
$_SESSION['studIdReg'] = $studentId;
$_SESSION['studEmlReg'] = $getSubmitData->EMAIL;
$_SESSION['studNmeReg'] = $getSubmitData->STUDENT_NAME;
//$getWorkDetail = $ob->getStudentWorkDetail($studentId);
$extensionArray = array('.jpg','.jpeg','.png');
$prfilePic="images/student-icon.png";
$picExist=0;
for($i=0;$i<count($extensionArray);$i++){
 if(file_exists("PHOTO/".$studentId."$extensionArray[$i]")){
         $prfilePic = "PHOTO/".$studentId."$extensionArray[$i]"."?".rand(0,10);
         $picExist = 1;
 }
 
}
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<title>DAMS USMLE Self Assessment Exam</title>
<link rel="shortcut icon" href=images/favicon.ico type=image/x-icon />
<link rel=icon href=images/favicon.ico type=image/x-icon />
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/font/stylesheet.css" rel="stylesheet" type="text/css">
<link href="css/screens.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery1.7.1.js"></script>
<script type="text/javascript" src="js/register.js"></script>

<!--[if IE 7]><link rel="stylesheet" href="css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="css/ie9.css" type="text/css" /><![endif]-->
<!--[if IE 10]><link rel="stylesheet" href="css/ie10.css" type="text/css" /><![endif]-->
</head>
<body>
<div class="pre_loader" id="preloader" style="display:none">
<div class="pre_loader_bg"></div>
<div class="ldr_line"></div>
<span class="icon"></span>
</div> 
<!--<div class="top_hdr">
 <div align="center" class="main_wpr no_border">
  <div class="hdr_ctnt">
   <h3>Online Application</h3>
   <h1>Registrati<span class="reg_o"><img src="images/reg_o.png"></span>n</h1>
  </div>
  <div class="pg_title">
   <span>Application Form</span>
  </div>
 </div>
</div>-->
<div class="header">
 <div class="main_wpr no_border">
  <a href="#" class="logo">
   <img src="images/logo.png">
  </a>
  <a href="index.php?p=" class="login_rgt">Logout</a>
 </div>
</div>
<div class="main_wpr no_border" style="margin-bottom:0;">
 <h2 class="inr_pg_hdr">Application Form</h2>
 <div class="hding_pgination">Home&nbsp;/&nbsp;Application Form</div>
</div>
<div class="main_wpr">
<div class="inst_wpr">
  <div class="ins_online_sub"><img src="images/instruction.png" class="img">Instructions For Submitting Online Application</div>
  <p class="inst_p">To fill the Registration Form, Kindly use Internet Explorer (Minimum Version 8) Mozilla (Minimum Version 14)  or Google Chrome (Minimum Version 20)</p>
  <b>Read the below instruction carefully, before filling the form: </b>
  <ul class="inst_points">
   <li>Candidate has to fill in the below mentioned details to receive the User Id and Password.</li>
   <li>Candidate will receive the User Id and Password in the registered email address and through SMS on the registered mobile number.</li>
   <li>Candidate can login with the User Id and Password to complete the application for CBT 2016.</li>
   <li>Candidate must provide correct Name, Date of Birth, Mobile Number and Email Address as these details cannot be changed after the registration complete.</li>
  </ul>
 </div>
 <div class="reg_form_wpr">
 <ul class="round_steps" id="tabs">
   <li class="first">
    <a href="#a" class="first" title="Detail">
     <div class="stp_line"></div>
     <span class="stp_circle">1</span>
     <div class="stp_name">Personal Details</div>
    </a>
   </li>
   <li class="menuTab1">
    <a href="#a" title="Centre">
     <div class="stp_line"></div>
     <span class="stp_circle">2</span>
     <div class="stp_name">Test Centre</div>
    </a>
   </li>
<!--   <li class="menuTab2">
    <a href="#a" title="tab3">
     <div class="stp_line"></div>
     <span class="stp_circle">3</span>
     <div class="stp_name">Work Experience</div>
    </a>
   </li>
   <li class="menuTab3">
    <a href="#a" title="tab4">
     <div class="stp_line"></div>
     <span class="stp_circle">4</span>
     <div class="stp_name">Programmes</div>
    </a>
   </li>-->
<li class="last">
    <a href="#a" class="last" title="Payment">
     <div class="stp_line"></div>
     <span class="stp_circle">3</span>
     <div class="stp_name">Payment</div>
    </a>
   </li>
 </ul>
 <div class="clear_both"></div>
<div id="content"> 
 <div class="appform_f_cntr sw_hd" id="Detail">
     <form action="index.php?p=submit" method="post" name="personalDetail" id="personalDetail" enctype="multipart/form-data" >
  <fieldset>
   <legend>Personal Details</legend>
   <div class="flds_cntr">
    <label>Candidate Name<b>*</b></label>
    <input type="text" readonly value="<?php echo html_entity_decode($getSubmitData->STUDENT_NAME); ?>">
   </div>
   <div class="flds_cntr">
    <label>Mobile Number<b>*</b></label>
    <input type="text" readonly value="<?php echo $getSubmitData->MOBILE; ?>">
   </div>
  <!--     <div class="flds_cntr appform_full">
<div class="appform_note"><b>Note:</b>&nbsp;&nbsp; Candidate name should be entered as per 10th/12th/Diploma/Degree Certificate</div>
   </div>-->
<!--   <div class="flds_cntr">
    <label>Father’s/Husband’s/Gaurdian’s Name<b>*</b></label>
    <input type="text" id="fatherName" name="fatherName" value="<?php echo $getSubmitData->GAURDIAN_NAME; ?>">
   </div>-->
<!--   <div class="flds_cntr">
    <label>Mother’s Name<b>*</b></label>
    <input type="text" id="motherName" name="motherName" value="<?php echo $getSubmitData->MOTHER_NAME; ?>">
   </div>-->
   <div class="flds_cntr">
    <label>Date of Birth</label>
    <input type="text" id="dob" name="dob" readonly value="<?php echo date("d/m/Y", strtotime($getSubmitData->DOB)); ?>">
   </div>
<!--   <div class="flds_cntr">
    <label>Gender<b>*</b></label>
    <select id="gender" name="gender">
     <option value="" selected>--Select--</option>
     <option value="male">Male</option>
     <option value="female">Female</option>
    </select>
   </div>-->
<!--   <div class="flds_cntr">
    <label>Category<b>*</b></label>
    <select id="category" name="category">
     <option value="0">--select--</option>
     <option value="1">--OBC--</option>
    </select>
   </div>-->
<!--   <div class="flds_cntr">
    <label>State/Union Territory of caste/Tribe<b>*</b></label>
    <select id="state" name="state">
     <option value="0">Please select your State/Union Territory</option>
        <?php $state = $ob->getState();
        while($row = mysql_fetch_array($state)){
         echo "<option value='$row[STATE_ID]'>$row[STATE_NAME]</option>";   
        }?>
    </select>
   </div>-->
<!--   <div class="flds_cntr">
    <label>Serial Number of Caste/Tribe<b>*</b></label>
    <input type="text" name="serialNo" id="serialNo">
   </div>-->
<!--   <div class="flds_cntr">
    <label>Caste/Tribe Name<b>*</b></label>
    <input type="text" id="Caste" name="Caste">
   </div>-->
<!--   <div class="flds_cntr">
    <label>Country<b>*</b></label>
    <select id="nationality" name="nationality">
    <option value="0">Please select your country</option>
        <?php $country = $ob->getCountry();
        while($row = mysql_fetch_array($country)){
         echo "<option value='$row[COUNTRY_ID]'>$row[COUNTRY_NAME]</option>";   
        }?>
    </select>
   </div>-->
<!--   <div class="flds_cntr appform_full">
    <label>Person with Disability/Differently Abled (PWD/DA)<b>*</b></label>
    <div class="chxrdo_cntr hglt_chx">
    <input type="radio" id="disableY" name="disablity" class="radio"><label for="dyes" class="chxrdo_span">Yes</label>
    <input type="radio" id="disableN" name="disablity" class="radio"><label for="dno" class="chxrdo_span">No</label>
    <input type="hidden" id="disableYN" name="disableYN" value="">
    </div>
   </div>-->
<!--   <div class="flds_cntr" id="categoryDiv">
    <label>PWD/DA Category<b>*</b></label>
    <select id="pwCategory" name="pwCategory">
     <option value="0">--Select--</option>
     <option value="1">--PWD category--</option>
    </select>
   </div>
   <div class="flds_cntr" id="percentageDiv">
    <label>Disability Percentage<b>*</b></label>
    <select id="disablePer" name="disablePer">
     <option value="0">--Select--</option>
     <option value="90">--90--</option>
     
    </select>
   </div>-->
<!--   <div class="flds_cntr appform_full" id="wheelchairDiv">
    <label>Do you require wheelchair support at the test center?<b>*</b></label>
    <div class="chxrdo_cntr hglt_chx">
    <input type="radio" id="chairY" name="chair" class="radio"><label for="dyes" class="chxrdo_span">Yes</label>
    <input type="radio" id="chairN" name="chair" class="radio"><label for="dno" class="chxrdo_span">No</label>
    <input type="hidden" id="chairYN" name="chairYN" value="">
    </div>
   </div>-->
  </fieldset>
  
   
 <div class="clear_both"></div>
  <fieldset>
   <legend>Communication Address</legend>   
    <div class="flds_cntr appform_full">
     <div class="appform_note"><b>Note:</b>&nbsp;&nbsp;Enter a valid address as it will be used for further communication</div>
    </div>
    <div class="flds_cntr">
     <label>Address 1<b>*</b></label>
     <input type="text" id="address1" name="address1">
    </div>
    <div class="flds_cntr">
     <label>Address 2<b></b></label>
     <input type="text" id="address2" name="address2">
    </div>
   <div class="flds_cntr">
     <label>Town/City<b>*</b></label>
     <input type="text" id="city" name="city">
    </div>
    <div class="flds_cntr">
     <label>Pin Code<b>*</b></label>
     <input type="text" id="pincode" name="pincode" maxlength="6" oninput="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1'); this.value = this.value.replace(/^(-?).*?(\d+\.\d{2}).*$/, '$1$2');">
    </div>
   
   
   
<!--    <div class="flds_cntr">
     <label>Address 3<b></b></label>
     <input type="text" id="address3" name="address3">
    </div>-->
    <div class="flds_cntr">
     <label>State Union Territory<b>*</b></label>
    <select id="cState" name="cState">
     <option value="" selected="selected">Please select your State or Union Territory</option>
        <?php $state = $ob->getState();
        while($row = mysql_fetch_array($state)){
         echo "<option value='$row[STATE_ID]'>$row[STATE_NAME]</option>";   
        }?>
    </select>
    </div>
<div class="flds_cntr">
     <label>Country<b>*</b></label>
     <select id="cCountry" name="cCountry">
      <option value="">Select Country</option>
       <?php $country = $ob->getCountry();
        while($row = mysql_fetch_array($country)){
         echo "<option value='$row[COUNTRY_ID]'>$row[COUNTRY_NAME]</option>";   
        }?>
     </select>
    </div>
    
<!--    <div class="flds_cntr">
     <label>Email Address<b></b></label>
     <input type="text">
    </div>-->
<!--    <div class="flds_cntr">
     <label>Guardian Email Address <b></b></label>
     <input type="text" id="cEmail" name="cEmail">
    </div>
    <div class="flds_cntr">
     <label>Mobile Number <b></b></label>
     <input type="text" id="cMobile" name="cMobile">
    </div>-->
   </fieldset>
    
  <div class="clear_both"></div>
  <fieldset>
   <legend>Upload Documents</legend>
<!--   <div class="flds_cntr appform_full">
    <div class="appform_note"><b>Note:</b>&nbsp;&nbsp;
    <ul class="inst_points">
       <li>The NC-OBC/SC/ST/DA/PWD certificate should be in the prescribed format issued by the competent authority.</li>
       <li>The self-attested scanned copy of the certificate should be clearly visible.</li>
       <li>Your name on the certificate should match with the name you have mentioned in the Personal Details section.<br>
       (If there is a name mismatch, please refer to the FAQs/Registration Guide provided on the OAR website)</li>
     </ul>
    </div>
   </div>-->
<form>
   <div class="flds_cntr appform_full file_upload">
    <p>
     <span class="btn btn-success fileinput-button">
<!--      <span><strong class="red_astrk">*</strong> Click here</span>
      <input id="fileuploadPic" type="file" name=""  onchange="showimagepreview(this)">-->
      
      <input type="hidden" name="checkForNext" id="checkForNext" value="0" >
      <?php if(file_exists("PHOTO/".$studentId.".jpg")) { ?>
           <input type="hidden" id="fileupload" name="fileupload" value="1" />
     <?php } else { ?>
            <input type="hidden" name="fileupload" id="fileupload" value="0"/>
       <?php } ?>
      
     </span> 
        Upload scanned copy of passport size photograph. Image with jpg , jpeg or png extensions are valid to upload. 
     <br >Please upload your recent passport size photograph between 5KB to 200KB.
    </p>
<!--    <p id="obcUpload">
    <span class="btn btn-success fileinput-button">
      <span><strong class="red_astrk">*</strong> Click here</span>
      <input id="fileupload" type="file" name="files1" multiple>
     </span> to upload scanned copy of Nc-OBC/SC/ST certificate.</p>-->
<!--    <p id="pwdUpload">
    <span class="btn btn-success fileinput-button">
      <span><strong class="red_astrk">*</strong> Click here</span>
      <input id="fileupload" type="file" name="files2" multiple>
     </span> to upload scanned copy of PWD/DA certificate.</p>-->
   </div>
<!--    <div id="imagePre" style="display:none;"> <img src=" " id="preview"  style="width:90px;height:120px;"/></div>-->
   <div class="flds_cntr appform_full upld_photo fileinput-button">
       <div class="upld_phto">
         <input id="fileuploadPic1" type="file" name=""  onchange="showimagepreview1(this)" style='width:10%; height:80%; left:0 '>
           <img  id="preview" src="<?php echo $prfilePic; ?>">       
           <?php if($picExist==0 || $picExist=='0'){ ?>
           <input type="hidden" id="imageExist" name="picExist" value="0" />
           <input type="hidden" id="picChange" name="picChange" value="0" />
           <?php } else if($picExist==1 || $picExist=='1') { ?>
           <input type="hidden" id="imageExist" name="imageExist" value="1" />
           <input type="hidden" id="picChange" name="picChange" value="0" />
           <?php } ?>
       </div>
<!--       <div class="upld_phto" id="obcPic"><img src="OBC/<?php //echo $studentId.".jpg"; ?>" ></div>
    <div class="upld_phto" id="pwdPic"><img src="PWD/<?php //echo $studentId.".jpg"; ?>" ></div>-->
   </div>
  </fieldset>
  <input type="hidden" id="mode" value="personalDetailAdd" name="mode" >
        <input type="hidden" id="studentId" name="studentId" value="<?php echo $studentId; ?>" >
    </form>
  <div class="register_cntr">
   <a id="validatePersonalDetail" class="reg_btn">Save & Continue</a>
   <!--<input type="submit" value="Register Now" class="reg_btn">-->
  </div>
  </div>
        

 <div class="appform_f_cntr sw_hd" id="Centre">
     <form action="index.php?p=application_process&c=last" method="post" id="academic" name="academic" onsubmit="return false">
 <div class="clear_both"></div>
  <fieldset style="position:relative; overflow:hidden;">
   <legend>DAMS Detail.</legend>   
   
    <div class="flds_cntr appform_full">
<p>If You are not DAMS 2016 Batch Student then you have to pay Rs.800 as an outsider.</p>
<p></p>
     <label>Are You a DAMOSIANS<b>*</b></label>
     <div class="chxrdo_cntr">
      <input type="radio" class="radio" id="dyes" name="damosian" value="1"><label for="dyes" class="chxrdo_span">Yes</label>
      <input type="radio" class="radio" id="dno" name="damosian" value="0" checked="true"><label for="dno" class="chxrdo_span">No</label>
      <input type="hidden" name="damosian" id="damosian" value="" >
    <p style="float:left;font-size: 14px" valign="middle" id="cost" name="cost"></p>
     </div>
    
    </div>
    <div class="flds_cntr" id="enrollDiv" class="enrollDiv">
     <label>Please Enter Your Roll Number.<b>*</b></label>
     <input type="text" id="enroll" name="enroll"> 
     <input type="hidden" id="damosianYN" name="damosianYN" value=""> 
    </div>
    <!--     	<div class="flds_cntr appform_full">
<div class="appform_note"><b>Note:</b>&nbsp;&nbsp;For computing percentages of marks obtained in 12th/Equivalent/Diploma examination, the aggregate
     marks of all subjects that appear in the mark sheet/grade sheet would be considered irrespective of the Boards regulation.</div>
   
        </div>-->
   </fieldset>
   
  <div class="clear_both"></div>
  <fieldset>
 <?php $centre = $ob->getExamCentre();$i=0; 
 while($row = mysql_fetch_array($centre)){
     $centreId[$i] = $row['REGION_ID'];
     $centreName[$i] = $row['NAME'];
     $i++;
 }
 ?>
   <legend>Exam Centre City Preferences</legend>
  
<!--   <div class="flds_cntr appform_full">
     <div class="appform_note"><b>Note:</b>&nbsp;&nbsp; You can select any three preferred cities for the CBT examination, However, city allocation will be subject to availability.</div>
   </div> -->
   <div class="flds_cntr">
    <label>Preferred City 1:<b>*</b></label>
    <select id="centre1" name="centre1">
        <option value="0" selected>--Select--</option>f
        <?php for($i=0;$i<sizeOf($centreId);$i++){ ?>
             <option value="<?php echo $centreId[$i]; ?>"><?php echo $centreName[$i];?></option>
        <?php  } ?>
    </select>
   </div>
<!--   <div class="flds_cntr">
    <label>Preferred City 2:<b>*</b></label>
    <select id="centre2" name="centre2">
             <option value="0">--Select--</option>
        <?php for($i=0;$i<sizeOf($centreId);$i++){ ?>
             <option value="<?php echo $centreId[$i]; ?>"><?php echo $centreName[$i];?></option>
        <?php  } ?>
    </select>
   </div>-->
<!--   <div class="flds_cntr">
    <label>Preferred City 3:<b>*</b></label>
    <select id="centre3" name="centre3">
         <option value="0">--Select--</option>
        <?php for($i=0;$i<sizeOf($centreId);$i++){ ?>
             <option value="<?php echo $centreId[$i]; ?>"><?php echo $centreName[$i];?></option>
        <?php  } ?>
    </select>
   </div>-->
  </fieldset>
   
  <div class="clear_both"></div>
  <div class="register_cntr">
      <input type="submit" id="checkAcademic"  class="reg_btn" value="Save & Continue">
   <!--<input type="submit" value="Register Now" class="reg_btn">-->
  </div>
     </form>
  </div>
  <div class="appform_f_cntr sw_hd" id="Payment">
      <form action="index.php?p=payment" method="post" name="redirectPayment" id="redirectPayment">
          
 <fieldset>
   <legend>Payment Procedure</legend>
   <div class="flds_cntr declaration">
      <p>You have to pay - <b id="examCost" name="examCost"></b>
   <br>
   Click On <b>Proceed To Payment</b> ,You will be redirect on <b>Payment Gateway</b>.<br>
   Please wait and do not press <b>BACK</b> or <b>REFRESH</b> button of your browser when you are on payment gateway.</p>
       <div class="pymnt_option" style="">
     <input type="checkbox" name="iagree" id="couponSelect" class="checkbox" name="paymentMode">
     <label for="iagree" class="pymnt_type" >Coupon</label>
    </div>
    <div class="pymnt_option" >
     <input type="checkbox" name="iagree" id="onlineSelect" name="paymentMode" class="checkbox">
     <label for="iagree" class="pymnt_type" >Online</label>
    </div>
   </div>
   <div class="flds_cntr" style="margin-left: 9px;display: none" id="couponBox">
    <label>Coupon Code<b>*</b></label>
    <input type="text" placeholder="Enetre your coupon code" id="couponCode">
   </div>
  </fieldset>
  
 <div class="clear_both"></div>
    
 <div class="register_cntr">
      <a id="submitPayment" class="reg_btn">Proceed To Payment</a>
<!--   <input type="submit" value="Register Now" class="reg_btn">-->
  </div>
 <input type="hidden" id="packageId" name="packageId" value=""> 
 <input type="hidden" id="packageCost" name="packageCost" value="">
      </form>
  </div>
 </div>
 
 
 
 
 
 
  </div>
 </div>
</div>
<script src="cal/jquery.js"></script>
<link rel="stylesheet" href="calci/css/pickmeup.css" type="text/css" />
<script type="text/javascript" src="calci/js/jquery.pickmeup.js"></script>
<script>
$(document).ready(function() {
    	$('#dob').pickmeup({
                format          :'d-m-Y',
		position        : 'left',
		hide_on_select	: true ,
                max             : new Date(),
                trigger_event   : 'click'
	});
//$("#preview").attr("src","");
    if($("#picExist").val()==1){
//$("#preview").attr("src","PHOTO/<?php echo $studentId.".jpg"; ?>");
$('#preview').prop('src',"<?php echo $prfilePic; ?>" +'?'+ Math.random());
}
    	$("#content .sw_hd").hide(); // Initially hide all content
	$("#tabs li:first").attr("id","current"); // Activate first tab
	$("#content .sw_hd:first").fadeIn(); // Show first tab content
        $('#tabs a').click(function(e) {
    if($(this).attr('title')!='Payment'){
        e.preventDefault();
        var targetParam = "<?php  echo $_REQUEST['c'];  ?>";
        if(targetParam!=''){
        $("#content .sw_hd").hide(); //Hide all content
        $("#tabs li").attr("id",""); //Reset id's
        $(this).parent().attr("id","current"); // Activate this
       // alert($('#' + $(this).attr('title')).val());
        $('#' + $(this).attr('title')).fadeIn(); // Show content for current tab
    }
    } });
    <?php if($_REQUEST['c']!='' && $tab1=='1' ) { ?>
        $("#content .sw_hd").hide(); //Hide all content
        $("#tabs li").attr("id",""); //Reset id's
        $(".menuTab<?php echo $_REQUEST['c'];?>").attr("id","current"); // Activate this
        $("#"+$(".menuTab<?php echo $_REQUEST['c'];?> a").attr('title')).fadeIn();
<?php  }  
         
          if($_REQUEST['c']=='last' && $tab1==1) {?>
                  
            $("#content .sw_hd").hide(); 
            $("#tabs li").attr("id",""); //Reset id's
            $(".menuTab1").attr("id","current"); // Activate this
            $("#"+$(".menuTab1 a").attr('title')).fadeIn();
  
        <?php  }
        if($_REQUEST['c']=='last' && $tab1==1 && $tab2==1) { ?>
        $(".menuTab1").attr("id",""); 	
        $("#content .sw_hd").hide(); // Initially hide all content
	$("#tabs li:last").attr("id","current"); // Activate first tab
	$("#content .sw_hd:last").fadeIn(); // Show first tab content
<?php } ?>
$("#dob").val("<?php echo date("d/m/Y", strtotime($getSubmitData->DOB)); ?>");
$("#nationality").val("<?php echo $getSubmitData->COUNTRY_ID; ?>");
$("#gender").val("<?php echo $getSubmitData->GENDER; ?>");
$("#category").val("<?php echo $getSubmitData->CATEGORY; ?>");
$("#state").val("<?php echo $getSubmitData->STATE_ID; ?>");
$("#serialNo").val("<?php echo $getSubmitData->SERIAL_NUMBER; ?>");
$("#Caste").val("<?php echo $getSubmitData->CASTE; ?>");    
$("#pwCategory").val("<?php echo $getSubmitData->PWD_CATEGORY; ?>");
$("#disablePer").val("<?php echo $getSubmitData->DIS_PERCENTAGE;?>");
$("#address1").val("<?php echo html_entity_decode($getSubmitData->ADDRESS,ENT_QUOTES); ?>");
$("#address2").val("<?php echo html_entity_decode($getSubmitData->ADDRESS1,ENT_QUOTES); ?>");
$("#address3").val("<?php echo html_entity_decode($getSubmitData->ADDRESS2,ENT_QUOTES); ?>");
$("#city").val("<?php echo $getSubmitData->cCITY; ?>");
$("#cState").val("<?php echo $getSubmitData->cSTATE_ID; ?>");
$("#cCountry").val("<?php echo $getSubmitData->cCOUNTRY_ID; ?>");
$("#pincode").val("<?php echo $getSubmitData->cPINCODE; ?>");
});
    
</script>
<script type="application/javascript">
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'server/php/';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('#files');
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>
</body>
</html>
<?php 
while($row = mysql_fetch_array($getSubmitData)){ ?>
<script type="text/javascript">
  </script>   
<?php } ?>
<script type="text/javascript">
    $(":checkbox[value=<?php echo $row['PROGRAMME_ID'];?>]").prop("checked","true");
    $("#"+$(":checkbox[value=<?php echo $row['PROGRAMME_ID'];?>]").attr("id").replace("checkProgram","location")).val("<?php echo $row['INTERVIEW_LOCATION'];?>");
   </script>   
<script type="text/javascript">
    $("#centre1").val("<?php echo $getSubmitData->REGION1;?>");
    $("#centre2").val("<?php echo $getSubmitData->REGION2;?>");
    $("#centre3").val("<?php echo $getSubmitData->REGION3;?>");
    $('input:radio[name="damosian"]').filter('[value="<?php echo $getSubmitData->DAMOSIAN;?>"]').attr('checked', true);
    if(<?php echo $getSubmitData->DAMOSIAN;?>==1){
    $("#enroll").val("<?php echo $getSubmitData->ENROLLMENT_NO;?>");    
    }else{
        $("#enroll").val("");    
    }
</script>   