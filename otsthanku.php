<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<title>Damsdelhi</title>
<link rel="shortcut icon" href="document/company/favicon.ico" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<link href="student/css/style1.css?v=1.2" rel="stylesheet" type="text/css">
<link href="student/css/screens.css?v=1.2" rel="stylesheet" type="text/css">
    <style>
        .form-wrapper {
    background-repeat: no-repeat;

    position: fixed;
    width: 100%;
    top: 0;
    bottom: 0;
}
        .form-content-wrapper {
    position: fixed;
    left: 0;
    right: 0;
    top: 10%;
    bottom: 0;
    width: 500px;
    height: auto;
    margin: auto;
}
.form-content {
    background: #fff;
    padding: 15px 15px 22px;
    border-radius: 6px;
}
.form-content h3 {
    color: #666;
    margin: 0px 10px 10px 10px;
    font-size: 20px;
    line-height: 30px;
    text-align: center;
}
.submit-btn-wrapper {
    text-align: center;
    margin: 35px 10px 0;
}
.submit-btn-wrapper span {
    border: none;text-align: center;
    background: #40ade5 none repeat scroll 0 0;
    padding: 7px 20px;
    color: #fff;
    border-radius: 5px;
    font-size: 14px;
    transition: all 0.2s ease;
    -webkit-transition: all 0.2s ease;
    cursor: pointer;
}
@media screen and (max-width: 980px) {
    
    .form-content-wrapper {

    width: 90%;
}
}
    </style>
</head>
<body>
 <div class="clear_both"></div>
 <!--Slider Area start-->
 <section class="top_bg_section form-wrapper">
 <div class="wrapper_main">
  <div class="sld_cntr">
  <div class="form-content-wrapper ">
  <div class="form-content ">

 <h3>"Successfully Submitted
     <br>
We will send you SMS/Email your projected rank on 12 Jan, 2018
 <br>
Disclaimer: This All india Rank is just based on data submitted and DAMS doesnt take any responsibility for the same."
    </h3>
      <div class="submit-btn-wrapper">
                               
          <a href="projectedrank.php"><span> OK</span></a>
                               
                            </div>
</div>
</div>
</div>
</div>
     </section>
</body>
</html>
