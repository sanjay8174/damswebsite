<div class="pg-medical-main tab-hide">
  <div class="pg-heading"><span></span>Face to Face Courses</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <h5 class="h2toggle" onclick="sliddes('1');"><span class="plus-ico" id="s1"></span> Intern/Post Intern Students</h5>
      <div class="coures-list-box-content" id="di1" style="display:none;">
        <ul class="course-new-list">
          <li style="margin:0px;">
            <h6>Classroom Courses</h6>
          </li>
          <li><a href="regular_course_for_pg_medical.php" title="Regular Course (weekend)"><span class="sub-arrow"></span>Regular Course</a></li>
          <li><a href="t&amp;d_md-ms.php" title="Test &amp; Discussion Course"><span class="sub-arrow"></span>Test &amp; Discussion Course</a></li>
          <li><a href="crash_course.php" title="Crash course"><span class="sub-arrow"></span>Crash Course</a></li>
        </ul>
        <ul class="course-new-list">
          <li style="margin:0px;">
            <h6>Distant Learning Programme</h6>
          </li>
          <li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span>Postal Course</a></li>
          <li><a href="shortcut-to-nimhans.php" title="Shortcut To Nimhans"><span class="sub-arrow"></span>Shortcut To Nimhans</a></li>
          <li><a href="idams.php" title="Tablet Based Course -IDAMS"><span class="sub-arrow"></span>Tablet Based Course -IDAMS</a></li>
        </ul>
        <ul class="course-new-list">
          <li style="margin:0px;">
            <h6>Test Series</h6>
          </li>
          <li><a href="online-test-series.php" title="Online"><span class="sub-arrow"></span>Online</a></li>
          <li><a href="offline-test-series.php" title="Offline"><span class="sub-arrow"></span>Offline</a></li>
          <li><a href="postal-test-series.php" title="Postal"><span class="sub-arrow"></span>Postal</a></li>
        </ul>
      </div>
    </div>
    <div class="coures-list-box">
      <h5 class="h2toggle" onclick="sliddes('2');"><span class="plus-ico" id="s2"></span> Pre-Final/Final Year Student</h5>
      <div class="coures-list-box-content" id="di2" style="display:none;">
        <ul class="course-new-list">
          <li style="margin:0px;">
            <h6>Classroom Courses</h6>
          </li>
          <li><a href="foundation_course.php" title="Foundation Course"><span class="sub-arrow"></span>Foundation Course</a></li>
          <li><a href="#" title="Distant Learning Programme"><span class="sub-arrow"></span>Distant Learning Programme</a></li>
          <li><a href="dams-publication.php?c=1" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span>Tablet Based Course - IDAMS</a></li>
          <li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span>Test Series</a></li>
        </ul>
      </div>
    </div>
    <div class="coures-list-box">
      <h5 class="h2toggle" onclick="sliddes('3');" ><span class="plus-ico" id="s3"></span> 2nd Professional Students</h5>
      <div class="coures-list-box-content" id="di3" style="display:none;">
        <ul class="course-new-list">
          <li style="margin:0px;">
            <h6>Classroom Courses</h6>
          </li>
          <li><a href="prefoundation_course.php" title="Prefoundation Course"><span class="sub-arrow"></span>Prefoundation Course</a></li>
          <li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span>Postal Course</a></li>
          <li><a href="dams-publication.php?c=1" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span>Tablet Based Course - IDAMS</a></li>
          <li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span>Test Series</a></li>
        </ul>
      </div>
    </div>
    <div class="coures-list-box">
      <h5 class="h2toggle" onclick="sliddes('4');" ><span class="plus-ico" id="s4"></span> 1st Professional Students</h5>
      <div class="coures-list-box-content" id="di4" style="display:none;">
        <ul class="course-new-list">
          <li><a href="the_first_step.php" title="First Step Course"><span class="sub-arrow"></span>First Step Course</a></li>
          <li><a href="postal.php" title="Postal Course"><span class="sub-arrow"></span>Postal Course</a></li>
          <li><a href="dams-publication.php?c=1" title="Tablet Based Course - IDAMS"><span class="sub-arrow"></span>Tablet Based Course - IDAMS</a></li>
          <li><a href="test-series.php" title="Test Series"><span class="sub-arrow"></span>Test Series</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
