<?php 
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
include 'connect.php';
$email=base64_decode($_REQUEST['email']);
//var_dump($con);
$sql1="select count(*) cnt from  unsubscribe where email='$email'";
$res=mysql_query($sql1);
$rows=  mysql_fetch_object($res);
//echo "count >>> " . $rows->cnt; 
//var_dump($rows->cnt);
if($rows->cnt == '0'){
$sql="INSERT INTO unsubscribe (email) VALUES ('$email')";
$rs=mysql_query($sql);
}
?>

<!Doctype html>
<html>
	<head>
		<title>DAMS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
		<style>
			*{
				margin:0px;
				padding:0px;
			}
			body{
				font-size:12px;
				font-family: 'Roboto', sans-serif;
			}
			.top-header{
				background:#f5f5f5;
				padding:10px 0px;
				width:100%;
				box-sizing: border-box;
			}
			.logo-part{
				width:100%;
			  padding: 15px 0px 10px;
				background:#fff;
				text-align:center;
				box-sizing: border-box;
			}
			.logo-part img{
				max-width:230px;
			}
			.outer-main-part{
				width:100%;
				margin-top:70px;
				text-align:center;
			}
			.image-data-flow{
				width:100%;
				text-align:center;
			}
			.image-data-flow img{
				max-width: 338px;
			}
			.hding-data{
				width:100%;
				display:block;
				margin-top:20px;
			}
			.hding-data h1{
				color: #ed5a20;
				font-weight: 500;
				font-size:26px;
			}
			.hding-data p{
				font-size:16px;
				color:#333;
				margin: 5px 0px 10px;
				line-height: 22px;
			}
			.btn-data{
				width:100%;
				margin: 40px 0px 20px;
			}
			.btn-data a{
				text-decoration: none;
				color: #fff;
				padding: 16px 35px;
				background: #00a64e;
				display: inline-block;
				font-size:20px;
				border-radius: 10px;
				font-weight: 500;
			}
			@media only screen and (max-width: 500px) {
			  .image-data-flow img{
				max-width:200px;
			}
				.outer-main-part{
					margin-top:30px;
				}
				.hding-data h1{
					font-size:20px;
				}
				.hding-data p{
					font-size:16px;
				}
				.btn-data a{
					padding: 15px 25px;
					font-size: 20px;
				}
			}
        </style>
	</head>
	<body>
		<header class="top-header">
			<div class="logo-part">
				<img src="logo_mobile.jpg">
			</div>
		</header>
		<div class="outer-main-part">
			<div class="image-data-flow">
				<img src="UNSUBSCRIBE-Empty-State.png">
			</div>
			<div class="hding-data">
				<h1>Successfully Unsubscribed.</h1>
				<p>We are sorry to see you go. <br>We have removed you from our mailing list.</p>
				<div class="btn-data">
									<a href="subscribe.php?email=<?php echo  $_REQUEST['email']?>" >Subscribed Again</a>
								   </div>
			</div>
		</div>
	</body>
</html>
