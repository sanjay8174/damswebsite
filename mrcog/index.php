<!DOCTYPE html>
<?php
error_reporting(0);
$course_id = $_REQUEST['c'];
?>
<html>
<head>
<meta charset="UTF-8">
<meta content=True name=HandheldFriendly />
<meta name=viewport content="width=device-width" />
<meta name=viewport content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
<title>MRCOG Coaching Institute, MRCOG</title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/responcive_css.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if gte IE8]><link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '481909165304365');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=481909165304365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="inner-bg no_bg">
<?php include 'registration.php'; ?>
<?php include 'enquiry.php'; ?>
<?php include 'header.php'; ?>
<!-- Banner Start Here -->
<section class="inner-banner">
  <div class="wrapper">
    <article class="usmle-edge-banner">
      <aside class="banner-left">
          <h2>MRCOG - UK</h2>
          <!--<h3>MEMBERSHIP OF THE ROYAL COLLEGES OF OBSTETRICS AND GYNAECOLOGISTS -UK</h3>-->
<!--          <h4>MEMBERSHIP OF THE ROYAL COLLEGES OF OBSTETRICS AND GYNAECOLOGISTS OF THE UNITED KINGDOM</h4>-->
          <h3>PART-1  PREPARATORY COURSE –DELHI </h3>
          <h4>Dates: 3rd and 4th December 2019 </h4>
          <h4>Venue : DAMS, New Delhi, India </h4>
		  <h4>Fee Structure :  Rs.20, 000+GST i.e. Rs.23, 600/-  </h4>
      </aside>
      <?php include 'usmle-banner-btn.php'; ?>
    </article>
  </div>
</section>
<!-- Banner End Here --> 
<!-- Midle Content Start Here -->
<!--<div style="overflow:hidden;">-->
<section class="inner-midle-content">
  <div class="wrapper">
    <aside class="content-left">
      <div class="course-box mrcog_crsbox ">
       <ul class="idTabs responce-show">
        <li><a href="#part1">PART 1</a></li>
        <li><a href="mrcog-part2.php">PART 2</a></li>
       </ul>
      <div id="part1">
          <div style="border: 1px solid rgb(204, 204, 204); display: inline-block; padding: 20px; margin-top: -1px;">
        <h3><span class="book-ur-seat-btn more"><a title="Book Your Seat" href="http://registration.damsdelhi.com"> <span>&nbsp;</span> Book Your Seat</a></span></h3>
              

<!--<span class="book-ur-seat-btn"><a title="Book Your Seat" href="online-registration.php"> <span>&nbsp;</span> Book Your Seat</a></span>--></h3>
        
          <div class="world_class" style="margin: 5px 0 15px; font-size:20px;"><strong>ARLINGTON BRITISH MEDICAL ACADEMY(ABMA)—UK  & Delhi institute of medical sciences(DAMS)—INDIA</strong></div>
        <p>ABMA is proud to announce it’s very popular MRCOG PART 1 Exam preparation course once again. The course is delivered over two days and is divided into several lectures that vary in length from 30 to 60 minutes. The MRCOG Part I examination has had a major overhaul recently with a new format, curriculum and syllabus; accordingly our MRCOG Part I course has been completely modernised to accurately reflect these changes. The faculty is UK based and has been involved in delivering this course both nationally and internationally.<br><br></p>         
          <div>
          <div class="new_image"><img class="img_size" src='images/DAMS_1.jpg'/></div>
          <div class="new_image"><img  class="img_size"  src='images/DAMS_2.jpg'/></div>
          <div class="new_image"><img class="img_size" src='images/DAMS_3.jpg'/></div>
          <div class="new_image"><img class="img_size" src='images/DAMS_4.jpg'/></div>
          <p>(MRCOG award Ceremony – Courtesy RCOG website) </p><br>
          </div>
<!--          <p>Our MRCOG exam preparatory Courses have reputation and high pass rate. Our course uses internationally renowned speakers throughout three days. Prior to, and during the course, we will give you a minimum exam guidance, revision format with feedback and examination technique tips. There will be a full mock exam with review at the end of the course. The course also includes guideline discussion, lectures and small group teaching.<br><br></p>-->
          <p class="new_cont" style="float: left;width: 100%;"><span style="padding: 8px 10px;background-color: #faf47f;float: left;"><strong >The gold standard qualification in O&G</strong></span></p>
         <p>The MRCOG exam is internatinally respected asthe gold standard qualificaion for career propression in O&G, it's your passport to the very pinnacle of the profession </p>
<!--   
         <p class="new_cont"><strong>Take the first steps with the Part 1 MRCOG exam</strong></p>
         <p>The Part 1 MRCOG is a written exam in the basic and clinical sciences relevant to O&G. You can take the Part 1 MRCOG any time after graduation with a medical degree and can sit the exam in over several exam centres across India. </p>-->
          </div>
          
      <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course Facilitator</div>
  <div class="course-new-section">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
<!--          <div class="mrcp_boxess mrcp_boxess_cntr" style="margin-top:0;">
        <div class="mrcp_dr_img dr_imgbordr"><img src='images/Dr_Matthe_Wood.jpg'/></div>
        <div class="mrcp_inr_boxes" style="float:left;">
            <h1 class="mrcp_dr_ttl">Dr. GUY CALCOTT</h1>
            <p class="mrcp_dr_sbtl">MRCOG</p>
            <p class="mrcp_dr_sbtl"> Course Co-Director</p>
            <p class="mrcp_dr_sbtl"> Obs & Gyn – Senior Specialist Registrar,(ST5) Shrewsbury and Telford Hospital NHS trustWest Midland, UK</p><br>
            <p style="font-size:14px;">Dr Wood is currently a senior registrar in West Midland region of the UK. His undergraduate training was at University of Southampton School of Medicine UK. He has been involved in teaching and training on various courses including Health
                Education West Midlands O&G careers day lecturer and facilitator, medical student teaching from Birmingham University medical, Writing questions for question bank of RCOG STRATOG education resource, etc. He is our course co-director.</p>
        </div>
</div>-->

          <div class="mrcp_boxess mrcp_boxess_cntr">
            <div class="mrcp_dr_img dr_imgbordr"><img src='images/Sandeep_Goyal.png'/></div>
            <div class="mrcp_inr_boxes" style="float:left;">
                <h1 class="mrcp_dr_ttl">Dr Sandeep Goyal</h1>
                <p class="mrcp_dr_sbtl">MRCS, MRCOG </p>
                <p class="mrcp_dr_sbtl">Member of Royal College of Obs & Gyn UK</p>          
                <p style="font-size:14px;">Senior Clinical fellow in Obstetrics and Gynaecology at Royal Wolverhampton NHS, UK </p>
                <p style="font-size:14px;">Has wide range of experience in various surgical specialities gained from his practice in various hospitals in UK.</p>
                <p style="font-size:14px;">Dr Goyal is our course director for MRCOG PART 1. He has been involved in in facilitating O&G specialist training in the UK and involved in teaching for several year and soon will be taking a consultant post in the UK.</p>
            </div>

        </div>   

       <div class="mrcp_boxess mrcp_boxess_cntr">
           <div class="mrcp_dr_img dr_imgbordr"><img src='images/Olivia_Moran.jpg'/></div>
            <div class="mrcp_inr_boxes" style="float:left;">
                <h1 class="mrcp_dr_ttl">Dr. Olivia Moran</h1>
                <p class="mrcp_dr_sbtl">MBChB (Hons), BSc (Hons)</p>          
                <p style="font-size:14px;">Dr Moran currently works as an Obstetrics and Gynaecology registrar in a busy tertiary centre in Manchester UK. She hold a post as an academic fellow, conducting research at the Maternal and Fetal Health Research Centre, with a special interest in hypertension in pregnancy.</p>
            </div>

        </div>

       <div class="mrcp_boxess mrcp_boxess_cntr">
           <div class="mrcp_dr_img dr_imgbordr"><img src='images/Gillian_Jackson.jpg'/></div>
            <div class="mrcp_inr_boxes" style="float:left;">
                <h1 class="mrcp_dr_ttl">Dr. Gillian Jackson</h1>
                <p style="font-size:14px;">Qualified from University of Newcastle with MBBS Merit in 2009, completed Foundation Programme in 2011 in Manchester. Created an Educational project 'ATSP' to help Junior Doctors make the transition from student to doctor in NHS  Hospitals.
</p>
            </div>

        </div>
      
      
      </div>
    </div>
    </div>
  </div>
  
  <div class="pg-medical-main" style="display:block;">
  <div class="pg-heading"><span></span>Course highlight</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
       <p class="course_dtlsp"> Join us for an interactive, engaging and condensed preparatory course to help you pass the MRCOG Part 1.  This course will be two days of lectures, group work and practice questions used to help candidates pass the Part 1. </p>

         <ul class="course-new-list">
          <li><span class="sub-arrow"></span>A <span style="font-weight: 600;">comprehensive course </span>that covers the content of the new MRCOG Part I exam syllabus.</li>
          <li><span class="sub-arrow"></span>Extensive <span style="font-weight: 600;">exam-like questions</span>, covering entire syllabus and topics from recent exams.</li>
          <li><span class="sub-arrow"></span>We supply <span style="font-weight: 600;">comprehensive delegate notes</span>.</li>
          <li><span class="sub-arrow"></span>Interactive tuition with individual feedback.</li> 
          <li><span class="sub-arrow"></span>We provide essential exam tips and recent and relevant updates.</li>
          <li><span class="sub-arrow"></span>An entire three intense days activity is dedicated to the course, to maximise work intensity and syllabus coverage.</li>          
        </ul>
      </div>
    </div>
  </div>
 </div>
 
 <div class="pg-medical-main" >
  <div class="pg-heading"><span></span>Course Strengths</div>
  <div class="course-new-section" style="text-align:left;">
    <div class="coures-list-box">
      <div class="coures-list-box-content">
        <ul class="course-new-list">
          <li><span class="sub-arrow"></span>Small teaching faculty with an active interest in medical education.</li>
          <li><span class="sub-arrow"></span>Constantly updated teaching material.</li>
          <li><span class="sub-arrow"></span>Multiple Choice Question based format to mirror the real exam.</li>
          <li><span class="sub-arrow"></span>Non confrontational teaching style.</li> 
          <li><span class="sub-arrow"></span>Use of repetition of difficult concepts to aid learning.</li>
          <li><span class="sub-arrow"></span>Clear, well designed visual teaching aids.</li>          
        </ul>
        <p class="course_dtlsp"><strong><br>Come join us to help you prepare and pass the MRCOG Part 1 in an engaging and open learning environment.</strong></p>
      </div>
    </div>
  </div>
 </div>
 <img src="images/mrcog.jpg" width="100%" style="margin-top:15px; border:1px solid #ccc;">
</div>
      </div>
        
       <div class="course-box mrcog_crsbox ">
      
        
       </div>
      </div>
   </div>   
      
    </aside>    
    <aside class="content-right">
        <div class="content-royal-college res_css">
            <img src="images/logo-royal-college.png" />
            <p>MEMBERSHIP OF THE ROYAL COLLEGES</p>
            <p>OF OBSTETRICS AND GYNAECOLOGISTS OF THE UNITED KINGDOM</p>
        </div>
        <div class="content-royal-college res_css" style="margin-top: 15px;">
            <img style="width:100px;" src="images/AMBA.png" />
            <p>ARLINGTON BRITISH MEDICAL ACADEMY(ABMA)—UK </p>
           
        </div>
        <div class="content-date-venue res_css">
            <h1>DATES</h1>
            <p style="font-weight:bold;font-size:24px;">3<small class="th">rd</small> and 4<small class="th">th</small> December 2019 </p>
            <p style="border: 1px dotted #c4c4c4;margin:10px 0px;width:95%;"></p>
             <h1>VENUE</h1>
            <p>4 B, Grover Chamber</p>
            <p>Near Karol Bagh Metro Station</p>
            <p>New Delhi, Delhi</p>
            <p>Ph. No. : 011-40094009</p>
        </div>
        
<!--        <div id="part1">
      
        <div class="content-date-venue res_css">
            <h1 style="color:black">What is unique about this course</h1>
            <p style="color:black">The faculty have attended some of the most popular UK MRCOG Part 1 preparation courses and picked the best aspects from each to deliver this course. Instead of the traditional week of tiring endless lectures full of slides, we hope to prepare candidates with a whistle stop tour of MRCOG Part 1. We will cover 18 core modules of the MRCOG Part 1. We will provide concise invigorating and motivational preparatory content to guide candidates on a path to MRCOG Part 1 success.</p>
        </div>
      
        </div>-->
        
        
       
          <?php include 'enquiryform.php'; ?>
    </aside>
  </div>
</section>
<!-- Midle Content End Here --> 
<!-- Footer Css Start Here -->
<?php include 'footer.php'; ?>
<!-- Footer Css End Here --> 
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/registration.js"></script>
<script type="text/javascript" src="js/add-cart.js"></script>
</body>
</html>
